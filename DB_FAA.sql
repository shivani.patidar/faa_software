
CREATE Database DB_FAA_Software;
Use DB_FAA_Software;

-- MySQL dump 10.13  Distrib 8.0.18, for Linux (x86_64)
--
-- Host: localhost    Database: DB_FAA_240
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Tbl_Address`
--

DROP TABLE IF EXISTS `Tbl_Address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Address` (
  `Add_ID` mediumint(9) NOT NULL AUTO_INCREMENT,
  `Add_Line1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Add_Line2` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Add_Line3` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Add_Pin` mediumint(6) DEFAULT NULL,
  `Add_City` smallint(6) DEFAULT '0',
  `Add_State` tinyint(4) DEFAULT '0',
  `Add_Country` tinyint(4) DEFAULT '0',
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  PRIMARY KEY (`Add_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Address`
--

LOCK TABLES `Tbl_Address` WRITE;
/*!40000 ALTER TABLE `Tbl_Address` DISABLE KEYS */;
INSERT INTO `Tbl_Address` VALUES (1,'dfasdf','asdfas','sadfa',4004004,444,1,1,1,1,1),(2,'asdf','sadf','sadfas',223223,33,33,33,1,1,1),(3,'sdf','sadf','sadf',23442,3,3,3,1,1,1),(4,'asfasdfdasfasdf','asdfasdfasdfasdfasd','asdfsadfasdfasdf',13123,2,2,2,1,1,1),(5,'asfasdfdasfasdf','asdfasdfasdfasdfasd','asdfsadfasdfasdf',13123,2,2,2,1,1,1),(6,'asfasdfdasfasdf','asdfasdfasdfasdfasd','asdfsadfasdfasdf',13123,2,2,2,1,1,1),(7,'asfasdfdasfasdf','asdfasdfasdfasdfasd','asdfsadfasdfasdf',13123,2,2,2,1,1,1),(8,'asfasdfdasfasdf','asdfasdfasdfasdfasd','asdfsadfasdfasdf',13123,2,2,2,1,1,1),(9,'asfasdfdasfasdf','asdfasdfasdfasdfasd','asdfsadfasdfasdf',13123,2,2,2,1,1,1),(10,'asfasdfdasfasdf','asdfasdfasdfasdfasd','asdfsadfasdfasdf',13123,2,2,2,1,1,1),(11,'asfasdfdasfasdf','asdfasdfasdfasdfasd','asdfsadfasdfasdf',13123,2,2,2,1,1,1),(12,'asfasdfdasfasdf','asdfasdfasdfasdfasd','asdfsadfasdfasdf',13123,2,2,2,1,1,1),(13,'asfasdfdasfasdf','asdfasdfasdfasdfasd','asdfsadfasdfasdf',13123,2,2,2,1,1,1),(14,'asfasdfdasfasdf','asdfasdfasdfasdfasd','asdfsadfasdfasdf',13123,2,2,2,1,1,1),(15,'asfasdfdasfasdf','asdfasdfasdfasdfasd','asdfsadfasdfasdf',13123,2,2,2,1,1,1);
/*!40000 ALTER TABLE `Tbl_Address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Calibration`
--

DROP TABLE IF EXISTS `Tbl_Calibration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Calibration` (
  `Cal_ID` mediumint(9) NOT NULL,
  `Test_ID` smallint(6) NOT NULL,
  `CurveType` tinyint(4) NOT NULL,
  `Cal_Expiry` date NOT NULL,
  `Cal_Validity` date NOT NULL,
  `Cal_Validity_Days` smallint(6) NOT NULL,
  `NoOfStds` tinyint(4) NOT NULL,
  `IsModified` tinyint(4) NOT NULL,
  `Factor` float NOT NULL,
  `Factor1` float NOT NULL,
  `Factor2` float NOT NULL,
  `Factor3` float NOT NULL,
  `Factor4` float NOT NULL,
  `Factor5` float NOT NULL,
  `Cal_Date` date NOT NULL,
  `Cal_Time` time NOT NULL,
  `PrevCalID_RgtBlnking` mediumint(9) NOT NULL,
  `Cal_Status` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  PRIMARY KEY (`Cal_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Calibration`
--

LOCK TABLES `Tbl_Calibration` WRITE;
/*!40000 ALTER TABLE `Tbl_Calibration` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Calibration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_CalibrationDet`
--

DROP TABLE IF EXISTS `Tbl_CalibrationDet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_CalibrationDet` (
  `Cal_ID` int(11) NOT NULL,
  `IsBlank` tinyint(4) NOT NULL,
  `Pos` int(11) NOT NULL,
  `Conc` float NOT NULL,
  `ActualOD` float NOT NULL,
  `OD` float NOT NULL,
  `Lot` int(11) NOT NULL,
  `DilRatio` int(11) NOT NULL,
  `Cuv` int(11) NOT NULL,
  `IsModified` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_CalibrationDet`
--

LOCK TABLES `Tbl_CalibrationDet` WRITE;
/*!40000 ALTER TABLE `Tbl_CalibrationDet` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_CalibrationDet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_DeadVolume`
--

DROP TABLE IF EXISTS `Tbl_DeadVolume`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_DeadVolume` (
  `Dict_ItemID` int(11) NOT NULL,
  `Steps` mediumint(9) DEFAULT NULL,
  `PerStepVol` float DEFAULT NULL,
  `DV_Volume` mediumint(9) DEFAULT NULL,
  `DV_TIME` datetime DEFAULT NULL,
  `UID` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_DeadVolume`
--

LOCK TABLES `Tbl_DeadVolume` WRITE;
/*!40000 ALTER TABLE `Tbl_DeadVolume` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_DeadVolume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Directory`
--

DROP TABLE IF EXISTS `Tbl_Directory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Directory` (
  `Dict_ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `Dict_Desc` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Dict_Remark` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `IsEditable` tinyint(1) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  PRIMARY KEY (`Dict_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Directory`
--

LOCK TABLES `Tbl_Directory` WRITE;
/*!40000 ALTER TABLE `Tbl_Directory` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Directory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Directory_Det`
--

DROP TABLE IF EXISTS `Tbl_Directory_Det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Directory_Det` (
  `Dict_ItemID` smallint(6) NOT NULL AUTO_INCREMENT,
  `Dict_ID` smallint(6) NOT NULL,
  `Dict_Item` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ItemValue` varchar(25) DEFAULT NULL,
  `Dict_Item_Desc` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `IsEditable` tinyint(1) DEFAULT NULL,
  `IsVisible` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  PRIMARY KEY (`Dict_ItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Directory_Det`
--

LOCK TABLES `Tbl_Directory_Det` WRITE;
/*!40000 ALTER TABLE `Tbl_Directory_Det` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Directory_Det` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Doctor`
--

DROP TABLE IF EXISTS `Tbl_Doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Doctor` (
  `DR_ID` smallint(6) NOT NULL,
  `Person_ID` mediumint(9) NOT NULL,
  `Hospital_ID` smallint(6) DEFAULT NULL,
  `DispName` varchar(20) DEFAULT NULL,
  `C_UID` tinyint(4) DEFAULT NULL,
  `LastMod_UID` tinyint(4) DEFAULT NULL,
  `D_UID` tinyint(4) DEFAULT NULL,
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`DR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Doctor`
--

LOCK TABLES `Tbl_Doctor` WRITE;
/*!40000 ALTER TABLE `Tbl_Doctor` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Errors`
--

DROP TABLE IF EXISTS `Tbl_Errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Errors` (
  `Error_ID` smallint(6) NOT NULL,
  `Error_Code` varchar(5) DEFAULT NULL,
  `Error_Desc` varchar(50) DEFAULT NULL,
  `Error_Action` varchar(45) DEFAULT NULL,
  `Error_Flag` varchar(5) DEFAULT NULL,
  `Is_StopError` tinyint(4) DEFAULT '0',
  `Assembly_ID` smallint(6) DEFAULT NULL,
  `Instrument_ID` tinyint(4) DEFAULT NULL,
  `Log_Error` tinyint(4) DEFAULT '1' COMMENT '1 if need to log this error in Database else 0',
  `Recover_Try_Cnt` tinyint(4) DEFAULT NULL COMMENT 'No of times software should try to auto recover due to this error\n',
  `DisplayToUser` tinyint(4) DEFAULT '1' COMMENT '1 . If error displayed to user.\n',
  PRIMARY KEY (`Error_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Errors`
--

LOCK TABLES `Tbl_Errors` WRITE;
/*!40000 ALTER TABLE `Tbl_Errors` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Errors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Flags`
--

DROP TABLE IF EXISTS `Tbl_Flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Flags` (
  `Flag_ID` int(11) NOT NULL,
  `Flag_Type` tinyint(4) DEFAULT NULL COMMENT '1. Absent\n2. Instrument_Warning.\n3. Normal_Warning\n4..RESULT\n5.  Instrument_Error\n ',
  `Flag_Code` varchar(7) DEFAULT NULL,
  `Flag_Msg` varchar(50) DEFAULT NULL,
  `Tbl_Flagscol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Flag_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Flags`
--

LOCK TABLES `Tbl_Flags` WRITE;
/*!40000 ALTER TABLE `Tbl_Flags` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Flags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Instrument_Settings`
--


DROP TABLE IF EXISTS `Tbl_Instrument_Settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Instrument_Settings` (
  `Instrument_Settings_ID` int(11) NOT NULL,
  `Instrument_ID` int(11) NOT NULL,
  `Arm_Pos_Home` INT NOT NULL DEFAULT '0',
  `Arm_Pos_RGT_In` INT NOT NULL DEFAULT '0',
  `Arm_Pos_RGT_Out` INT NOT NULL DEFAULT '0',
  `Arm_Pos_RGT_Smp` INT NOT NULL DEFAULT '0',
  `Arm_Pos_Cuv` INT NOT NULL DEFAULT '0',
  `Arm_Depth_Cuv` INT NOT NULL DEFAULT '0',
  `Arm_Depth_Home` INT NOT NULL DEFAULT '0',
  `Arm_Depth_RGT_In` INT NOT NULL DEFAULT '0',
  `Arm_Depth_RGT_Out` INT NOT NULL DEFAULT '0',
  `Cru_Depth_Wash` INT NOT NULL DEFAULT '0',
  `Cru_Depth_Water` INT NOT NULL DEFAULT '0',
  `Str_Pos_Home` INT NOT NULL DEFAULT '0',
  `Str_Pos_Cuv` INT NOT NULL DEFAULT '0',
  `Str_Depth_Cuv` INT NOT NULL DEFAULT '0',
  `Str_Depth_Home` INT NOT NULL DEFAULT '0',
  `Str_Speed` INT NOT NULL DEFAULT '0',
  `Rgt_Pos_In` INT NOT NULL DEFAULT '0',
  `Rgt_Pos_Out` INT NOT NULL DEFAULT '0',
  `Rgt_Pos_Smp` INT NOT NULL DEFAULT '0',
  `Rgt_Scan_In` INT NOT NULL DEFAULT '0',
  `Rgt_Scan_Out` INT NOT NULL DEFAULT '0',
  `Rgt_Scan_Smp` INT NOT NULL DEFAULT '0',
  `Syringe_Offset` INT NOT NULL DEFAULT '0',
  `Add_Water_Time` INT NOT NULL DEFAULT '0',
  `Remove_Water_Time` INT NOT NULL DEFAULT '0',
  `Path_Length` DECIMAL(2,2) NOT NULL DEFAULT '0.65',
   PRIMARY KEY (`Instrument_Settings_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Instrument_Settings`
--

LOCK TABLES `Tbl_Instrument_Settings` WRITE;
/*!40000 ALTER TABLE `Tbl_Instrument_Settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Instrument_Settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Instrument_Assmbly`
--

DROP TABLE IF EXISTS `Tbl_Instrument_Assmbly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Instrument_Assmbly` (
  `Assembly_ID` smallint(6) NOT NULL,
  `Instrument_ID` tinyint(4) DEFAULT NULL,
  `Assembly` varchar(20) DEFAULT NULL,
  `ASW_CODE` tinyint(4) DEFAULT NULL,
  `ESW_CODE` tinyint(4) DEFAULT NULL,
  `ASW_NAME` varchar(10) DEFAULT NULL,
  `InstalledDate` datetime DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT '1',
  `UID` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`Assembly_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Instrument_Assmbly`
--

LOCK TABLES `Tbl_Instrument_Assmbly` WRITE;
/*!40000 ALTER TABLE `Tbl_Instrument_Assmbly` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Instrument_Assmbly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Instrument_Communication`
--

DROP TABLE IF EXISTS `Tbl_Instrument_Communication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Instrument_Communication` (
  `Instrument_ID` tinyint(4) NOT NULL,
  `Com_Type` tinyint(4) DEFAULT NULL COMMENT '1 Serial Port\n2 TCP/IP\n',
  `Com_Port` tinyint(4) DEFAULT NULL,
  `BaudRate` smallint(6) DEFAULT NULL,
  `LIS_Enabled` tinyint(4) DEFAULT NULL COMMENT '1- if LIS Settings are ON\n0- if LIS Settings are OFF',
  `LIS_Com_Type` tinyint(4) DEFAULT NULL,
  `LIS_Com_Port` tinyint(4) DEFAULT NULL,
  `LIS_BaudRate` smallint(6) DEFAULT NULL,
  `LIS_Server_IP` varchar(15) DEFAULT NULL,
  `LIS_TCP_Port` tinyint(4) NOT NULL,
  `LIS_Data_Type` tinyint(4) DEFAULT NULL COMMENT '1- Single Packet at a time\n2. Multiple Packets at a time\n \n\n',
  `LIS_SOT` tinyint(4) DEFAULT NULL,
  `LIS_EOT` tinyint(4) DEFAULT NULL,
  `LIS_STX` tinyint(4) DEFAULT NULL,
  `LIS_ETX` tinyint(4) DEFAULT NULL,
  `LIS_ACK` tinyint(4) DEFAULT NULL,
  `LIS_NACK` tinyint(4) DEFAULT NULL,
  `LIS_ENQ` tinyint(4) DEFAULT NULL,
  `LIS_PACKET_SEPERATOR` tinyint(4) DEFAULT NULL,
  `LIS_PARMETER_SEPERATOR` tinyint(4) DEFAULT NULL,
  `LIS_DELIMETER` tinyint(4) DEFAULT NULL,
  UNIQUE KEY `Instrument_ID_UNIQUE` (`Instrument_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Instrument_Communication`
--

LOCK TABLES `Tbl_Instrument_Communication` WRITE;
/*!40000 ALTER TABLE `Tbl_Instrument_Communication` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Instrument_Communication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Log_Action`
--

DROP TABLE IF EXISTS `Tbl_Log_Action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Log_Action` (
  `LogA_ID` bigint(20) NOT NULL,
  `U_ID` varchar(45) DEFAULT NULL,
  `A_Date` date DEFAULT NULL,
  `A_Time` time DEFAULT NULL,
  `LogA_Type` tinyint(4) DEFAULT NULL COMMENT '1. USER_LOGIN\n2. USER_LOGOUT\n3. SAVE\n4. UPDATE\n5. DELETE\n6. SERVICE_CHECK\n7. MAINTENACE\n8..AUTOMAINTENANCE\n9. OPEN_Window\n10.CLOSE_WINDOW\n11. WARNING\n12. ',
  `Functionality` varchar(20) DEFAULT NULL,
  `Action` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`LogA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Log_Action`
--

LOCK TABLES `Tbl_Log_Action` WRITE;
/*!40000 ALTER TABLE `Tbl_Log_Action` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Log_Action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Log_Action_Det`
--

DROP TABLE IF EXISTS `Tbl_Log_Action_Det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Log_Action_Det` (
  `LogA_ID` bigint(20) NOT NULL,
  `OLD_VALUE` varchar(30) DEFAULT NULL,
  `NW_VALUE` varchar(30) DEFAULT NULL,
  `Column_Name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Log_Action_Det`
--

LOCK TABLES `Tbl_Log_Action_Det` WRITE;
/*!40000 ALTER TABLE `Tbl_Log_Action_Det` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Log_Action_Det` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Log_Error`
--

DROP TABLE IF EXISTS `Tbl_Log_Error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Log_Error` (
  `Error_ID` bigint(20) NOT NULL,
  `U_ID` tinyint(4) DEFAULT NULL,
  `Error_During` tinyint(4) DEFAULT NULL COMMENT '1. AUTO MAINTENANCE_STARTUP\n2. AUTO MAINTENANCE_SHUTDOWN\n3. SERVICE_CHECK\n4. MAINTENANCE\n5.PRE_RUN\n6. RUN\n7.POST_RUN\n8.USER_ACTIONS(Barcode-scan/volume-check)',
  `Error_Code` smallint(6) DEFAULT NULL,
  `Err_Date` date DEFAULT NULL,
  `Err_Time` time DEFAULT NULL,
  `Functionality` varchar(30) DEFAULT NULL COMMENT 'RUN/CuvWash/tempsetting/servicecheck/preobe-clean',
  `Action` varchar(30) DEFAULT NULL COMMENT 'armrotate/updn/rgt rotate',
  `Assembly` varchar(30) DEFAULT NULL COMMENT 'ARM/TRAY/ISE/STR/CRU',
  `ForPosition` tinyint(4) DEFAULT NULL COMMENT 'RGT/SMP Pos',
  `ForCuv` tinyint(4) DEFAULT NULL COMMENT 'For which CUV(STR/ARM HIT)',
  PRIMARY KEY (`Error_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Log_Error`
--

LOCK TABLES `Tbl_Log_Error` WRITE;
/*!40000 ALTER TABLE `Tbl_Log_Error` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Log_Error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Log_Settings`
--

DROP TABLE IF EXISTS `Tbl_Log_Settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Log_Settings` (
  `Set_ID` int(11) NOT NULL,
  `U_ID` tinyint(4) DEFAULT NULL,
  `Assembly_ID` tinyint(4) NOT NULL COMMENT 'ARM/TRAY/ISE/STR/CRU',
  `Set_Date` date DEFAULT NULL,
  `Set_Time` time DEFAULT NULL,
  `PATH` tinyint(4) NOT NULL,
  `Old_Value` tinyint(4) NOT NULL,
  `New_Value` tinyint(4) NOT NULL,
  PRIMARY KEY (`Set_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Log_Settings`
--

LOCK TABLES `Tbl_Log_Settings` WRITE;
/*!40000 ALTER TABLE `Tbl_Log_Settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Log_Settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Lot`
--

DROP TABLE IF EXISTS `Tbl_Lot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Lot` (
  `Lot_ID` mediumint(9) DEFAULT NULL,
  `ConsumableType` tinyint(4) NOT NULL,
  `Test_ID` smallint(6) NOT NULL,
  `LotNo` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `MFG_ID` tinyint(4) NOT NULL,
  `MFG_Month` tinyint(4) NOT NULL,
  `MFG_Year` smallint(6) NOT NULL,
  `EXP_Month` tinyint(4) NOT NULL,
  `EXP_Year` smallint(6) NOT NULL,
  `OpenWhileStability` smallint(6) NOT NULL,
  `StabilityEnd` date NOT NULL,
  `CONC` float NOT NULL,
  `OD` float NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `IsMultiPoint` tinyint(4) NOT NULL,
  `MEAN` float NOT NULL,
  `SD` float NOT NULL,
  `Lab_Mean` float NOT NULL,
  `Lab_SD` float NOT NULL,
  `NoOfStd` tinyint(4) NOT NULL,
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Lot`
--

LOCK TABLES `Tbl_Lot` WRITE;
/*!40000 ALTER TABLE `Tbl_Lot` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Lot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Lot_Det`
--

DROP TABLE IF EXISTS `Tbl_Lot_Det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Lot_Det` (
  `Lot_ID` mediumint(9) NOT NULL,
  `CONC` float NOT NULL,
  `STD_OD` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Lot_Det`
--

LOCK TABLES `Tbl_Lot_Det` WRITE;
/*!40000 ALTER TABLE `Tbl_Lot_Det` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Lot_Det` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Patient`
--

DROP TABLE IF EXISTS `Tbl_Patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Patient` (
  `Patient_ID` mediumint(9) NOT NULL AUTO_INCREMENT,
  `Pat_Barcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DispName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DOB` datetime DEFAULT NULL,
  `Age` float DEFAULT NULL,
  `AgeUnit` tinyint(4) NOT NULL,
  `Add_ID` mediumint(9) NOT NULL,
  `Sex` tinyint(4) NOT NULL,
  `Remark` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  PRIMARY KEY (`Patient_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Patient`
--

LOCK TABLES `Tbl_Patient` WRITE;
/*!40000 ALTER TABLE `Tbl_Patient` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Person`
--

DROP TABLE IF EXISTS `Tbl_Person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Person` (
  `Person_ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `P_Title` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `P_FirstName` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `P_MidleName` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `P_LastName` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `P_DisplayName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `P_DOB` date DEFAULT NULL,
  `P_AgeDisplay` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `P_AgeYear` smallint(6) DEFAULT '0',
  `P_AgeMonth` smallint(6) DEFAULT '0',
  `P_AgeDays` smallint(6) DEFAULT '0',
  `P_IdentityMark` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `P_HeightInCm` smallint(6) DEFAULT '0',
  `P_WeightInKG` float DEFAULT '0',
  `P_AddID` smallint(6) NOT NULL,
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  PRIMARY KEY (`Person_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Person`
--

LOCK TABLES `Tbl_Person` WRITE;
/*!40000 ALTER TABLE `Tbl_Person` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Profile_Det`
--

DROP TABLE IF EXISTS `Tbl_Profile_Det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Profile_Det` (
  `Profile_ID` int(11) NOT NULL,
  `Test_ID` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Profile_Det`
--

LOCK TABLES `Tbl_Profile_Det` WRITE;
/*!40000 ALTER TABLE `Tbl_Profile_Det` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Profile_Det` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Reads_Results`
--

DROP TABLE IF EXISTS `Tbl_Reads_Results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Reads_Results` (
  `Read_ID` mediumint(9) NOT NULL,
  `Blank_Count` mediumint(9) NOT NULL,
  `Blank_OD` float NOT NULL,
  `Read_Count` tinyint(4) NOT NULL,
  `Instrument_ID` tinyint(4) NOT NULL,
  `R1` float DEFAULT NULL,
  `R2` float DEFAULT NULL,
  `R3` float DEFAULT NULL,
  `R4` float DEFAULT NULL,
  `R5` float DEFAULT NULL,
  `R6` float DEFAULT NULL,
  `R7` float DEFAULT NULL,
  `R8` float DEFAULT NULL,
  `R9` float DEFAULT NULL,
  `R10` float DEFAULT NULL,
  `R11` float DEFAULT NULL,
  `R12` float DEFAULT NULL,
  `R13` float DEFAULT NULL,
  `R14` float DEFAULT NULL,
  `R15` float DEFAULT NULL,
  `R16` float DEFAULT NULL,
  `R17` float DEFAULT NULL,
  `R18` float DEFAULT NULL,
  `R19` float DEFAULT NULL,
  `R20` float DEFAULT NULL,
  `R21` float DEFAULT NULL,
  `R22` float DEFAULT NULL,
  `R23` float DEFAULT NULL,
  `R24` float DEFAULT NULL,
  `R25` float DEFAULT NULL,
  `R26` float DEFAULT NULL,
  `R27` float DEFAULT NULL,
  `R28` float DEFAULT NULL,
  `R29` float DEFAULT NULL,
  `R30` float DEFAULT NULL,
  `R31` float DEFAULT NULL,
  `R32` float DEFAULT NULL,
  `R33` float DEFAULT NULL,
  `R34` float DEFAULT NULL,
  `R35` float DEFAULT NULL,
  `R36` float DEFAULT NULL,
  `R37` float DEFAULT NULL,
  `R38` float DEFAULT NULL,
  `R39` float DEFAULT NULL,
  `R40` float DEFAULT NULL,
  `R41` float DEFAULT NULL,
  `R42` float DEFAULT NULL,
  `R43` float DEFAULT NULL,
  `R44` float DEFAULT NULL,
  `R45` float DEFAULT NULL,
  `R46` float DEFAULT NULL,
  `R47` float DEFAULT NULL,
  `R48` float DEFAULT NULL,
  `R49` float DEFAULT NULL,
  `R50` float DEFAULT NULL,
  `R51` float DEFAULT NULL,
  `R52` float DEFAULT NULL,
  `R53` float DEFAULT NULL,
  `R54` float DEFAULT NULL,
  `R55` float DEFAULT NULL,
  `R56` float DEFAULT NULL,
  `R57` float DEFAULT NULL,
  `R58` float DEFAULT NULL,
  `R59` float DEFAULT NULL,
  `R60` float DEFAULT NULL,
  `R61` float DEFAULT NULL,
  `R62` float DEFAULT NULL,
  `R63` float DEFAULT NULL,
  `R64` float DEFAULT NULL,
  `R65` float DEFAULT NULL,
  `R66` float DEFAULT NULL,
  `R67` float DEFAULT NULL,
  `R68` float DEFAULT NULL,
  `R69` float DEFAULT NULL,
  `R70` float DEFAULT NULL,
  `R71` float DEFAULT NULL,
  `R72` float DEFAULT NULL,
  `R73` float DEFAULT NULL,
  `R74` float DEFAULT NULL,
  `R75` float DEFAULT NULL,
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Read_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Reads_Results`
--

LOCK TABLES `Tbl_Reads_Results` WRITE;
/*!40000 ALTER TABLE `Tbl_Reads_Results` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Reads_Results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Reads_blank`
--

DROP TABLE IF EXISTS `Tbl_Reads_blank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Reads_blank` (
  `CuvNo` tinyint(4) NOT NULL,
  `Filter_ID` tinyint(4) NOT NULL,
  `RCount` int(11) NOT NULL,
  `ReadTime` datetime DEFAULT NULL,
  `Read` float DEFAULT NULL,
  `Read_Status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`CuvNo`,`Filter_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Reads_blank`
--

LOCK TABLES `Tbl_Reads_blank` WRITE;
/*!40000 ALTER TABLE `Tbl_Reads_blank` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Reads_blank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Ref_Ranges`
--

DROP TABLE IF EXISTS `Tbl_Ref_Ranges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Ref_Ranges` (
  `Ref_ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `Test_ID` smallint(6) NOT NULL,
  `Gender` tinyint(4) DEFAULT NULL,
  `Ref_Low` float NOT NULL DEFAULT '0',
  `Ref_High` float NOT NULL DEFAULT '100',
  `Age_Min` smallint(6) NOT NULL DEFAULT '0',
  `Age_Max` smallint(6) NOT NULL,
  `Age_Unit` tinyint(4) NOT NULL DEFAULT '1',
  `Age_Min_Days` smallint(6) NOT NULL DEFAULT '0' COMMENT 'FLAG SHOULD BE CALCULATED ON THIS COLOUM',
  `Age_Max_Days` smallint(6) NOT NULL,
  PRIMARY KEY (`Ref_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Ref_Ranges`
--

LOCK TABLES `Tbl_Ref_Ranges` WRITE;
/*!40000 ALTER TABLE `Tbl_Ref_Ranges` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Ref_Ranges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Reflex_Det`
--

DROP TABLE IF EXISTS `Tbl_Reflex_Det`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Reflex_Det` (
  `Reflex_ID` tinyint(4) NOT NULL,
  `Test_ID` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Reflex_Det`
--

LOCK TABLES `Tbl_Reflex_Det` WRITE;
/*!40000 ALTER TABLE `Tbl_Reflex_Det` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Reflex_Det` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Result`
--

DROP TABLE IF EXISTS `Tbl_Result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Result` (
  `Result_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Test_ID` smallint(6) NOT NULL,
  `Sample_ID` int(11) NOT NULL,
  `DaySeqID` int(11) NOT NULL,
  `ScheduleID` int(11) NOT NULL,
  `Smp_Pos` tinyint(4) NOT NULL,
  `Cuv` tinyint(4) NOT NULL,
  `SchType` int(11) NOT NULL,
  `Cal_ID` int(11) NOT NULL,
  `RgtLot` int(11) NOT NULL,
  `ConsLot` int(11) NOT NULL,
  `OD0` float NOT NULL,
  `RgtBlank` float NOT NULL,
  `OD` float NOT NULL,
  `ResultValue` float NOT NULL,
  `Result` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Flag` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `R_Date` date NOT NULL,
  `R_Time` time NOT NULL,
  `Test_Type` int(11) NOT NULL,
  PRIMARY KEY (`Result_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Result`
--

LOCK TABLES `Tbl_Result` WRITE;
/*!40000 ALTER TABLE `Tbl_Result` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Results_Extern`
--

DROP TABLE IF EXISTS `Tbl_Results_Extern`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Results_Extern` (
  `ScheduleID` int(11) NOT NULL,
  `Extern_TestID` smallint(6) DEFAULT NULL,
  `Result` varchar(15) DEFAULT NULL,
  `Flag` varchar(7) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT '1',
  `C_UID` tinyint(4) DEFAULT NULL,
  `LastMod_UID` tinyint(4) DEFAULT NULL,
  `D_UID` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ScheduleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Results_Extern`
--

LOCK TABLES `Tbl_Results_Extern` WRITE;
/*!40000 ALTER TABLE `Tbl_Results_Extern` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Results_Extern` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_RgtBotle`
--

DROP TABLE IF EXISTS `Tbl_RgtBotle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_RgtBotle` (
  `RBtl_ID` mediumint(9) NOT NULL,
  `Test_ID` smallint(6) NOT NULL,
  `RLot_ID` mediumint(9) DEFAULT NULL,
  `Rgt_Type` tinyint(4) NOT NULL DEFAULT '1',
  `Btl_No` tinyint(4) NOT NULL DEFAULT '1',
  `Btl_Type` tinyint(4) NOT NULL,
  `Btl_Size` float NOT NULL,
  `Volume` float NOT NULL,
  `Test_Cnt` smallint(6) NOT NULL DEFAULT '0',
  `Rgt_blank` float NOT NULL DEFAULT '0',
  `OpenWhileStability` smallint(6) NOT NULL,
  `StabilityEnd` date NOT NULL,
  `Barcode` varchar(20) DEFAULT NULL,
  `IsExhausted` tinyint(1) NOT NULL DEFAULT '0',
  `IsActive` tinyint(1) NOT NULL,
  PRIMARY KEY (`RBtl_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_RgtBotle`
--

LOCK TABLES `Tbl_RgtBotle` WRITE;
/*!40000 ALTER TABLE `Tbl_RgtBotle` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_RgtBotle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_RgtLot`
--

DROP TABLE IF EXISTS `Tbl_RgtLot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_RgtLot` (
  `RLot_ID` mediumint(9) NOT NULL,
  `ConsumableType` tinyint(4) NOT NULL DEFAULT '1',
  `Test_ID` smallint(6) NOT NULL,
  `LotNo` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `MFG_ID` tinyint(4) NOT NULL,
  `MFG_Month` tinyint(4) NOT NULL,
  `MFG_Year` smallint(6) NOT NULL,
  `EXP_Month` tinyint(4) NOT NULL,
  `EXP_Year` smallint(6) NOT NULL,
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  PRIMARY KEY (`RLot_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_RgtLot`
--

LOCK TABLES `Tbl_RgtLot` WRITE;
/*!40000 ALTER TABLE `Tbl_RgtLot` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_RgtLot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_RgtPos`
--

DROP TABLE IF EXISTS `Tbl_RgtPos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_RgtPos` (
  `RBtl_ID` mediumint(9) DEFAULT NULL,
  `Tray_ID` tinyint(4) NOT NULL DEFAULT '1',
  `Tray_Pos` tinyint(4) NOT NULL,
  `Test_ID` smallint(6) NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_RgtPos`
--

LOCK TABLES `Tbl_RgtPos` WRITE;
/*!40000 ALTER TABLE `Tbl_RgtPos` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_RgtPos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Sample`
--

DROP TABLE IF EXISTS `Tbl_Sample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Sample` (
  `Sample_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Patient_ID` mediumint(9) NOT NULL,
  `SMP_Barcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SMP_Type` tinyint(4) NOT NULL,
  `CollectionDate` datetime DEFAULT NULL,
  `RegistrationDate` datetime NOT NULL,
  `SMP_Remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Ref_Dr` smallint(6) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  PRIMARY KEY (`Sample_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Sample`
--

LOCK TABLES `Tbl_Sample` WRITE;
/*!40000 ALTER TABLE `Tbl_Sample` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Sample` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Schedule`
--

DROP TABLE IF EXISTS `Tbl_Schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Schedule` (
  `Schedule_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SeqID` int(11) NOT NULL,
  `Position` tinyint(4) NOT NULL,
  `Patient_ID` int(11) NOT NULL,
  `Sample_ID` int(11) NOT NULL,
  `PrevCalID` smallint(6) NOT NULL,
  `Container` tinyint(4) NOT NULL,
  `ScheduleType` tinyint(4) NOT NULL,
  `SchStatus` tinyint(4) NOT NULL,
  `SchTime` datetime NOT NULL,
  `DiskNo` tinyint(4) NOT NULL,
  PRIMARY KEY (`Schedule_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Schedule`
--

LOCK TABLES `Tbl_Schedule` WRITE;
/*!40000 ALTER TABLE `Tbl_Schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_ScheduleDet`
--

DROP TABLE IF EXISTS `Tbl_ScheduleDet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_ScheduleDet` (
  `ScheduleDet_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Schedule_ID` mediumint(9) NOT NULL,
  `ScheduleType` tinyint(4) NOT NULL,
  `Test_ID` smallint(6) NOT NULL,
  `ReplicateNo` tinyint(4) NOT NULL,
  `DilRatio` tinyint(4) NOT NULL,
  `Lot_ID` mediumint(9) NOT NULL,
  `RunStatus` tinyint(4) NOT NULL,
  `TestType` tinyint(4) NOT NULL,
  `Position` tinyint(4) NOT NULL,
  PRIMARY KEY (`ScheduleDet_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_ScheduleDet`
--

LOCK TABLES `Tbl_ScheduleDet` WRITE;
/*!40000 ALTER TABLE `Tbl_ScheduleDet` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_ScheduleDet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Test`
--

DROP TABLE IF EXISTS `Tbl_Test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Test` (
  `Test_ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `Unique_Test_ID` smallint(6) NOT NULL COMMENT 'Uniqe Id given by Company for every test parameter for upload/download Testdeatils/standards/Reagents\n',
  `TestCode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TestName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DefaultTest` tinyint(1) DEFAULT '0' COMMENT '1 IF IT IS CLOSE-PARAMETER 0- IF OPEN PARAMETER',
  `W1` tinyint(4) NOT NULL,
  `W2` tinyint(4) NOT NULL,
  `CurveType` tinyint(4) NOT NULL,
  `Method` tinyint(4) NOT NULL,
  `Direction` tinyint(4) NOT NULL COMMENT 'Increasing/Decreasing',
  `Reagents` tinyint(1) NOT NULL COMMENT 'Total No of Reagents (1/2/3)',
  `Decimals` tinyint(4) NOT NULL COMMENT 'for Result Display only',
  `Std_Cnt` tinyint(4) NOT NULL COMMENT 'No of Standards used for new calibration\n',
  `Unit` tinyint(4) NOT NULL,
  `DelayedR2` tinyint(1) NOT NULL,
  `ReadTime1_S` tinyint(3) NOT NULL,
  `ReadTime1_E` tinyint(3) NOT NULL,
  `ReadTime2_S` tinyint(3) NOT NULL,
  `ReadTime2_E` tinyint(3) NOT NULL,
  `RgtStability` smallint(6) NOT NULL,
  `Blanking_Type` tinyint(1) NOT NULL,
  `A` float NOT NULL,
  `B` float NOT NULL,
  `CalReq` tinyint(1) NOT NULL,
  `R1StrSpeed` tinyint(4) NOT NULL,
  `R2StrSpeed` tinyint(4) NOT NULL,
  `IsVisible` tinyint(4) NOT NULL DEFAULT '1',
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  PRIMARY KEY (`Test_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Test`
--

LOCK TABLES `Tbl_Test` WRITE;
/*!40000 ALTER TABLE `Tbl_Test` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Test_Calcu`
--

DROP TABLE IF EXISTS `Tbl_Test_Calcu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Test_Calcu` (
  `Cal_Test_ID` tinyint(4) NOT NULL,
  `Cal_TestCode` varchar(10) DEFAULT NULL,
  `Cal_TestName` varchar(20) DEFAULT NULL,
  `Cal_Expression` varchar(45) NOT NULL,
  `Decimals` tinyint(4) DEFAULT NULL,
  `Ref_Low` float DEFAULT NULL,
  `Ref_High` float DEFAULT NULL,
  `Unit` tinyint(4) DEFAULT NULL,
  `SendToHost` tinyint(4) NOT NULL DEFAULT '1',
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Cal_Test_ID`),
  UNIQUE KEY `Cal_TestCode_UNIQUE` (`Cal_TestCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Test_Calcu`
--

LOCK TABLES `Tbl_Test_Calcu` WRITE;
/*!40000 ALTER TABLE `Tbl_Test_Calcu` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Test_Calcu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Test_Calib`
--

DROP TABLE IF EXISTS `Tbl_Test_Calib`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Test_Calib` (
  `Test_ID` smallint(6) NOT NULL,
  `IsBlank` tinyint(4) NOT NULL,
  `Pos` int(11) NOT NULL,
  `Conc` float NOT NULL,
  `OD` float NOT NULL,
  `Lot` int(11) NOT NULL,
  `DilRatio` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Test_Calib`
--

LOCK TABLES `Tbl_Test_Calib` WRITE;
/*!40000 ALTER TABLE `Tbl_Test_Calib` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Test_Calib` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Test_Carry_Over`
--

DROP TABLE IF EXISTS `Tbl_Test_Carry_Over`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Test_Carry_Over` (
  `IDPK` smallint(6) NOT NULL AUTO_INCREMENT,
  `Test_ID` smallint(6) NOT NULL,
  `Afected_Test_ID` smallint(6) NOT NULL,
  `Action` tinyint(4) NOT NULL,
  `WashType` tinyint(4) NOT NULL,
  `WashVolume` smallint(6) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  PRIMARY KEY (`IDPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Test_Carry_Over`
--

LOCK TABLES `Tbl_Test_Carry_Over` WRITE;
/*!40000 ALTER TABLE `Tbl_Test_Carry_Over` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Test_Carry_Over` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Test_Details`
--

DROP TABLE IF EXISTS `Tbl_Test_Details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Test_Details` (
  `Test_ID` smallint(6) NOT NULL,
  `PrintName` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'will be TestName by default\n',
  `LIS_Name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'will be TestCode by default\n',
  `RunIndex` smallint(6) NOT NULL DEFAULT '0',
  `PrintIndex` smallint(6) NOT NULL DEFAULT '0',
  `DispIndex` smallint(6) NOT NULL DEFAULT '0',
  `Auto_Dil` tinyint(4) NOT NULL DEFAULT '1',
  `SendToHost` tinyint(4) NOT NULL DEFAULT '1',
  `Replicate_Smp` tinyint(4) NOT NULL DEFAULT '2',
  `Replicate_Std` tinyint(4) NOT NULL DEFAULT '2',
  `Replicate_Cntrl` tinyint(4) NOT NULL DEFAULT '2',
  `CalLot` int(11) NOT NULL DEFAULT '0',
  `ControlInterval` tinyint(2) NOT NULL DEFAULT '0',
  `OnlineCalib` tinyint(1) NOT NULL DEFAULT '0',
  `CuvWashB` tinyint(4) NOT NULL DEFAULT '0',
  `CuvWashA` tinyint(4) NOT NULL DEFAULT '0',
  `SpecialDil` tinyint(1) NOT NULL DEFAULT '0',
  `CalBlanking` tinyint(4) NOT NULL DEFAULT '0',
  `Cost` float NOT NULL DEFAULT '0',
  `CalVarFactor` float NOT NULL,
  `RB_LOW` float NOT NULL DEFAULT '0',
  `RB_HIGH` float NOT NULL DEFAULT '0',
  `Abs_Limit_Low` float NOT NULL DEFAULT '0',
  `Abs_Limit_High` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Test_Details`
--

LOCK TABLES `Tbl_Test_Details` WRITE;
/*!40000 ALTER TABLE `Tbl_Test_Details` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Test_Details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Test_Extern`
--

DROP TABLE IF EXISTS `Tbl_Test_Extern`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Test_Extern` (
  `Extern_TestID` smallint(6) NOT NULL,
  `Test_Code` varchar(20) DEFAULT NULL,
  `Test_Name` varchar(45) DEFAULT NULL,
  `Result_Type` tinyint(4) DEFAULT NULL COMMENT '1. Numeric(Quantitative)\n2. chars(Qualitative)',
  `Decimals` tinyint(4) DEFAULT '3',
  `Unit` smallint(6) DEFAULT NULL,
  `RefLow` float DEFAULT NULL,
  `RefHigh` float DEFAULT NULL,
  PRIMARY KEY (`Extern_TestID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Test_Extern`
--

LOCK TABLES `Tbl_Test_Extern` WRITE;
/*!40000 ALTER TABLE `Tbl_Test_Extern` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Test_Extern` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `Tbl_System_Settings`
--

DROP TABLE IF EXISTS `Tbl_System_Settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_System_Settings` (
  `System_SettingID` smallint(6) NOT NULL,
  `InstrumentPort` varchar(20) DEFAULT NULL,
  `InstrumentBaudRate` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`System_SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_System_Settings`
--

LOCK TABLES `Tbl_System_Settings` WRITE;
/*!40000 ALTER TABLE `Tbl_System_Settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_System_Settings` ENABLE KEYS */;
UNLOCK TABLES;
--
-- Table structure for table `Tbl_Test_Profile`
--

DROP TABLE IF EXISTS `Tbl_Test_Profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Test_Profile` (
  `Profile_ID` tinyint(4) NOT NULL AUTO_INCREMENT,
  `ProfileName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ProfileCode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  PRIMARY KEY (`Profile_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Test_Profile`
--

LOCK TABLES `Tbl_Test_Profile` WRITE;
/*!40000 ALTER TABLE `Tbl_Test_Profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Test_Profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Test_Reflex`
--

DROP TABLE IF EXISTS `Tbl_Test_Reflex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Test_Reflex` (
  `Reflex_ID` tinyint(4) NOT NULL AUTO_INCREMENT,
  `Test_ID` smallint(6) NOT NULL,
  `Reflex_Type` tinyint(4) NOT NULL,
  `LowValue` float NOT NULL,
  `HighValue` float NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  PRIMARY KEY (`Reflex_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Test_Reflex`
--

LOCK TABLES `Tbl_Test_Reflex` WRITE;
/*!40000 ALTER TABLE `Tbl_Test_Reflex` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Test_Reflex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_Test_Volumes`
--

DROP TABLE IF EXISTS `Tbl_Test_Volumes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Test_Volumes` (
  `Test_ID` smallint(6) NOT NULL,
  `SVol_N` float NOT NULL DEFAULT '2',
  `SVol_I` float NOT NULL DEFAULT '2',
  `SVol_D` float NOT NULL DEFAULT '2',
  `StdVol` tinyint(4) NOT NULL DEFAULT '2',
  `DilRatio_N` tinyint(4) NOT NULL DEFAULT '1',
  `DilRatio_I` tinyint(4) NOT NULL DEFAULT '1',
  `DilRatio_D` tinyint(4) NOT NULL DEFAULT '1',
  `R1Vol` float NOT NULL DEFAULT '150',
  `R2Vol` float NOT NULL DEFAULT '50',
  `R3Vol` float NOT NULL DEFAULT '50'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Test_Volumes`
--

LOCK TABLES `Tbl_Test_Volumes` WRITE;
/*!40000 ALTER TABLE `Tbl_Test_Volumes` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Test_Volumes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_User`
--

DROP TABLE IF EXISTS `Tbl_User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_User` (
  `UID_PK` tinyint(4) NOT NULL AUTO_INCREMENT,
  `U_LoginID` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `U_Pwd` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `U_PwdHint` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `U_Type` tinyint(4) NOT NULL,
  `Person_ID` smallint(6) NOT NULL,
  `C_UID` tinyint(4) NOT NULL,
  `LastMod_UID` tinyint(4) NOT NULL,
  `D_UID` tinyint(4) NOT NULL,
  PRIMARY KEY (`UID_PK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_User`
--

LOCK TABLES `Tbl_User` WRITE;
/*!40000 ALTER TABLE `Tbl_User` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_UserRights`
--

DROP TABLE IF EXISTS `Tbl_UserRights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_UserRights` (
  `SEQID` smallint(6) NOT NULL,
  `UserID` tinyint(4) NOT NULL,
  `FormID` tinyint(4) NOT NULL,
  `FunctionID` tinyint(4) NOT NULL,
  `IsAllowed` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_UserRights`
--

LOCK TABLES `Tbl_UserRights` WRITE;
/*!40000 ALTER TABLE `Tbl_UserRights` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_UserRights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tbl_WorkList`
--

DROP TABLE IF EXISTS `Tbl_WorkList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_WorkList` (
  `SeqNo` smallint(6) NOT NULL AUTO_INCREMENT,
  `Cuv` tinyint(4) NOT NULL,
  `Schedule_ID` int(11) NOT NULL,
  `ReplicateNo` tinyint(4) NOT NULL,
  `Test_ID` smallint(6) NOT NULL,
  `ScheduleType` tinyint(4) NOT NULL,
  `RunTestType` tinyint(4) NOT NULL,
  `DilRatio` tinyint(4) NOT NULL,
  `Lot_ID` mediumint(9) DEFAULT NULL,
  `Smp_Pos` tinyint(4) NOT NULL,
  `R1Pos` tinyint(4) NOT NULL,
  `R2Pos` tinyint(4) NOT NULL,
  `R3Pos` tinyint(4) NOT NULL,
  `SampleVol` tinyint(4) NOT NULL,
  `R1Vol` smallint(6) NOT NULL,
  `R2Vol` smallint(6) NOT NULL,
  `R3Vol` smallint(6) NOT NULL,
  `DilPos` int(11) NOT NULL,
  `WashPos` int(11) NOT NULL,
  `R1Short` tinyint(4) NOT NULL,
  `R2Short` tinyint(4) NOT NULL,
  `R3Short` tinyint(4) NOT NULL,
  `SMP_Short` tinyint(4) NOT NULL,
  `RunStatus` tinyint(4) NOT NULL,
  `OD` float NOT NULL,
  `ResultValue` float NOT NULL,
  `Result` decimal(10,0) NOT NULL,
  `Flag` varchar(30) DEFAULT NULL,
  `R1Time` datetime NOT NULL,
  `R2Time` datetime NOT NULL,
  `R3Time` datetime NOT NULL,
  `SmpTime` datetime NOT NULL,
  `ResultID` int(11) NOT NULL,
  `LisStatus` tinyint(4) NOT NULL,
  `DilCuv` tinyint(4) NOT NULL,
  `DilSeqNo` tinyint(4) NOT NULL,
  PRIMARY KEY (`SeqNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_WorkList`
--

LOCK TABLES `Tbl_WorkList` WRITE;
/*!40000 ALTER TABLE `Tbl_WorkList` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_WorkList` ENABLE KEYS */;
UNLOCK TABLES;




--
-- Table structure for table `Tbl_Instrument`
--

DROP TABLE IF EXISTS `Tbl_Instrument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tbl_Instrument` (
  `Instrument_ID` int(11) NOT NULL,
  `Instrument_Name` varchar(20) NOT NULL,
  `Cur_Instrument` tinyint(4) NOT NULL DEFAULT '0',
  `Rgt_Positions` tinyint(4) NOT NULL,
  `Smp_positions` tinyint(4) NOT NULL COMMENT 'no of sample positions',
  `Cuv_Position` tinyint(4) NOT NULL,
  `Cycle_Time` tinyint(4) NOT NULL,
  `ESW_Version` tinyint(4) NOT NULL,
  `ASW_Version` tinyint(4) NOT NULL,
  `ISE_MODULE` tinyint(4) NOT NULL,
  `Max_Read_Cycles` tinyint(4) NOT NULL COMMENT 'no of overall cycles before wash',
  `Filter_Cnt` tinyint(4) NOT NULL COMMENT 'avalible number of filters',
  `Instrument_Serial_no` varchar(15) DEFAULT NULL,
  `Init_Photo_CPos` tinyint(4) NOT NULL,
  `Init_Dryer_CPos` tinyint(4) DEFAULT NULL,
  `Init_R1_CPos` tinyint(4) NOT NULL,
  `Init_R2_CPos` tinyint(4) NOT NULL,
  `Init_R3_CPos` tinyint(4) NOT NULL,
  `Init_Smp_CPos` tinyint(4) NOT NULL,
  `Init_Str1_CPos` tinyint(4) NOT NULL,
  `Init_Str2_CPos` tinyint(4) NOT NULL,
  `Init_Str3_CPos` tinyint(4) NOT NULL,
  `Smp_Out_Pos_S` tinyint(4) NOT NULL,
  `Smp_Mid_Pos_S` tinyint(4) NOT NULL,
  `Smp_Inr_Pos_S` tinyint(4) NOT NULL,
  `Smp_Out_Pos_E` tinyint(4) NOT NULL,
  `Smp_Mid_Pos_E` tinyint(4) NOT NULL,
  `Smp_Inr_Pos_E` tinyint(4) NOT NULL,
  PRIMARY KEY (`Instrument_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tbl_Instrument`
--

LOCK TABLES `Tbl_Instrument` WRITE;
/*!40000 ALTER TABLE `Tbl_Instrument` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tbl_Instrument` ENABLE KEYS */;
UNLOCK TABLES;









/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-16 12:33:01