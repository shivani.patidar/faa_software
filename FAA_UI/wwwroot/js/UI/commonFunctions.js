/* following function added by jayashri - datepicker date format as dd-mmm-yyyy*/
$("input[type='date']").on("change", function () {
    var date = moment(this.value, "YYYY-MM-DD").format(this.getAttribute("data-date-format"));
    if (date == "Invalid date") {
        date = "dd-mmm-yyyy";
    }
    this.setAttribute("data-date", date)}).trigger("change")

