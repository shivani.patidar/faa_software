﻿let currentElementP = "";
//let list = document.getElementById("list");
let initialXP = 0,
    initialYP = 0;

const isTouchDeviceP = () => {
    try {
        //We try to create TouchEvent (it would fail for desktops and throw error)
        document.createEvent("TouchEvent");
        return true;
    } catch (e) {
        return false;
    }
};

//Create List Items
const creatorP = (count) => {
    // for (let i = 1; i <= count; i++) {
    // list.innerHTML += `<li class="list-item" data-value ="${i}">Item-${i} </li>`;
    // }
};

//Returns element index with given value
const getPositionP = (value) => {
    let elementIndexP;
    let listItemsP = document.querySelectorAll(".printSortable");
    listItemsP.forEach((element, index) => {
        let elementValueP = element.getAttribute("data-value");
        if (value == elementValueP) {
            elementIndexP = index;
        }
    });
    return elementIndexP;
};

//Drag and dropP functions
function dragstartP(e) {
    //initialXP = 0;
    //initialYP = 0;
    initialXP = isTouchDeviceP() ? e.touches[0].clientX : e.clientX;
    initialYP = isTouchDeviceP() ? e.touches[0].clientY : e.clientY;
    //Set current Element
    currentElementP = e.target;
}
function dragoverP(e) {
    e.preventDefault();
}

const dropP = (e) => {
    e.preventDefault();
    let newXP = isTouchDeviceP() ? e.touches[0].clientX : e.clientX;
    let newYP = isTouchDeviceP() ? e.touches[0].clientY : e.clientY;

    //Set targetElementP(where we dropP the picked element).It is based on mouse position
    let targetElementP = document.elementFromPoint(newXP, newYP);
    let currentValueP = currentElementP.getAttribute("data-value");
    let targetValueP = targetElementP.getAttribute("data-value");
    //get index of current and target based on value
    let [currentPositionP, targetPositionP] = [
        getPositionP(currentValueP),
        getPositionP(targetValueP),
    ];
    initialXP = newXP;
    initialYP = newYP;

    try {
        //'afterend' inserts the element after the target element and 'beforebegin' inserts before the target element
        if (currentPositionP < targetPositionP) {
            targetElementP.insertAdjacentElement("afterend", currentElementP);
        } else {
            targetElementP.insertAdjacentElement("beforebegin", currentElementP);
        }
    } catch (err) { }
};

window.onload = async () => {
    // customElement = "";
    // list.innerHTML = "";
    //This creates 5 elements
    // await creatorP(5);

    let listItemsP = document.querySelectorAll(".printSortable");
    listItemsP.forEach((element) => {
        element.draggable = true;
        element.addEventListener("dragstartP", dragstartP, false);
        element.addEventListener("dragoverP", dragoverP, false);
        element.addEventListener("dropP", dropP, false);
        element.addEventListener("touchstartP", dragstartP, false);
        element.addEventListener("touchstartP", dropP, false);
    });
};
