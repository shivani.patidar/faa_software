﻿var SampleContainer = "";

$(document).ready(function () {
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })

    $('.editable-select').editableSelect();

    RgntGenerateHtml();
    /*    RgntGenerateHtml2();*/
    //smpGenerateHtml();
    //smpGenerateHtml1();
    //smpGenerateHtml2();

    GetCalibrationQCMst();

    setTimeout(function () {
        GetAllSampleTray();
        GetAllReagentTray();
    }, 3000);

    GetAllSample();
    GetAllCalibration();
    GetAllQC();

    GetAllPatientMaster();


    // GetMonthYear();

    //GetAllReagentMaster(); // reagent master combos

    BindTableWorkList();
    /*    CalNewRow();*/
    /*   QcNewRow();*/

    GetTestCode();
    GetTestProfile();
    GetListLot();
    /*   BindTableCalibration();*/
    /*    BindTableQC()*/
    $("#ddlRgtTestCode").select2({
        dropdownParent: $("#modelReagentNewLotNo"),
        width: "100%"
    });

    $('.circle div').on('click', function (e) {
        var accesskey = $(this).attr("accesskey");
        if (typeof (accesskey) == "undefined") {
            accesskey = "";
        }
        setSamplePosition(e, $(this).text(), accesskey, $(this).attr("id"));
    });


    $('.divRgtcircle').on('click', function (e) {
        var accesskey = $(this).attr("accesskey");
        if (typeof (accesskey) == "undefined") {
            accesskey = "";
        }
        setReagentPosition(e, $(this).text(), accesskey, $(this).attr("id"));
    });


    /* Tab 4 Start Image Cordinate Online*/
    //var App = App || {};
    //App.points = [];


    //jQuery(function ($) {

    //    const $radius = $(".Imgradius");

    //    $('#Imgimage_preview').on('click', function (ev) {

    //        const $this = $(this),
    //            r = parseInt($radius.val().trim(), 10),
    //            o = $this.offset(),
    //            y = ev.pageY - o.top,
    //            x = ev.pageX - o.left;

    //        $("<div />", {
    //            "class": "Imgcircle",
    //            css: {
    //                top: y,
    //                left: x,
    //                width: 29,
    //                height: 29,
    //            },
    //            appendTo: $this
    //        });

    //        App.points.push({ x, y, r });
    //        // Test
    //        console.log(App.points)

    //    });

    //});
    /* Tab 4 End Image Cordinate Online*/

    //$("#txtPatientId").keyup($.debounce(5000, function (e) {
    //    var PatientId = $("#txtPatientId").val();
    //    if (PatientId.length >= 3) {
    //        SearchPatientById()
    //    }
    //}))
    //$("#txtPatientId").keydown(function () {
    //    $("#txtPatientId").autocomplete({
    //        source: function (request, response) {
    //            $.ajax({
    //                url: '/Schedule/SearchPatientById?term=' + request.term,
    //                method: "post",
    //                contentType: "application/json;charset=utf-8",
    //               /* data: JSON.stringify({ term: request.term }),*/
    //                //data: { PatientId: request.term },
    //                dataType: 'json',
    //                success: function (data) {
    //                    response($.map(data, function (item) {
    //                        return item;
    //                    }))
    //                },
    //                error: function (response) {
    //                    alert(response.responseText);
    //                },
    //                failure: function (response) {
    //                    alert(response.responseText);
    //                }
    //            });
    //        },
    //        select: function (e, ui) {
    //          /*  $("#BpId_" + count).val(ui.item.glacid);*/
    //        },
    //        minLength: 2,
    //    });

    //});


    $("#txtPatientId").keyup(function (e) {
        var PatientId = $("#txtPatientId").val();
        if ($("#txtPatientId").val().length == 2) {
            SearchPatientById();
        }
    });
    $("#txtPatName").keyup(function (e) {
        if ($("#txtPatName").val().length == 2) {
            SearchPatientByName();
        }
    });
    $("#txtSmpBarCode").keypress(function (e) {
        if ($("#txtSmpBarCode").val().length == 2) {
            SearchSampleBySmpBarCode();
        }
    });
});



function getAge() {
    var dateString = $("#dtDOB").val();
    if (dateString != "") {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();
    var dob = new Date(dateString)
 
    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " Years";
    else yearString = " Year";
    if (age.months > 1) monthString = " Months";
    else monthString = " Month";
    if (age.days > 1) dayString = " Days";
    else dayString = " Day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + yearString + " " + age.months + monthString +" "+  age.days + dayString + " ";
    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = "" + age.days + dayString + " ";
    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + yearString + "";
    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + yearString + " " + age.months + monthString + "";
    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.months + monthString + " " + age.days + dayString + "";
    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + yearString + " " + age.days + dayString + "";
    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.months + monthString;
    else ageString = "Oops! Could not calculate age!";
    $("#txtSmpAge").val(ageString);
}
}




function GetPatientDetail() {
    UpdateShowPatient($("#divPatientId .es-list .es-visible").val());
}

function GetPatientNameDetail() {
    UpdateShowPatient($("#divPatientName .es-list .es-visible").val());
}

function GetSampleDetailbySmpBarCode() {
    UpdateShowSample($("#divSmpBarCode .es-list .es-visible").val());
}

function SearchPatientById() {
        $("#preloader").css("display", "block");

        $.ajax({
            method: "GET",
            url: '/Schedule/SearchPatientById?PatientId=' + $("#txtPatientId").val(),
            success: function (result) {
                if (result != null) {
                    $("#divPatientId .es-list").empty();
                    var PatientIdList = '';
                    for (var i = 0; i < result.length; i++) {
                        PatientIdList += '<li class="es-visible" value="' + result[i].patId + '">' + result[i].patientId + '</li>';
                    }
                    $("#divPatientId .es-list").append(PatientIdList);
                    $('#divPatientId .editable-select').editableSelect();
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    
}

function SearchPatientByName() {
 
        $("#preloader").css("display", "block");
        $.ajax({
            method: "GET",
            url: '/Schedule/SearchPatientByName?PatientName=' + $("#txtPatName").val(),
            success: function (result) {
                if (result != null) {
                    $("#divPatientName .es-list").empty();
                    var PatientIdList = '';
                    for (var i = 0; i < result.length; i++) {
                        PatientIdList += '<li class="es-visible" value="' + result[i].patId + '">' + result[i].patName + '</li>';
                    }
                    $("#divPatientName .es-list").append(PatientIdList);
                    $('#divPatientName .editable-select').editableSelect();
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    
}

function SearchSampleBySmpBarCode() {
        $("#preloader").css("display", "block");

        $.ajax({
            method: "GET",
            url: '/Schedule/SearchSampleBySmpBarCode?SmpBarCode=' + $("#txtSmpBarCode").val(),
            success: function (result) {
                if (result != null) {
                    $("#divSmpBarCode .es-list").empty();
                    var SmpBarcodeList = '';
                    for (var i = 0; i < result.length; i++) {
                        SmpBarcodeList += '<li class="es-visible" value="' + result[i].smpId + '">' + result[i].smpBarcode + '</li>';
                    }
                    $("#divSmpBarCode .es-list").append(SmpBarcodeList);
                    $('#divSmpBarCode .editable-select').editableSelect();
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    
}
function setReagentPosition(e, Positions, accesskey, PkId) {
    var bodyOffsets = document.body.getBoundingClientRect();
    tempX = e.pageX - bodyOffsets.left;
    tempY = e.pageY;
    if (accesskey == "") {
        $("#RgtTrayCustomMenu").empty();
        $("#RgtTrayCustomMenu").append('<li><a onclick="csmReagentMenuClick(\'\',\'' + Positions + '\',\'' + PkId + '\')" class="CustomMenu" style="color: #656FEA;" href="#" onclick="" data-bs-toggle="modal" data-bs-target="#modelReagent"><button data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Add New Reagent"><b><i class="bi bi-plus"></i></b> Reagent</button></a></li>');
    }
    else if (accesskey == "Rgt") {
        $("#RgtTrayCustomMenu").empty();
        $("#RgtTrayCustomMenu").append('<li><a class="CustomMenu" href="#" class="" data-bs-toggle="modal" data-bs-target="#modelReagent" onclick="csmReagentMenuClick(\'Rgt\',\'' + Positions + '\',\'' + PkId + '\')" style="color:#656FEA"><button data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Edit Reagent"><b><i class="bi bi-pencil-fill"></i></b> Edit</button></a></li><li><a class="CustomMenu" href="#" onclick="DeleteConfirmationReagentTray(\'Rgt\',\'' + Positions + '\',\'' + PkId + '\')" style="color:red;"><button data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Reagent"><b><i class="bi bi-trash"></i></b> Delete</button></a></li>');
    }
    $("#SmpTrayCustomMenu").css("display", "none");
    $("#RgtTrayCustomMenu").css({ 'top': tempY, 'left': tempX, 'display': 'block' });
}
function setSamplePosition(e, Positions, accesskey, PkId) {
    var bodyOffsets = document.body.getBoundingClientRect();
    tempX = e.pageX - bodyOffsets.left;
    tempY = e.pageY;
    if (accesskey == "") {
        $("#SmpTrayCustomMenu").empty();
        $("#SmpTrayCustomMenu").append('<li><a class="CustomMenu" onclick="csmSampleMenuClick(\'\',\'' + Positions + '\',\'' + PkId + '\')" style="color: #656FEA;" href="#" data-bs-toggle="modal" data-bs-target="#modelSample"><b><i class="bi bi-plus"></i></b> Sample</a></li><li><a onclick="csmSampleMenuClick(\'\',\'' + Positions + '\',\'' + PkId + '\')" class="CustomMenu" href="#" data-bs-toggle="modal" data-bs-target="#modelQC" style="color: #656FEA;"><b><i class="bi bi-plus"></i></b> Quality Control</a></li><li><a onclick="csmSampleMenuClick(\'\',\'' + Positions + '\',\'' + PkId + '\')" class="CustomMenu" style="color: #656FEA;" href="#" data-bs-toggle="modal" data-bs-target="#modelCalibration"><b><i class="bi bi-plus"></i></b> Calibration</a></li>');

    }
    else if (accesskey == "Cal") {
        $("#SmpTrayCustomMenu").empty();
        $("#SmpTrayCustomMenu").append('<li><a class="CustomMenu" href="#" class="" onclick="csmSampleMenuClick(\'Cal\',\'' + Positions + '\',\'' + PkId + '\')"  data-bs-toggle="modal" data-bs-target="#modelCalibration" style="color:#656FEA"><b  data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Calibration"><i class="bi bi-pencil-fill"></i></b> Edit</a></li><li><a class="CustomMenu" href="#" onclick="DeleteConfirmationSampleTray(\'Cal\',\'' + Positions + '\',\'' + PkId + '\')" style="color:red;"><b><i class="bi bi-trash"></i></b> Delete</a></li>');

    }
    else if (accesskey == "QC") {
        $("#SmpTrayCustomMenu").empty();
        $("#SmpTrayCustomMenu").append('<li><a class="CustomMenu" href="#" onclick="csmSampleMenuClick(\'QC\',\'' + Positions + '\',\'' + PkId + '\')" class="" data-bs-toggle="modal" data-bs-target="#modelQC"  style="color:#656FEA"><b><i class="bi bi-pencil-fill"></i></b> Edit</a></li><li><a class="CustomMenu" href="#" onclick="DeleteConfirmationSampleTray(\'QC\',\'' + Positions + '\',\'' + PkId + '\')" style="color:red;"><b><i class="bi bi-trash"></i></b> Delete</a></li>');

    }
    else if (accesskey == "Sample") {
        $("#SmpTrayCustomMenu").empty();
        $("#SmpTrayCustomMenu").append('<li><a class="CustomMenu" href="#" class="" onclick="csmSampleMenuClick(\'Sample\',\'' + Positions + '\',\'' + PkId + '\')" style="color:#656FEA"><b><i class="bi bi-pencil-fill"></i></b> Edit</a></li><li><a class="CustomMenu" href="#" onclick="DeleteConfirmationSampleTray(\'Sample\',\'' + Positions + '\',\'' + PkId + '\')" style="color:red;"><b><i class="bi bi-trash"></i></b> Delete</a></li>');
    }
    $("#RgtTrayCustomMenu").css("display", "none");
    $("#SmpTrayCustomMenu").css({ 'top': tempY, 'left': tempX, 'display': 'block' });
}

function csmSampleMenuClick(Type, Positions, PkId) {
    $("#SmpTrayCustomMenu").css("display", "none");
    if (Type == "Cal") {
        $("#tblCalibration tr").each(function (index) {
            $($(this).closest('tr')).find('td').find('.ddlCalPosition1').css("background", "");
            $($(this).closest('tr')).find('td').find('.ddlCalPosition2').css("background", "");
            $($(this).closest('tr')).find('td').find('.ddlCalPosition3').css("background", "");
            $($(this).closest('tr')).find('td').find('.ddlCalPosition4').css("background", "");
            $($(this).closest('tr')).find('td').find('.ddlCalPosition5').css("background", "");
            $($(this).closest('tr')).find('td').find('.ddlCalPosition6').css("background", "");
            $($(this).closest('tr')).find('td').find('.ddlCalPosition7').css("background", "");
            $($(this).closest('tr')).find('td').find('.ddlCalPosition8').css("background", "");
            $($(this).closest('tr')).find('td').find('.ddlCalPosition9').css("background", "");
            $($(this).closest('tr')).find('td').find('.ddlCalPosition10').css("background", "");

            if ($($(this).closest('tr')).find('td').find('.ddlCalPosition1').val() == Positions) {
                $($(this).closest('tr')).find('td').find('.ddlCalPosition1').css("background", "#ffff007a");
            }
            else if ($($(this).closest('tr')).find('td').find('.ddlCalPosition2').val() == Positions) {
                $($(this).closest('tr')).find('td').find('.ddlCalPosition2').css("background", "#ffff007a");
            }
            else if ($($(this).closest('tr')).find('td').find('.ddlCalPosition3').val() == Positions) {
                $($(this).closest('tr')).find('td').find('.ddlCalPosition3').css("background", "#ffff007a");
            }
            else if ($($(this).closest('tr')).find('td').find('.ddlCalPosition4').val() == Positions) {
                $($(this).closest('tr')).find('td').find('.ddlCalPosition4').css("background", "#ffff007a");
            }
            else if ($($(this).closest('tr')).find('td').find('.ddlCalPosition5').val() == Positions) {
                $($(this).closest('tr')).find('td').find('.ddlCalPosition5').css("background", "#ffff007a");
            }
            else if ($($(this).closest('tr')).find('td').find('.ddlCalPosition6').val() == Positions) {
                $($(this).closest('tr')).find('td').find('.ddlCalPosition6').css("background", "#ffff007a");
            }
            else if ($($(this).closest('tr')).find('td').find('.ddlCalPosition7').val() == Positions) {
                $($(this).closest('tr')).find('td').find('.ddlCalPosition7').css("background", "#ffff007a");
            }
            else if ($($(this).closest('tr')).find('td').find('.ddlCalPosition8').val() == Positions) {
                $($(this).closest('tr')).find('td').find('.ddlCalPosition8').css("background", "#ffff007a");
            }
            else if ($($(this).closest('tr')).find('td').find('.ddlCalPosition9').val() == Positions) {
                $($(this).closest('tr')).find('td').find('.ddlCalPosition9').css("background", "#ffff007a");
            }
            else if ($($(this).closest('tr')).find('td').find('.ddlCalPosition10').val() == Positions) {
                $($(this).closest('tr')).find('td').find('.ddlCalPosition10').css("background", "#ffff007a");
            }

        })
    }
    else if (Type == "QC") {
        $("#tblQC tr").each(function () {
            $($(this).closest('tr')).find('td').find('.ddlQcPosition').css("background", "");
            if ($($(this).closest('tr')).find('td').find('.ddlQcPosition').val() == Positions) {
                $($(this).closest('tr')).find('td').find('.ddlQcPosition').css("background", "#19e719a3");

            }
        });
    }
    else if (Type == "Sample") {
        UpdateShowSample(PkId);
    }
}

function csmReagentMenuClick(Type, Positions, PkId) {
    $("#RgtTrayCustomMenu").css("display", "none");
    if (Type == "Rgt") {
        $("#tblReg tr").each(function () {
            $($(this).closest('tr')).find('td').find('.ddlRegPosition').css("background", "");
            if ($($(this).closest('tr')).find('td').find('.ddlRegPosition').val() == Positions) {
                if ($($(this).closest('tr')).find('td').find('.ddlReagentType').val() == "1") {
                    $($(this).closest('tr')).find('td').find('.ddlRegPosition').css("background", "skyblue");
                }
                else if ($($(this).closest('tr')).find('td').find('.ddlReagentType').val() == "2") {
                    $($(this).closest('tr')).find('td').find('.ddlRegPosition').css("background", "#eb6ce9");
                }
                //else {
                //    $($(this).closest('tr')).find('td').find('.ddlRegPosition').css("background", "#0ee20e4d");

                //}

            }
        });
    }
}

function DeleteConfirmationSampleTray(Type, Positions, PkId) {

    $("#SmpTrayCustomMenu").css("display", "none");
    if (Type == "Cal") {
        $("#lbldltSmpTrayTitle").text("Calibration Confirmation");
    }
    else if (Type == "QC") {
        $("#lbldltSmpTrayTitle").text("Quality Control Confirmation");
    }
    else if (Type == "Sample") {
        $("#lbldltSmpTrayTitle").text("Sample Confirmation");
    }
    var Detail = $("#SmpTray_" + Positions).attr("data-bs-original-title");
    var strArray = Detail.split(',');
    var data = "";
    for (var i = 0; i < strArray.length; i++) {
        data += strArray[i] + "</br>";
    }
    $("#hdDeleteSmpTrayType").val(Type);
    $("#hdDeleteSmpTrayPosition").val(Positions);
    $("#lblDeleteSmpTrayMsg").html(data);
    $("#modelDeleteConfirmationSampleTray").modal("show");
}

function DeleteSampleTray() {
    $("#preloader").css("display", "block");

    var Type = $("#hdDeleteSmpTrayType").val();
    var Positions = $("#hdDeleteSmpTrayPosition").val();
    $("#modelDeleteConfirmationSampleTray").modal('hide');
    $("#hdDeleteSmpTrayPosition").val('');
    if (Positions > 0) {
        $.ajax({
            method: "GET",
            url: '/Schedule/DeleteSampleTray',
            data: { Type: Type, Positions: Positions },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    //    window.location.reload(true);
                    ClearSampleTray()
                    BindTableWorkList();
               
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function DeleteConfirmationReagentTray(Type, Positions, PkId) {
    $("#RgtTrayCustomMenu").css("display", "none");
    if (Type == "Rgt") {
        $("#lbldltRgtTrayTitle").text("Reagent Confirmation");
    }
    var Detail = "";
    if (Positions >= 41) {
        Detail = $("#RgtTray_" + Positions).attr("data-bs-original-title");
    }
    else {
        Detail = $("#RgtTray_" + Positions + " div div").attr("data-bs-original-title");
    }

    var strArray = Detail.split(',');
    var data = "";
    for (var i = 0; i < strArray.length; i++) {
        data += strArray[i] + "</br>";
    }
    $("#hdDeleteRgtTrayType").val(Type);
    $("#hdDeleteRgtTrayPosition").val(Positions);
    $("#lblDeleteRgtTrayMsg").html(data);
    $("#modelDeleteConfirmationReagentTray").modal("show");
}

function DeleteReagentTray() {
    $("#preloader").css("display", "block");
    var Type = $("#hdDeleteRgtTrayType").val();
    var Positions = $("#hdDeleteRgtTrayPosition").val();
    $("#modelDeleteConfirmationReagentTray").modal('hide');
    $("#hdDeleteRgtTrayPosition").val('');
    if (Positions > 0) {
        $.ajax({
            method: "GET",
            url: '/Schedule/DeleteReagentTray',
            data: { Type: Type, Positions: Positions },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    ClearReagentTray()
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearSampleTray() {

    $(".SampleTrayDivaa .circle").each(function (index) {
        $("#SmpTray_" + (index + 1) + " div").css("background", "");
        $("#SmpTray_" + (index + 1)).attr("data-bs-original-title", (index + 1));
    });
    //GetAllSampleTray();
    GetAllSample();
    GetAllCalibration();
    GetAllQC();
}

/*Start Patient*/
function GetAllPatientMaster() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Schedule/GetPaitentMst',
        success: function (result) {
            if (result != null) {


                //Doctor 
                $("#ddlDoctorId").empty();
                var doctor = '<option value=""></option>';
                for (var i = 0; i < result.doctor.length; i++) {
                    doctor += '<option value="' + result.doctor[i].drId + '">' + result.doctor[i].drName + '</option>'
                }
                $("#ddlDoctorId").append(doctor);

                //Sample Type 
                $("#ddlSmpType").empty();
                var SmpType = '<option value=""></option>';
                for (var i = 0; i < result.sample.length; i++) {
                    SmpType += '<option value="' + result.sample[i].smpId + '">' + result.sample[i].smpType + '</option>'
                }
                $("#ddlSmpType").append(SmpType);

                //Sample Container Type 
                $("#ddlSmpConType").empty();
                SampleContainer = '<option value="">select sample container</option>';
                for (var i = 0; i < result.sampleCon.length; i++) {
                    SampleContainer += '<option value="' + result.sampleCon[i].smpConId + '">' + result.sampleCon[i].smpConName + '</option>'
                }
                $("#ddlSmpConType").append(SampleContainer);
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function BindTablePatient() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/Schedule/GetAllPatient",
        method: 'get',
        success: OnsuccessPatient
    });
}

function OnsuccessPatient(response) {
    $('#tblPatient').DataTable().destroy();
    $('#tblPatient').DataTable({
        bProcessing: true,
        bLenghtChange: true,
        retrieve: true,
        bfilter: true,
        bSort: true,
        bPaging: true,
        data: response,
        columns: [
            { data: 'dispName', title: 'Name' },

            {
                data: "patBarcode", title: 'Bar code',
                render: function (data, type, row, meta) {
                    return row.patBarcode
                }
            },


            //{
            //    data: "age", title: 'Age ',
            //    render: function (data, type, row, meta) {
            //        return row.age + " " + row.ageUnit
            //    }
            //},
            {
                data: "untId", title: 'Action',
                render: function (data, type, row, meta) {
                    return '<button type="button" class="" onclick ="UpdateShowPatient(' + row.patientId + ')" style="color:#656FEA;" > <b><i class="bi bi-pencil-fill"></i></b> </button> &nbsp;<button type="button" class="" onclick="DeleteConfirmationPatient(' + row.patientId + ')" style="color:red;"><b><i class="bi bi-trash"></i></b></button>';

                }
            },
        ],

    });
    $("#preloader").css("display", "none");

}

function SavePatient() {
    var PatId = 0;
    var returnValue = true;

    if ($("#txtPatName").val() == "") {
        SaveSample();
    }
    else {

        if ($("#txtPatientId").val() == "") {
            $("#txtPatientId").focus();
            Message("error", "Patient Id is required");
            returnValue = false;
        }
        else if ($("#txtPatName").val() == "") {
            $("#txtPatName").focus();
            Message("error", "Patient Name is required");
            returnValue = false;
        }
        else if ($("#dtDOB").val() == "") {
            $("#dtDOB").focus();
            Message("error", "Date of Birth is required");
            returnValue = false;
        }
        else if ($("#ddlGender").val() == "") {
            $("#ddlGender").focus();
            Message("error", "Gender is required");
            returnValue = false;
        }
        else {
            $("#preloader").css("display", "block");

            var obj = {
                PatId: $("#hdPatId").val(),
                patientId: $("#txtPatientId").val(),
                PatName: $("#txtPatName").val(),
                DOB: $("#dtDOB").val(),
                Height: $("#txtHeight").val(),
                Weight: $("#txtWeight").val(),
                sex: $("#ddlGender").val(),
                Address: $("#txtAddress").val(),

            }
            $.ajax({
                method: "POST",
                url: '/Schedule/SavePatient',
                data: obj,
                success: function (data) {

                    if (data.result) {
                        if (data.patId > 0) {
                            PatId = data.patId;
                            if ($("#hdPatId").val() > 0) {
                                Message("success", "Updated Data Successfully");

                            }
                            else {
                                Message("success", "Saved Data Successfully");
                            }
                            $("#hdPatId").val(PatId);

                            if ($("#txtSmpBarCode").val() == "") {
                                $("#modelSample").modal('hide');
                            }
                            else {
                                SaveSample();
                            }
                        }
                    }
                    else {
                        Message("error", "Somthing went wrong");
                    }
                    $("#preloader").css("display", "none");

                },
                error: function (e) {
                    Message("warning", "Somthing went wrong");
                    $("#preloader").css("display", "none");

                }
            });
        }
    }
    return PatId;
}

function UpdateShowPatient(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Schedule/GetByIdPatient',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {

            if (result.patient != null) {
                //$("#txtPatientId").val(result.patientId);
                //$("#txtBarCode").val(result.patBarcode);
                //$("#txtName").val(result.dispName);
                //$("#txtAge").val(result.age);
                //$("#ddlAgeUnit").val(result.ageUnit);
                //$("#ddlDepartmentId").val(result.departmentId);
                //$("#ddlSex").val(result.sex);
                //$("#dtCollectDt").val(result.collectDt);
                //$("#ddlDoctorId").val(result.doctorId);
                //$("#txtRemark").val(result.remark);

                //$("#btnSavePatient").html("Update");
                //$("#lblPatient").html("Update Patient");

                //$("#modelSample").modal('show');

                console.log(result)
                $("#hdPatId").val(result.patient.patId);
                $("#txtPatientId").val(result.patient.patientId);
                $("#txtPatName").val(result.patient.patName);
                if (result.patient.dob != null) {
                    $("#dtDOB").val(moment(result.patient.dob).format("YYYY-MM-DD"));
                    $("#dtDOB").change();
                }
                $("#txtHeight").val(result.patient.height);
                $("#txtWeight").val(result.patient.weight);
                $("#ddlGender").val(result.patient.sex);
                $("#txtAddress").val(result.patient.address);

                $("#txtSmpHeight").val(result.patient.height);
                $("#txtSmpWeight").val(result.patient.weight);
                $("#modelSample").modal('show');
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function DeleteConfirmationPatient(Id) {
    $("#modelDeleteConfirmationPatient").modal('show');
    $("#hdDeletePatientId").val(Id);
}

function DeletePatient() {
    $("#preloader").css("display", "block");
    var Id = $("#hdDeletePatientId").val();
    $("#modelDeleteConfirmationPatient").modal('hide');
    $("#hdDeletePatientId").val('');
    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/Schedule/DeletePatient',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTablePatient();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearPatient() {

    $("#hdPatId").val('');
    $("#txtPatientId").val('');
    $("#txtPatName").val('');

    $("#dtDOB").attr("data-date", '').trigger("change")
    //$("#dtDOB").val(format2);

    $("#txtHeight").val('');
    $("#txtWeight").val('');
    $("#ddlGender").val('');
    $("#txtAddress").val('');

    $("#btnSavePatient").html("Save");
    $("#lblPatient").html("New Patient");

    $("#modelSample").modal('hide');
}


/*End Patient*/

/*Start Sample*/

function GetTestCode() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/TestParameter/GetTestCode',
        success: function (result) {
            if (result != null) {
                //Test Code 
                $("#divTestCodeBind").empty();
                $("#ddlRgtTestCode").empty();

                var proTestCode = '';
                var rgtTestCode = '';
                for (var i = 0; i < result.testCode.length; i++) {
                    proTestCode += '<div class="col-lg-2 mb-2 btn_Color btn rounded-pill" style="width:145px" id="' + result.testCode[i].testId + '" onclick = "funBtnSelect(this)"><button type="button"  class=""  >' + result.testCode[i].testCode + ' </button ></div>';
                    rgtTestCode += '<option value="' + result.testCode[i].testId + '">' + result.testCode[i].testCode + '</option>';
                }

                $("#divTestCodeBind").append(proTestCode);
                $("#ddlRgtTestCode").append(rgtTestCode);
                $("#preloader").css("display", "none");

            }
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function GetTestProfile() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/TestParameter/GetAllTestProfile',
        success: function (result) {
            if (result != null) {

                //Test Profile 
                $("#divTestProfileBind").empty();


                var testProfile = '';
                for (var i = 0; i < result.length; i++) {
                    testProfile += '<div class="col-lg-2 mb-2 btn_Color btn rounded-pill" style="width:140px" id="' + result[i].profileId + '" onclick = "funTestProfileSelect(this)"><button type="button"  class="">' + result[i].profileName + ' </button ></div>';
                }

                $("#divTestProfileBind").append(testProfile);
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function funBtnSelect(e) {
    var Id = $(e).parent().prev("div").attr('id'); //50, 100, 151

    if (e.style.background == "" || e.style.background == "#707274" || e.style.background == "rgb(112, 114, 116)") {
        e.style.background = "#039b54" // "#039b54"; // Green
        $(e).attr("accesskey", "check");
    }
    else if (e.style.background == "#039b54" || e.style.background == "rgb(3, 155, 84)") { //"#039b54" //rgb(50, 100, 151)
        e.style.background = "#707274"; //Gray
        $(e).attr("accesskey", "");
    }
}

function funTestProfileSelect(e) {
    var ProfileId = $(e).attr('id');

    if (e.style.background == "" || e.style.background == "#707274" || e.style.background == "rgb(112, 114, 116)") { //"rgb(112, 114, 116)") {
        e.style.background = "#039b54" //"#039b54"; // Green
        $(e).attr("accesskey", "check");

    }
    else if (e.style.background == "#039b54" || e.style.background == "rgb(3, 155, 84)") { //"rgb(50, 100, 151)"
        e.style.background = "#858987"; //"#707274"; //Gray
        $(e).attr("accesskey", "");
        /* ClearSelectTestCode();*/
    }
    GetProfileTestCode(ProfileId, $(e).attr("accesskey"));
}

function GetProfileTestCode(ProfileId, Type) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Schedule/GetProfileTestCode?ProfileId=' + ProfileId,
        crossDomain: true,
        success: function (result) {

            if (result != null) {
                if (Type == "check") {
                    for (var i = 0; i < result.length; i++) {
                        $("#divTestCodeBind div").each(function () {
                            if (result[i].testId == $(this).attr("id")) {
                                $(this).attr("accesskey", "check");
                                this.style.background = "#039b54";
                            }
                        });
                    }
                }
                else {
                    for (var i = 0; i < result.length; i++) {
                        $("#divTestCodeBind div").each(function () {
                            if (result[i].testId == $(this).attr("id")) {
                                $(this).attr("accesskey", "");
                                this.style.background = "#707274";
                            }
                        });
                    }
                }
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function BindTableWorkList() {
    $("#preloader").css("display", "none");

    $.ajax({
        url: "/Schedule/GetAllWorkList",
        method: 'get',
        success: OnsuccessWorkList
    });
}

function OnsuccessWorkList(response) {
    $('#tblWorkList').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblWorkList").css("display", "block");
        $("#divWorkListNoRecordFound").empty();
        $('#tblWorkList').DataTable({
            bProcessing: true,
            bLenghtChange: true,
            retrieve: true,
            bfilter: true,
            bSort: true,
            bPaging: true,
            data: response,
            columns: [
                {
                    data: 'sampleId', title: 'Sample ID', width:'50%',
                    render: function (data, type, row, meta) {
                        if (row.type == "Cal") {
                            return '<div style="float: left;">Calibration</div><div style="display:inline-flex;"><a class="" class="" onclick="csmSampleMenuClick(\'Cal\',\'' + row.position + '\',\'' + row.pkId + '\')"  data-bs-toggle="modal" data-bs-target="#modelCalibration" style="color:#656FEA;"><button data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Edit Calibration"><i class="bi bi-pencil-fill"></i></button></a>&nbsp;&nbsp;<a class="" onclick="DeleteConfirmationSampleTray(\'Cal\',\'' + row.position + '\',\'' + row.pkId + '\')" style="color:red;"><button data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Calibration"><i class="bi bi-trash"></i></button></a></div>';
                        }
                        else if (row.type == "QC") {
                            return '<div style="float: left;">QC </div><div style="display:inline-flex;"><a class=""class="" onclick="csmSampleMenuClick(\'QC\',\'' + row.position + '\',\'' + row.pkId + '\')"  data-bs-toggle="modal" data-bs-target="#modelQC" style="color:#656FEA;"> <button data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Edit QC"><i class="bi bi-pencil-fill"></i></button></a>&nbsp;&nbsp;<a class="" onclick="DeleteConfirmationSampleTray(\'QC\',\'' + row.position + '\',\'' + row.pkId + '\')" style="color:red;"><button data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete QC"><i class="bi bi-trash"></i></button></a></div>';
                        }
                        else if (row.type == "Sample") {
                            return '<div style="float: left;">' + row.sampleId + '</div><div style="display:inline-flex;"><a class="" class="" onclick="csmSampleMenuClick(\'Sample\',\'' + row.position + '\',\'' + row.pkId + '\')" style="color:#656FEA;"> <button data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Edit Sample"><i class="bi bi-pencil-fill"></i></button></a>&nbsp;&nbsp;<a class=""  onclick="DeleteConfirmationSampleTray(\'Sample\',\'' + row.testCode[0].position + '\',\'' + row.pkId + '\')" style="color:red;"><button data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Sample"><i class="bi bi-trash"></i></button></a></div>';
                        }
                    }
                },
                { data: 'patient', title: 'Patient Detail' },
                {
                    data: "testCode", title: 'Position',
                    render: function (data, type, row, meta) {
                        if (row.testCode.length > 0) {
                            return row.testCode[0].position;
                        }
                        else {
                            return '';
                        }
                    }
                },
                { data: 'sampleType', title: 'Sample Type' },

                { data: 'lotNo', title: 'Lot No' },
                {
                    data: "testCode", title: 'Test Code ', "width": "100%",
                    render: function (data, type, row, meta) {
                        var testCode = '<div id="divWorkListTestCode">';
                        for (var i = 0; i < row.testCode.length; i++) {
                            if (row.testCode[i].position > 0) {
                                testCode += '<div class="btn_Color btn rounded-pill" style="margin:4px;"  accesskey="check" id="' + row.testCode[i].testCode + '" style="background: #039b54;"><input type="hidden" id="hdQcDetId" value="' + row.testCode[i].testCode + '"/><button type="button" class="">' + row.testCode[i].testCode + ' </button></div>'
                            }
                            else {
                                testCode += '<div class="btn_Color btn rounded-pill"  style="margin:4px;"  accesskey="" id="' + row.testCode[i].testCode + '" style="background: rgb(112, 114, 116);"><input type="hidden" id="hdQcDetId" value="' + row.testCode[i].testCode + '"/><button type="button" class="">' + row.testCode[i].testCode + ' </button></div>'
                            }
                        }
                        testCode += '</div>'
                        return testCode;
                    }
                },

            ],
            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblWorkList").css("display", "none");
        $("#divWorkListNoRecordFound").empty();
        $("#divWorkListNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');

    }
    $("#preloader").css("display", "none");

}


function DeleteConfirmationWorkList(Type, Id) {
    if (Type == "Sample") {
        DeleteConfirmationSample(Id);
    }
}

function SaveSample() {

        var returnValue = true;
        var TestCode = [];
        $("#divTestCodeBind div").each(function () {
            if (($(this).attr("accesskey")) == "check") {
                TestCode.push({
                    TestId: $(this).attr("id"),
                });
            }
        });
        if ($("#txtSmpBarCode").val() == "") {
            $("#txtSmpBarCode").focus();
            Message("error", "sample bar code is required");
            returnValue = false;
        }
        else if ($("#txtSmpAge").val() == "") {
            $("#txtSmpAge").focus();
            Message("error", "Age is required");
            returnValue = false;
        }
        else if ($("#ddlSmpType").val() == "") {
            $("#ddlSmpType").focus();
            Message("error", "Sample Type is required");
            returnValue = false;
        }
        else if ($("#ddlSmpConType").val() == "") {
            $("#ddlSmpConType").focus();
            Message("error", "Sample Container Type is required");
            returnValue = false;
        }
        else if ($("#ddlSmpPosition").val() == "") {
            $("#ddlSmpPosition").focus();
            Message("error", "Position is required");
            returnValue = false;
        }
        else {
            $("#preloader").css("display", "block");

            var obj = {
                SmpId: $("#hdsmpId").val(),
                PatientId: $("#hdPatId").val(),
                Age: $("#txtSmpAge").val(),
                Height: $("#txtSmpHeight").val(),
                Weight: $("#txtSmpWeight").val(),
                BedNumber: $("#txtSmpBedNo").val(),
                SmpBarCode: $("#txtSmpBarCode").val(),
                drId: $("#ddlDoctorId").val(),
                LebHospitalName: $("#txtLeb_HospitalName").val(),
                RegistrationDate: $("#dtRegistrationDate").val(),
                CollectionDate: $("#dtCollectionDate").val(),
                SmpTypeId: $("#ddlSmpType").val(),
                SmpContTypeId: $("#ddlSmpConType").val(),
                Position: $("#ddlSmpPosition").val(),
                HospitalArea: $("#txtHospitalArea").val(),
                SmpRemark: $("#txtSmpRemark").val(),

            }
            $.ajax({
                method: "POST",
                url: '/Schedule/SaveSample',
                data: obj,
                data: { sampleTest: TestCode, sample: obj },
                success: function (result) {
                    if (result > 0) {
                        if ($("#hdsmpId").val() > 0) {
                            Message("success", "Updated Data Successfully");

                        }
                        else {
                            Message("success", "Saved Data Successfully");
                        }
                        ClearSampleTray();
                        BindTableWorkList();
                        ClearSample();

                    }
                    else {

                        Message("error", "Somthing went wrong");
                    }
                    $("#preloader").css("display", "none");

                },
                error: function (e) {
                    Message("warning", "Somthing went wrong");
                    $("#preloader").css("display", "none");

                }
            });
        }
    
}

function UpdateShowSample(Id) {
    $("#preloader").css("display", "block");

    $.ajax({

        method: "GET",
        url: '/Schedule/GetByIdSample?Id=' + Id,
        success: function (result) {
            if (result != null) {
                console.log(result);
                if (result.patient != null) {

                    $("#hdPatId").val(result.patient.patId);
                    $("#txtPatientId").val(result.patient.patientId);
                    $("#txtPatName").val(result.patient.patName);
                    if (result.patient.dob != null) {
                        $("#dtDOB").val(moment(result.patient.dob).format("YYYY-MM-DD"));
                        $("#dtDOB").change();
                    }

                    $("#txtHeight").val(result.patient.height);
                    $("#txtWeight").val(result.patient.weight);
                    $("#ddlGender").val(result.patient.sex);
                    $("#txtAddress").val(result.patient.address);
                }

                if (result.sample != null) {
                    $("#hdsmpId").val(result.sample.smpId);
                    $("#txtSmpAge").val(result.sample.age);
                    $("#txtSmpHeight").val(result.sample.height);
                    $("#txtSmpWeight").val(result.sample.weight);
                    $("#txtSmpBedNo").val(result.sample.bedNumber);
                    $("#txtSmpBarCode").val(result.sample.smpBarcode);
                    $("#ddlDoctorId").val(result.sample.drId);
                    $("#txtLeb_HospitalName").val(result.sample.lebHospitalName);
                    if (result.sample.registrationDate != null) {
                        $("#dtRegistrationDate").val(moment(result.sample.registrationDate).format("YYYY-MM-DD"));
                        $("#dtRegistrationDate").change();
                    }
                    if (result.sample.collectionDate != null) {
                        $("#dtCollectionDate").val(moment(result.sample.collectionDate).format("YYYY-MM-DD"));
                        $("#dtCollectionDate").change();
                    }
                    //$("#dtRegistrationDate").val(result.sample.registrationDate);
                    //$("#dtCollectionDate").val(result.sample.collectionDate);
                    $("#ddlSmpType").val(result.sample.smpTypeId);
                    $("#ddlSmpConType").val(result.sample.smpContTypeId);
                    $("#ddlSmpPosition").append('<option value="' + result.sample.position + '">' + result.sample.position + '</option>');
                    $("#ddlSmpPosition").val(result.sample.position);

                    $("#txtHospitalArea").val(result.sample.hospitalArea);
                    $("#txtSmpRemark").val(result.sample.smpRemark);

                    if (result.sample.sampleTest.length > 0) {
                        var sampleTest = result.sample.sampleTest;
                        for (var i = 0; i < sampleTest.length; i++) {
                            $("#divTestCodeBind div").each(function () {
                                if (sampleTest[i].testId == $(this).attr("id")) {
                                    $(this).attr("accesskey", "check");
                                    this.style.background = "#039b54";
                                }
                            });
                        }
                    }
                }

                $("#btnSaveTestProfile").html("Update");
                $("#modelSample").modal("show");

            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationSample(Id) {
    $("#modelDeleteConfirmationSample").modal('show');
    $("#hdDeleteSampleId").val(Id);
}

function DeleteSample() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteSampleId").val();
    $("#modelDeleteConfirmationSample").modal('hide');
    $("#hdDeleteSampleId").val('');
    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/Schedule/DeleteSample',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    ClearSampleTray();
                    BindTableSample();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function ClearSample() {
    ClearPatient();
    $("#hdsmpId").val('');
    $("#txtSmpAge").val('');
    $("#txtSmpHeight").val('');
    $("#txtSmpWeight").val('');
    $("#txtSmpBedNo").val('');
    $("#txtSmpBarCode").val('');
    $("#ddlDoctorId").val('');
    $("#txtLeb_HospitalName").val('');
    $("#dtRegistrationDate").val('');
    $("#dtCollectionDate").val('');
    $("#ddlSmpType").val('');
    $("#ddlSmpConType").val('');
    $("#ddlSmpPosition").val('');

    $("#txtHospitalArea").val('');
    $("#txtSmpRemark").val('');
    ClearSelectTestCode();
    $("#divTestProfileBind div").each(function () {
        $(this).attr("accesskey", "");
        this.style.background = "#707274";
    });
    $("#btnSaveTestProfile").text("Save");
    $("#modelSample").modal('hide');
}

function ClearSelectTestCode() {
    $("#divTestCodeBind div").each(function () {
        $(this).attr("accesskey", "");
        this.style.background = "#707274";
    });
}

function GetAllSample() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Schedule/GetAllSample',
        success: function (result) {
            if (result != null) {
                for (var i = 0; i < result.length; i++) {
                    var TooltipData = "<b>Sample</b> <br>";
                    TooltipData += " <p>Sample ID : " + result[i].sampleId + "<br>";
                    TooltipData += " Patient Name : " + result[i].patient + "<br>";
                    TooltipData += " Position : " + result[i].testCode[0].position + "<br>";
                    TooltipData += " Test Code : ";
                    var testCode = result[i].testCode;

                    for (var j = 0; j < testCode.length; j++) {
                        if (testCode[j].position > 0) {
                            $("#SmpTray_" + testCode[j].position + " div").css("background", "#f5060694");
                            TooltipData += testCode[j].testCode + ", ";
                            $("#SmpTray_" + testCode[j].position).attr("data-bs-original-title", TooltipData);
                            $("#SmpTray_" + testCode[j].position + " div").attr("accesskey", "Sample");
                            $("#SmpTray_" + testCode[j].position + " div").attr("id", result[i].pkId);
                        }
                    }
                }
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });

}

/*End Sample*/



/*Start Calibration */




function GetListLot() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Schedule/GetListLot',
        success: function (result) {
            if (result != null) {
                //Calibration Lot
                $("#ddlCalLotNo").empty();
                var calibrationLot = '';
                for (var i = 0; i < result.calibrationLot.length; i++) {
                    calibrationLot += '<option value="' + result.calibrationLot[i].calibLotId + '">' + result.calibrationLot[i].lotNo + '</option>'
                }
                $("#ddlCalLotNo").append(calibrationLot);

                //QC Lot
                $("#ddlQCLotNo").empty();
                var qcLot = '';
                for (var i = 0; i < result.qcLot.length; i++) {
                    qcLot += '<option value="' + result.qcLot[i].lotNo + '">' + result.qcLot[i].lotNo + '</option>'
                }
                $("#ddlQCLotNo").append(qcLot);

                //Reagent Lot
                $("#ddlRgtLotNo").empty();
                var rgLot = '';
                for (var i = 0; i < result.rgLot.length; i++) {
                    rgLot += '<option value="' + result.rgLot[i].lotNo + '">' + result.rgLot[i].lotNo + '</option>'
                }
                $("#ddlRgtLotNo").append(rgLot);
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function BindTableCalibration() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/Schedule/GetAllCalibration",
        method: 'get',
        success: OnsuccessCalibration
    });
}

function OnsuccessCalibration(response) {
    $('#tblCalibration').DataTable().destroy();

    var Pos = '<option value="0">0</option>';
    for (var i = 1; i <= 50; i++) {
        Pos += '<option value="' + i + '">' + i + '</option>';
    }
    $('#tblCalibration').DataTable({
        bProcessing: true,
        bLenghtChange: true,
        retrieve: true,
        bfilter: true,
        bSort: true,
        autoWidth: true,
        destroy: true,
        bPaging: true,
        data: response,
        columns: [
            {
                data: "lotNo", title: 'Lot No',
                render: function (data, type, row, meta) {
                    return '<p id="LotNo">' + row.lotNo + '</p>'
                }
            },
            {
                data: "testCode", title: 'Concentration',
                render: function (data, type, row, meta) {
                    return '<select class="form-control form-select SmpConId" id="ddlConcentration"><option value="1">High</option><option value="2">Normal</option><option value="3">Low</option></select>';
                }
            },
            {
                data: "testCode", title: 'Container Type ',
                render: function (data, type, row, meta) {
                    return '<select class="form-control form-select SmpConId" id="ddlContainerType">' + SampleContainer + '</select>';
                }
            },
            {
                data: "position", title: 'Position',
                render: function (data, type, row, meta) {
                    if (row.testCode.length >= 1) {
                        // return row.pos1[0].calDetId
                        return '<select class="form-control form-select position" id="ddlSmpPosition">' + Pos + '</select><input type="hidden" id="hdLotId" value="' + row.qcId + '"><input type="hidden" id="hdQcDetId" value="' + row.testCode[0].qcLotDetId + '">';
                    }
                    else {
                        return '<select class="form-control form-select position" id="ddlSmpPosition">' + Pos + '</select><input type="hidden" id="hdLotId" value="' + row.qcId + '"><input type="hidden" id="hdQcDetId" value="">';
                    }
                }
            },
            {
                data: "testCode", title: 'TestCode ', "width": "100%",
                render: function (data, type, row, meta) {
                    var testCode = '<div id="divCalibrationTestCodeBind">';
                    for (var i = 0; i < row.testCode.length; i++) {
                        if (row.testCode[i].position > 0) {
                            testCode += '<div class="btn_Color btn rounded-pill" onclick="funBtnSelect(this)" accesskey="check" id="' + row.testCode[i].calId + '" style="background: #039b54;"><input type="hidden" id="hdQcDetId" value="' + row.testCode[i].calDetId + '"/><button type="button" class="">' + row.testCode[i].testCode + ' </button></div>'
                        }
                        else {
                            testCode += '<div class="btn_Color btn rounded-pill" onclick="funBtnSelect(this)" accesskey="" id="' + row.testCode[i].calId + '" style="background: rgb(112, 114, 116);"><input type="hidden" id="hdQcDetId" value="' + row.testCode[i].calDetId + '"/><button type="button" class="">' + row.testCode[i].testCode + ' </button></div>'
                        }
                    }
                    testCode += '</div>'

                    return testCode;
                }
            },



        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData.testCode.length > 0) {
                $("td #ddlPosition", nRow).val(aData.testCode[0].position);
                $("td #ddlConcentration", nRow).val(aData.testCode[0].conc);
                $("td #ddlContainerType", nRow).val(aData.testCode[0].containerTypeId);

            }
        }

    });


    $('#tblCalibration').DataTable({
        bProcessing: true,
        bLenghtChange: true,
        retrieve: true,
        bfilter: true,
        bSort: true,
        bPaging: true,
        data: response,
        columns: [
            { data: 'testCode', title: 'Test Code' },

            {
                data: "pos1", title: 'Container Type ',
                render: function (data, type, row, meta) {
                    return '<select class="form-control SmpConId" id="ddlSmpContainer">' + SampleContainer + '</select>';
                }
            },
            {
                data: "pos1", title: 'Pos 1',
                render: function (data, type, row, meta) {
                    if (row.pos1.length >= 1) {
                        // return row.pos1[0].calDetId
                        return '<select class="form-control Pos" id="ddlPos1">' + Pos + '</select><input type="hidden" id="hdCalDetIdPos1" value="' + row.pos1[0].calDetId + '">';
                    }
                    else {
                        return ''
                    }
                }
            },
            {
                data: "pos1", title: 'Pos 2',
                render: function (data, type, row, meta) {
                    if (row.pos1.length >= 2) {
                        //return row.pos1[1].calDetId
                        return '<select class="form-control ddlPos2" id="ddlPos2">' + Pos + '</select><input type="hidden" id="hdCalDetIdPos2" value="' + row.pos1[1].calDetId + '">';
                    }
                    else {
                        return ''
                    }
                }
            },
            {
                data: "pos1", title: 'Pos 3',
                render: function (data, type, row, meta) {
                    if (row.pos1.length >= 3) {
                        //return row.pos1[2].calDetId
                        return '<select class="form-control ddlPos3" id="ddlPos3">' + Pos + '</select><input type="hidden" id="hdCalDetIdPos3" value="' + row.pos1[2].calDetId + '">';
                    }
                    else {
                        return ''
                    }
                }
            },
            {
                data: "pos1", title: 'Pos 4',
                render: function (data, type, row, meta) {
                    if (row.pos1.length >= 4) {
                        //return row.pos1[3].calDetId
                        return '<select class="form-control" id="ddlPos4">' + Pos + '</select><input type="hidden" id="hdCalDetIdPos4" value="' + row.pos1[3].calDetId + '">';
                    }
                    else {
                        return ''
                    }
                }
            },
            {
                data: "pos1", title: 'Pos 5',
                render: function (data, type, row, meta) {
                    if (row.pos1.length >= 5) {
                        //return row.pos1[4].calDetId
                        return '<select class="form-control" id="ddlPos5">' + Pos + '</select><input type="hidden" id="hdCalDetIdPos5" value="' + row.pos1[4].calDetId + '">';
                    }
                    else {
                        return ''
                    }
                }
            },
            {
                data: "pos1", title: 'Pos 6',
                render: function (data, type, row, meta) {
                    if (row.pos1.length >= 6) {
                        //return row.pos1[5].calDetId
                        return '<select class="form-control" id="ddlPos6">' + Pos + '</select><input type="hidden" id="hdCalDetIdPos6" value="' + row.pos1[5].calDetId + '">';
                    }
                    else {
                        return ''
                    }
                }
            },


            //{
            //    data: "calDetId", title: 'Action',
            //    render: function (data, type, row, meta) {
            //        return '<button type="button" class="btn" onclick ="UpdateShowSample(' + row.calDetId + ')" style="color:#656FEA;" > <b><i class="bi bi-pencil-fill"></i></b> </button> &nbsp;<button type="button" class="btn" onclick="DeleteConfirmationSample(' + row.calDetId + ')" style="color:red;"><b><i class="bi bi-trash"></i></b></button>';

            //    }
            //},
        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData.pos1.length > 0) {
                $("td #ddlPos1", nRow).val(aData.pos1[0].pos);
                $("td #ddlSmpContainer", nRow).val(aData.pos1[0].containerTypeId);

            }
            if (aData.pos1.length > 1) {
                $("td #ddlPos2", nRow).val(aData.pos1[1].pos);
            }
            if (aData.pos1.length > 2) {
                $("td #ddlPos3", nRow).val(aData.pos1[2].pos);
            }
            if (aData.pos1.length > 3) {
                $("td #ddlPos4", nRow).val(aData.pos1[3].pos);
            }
            if (aData.pos1.length > 4) {
                $("td #ddlPos5", nRow).val(aData.pos1[4].pos);
            }
            if (aData.pos1.length > 5) {
                $("td #ddlPos6", nRow).val(aData.pos1[5].pos);
            }
        }

    });

    $("#preloader").css("display", "none");

}

function GetAllCalibration() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Schedule/GetAllCalibration',
        success: function (result) {
            if (result.length > 0) {
                for (var i = 0; i < result.length; i++) {
                    var TooltipData = "<b>Calibration </b><br>";
                    if (result[i].lotNo != null) {
                        TooltipData += "<p> Lot No. : " + result[i].lotNo.lotNo + "<br>";
                        TooltipData += " Expiry Date : " + result[i].lotNo.expiry + "<br>";
                    }
                    else {
                        TooltipData += "<p> Lot No. : <br>";
                        TooltipData += " Expiry Date : <br>";
                    }
                    var testCode = result[i].testCode;
                    var CalId = [];

                    for (var j = 0; j < testCode.length; j++) {
                        var TestCode = "";
                        if (testCode[j].position > 0) {
                            if (jQuery.inArray(testCode[j].calId, CalId) == -1) {
                                CalId.push(testCode[j].calId);
                                TestCode = "";
                                TestCode += " Position : " + testCode[j].position + "<br>";
                                TestCode += " Test Code : " + testCode[j].testCode;
                            }
                            else {
                                TestCode += "," + testCode[j].testCode;
                            }
                            $("#SmpTray_" + testCode[j].position + " div").css("background", "#ffff0085");
                            $("#SmpTray_" + testCode[j].position).attr("data-bs-original-title", TooltipData + TestCode);
                            $("#SmpTray_" + testCode[j].position + " div").attr("accesskey", "Cal");
                        }
                    }
                }
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });

}


function SaveCalibration() {

    var returnValue = true;
    var list = [];
    var CalPosition = [];
    $("#tblCalibration tr").each(function (index) {
        if ($($(this).closest('tr')).find('td').find('.ddlCalLotNo').val() != "") {
            if (typeof ($($(this).closest('tr')).find('td').find('.hdCalDetIdPos1').val()) !== "undefined" && $($(this).closest('tr')).find('td').find('.hdCalDetIdPos1').val() != "" && $($(this).closest('tr')).find('td').find('.ddlCalPosition1').val() != "0") {
                if (jQuery.inArray($($(this).closest('tr')).find('td').find('.ddlCalPosition1').val(), CalPosition) == -1) {
                    list.push({
                        CalDetId: $($(this).closest('tr')).find('td').find('.hdCalDetIdPos1').val(),
                        Pos: $($(this).closest('tr')).find('td').find('.ddlCalPosition1').val(),
                        ContainerTypeId: $($(this).closest('tr')).find('td').find('.ddlCalContinerType').val(),
                    });
                    CalPosition.push($($(this).closest('tr')).find('td').find('.ddlCalPosition1').val());
                }
                else {
                    returnValue = false;
                }
            }

            if (typeof ($($(this).closest('tr')).find('td').find('.hdCalDetIdPos2').val()) !== "undefined" && $($(this).closest('tr')).find('td').find('.hdCalDetIdPos2').val() != "" && $($(this).closest('tr')).find('td').find('.ddlCalPosition2').val() != "0") {
                if (jQuery.inArray($($(this).closest('tr')).find('td').find('.ddlCalPosition2').val(), CalPosition) == -1) {

                    list.push({
                        CalDetId: $($(this).closest('tr')).find('td').find('.hdCalDetIdPos2').val(),
                        Pos: $($(this).closest('tr')).find('td').find('.ddlCalPosition2').val(),
                        ContainerTypeId: $($(this).closest('tr')).find('td').find('.ddlCalContinerType').val(),
                    });
                    CalPosition.push($($(this).closest('tr')).find('td').find('.ddlCalPosition2').val());
                }
                else {
                    returnValue = false;
                }
            }

            if (typeof ($($(this).closest('tr')).find('td').find('.hdCalDetIdPos3').val()) !== "undefined" && $($(this).closest('tr')).find('td').find('.hdCalDetIdPos3').val() != "" && $($(this).closest('tr')).find('td').find('.ddlCalPosition3').val() != "0") {
                if (jQuery.inArray($($(this).closest('tr')).find('td').find('.ddlCalPosition3').val(), CalPosition) == -1) {

                    list.push({
                        CalDetId: $($(this).closest('tr')).find('td').find('.hdCalDetIdPos3').val(),
                        Pos: $($(this).closest('tr')).find('td').find('.ddlCalPosition3').val(),
                        ContainerTypeId: $($(this).closest('tr')).find('td').find('.ddlCalContinerType').val(),
                    });
                    CalPosition.push($($(this).closest('tr')).find('td').find('.ddlCalPosition3').val());
                }
                else {
                    returnValue = false;
                }
            }

            if (typeof ($($(this).closest('tr')).find('td').find('.hdCalDetIdPos4').val()) !== "undefined" && $($(this).closest('tr')).find('td').find('.hdCalDetIdPos4').val() != "" && $($(this).closest('tr')).find('td').find('.ddlCalPosition4').val() != "0") {
                if (jQuery.inArray($($(this).closest('tr')).find('td').find('.ddlCalPosition4').val(), CalPosition) == -1) {

                    list.push({
                        CalDetId: $($(this).closest('tr')).find('td').find('.hdCalDetIdPos4').val(),
                        Pos: $($(this).closest('tr')).find('td').find('.ddlCalPosition4').val(),
                        ContainerTypeId: $($(this).closest('tr')).find('td').find('.ddlCalContinerType').val(),
                    });
                    CalPosition.push($($(this).closest('tr')).find('td').find('.ddlCalPosition4').val());
                }
                else {
                    returnValue = false;
                }
            }
            if (typeof ($($(this).closest('tr')).find('td').find('.hdCalDetIdPos5').val()) !== "undefined" && $($(this).closest('tr')).find('td').find('.hdCalDetIdPos5').val() != "" && $($(this).closest('tr')).find('td').find('.ddlCalPosition5').val() != "0") {
                if (jQuery.inArray($($(this).closest('tr')).find('td').find('.ddlCalPosition5').val(), CalPosition) == -1) {

                    list.push({
                        CalDetId: $($(this).closest('tr')).find('td').find('.hdCalDetIdPos5').val(),
                        Pos: $($(this).closest('tr')).find('td').find('.ddlCalPosition5').val(),
                        ContainerTypeId: $($(this).closest('tr')).find('td').find('.ddlCalContinerType').val(),
                    });
                    CalPosition.push($($(this).closest('tr')).find('td').find('.ddlCalPosition5').val());
                }
                else {
                    returnValue = false;
                }
            }

            if (typeof ($($(this).closest('tr')).find('td').find('.hdCalDetIdPos6').val()) !== "undefined" && $($(this).closest('tr')).find('td').find('.hdCalDetIdPos6').val() != "" && $($(this).closest('tr')).find('td').find('.ddlCalPosition6').val() != "0") {
                if (jQuery.inArray($($(this).closest('tr')).find('td').find('.ddlCalPosition6').val(), CalPosition) == -1) {

                    list.push({
                        CalDetId: $($(this).closest('tr')).find('td').find('.hdCalDetIdPos6').val(),
                        Pos: $($(this).closest('tr')).find('td').find('.ddlCalPosition6').val(),
                        ContainerTypeId: $($(this).closest('tr')).find('td').find('.ddlCalContinerType').val(),
                    });
                    CalPosition.push($($(this).closest('tr')).find('td').find('.ddlCalPosition6').val());
                }
                else {
                    returnValue = false;
                }
            }

            if (typeof ($($(this).closest('tr')).find('td').find('.hdCalDetIdPos7').val()) !== "undefined" && $($(this).closest('tr')).find('td').find('.hdCalDetIdPos7').val() != "" && $($(this).closest('tr')).find('td').find('.ddlCalPosition7').val() != "0") {
                if (jQuery.inArray($($(this).closest('tr')).find('td').find('.ddlCalPosition7').val(), CalPosition) == -1) {

                    list.push({
                        CalDetId: $($(this).closest('tr')).find('td').find('.hdCalDetIdPos7').val(),
                        Pos: $($(this).closest('tr')).find('td').find('.ddlCalPosition7').val(),
                        ContainerTypeId: $($(this).closest('tr')).find('td').find('.ddlCalContinerType').val(),
                    });
                    CalPosition.push($($(this).closest('tr')).find('td').find('.ddlCalPosition7').val());
                }
                else {
                    returnValue = false;
                }
            }


            if (typeof ($($(this).closest('tr')).find('td').find('.hdCalDetIdPos8').val()) !== "undefined" && $($(this).closest('tr')).find('td').find('.hdCalDetIdPos8').val() != "" && $($(this).closest('tr')).find('td').find('.ddlCalPosition8').val() != "0") {
                if (jQuery.inArray($($(this).closest('tr')).find('td').find('.ddlCalPosition8').val(), CalPosition) == -1) {

                    list.push({
                        CalDetId: $($(this).closest('tr')).find('td').find('.hdCalDetIdPos8').val(),
                        Pos: $($(this).closest('tr')).find('td').find('.ddlCalPosition8').val(),
                        ContainerTypeId: $($(this).closest('tr')).find('td').find('.ddlCalContinerType').val(),
                    });
                    CalPosition.push($($(this).closest('tr')).find('td').find('.ddlCalPosition8').val());
                }
                else {
                    returnValue = false;
                }
            }

            if (typeof ($($(this).closest('tr')).find('td').find('.hdCalDetIdPos9').val()) !== "undefined" && $($(this).closest('tr')).find('td').find('.hdCalDetIdPos9').val() != "" && $($(this).closest('tr')).find('td').find('.ddlCalPosition9').val() != "0") {
                if (jQuery.inArray($($(this).closest('tr')).find('td').find('.ddlCalPosition9').val(), CalPosition) == -1) {

                    list.push({
                        CalDetId: $($(this).closest('tr')).find('td').find('.hdCalDetIdPos9').val(),
                        Pos: $($(this).closest('tr')).find('td').find('.ddlCalPosition9').val(),
                        ContainerTypeId: $($(this).closest('tr')).find('td').find('.ddlCalContinerType').val(),
                    });
                    CalPosition.push($($(this).closest('tr')).find('td').find('.ddlCalPosition9').val());
                }
                else {
                    returnValue = false;
                }
            }

            if (typeof ($($(this).closest('tr')).find('td').find('.hdCalDetIdPos10').val()) !== "undefined" && $($(this).closest('tr')).find('td').find('.hdCalDetIdPos10').val() != "" && $($(this).closest('tr')).find('td').find('.ddlCalPosition10').val() != "0") {
                if (jQuery.inArray($($(this).closest('tr')).find('td').find('.ddlCalPosition10').val(), CalPosition) == -1) {

                    list.push({
                        CalDetId: $($(this).closest('tr')).find('td').find('.hdCalDetIdPos10').val(),
                        Pos: $($(this).closest('tr')).find('td').find('.ddlCalPosition10').val(),
                        ContainerTypeId: $($(this).closest('tr')).find('td').find('.ddlCalContinerType').val(),
                    });
                    CalPosition.push($($(this).closest('tr')).find('td').find('.ddlCalPosition10').val());
                }
                else {
                    returnValue = false;
                }
            }
        }

    });
    if (returnValue == false) {
        Message("warning", "Please select other position");
        return false;
    }

    else {
        $("#preloader").css("display", "block");
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/Schedule/SaveCalibration',
            data: { calibrationdets: list },
            success: function (result) {
                if (result) {
                    Message("success", "Saved Data Successfully");
                    $("#modelCalibration").modal('hide');
                    ClearSampleTray();
                    BindTableWorkList();
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}
/*End Calibration*/


/*Start Quality Control */
function BindTableQC() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/Schedule/GetAllQC",
        method: 'get',
        success: OnsuccessQC
    });
}

function OnsuccessQC(response) {
    $('#tblQC').DataTable().destroy();
    var Pos = '<option value=""></option>';
    for (var i = 1; i <= 40; i++) {
        Pos += '<option value="' + i + '">' + i + '</option>';
    }
    console.log("QC " + response);

    $('#tblQC').DataTable({
        bProcessing: true,
        bLenghtChange: true,
        retrieve: true,
        bfilter: true,
        bSort: true,
        bPaging: true,
        data: response,
        columns: [
            {
                data: "lotNo", title: 'Lot No',
                render: function (data, type, row, meta) {
                    return '<p id="LotNo">' + row.lotNo + '</p>'
                }
            },
            {
                data: "testCode", title: 'Concentration',
                render: function (data, type, row, meta) {
                    return '<select class="form-control form-select SmpConId" id="ddlConcentration"><option value="1">High</option><option value="2">Normal</option><option value="3">Low</option></select>';
                }
            },
            {
                data: "testCode", title: 'Container Type ',
                render: function (data, type, row, meta) {
                    return '<select class="form-control form-select SmpConId" id="ddlContainerType">' + SampleContainer + '</select>';
                }
            },
            {
                data: "position", title: 'Position',
                render: function (data, type, row, meta) {
                    if (row.testCode.length >= 1) {
                        // return row.pos1[0].calDetId
                        return '<select class="form-control form-select position" id="ddlPosition">' + Pos + '</select><input type="hidden" id="hdLotId" value="' + row.qcId + '"><input type="hidden" id="hdQcDetId" value="' + row.testCode[0].qcLotDetId + '">';
                    }
                    else {
                        return '<select class="form-control form-select position" id="ddlPosition">' + Pos + '</select><input type="hidden" id="hdLotId" value="' + row.qcId + '"><input type="hidden" id="hdQcDetId" value="">';
                    }
                }
            },
            {
                data: "testCode", title: 'TestCode ',
                render: function (data, type, row, meta) {
                    var testCode = '<div id="divQCTestCodeBind">';
                    for (var i = 0; i < row.testCode.length; i++) {
                        if (row.testCode[i].position > 0) {
                            testCode += '<div class="btn_Color btn rounded-pill" onclick="funBtnSelect(this)" accesskey="check" id="' + row.testCode[i].lotId + '" style="background: #039b54;"><input type="hidden" id="hdQcDetId" value="' + row.testCode[i].qcLotDetId + '"/><button type="button" class="">' + row.testCode[i].testCode + ' </button></div>'
                        }
                        else {
                            testCode += '<div class="btn_Color btn rounded-pill" onclick="funBtnSelect(this)" accesskey="" id="' + row.testCode[i].lotId + '" style="background: rgb(112, 114, 116);"><input type="hidden" id="hdQcDetId" value="' + row.testCode[i].qcLotDetId + '"/><button type="button" class="">' + row.testCode[i].testCode + ' </button></div>'
                        }
                    }
                    testCode += '</div>'

                    return testCode;
                }
            },



        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData.testCode.length > 0) {
                $("td #ddlPosition", nRow).val(aData.testCode[0].position);
                $("td #ddlConcentration", nRow).val(aData.testCode[0].concentration);
                $("td #ddlContainerType", nRow).val(aData.testCode[0].concentration);


            }
        }

    });
    $("#preloader").css("display", "none");

}

function GetAllQC() {
    $("#preloader").css("display", "block");
    $.ajax({
        method: "GET",
        url: '/Schedule/GetAllQC',
        success: function (result) {
            if (result != null) {
                for (var i = 0; i < result.length; i++) {
                    var TooltipData = "<b>Quality Control </b><br>";
                    TooltipData += "<p> Lot No : " + result[i].lotNo + "<br>";
                    TooltipData += " Expiry Date : " + result[i].expiry + "<br>";
                    TooltipData += " Position : " + result[i].testCode[0].position + "<br>";
                    TooltipData += " Test Code : ";
                    var testCode = result[i].testCode;

                    for (var j = 0; j < testCode.length; j++) {
                        if (testCode[j].position > 0) {
                            $("#SmpTray_" + testCode[j].position + " div").css("background", "#19e719a3");
                            TooltipData += testCode[j].testCode;
                            $("#SmpTray_" + testCode[j].position).attr("data-bs-original-title", TooltipData);
                            $("#SmpTray_" + testCode[j].position + " div").attr("accesskey", "QC");
                        }
                    }
                }
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });

}

function SaveQC() {
    var list = [];
    var position = [];
    var LotId = [];
    var retunValue = true;
    $("#tblQC tr").each(function () {
        if (typeof ($($(this).closest('tr')).find('td').find('.ddlQCLotNo').val()) !== "undefined") {
            if ($($(this).closest('tr')).find('td').find('.ddlQCLotNo').val() != "" && ($($(this).closest('tr')).find('td').find('.ddlQcPosition').val() != "" && $($(this).closest('tr')).find('td').find('.ddlQcPosition').val() != 0)) {

                $($(this).closest('tr')).find('td').find(".divQcTestCode div").each(function () {
                    /*if (($(this).attr("accesskey") == "check")) { */
                    var mLotId = $($(this).closest('tr')).find('td').find('.ddlQCLotNo').val();
                    var mPosition = $($(this).closest('tr')).find('td').find('.ddlQcPosition').val();

                    if ((jQuery.inArray(mPosition, position) == -1 && jQuery.inArray(mLotId, LotId) == -1)

                        || ((jQuery.inArray(mPosition, position) != -1) && (jQuery.inArray(mLotId, LotId) != -1))) {
                        list.push({
                            LotId: $(this).attr("id"),
                            QcdetId: $(this).find('.hdQcDetId').val(),
                            Position: $($(this).closest('tr')).find('td').find('.ddlQcPosition').val(),
                            Conc: $($(this).closest('tr')).find('td').find('.hdQcContinerType').val(),
                        });
                        //LotId.push($(this).attr("id"));
                        //position.push($($(this).closest('tr')).find('td').find('.ddlQcPosition').val());
                    }
                    else {
                        retunValue = false;
                    }
                });
                LotId.push($($(this).closest('tr')).find('td').find('.ddlQCLotNo').val());
                position.push($($(this).closest('tr')).find('td').find('.ddlQcPosition').val());
            }
            else {
                retunValue = false;
            }
        }
    });
    if (retunValue == false) {
        Message("warning", "Please select other position");
        return false;
    }
    else {
        $("#preloader").css("display", "block");

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/Schedule/SaveQC',
            data: { qcDet: list },
            success: function (result) {
                if (result) {
                    Message("success", "Saved Data Successfully");
                    $("#modelQC").modal('hide');
                    ClearSampleTray()
                    BindTableWorkList();
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

/*End  Quality Control */


function GetCalibrationQCMst() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Schedule/GetCalibrationQCMst',
        success: function (result) {
            if (result != null) {

                var ddlCalLotNo = '<option value="">Select Lot No</option>';
                var ddlQCLotNo = '<option value="">Select Lot No</option>';

                var ddlRegLotNo = '<option value="">Lot No</option>'; //reagent
                var ddlRegPos = '<option value="">Select Position</option>'; //reagent //RPoistion
                var ddlbottleType = '<option value="">Select Bottle Size</option>'; //reagent //bottle size
                var ddlRgtManufacturer = '<option value="">Select Manufacturer</option>';

                var ddlRegExpMonth = '<option value="">Month</option>'; //reagent //ddlRegExpMonth size
                var ddlRegExpYr = '<option value="">Year</option>';

                var ddlcontainerType = '<option value="">select container type</option>';
                var ddlPosition = '<option value="">Select Position</option>';

                for (var i = 0; i < result.calibrationLotNo.length; i++) {
                    ddlCalLotNo += '<option value="' + result.calibrationLotNo[i].lotId + '">' + result.calibrationLotNo[i].lotNo + '</option>'
                }
                sessionStorage.setItem("ddlCalLotNo", ddlCalLotNo);

                for (var i = 0; i < result.qcLotNo.length; i++) {
                    ddlQCLotNo += '<option value="' + result.qcLotNo[i].lotId + '">' + result.qcLotNo[i].lotNo + '</option>'
                }
                sessionStorage.setItem("ddlQCLotNo", ddlQCLotNo);

                for (var i = 0; i < result.containerType.length; i++) {
                    ddlcontainerType += '<option value="' + result.containerType[i].smpConId + '">' + result.containerType[i].smpConName + '</option>'
                }
                sessionStorage.setItem("ddlcontainerType", ddlcontainerType);

                for (var i = 0; i < result.samplePoistion.length; i++) {
                    ddlPosition += '<option value="' + result.samplePoistion[i] + '">' + result.samplePoistion[i] + '</option>';
                }
                $("#ddlSmpPosition").empty();
                $("#ddlSmpPosition").append(ddlPosition);

                sessionStorage.setItem("ddlPosition", ddlPosition);
                //console.log(result.regLotNo);
                //RegLotNo
                for (var i = 0; i < result.regLotNo.length; i++) {

                    ddlRegLotNo += '<option value="' + result.regLotNo[i].rlotId + '">' + result.regLotNo[i].lotNo + '</option>'
                }
                sessionStorage.setItem("ddlRegLotNo", ddlRegLotNo);

                //RPoistion
                for (var i = 0; i < result.rPoistion.length; i++) {
                    ddlRegPos += '<option value="' + result.rPoistion[i] + '">' + result.rPoistion[i] + '</option>';
                }
                sessionStorage.setItem("ddlRegPos", ddlRegPos);

                $("#ddlRgtManufacturer").empty();
                for (var i = 0; i < result.rgtManufacturer.length; i++) {
                    ddlRgtManufacturer += '<option value="' + result.rgtManufacturer[i].mnfId + '">' + result.rgtManufacturer[i].mnfName + '</option>';
                }
                $("#ddlRgtManufacturer").append(ddlRgtManufacturer);


                for (var i = 0; i < result.bottlesize.length; i++) {
                    ddlbottleType += '<option value="' + result.bottlesize[i].rgntBtlSize + '">' + result.bottlesize[i].rgntBtlSize + '</option>';
                }
                sessionStorage.setItem("ddlbottleType", ddlbottleType);

                //ddlRegExpMonth
                $("#ddlRgtExpiryMonth").empty();
                for (var i = 0; i < result.months.length; i++) {
                    ddlRegExpMonth += '<option value="' + result.months[i].monValue + '">' + result.months[i].monName + '</option>';
                }
                sessionStorage.setItem("ddlRegExpMonth", ddlRegExpMonth);
                $("#ddlRgtExpiryMonth").append(ddlRegExpMonth);
                //ddlRegExpYear

                $("#ddlRgtExpiryYear").empty();
                for (var i = 0; i < result.year.length; i++) {
                    ddlRegExpYr += '<option value="' + result.year[i] + '">' + result.year[i] + '</option>';
                }
                sessionStorage.setItem("ddlRegExpYr", ddlRegExpYr);
                $("#ddlRgtExpiryYear").append(ddlRegExpYr);
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });

    // month yr function removed from here
}


var Caltab = '';
var counter = 0;
function CalNewRow() {
    if (counter > 0 && $("#ddlCalLotNo" + (counter - 1)).val() == "") {
        Message("warning", "Please select current row lot number");
        return false;
    }
    else {
        Caltab = $('#tblCalibration').DataTable({
            scroll: true, // used for fixed header
            bProcessing: true,
            bLenghtChange: true,
            retrieve: true,
            bfilter: true,
            bSort: true,
            autoWidth: false,
            destroy: true,

            dom: 'Bfrtip',
            buttons: [
                {
                    className: 'btn btn-success Add Another row',
                    text: "<button type='button' onclick='CalNewRow()'>Add Another Row</button>",
                    action: function (e, dt, node, config) {
                    },
                    titleAttr: 'Add Another Row'
                },
              
            ],


           // bPaging: true,
            //scrollX: true,
            //"sScrollX": "100%",
           // "scrollX": '100%',

            //"scrollXInner": true,
            //"scrollCollapse": true
            // scroller: {
            //     loadingIndicator: true
            // },
            // "scrollCollapse": true,

            // "scrollX": true,
            //"paging": false,
            //autoWidth: true,
            //destroy: true,
            //"searching": true,
            //ordering: true,
            //scrollY: 350,
            //scroller: {
            //    loadingIndicator: true
            //},
            "scrollCollapse": true,
            "paging": false,
            "jQueryUI": true,
            columnDefs: [{ type: 'hidden', 'targets': [0] }],
            order: [[0, 'desc']],
            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
        Caltab.row.add([
            '<input type="hidden" value="' + counter + '"><div style="display:inline-flex"><a class="btn CalDeleteRow" onclick="CalDeleteRow(' + counter + ')" style="color:red;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Calibration"><b><i class="bi bi-trash"></i></b></a>&nbsp;<select id="ddlCalLotNo' + counter + '" onchange="GetCalDetailByLotId(' + counter + ')" name="ddlCalLotNo" class="form-control ddlCalLotNo">' + sessionStorage.getItem("ddlCalLotNo") + '</select></div>',
            '<select id="ddlCalContinerType' + counter + '" name="ddlCalContinerType" class="form-control form-select ddlCalContinerType">' + sessionStorage.getItem("ddlcontainerType") + '</select>',
            //'<select id="ddlCalPosition' + counter + '" name="ddlCalPosition" class="form-control ddlCalPosition">' + sessionStorage.getItem("ddlPosition") + '</select>',
            '<select class="form-control form-select ddlCalPosition1" style="display: none;" id="ddlCalPosition1_' + counter + '">' + sessionStorage.getItem("ddlPosition") + '</select><input type="hidden" class="hdCalDetIdPos1" id="hdCalDetIdPos1_' + counter + '" value="">',
            '<select class="form-control form-select ddlCalPosition2" style="display: none;"  id="ddlCalPosition2_' + counter + '">' + sessionStorage.getItem("ddlPosition") + '</select><input type="hidden" class="hdCalDetIdPos2" id="hdCalDetIdPos2_' + counter + '" value="">',
            '<select class="form-control form-select ddlCalPosition3" style="display: none;"  id="ddlCalPosition3_' + counter + '">' + sessionStorage.getItem("ddlPosition") + '</select><input type="hidden" class="hdCalDetIdPos3" id="hdCalDetIdPos3_' + counter + '" value="">',
            '<select class="form-control form-select ddlCalPosition4" style="display: none;"  id="ddlCalPosition4_' + counter + '">' + sessionStorage.getItem("ddlPosition") + '</select><input type="hidden" class="hdCalDetIdPos4" id="hdCalDetIdPos4_' + counter + '" value="">',
            '<select class="form-control form-select ddlCalPosition5" style="display: none;"  id="ddlCalPosition5_' + counter + '">' + sessionStorage.getItem("ddlPosition") + '</select><input type="hidden" class="hdCalDetIdPos5" id="hdCalDetIdPos5_' + counter + '" value="">',
            '<select class="form-control form-select ddlCalPosition6" style="display: none;"  id="ddlCalPosition6_' + counter + '">' + sessionStorage.getItem("ddlPosition") + '</select><input type="hidden" class="hdCalDetIdPos6" id="hdCalDetIdPos6_' + counter + '" value="">',
            '<select class="form-control form-select ddlCalPosition7" style="display: none;"  id="ddlCalPosition7_' + counter + '">' + sessionStorage.getItem("ddlPosition") + '</select><input type="hidden" class="hdCalDetIdPos7" id="hdCalDetIdPos7_' + counter + '" value="">',
            '<select class="form-control form-select ddlCalPosition8" style="display: none;"  id="ddlCalPosition8_' + counter + '">' + sessionStorage.getItem("ddlPosition") + '</select><input type="hidden" class="hdCalDetIdPos8" id="hdCalDetIdPos8_' + counter + '" value="">',
            '<select class="form-control form-select ddlCalPosition9" style="display: none;"  id="ddlCalPosition9_' + counter + '">' + sessionStorage.getItem("ddlPosition") + '</select><input type="hidden" class="hdCalDetIdPos9" id="hdCalDetIdPos9_' + counter + '" value="">',
            '<select class="form-control form-select ddlCalPosition10" style="display: none;"  id="ddlCalPosition10_' + counter + '">' + sessionStorage.getItem("ddlPosition") + '</select><input type="hidden" class="hdCalDetIdPos10" id="hdCalDetIdPos10_' + counter + '" value="">',

            '<div class="divCalTestCode" id="divCalTestCode' + counter + '" style="width:425px;"></div>',
        ]).draw(false);

        if (counter > 0) {
            for (var i = counter; i >= 0; i--) {
                $('#ddlCalLotNo' + counter + ' option[value="' + $('#ddlCalLotNo' + (i - 1)).val() + '"]').detach();
            }
        }
        $("#ddlCalLotNo" + counter).select2({
            dropdownParent: $("#modelCalibration"),
            width: "100%"
        });
        /*    BindCalTableData();*/
        counter++;
    }
}


function onsuccessCalTable(response) {
    $('#tblTestParameter').DataTable().destroy();
    $('#tblTestParameter').DataTable({
        scrollX: true,
        scroll: true, // used for fixed header
        scrollY: 300, // used for fixed Hieght
        bProcessing: true,
        bLenghtChange: true,
        retrieve: true,
        bfilter: true,
        bSort: true,
        bPaging: true,
        data: response,
        columns: [
            { data: 'drId', title: 'sr no' },
            { data: 'drName', title: 'Doctor' },
            { data: 'drDepartment', title: 'Department' },
            { data: 'drRemark', title: 'Remark' },
            {
                data: "drId", title: 'Action',
                render: function (data, type, row, meta) {
                    return '<button type="button" class="btn btn-primary" onclick = UpdateShowTestParameter(' + row.drId + ') > Update </button>&nbsp;<button type="button" class="btn btn-danger" onclick="DeleteConfirmationTestParameter(' + row.drId + ')">Delete</button>';
                }
            },
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var index = iDisplayIndex + 1;
            $('td:eq(0)', nRow).html(index);
            return nRow;
        }
    });
}




var QcTab = '';
var QcCounter = 0;
function QcNewRow() {

    if (QcCounter > 0 && $("#ddlQCLotNo" + (QcCounter - 1)).val() == "") {
        Message("warning", "Please select current row lot number");
        return false;
    }
    else {
        QcTab = $('#tblQC').DataTable({
            scroll: true, // used for fixed header
            bProcessing: true,
            bLenghtChange: true,
            retrieve: true,
            bfilter: true,
            bSort: true,
            autoWidth: false,
            destroy: true,
            "scrollCollapse": true,
            "paging": false,
            "jQueryUI": true,
            columnDefs: [{ type: 'hidden', 'targets': [0] }],
            order: [[0, 'desc']],
            dom: 'Bfrtip',
            buttons: [
                {
                    className: 'btn btn-success Add Another row',
                    text: "<button type='button' onclick='QcNewRow()'>Add Another Row</button>",
                    action: function (e, dt, node, config) {
                    },
                    titleAttr: 'Add Another Row'
                },

            ],








           //// "scrollX": true,
           // "paging": false,
           // autoWidth: true,
           // destroy: true,
           // "searching": true,
           // ordering: true,
           // scrollY: 350,
           // scroller: {
           //     loadingIndicator: true
           // },
           // "scrollCollapse": true,
           // // "paging": false,
           // "jQueryUI": true,
           // columnDefs: [{ type: 'hidden', 'targets': [0] }],
           // order: [[0, 'desc']],


            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
        QcTab.row.add([
            '<input type="hidden" value="' + QcCounter + '"><div style="display:inline-flex"><a class="btn QcDeleteRow" onclick="QcDeleteRow(' + QcCounter + ')" style="color:red;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete QC"><b><i class="bi bi-trash"></i></b></a>&nbsp;<select id="ddlQCLotNo' + QcCounter + '" onchange="GetQCDetailByLotId(' + QcCounter + ')" name="ddlQCLotNo" class="form-control ddlQCLotNo">' + sessionStorage.getItem("ddlQCLotNo") + '</select><div>',
            '<p id="QcContinerType' + QcCounter + '"></p><input type="hidden" id="hdQcContinerType_' + QcCounter + '" class="hdQcContinerType">',
            //'<select id="ddlCalContinerType' + QcCounter + '" name="ddlCalContinerType" class="form-control ddlCalContinerType">' + sessionStorage.getItem("ddlcontainerType") + '</select>',
            '<select class="form-control form-select ddlQcPosition"  id="ddlQcPosition' + QcCounter + '">' + sessionStorage.getItem("ddlPosition") + '</select><input type="hidden" class="hdCalDetIdPos1" id="hdCalDetIdPos1_' + QcCounter + '" value="">',
            '<div class="divQcTestCode" id="divQcTestCode' + QcCounter + '" style="width:425px;"></div>',

        ]).draw(false);

        if (QcCounter > 0) {
            for (var i = QcCounter; i >= 0; i--) {
                $('#ddlQCLotNo' + QcCounter + ' option[value="' + $('#ddlQCLotNo' + (i - 1)).val() + '"]').detach();
            }
        }

        $("#ddlQCLotNo" + QcCounter).select2({
            dropdownParent: $("#modelQC"),
            width: "100%"
        });

        QcCounter++;
    }

}

/*  ------ following code added by jayashri - save reagent ----------*/

// ------------------- Reagent code -----------------------------------

function BindTableReagent() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/Schedule/GetAllReagent", //GetAllCalibration
        method: 'get',
        success: OnsuccessReagent //OnsuccessCalibration
    });
}

function OnsuccessReagent(response) {
    $('#tblReg').DataTable().destroy();
    var Pos = '<option value="0">0</option>';
    for (var i = 1; i <= 50; i++) {
        Pos += '<option value="' + i + '">' + i + '</option>';
    }
    $('#tblReg').DataTable({
        bProcessing: true,
        bLenghtChange: true,
        retrieve: true,
        bfilter: true,
        bSort: true,
        bPaging: true,
        data: response,
        columns: [
            {
                data: "lotNo", title: 'Lot No',
                render: function (data, type, row, meta) {
                    return '<p id="LotNo">' + row.lotNo + '</p>'
                }
            },


            {
                data: "position", title: 'Position',

                render: function (data, type, row, meta) {
                    if (row.testCode.length >= 1) {
                        // return row.pos1[0].calDetId
                        return '<select class="form-control form-select position" id="ddlPosition">' + Pos + '</select><input type="hidden" id="hdLotId" value="' + row.qcId + '"><input type="hidden" id="hdQcDetId" value="' + row.testCode[0].qcLotDetId + '">';
                    }
                    else {
                        return '<select class="form-control form-select position" id="ddlPosition">' + Pos + '</select><input type="hidden" id="hdLotId" value="' + row.qcId + '"><input type="hidden" id="hdQcDetId" value="">';
                    }
                }
            },
            {
                data: "testCode", title: 'TestCode ', "width": "100%",
                render: function (data, type, row, meta) {
                    var testCode = '<div id="divCalibrationTestCodeBind">';
                    for (var i = 0; i < row.testCode.length; i++) {
                        if (row.testCode[i].position > 0) {
                            testCode += '<div class="btn_Color btn rounded-pill" onclick="funBtnSelect(this)" accesskey="check" id="' + row.testCode[i].calId + '" style="background: #039b54;"><input type="hidden" id="hdQcDetId" value="' + row.testCode[i].calDetId + '"/><button type="button" class="">' + row.testCode[i].testCode + ' </button></div>'
                        }
                        else {
                            testCode += '<div class="btn_Color btn rounded-pill" onclick="funBtnSelect(this)" accesskey="" id="' + row.testCode[i].calId + '" style="background: rgb(112, 114, 116);"><input type="hidden" id="hdQcDetId" value="' + row.testCode[i].calDetId + '"/><button type="button" class="">' + row.testCode[i].testCode + ' </button></div>'
                        }
                    }
                    testCode += '</div>'

                    return testCode;
                }
            },



        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData.testCode.length > 0) {
                $("td #ddlPosition", nRow).val(aData.testCode[0].position);
                $("td #ddlConcentration", nRow).val(aData.testCode[0].conc);
                $("td #ddlContainerType", nRow).val(aData.testCode[0].containerTypeId);

            }
        }

    });
    $("#preloader").css("display", "none");

}


// following function not in use....to be deleted
function GetAllRegTray() { //GetAllSampleTray GetReagentPositions
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Schedule/GetReagentPositions', //GetAllSampleTray',
        success: function (result) {
            if (result != null) {

                if (result.RegPosition != null) {

                    if (result.RegPosition[i].position > 0) {
                        // QcNewRow();

                        $("#hdCalDetIdPos1" + (QcCounter - 1)).append('<option value="' + result.RegPosition[i].position + '">' + result.RegPosition[i].position + '</option>');
                        $("#hdCalDetIdPos1" + (QcCounter - 1)).val(result.RegPosition[i].position);

                        //GetMonthYear(QcCounter - 1);
                    }


                }
                else {
                    RegNewRow(); // QcNewRow();
                }
            }
            $("#preloader").css("display", "none");

        },

        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });

}

var RegTab = '';
var RegCounter = 0;
function RegNewRow() {
    if (RegCounter > 0 && $("#ddlRegLotNo" + (RegCounter - 1)).val() == "") {
        Message("warning", "Please select current row lot number");
        return false;
    }
    else {
        RegTab = $('#tblReg').DataTable({
            scroll: true, // used for fixed header
            bProcessing: true,
            bLenghtChange: true,
            retrieve: true,
            bfilter: true,
            bSort: true,
            autoWidth: false,
            destroy: true,
            "scrollCollapse": true,
            "paging": false,
            "jQueryUI": true,
            columnDefs: [{ type: 'hidden', 'targets': [0] }],
            order: [[0, 'desc']],
            dom: 'Bfrtip',
            buttons: [
                {
                    //className: 'btn btn-success Add Another row',
                    text: "<button type='button' class='btn btn-success' onclick='RegNewRow()' data-bs-toggle='tooltip' aria-label='Add New Reagent Position' data-bs-original-title='Add New Reagent Position'>Add New Reagent Position</button>",
                    action: function (e, dt, node, config) {
                    },
                    titleAttr: 'Add Another Row'
                },
                {
                    //className: 'btn btn-success Add Another row',
                    text: "<button type='button' class='btn btn-success' onclick='modelNewReagentLotNo()' data-bs-toggle='tooltip' aria-label='Add New Reagent Lot Number' data-bs-original-title='Add New Reagent Lot Number'>Add New Reagent Lot Number</button>",
                    action: function (e, dt, node, config) {
                    },
                    titleAttr: 'Add Another Row'
                },
                {
                    //className: 'btn btn-success Add Another row',
                    text: "<button type='button' class='btn btn-success' onclick='' data-bs-toggle='tooltip' aria-label='Barcode scan + Volume scan' data-bs-original-title='Barcode scan + Volume scan'>Barcode Scan</button>",
                    action: function (e, dt, node, config) {
                    },
                    titleAttr: 'Add Another Row'
                },
                {
                    //className: 'btn btn-success Add Another row',
                    text: "<button type='button' class='btn btn-success' onclick='' data-bs-toggle='tooltip' aria-label='Volume scan' data-bs-original-title='Volume scan'>Volume Scan</button>",
                    action: function (e, dt, node, config) {
                    },
                    titleAttr: 'Add Another Row'
                },
            ],



            //"scrollX": true,
            //"paging": false,
            //autoWidth: true,
            //destroy: true,
            //"searching": true,
            //ordering: true,
            //scrollY: 350,
            //scroller: {
            //    loadingIndicator: true
            //},
            //"scrollCollapse": true,
            //"paging": false,
            //"jQueryUI": true,
            //columnDefs: [{ type: 'hidden', 'targets': [1] }],
            //order: [[1, 'desc']],


            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });

        RegTab.row.add([
            '<div style="display:inline-flex"><a class="btn RegDeleteRow" onclick="RegDeleteRow(' + RegCounter + ')" style="color:red;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Reagent Lot"><b><i class="bi bi-trash"></i></b></a>&nbsp;<select id="ddlRegLotNo' + RegCounter + '" onchange="GetRegDetailByLotId(' + RegCounter + ')" name="ddlRgtLotNo" class="form-select select floating-label-control cd-form-control editable-select ddlRegLotNo">' + sessionStorage.getItem("ddlRegLotNo") + '</select><input type="hidden" id="hdRbtlId' + RegCounter + '" class="hdRbtlId"><div>',
            '<input type="hidden" value="' + RegCounter + '"><select class="form-control form-select ddlRegPosition"  id="ddlRegPosition' + RegCounter + '">' + sessionStorage.getItem("ddlRegPos") + '</select>',
            '<select id="ddlReagentType' + RegCounter + '" name="ddlReagentType" class="form-control form-select ddlReagentType"><option value="1">R1</option><option value="2">R2</option></select>',
            '<select id="ddlbottleType' + RegCounter + '" name="ddlbottleType" class="form-control form-select ddlbottleType">' + sessionStorage.getItem("ddlbottleType") + '</select>',
            '<div style="display:inline-flex;width:280px;!important"><select id="ddlRegExpMonth' + RegCounter + '" name="ddlRegExpMonth" class="form-control form-select ddlRegExpMonth">' + sessionStorage.getItem("ddlRegExpMonth") + '</select>&nbsp;<select id="ddlRegExpYr' + RegCounter + '" name="ddlRegExpYr" class="form-control form-select ddlRegExpYr">' + sessionStorage.getItem("ddlRegExpYr") + '</select></div>',
            '<div class="divRegTestCode" id="divRegTestCode' + RegCounter + '"></div>',
        ]).draw(false);



        if (RegCounter > 0) {
            for (var i = RegCounter; i >= 0; i--) {
                $('#ddlRegLotNo' + RegCounter + ' option[value="' + $('#ddlRegLotNo' + (i - 1)).val() + '"]').detach();
            }
        }

        $("#ddlRegLotNo" + RegCounter).select2({
            dropdownParent: $("#modelReagent"),
            width: "100%"
        });


        RegCounter++;
    }

}

function SaveReagent() {
    var reglist = [];
    var retunValue = true;

    $("#tblReg tr").each(function () {
        if (typeof ($($(this).closest('tr')).find('td').find('.ddlRegLotNo').val()) !== "undefined") {
            if ($($(this).closest('tr')).find('td').find('.ddlRegLotNo').val() != "" && ($($(this).closest('tr')).find('td').find('.ddlRegPosition').val() != "" && $($(this).closest('tr')).find('td').find('.ddlRegPosition').val() != 0)) {

                reglist.push({
                    RbtlId: $($(this).closest('tr')).find('td').find('.hdRbtlId').val(),
                    RlotId: $($(this).closest('tr')).find('td').find('.ddlRegLotNo').val(),
                    Position: $($(this).closest('tr')).find('td').find('.ddlRegPosition').val(),
                    RgtType: $($(this).closest('tr')).find('td').find('.ddlReagentType').val(),
                    BtlSize: $($(this).closest('tr')).find('td').find('.ddlbottleType').val(),
                    ExpMonth: $($(this).closest('tr')).find('td').find('.ddlRegExpMonth').val(),
                    ExpYear: $($(this).closest('tr')).find('td').find('.ddlRegExpYr').val(),
                })
            }
            else {
                retunValue = false;
            }
        }

    });
    if (retunValue == false) {
        Message("warning", "Please select other position");
        return false;
    }
    else {
        $("#preloader").css("display", "block");

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/Schedule/Savereagent',
            data: { rgtbotles: reglist },
            success: function (result) {
                if (result) {
                    Message("success", "Saved Data Successfully");
                    $("#modelReagent").modal('hide');
                    ClearReagentTray();

                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });

    }
}


function GetMonthYear() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/QualityControl/MonthYear',
        success: function (result) {
            if (result != null) {
                $("#ddlRegExpMonth").empty();
                $("#ddlRegExpYr").empty();

                const d = new Date();

                var month = '';
                for (var i = 0; i < result.months.length; i++) {

                    //if (result.months[i].monValue >= d.getMonth()) {
                    month += '<option value="' + result.months[i].monValue + '">' + result.months[i].monName + '</option>';
                    //}
                }

                var year = '';
                for (var i = 0; i < result.year.length; i++) {
                    //if (result.year[i] >= d.getFullYear()) {
                    year += '<option value="' + result.year[i] + '">' + result.year[i] + '</option>';
                    //}
                }

                $("#ddlRegExpMonth").append(month);
                $("#ddlRegExpYr").append(year);


                //sessionStorage.setItem("ddlRegExpMonth", ddlRegExpMonth);
                sessionStorage.setItem("ddlRegExpYr", ddlRegExpYr);

            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}


function saveReagentLotNo() {
    var RgtLotNoId = $("#hdRgtLotNoId").val();
    var RgtTestCode = $("#ddlRgtTestCode").val();
    var RgtLotNo = $("#txtRgtLotNo").val();
    var RgtManufacturer = $("#ddlRgtManufacturer").val();
    var RgtExpiryMonth = $("#ddlRgtExpiryMonth").val();
    var RgtExpiryYear = $("#ddlRgtExpiryYear").val();
    var returnValue = true;

    if (RgtTestCode == "") {
        $("#ddlRgtTestCode").focus();
        Message("warning", "Please select Test Code");
        returnValue = false;
    }
    else if (RgtLotNo == "") {
        $("#txtRgtLotNo").focus();
        Message("warning", "Please enter Lot Number");
        returnValue = false;
    }
    else if (RgtManufacturer == "") {
        $("#ddlRgtManufacturer").focus();
        Message("warning", "Please select Manufacturer");
        returnValue = false;
    }
    else if (RgtExpiryMonth == "") {
        $("#ddlRgtExpiryMonth").focus();
        Message("warning", "Please select Expiry Month");
        returnValue = false;
    }
    else if (RgtExpiryYear == "") {
        $("#ddlRgtExpiryYear").focus();
        Message("warning", "Please select Expiry Year");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            RlotId: RgtLotNoId,
            TestId: RgtTestCode,
            LotNo: RgtLotNo,
            MfgId: RgtManufacturer,
            ExpMonth: RgtExpiryMonth,
            ExpYear: RgtExpiryYear
        }

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/Schedule/saveReagentLotNo',
            data: obj,
            success: function (result) {
                if (result.returnValue) {
                    $("#ddlRegLotNo" + (RegCounter - 1)).append('<option value="' + result.rlotId + '">' + result.lotNo + '</option>');
                    Message("success", "Saved Data Successfully");
                    ClearReagentLotNo();
                }
                else {
                    Message("error", result.lotNo + " Lot Number is allready exist");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearReagentLotNo() {
    $("#hdRgtLotNoId").val('');
    $("#ddlRgtTestCode").val('');
    $("#txtRgtLotNo").val('');
    $("#ddlRgtManufacturer").val('');
    $("#ddlRgtExpiryMonth").val('');
    $("#ddlRgtExpiryYear").val('');
    $("#modelReagentNewLotNo").modal('hide');
}

// ------------------- Reagent code end here -------------------------




function GetQCDetailByLotId(counter) {
    if ($("#ddlQCLotNo" + counter).val() != null) {
        $("#preloader").css("display", "block");

        $.ajax({
            method: "GET",
            url: '/Schedule/GetQcByLotId?LotId=' + $("#ddlQCLotNo" + counter).val(),
            success: function (result) {
                if (result != null) {
                    if (result.qualtityControl.length > 0) {

                        if (result.qualtityControl[0].testCode.length > 0) {
                            $("#divQcTestCode" + counter).empty();
                            var testCode = result.qualtityControl[0].testCode;

                            $("#ddlQcPosition" + counter).val(testCode[0].position);
                            $("#hdQcContinerType_" + counter).val(testCode[0].concentration);
                            if (testCode[0].concentration == 1) {
                                $("#QcContinerType" + counter).text("High");
                            }
                            else if (testCode[0].concentration == 2) {
                                $("#QcContinerType" + counter).text("Normal");
                            }
                            else if (testCode[0].concentration == 3) {
                                $("#QcContinerType" + counter).text("Low");
                            }



                            var divTestCode = "";
                            for (var i = 0; i < testCode.length; i++) {
                                if (testCode[i].position > 0) {
                                    divTestCode += '<div class="btn_Color btn rounded-pill" onclick="funBtnSelect(this)" style="margin:4px; background:#039b54;"  accesskey="check" id="' + testCode[i].lotId + '" ><input type="hidden" id="hdQcDetId" class="hdQcDetId" value="' + testCode[i].qcLotDetId + '"/><button type="button" class="">' + testCode[i].testCode + ' </button></div>'
                                }
                                else {
                                    divTestCode += '<div class="btn_Color btn rounded-pill" onclick="funBtnSelect(this)" style="margin:4px; background:rgb(112, 114, 116);"  accesskey="" id="' + testCode[i].lotId + '"><input type="hidden" id="hdQcDetId" class="hdQcDetId" value="' + testCode[i].qcLotDetId + '"/><button type="button" class="">' + testCode[i].testCode + ' </button></div>'
                                }
                            }
                            $("#divQcTestCode" + counter).append(divTestCode);
                        }
                    }
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

/* following function added by jayashri - to fill reagent position and tests  - calling on change */
function GetRegDetailByLotId(counter) {
    if ($("#ddlRegLotNo" + counter).val() != null) {
        $("#preloader").css("display", "block");

        $.ajax({
            method: "GET",
            url: '/Schedule/GetRegByLotId?LotId= ' + $("#ddlRegLotNo" + counter).val(), // GetQcByLotId reagentData hello
            success: function (result) {
                if (result != null) {
                    $("#divRegTestCode" + counter).empty();
                    $("#divRegTestCode" + counter).append('<div class="btn_Color btn rounded-pill" onclick="funBtnSelect(this)" style="margin:4px; background:#039b54;"  accesskey="check" id="' + result.lotId + '" ><input type="hidden" id="hdRegDetId" class="hdRegDetId" value="' + result.lotId + '"/><button type="button" class="">' + result.testCode + ' </button></div>');
                    $("#ddlRegPosition" + counter).val(result.position);
                    $("#ddlRegExpMonth" + counter).val(result.expMonth);
                    $("#ddlRegExpYr" + counter).val(result.expYear);
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

// this function not in use
function BindCalTableData() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Schedule/GetAllSampleTray',
        success: function (result) {
            if (result != null) {
                if (result.calibration != null) {
                    var calibration = result.calibration;
                    onsuccessCalTable(result.calibration);
                    for (var i = 0; i < calibration.length; i++) {
                        if ((counter - 1) == i) {
                            $("#ddlCalLotNo" + (counter - 1)).val(calibration[i].lot).trigger('change');
                            $("#ddlCalPosition" + (counter - 1)).append('<option value="' + calibration[i].Pos + '">' + calibration[i].Pos + '</option>');
                            $("#ddlCalPosition" + (counter - 1)).val(calibration[i].Pos);
                            if ((calibration.length - 1) != i) {
                                CalNewRow();
                            }
                            break;
                        }
                    }
                    $(".divtblCalibration .dataTables_wrapper .").append('<button type="button" class="btn btn-success">Save</button>')
                    //$('#tblCalibration').on('search.dt', function (e, settings) {
                    //    Caltab.search('shivab').draw();
                    //})

                   // $('#tblCalibration .dataTables_filter input').val('shi');
                   // $("#tblCalibration_wrapper #tblCalibration_filter input").val(' ');
                   // Caltab.fnSort([[0, 'desc']]); // Sort by first column descending
                }
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });

    //Caltab.columns.adjust().draw();
    //$('#tblCalibration').DataTable().columns.adjust();
    //$('#tblCalibration').DataTable().responsive.recalc();

}


function GetCalDetailByLotId(counter) {
    if ($("#ddlCalLotNo" + counter).val() != null) {
        $("#preloader").css("display", "block");

        $.ajax({
            method: "GET",
            url: '/Schedule/GetCalibrationByLotId?LotId=' + $("#ddlCalLotNo" + counter).val(),
            success: function (result) {
                if (result != null) {
                    if (result.calibration.length > 0) {
                        // $("#ddlCalPosition" + counter).val(result.calibration[0].pos);

                        if (result.calibration[0].testCode.length > 0) {

                            $("#divCalTestCode" + counter).empty();
                            var testCode = result.calibration[0].testCode;
                            $("#ddlCalContinerType" + counter).val(testCode[0].containerTypeId);
                            var divTestCode = "";
                            for (var i = 0; i < testCode.length; i++) {

                                $("#ddlCalPosition" + (i + 1) + "_" + counter).css("display", "flex");
                                $("#ddlCalPosition" + (i + 1) + "_" + counter).append('<option value="' + testCode[i].position + '">' + testCode[i].position + '</option>');
                                $("#ddlCalPosition" + (i + 1) + "_" + counter).val(testCode[i].position)
                                $("#hdCalDetIdPos" + (i + 1) + "_" + counter).val(testCode[i].calDetId)

                                if (testCode[i].position > 0) {
                                    divTestCode += '<div class="btn_Color btn rounded-pill" onclick="funBtnSelect(this)" style="margin:4px; background:#039b54;"  accesskey="check" id="' + testCode[i].testCode + '" ><input type="hidden" id="hdQcDetId" value="' + testCode[i].testCode + '"/><button type="button" class="">' + testCode[i].testCode + ' </button></div>'
                                }
                                else {
                                    divTestCode += '<div class="btn_Color btn rounded-pill" onclick="funBtnSelect(this)" style="margin:4px; background:rgb(112, 114, 116);"  accesskey="" id="' + testCode[i].testCode + '"><input type="hidden" id="hdQcDetId" value="' + testCode[i].testCode + '"/><button type="button" class="">' + testCode[i].testCode + ' </button></div>'
                                }
                            }
                            $("#divCalTestCode" + counter).append(divTestCode);
                        }
                    }
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}



$(document).ready(function () {
    //$('#tblQC').on('click', '.QcDeleteRow', function () {

    //    var row_index = $(this).closest('td').parent()[0].sectionRowIndex;
    //    $("#modelDeleteConfirmationQCRow").modal("show");
    //    var data = $("#ddlQCLotNo" + row_index).select2('data')
    //    $("#lblDeleteMsg").text("Are you sure delete this " + data[0].text + " Lot Number ?")
    //    $("#hdDeleteQcLotId").val(data[0].text);
    //    $("#hdDeleteQcIndex").val(row_index);
    //});

    //// Reagent Lot 

    //$('#tblCalibration').on('click', '.CalDeleteRow', function () {
    //    var row_index = $(this).closest('td').parent()[0].sectionRowIndex;
    //    $("#modelDeleteConfirmationCalRow").modal("show");
    //    var data = $("#ddlCalLotNo" + row_index).select2('data')
    //    $("#lblDeleteCalMsg").text("Are you sure delete this " + data[0].text + " Lot Number ?")
    //    $("#hdDeleteCalLotId").val(data[0].id);
    //    $("#hdDeleteCalIndex").val(row_index);

    //    Caltab.row(row_index).remove().draw();
    //});

    //$('#tblReg').on('click', '.RegDeleteRow', function () {

    //    var row_index = $(this).closest('td').parent()[0].sectionRowIndex;
    //    $("#modelDeleteConfirmationRegRow").modal("show");
    //    var data = $("#ddlRegLotNo" + row_index).select2('data')
    //    $("#lblDeleteRegMsg").text("Are you sure delete this " + data[0].text + " Lot Number ?")
    //    $("#hdDeleteRegLotId").val(data[0].text);
    //    $("#hdDeleteRegIndex").val(row_index);
    //});

});

function CalDeleteRow(Counter) {
    $("#modelDeleteConfirmationCalRow").modal("show");
    var data = $("#ddlCalLotNo" + Counter).select2('data')
    $("#lblDeleteCalMsg").text("Are you sure delete this " + data[0].text + " Lot Number ?")
    $("#hdDeleteCalLotId").val(data[0].id);
    $("#hdDeleteCalIndex").val(Counter);

    //Caltab.row(row_index).remove().draw();
}

function QcDeleteRow(Counter) {
    $("#modelDeleteConfirmationQCRow").modal("show");
    var data = $("#ddlQCLotNo" + Counter).select2('data')
    $("#lblDeleteMsg").text("Are you sure delete this " + data[0].text + " Lot Number ?")
    $("#hdDeleteQcLotId").val(data[0].text);
    $("#hdDeleteQcIndex").val(Counter);
}
function RegDeleteRow(Counter) {
    $("#modelDeleteConfirmationRegRow").modal("show");
    var data = $("#ddlRegLotNo" + Counter).select2('data')
    $("#lblDeleteRegMsg").text("Are you sure delete this " + data[0].text + " Lot Number ?")
    $("#hdDeleteRegLotId").val(data[0].text);
    $("#hdDeleteRegIndex").val(Counter);
}


function DeleteQcRow() {
    var LotNo = $("#hdDeleteQcLotId").val();
    var Row_Index = $("#hdDeleteQcIndex").val();
    $("#modelDeleteConfirmationQCRow").modal("hide");


    if (LotNo != "") {
        $("#preloader").css("display", "block");

        QcTab.row(Row_Index).remove().draw();


        $.ajax({
            method: "GET",
            url: '/Schedule/DeleteQC?LotNo=' + LotNo,
            success: function (result) {
                if (result) {
                    Message("success", "successfull delete this row");
                    BindTableWorkList();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}


function DeleteCalRow() {
    var LotId = $("#hdDeleteCalLotId").val();
    var Row_Index = $("#hdDeleteCalIndex").val();
    $("#modelDeleteConfirmationCalRow").modal("hide");
    if (LotId != "") {
        Caltab.row(Row_Index).remove().draw();
        $("#preloader").css("display", "block");


        $.ajax({
            method: "GET",
            url: '/Schedule/DeleteCal?LotId=' + LotId,
            success: function (result) {
                if (result) {
                    Message("success", "successfull delete this row");
                    BindTableWorkList();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function DeleteRegRow() {
    var LotNo = $("#hdDeleteRegLotId").val();
    var Row_Index = $("#hdDeleteRegIndex").val();
    $("#modelDeleteConfirmationRegRow").modal("hide");


    if (LotNo != "") {
        $("#preloader").css("display", "block");
        RegTab.row(Row_Index).remove().draw();
        $.ajax({
            method: "GET",
            url: '/Schedule/DeleteReg?LotNo=' + LotNo, //DeleteQC
            success: function (result) {
                if (result) {
                    Message("success", "successfull delete this row");
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}





$(document).ready(function () {

    //Circle Create start

    //$(".CalInnerCircle .CalInnerLi4").css("background", "black");
    //$(".CalInnerCircle .CalInnerLi35").css("background", "blue");
    //$(".CalInnerCircle .CalInnerLi26").css("background", "green");


 
})


function GetAllSampleTray() {
    $("#preloader").css("display", "block");
    $.ajax({
        method: "GET",
        url: '/Schedule/GetAllSampleTray',
        success: function (result) {
            if (result != null) {
                if (result.calibration != null) {
                    for (var i = 0; i < result.calibration.length; i++) {
                        $(".SampleTrayDiv ul .SmpTrayLi_" + result.calibration[i].pos + " div div div").css("background", "Yellow");

                        if (result.calibration[i].pos > 0) {
                            CalNewRow();
                            $("#ddlCalLotNo" + (counter - 1)).val(result.calibration[i].lot).trigger('change');
                            //$("#ddlCalPosition" + (counter - 1)).append('<option value="' + result.calibration[i].pos + '">' + result.calibration[i].pos + '</option>');
                            //$("#ddlCalPosition" + (counter - 1)).val(result.calibration[i].pos); //////// RegNewRow
                        }
                    }
                }
                else {
                    CalNewRow();
                }

                if (result.sample != null) {
                    sessionStorage.setItem("SampleTray", result.sample);
                    for (var i = 0; i < result.sample.length; i++) {
                        $(".SampleTrayDiv ul .SmpTrayLi_" + result.sample[i].position + " div div div").css("background", "Red");
                    }
                }

                if (result.qualtityControl != null) {
                    for (var i = 0; i < result.qualtityControl.length; i++) {
                        $(".SampleTrayDiv ul .SmpTrayLi_" + result.qualtityControl[i].position + " div div div").css("background", "#57e957");
                        if (result.qualtityControl[i].position > 0) {
                            QcNewRow();
                            $("#ddlQCLotNo" + (QcCounter - 1)).val(result.qualtityControl[i].lotId).trigger('change');
                            $("#ddlQcPosition" + (QcCounter - 1)).append('<option value="' + result.qualtityControl[i].position + '">' + result.qualtityControl[i].position + '</option>');
                            $("#ddlQcPosition" + (QcCounter - 1)).val(result.qualtityControl[i].position);
                        }
                    }
                }
                else {
                    QcNewRow();
                }
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}


//Start Reagent Tray
var RgntNumberOfElement = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40];

var RgntInitialRender = RgntNumberOfElement.slice(0, 40);
function RgntRenderAngle(data) {
    var angles = [10];
    data.forEach(function (item, index) {
        angles.push((angles[index] + 9) % 360);      // TAKIT: Added modulo
    })
    return angles;
}
function RgntGenerateHtml() {
    var html = '';
    var angles = RgntRenderAngle(RgntInitialRender);
    angles.forEach(function (item, index) {
        if (typeof (RgntInitialRender[index]) == "undefined") {
            html += '<li id="RgtTray_' + RgntInitialRender[index] + '" class="CalLi CalInnerLi1" style="--webkit-transform: rotate(' + item + 'deg) skewY(-60deg);-ms-transform: rotate(' + item + 'deg) skewY(-60deg); transform: rotate(' + item + 'deg) skewY(-80deg); border-left: 1px solid;"><div class="CalInnerDiv" ><div class="divRgtcircle" style="-webkit-transform: rotate(-' + item + 'deg);" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="1">1</div></div></li>';
        }
        else {
            html += '<li id="RgtTray_' + RgntInitialRender[index] + '" class="CalLi CalInnerLi' + RgntInitialRender[index] + '" style="--webkit-transform: rotate(' + item + 'deg) skewY(-60deg);-ms-transform: rotate(' + item + 'deg) skewY(-60deg); transform: rotate(' + item + 'deg) skewY(-80deg); border-left: 1px solid;"><div class="CalInnerDiv" ><div class="divRgtcircle" style="-webkit-transform: rotate(-' + item + 'deg);" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="' + RgntInitialRender[index] + '">' + RgntInitialRender[index] + '</div></div></li>';
        }
    });
    document.querySelector('.CalInnerCircle').innerHTML = html; // TAKIT: Moved it here, after the loop
}

var RgntNumberOfElement2 = [41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80];
var RgntInitialRender2 = RgntNumberOfElement2.slice(0, 40);
function RgntGenerateHtml2() {
    var html = '';
    var angles = RgntRenderAngle(RgntInitialRender2);
    angles.forEach(function (item, index) {
        if (typeof (RgntInitialRender2[index]) == "undefined") {
            html += '<li class="CalLi CalOuterLi1" style="--webkit-transform: rotate(' + item + 'deg) skewY(-60deg);-ms-transform: rotate(' + item + 'deg) skewY(-60deg); transform: rotate(' + item + 'deg) skewY(-80deg);" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Outer div" ><div class="CalOuterDiv"><div  style="-webkit-transform: rotate(-' + item + 'deg);">41</div></div></li>';
        }
        else {
            html += '<li class="CalLi CalOuterLi' + RgntInitialRender2[index] + '" style="--webkit-transform: rotate(' + item + 'deg) skewY(-60deg);-ms-transform: rotate(' + item + 'deg) skewY(-60deg); transform: rotate(' + item + 'deg) skewY(-80deg);" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Outer Div" ><div class="CalOuterDiv"><div  style="-webkit-transform: rotate(-' + item + 'deg);">' + RgntInitialRender2[index] + '</div></div></li>';
        }
    });
    document.querySelector('.CalOuterCircle').innerHTML = html; // TAKIT: Moved it here, after the loop
}

//End Reagent Tray




//Start Sample Tray

var numberOfElement = [25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 25];
var initialRender = numberOfElement.slice(0, 24); // TAKIT: Modified to test
function SmpRenderAngle(data) {
    var smpAngles = [25];
    data.forEach(function (item, index) {
        smpAngles.push((smpAngles[index] + 14.4) % 360);      // TAKIT: Added modulo
    })
    return smpAngles;
}

function smpGenerateHtml() {
    var html = '';
    var angles = SmpRenderAngle(initialRender);
    angles.forEach(function (item, index) {
        if (typeof (initialRender[index]) == "undefined") {
            html += '<li class="SmpLi SmpTrayLi_1" style="--webkit-transform: rotate(' + item + 'deg) skewY(-60deg);-ms-transform: rotate(' + item + 'deg) skewY(-60deg); transform: rotate(' + item + 'deg) skewY(-80deg);"><div class="SmpOuterDiv"><div><div  style="-webkit-transform: rotate(-' + item + 'deg);" data-bs-toggle="tooltip" title="1" data-bs-placement="top">1</div></div></div></li>';
        }
        else {
            html += '<li class="SmpLi SmpTrayLi_1" style="--webkit-transform: rotate(' + item + 'deg) skewY(-60deg);-ms-transform: rotate(' + item + 'deg) skewY(-60deg); transform: rotate(' + item + 'deg) skewY(-80deg);"><div class="SmpOuterDiv"><div><div  style="-webkit-transform: rotate(-' + item + 'deg);" data-bs-toggle="tooltip" title="1" data-bs-placement="top" >' + initialRender[index] + '</div></div></div></li>';

            //  html += '<li class="SmpLi SmpTrayLi_' + initialRender[index] + '" style="--webkit-transform: rotate(' + item + 'deg) skewY(-60deg);-ms-transform: rotate(' + item + 'deg) skewY(-60deg); transform: rotate(' + item + 'deg) skewY(-80deg);" ><div class="SmpOuterDiv" ><div><div  style="-webkit-transform: rotate(-' + item + 'deg);">' + initialRender[index] + '</div></div></div></li>';
        }
    });
    document.querySelector('.SmpOuterCircle').innerHTML = html; // TAKIT: Moved it here, after the loop
}



var numberOfElement1 = [50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26];
var initialRender1 = numberOfElement1.slice(0, 24); // TAKIT: Modified to test

function SmpRenderAngle1(data) {
    var smpAngles = [25];
    data.forEach(function (item, index) {
        smpAngles.push((smpAngles[index] + 14.4) % 360);      // TAKIT: Added modulo
    })
    return smpAngles;
}

function smpGenerateHtml1() {
    var html = '';
    var angles = SmpRenderAngle1(initialRender1);
    angles.forEach(function (item, index) {
        if (typeof (initialRender1[index]) == "undefined") {
            html += '<li class="SmpLi SmpTrayLi_26" style="--webkit-transform: rotate(' + item + 'deg) skewY(-60deg);-ms-transform: rotate(' + item + 'deg) skewY(-60deg); transform: rotate(' + item + 'deg) skewY(-80deg);"><div class="SmpMiddleDiv"><div><div style="-webkit-transform: rotate(-' + item + 'deg);" data-bs-toggle="tooltip" title="AAA 26" data-bs-placement="top" >26</div></div></div></li>';
        }

        else {
            html += '<li class="SmpLi SmpTrayLi_' + initialRender1[index] + '" style="--webkit-transform: rotate(' + item + 'deg) skewY(-60deg);-ms-transform: rotate(' + item + 'deg) skewY(-60deg); transform: rotate(' + item + 'deg) skewY(-80deg);" ><div class="SmpMiddleDiv" ><div><div  style="-webkit-transform: rotate(-' + item + 'deg);" data-bs-toggle="tooltip" title="ABC" data-bs-placement="top">' + initialRender1[index] + '</div></div></div></li>';

            // html += '<li class="SmpLi SmpTrayLi_' + initialRender1[index] + '" style="--webkit-transform: rotate(' + item + 'deg) skewY(-60deg);-ms-transform: rotate(' + item + 'deg) skewY(-60deg); transform: rotate(' + item + 'deg) skewY(-80deg);"><div class="SmpMiddleDiv"><div><div style="-webkit-transform: rotate(-' + item + 'deg);" data-bs-toggle="tooltip" title="AAA 26" data-bs-placement="top" >' + initialRender1[index] + '</div></div></div></li>';
        }
    });
    document.querySelector('.SmpMiddleCircle').innerHTML = html; // TAKIT: Moved it here, after the loop
}

//var numberOfElement2 = [60, 59, 58, 57, 56, 55, 54, 53, 52, 51];
var numberOfElement2 = [51, 52, 53, 54, 55, 56, 57, 58, 59, 60];
var initialRender2 = numberOfElement2.slice(0, 9); // TAKIT: Modified to test
function SmpRenderAngle2(data) {
    var smpAngles = [10];
    data.forEach(function (item, index) {
        smpAngles.push((smpAngles[index] + 36) % 360);      // TAKIT: Added modulo
    })
    return smpAngles;
}

function smpGenerateHtml2() {
    var html = '';
    var angles = SmpRenderAngle2(initialRender2);
    angles.forEach(function (item, index) {
        if (typeof (initialRender2[index]) == "undefined") {
            html += '<li class="SmpLi SmpTrayLi_51" style="--webkit-transform: rotate(' + item + 'deg) skewY(-60deg);-ms-transform: rotate(' + item + 'deg) skewY(-60deg); transform: rotate(' + item + 'deg) skewY(-80deg);" ><div class="SmpInnerDiv"><div><div  style="-webkit-transform: rotate(-' + item + 'deg);" data-bs-toggle="tooltip" title="51" data-bs-placement="top" >60</div></div></div></li>';
        }
        else {
            html += '<li class="SmpLi SmpTrayLi_' + initialRender2[index] + '" style="--webkit-transform: rotate(' + item + 'deg) skewY(-60deg);-ms-transform: rotate(' + item + 'deg) skewY(-60deg); transform: rotate(' + item + 'deg) skewY(-80deg);" ><div class="SmpInnerDiv" ><div><div  style="-webkit-transform: rotate(-' + item + 'deg);" data-bs-toggle="tooltip" title="' + initialRender2[index] + '" data-bs-placement="top">' + initialRender2[index] + '</div></div></div></li>';
        }
    });
    document.querySelector('.SmpInnerCircle').innerHTML = html; // TAKIT: Moved it here, after the loop
}


//End Sample Tray

function funReagntOuterCrcl(ID) {

    $("#" + ID).css("background", "rgba(255, 0, 0, 0.2)");
    $("#" + ID).css("border", "1px solid");
    //$("#" + ID).text("5");
    //$("#" + ID).css("color", "black");
}

function funSampleOuterCrcl(ID) {
    $("#" + ID).css("background", "rgba(255, 0, 0, 0.2)");
    /* $("#" + ID).css("border", "1px solid");*/
}

function funPerentTab(Type) {
    if (Type == "WorkList-tab") {
        // $("#btnToggle").css("pointer-events", "visible");
        $("#btnReagent").css("display", "none");

    }
    else if (Type == "Instrument-tab") {
        //$("#btnToggle").css("pointer-events", "none");
        $("#btnReagent").css("display", "block");
    }
}


function modelNewReagentLotNo() {
    $("#modelReagentNewLotNo").modal("show");
}

function GetAllReagentTray() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Schedule/GetAllReagentTray',
        success: function (result) {
            if (result != null) {

                if (result.reagent != null) {
                    for (var i = 0; i < result.reagent.length; i++) {
                        if (result.reagent[i].rgtType == "1") {
                            $("#RgtTray_" + result.reagent[i].position).css("background", "skyblue");
                            $("#RgtTray_" + result.reagent[i].position + " div div").css("background", "skyblue");
                        }
                        else if (result.reagent[i].rgtType == "2") {
                            $("#RgtTray_" + result.reagent[i].position).css("background", "#eb6ce9");
                            $("#RgtTray_" + result.reagent[i].position + " div div").css("background", "#eb6ce9");
                        }
                        else {
                            $("#RgtTray_" + result.reagent[i].position).css("background", "#0ee20e4d");
                        }
                        if (result.reagent[i].position > 0) {
                            RegNewRow();
                            $("#hdRbtlId" + (RegCounter - 1)).val(result.reagent[i].rbtlId);
                            $("#ddlRegLotNo" + (RegCounter - 1)).val(result.reagent[i].rlotId).trigger('change');
                            $("#ddlRegPosition" + (RegCounter - 1)).append('<option value="' + result.reagent[i].position + '">' + result.reagent[i].position + '</option>');
                            $("#ddlRegPosition" + (RegCounter - 1)).val(result.reagent[i].position);
                            $("#ddlReagentType" + (RegCounter - 1)).val(result.reagent[i].rgtType);
                            $("#ddlbottleType" + (RegCounter - 1)).val(result.reagent[i].btlSize);
                            $("#ddlRegExpMonth" + (RegCounter - 1)).val(result.reagent[i].expMonth);
                            $("#ddlRegExpYr" + (RegCounter - 1)).val(result.reagent[i].expYear);

                            var TooltipData = "<b>Reagent</b>";
                            TooltipData += "<p>Lot No. : " + result.reagent[i].lotNo + "<br>";
                            TooltipData += "Expiry Date : " + result.reagent[i].expMonth + "/" + result.reagent[i].expYear + "<br>";
                            TooltipData += "Position : " + result.reagent[i].position + "<br>";
                            TooltipData += "Test Code : " + result.reagent[i].testCode + "<br>";
                            TooltipData += "Avaliable Volume : " + result.reagent[i].volume + " ml<br>";

                            if (result.reagent[i].position > 41) {
                                $("#RgtTray_" + result.reagent[i].position).attr("data-bs-original-title", TooltipData);
                                $("#RgtTray_" + result.reagent[i].position).attr("accesskey", "Rgt");

                            }
                            else {
                                $("#RgtTray_" + result.reagent[i].position + " div div").attr("data-bs-original-title", TooltipData);
                                $("#RgtTray_" + result.reagent[i].position + " div div").attr("accesskey", "Rgt");

                            }

                        }
                    }
                }
                else {
                    RegNewRow();
                }
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function ClearReagentTray() {
    /*    $('#tblReg').DataTable().destroy();*/
    $("#tblReg").empty();
    $(".divRgtcircle").each(function (index) {
        if ((index + 1) > 41) {
            $("#RgtTray_" + (index + 1)).css("background", "");
            $("#RgtTray_" + (index + 1) + " div div").attr("data-bs-original-title", (index + 1));
        }
        else {
            $("#RgtTray_" + (index + 1)).css("background", "");
            $("#RgtTray_" + (index + 1) + " div div").css("background", "");
            $("#RgtTray_" + (index + 1)).attr("data-bs-original-title", (index + 1));
        }

    });


    GetAllReagentTray();

}
//function funSamplePosition(this) {


//    alert($(this).offset().left + " , " + $(this).offset().top);
//}

