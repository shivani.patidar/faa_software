﻿$(document).ready(function () {
    GetAllRoles();

    // for search menue
    //GetMenuBind();
    //GetMenuBindHidden();
    //UserLogin();
});

function UserLogin() {
    var LoginId = $("#txtLogin_UserName").val();
    var Password = $("#txtLogin_Password").val();
    var returnValue = true;
    if (LoginId == "") {
        $("#txtLogin_UserName").focus();
        Message("error", "User Name is required");
        returnValue = false;
    }
    else if (Password == "") {
        $("#txtLogin_Password").focus();
        Message("error", "Password is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");
        $.ajax({
            method: "POST",
            url: '/Admin/UserLogin?LoginId=' + LoginId + '&&Password=' + Password,
            success: function (result) {
                if (result) {
                    Message("success", "Login Successfully");
                    window.location.href = "/DataDirectory/Index";

                    //GetMenuBind(); //....comment to be removed

                   // GetMenuBindHidden();
                   
                }
                else {

                    Message("error", "Invalid User name Or Password");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function UserLogout() {
    window.location.href = "/Admin/Logout";
    //$.ajax({
    //    method: "POST",
    //    url: '/Admin/Logout',
    //    success: function (result) {
    //    }, error: function () {

    //    }
    //});
}


function GetMenuBind_search() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Admin/MenuBind',
        success: function (result) {
            if (result != null) {
                sessionStorage.setItem("MenuBind", JSON.stringify(result));
                var data = "";

                //<ul id="components-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">


                for (var i = 0; i < result.length; i++) {
                    if (result[i].subMenu.length > 0) {

                        //data += '<li class="nav-item"><a class="nav-link collapsed" data-bs-target="#forms-nav-' + result[i].menuMappingId + '" data-bs-toggle="collapse" href="#" ><i class="bi bi-journal-text"></i><span>' + result[i].page + ' </span > <i class="bi bi-chevron-down ms-auto"></i></a><ul id="forms-nav-' + result[i].menuMappingId + '" class="nav-content collapse " data-bs-parent="#sidebar-nav">'

                        data += '<li class="nav-item"><a class="nav-link collapsed" data-bs-target="#forms-nav-' + result[i].menuMappingId + '" data-bs-toggle="collapse" href="#" ><i class="bi bi-journal-text"></i><span>' + result[i].page + ' </span > <i class="bi bi-chevron-down ms-auto"></i></a>'
                        data += '<ul id = "forms-nav-' + result[i].menuMappingId + '" class="nav-content collapse" data - bs - parent="#sidebar-nav" > '


                        //data += '<ul id="components-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">'; //newly added code

                        for (var j = 0; j < result[i].subMenu.length; j++) {
                            data += '<li><a href = "forms-elements.html"><i class="bi bi-circle"></i><span>' + result[i].subMenu[j].page + '</span></a></li>'
                        }
                        data += '</ul></li>';
                    }
                    else {
                        data += '<li class="nav-item" id="' + result[i].menuMappingId + '"><a class="nav-link collapsed" href = "pages-contact.html" ><i class="bi bi-envelope"> </i><span > ' + result[i].page + ' </span></a></li>';
                    }
                }
                $(".sidebar-nav").append(data);
                //window.location.href = "/DataDirectory/Index";

            }
            else {
                Message("error", "Invalid User name Or Password");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}


function GetAllRoles() {

    $.ajax({
        method: "GET",
        url: '/Admin/GetAllRole',
        crossDomain: true,
        success: function (result) {
            if (result != null) {
                var RoleId = $("#ddlRole").val();
                $("#ddlRole").empty();
                var data = '<option value"">Select Role</option>';
                for (var i = 0; i < result.length; i++) {
                    data += '<option value="' + result[i].roleId + '">' + result[i].role + '</option>'
                }
                $("#ddlRole").append(data);
                if (RoleId > 0) {
                    $("#ddlRole").val(RoleId);
                }
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}


function BindTableMenuRightByRoleId() {
    $("#preloader").css("display", "block");

    var RoleId = $("#ddlRole").val();
    if (RoleId > 0) {
        $("#btnUpdateRole").css("display", "block");
        $.ajax({
            url: "/Admin/GetAllMenuRight?RoleId=" + RoleId,
            method: 'get',
            success: OnsuccessMenuRight
        });
    }
    else {
        $("#btnUpdateRole").css("display", "none");
    }
}

function funmodelRole(Response) {
    if (Response == "Update") {
        $("#hdRole_Id").val($("#ddlRole").val());
        $("#txtRole_Name").val($('#ddlRole :selected').text());
        $("#btnRole").text('Update');
    }
    else {
        ClearRole();
    }
    $("#modelRole").modal('show');
}

function OnsuccessMenuRight(response) {
    $('#tblMenuRight').DataTable({
        //scrollX: true,
        //scroll: true, // used for fixed header
        //scrollY: 300, // used for fixed Hieght
        //bProcessing: true,
        //bLenghtChange: true,
        //retrieve: true,
        //bfilter: true,
        //bSort: true,
        //bPaging: true,
        data: response,
        columns: [
            {
                data: "page.Role", title: 'Page Name',
                render: function (data, type, row, meta) {
                    return '<p>' + row.page + '</p><input type="hidden" class="AccessId" value="' + row.accessId + '"><input type="hidden" class="RoleId" value="' + $("#ddlRole").val() + '"><input type="hidden" class="MenuMappingId" value="' + row.menuMappingId + '">';

                }
            },
            {
                data: "added", title: 'Add',
                render: function (data, type, row, meta) {
                    if (row.added == true) {
                        return '<input type="checkbox" class="larger Add" checked="checked" />';
                    }
                    else {
                        return '<input type="checkbox" class="larger Add" />';
                    }
                }
            },
            {
                data: "edited", title: 'Edit',
                render: function (data, type, row, meta) {
                    if (row.edited == true) {
                        return '<input type="checkbox" checked="checked" class="larger Edit" />';
                    }
                    else {
                        return '<input type="checkbox" class="larger Edit" />';
                    }
                }
            },
            {
                data: "viewed", title: 'View',
                render: function (data, type, row, meta) {
                    if (row.viewed == true) {
                        return '<input type="checkbox" class="larger View" checked="checked" />';
                    }
                    else {
                        return '<input type="checkbox" class="larger View" />';
                    }
                }
            },
            {
                data: "deleted", title: 'Delete',
                render: function (data, type, row, meta) {
                    if (row.deleted == true) {
                        return '<input type="checkbox" class="larger Delete" checked="checked" />';
                    }
                    else {
                        return '<input type="checkbox" class="larger Delete" />';
                    }
                }
            },

        ],

    });
    $("#preloader").css("display", "none");

}

function SaveMenuRight() {
    $("#preloader").css("display", "block");

    var list = [];
    $("#tblMenuRight tr").each(function () {
        //alert($($(this).closest('tr')).find('td').find('.MenuMappingId').val());
        if (typeof ($($(this).closest('tr')).find('td').find('.MenuMappingId').val()) !== "undefined") {
            list.push({
                AccessId: $($(this).closest('tr')).find('td').find('.AccessId').val(),
                RoleId: $($(this).closest('tr')).find('td').find('.RoleId').val(),
                MenuMappingId: $($(this).closest('tr')).find('td').find('.MenuMappingId').val(),
                Added: $($(this).closest('tr')).find('td').find('.Add').prop('checked'),
                Edited: $($(this).closest('tr')).find('td').find('.Edit').prop('checked'),
                Viewed: $($(this).closest('tr')).find('td').find('.View').prop('checked'),
                Deleted: $($(this).closest('tr')).find('td').find('.Delete').prop('checked')
            });
        }

    });
    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: '/Admin/SaveMenuRight',
        data: { model: list },
        success: function () {
            Message("success", "Saved Data Successfully");
            BindTableMenuRightByRoleId();
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function SaveRole() {
    var returnValue = true;
    if ($("#txtRole_Name").val() == "") {
        $("#txtRole_Name").focus();
        Message("error", "Role Name is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            RoleId: $("#hdRole_Id").val(),
            Role: $("#txtRole_Name").val(),
        }
        $.ajax({
            method: "POST",
            url: '/Admin/SaveRole',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result > 0) {
                    if ($("#hdRole_Id").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearRole();
                    GetAllRoles()
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearRole() {
    $("#hdRole_Id").val('');
    $("#txtRole_Name").val('');
    $("#btnRole").text('Save');
    $("#modelRole").modal('hide');
}



