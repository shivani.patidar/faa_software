﻿$(document).ready(function () {

    GetQCLotNo();

    $('#divQcLotNo .editable-select').editableSelect();
    $("#ddlTestId").select2({
        dropdownParent: $("#modelQCLot"),
        width: "100%"
    });

    //$("#ddlQCLot").select2({
    //    dropdownParent: $("#modelQCLot"),
    //    width: "100%"
    //});


    GetMonthYear();
    GetTestCode();
    GetMonthlyQCTestCode();
    BindTableQCLot();



    //GetDailyQCLot(); // Daily QC Lot No dropdown
    //GetMonthlyQCLot();
    //BindTableDailyQC();
    //BindTableMonthlyQC();
    //BindTableQCValuePrint(); // QC Value Print Table

});




//Start QC Lot

function GetQCLotNo() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/QualityControl/GetQCLot',
        success: function (result) {
            if (result != null) {
                //Qc Lot 
                $("#ddlQCLot").empty();

                //var ddlQCLot = '<option value=""></option>';
                //for (var i = 0; i < result.length; i++) {
                //    ddlQCLot += '<option value="' + result[i].lotId + '">' + result[i].lotNo + '</option>'
                //}
                //$(".es-list").append(ddlQCLot);
                //$('.editable-select').editableSelect();

                $("#divQcLotNo .es-list").empty();
                var calLotNo = '';
                for (var i = 0; i < result.length; i++) {
                    calLotNo += '<li class="es-visible">' + result[i].lotNo + '</li>';
                }
                $("#divQcLotNo .es-list").append(calLotNo);
                $('#divQcLotNo .editable-select').editableSelect();
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}



function Update() {
    $("#hdNewQCLotId").val($("#ddlQCLot").val());
    $("#txtNewQCLotNo").val($("#ddlQCLot").find("option:selected").text());
    $("#modelQCLotNo").modal('show');
}

//function saveQCLotNo() {
//    if ($("#txtNewQCLotNo").val() == "") {
//        $("#txtNewQCLotNo").focus();
//        Message("error", "Lot No is required");
//        return false;
//    }
//    else {
//        $("#preloader").css("display", "block");

//        $.ajax({
//            method: "GET",
//            url: '/QualityControl/DuplicateCheckQCLotNo',
//            data: { LotId: $("#hdNewQCLotId").val(), LotNo: $("#txtNewQCLotNo").val() },
//            crossDomain: true,
//            success: function (result) {
//                if (result) {
//                    if ($("#hdNewQCLotId").val() != "" && $("#txtNewQCLotNo").val() != "") {
//                        $("#ddlQCLot").select2('destroy');
//                        $("#ddlQCLot").find("option:selected").text($("#txtNewQCLotNo").val());
//                        $("#ddlQCLot").find("option:selected").val($("#hdNewQCLotId").val());
//                        $("#ddlTestId").select2({ dropdownParent: $("#modelQCLot"), width: "100%" });
//                        ClearLotNo();
//                    }
//                    else if ($("#txtNewQCLotNo").val() != "") {
//                        $("#ddlQCLot").append('<option value="' + $("#txtNewQCLotNo").val() + '" selected>' + $("#txtNewQCLotNo").val() + '</option>');
//                        // gTestCode = '<option value="' + $("#txtNewQCLotNo").val() + '" selected>' + $("#txtNewQCLotNo").val() + '</option>';
//                        ClearLotNo();
//                    }
//                    else {
//                        $("#txtNewQCLotNo").focus();
//                        Message("error", "Lot No is required");
//                        return false;
//                    }
//                }
//                else {
//                    $("#txtNewQCLotNo").focus();
//                    Message("error", "Lot No is already exist");
//                    return false;
//                }
//                $("#preloader").css("display", "none");

//            },
//            error: function (e) {
//                Message("warning", "Somthing went wrong");
//                $("#preloader").css("display", "none");

//            }
//        });
//    }


//}

function ClearLotNo() {
    $("#hdNewQCLotId").val('');
    $("#txtNewQCLotNo").val('');
    $("#modelQCLotNo").modal('hide');
}

function GetTestCode() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/QualityControl/GetTestCode',
        success: function (result) {
            if (result != null) {
                //Test Code 
                $("#ddlDailyQCTestcode").empty();

                var ddlTestCode = '<option value=""></option>';
                for (var i = 0; i < result.testCode.length; i++) {
                    ddlTestCode += '<option value="' + result.testCode[i].testId + '">' + result.testCode[i].testCode + '</option>'
                }

                $("#ddlDailyQCTestcode").append(ddlTestCode).select2();

                var ddlTestId = '<option value=""></option>';
                for (var i = 0; i < result.testCode.length; i++) {
                    ddlTestId += '<option value="' + result.testCode[i].testId + '">' + result.testCode[i].testCode + '</option>'
                }
                $("#ddlTestId").append(ddlTestId).select2();
                $("#ddlTestId").select2({
                    dropdownParent: $("#modelQCLot"),
                    width: "100%"
                });
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function GetMonthYear() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/QualityControl/MonthYear',
        success: function (result) {
            if (result != null) {
                $("#txtmonth").empty();
                $("#txtYear").empty();

                var month = '';
                for (var i = 0; i < result.months.length; i++) {
                    month += '<option value="' + result.months[i].monValue + '">' + result.months[i].monName + '</option>';
                }

                var year = '';
                for (var i = 0; i < result.year.length; i++) {
                    year += '<option value="' + result.year[i] + '">' + result.year[i] + '</option>';
                }

                $("#txtmonth").append(month);
                $("#txtYear").append(year);
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function GetMonthlyQCTestCode() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/QualityControl/GetTestCode',
        success: function (result) {
            if (result != null) {
                //Test Code 
                $("#ddlMonthlyQCTestcode").empty();

                var ddlTestCode = '<option value=""></option>';
                for (var i = 0; i < result.testCode.length; i++) {
                    ddlTestCode += '<option value="' + result.testCode[i].testId + '">' + result.testCode[i].testCode + '</option>'
                }


                $("#ddlMonthlyQCTestcode").append(ddlTestCode).select2();
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

// Start QC Lot
function BindTableQCLot() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/QualityControl/GetAllQCLot",
        method: 'get',
        success: OnsuccessQCLot
    });
}

function OnsuccessQCLot(response) {
    if (response.length > 0) {
        $("#divQcLot").css("display", "block");
        $("#divQcLotNoRecordFound").empty();

        $('#tblQCLot').DataTable().destroy();
        $('#tblQCLot').DataTable({
            /*
            dom: '<"testdiv">frtip',
            "columnDefs": [{
                className: "Name",
                "targets": [0],
                "visible": true,
                "searchable": true
            }],
            */
            scrollX: true,
            scroll: true, // used for fixed header
            scrollY: 900, // used for fixed header
            scrollCollapse: true, // used for fixed header
            bProcessing: true,
            bLenghtChange: true,
            retrieve: true,
            bfilter: true,
            bSort: true,
            bPaging: true,
            data: response,
            columns: [
                { data: 'lotNo', title: 'Lot Number' },
                { data: 'testCode', title: 'Test Code' },
                { data: 'unit', title: 'Unit' },
                { data: 'decimals', title: 'Decimals' },
                { data: 'mean', title: 'QC Mean Value' },
                { data: 'min', title: 'QC Min' },
                { data: 'max', title: 'QC Max' },
                { data: 'sd', title: 'SD Value' },
                {
                    data: "mean", title: '1SD Range',
                    render: function (data, type, row, meta) {
                        return (row.mean - row.sd) + "-" + (row.mean + row.sd);
                    }
                },
                {
                    data: "mean", title: '2SD Range',
                    render: function (data, type, row, meta) {
                        return (row.mean - (row.sd * 2)) + "-" + (row.mean + (row.sd * 2));
                    }
                },
                {
                    data: "mean", title: '3SD Range',
                    render: function (data, type, row, meta) {
                        return (row.mean - (row.sd * 3)) + "-" + (row.mean + (row.sd * 3));
                    }
                },
                {
                    data: "ExpMonth", title: 'Expiry Date',
                    render: function (data, type, row, meta) {
                        if (row.expMonth != "") {
                            if (row.expMonth < 10) {
                                return "0" + row.expMonth + "/" + row.expYear;
                            }
                            else {
                                return row.expMonth + "/" + row.expYear;
                            }

                        }

                    }
                },
                {
                    data: "lotId", title: 'Action',
                    render: function (data, type, row, meta) {
                        return '<div style="display: inline-flex;"><a class="" href="#" onclick = UpdateShowQCLot(' + row.lotId + ',' + row.testId + ') style = "color:#656FEA;width:20px" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update QC Lot"> <b><i class="bi bi-pencil-fill"></i></b></a > &nbsp;&nbsp;<a class="" href="#" onclick="DeleteConfirmationQCLot(' + row.lotId + ')" style="color:red;width:20px" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete QC Lot"><b><i class="bi bi-trash"></i></b></a></div > ';
                    }
                },
            ],


            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
        $("#preloader").css("display", "none");
    }
    else {
        $("#divQcLot").css("display", "none");
        $("#divQcLotNoRecordFound").empty();
        $("#divQcLotNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');

    }
    //$("div.testdiv").html('<div style="float: right"><button class="btn colorCode" ><i class="bi bi-plus" data-bs-toggle="tooltip" data-bs-placement="bottom" title="New QC Lot"></i>New QC Lot</button> </div>').insertNext(".dataTables_search");

}

function SaveQCLot() {
    var returnValue = true;
    var minval = $("#txtMin").val();
    var maxval = $("#txtMax").val();

    if ($("#ddlQCLot").val() == "") {
        $("#ddlQCLot").focus();
        Message("error", "QCLot is required"); //ddQCLot
        returnValue = false;
    }
    else if ($("#ddlConc").val() == "") {
        $("#ddlConc").focus();
        Message("error", "Concentration is required"); //ddQCLot
    }
    else {
        if (maxval <= minval && maxval != 0 && minval != 0) {
            $("#txtMin").focus();
            Message("error", "QC max value should be greater than min value."); //ddQCLot
            returnValue = false;
        }
        else {
            $("#preloader").css("display", "block");

            var lot = {
                LotId: $("#hdQCLotId").val(),
                LotNo: $("#ddlQCLot").val(),
                ExpMonth: $("#txtmonth").val(),
                Conc: $("#ddlConc").val(),
                ExpYear: $("#txtYear").val(),
                Mean: $("#txtMean").val(),
                Max: $("#txtMax").val(),
                Min: $("#txtMin").val(),
                Sd: ($("#txtMax").val() - $("#txtMin").val()) / 4,  //$("#txtSd").val(),
            }
            var qclotDet = {
                TestId: $("#ddlTestId").val(),
                Conc: $("#ddlConc").val(),
            }


            $.ajax({
                method: "POST",
                url: '/QualityControl/SaveQCLot',
                data: { lot: lot, qclotDet: qclotDet }
                ,
                success: function (result) {
                    if (result > 0) {
                        if ($("#hdUnt_Id").val() > 0) {
                            Message("success", "Updated Data Successfully");
                        }
                        else {
                            Message("success", "Saved Data Successfully");
                        }
                        ClearQCLot();
                        BindTableQCLot()
                    }
                    else {

                        Message("error", "Somthing went wrong");
                    }
                    $("#preloader").css("display", "none");

                },
                error: function (e) {
                    Message("warning", "Somthing went wrong");
                    $("#preloader").css("display", "none");

                }
            });
        }
    }//hello
}

function UpdateShowQCLot(Id, tid) {
    $("#preloader").css("display", "block");

    //alert(tid);
    $.ajax({
        method: "GET",
        url: '/QualityControl/GetByIdQCLot',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {

            if (result != null) {
                $("#hdQCLotId").val(result.lotId);

                $("#ddlTestId").val(result.testId).trigger('change');

                // $("#ddlTestId").val(tid.toString()).trigger('change');
                $("#ddlQCLot").val(result.lotNo).trigger('change');
                $("#ddlConc").val(result.conc);
                $("#txtmonth").val(result.expMonth);
                $("#txtyear").val(result.expYear);
                $("#txtMean").val(result.mean);
                $("#txtSd").val(result.sd);

                $("#txtMax").val(result.max);
                $("#txtMin").val(result.min);

                //Max: $("#txtMax").val(),
                //    Min: $("#txtMin").val(),

                //         Sd: ($("#txtMax").val() - $("#txtMin").val()) / 4,

                $("#btnQCLot").html("Update");
                $("#modelQCLot").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationQCLot(Id) {
    $("#modelDeleteConfirmationQCLot").modal('show');
    $("#hdDeleteQCLotId").val(Id);
}

function DeleteQCLot() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteQCLotId").val();
    $("#modelDeleteConfirmationQCLot").modal('hide');
    $("#hdDeleteQCLotId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/QualityControl/DeleteQCLot',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableQCLot();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

//ENd QC Lot

// Start Daily QC


function funShowQCDailyData(Type) {
    if (Type == "Table") {
        $("#divDailyQCTable").css("display", "flex");
        $("#divDailyQCChart").css("display", "none");
    }
    else if (Type == "Chart") {
        $("#divDailyQCTable").css("display", "none");
        $("#divDailyQCChart").css("display", "flex");
    }
}

function funShowQCMonthlyData(Type) {
    if (Type == "Table") {
        $("#divMonthlyQCTable").css("display", "block");
        $("#divMonthlyQCChart").css("display", "none");
    }
    else if (Type == "Chart") {
        $("#divMonthlyQCTable").css("display", "none");
        $("#divMonthlyQCChart").css("display", "block");
    }
}

function funQCTestCode(ddlTestCode, ddlLotno) {
    $("#preloader").css("display", "block");

    var TestId = $("#" + ddlTestCode).val();
    if (TestId > 0) {
        $.ajax({
            method: "GET",
            url: '/QualityControl/GetLotNoByTestId',
            data: { TestId: TestId },
            crossDomain: true,
            success: function (DailyQCLotNoType) {
                if (DailyQCLotNoType != null) {

                    $("#" + ddlLotno).empty();
                    var dailyQCLot = '<option value=""></option>';
                    for (var i = 0; i < DailyQCLotNoType.length; i++) {
                        dailyQCLot += '<option value="' + DailyQCLotNoType[i].lotNo + '">' + DailyQCLotNoType[i].lotNo + '</option>';
                    }
                    $("#" + ddlLotno).append(dailyQCLot);
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function GetDailyQCLot() {
    $("#preloader").css("display", "block");
    $.ajax({
        method: "GET",
        url: '/QualityControl/DailyQCLot',
        success: function (DailyQCLotNoType) {
            if (DailyQCLotNoType != null) {

                $("#ddlDailyQCLot").empty();
                var dailyQCLot = '<option value=""></option>';
                for (var i = 0; i < DailyQCLotNoType.length; i++) {
                    dailyQCLot += '<option value="' + DailyQCLotNoType[i].lotNo + '">' + DailyQCLotNoType[i].lotNo + '</option>';
                }
                $("#ddlDailyQCLot").append(dailyQCLot);
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

/* Get Lot No as per Test Code */


function funQCMonthlyTestCode() {
    $("#preloader").css("display", "block");
    var TestId = $("#ddlMonthlyQCTestcode").val();
    //alert(TestId);
    if (TestId > 0) {
        $.ajax({
            method: "GET",
            url: '/QualityControl/GetLotNoByTestId',
            data: { TestId: TestId },
            crossDomain: true,
            success: function (DailyQCLotNoType) {

                //console.log(ddlMonthlyQCLot);
                if (DailyQCLotNoType != null) {

                    $("#ddlMonthlyQCLot").empty();
                    var dailyQCLot = '<option value=""></option>';
                    for (var i = 0; i < DailyQCLotNoType.length; i++) {
                        dailyQCLot += '<option value="' + DailyQCLotNoType[i].lotNo + '">' + DailyQCLotNoType[i].lotNo + '</option>';
                    }
                    $("#ddlMonthlyQCLot").append(dailyQCLot);
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}


function funSerchDailyQC() {

    BindTableDailyQC();
}

function funSerchMonthlyQC() {

    BindTableMonthlyQC();
}

function funSerchQCPrintVal() {

    BindTableQCValuePrint();
}

function BindTableDailyQC() {
    //$("#preloader").css("display", "block");

    //alert($("#txtQCDailyDt").val
    var dtInput = $("#txtQCDailyDt").val();
    const atrArr = dtInput.split("-");
    var indate = atrArr[2].toString();
    var inmonth = atrArr[1].toString();
    var inyr = atrArr[0].toString();
    $.ajax({
        url: "/QualityControl/GetAllDailyQC?lotNo=" + $("#ddlDailyQCLot").find("option:selected").text() + "&&concentrationVal=" + $("#ddlDailyQCConcentration").val() + "&&rundateVal=" + $("#txtQCDailyDt").val(),
        method: 'get',
        success: OnsuccessDailyQC
    });
}

function OnsuccessDailyQC(response) {
    if (response.length > 0) {
        $("#divDailyQcNoRecordFound").empty();
        var result = [];
        var SD = response[0].sd;
        var Mean = response[0].mean;

        CreateLineGraph("LineChartDailyQC", "Daily QC", "#3b8bba", response, SD, Mean);

        $("#divDailyQCTableChart").css("display", "block");
        $('#tblDailyQc').DataTable().destroy();

        $('#tblDailyQc').DataTable({
            scrollX: true,
            scroll: true, // used for fixed header
            // scrollY: 300, // used for fixed Hieght
            bProcessing: true,
            bLenghtChange: true,
            //retrieve: true,
            //bfilter: true,
            //bSort: true,
            //bPaging: true,
            //dom: '<"dt-buttons"Bf><"clear">lirtp',
            dom: 'Bfrtip',

            data: response,
            columns: [

                { data: 'testCode', title: 'Test Code' },
                {
                    data: "rundat", title: 'Run Date Time',
                    render: function (data, type, row, meta) {
                        var RDate = "";
                        RDate = moment(row.rundate).format("DD MMM YYYY, hh:mm:ss a");
                        return RDate;
                    }
                },

                { data: 'mean', title: 'QC Mean' },
                {
                    data: 'resultValue', title: 'Result',
                    render: function (data, type, row, meta) {
                        result.push(row.resultValue);
                        return row.resultValue;
                    }
                },
                {
                    data: 'sd', title: 'SD Value',
                    render: function (data, type, row, meta) {
                        return row.sd;
                    }
                },
                {
                    data: "mean", title: 'SD Range',
                    render: function (data, type, row, meta) {
                        return (row.mean - row.sd) + "-" + (row.mean + row.sd);
                    }
                },
                { data: 'flag', title: 'Flag' },
                { data: 'sd', title: 'QC Rules' },
                {
                    data: "testCode", title: 'Action',
                    render: function (data, type, row, meta) {
                        return '<a class="" href="#" onclick="DeleteConfirmationDailyQCLot(' + row.lotId + ')" style="color:red;"><b><i class="bi bi-trash"></i></b></a>';
                    }
                },
            ],
            buttons: [
                {
                    text: 'Print PDF',
                    extend: 'pdfHtml5',
                    download: 'open',
                    filename: 'Daily QC',
                    orientation: 'portrait', //portrait
                    pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                    exportOptions: {
                        columns: ':visible',
                        search: 'applied',
                        order: 'applied'
                    },

                    customize: function (doc) {
                        /*  var pdf = new jsPDF();*/
                        var chartTheme = chart.get("theme");
                        chart.set("theme", "light");
                        $("#LineChartDailyQC .canvasjs-chart-container .canvasjs-chart-canvas").css("width","700px;")
                        var canvas = $("#LineChartDailyQC .canvasjs-chart-canvas").get(0);
                        var dataURL = canvas.toDataURL();
                        chart.set("theme", chartTheme);

                        //pdf.addImage(dataURL, 'JPEG', 0, 0);
                        //pdf.save("download.pdf");

                        //Remove the title created by datatTables
                        //doc.content.splice(0, 1);
                        doc.content.splice(0, 1,
                            {
                                columns: [
                                    {
                                        //   margin: [0, 12, 0, 0],
                                        alignment: 'left',
                                        text: 'Test Code : ' + $("#ddlDailyQCTestcode option:selected").text()
                                    },
                                    {
                                        //   margin: [0, 12, 0, 0],
                                        alignment: 'left',
                                        text: 'QC Lot : ' + $("#ddlDailyQCLot option:selected").text()
                                    },
                                    {
                                        alignment: 'left',
                                        text: 'Concentration : ' + $("#ddlDailyQCConcentration option:selected").text(),
                                        // margin: [0, 12, 0, 0],
                                    },
                                    {
                                        alignment: 'right',
                                        text: 'Run Date : ' + moment($("#txtQCDailyDt").val()).format('DD MMM YYYY'),
                                        //   margin: [0, 12, 0, 0],
                                    }
                                ],
                            },
                            {
                                margin: [10, 12, 10, 10],
                                alignment: 'center',
                                image: dataURL,
                                fit:[550,350]
                            },
                        );

                        //Create a date string that we use in the footer. Format is dd-mm-yyyy
         
                        // doc.addImage(dataURL, 'JPEG', 0, 0);
                        var logo = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAICAgICAQICAgIDAgIDAwYEAwMDAwcFBQQGCAcJCAgHCAgJCg0LCQoMCggICw8LDA0ODg8OCQsQERAOEQ0ODg7/2wBDAQIDAwMDAwcEBAcOCQgJDg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg7/wAARCAAwADADASIAAhEBAxEB/8QAGgAAAwEAAwAAAAAAAAAAAAAABwgJBgIFCv/EADUQAAEDAgQDBgUDBAMAAAAAAAECAwQFBgAHESEIEjEJEyJBUXEUI0JhgRVSYhYXMpEzcrH/xAAYAQADAQEAAAAAAAAAAAAAAAAEBQYHAv/EAC4RAAEDAgMGBQQDAAAAAAAAAAECAxEABAUGEhMhMUFRcSIyYaHBFkKB0ZGx8P/aAAwDAQACEQMRAD8Avy44hlhTrqw22kEqUo6BIG5JPkMSxz67RlFPzFquWnDParOaN4QVlmqXDKcKKLS19CCsf8qh6A6e+OfaK573LDTanDJllVV0q8r3ZVIuGqR1fMpdJSdHCCOinN0j7e+FjymydjRKdSbGsikpbSlG5O3/AHfeX5nU6knck6DFdg+DovkquLlWllHE8yeg+f4FBPvluEpEqNC657/4yr4ecm3ZxH1OghzxfptpQERI7X8QrqdPXGNpucXGLltU0SbZ4jazW0tHX4C6IiJcd37HUEj8YoHNtTKOzwuHVPj79rTfhkfCudxEbUOqQQd9Pc4HlaoGRt2JVAcptRsOe54WZZkd6yFHpzakgD3098ahYWuVVDQ/YrKD9wJnvGqfb8UAHH584npWw4eu0+iVO+6Vl3xO2zHy1uKa4GafdcBwqos5w7AOE6lgk+epT68uK8MvNPxmnmHEvMuJCm3EKCkqSRqCCNiCPPHmbzdyWcozkq1rpitVSkzGyqHNbT4HU+S0H6Vp22/9Bw8XZkcQ1wuzLg4V8yqq5U69a0X42zalJXq5NpeuhZJO5LWo0/idPpxI5ryszgyG77D3Nrau+U8weh/cDgQRI3sGXi54VCCKXK6Ku5fnbOcTt2znO/8A0SfFtymcx17llpGqgPTUjDj5WOIOUmYFPpLgjXQ5ES627r43I6R40I9D16fuGEfzPZeyq7afiRtec0W03O/GuSj82wdbdb8ZB89FEjb0xvrIzGk2pmnSrgcdUttl3lkoB2UyrZadPbf8DFFhGHuX+W0bASUyY6kKJg96XPK0XJmt9MrkFuIQw2XNup8IwFbruVaWXkttMgadCCcEfNuPTbbzPkiK87+jVRsTqctlIKVNubkD2J/0RgBVFDVQUpTTEksjdTjpG4xc4TYOvBu5AhB3yf8AcfmgTIUUmiMxcs27+CG42Koy3JqFqym3YLytebuVfRr9gVD2AwvOWt5u2f2qXDle0FK4UhVwijzgFbPMSUlBSftqdcMAqN/TfCVV0yGBDl3O+huMwvZXw6Oqzr67n8jC85VWw/fnakZD2tAaL/wtwGsSuTfu2YyCeY+6ikY5x1yzVlDECB4C8Nn3lEx6SFe9MWtW3R1jfVTu0l4a7lv6wbaz8yqp6p2Z2X6FmXT2U6uVelq8TrQA3UtG6gPMFQG+mJe2Xf8ASL5s1qp0p35qfDLhuHR2M4P8kLT5aH/ePUSpIUnQjUemJh8SXZs2fmVf8/MvJevKyfzNkEuTPhGeamVNZ3JeZGnKonqpPXqQTjE8tZmdwF4hSdbSjvHMHqP1zo24tw8J4EUn9MvWz7iymo9tX27PgTqQ4tMCfGY735SuiFdenTTTyGOIrGV1DSJLCqndb7Z1aamIDEZJHQqGg5vyDga3Fw28bVhS1wqrlHAzAjtkhFSt2sIQHR5HkXoQftjrqJw5cYt81BESDkuxaCVnRU24K0Fpb+/I3qT7Y1b6kygptSi88lKiSWxIEkyRygE8tUUDsbieA71mM2M0mZxlVytTQ0w0jkQlIIQ2PpabR1JJ6Abk4oP2bHDhW6O9WuITMKlLplxV9hMeg06Sn5lPgjdIUPJayedX4HljvOHvs16VbF7Uy/c86/8A3DuyIoOwoAaDdPgL66ts7gqH7lan2xVaJEjQaezFiMIjx2khLbaBoEgYyzMmZTjWi2t0bK3b8qfk+v8AW/jNMGWdn4lGVGv/2SAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA=';
                        doc.pageMargins = [20, 80, 20, 60];
                        // Set the font size fot the entire document
                        doc.defaultStyle.fontSize = 10;
                        // Set the fontsize for the table header
                        doc.styles.tableHeader.fontSize = 13;
                        // doc.content[2].table.widths = ['*', '15%', '15%', '10%', '15%', '10%', '5%', '10%', '10%'];
                        // Create a header object with 3 columns
                        // Left side: Logo
                        // Middle: brandname
                        // Right side: A document title
                        doc['header'] = (function () {
                            return {
                                columns: [
                                    //{
                                    //    image: dataURL,
                                    //    /* width: 80*/
                                    //},
                                    {
                                        alignment: 'center',
                                        italics: true,
                                        text: 'Daily QC ',
                                        fontSize: 18,
                                        // margin: [10, 0]
                                    },
                                    //{
                                    //    alignment: 'right',
                                    //    fontSize: 14,
                                    //    text: 'Test Code : ' + $("#ddlTestCode option:selected").text()
                                    //},
                                    //{
                                    //    alignment: 'right',
                                    //    fontSize: 14,
                                    //    text: '   Curve Type : ' + $("#ddlCalibrationCurveType option:selected").text()
                                    //}
                                ],
                                margin: 20
                            }
                        });

                        // Create a footer object with 2 columns
                        // Left side: report creation date
                        // Right side: current page and total pages

                        doc['footer'] = (function (page, pages) {
                            return {
                                columns: [
                                    {
                                        alignment: 'left',
                                        //  text: 'Report Date : 12/12/2023'
                                        text: ['Report Date : ', { text: moment(new Date()).format('DD MMM YYYY').toString() }]
                                    },
                                    {
                                        alignment: 'center',
                                        text: ['This test has been conducted on FAA 200.']
                                    },
                                    {
                                        alignment: 'right',
                                        text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                    }
                                ],
                                margin: 20
                            }
                        });

                       

                        // Change dataTable layout (Table styling)
                        // To use predefined layouts uncomment the line below and comment the custom lines below
                        // doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
                        var objLayout = {};
                        objLayout['width'] = function (i) { return 800; };
                        objLayout['hLineWidth'] = function (i) { return .5; };
                        objLayout['vLineWidth'] = function (i) { return .5; };
                        objLayout['hLineColor'] = function (i) { return '#aaa'; };
                        objLayout['vLineColor'] = function (i) { return '#aaa'; };
                        objLayout['paddingLeft'] = function (i) { return 4; };
                        objLayout['paddingRight'] = function (i) { return 4; };
                        doc.content[0].layout = objLayout;
                    }
                }],
        });

        funShowQCDailyData($('input[name="chkDailyQCTableChart"]:checked').val());
        $(".buttons-pdf").css("background", "#656FEA");
        $(".buttons-pdf").css("color", "white");

    }
    else {
        $(".divLineChartDailyQC").empty();
        $("#divDailyQCTableChart").css("display", "none");
        $("#divDailyQcNoRecordFound").empty();
        $("#divDailyQcNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
}
var chart = "";

function CreateLineGraph(LineChartId, Label, Color, Result, SD, Mean) {
    $(".div" + LineChartId).empty();
    $(".div" + LineChartId).append('<div id="' + LineChartId + '" style="height: 300px; width: 100%;"></div>');
    var rst = [];
    var sumResult = 0;
    for (var i = 0; i < Result.length; i++) {
        rst.push({ x: (i + 1), y: Result[i].resultValue });
        sumResult += Result[i].resultValue;
    }
    var ValMean = (sumResult / Result.length).toFixed(2);

    var SDprep = 0;
    for (var i = 0; i < Result.length; i++) {
        SDprep += Math.pow(parseFloat(Result[i].resultValue) - ValMean, 2);
    }
   
    var ValSD =  Math.sqrt(SDprep / (Result.length - 1)).toFixed(2);
    if (isNaN(ValSD)) {
        ValSD = 0;
    }
    var ValCV =  Number(((ValSD / ValMean) * 100)).toFixed(2);
    if (isNaN(ValCV)) {
        ValCV = 0;
    }
    chart = new CanvasJS.Chart(LineChartId, {
        animationEnabled: true,
        title: {
            label: "Daily QC",
            text: "N=" + Result.length + "; Mean=" + ValMean + "; SD=" + ValSD + "; CV=" + ValCV + ";",
            fontSize: 15,
            fontFamily: "tahoma",
        },
        theme: "light1",
        axisX: {
            interval: 1,
        },
        axisY: {
            title: "Result",
            gridThickness: 0,
            //minimum: -3,
            //maximum: 13,
            //valueFormatString: "#0,,.",
            //suffix: "mn",
            stripLines: [
                {
                    value: Mean,
                    label: "Mean",
                    color: "green",
                    labelFontColor: "green",
                },
                {
                    value: (3 * SD) + Mean,
                    label: "3SD",
                    color: "red",
                    labelFontColor: "red",
                },
                {
                    value: (2 * SD) + Mean,
                    label: "2SD",
                    color: "red",
                    labelFontColor: "red",
                },

                {
                    value: (1 * SD) + Mean,
                    label: "1SD",
                    color: "red",
                    labelFontColor: "red",
                },

                {
                    value: (-1 * SD) + Mean,
                    label: "-1SD",
                    color: "red",
                    labelFontColor: "red",
                },

                {
                    value: (-2 * SD) + Mean,
                    label: "-2SD",
                    color: "red",
                    labelFontColor: "red",
                },

                {
                    value: (-3 * SD) + Mean,
                    label: "-3SD",
                    color: "red",
                    labelFontColor: "red",
                }
            ],
        },
        toolTip: {
            shared: true
        },
        /*      legend: false,*/
        //legend: {
        //    cursor: "pointer",
        //    verticalAlign: "top",
        //    horizontalAlign: "center",
        //    dockInsidePlotArea: true,
        //    itemclick: toogleDataSeries
        //},
        note: {

        },
        data: [{
            type: "spline",
            indexLabelFontSize: 16,
            showInLegend: false,
            //visible: false,
            //yValueFormatString: "##.00mn",
            name: "Result",
            dataPoints: rst,
        },
        ]
    });
    chart.render();
    chart.axisY[0].set("minimum", (-4 * SD) + Mean);
    chart.axisY[0].set("maximum", (4 * SD) + Mean);

    function toogleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        chart.render();
    }
}

function BindTableMonthlyQC() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/QualityControl/GetAllMonthlyQC?lotNo=" + $("#ddlMonthlyQCLot").find("option:selected").text() + "&&concentrationVal=" + $("#ddlMonthlyQCConcentration").val() + "&&rundateVal=" + $("#txtQCMonthlyDt").val(),
        method: 'get',
        success: OnsuccessMonthlyQC
    });
}

function OnsuccessMonthlyQC(response) {
    if (response.length > 0) {
        $("#divMonthlyQcNoRecordFound").empty();
        var result = [];
        var SD = response[0].sd;
        var Mean = response[0].mean;
        CreateLineGraph("LineChartMonthlyQC", "Monthly QC", "#3b8bba", response, SD, Mean);

        $("#divMonthlyQCTableChart").css("display", "block");
        $('#tblMonthlyQc').DataTable().destroy();
        $('#tblMonthlyQc').DataTable({
            scrollX: true,
            scroll: true, // used for fixed header
            //scrollY: 300, // used for fixed Hieght
            bProcessing: true,
            bLenghtChange: true,
            //dom: '<"dt-buttons"Bf><"clear">lirtp',
            dom: 'Bfrtip',
            data: response,
            columns: [

                { data: 'testCode', title: 'Test Code' },
                //{ data: 'rundat', title: 'Run Datetime' },
                {
                    data: "rundat", title: 'Run Date',
                    render: function (data, type, row, meta) {
                        if (row.rundate != null) {
                            return moment(row.rundate).format("DD MMM YYYY");
                        }
                        else {
                            return ' ';
                        }
                    }
                },
                { data: 'mean', title: 'QC Mean Value' },
                { data: 'resultValue', title: 'Result' },
                { data: 'sd', title: 'SD Value' },
                {
                    data: "mean", title: 'SD Range',
                    render: function (data, type, row, meta) {
                        return (row.mean - row.sd) + "-" + (row.mean + row.sd);
                    }
                },
                { data: 'flag', title: 'Flag' },
                { data: 'sd', title: 'QC Rules' },
                {
                    data: "ExpMonth", title: 'Expiry Date',
                    render: function (data, type, row, meta) {
                        if (row.expMonth != "") {
                            if (row.expMonth < 10) {
                                return "0" + row.expMonth + "/" + row.expYear;
                            }
                            else {
                                return row.expMonth + "/" + row.expYear;
                            }

                        }

                    }
                },

            ],
            buttons: [
                {
                    text: 'Print PDF',
                    extend: 'pdfHtml5',
                    download: 'open',
                    filename: 'Monthly QC',
                    orientation: 'portrait', // landscape
                    pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                    exportOptions: {
                        columns: ':visible',
                        search: 'applied',
                        order: 'applied'
                    },
                    customize: function (doc) {
                        /*  var pdf = new jsPDF();*/
                        var chartTheme = chart.get("theme");
                        chart.set("theme", "light");
                        var canvas = $("#LineChartMonthlyQC .canvasjs-chart-canvas").get(0);
                        var dataURL = canvas.toDataURL();
                        chart.set("theme", chartTheme);
                        //pdf.addImage(dataURL, 'JPEG', 0, 0);
                        //pdf.save("download.pdf");


                        //Remove the title created by datatTables
                        // doc.content.splice(0, 1);
                        doc.content.splice(0, 1,
                            {
                                columns: [
                                    {
                                        //   margin: [0, 12, 0, 0],
                                        alignment: 'left',
                                        text: 'Test Code : ' + $("#ddlMonthlyQCTestcode option:selected").text()
                                    },
                                    {
                                        //   margin: [0, 12, 0, 0],
                                        alignment: 'left',
                                        text: 'QC Lot : ' + $("#ddlMonthlyQCLot option:selected").text()
                                    },
                                    {
                                        alignment: 'left',
                                        text: 'Concentration : ' + $("#ddlMonthlyQCConcentration option:selected").text(),
                                        // margin: [0, 12, 0, 0],
                                    },
                                    {
                                        alignment: 'right',
                                        text: 'Run Date : ' + moment($("#txtQCMonthlyDt").val()).format('DD MMM YYYY'),
                                        //   margin: [0, 12, 0, 0],
                                    }
                                ],
                            },
                            {
                                margin: [0, 12, 0, 10],
                                alignment: 'center',
                                image: dataURL,
                                fit: [550, 350]

                            },

                        );
                        //Create a date string that we use in the footer. Format is dd-mm-yyyy


                        doc.pageMargins = [20, 80, 20, 60];
                        // Set the font size fot the entire document
                        doc.defaultStyle.fontSize = 10;
                        // Set the fontsize for the table header
                        doc.styles.tableHeader.fontSize = 13;
                        //doc.content[0].table.widths = ['*', '15%', '15%', '10%', '15%', '10%', '5%', '10%', '10%'];
                        // Create a header object with 3 columns
                        // Left side: Logo
                        // Middle: brandname
                        // Right side: A document title
                        doc['header'] = (function () {
                            return {
                                columns: [
                                    //{
                                    //    image: dataURL,
                                    //    /* width: 80*/
                                    //},
                                    {
                                        alignment: 'center',
                                        italics: true,
                                        text: 'Monthly QC ',
                                        fontSize: 18,
                                        // margin: [10, 0]
                                    },
                                    //{
                                    //    alignment: 'right',
                                    //    fontSize: 14,
                                    //    text: 'Test Code : ' + $("#ddlTestCode option:selected").text()
                                    //},
                                    //{
                                    //    alignment: 'right',
                                    //    fontSize: 14,
                                    //    text: '   Curve Type : ' + $("#ddlCalibrationCurveType option:selected").text()
                                    //}
                                ],
                                margin: 20
                            }
                        });

                        // Create a footer object with 2 columns
                        // Left side: report creation date
                        // Right side: current page and total pages

                        doc['footer'] = (function (page, pages) {
                            return {
                                columns: [
                                    {
                                        alignment: 'left',
                                        //  text: 'Report Date : 12/12/2023'
                                        text: ['Report Date : ', { text: moment(new Date()).format('DD MMM YYYY').toString() }]
                                    },
                                    {
                                        alignment: 'center',
                                        text: ['This test has been conducted on FAA 200.']
                                    },
                                    {
                                        alignment: 'right',
                                        text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                    }
                                ],
                                margin: 20
                            }
                        });




                        // Change dataTable layout (Table styling)
                        // To use predefined layouts uncomment the line below and comment the custom lines below
                        // doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
                        var objLayout = {};
                        objLayout['width'] = function (i) { return 800; };
                        objLayout['hLineWidth'] = function (i) { return .5; };
                        objLayout['vLineWidth'] = function (i) { return .5; };
                        objLayout['hLineColor'] = function (i) { return '#aaa'; };
                        objLayout['vLineColor'] = function (i) { return '#aaa'; };
                        objLayout['paddingLeft'] = function (i) { return 4; };
                        objLayout['paddingRight'] = function (i) { return 4; };
                        doc.content[0].layout = objLayout;
                    }
                }],
        });
        funShowQCMonthlyData($('input[name="chkMonthlyQCTableChart"]:checked').val());
        $(".buttons-pdf").css("background", "#656FEA");
        $(".buttons-pdf").css("color", "white");

        $("#preloader").css("display", "none");

    }

    else {
        $(".divLineChartMonthlyQC").empty();
        $("#divMonthlyQCTableChart").css("display", "none");
        $("#divMonthlyQcNoRecordFound").empty();
        $("#divMonthlyQcNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
        $("#preloader").css("display", "none");

    }
}


function MonthlyprintData() {
    var pdf = new jsPDF('p', 'pt', 'a3', true);
    var chartTheme = chart.get("theme");
    chart.set("theme", "light2");
    var canvas = $("#LineChartMonthlyQC .canvasjs-chart-canvas").get(0);
    var dataURL = canvas.toDataURL();
    chart.set("theme", chartTheme);
    pdf.addImage(dataURL, 'JPEG', 0, 0);
    pdf.save("download.pdf");


}

function BindTableQCValuePrint() {
    $("#preloader").css("display", "block");
    $.ajax({
        url: "/QualityControl/GetAllValueQC?concentrationVal=" + $("#ddlConcentration").val() + "&&fromdateVal=" + $("#txtQCValPrintFromDt").val() + "&&todateVal=" + $("#txtQCValPrintToDt").val(),
        method: 'get',
        method: 'get',
        success: OnsuccessQCValuePrint
    });
}
var tblValuePrint = "";
function OnsuccessQCValuePrint(response) {
    if (response.length > 0) {
        $("#divValuePrintNoRecordFound").empty();

        $("#divValueQCTable").css("display", "block");
        $('#tblValueQC').DataTable().destroy();
     tblValuePrint =   $('#tblValueQC').DataTable({
            scrollX: true,
            scroll: true, // used for fixed header
            scrollY: 300, // used for fixed Hieght
            bProcessing: true,
            bLenghtChange: true,

            data: response,
            columns: [
                {
                    data: "lotId", title: '<input type="checkbox" id="chkSelectAll" class="form-check-input" onchange="funchkSelectAllValuePrint()">',
                    render: function (data, type, row, meta) {
                        return '<input type="checkbox" class="form-check-input chkPrintCheck" name="checkbox" id="' + row.lotId + '" value="' + row.lotId + '" >';
                    }
                },
                {
                    data: "rundate", title: 'Run Datetime',
                    render: function (data, type, row, meta) {
                        if (row.rundate != null) {
                            return '<p class="RunDate">' + moment(row.rundate).format("DD MMM YYYY, hh:mm a")+'</p>';
                        }
                        else {
                            return ' ';
                        }
                    }
                },
                {
                    data: 'testCode', title: 'Test Code',
                    render: function (data, type, row, meta) {
                        return '<p class="TestCode">' + row.testCode + '</p>';
                    }
                },
                {
                    data: 'lotNo', title: 'QC LOT',
                    render: function (data, type, row, meta) {
                        return '<p class="LotNo">' + row.lotNo + '</p>';
                    }
                },
                {
                    data: 'resultValue', title: 'Result',
                    render: function (data, type, row, meta) {
                        return '<p class="Result">' + row.resultValue + '</p>';
                    }
                },             
                {
                    data: 'flag', title: 'Flag',
                    render: function (data, type, row, meta) {
                        return '<p class="Flag">' + row.flag + '</p>';
                    }
                },
                {
                    data: 'resultValue', title: 'QC Range',
                    render: function (data, type, row, meta) {
                        return '<p class="Range">' + row.resultValue + '</p>';
                    }
                },

                

            ],

        });
        $("#preloader").css("display", "none");

    }
    else {
        $("#divValueQCTable").css("display", "none");
        $("#divValuePrintNoRecordFound").empty();
        $("#divValuePrintNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
        $("#preloader").css("display", "none");

    }
}

function funchkSelectAllValuePrint() {
    var rows = tblValuePrint.rows().nodes();
    if ($("#chkSelectAll").prop("checked") == true) {
        $('input[type="checkbox"]', rows).prop('checked', true);
    }
    else {
        $('input[type="checkbox"]', rows).prop('checked', false);
    }
}

function ValuePrintPDF() {
    $("#preloader").css("display", "block");

    var list = [];
    $("#tblValueQC tr").each(function () {
        if ($($(this).closest('tr')).find('td').find('.chkPrintCheck').prop("checked") == true && typeof ($($(this).closest('tr')).find('td').find('.chkPrintCheck').val()) !== "undefined") {
            list.push({
                RunDate: $($(this).closest('tr')).find('td').find('.RunDate').text(),
                TestCode: $($(this).closest('tr')).find('td').find('.TestCode').text(),
                LotNo: $($(this).closest('tr')).find('td').find('.LotNo').text(),
                Result: $($(this).closest('tr')).find('td').find('.Result').text(),
                Flag: $($(this).closest('tr')).find('td').find('.Flag').text(),
                Range: $($(this).closest('tr')).find('td').find('.Range').text(),
            });
        }
    });

    if (list != null) {
        if (list.length > 0) {

                    const doc = new jsPDF('p', 'pt', 'A4');
                    doc.setFontSize(15);
                    doc.text(250, 30, 'Report Value Print');  //1 Left Margin, 2. Top margin, 3 Text
                    doc.setFontSize(10);

            doc.text(20, 65, 'From Date: ' + moment($("#txtQCValPrintFromDt").val()).format('DD MMM YYYY'));
                    //doc.text(20, 75, 'Patient ID : ' + result.reportPDF[0].patientId);
                    //doc.text(20, 90, 'Sample Type : ' + result.reportPDF[0].sampleType);
                    //doc.text(20, 105, 'Patient Remark : ' + result.reportPDF[0].patientName);

            doc.text(250, 65, 'To Date: ' + moment($("#txtQCValPrintToDt").val()).format('DD MMM YYYY'));
                    //doc.text(250, 75, 'Gender : ' + result.reportPDF[0].gender);
                    //doc.text(250, 90, 'Lab Name : ' + result.reportPDF[0].labName);
                    //doc.text(250, 105, 'Department : ' + result.reportPDF[0].department);

            doc.text(450, 65, 'Concentration : ' + $("#ddlConcentration option:selected").text());
                    //doc.text(450, 75, 'ID : ' + result.reportPDF[0].patId);
                    //doc.text(450, 90, 'Check Date : ' + moment(result.reportPDF[0].runDate).format('DD MMM YYYY'));

                    //doc.line(20, 110, 570, 110); // buttom line
                    //doc.setFontSize(15);
                    //doc.text(250, 130, 'Test Result');  //1 Left Margin, 2. Top margin, 3 Text
                    //doc.setFontSize(10);

                    const col = ['Run Date', 'Test Code', 'QC Lot No.', 'Result', 'Flag','Range'];

                    const rows = [];

                    /* The following array of object as response from the API req  */

            const itemNew = list;

                    itemNew.forEach(element => {

                        const temp = [element.RunDate, element.TestCode, element.LotNo, element.Result, element.Flag, element.Range];
                        rows.push(temp);
                        doc.autoTable(col, rows, { margin: { top: 75 }, height: 'auto' });
                    });

                    doc.line(20, 800, 570, 800); // buttom line
                    doc.text(20, 820, 'Operator : Admin');
            doc.text(450, 820, 'Report Date : ' + moment(new Date()).format('DD MMM YYYY').toString());
                    doc.setFontType("bold");
                    doc.text(200, 830, 'This test has been conducted on FAA 200.');

            window.open(doc.output('bloburl'), '_blank');

                    doc.save('Report.pdf');
                }
            }
    $("#preloader").css("display", "none");

}




function DeleteConfirmationDailyQCLot(Id) {
    $("#modelDeleteConfirmationDailyQCLot").modal('show');
    $("#hdDeleteDailyQCLotId").val(Id);
}

function DeleteDailyQCLot() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteDailyQCLotId").val();
    $("#modelDeleteConfirmationDailyQCLot").modal('hide');
    $("#hdDeleteDailyQCLotId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/QualityControl/DeleteDailyQClot',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableQCLot();
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}
function ClearQCLot() {
    $("#hdQCLotId").val('');
    $("#ddlTestId").val('');
    $("#ddlQCLot").val('');
    $("#ddlConc").val('');
    $("#txtmonth").val('');
    $("#txtyear").val('');
    $("#txtMean").val('');
    $("#txtSd").val('');
    $("#btnQCLot").html("Save");
    $("#modelQCLot").modal('hide');
}


//#region "pring pdf"

function funDtFormate() {

    var varDt = document.getElementById("txtQCExpDt").value;
    //alert(varDt);
    varArrDt = varDt.split("-");
    //alert(varArrDt[2] +  "-" + varArrDt[0]);
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var month_index = parseInt(varArrDt[1], 10) - 1;
    //alert(months[month_index]);

    varNewDt = varArrDt[2] + "-" + months[month_index] + "-" + varArrDt[0]

    //alert(varNewDt);
    document.getElementById("txtQCExpDt").value = varNewDt;

}


function printData() {
    //var reportPageHeight = $('#divDailyQCChart').innerHeight();
    //var reportPageWidth = $('#divDailyQCChart').innerWidth();


    //var pdfCanvas = $('<div />').attr({
    //    id: "LineChartDailyQC",
    //    width: reportPageWidth,
    //    height: reportPageHeight
    //});

    //var pdfctx = $("#LineChartDailyQC")[0].getContext('2d');
    //var pdfctxX = 0;
    //var pdfctxY = 0;
    //var buffer = 100;

    //// for each chart.js chart
    //$("#LineChartDailyQC").each(function (index) {
    //    // get the chart height/width
    //    var canvasHeight = $(this).innerHeight();
    //    var canvasWidth = $(this).innerWidth();

    //    // draw the chart into the new canvas
    //    pdfctx.drawImage($(this)[0], pdfctxX, pdfctxY, canvasWidth, canvasHeight);
    //    pdfctxX += canvasWidth + buffer;

    //    // our report page is in a grid pattern so replicate that in the new canvas
    //    if (index % 2 === 1) {
    //        pdfctxX = 0;
    //        pdfctxY += canvasHeight + buffer;
    //    }
    //});

    //var pdf = new jsPDF('l', 'pt', [reportPageWidth, reportPageHeight]);
    //pdf.addImage($(pdfCanvas)[0], 'PNG', 0, 0);

    //// download the pdf
    //pdf.save('filename.pdf');


    var pdf = new jsPDF();
    var chartTheme = chart.get("theme");
    chart.set("theme", "light2");
    var canvas = $("#LineChartDailyQC .canvasjs-chart-canvas").get(0);
    var dataURL = canvas.toDataURL();
    chart.set("theme", chartTheme);
    pdf.addImage(dataURL, 'JPEG', 0, 0);
    pdf.save("download.pdf");





    //var pdf = new jsPDF();
    ////var chartTheme = chart.get("theme");
    ////chart.set("theme", "light2");
    //var canvas = $("#LineChartDailyQC .canvasjs-chart-canvas").get(0);
    //alert(canvas);
    //var dataURL = canvas.toDataURL();
    ////chart.set("theme", chartTheme);
    //pdf.addImage(dataURL, 'JPEG', 0, 0);
    //pdf.save("download.pdf");



    //ch_list = Array();
    //var lotidList = [];
    //$("input:checkbox[name=checkbox]:checked").each(function () {
    //    ch_list.push($(this).val());
    //    lotidList.push($(this).val());
    //    //ch_list.push($(this).value);        
    //});


    ////var pageCount = doc.internal.getNumberOfPages();
    ////doc.deletePage(pageCount);
    //var doc = new jsPDF('p', 'pt', 'letter');

    //var specialElementHandlers = {
    //    '#editor': function (element, renderer) {
    //        return true;
    //    }
    //};

    //var divid = printData.arguments[0];

    //var currentdate = new Date();
    //var dt = currentdate.getDate() + "/"
    //    + (currentdate.getMonth() + 1) + "/"
    //    + currentdate.getFullYear() + ":"
    //    + currentdate.getHours() + ":"
    //    + currentdate.getMinutes() + ":"
    //    + currentdate.getSeconds();

    //margins = {
    //    top: 80,
    //    bottom: 60,
    //    left: 80,
    //    width: 900
    //};

    ////lineChart
    //if (printData.arguments[2] == "1") {

    //    var DailyQCData = 'Test Code:  ' + $("#ddlDailyQCTestcode").find("option:selected").text() + '    |    ' + '   LotNo:    ' + $("#ddlDailyQCLot").find("option:selected").text() + '    |    ' + '    Concentration:  ' + $("#ddlDailyQCConcentration").find("option:selected").text();
    //    //document.getElementById("QCheaderline").innerHTML = DailyQCData;

    //    varImg = "<img src='~/img/HeaderLogo.jpg' style='height:30px; width:30px' />";

    //    $("#tblDailyQc_filter").css("display", "none");

    //    doc.text(276, 25, "QC Chart");

    //    /* start */

    //    var canvas = document.querySelector('#LineChartDailyQC');
    //    var canvasImg = canvas.toDataURL("~/img/jpeg", 1.0);

    //    doc.addImage(canvasImg, 'JPEG', 50, 50, 300, 300);

    //    /* End  */

    //    doc.fromHTML($('#divDailyQCTablePrint').html(),
    //        margins.top,
    //        400, {
    //        'width': margins.width,
    //        'elementHandlers': specialElementHandlers
    //    });

    //    /* Header Footer code */
    //    const pageCount = doc.internal.getNumberOfPages();

    //    for (let i = 1; i <= pageCount; i++) {
    //        doc.setPage(i);
    //        const pageSize = doc.internal.pageSize;
    //        const pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
    //        const pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
    //        const header = DailyQCData; //'Report 2014';
    //        const footer = 'This test has been conducted on AutoQuant 200i   ' + `       Page ${i} of ${pageCount}`;

    //        // Header
    //        doc.text(header, 40, 55, { baseline: 'top' });

    //        // Footer
    //        doc.text(footer, pageWidth / 2 - (doc.getTextWidth(footer) / 2), pageHeight - 15, { baseline: 'bottom' });
    //    }
    //    doc.save(printData.arguments[1] + dt + '.pdf');

    //}

    //else if (printData.arguments[2] == "2") {

    //    var HeaderLine = "Test Code :   " + $("#ddlMonthlyQCTestcode").find("option:selected").text() + '    |    ' + "    LotNo :  " + $("#ddlMonthlyQCLot").find("option:selected").text() + '    |    ' + "    Concentration  :  " + $("#ddlMonthlyQCConcentration").find("option:selected").text();

    //    //document.getElementById("pRunDate").innerHTML = "Run Date:  " + $("#txtQCDailyDt").text();
    //    //$("#tblDailyQc_filter").css("display", "none");
    //    doc.text(270, 25, "QC Result Chart");
    //    doc.fromHTML($('#divMonthlyQCTablePrint').html(),
    //        margins.top,
    //        margins.left, {
    //        'width': margins.width,
    //        'elementHandlers': specialElementHandlers
    //    });

    //    /* Header Footer code */
    //    const pageCount = doc.internal.getNumberOfPages();

    //    for (let i = 1; i <= pageCount; i++) {
    //        doc.setPage(i);
    //        const pageSize = doc.internal.pageSize;
    //        const pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
    //        const pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
    //        const header = HeaderLine; //'Report 2014';
    //        const footer = 'This test has been conducted on AutoQuant 200i   ' + `       Page ${i} of ${pageCount}`;

    //        // Header
    //        doc.text(header, 40, 55, { baseline: 'top' });

    //        // Footer
    //        doc.text(footer, pageWidth / 2 - (doc.getTextWidth(footer) / 2), pageHeight - 15, { baseline: 'bottom' });
    //    }

    //    doc.save(printData.arguments[1] + dt + '.pdf');

    //}

    //else if (printData.arguments[2] == "3") {

    //    //BindTableDailyQC()
    //    BindTableQCValuePrint_pdf(lotidList);//lotidList
    //    //doc.page=1; txtQCValPrintFromDt

    //    var headerdetails = $("#ddlMonthlyQCConcentration").find("option:selected").text();
    //    var headerlineExFrdt = "";
    //    headerlineExFrdt = moment($("#txtQCValPrintFromDt").val()).format("DD-MMM-YYYY");
    //    var headerlineExTodt = "";
    //    headerlineExTodt = moment($("#txtQCValPrintToDt").val()).format("DD-MMM-YYYY");
    //    var headerLinedata = "Concentration : " + headerdetails + "  |    " + "   From Date :  " + headerlineExFrdt + "    |    " + "  To Date:  " + headerlineExTodt;
    //    doc.text(276, 25, "QC Result");
    //    doc.fromHTML($('#divQCValuePrint').html(),
    //        margins.top,
    //        margins.left, {
    //        'width': margins.width,
    //        'elementHandlers': specialElementHandlers
    //    });

    //    /* Header Footer code */
    //    const pageCount = doc.internal.getNumberOfPages();

    //    for (let i = 1; i <= pageCount; i++) {
    //        doc.setPage(i);
    //        const pageSize = doc.internal.pageSize;
    //        const pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
    //        const pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
    //        const header = headerLinedata; //'Report 2014';
    //        const footer = 'This test has been conducted on AutoQuant 200i   ' + `       Page ${i} of ${pageCount}`;

    //        // Header
    //        doc.text(header, 40, 55, { baseline: 'top' });

    //        // Footer
    //        doc.text(footer, pageWidth / 2 - (doc.getTextWidth(footer) / 2), pageHeight - 15, { baseline: 'bottom' });
    //    }

    //    doc.save(printData.arguments[1] + dt + '.pdf');
    //}
}
//#endregion

function BindTableQCValuePrint_pdf(lotidList) {
    $("#preloader").css("display", "block");
    $.ajax({

        url: "/QualityControl/GetAllQCValuePrint_pdf?concentrationVal=" + $("#ddlConcentration").val() + "&&fromdateVal=" + $("#txtQCValPrintFromDt").val() + "&&todateVal=" + $("#txtQCValPrintToDt").val() + "&&lotidList=" + lotidList,// && +"chklist+" + BindTableQCValuePrint_pdf.arguments[0],

        //url: "/QualityControl/GetAllQCValuePrint_pdf?concentrationVal=" + $("#ddlConcentration").val(), //lotidList
        method: 'get',
        success: OnsuccessQCValuePrint_print
    });
}
function OnsuccessQCValuePrint_print(response) {
    $('#tbl_QCValuePrint').DataTable().destroy();
    $('#tbl_QCValuePrint').DataTable({
        scrollX: false,
        bProcessing: false,
        bLenghtChange: false,
        retrieve: false,
        bfilter: false,
        bSort: false,
        bPaging: false,
        bPaginate: false,
        bInfo: false,
        bsearching: false,
        data: response,
        columns: [
            { data: 'testCode', title: 'Test Code' },
            { data: 'lotNo', title: 'QC LOT' },
            { data: 'resu', title: 'Result' },
            //{ data: 'rundat', title: 'Run Datetime' },
            {
                data: "rundat", title: 'Run Datetime',
                render: function (data, type, row, meta) {

                    var vpDateList = "";
                    vpDateList = moment(row.rundat).format("DD-MMM-YYYY");
                    return vpDateList;
                }
            },
            { data: 'flag', title: 'Flag' },
            { data: '', title: 'QC Range' },

        ],

    });
    $("#preloader").css("display", "none");

}
function funReadConVal() {
    //alert($("#ddlDailyQCConcentration").find("option:selected").text());
    //alert($("#ddlConcentration").val());   
    //BindTableQCValuePrint();
}


