/* Added by jayashri ---- calibration functions */

// calculated test - expression text
function funExpression() {
    document.getElementById('txtCalExpression').textContent = "";
    document.getElementById('txtCalExpression').append(document.getElementById('ddCalcTestCode').options[ddCalcTestCode.selectedIndex].text);

};

// curve type # 1
function Fun_CurveType() {

    BindTableCalHisCurveType('ddlCalibrationCurveType');

    if ($("#ddlCalibrationCurveType").val() == "1") {
        $('#divk').css("display", "flex");
        $('#diva').css("display", "none");
        $('#divb').css("display", "none");
        $('#divc').css("display", "none");
        $('#divd').css("display", "none");
        $('#divRo').css("display", "none");

        $('#div1').css("display", "flex");
        $('#div2').css("display", "none");
        $('#div3').css("display", "none");
        $('#div4').css("display", "none");
        $('#div5').css("display", "none");
        $('#div6').css("display", "none");
        $('#div7').css("display", "none");
        $('#div8').css("display", "none");
        $('#div9').css("display", "none");
        $('#div10').css("display", "none");


        $('#txtCalibrationStdVal1').css("display", "flex");
        $('#txtCalibrationStdVal2').css("display", "none");
        $('#txtCalibrationStdVal3').css("display", "none");
        $('#txtCalibrationStdVal4').css("display", "none");
        $('#txtCalibrationStdVal5').css("display", "none");
        $('#txtCalibrationStdVal6').css("display", "none");
        $('#txtCalibrationStdVal7').css("display", "none");
        $('#txtCalibrationStdVal8').css("display", "none");
        $('#txtCalibrationStdVal9').css("display", "none");
        $('#txtCalibrationStdVal10').css("display", "none");

        $('#txtCalibrationAbsorbance1').css("display", "flex");
        $('#txtCalibrationAbsorbance2').css("display", "none");
        $('#txtCalibrationAbsorbance3').css("display", "none");
        $('#txtCalibrationAbsorbance4').css("display", "none");
        $('#txtCalibrationAbsorbance5').css("display", "none");
        $('#txtCalibrationAbsorbance6').css("display", "none");
        $('#txtCalibrationAbsorbance7').css("display", "none");
        $('#txtCalibrationAbsorbance8').css("display", "none");
        $('#txtCalibrationAbsorbance9').css("display", "none");
        $('#txtCalibrationAbsorbance10').css("display", "none");

        $('#txtCalibrationStdNo').val("1");

        $('#divCalDilution').css("display", "none");
        $("#divCalDilutiontxt").css("display", "none");
    }
    else if (document.getElementById('ddlCalibrationCurveType').value == "2") //one point
    {
        document.getElementById('divk').style.display = 'none';
        document.getElementById('diva').style.display = 'flex';
        document.getElementById('divb').style.display = 'flex';
        document.getElementById('divc').style.display = 'none';
        document.getElementById('divd').style.display = 'none';
        document.getElementById('divRo').style.display = 'none';
      

        document.getElementById('div1').style.display = 'flex';
        document.getElementById('div2').style.display = 'flex';
        document.getElementById('div3').style.display = 'none';
        document.getElementById('div4').style.display = 'none';
        document.getElementById('div5').style.display = 'none';
        document.getElementById('div6').style.display = 'none';
        document.getElementById('div7').style.display = 'none';
        document.getElementById('div8').style.display = 'none';
        document.getElementById('div9').style.display = 'none';
        document.getElementById('div10').style.display = 'none';

        document.getElementById('txtCalibrationStdVal1').style.display = 'flex';
        document.getElementById('txtCalibrationStdVal2').style.display = 'flex';
        document.getElementById('txtCalibrationStdVal3').style.display = 'none';
        document.getElementById('txtCalibrationStdVal4').style.display = 'none';
        document.getElementById('txtCalibrationStdVal5').style.display = 'none';
        document.getElementById('txtCalibrationStdVal6').style.display = 'none';
        $('#txtCalibrationStdVal7').css("display", "none");
        $('#txtCalibrationStdVal8').css("display", "none");
        $('#txtCalibrationStdVal9').css("display", "none");
        $('#txtCalibrationStdVal10').css("display", "none");

        document.getElementById('txtCalibrationAbsorbance1').style.display = 'flex';
        document.getElementById('txtCalibrationAbsorbance2').style.display = 'flex';
        document.getElementById('txtCalibrationAbsorbance3').style.display = 'none';
        document.getElementById('txtCalibrationAbsorbance4').style.display = 'none';
        document.getElementById('txtCalibrationAbsorbance5').style.display = 'none';
        document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';
        $('#txtCalibrationAbsorbance7').css("display", "none");
        $('#txtCalibrationAbsorbance8').css("display", "none");
        $('#txtCalibrationAbsorbance9').css("display", "none");
        $('#txtCalibrationAbsorbance10').css("display", "none");


        document.getElementById('txtCalibrationStdNo').value = "2";
        document.getElementById('divCalDilution').style.display = 'none';
        $("#divCalDilutiontxt").css("display", "none");

    }
    else if (document.getElementById('ddlCalibrationCurveType').value == "3") //two points
    {
        document.getElementById('divk').style.display = 'none';
        document.getElementById('diva').style.display = 'flex';
        document.getElementById('divb').style.display = 'flex';
        document.getElementById('divc').style.display = 'none';
        document.getElementById('divRo').style.display = 'none';

    //    document.getElementById('div1').style.display = 'flex';
    //    document.getElementById('div2').style.display = 'flex';
    //    document.getElementById('div3').style.display = 'flex';
    //    document.getElementById('div4').style.display = 'none';
    //    document.getElementById('div5').style.display = 'none';
    //    document.getElementById('div6').style.display = 'none';

    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'none';
    //    $('#txtCalibrationStdVal7').css("display", "none");
    //    $('#txtCalibrationStdVal8').css("display", "none");
    //    $('#txtCalibrationStdVal9').css("display", "none");
    //    $('#txtCalibrationStdVal10').css("display", "none");


    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';
    //    $('#txtCalibrationAbsorbance7').css("display", "none");
    //    $('#txtCalibrationAbsorbance8').css("display", "none");
    //    $('#txtCalibrationAbsorbance9').css("display", "none");
    //    $('#txtCalibrationAbsorbance10').css("display", "none");

    //    document.getElementById('txtCalibrationStdNo').value = "3";

    //    document.getElementById('divCalDilution').style.display = 'flex';
    //    $("#divCalDilutiontxt").css("display", "flex");

    }
    else if (document.getElementById('ddlCalibrationCurveType').value == "4") //multiple points
    {
        document.getElementById('divk').style.display = 'flex';
        document.getElementById('diva').style.display = 'flex';
        document.getElementById('divb').style.display = 'flex';
        document.getElementById('divc').style.display = 'none';
        document.getElementById('divd').style.display = 'none';
        document.getElementById('divRo').style.display = 'flex';

    //    document.getElementById('div1').style.display = 'flex';
    //    document.getElementById('div2').style.display = 'flex';
    //    document.getElementById('div3').style.display = 'flex';
    //    document.getElementById('div4').style.display = 'flex';
    //    document.getElementById('div5').style.display = 'none';
    //    document.getElementById('div6').style.display = 'none';

    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'flex';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'none';
    //    $('#txtCalibrationStdVal7').css("display", "none");
    //    $('#txtCalibrationStdVal8').css("display", "none");
    //    $('#txtCalibrationStdVal9').css("display", "none");
    //    $('#txtCalibrationStdVal10').css("display", "none");

    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'flex';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';
    //    $('#txtCalibrationAbsorbance7').css("display", "none");
    //    $('#txtCalibrationAbsorbance8').css("display", "none");
    //    $('#txtCalibrationAbsorbance9').css("display", "none");
    //    $('#txtCalibrationAbsorbance10').css("display", "none");

    //    document.getElementById('txtCalibrationStdNo').value = "4";

    //    document.getElementById('divCalDilution').style.display = 'flex';
    //    $("#divCalDilutiontxt").css("display", "flex");

    }
    else if (document.getElementById('ddlCalibrationCurveType').value == "5") //logit-log-4p
    {
        document.getElementById('divk').style.display = 'flex';
        document.getElementById('diva').style.display = 'flex';
        document.getElementById('divb').style.display = 'flex';
        document.getElementById('divc').style.display = 'flex';
        document.getElementById('divd').style.display = 'none';
        document.getElementById('divRo').style.display = 'flex';
       

    //    document.getElementById('div1').style.display = 'flex';
    //    document.getElementById('div2').style.display = 'flex';
    //    document.getElementById('div3').style.display = 'flex';
    //    document.getElementById('div4').style.display = 'flex';
    //    document.getElementById('div5').style.display = 'flex';
    //    document.getElementById('div6').style.display = 'none';

    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'flex';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'none';
    //    $('#txtCalibrationStdVal7').css("display", "none");
    //    $('#txtCalibrationStdVal8').css("display", "none");
    //    $('#txtCalibrationStdVal9').css("display", "none");
    //    $('#txtCalibrationStdVal10').css("display", "none");

    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'flex';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';
    //    $('#txtCalibrationAbsorbance7').css("display", "none");
    //    $('#txtCalibrationAbsorbance8').css("display", "none");
    //    $('#txtCalibrationAbsorbance9').css("display", "none");
    //    $('#txtCalibrationAbsorbance10').css("display", "none");

    //    document.getElementById('txtCalibrationStdNo').value = "5";

    //    document.getElementById('divCalDilution').style.display = 'flex';
    //    $("#divCalDilutiontxt").css("display", "flex");

    }
    else if (document.getElementById('ddlCalibrationCurveType').value == "6") //logit-log-5p
    {
        document.getElementById('divk').style.display = 'flex';
        document.getElementById('diva').style.display = 'flex';
        document.getElementById('divb').style.display = 'flex';
        document.getElementById('divc').style.display = 'flex';
        document.getElementById('divd').style.display = 'none';
        document.getElementById('divRo').style.display = 'flex';

    //    document.getElementById('div1').style.display = 'flex';
    //    document.getElementById('div2').style.display = 'flex';
    //    document.getElementById('div3').style.display = 'flex';
    //    document.getElementById('div4').style.display = 'flex';
    //    document.getElementById('div5').style.display = 'flex';
    //    document.getElementById('div6').style.display = 'none';

    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'none';
    //    $('#txtCalibrationStdVal7').css("display", "none");
    //    $('#txtCalibrationStdVal8').css("display", "none");
    //    $('#txtCalibrationStdVal9').css("display", "none");
    //    $('#txtCalibrationStdVal10').css("display", "none");

    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';
    //    $('#txtCalibrationAbsorbance7').css("display", "none");
    //    $('#txtCalibrationAbsorbance8').css("display", "none");
    //    $('#txtCalibrationAbsorbance9').css("display", "none");
    //    $('#txtCalibrationAbsorbance10').css("display", "none");

    //    document.getElementById('txtCalibrationStdNo').value = "5";

    //    document.getElementById('divCalDilution').style.display = 'flex';
    //    $("#divCalDilutiontxt").css("display", "flex");

    }
    else if (document.getElementById('ddlCalibrationCurveType').value == "7") //exponential
    {
        document.getElementById('divk').style.display = 'none';
        document.getElementById('diva').style.display = 'flex';
        document.getElementById('divb').style.display = 'flex';
        document.getElementById('divc').style.display = 'flex';
        document.getElementById('divd').style.display = 'none';
        document.getElementById('divRo').style.display = 'flex';

    //    document.getElementById('div1').style.display = 'flex';
    //    document.getElementById('div2').style.display = 'flex';
    //    document.getElementById('div3').style.display = 'flex';
    //    document.getElementById('div4').style.display = 'flex';
    //    document.getElementById('div5').style.display = 'flex';
    //    document.getElementById('div6').style.display = 'none';

    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'none';
    //    $('#txtCalibrationStdVal7').css("display", "none");
    //    $('#txtCalibrationStdVal8').css("display", "none");
    //    $('#txtCalibrationStdVal9').css("display", "none");
    //    $('#txtCalibrationStdVal10').css("display", "none");

    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';
    //    $('#txtCalibrationAbsorbance7').css("display", "none");
    //    $('#txtCalibrationAbsorbance8').css("display", "none");
    //    $('#txtCalibrationAbsorbance9').css("display", "none");
    //    $('#txtCalibrationAbsorbance10').css("display", "none");

    //    document.getElementById('txtCalibrationStdNo').value = "5";

    //    document.getElementById('divCalDilution').style.display = 'flex';
    //    $("#divCalDilutiontxt").css("display", "flex");

    }
    else if (document.getElementById('ddlCalibrationCurveType').value == "8") // polynomil
    {
        document.getElementById('divk').style.display = 'none';
        document.getElementById('diva').style.display = 'flex';
        document.getElementById('divb').style.display = 'flex';
        document.getElementById('divc').style.display = 'flex';
        document.getElementById('divd').style.display = 'none';
        document.getElementById('divRo').style.display = 'none';

    //    document.getElementById('div1').style.display = 'flex';
    //    document.getElementById('div2').style.display = 'flex';
    //    document.getElementById('div3').style.display = 'flex';
    //    document.getElementById('div4').style.display = 'flex';
    //    document.getElementById('div5').style.display = 'flex';
    //    document.getElementById('div6').style.display = 'none';

    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'none';
    //    $('#txtCalibrationStdVal7').css("display", "none");
    //    $('#txtCalibrationStdVal8').css("display", "none");
    //    $('#txtCalibrationStdVal9').css("display", "none");
    //    $('#txtCalibrationStdVal10').css("display", "none");

    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';
    //    $('#txtCalibrationAbsorbance7').css("display", "none");
    //    $('#txtCalibrationAbsorbance8').css("display", "none");
    //    $('#txtCalibrationAbsorbance9').css("display", "none");
    //    $('#txtCalibrationAbsorbance10').css("display", "none");

    //    document.getElementById('txtCalibrationStdNo').value = "5";

    //    document.getElementById('divCalDilution').style.display = 'flex';
    //    $("#divCalDilutiontxt").css("display", "flex");

    }
    else if (document.getElementById('ddlCalibrationCurveType').value == "9") //parabola
    {
        document.getElementById('divk').style.display = 'none';
        document.getElementById('diva').style.display = 'none';
        document.getElementById('divb').style.display = 'none';
        document.getElementById('divc').style.display = 'none';
        document.getElementById('divd').style.display = 'none';
        document.getElementById('divRo').style.display = 'none';

    //    document.getElementById('div1').style.display = 'block';
    //    document.getElementById('div2').style.display = 'block';
    //    document.getElementById('div3').style.display = 'block';
    //    document.getElementById('div4').style.display = 'block';
    //    document.getElementById('div5').style.display = 'block';
    //    document.getElementById('div6').style.display = 'none';

    //    //txtCalibrationStdVal1
    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'none';
    //    $('#txtCalibrationStdVal7').css("display", "none");
    //    $('#txtCalibrationStdVal8').css("display", "none");
    //    $('#txtCalibrationStdVal9').css("display", "none");
    //    $('#txtCalibrationStdVal10').css("display", "none");

    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';
    //    $('#txtCalibrationAbsorbance7').css("display", "none");
    //    $('#txtCalibrationAbsorbance8').css("display", "none");
    //    $('#txtCalibrationAbsorbance9').css("display", "none");
    //    $('#txtCalibrationAbsorbance10').css("display", "none");

    //    document.getElementById('txtCalibrationStdNo').value = "5";

    //    document.getElementById('divCalDilution').style.display = 'flex';
    //    $("#divCalDilutiontxt").css("display", "flex");
    }
}

function funAddStdNo() {

    var intStdVal = $("#txtCalibrationStdNo").val();
    var strCurveType = $("#ddlCalibrationCurveType").val();
    if (strCurveType == "1") {
        $("#txtCalibrationStdNo").val('1');
    }
    else if (strCurveType == "2") {
        $("#txtCalibrationStdNo").val('2');
    }
    else if (intStdVal == "1") {
       // $("#ddlCalibrationCurveType").val("1");
        $("#div1").css("display", "block");
        $("#div2").css("display", "none");
        $("#div3").css("display", "none");
        $("#div4").css("display", "none");
        $("#div5").css("display", "none");
        $("#div6").css("display", "none");
        $("#div7").css("display", "none");
        $("#div8").css("display", "none");
        $("#div9").css("display", "none");
        $("#div10").css("display", "none");

        $("#txtCalibrationStdVal1").css("display", "block");
        $("#txtCalibrationStdVal2").css("display", "none");
        $("#txtCalibrationStdVal3").css("display", "none");
        $("#txtCalibrationStdVal4").css("display", "none");
        $("#txtCalibrationStdVal5").css("display", "none");
        $("#txtCalibrationStdVal6").css("display", "none");
        $("#txtCalibrationStdVal7").css("display", "none");
        $("#txtCalibrationStdVal8").css("display", "none");
        $("#txtCalibrationStdVal9").css("display", "none");
        $("#txtCalibrationStdVal10").css("display", "none");
        //$("#txtCalibrationStdVal2").val('');
        //$("#txtCalibrationStdVal3").val('');
        //$("#txtCalibrationStdVal4").val('');
        //$("#txtCalibrationStdVal5").val('');
        //$("#txtCalibrationStdVal6").val('');
        //$("#txtCalibrationStdVal7").val('');
        //$("#txtCalibrationStdVal8").val('');
        //$("#txtCalibrationStdVal9").val('');
        //$("#txtCalibrationStdVal10").val('');

        $("#txtCalibrationAbsorbance1").css("display", "block");
        $("#txtCalibrationAbsorbance2").css("display", "none");
        $("#txtCalibrationAbsorbance3").css("display", "none");
        $("#txtCalibrationAbsorbance4").css("display", "none");
        $("#txtCalibrationAbsorbance5").css("display", "none");
        $("#txtCalibrationAbsorbance6").css("display", "none");
        $("#txtCalibrationAbsorbance7").css("display", "none");
        $("#txtCalibrationAbsorbance8").css("display", "none");
        $("#txtCalibrationAbsorbance9").css("display", "none");
        $("#txtCalibrationAbsorbance10").css("display", "none");
    }

    else if (intStdVal == "2") {
        $("#div1").css("display", "block");
        $("#div2").css("display", "block");
        $("#div3").css("display", "none");
        $("#div4").css("display", "none");
        $("#div5").css("display", "none");
        $("#div6").css("display", "none");
        $("#div7").css("display", "none");
        $("#div8").css("display", "none");
        $("#div9").css("display", "none");
        $("#div10").css("display", "none");

        $("#txtCalibrationStdVal1").css("display", "block");
        $("#txtCalibrationStdVal2").css("display", "block");
        $("#txtCalibrationStdVal3").css("display", "none");
        $("#txtCalibrationStdVal4").css("display", "none");
        $("#txtCalibrationStdVal5").css("display", "none");
        $("#txtCalibrationStdVal6").css("display", "none");
        $("#txtCalibrationStdVal7").css("display", "none");
        $("#txtCalibrationStdVal8").css("display", "none");
        $("#txtCalibrationStdVal9").css("display", "none");
        $("#txtCalibrationStdVal10").css("display", "none");

        $("#txtCalibrationStdVal3").val('');
        $("#txtCalibrationStdVal4").val('');
        $("#txtCalibrationStdVal5").val('');
        $("#txtCalibrationStdVal6").val('');
        $("#txtCalibrationStdVal7").val('');
        $("#txtCalibrationStdVal8").val('');
        $("#txtCalibrationStdVal9").val('');
        $("#txtCalibrationStdVal10").val('');

        $("#txtCalibrationAbsorbance1").css("display", "block");
        $("#txtCalibrationAbsorbance2").css("display", "block");
        $("#txtCalibrationAbsorbance3").css("display", "none");
        $("#txtCalibrationAbsorbance4").css("display", "none");
        $("#txtCalibrationAbsorbance5").css("display", "none");
        $("#txtCalibrationAbsorbance6").css("display", "none");
        $("#txtCalibrationAbsorbance7").css("display", "none");
        $("#txtCalibrationAbsorbance8").css("display", "none");
        $("#txtCalibrationAbsorbance9").css("display", "none");
        $("#txtCalibrationAbsorbance10").css("display", "none");
    }
    else if (intStdVal == "3") {
        $("#div1").css("display", "block");
        $("#div2").css("display", "block");
        $("#div3").css("display", "block");
        $("#div4").css("display", "none");
        $("#div5").css("display", "none");
        $("#div6").css("display", "none");
        $("#div7").css("display", "none");
        $("#div8").css("display", "none");
        $("#div9").css("display", "none");
        $("#div10").css("display", "none");

        $("#txtCalibrationStdVal1").css("display", "block");
        $("#txtCalibrationStdVal2").css("display", "block");
        $("#txtCalibrationStdVal3").css("display", "block");
        $("#txtCalibrationStdVal4").css("display", "none");
        $("#txtCalibrationStdVal5").css("display", "none");
        $("#txtCalibrationStdVal6").css("display", "none");
        $("#txtCalibrationStdVal7").css("display", "none");
        $("#txtCalibrationStdVal8").css("display", "none");
        $("#txtCalibrationStdVal9").css("display", "none");
        $("#txtCalibrationStdVal10").css("display", "none");

        $("#txtCalibrationStdVal4").val('');
        $("#txtCalibrationStdVal5").val('');
        $("#txtCalibrationStdVal6").val('');
        $("#txtCalibrationStdVal7").val('');
        $("#txtCalibrationStdVal8").val('');
        $("#txtCalibrationStdVal9").val('');
        $("#txtCalibrationStdVal10").val('');

        $("#txtCalibrationAbsorbance1").css("display", "block");
        $("#txtCalibrationAbsorbance2").css("display", "block");
        $("#txtCalibrationAbsorbance3").css("display", "block");
        $("#txtCalibrationAbsorbance4").css("display", "none");
        $("#txtCalibrationAbsorbance5").css("display", "none");
        $("#txtCalibrationAbsorbance6").css("display", "none");
        $("#txtCalibrationAbsorbance7").css("display", "none");
        $("#txtCalibrationAbsorbance8").css("display", "none");
        $("#txtCalibrationAbsorbance9").css("display", "none");
        $("#txtCalibrationAbsorbance10").css("display", "none");

    }
    else if (intStdVal == "4") {
        $("#div1").css("display", "block");
        $("#div2").css("display", "block");
        $("#div3").css("display", "block");
        $("#div4").css("display", "block");
        $("#div5").css("display", "none");
        $("#div6").css("display", "none");
        $("#div7").css("display", "none");
        $("#div8").css("display", "none");
        $("#div9").css("display", "none");
        $("#div10").css("display", "none");

        $("#txtCalibrationStdVal1").css("display", "block");
        $("#txtCalibrationStdVal2").css("display", "block");
        $("#txtCalibrationStdVal3").css("display", "block");
        $("#txtCalibrationStdVal4").css("display", "block");
        $("#txtCalibrationStdVal5").css("display", "none");
        $("#txtCalibrationStdVal6").css("display", "none");
        $("#txtCalibrationStdVal7").css("display", "none");
        $("#txtCalibrationStdVal8").css("display", "none");
        $("#txtCalibrationStdVal9").css("display", "none");
        $("#txtCalibrationStdVal10").css("display", "none");

        $("#txtCalibrationStdVal5").val('');
        $("#txtCalibrationStdVal6").val('');
        $("#txtCalibrationStdVal7").val('');
        $("#txtCalibrationStdVal8").val('');
        $("#txtCalibrationStdVal9").val('');
        $("#txtCalibrationStdVal10").val('');

        $("#txtCalibrationAbsorbance1").css("display", "block");
        $("#txtCalibrationAbsorbance2").css("display", "block");
        $("#txtCalibrationAbsorbance3").css("display", "block");
        $("#txtCalibrationAbsorbance4").css("display", "block");
        $("#txtCalibrationAbsorbance5").css("display", "none");
        $("#txtCalibrationAbsorbance6").css("display", "none");
        $("#txtCalibrationAbsorbance7").css("display", "none");
        $("#txtCalibrationAbsorbance8").css("display", "none");
        $("#txtCalibrationAbsorbance9").css("display", "none");
        $("#txtCalibrationAbsorbance10").css("display", "none");

    }
    else if (intStdVal == "5") {
        $("#div1").css("display", "block");
        $("#div2").css("display", "block");
        $("#div3").css("display", "block");
        $("#div4").css("display", "block");
        $("#div5").css("display", "block");
        $("#div6").css("display", "none");
        $("#div7").css("display", "none");
        $("#div8").css("display", "none");
        $("#div9").css("display", "none");
        $("#div10").css("display", "none");

        $("#txtCalibrationStdVal1").css("display", "block");
        $("#txtCalibrationStdVal2").css("display", "block");
        $("#txtCalibrationStdVal3").css("display", "block");
        $("#txtCalibrationStdVal4").css("display", "block");
        $("#txtCalibrationStdVal5").css("display", "block");
        $("#txtCalibrationStdVal6").css("display", "none");
        $("#txtCalibrationStdVal7").css("display", "none");
        $("#txtCalibrationStdVal8").css("display", "none");
        $("#txtCalibrationStdVal9").css("display", "none");
        $("#txtCalibrationStdVal10").css("display", "none");

        $("#txtCalibrationStdVal6").val('');
        $("#txtCalibrationStdVal7").val('');
        $("#txtCalibrationStdVal8").val('');
        $("#txtCalibrationStdVal9").val('');
        $("#txtCalibrationStdVal10").val('');

        $("#txtCalibrationAbsorbance1").css("display", "block");
        $("#txtCalibrationAbsorbance2").css("display", "block");
        $("#txtCalibrationAbsorbance3").css("display", "block");
        $("#txtCalibrationAbsorbance4").css("display", "block");
        $("#txtCalibrationAbsorbance5").css("display", "block");
        $("#txtCalibrationAbsorbance6").css("display", "none");
        $("#txtCalibrationAbsorbance7").css("display", "none");
        $("#txtCalibrationAbsorbance8").css("display", "none");
        $("#txtCalibrationAbsorbance9").css("display", "none");
        $("#txtCalibrationAbsorbance10").css("display", "none");

    }
    else if (intStdVal == "6") {
        $("#div1").css("display", "block");
        $("#div2").css("display", "block");
        $("#div3").css("display", "block");
        $("#div4").css("display", "block");
        $("#div5").css("display", "block");
        $("#div6").css("display", "block");
        $("#div7").css("display", "none");
        $("#div8").css("display", "none");
        $("#div9").css("display", "none");
        $("#div10").css("display", "none");

        $("#txtCalibrationStdVal1").css("display", "block");
        $("#txtCalibrationStdVal2").css("display", "block");
        $("#txtCalibrationStdVal3").css("display", "block");
        $("#txtCalibrationStdVal4").css("display", "block");
        $("#txtCalibrationStdVal5").css("display", "block");
        $("#txtCalibrationStdVal6").css("display", "block");
        $("#txtCalibrationStdVal7").css("display", "none");
        $("#txtCalibrationStdVal8").css("display", "none");
        $("#txtCalibrationStdVal9").css("display", "none");
        $("#txtCalibrationStdVal10").css("display", "none");

        $("#txtCalibrationStdVal7").val('');
        $("#txtCalibrationStdVal8").val('');
        $("#txtCalibrationStdVal9").val('');
        $("#txtCalibrationStdVal10").val('');

        $("#txtCalibrationAbsorbance1").css("display", "block");
        $("#txtCalibrationAbsorbance2").css("display", "block");
        $("#txtCalibrationAbsorbance3").css("display", "block");
        $("#txtCalibrationAbsorbance4").css("display", "block");
        $("#txtCalibrationAbsorbance5").css("display", "block");
        $("#txtCalibrationAbsorbance6").css("display", "block");
        $("#txtCalibrationAbsorbance7").css("display", "none");
        $("#txtCalibrationAbsorbance8").css("display", "none");
        $("#txtCalibrationAbsorbance9").css("display", "none");
        $("#txtCalibrationAbsorbance10").css("display", "none");

    }
    else if (intStdVal == "7") {
        $("#div1").css("display", "block");
        $("#div2").css("display", "block");
        $("#div3").css("display", "block");
        $("#div4").css("display", "block");
        $("#div5").css("display", "block");
        $("#div6").css("display", "block");
        $("#div7").css("display", "block");
        $("#div8").css("display", "none");
        $("#div9").css("display", "none");
        $("#div10").css("display", "none");

        $("#txtCalibrationStdVal1").css("display", "block");
        $("#txtCalibrationStdVal2").css("display", "block");
        $("#txtCalibrationStdVal3").css("display", "block");
        $("#txtCalibrationStdVal4").css("display", "block");
        $("#txtCalibrationStdVal5").css("display", "block");
        $("#txtCalibrationStdVal6").css("display", "block");
        $("#txtCalibrationStdVal7").css("display", "block");
        $("#txtCalibrationStdVal8").css("display", "none");
        $("#txtCalibrationStdVal9").css("display", "none");
        $("#txtCalibrationStdVal10").css("display", "none");

        $("#txtCalibrationStdVal8").val('');
        $("#txtCalibrationStdVal9").val('');
        $("#txtCalibrationStdVal10").val('');

        $("#txtCalibrationAbsorbance1").css("display", "block");
        $("#txtCalibrationAbsorbance2").css("display", "block");
        $("#txtCalibrationAbsorbance3").css("display", "block");
        $("#txtCalibrationAbsorbance4").css("display", "block");
        $("#txtCalibrationAbsorbance5").css("display", "block");
        $("#txtCalibrationAbsorbance6").css("display", "block");
        $("#txtCalibrationAbsorbance7").css("display", "block");
        $("#txtCalibrationAbsorbance8").css("display", "none");
        $("#txtCalibrationAbsorbance9").css("display", "none");
        $("#txtCalibrationAbsorbance10").css("display", "none");

    }
    else if (intStdVal == "8") {
        $("#div1").css("display", "block");
        $("#div2").css("display", "block");
        $("#div3").css("display", "block");
        $("#div4").css("display", "block");
        $("#div5").css("display", "block");
        $("#div6").css("display", "block");
        $("#div7").css("display", "block");
        $("#div8").css("display", "block");
        $("#div9").css("display", "none");
        $("#div10").css("display", "none");

        $("#txtCalibrationStdVal1").css("display", "block");
        $("#txtCalibrationStdVal2").css("display", "block");
        $("#txtCalibrationStdVal3").css("display", "block");
        $("#txtCalibrationStdVal4").css("display", "block");
        $("#txtCalibrationStdVal5").css("display", "block");
        $("#txtCalibrationStdVal6").css("display", "block");
        $("#txtCalibrationStdVal7").css("display", "block");
        $("#txtCalibrationStdVal8").css("display", "block");
        $("#txtCalibrationStdVal9").css("display", "none");
        $("#txtCalibrationStdVal10").css("display", "none");

        $("#txtCalibrationStdVal9").val('');
        $("#txtCalibrationStdVal10").val('');

        $("#txtCalibrationAbsorbance1").css("display", "block");
        $("#txtCalibrationAbsorbance2").css("display", "block");
        $("#txtCalibrationAbsorbance3").css("display", "block");
        $("#txtCalibrationAbsorbance4").css("display", "block");
        $("#txtCalibrationAbsorbance5").css("display", "block");
        $("#txtCalibrationAbsorbance6").css("display", "block");
        $("#txtCalibrationAbsorbance7").css("display", "block");
        $("#txtCalibrationAbsorbance8").css("display", "block");
        $("#txtCalibrationAbsorbance9").css("display", "none");
        $("#txtCalibrationAbsorbance10").css("display", "none");

    }
    else if (intStdVal == "9") {
        $("#div1").css("display", "block");
        $("#div2").css("display", "block");
        $("#div3").css("display", "block");
        $("#div4").css("display", "block");
        $("#div5").css("display", "block");
        $("#div6").css("display", "block");
        $("#div7").css("display", "block");
        $("#div8").css("display", "block");
        $("#div9").css("display", "block");
        $("#div10").css("display", "none");

        $("#txtCalibrationStdVal1").css("display", "block");
        $("#txtCalibrationStdVal2").css("display", "block");
        $("#txtCalibrationStdVal3").css("display", "block");
        $("#txtCalibrationStdVal4").css("display", "block");
        $("#txtCalibrationStdVal5").css("display", "block");
        $("#txtCalibrationStdVal6").css("display", "block");
        $("#txtCalibrationStdVal7").css("display", "block");
        $("#txtCalibrationStdVal8").css("display", "block");
        $("#txtCalibrationStdVal9").css("display", "block");
        $("#txtCalibrationStdVal10").css("display", "none");

        $("#txtCalibrationStdVal10").val('');

        $("#txtCalibrationAbsorbance1").css("display", "block");
        $("#txtCalibrationAbsorbance2").css("display", "block");
        $("#txtCalibrationAbsorbance3").css("display", "block");
        $("#txtCalibrationAbsorbance4").css("display", "block");
        $("#txtCalibrationAbsorbance5").css("display", "block");
        $("#txtCalibrationAbsorbance6").css("display", "block");
        $("#txtCalibrationAbsorbance7").css("display", "block");
        $("#txtCalibrationAbsorbance8").css("display", "block");
        $("#txtCalibrationAbsorbance9").css("display", "block");
        $("#txtCalibrationAbsorbance10").css("display", "none");

    }
    else if (intStdVal == "10" || intStdVal > "10") {
        $("#txtCalibrationStdNo").val('10');
        $("#div1").css("display", "block");
        $("#div2").css("display", "block");
        $("#div3").css("display", "block");
        $("#div4").css("display", "block");
        $("#div5").css("display", "block");
        $("#div6").css("display", "block");
        $("#div7").css("display", "block");
        $("#div8").css("display", "block");
        $("#div9").css("display", "block");
        $("#div10").css("display", "block");

        $("#txtCalibrationStdVal1").css("display", "block");
        $("#txtCalibrationStdVal2").css("display", "block");
        $("#txtCalibrationStdVal3").css("display", "block");
        $("#txtCalibrationStdVal4").css("display", "block");
        $("#txtCalibrationStdVal5").css("display", "block");
        $("#txtCalibrationStdVal6").css("display", "block");
        $("#txtCalibrationStdVal7").css("display", "block");
        $("#txtCalibrationStdVal8").css("display", "block");
        $("#txtCalibrationStdVal9").css("display", "block");
        $("#txtCalibrationStdVal10").css("display", "block");


        $("#txtCalibrationAbsorbance1").css("display", "block");
        $("#txtCalibrationAbsorbance2").css("display", "block");
        $("#txtCalibrationAbsorbance3").css("display", "block");
        $("#txtCalibrationAbsorbance4").css("display", "block");
        $("#txtCalibrationAbsorbance5").css("display", "block");
        $("#txtCalibrationAbsorbance6").css("display", "block");
        $("#txtCalibrationAbsorbance7").css("display", "block");
        $("#txtCalibrationAbsorbance8").css("display", "block");
        $("#txtCalibrationAbsorbance9").css("display", "block");
        $("#txtCalibrationAbsorbance10").css("display", "block");

    }

    //if (strCurveType == "2" && intStdVal !== null) {
    //    intStdVal = '1';
    //    document.getElementById('txtCalibrationStdNo').value = "1";
    //}
    //if (strCurveType == "3" && intStdVal !== null) {
    //    intStdVal = '2';
    //    document.getElementById('txtCalibrationStdNo').value = "2";
    //}

    //if (intStdVal <= "6" && intStdVal == "1" && intStdVal !== null) {
    //    document.getElementById('ddlCalibrationCurveType').value = '2';

    //    document.getElementById('div1').style.display = 'block';
    //    document.getElementById('div2').style.display = 'none';
    //    document.getElementById('div3').style.display = 'none';
    //    document.getElementById('div4').style.display = 'none';
    //    document.getElementById('div5').style.display = 'none';
    //    document.getElementById('div6').style.display = 'none';

    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'none';
    //    $("#txtCalibrationStdVal2").val('');
    //    $("#txtCalibrationStdVal3").val('');
    //    $("#txtCalibrationStdVal4").val('');
    //    $("#txtCalibrationStdVal5").val('');
    //    $("#txtCalibrationStdVal6").val('');
    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';
    //}
    //else if (intStdVal <= "6" && intStdVal == "2" && intStdVal !== null) {
    //    document.getElementById('ddlCalibrationCurveType').value = '3';

    //    document.getElementById('div1').style.display = 'block';
    //    document.getElementById('div2').style.display = 'block';
    //    document.getElementById('div3').style.display = 'none';
    //    document.getElementById('div4').style.display = 'none';
    //    document.getElementById('div5').style.display = 'none';
    //    document.getElementById('div6').style.display = 'none';

    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'none';

    //    $("#txtCalibrationStdVal3").val('');
    //    $("#txtCalibrationStdVal4").val('');
    //    $("#txtCalibrationStdVal5").val('');
    //    $("#txtCalibrationStdVal6").val('');


    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';
    //}
    //else if (intStdVal <= "6" && intStdVal == "3" && intStdVal !== null && strCurveType !== "1" && strCurveType !== "2") {
    //    //document.getElementById('ddlCalibrationCurveType').value = '4';
    //    document.getElementById('div1').style.display = 'block';
    //    document.getElementById('div2').style.display = 'block';
    //    document.getElementById('div3').style.display = 'block';
    //    document.getElementById('div4').style.display = 'none';
    //    document.getElementById('div5').style.display = 'none';
    //    document.getElementById('div6').style.display = 'none';

    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'none';
    //    $("#txtCalibrationStdVal4").val('');
    //    $("#txtCalibrationStdVal5").val('');
    //    $("#txtCalibrationStdVal6").val('');

    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';

    //}
    //else if (intStdVal <= "6" && intStdVal == "4" && intStdVal !== null && strCurveType !== "1" && strCurveType !== "2") {
    //    //document.getElementById('ddlCalibrationCurveType').value = '5';
    //    document.getElementById('div1').style.display = 'block';
    //    document.getElementById('div2').style.display = 'block';
    //    document.getElementById('div3').style.display = 'block';
    //    document.getElementById('div4').style.display = 'block';
    //    document.getElementById('div5').style.display = 'none';
    //    document.getElementById('div6').style.display = 'none';

    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'none';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'none';
    //    $("#txtCalibrationStdVal5").val('');
    //    $("#txtCalibrationStdVal6").val('');

    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'none';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';
    //}
    //else if (intStdVal <= "6" && intStdVal == "5" && intStdVal !== null && strCurveType !== "1" && strCurveType !== "2") {
    //    //document.getElementById('ddlCalibrationCurveType').value = '5';
    //    document.getElementById('div1').style.display = 'block';
    //    document.getElementById('div2').style.display = 'block';
    //    document.getElementById('div3').style.display = 'block';
    //    document.getElementById('div4').style.display = 'block';
    //    document.getElementById('div5').style.display = 'block';
    //    document.getElementById('div6').style.display = 'none';

    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'none';
    //    $("#txtCalibrationStdVal6").val('');

    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'none';
    //}
    //else if (intStdVal <= "6" && intStdVal == "6" && intStdVal !== null && strCurveType !== "1" && strCurveType !== "2") {
    //    //document.getElementById('ddlCalibrationCurveType').value = '5';
    //    document.getElementById('div1').style.display = 'block';
    //    document.getElementById('div2').style.display = 'block';
    //    document.getElementById('div3').style.display = 'block';
    //    document.getElementById('div4').style.display = 'block';
    //    document.getElementById('div5').style.display = 'block';
    //    document.getElementById('div6').style.display = 'block';

    //    document.getElementById('txtCalibrationStdVal1').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal2').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal3').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal4').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal5').style.display = 'block';
    //    document.getElementById('txtCalibrationStdVal6').style.display = 'block';

    //    document.getElementById('txtCalibrationAbsorbance1').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance2').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance3').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance4').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance5').style.display = 'block';
    //    document.getElementById('txtCalibrationAbsorbance6').style.display = 'block';
    //}

    //Fun_CurveType();
}
// calculate standard value on dilution ratio and standard value change
function funDilutionRatio() {
    var intdilutionRatio = document.getElementById("txtCalibrationDiluRatio").value;    
    
    if (document.getElementById('CalibrationChkDilution').checked) {
        if (intdilutionRatio > 0 && intdilutionRatio != null) {

            document.getElementById("txtCalibrationStdVal2").value = (document.getElementById("txtCalibrationStdVal1").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal3").value = (document.getElementById("txtCalibrationStdVal2").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal4").value = (document.getElementById("txtCalibrationStdVal3").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal5").value = (document.getElementById("txtCalibrationStdVal4").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal6").value = (document.getElementById("txtCalibrationStdVal5").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal7").value = (document.getElementById("txtCalibrationStdVal6").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal8").value = (document.getElementById("txtCalibrationStdVal7").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal9").value = (document.getElementById("txtCalibrationStdVal8").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal10").value = (document.getElementById("txtCalibrationStdVal9").value / intdilutionRatio).toFixed(2);

            // Math.round(num * 100) / 100
        }
    }    
}

// calculate standard value on dilution ratio and standard value change
function funStdValonChange() {
    var intdilutionRatio = document.getElementById("txtCalibrationDiluRatio").value;

    if (document.getElementById('CalibrationChkDilution').checked) {
        if (intdilutionRatio > 0 && intdilutionRatio != null) {

            document.getElementById("txtCalibrationStdVal2").value = (document.getElementById("txtCalibrationStdVal1").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal3").value = (document.getElementById("txtCalibrationStdVal2").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal4").value = (document.getElementById("txtCalibrationStdVal3").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal5").value = (document.getElementById("txtCalibrationStdVal4").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal6").value = (document.getElementById("txtCalibrationStdVal5").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal7").value = (document.getElementById("txtCalibrationStdVal6").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal8").value = (document.getElementById("txtCalibrationStdVal7").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal9").value = (document.getElementById("txtCalibrationStdVal8").value / intdilutionRatio).toFixed(2);
            document.getElementById("txtCalibrationStdVal10").value = (document.getElementById("txtCalibrationStdVal9").value / intdilutionRatio).toFixed(2);

        }
    }
}


// Calibration Functions
function showDilRation() {
    //alert(document.getElementById('CalibrationChkDilution').checked);
    if (document.getElementById('CalibrationChkDilution').checked) {
        document.getElementById('divCalibrationDiluRatio').style.display = 'block';

        document.getElementById('txtCalibrationStdVal1').disabled = false;
        document.getElementById('txtCalibrationStdVal2').disabled = true;
        document.getElementById('txtCalibrationStdVal3').disabled = true;
        document.getElementById('txtCalibrationStdVal4').disabled = true;
        document.getElementById('txtCalibrationStdVal5').disabled = true;
        document.getElementById('txtCalibrationStdVal6').disabled = true;
        document.getElementById('txtCalibrationStdVal7').disabled = true;
        document.getElementById('txtCalibrationStdVal8').disabled = true;
        document.getElementById('txtCalibrationStdVal9').disabled = true;
        document.getElementById('txtCalibrationStdVal10').disabled = true;


    } else {
        document.getElementById('divCalibrationDiluRatio').style.display = 'none';

        document.getElementById('txtCalibrationStdVal1').disabled = false;
        document.getElementById('txtCalibrationStdVal2').disabled = false;
        document.getElementById('txtCalibrationStdVal3').disabled = false;
        document.getElementById('txtCalibrationStdVal4').disabled = false;
        document.getElementById('txtCalibrationStdVal5').disabled = false;
        document.getElementById('txtCalibrationStdVal6').disabled = false;
        document.getElementById('txtCalibrationStdVal7').disabled = false;
        document.getElementById('txtCalibrationStdVal8').disabled = false;
        document.getElementById('txtCalibrationStdVal9').disabled = false;
        document.getElementById('txtCalibrationStdVal10').disabled = false;

    }
}