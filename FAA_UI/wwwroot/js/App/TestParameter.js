﻿
var gTestCode = '';
$(document).ready(function () {
    /*BindTableTestParameter();*/
    GetAllMaster();
    funReagenct();
    GetAllCalibrationMst();

    BindTableTestReflex();
    BindTableCarryOver();
    BindTableTestExternal();
    BindTableTestCalculated();
    GetTestSequence();

    GetTestProfile();
    GetTestCode();

    //GetSampleType();
    //BindTableReferenceRange();// hello

    $('.select2').select2();
    
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
    $("#txtWashVolume").focusout(function (e) {
        if (Number($("#txtWashVolume").val()) < 50 || Number($("#txtWashVolume").val()) > 250) {
            $("#txtWashVolume").val('');
            Message("warning", "Washing Volume limit minimum volume 50 && maximum volumn 250 only");

        }
        else {
        }
    });

    $(".divCalibrationInput").focusout(function () {
        $("#lblErrorMsg").text("");
        if ($("#txtCalibrationStdVal1").val() != "" && $("#txtCalibrationStdVal2").val() != "") {
            if (Number($("#txtCalibrationStdVal1").val()) > Number($("#txtCalibrationStdVal2").val())) {
                if (Number($("#txtCalibrationStdVal2").val()) < Number($("#txtCalibrationStdVal3").val()) && $("#txtCalibrationStdVal3").val() != "") {
                    $("#txtCalibrationStdVal3").focus()
                    Message("error", "Decending order");
                    $("#lblErrorMsg").text("Decending order");
                }
                else if (Number($("#txtCalibrationStdVal3").val()) < Number($("#txtCalibrationStdVal4").val()) && $("#txtCalibrationStdVal4").val() != "") {
                    $("#txtCalibrationStdVal4").focus()
                    Message("error", "Decending order");
                    $("#lblErrorMsg").text("Decending order");

                }
                else if (Number($("#txtCalibrationStdVal4").val()) < Number($("#txtCalibrationStdVal5").val()) && $("#txtCalibrationStdVal5").val() != "") {
                    $("#txtCalibrationStdVal5").focus()
                    Message("error", "Decending order");
                    $("#lblErrorMsg").text("Decending order");
                }
                else if (Number($("#txtCalibrationStdVal5").val()) < Number($("#txtCalibrationStdVal6").val()) && $("#txtCalibrationStdVal6").val() != "") {
                    $("#txtCalibrationStdVal6").focus()
                    Message("error", "Decending order");
                    $("#lblErrorMsg").text("Decending order");

                }
                else if (Number($("#txtCalibrationStdVal6").val()) < Number($("#txtCalibrationStdVal7").val()) && $("#txtCalibrationStdVal7").val() != "") {
                    $("#txtCalibrationStdVal7").focus()
                    Message("error", "Decending order");
                    $("#lblErrorMsg").text("Decending order");

                }
                else if (Number($("#txtCalibrationStdVal7").val()) < Number($("#txtCalibrationStdVal8").val()) && $("#txtCalibrationStdVal8").val() != "") {
                    $("#txtCalibrationStdVal8").focus()
                    Message("error", "Decending order");
                    $("#lblErrorMsg").text("Decending order");

                }
                else if (Number($("#txtCalibrationStdVal8").val()) < Number($("#txtCalibrationStdVal9").val()) && $("#txtCalibrationStdVal9").val() != "") {
                    $("#txtCalibrationStdVal9").focus()
                    Message("error", "Decending order");
                    $("#lblErrorMsg").text("Decending order");

                }
                else if (Number($("#txtCalibrationStdVal9").val()) < Number($("#txtCalibrationStdVal10").val()) && $("#txtCalibrationStdVal10").val() != "") {
                    $("#txtCalibrationStdVal10").focus()
                    Message("error", "Decending order");
                    $("#lblErrorMsg").text("Decending order");

                }
            }
            else if (Number($("#txtCalibrationStdVal1").val()) < Number($("#txtCalibrationStdVal2").val())) {
                if ((Number($("#txtCalibrationStdVal2").val()) > Number($("#txtCalibrationStdVal3").val())) && $("#txtCalibrationStdVal3").val() != "") {
                    $("#txtCalibrationStdVal3").focus()
                    Message("error", "Asecending order");
                    $("#lblErrorMsg").text("Asecending order");


                }
                else if ((Number($("#txtCalibrationStdVal3").val()) > Number($("#txtCalibrationStdVal4").val())) && $("#txtCalibrationStdVal4").val() != "") {
                    $("#txtCalibrationStdVal4").focus()
                    Message("error", "Asecending order");
                    $("#lblErrorMsg").text("Asecending order");

                }
                else if ((Number($("#txtCalibrationStdVal4").val()) > Number($("#txtCalibrationStdVal5").val())) && $("#txtCalibrationStdVal5").val() != "") {
                    $("#txtCalibrationStdVal5").focus()
                    Message("error", "Asecending order");
                    $("#lblErrorMsg").text("Asecending order");

                }
                else if ((Number($("#txtCalibrationStdVal5").val()) > Number($("#txtCalibrationStdVal6").val())) && $("#txtCalibrationStdVal6").val() != "") {
                    $("#txtCalibrationStdVal6").focus()
                    Message("error", "Asecending order");
                    $("#lblErrorMsg").text("Asecending order");

                }
                else if ((Number($("#txtCalibrationStdVal6").val()) > Number($("#txtCalibrationStdVal7").val())) && $("#txtCalibrationStdVal7").val() != "") {
                    $("#txtCalibrationStdVal7").focus()
                    Message("error", "Asecending order");
                    $("#lblErrorMsg").text("Asecending order");

                }
                else if ((Number($("#txtCalibrationStdVal7").val()) > Number($("#txtCalibrationStdVal8").val())) && $("#txtCalibrationStdVal8").val() != "") {
                    $("#txtCalibrationStdVal8").focus()
                    Message("error", "Asecending order");
                    $("#lblErrorMsg").text("Asecending order");

                }
                else if ((Number($("#txtCalibrationStdVal8").val()) > Number($("#txtCalibrationStdVal9").val())) && $("#txtCalibrationStdVal9").val() != "") {
                    $("#txtCalibrationStdVal9").focus()
                    Message("error", "Asecending order");
                    $("#lblErrorMsg").text("Asecending order");

                }

                else if ((Number($("#txtCalibrationStdVal9").val()) > Number($("#txtCalibrationStdVal10").val())) && $("#txtCalibrationStdVal10").val() != "") {
                    $("#txtCalibrationStdVal10").focus()
                    Message("error", "Asecending order");
                    $("#lblErrorMsg").text("Asecending order");

                }
            }
        }
        
    })
});

function funPerentTab(Tab) {
    if (Tab == "TestProgramming-tab") {
        $("#divNewTestCode").css("display", "flex");
        GetAllMaster();
    }
    else if (Tab == "TestProfile-tab") {
        GetTestCode();
        GetTestProfile();
    }
    else if (Tab == "TestSequence-tab") {
        GetTestSequence();
    }
    else if (Tab == "TestCalculate-tab") {
        GetAllMaster();
        BindTableTestCalculated();

        $("#ddlCalTestId").select2({
            dropdownParent: $("#modelTestCalculate"),
            width: "100%"
        });
    }
    else if (Tab == "TestExternal-tab") {
        BindTableTestExternal();
    }
    else if (Tab == "TestReflex-tab") {
        GetTestCode();
        BindTableTestReflex();
        $("#ddlRflxTestName").select2({
            dropdownParent: $("#modelTestReflex"),
            width: "100%"
        });
    }
    else if (Tab == "CarryOver-tab") {
        BindTableCarryOver();
        GetTestCode();
        $("#ddlTest1").select2({
            dropdownParent: $("#modelCarryOver"),
            width: "100%"
        });
        $("#ddlTest2").select2({
            dropdownParent: $("#modelCarryOver"),
            width: "100%"
        });
    }
}

function funChildTab(Tab) {
    if (Tab == "Basic") {
        $("#divNewTestCode").css("display", "flex");
        $("#btnEditTestCode").css("display", "block");
        
     /*   GetAllMaster();*/
    }
    else if (Tab == "Reference") {
        $("#divNewTestCode").css("display", "none");
        $("#btnEditTestCode").css("display", "none");
        GetSampleType();
        BindTableReferenceRange();
    }
    else if (Tab == "Calibration") {
        $("#divNewTestCode").css("display", "none");
        $("#btnEditTestCode").css("display", "none");
     
        GetMonthYear();
        GetCaliLotNo();
        BindTableClibrationLotNo($("#ddlTestCode").val());
    }
}

//Start Test Parameter

function funReagenct() {
    if ($("#ddlReagent").val() == "2") {
        $(".divReagent2").css("display", "block");
    }
    else {
        $(".divReagent2").css("display", "none");
    }
}

function inputNumber(val) {
    val.value = parseFloat(val.value).toFixed(2).toString();
}

function funSecondaryChange() {
    if ($("#ddlW1").val() == "") {
        Message("error", "Before select primary filter");
        $("#ddlW2").val('');
    }
    else if (Number($("#ddlW1").val()) > Number($("#ddlW2").val())) {
        Message("error", "Secondary fitter should be higher than primary fitter");
        $("#ddlW2").val('');

    }
}

function GetAllMaster() {
    $.ajax({
        method: "GET",
        url: '/TestParameter/GetAllMaster',
        success: function (result) {
            if (result != null) {

                //Unique Test Id
                $("#txtUniqueTestId").val(result.uniqueTestId);

                //Test Code 
                $("#ddlTestCode").empty();
                $("#ddlCloneTestCode").empty();

                $("#ddlCalTestId").empty();
                var testCode = '';
                for (var i = 0; i < result.testParameter.length; i++) {
                    
                    testCode += '<option value="' + result.testParameter[i].testId + '">' + result.testParameter[i].testCode + '</option>'
                }
                $("#ddlTestCode").append(testCode).trigger('change');
                $("#ddlCloneTestCode").append('<option value="" selected disabled>Select Test Code to clone from</option>')
                $("#ddlCloneTestCode").append(testCode);
                $("#ddlCloneTestCode").select2({
                    dropdownParent: $("#modelTestClone"),
                    width: "100%"
                });
                $("#ddlCalTestId").append(testCode);

                if (gTestCode != '') {
                    $("#ddlTestCode").append(gTestCode).trigger('change');
                }

                //Method
                $("#ddlMethod").empty();
                var method = '<option value=""></option>';
                for (var i = 0; i < result.method.length; i++) {
                    method += '<option value="' + result.method[i].mthdId + '">' + result.method[i].mthdType +'</option>'
                }
                $("#ddlMethod").append(method);

                //Primary and Secondary filter
                $("#ddlW1").empty();
                $("#ddlW2").empty();
                
                var primaryFilter = '<option value=""></option>';
                var secondaryFilter = '<option value=""></option>';
                for (var i = 0; i < result.filter.length; i++) {
                    primaryFilter += '<option value="' + result.filter[i].fltrId + '">' + result.filter[i].filter + '</option>'
                    secondaryFilter += '<option value="' + result.filter[i].fltrId + '">' + result.filter[i].filter + '</option>'
                }
                secondaryFilter += '<option value="0">No</option>';
                $("#ddlW1").append(primaryFilter);
                $("#ddlW2").append(secondaryFilter);

                //Unit
                $("#ddlUnit").empty();
                $("#ddlCalUnit").empty();
                $("#ddlExtrnUnit").empty();
                var unit = '<option value=""></option>';
                for (var i = 0; i < result.unit.length; i++) {
                    unit += '<option value="' + result.unit[i].untId + '">' + result.unit[i].untName + '</option>'
                }
                $("#ddlUnit").append(unit);
                $("#ddlCalUnit").append(unit);
                $("#ddlExtrnUnit").append(unit);
            }
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");

        }
    });
}

function funIncubationTime(Incubation) {
    if (Incubation == "R1") {
        var R1 = Number($("#hdR1IncubationTimeCycle").val()) * Number($("#ddlR1IncubationTime").val());
        $("#txtR1IncubationTimeSec").val(R1)
    }
    else if (Incubation == "R2") {
        $("#txtR2IncubationTimeSec").val($("#hdR2IncubationTimeCycle").val() * $("#ddlR2IncubationTime").val());
    }
}
function funTestCode() {
    if ($("#ddlTestCode").val() != "") {
        UpdateShowTestParameter($("#ddlTestCode").val());
        BindTableReferenceRange();
        $("#lblCalLotTestCode").val($("#ddlTestCode").find("option:selected").text());
        BindTableClibrationLotNo($("#ddlTestCode").val());
        GetAllCalibrationMst();
        UpdateShowByTestIdCalibration($("#ddlTestCode").val());
    }
}
function modelTestCodeUpdate() {
    $("#hdNewTestId").val($("#ddlTestCode").val());
    $("#txtNewTestCode").val($("#ddlTestCode").find("option:selected").text());
    $("#modelTestCode").modal('show');
}

function saveTestCode() {

    if ($("#txtNewTestCode").val() == "") {
        $("#txtNewTestCode").focus();
        Message("error", "Test Code is required");
        return false;
    }
    else {
        $("#preloader").css("display", "block");

        $.ajax({
            method: "GET",
            url: '/TestParameter/DuplicateCheckTestCode',
            data: { TestId: $("#hdNewTestId").val(), TestCode: $("#txtNewTestCode").val() },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    if ($("#hdNewTestId").val() != "" && $("#txtNewTestCode").val() != "") {
                        $("#ddlTestCode").select2('destroy');
                        $("#ddlTestCode").find("option:selected").text($("#txtNewTestCode").val());
                        $("#ddlTestCode").find("option:selected").val($("#hdNewTestId").val());
                        $("#ddlTestCode").select2();
                        
                        ClearTestCode();
                        Message("success", "Test Code is updated. Enter test parameters and then save.");

                    }
                    else if ($("#txtNewTestCode").val() != "") {
                        ClearTestParameter();
                        
                        gTestCode = '<option value="' + $("#txtNewTestCode").val() + '" selected>' + $("#txtNewTestCode").val() + '</option>';
                        ClearTestCode();
                        Message("success", "Test Code is saved. Enter test parameters and then save.");

                    }
                    else {
                        $("#txtNewTestCode").focus();
                        Message("error", "Test Code is required");
                        return false;
                    }
                }
                else {
                    $("#txtNewTestCode").focus();
                    Message("error", "Test Code is already exist");
                    return false;
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}
function ClearTestCode() {
    $("#hdNewTestId").val('');
    $("#txtNewTestCode").val('');
    $("#modelTestCode").modal('hide');
}
function funAutoDilute() {
    if ($("#chkAutoDilute").prop("checked") == true) {
        $("#divAutoDilute").css("display", "flex");
    }
    else {
        $("#divAutoDilute").css("display", "none");
    }
}
function funDecimalLimit(e) {
    if (e.value > 5) {
        e.value = '';
        Message("warning", "Decimal limit 5 only");
    }
}
function funVolumeLimit(e) {
    if (e.value > 500) {
        e.value = '';
        Message("warning", "Volume limit 500 only");
    }
}

function funBlankValueValidation(e) {
    if (e.value > 4.5) {
        e.value = '';
        Message("warning", "Value limit 4.5 only");
    }
}


function BindTableTestParameter() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/TestParameter/GetAllTestParameter",
        method: 'get',
        success: OnsuccessTestParameter
    });
}

function OnsuccessTestParameter(response) {
    $('#tblTestParameter').DataTable().destroy();
    $('#tblTestParameter').DataTable({
        //scrollX: true,
        //scroll: true, // used for fixed header
        //scrollY: 300, // used for fixed Hieght
        //bProcessing: true,
        //bLenghtChange: true,
        //retrieve: true,
        //bfilter: true,
        //bSort: true,
        //bPaging: true,
        data: response,
        columns: [
            { data: 'drId', title: 'sr no' },
            { data: 'drName', title: 'Doctor' },
            { data: 'drDepartment', title: 'Department' },
            { data: 'drRemark', title: 'Remark' },
            {
                data: "drId", title: 'Action',
                render: function (data, type, row, meta) {
                    return '<button type="button" class="btn btn-primary" onclick = UpdateShowTestParameter(' + row.drId + ') > Update </button>&nbsp;<button type="button" class="btn btn-danger" onclick="DeleteConfirmationTestParameter(' + row.drId + ')">Delete</button>';
                }
            },
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var index = iDisplayIndex + 1;
            $('td:eq(0)', nRow).html(index);
            return nRow;
        }
    });
    $("#preloader").css("display", "none");

}


function funSaveCloneTestParameter() {
    var returnValue = true;
    var CloneTestCode = $("#ddlCloneTestCode").find("option:selected").text();
    var CloneTestCodeId = $("#ddlCloneTestCode").val();
    var CloneTestCodeLabel = $("#txtCloneTestCodeLabel").val();
    if (CloneTestCodeId == "") {
        $("#ddlCloneTestCode").focus();
        Message("error", "From test code is required");
        returnValue = false;
    }
    else if (CloneTestCodeLabel == "") {
        $("#txtCloneTestCodeLabel").focus();
        Message("error", "New test code is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        $.ajax({
            method: "GET",
            url: '/TestParameter/SaveCloneTestParameter',
            data: { CloneTestCodeId: CloneTestCodeId, CloneTestCode: CloneTestCode, CloneTestCodeLabel: CloneTestCodeLabel },
            crossDomain: true,
            success: function (result) {
                if (result > 0) {
                    $("#ddlCloneTestCode").append('<option value="' + result + '">' + $("#txtCloneTestCodeLabel").val() + '</option>')
                    $("#ddlTestCode").append('<option value="' + result + '">' + $("#txtCloneTestCodeLabel").val() + '</option>')
                    Message("success", $("#txtCloneTestCodeLabel").val() + " saved successfully");
                    $("#ddlCloneTestCode").val('').trigger('change');
                    $("#txtCloneTestCodeLabel").val('');
                    $("#modelTestClone").modal("hide");
                }
                else {
                    Message("error", $("#txtCloneTestCodeLabel").val() + " test Code already exist");
                }
                ClearTestParameter();
                BindTableTestParameter()
                $("#preloader").css("display", "none");

            },
            error: function () {
                Message("error", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }

        });
    }
}

function SaveTestParameter() {
    var returnValue = true;
    if ($("#txtUniqueTestId").val() == "") {
        $("#txtUniqueTestId").focus();
        Message("error", "Unique Test ID is required");
        returnValue = false;
    }
    else if ($("#ddlTestCode").find("option:selected").text() == "") {
        $("#ddlTestCode").focus();
        Message("error", "Test Code is required");
        returnValue = false;
    }
    else if ($("#txtTestName").val() == "") {
        $("#txtTestName").focus();
        Message("error", "Test Name is required");
        returnValue = false;
    }
    else if ($("#ddlMethod").val() == "") {
        $("#ddlMethod").focus();
        Message("error", "Method is required");
        returnValue = false;
    }
    else if ($("#ddlW1").val() == "") {
        $("#ddlW1").focus();
        Message("error", "Primary Filter is required");
        returnValue = false;
    }
    else if ($("#ddlW2").val() == "") {
        $("#ddlW2").focus();
        Message("error", "Secondary Filter is required");
        returnValue = false;
    }
    else if ($("#txtDecimals").val() == "") {
        $("#txtDecimals").focus();
        Message("error", "Decimal is required");
        returnValue = false;
    }
    else if ($("#ddlUnit").val() == "") {
        $("#ddlUnit").focus();
        Message("error", "Unit is required");
        returnValue = false;
    }
    else if ($("#ddlDirection").val() == "") {
        $("#ddlDirection").focus();
        Message("error", "Direction is required");
        returnValue = false;
    }
    else if ($("#ddlReagent").val() == "") {
        $("#ddlReagent").focus();
        Message("error", "Reagenct is required");
        returnValue = false;
    }
    else if ($("#txtA").val() == "") {
        $("#txtA").focus();
        Message("error", "M is required");
        returnValue = false;
    }
    else if ($("#txtB").val() == "") {
        $("#txtB").focus();
        Message("error", "C is required");
        returnValue = false;
    }
    else if ($("#txtSampleVolume").val() == "") {
        $("#txtSampleVolume").focus();
        Message("error", "Sample Volume is required");
        returnValue = false;
    }
    else if ($("#txtR1Volume").val() == "") {
        $("#txtR1Volume").focus();
        Message("error", "R1 Volume is required");
        returnValue = false;
    }
    else if ($("#txtR2Volume").val() == "" && $("#ddlReagent").val() == "2") {
        $("#txtR2Volume").focus();
        Message("error", "R2 Volume is required");
        returnValue = false;
    }
    else if ($("#ddlR1IncubationTime").val() == "") {
        $("#ddlR1IncubationTime").focus();
        Message("error", "R1 Incubation Time is required");
        returnValue = false;
    }
    else if ($("#ddlR2IncubationTime").val() == "" && $("#ddlReagent").val() == "2") {
        $("#ddlR2IncubationTime").focus();
        Message("error", "R2 Incubation Time is required");
        returnValue = false;
    }
    else if ($("#ddlR1StrSpeed").val() == "") {
        $("#ddlR1StrSpeed").focus();
        Message("error", "R1 StrSpeed is required");
        returnValue = false;
    }
    else if ($("#ddlR2StrSpeed").val() == "" && $("#ddlReagent").val() == "2") {
        $("#ddlR2StrSpeed").focus();
        Message("error", "R2 StrSpeed is required");
        returnValue = false;
    }
    else if ($("#txtReadTime").val() == "") {
        $("#txtReadTime").focus();
        Message("error", "Read Time is required");
        returnValue = false;
    }
    else if ($("#chkAutoDilute").prop("checked") == true && $("#txtSubDpltionLmt").val() == "") {
        $("#txtSubDpltionLmt").focus();
        Message("error", "Sub DpltionLmt is required");
        returnValue = false;
    }
    else if ($("#chkAutoDilute").prop("checked") == true && $("#txtLinearity").val() == "") {
        $("#txtLinearity").focus();
        Message("error", "Linearity is required");
        returnValue = false;
    }
    else if ($("#chkAutoDilute").prop("checked") == true && $("#txtSampleScale").val() == "") {
        $("#txtSampleScale").focus();
        Message("error", "Sample Scale is required");
        returnValue = false;
    }

    else {
        $("#preloader").css("display", "block");

        var obj = {
            TestID: $("#hdTest_ID").val(),
            UniqueTestId: $("#txtUniqueTestId").val(),
            TestCode: $("#ddlTestCode").find("option:selected").text(),
            TestName: $("#txtTestName").val(),
            Method: $("#ddlMethod").val(),
            W1: $("#ddlW1").val(),
            W2: $("#ddlW2").val(),
            Decimals: $("#txtDecimals").val(),
            Unit: $("#ddlUnit").val(),
            Direction: $("#ddlDirection").val(),
            Reagent: $("#ddlReagent").val(),
            A: $("#txtA").val(),
            B: $("#txtB").val(),
            SampleVolume: $("#txtSampleVolume").val(),
            R1volume: $("#txtR1Volume").val(),
            R2volume: $("#txtR2Volume").val(),
            R1IncubationTime: $("#ddlR1IncubationTime").val(),
            R1incubationTimeSec: $("#txtR1IncubationTimeSec").val(),
            R2IncubationTime: $("#ddlR2IncubationTime").val(),
            R2incubationTimeSec: $("#txtR2IncubationTimeSec").val(),
            R1StrSpeed: $("#ddlR1StrSpeed").val(),
            R2StrSpeed: $("#ddlR2StrSpeed").val(),
            ReadTime: $("#txtReadTime").val(),
            SubDpltionLmt: $("#txtSubDpltionLmt").val(),
            Linearity: $("#txtLinearity").val(),
            //EquationBConstantC: $("#txtEquationBConstantC").val(),
            AutoDilute: $("#chkAutoDilute").prop("checked"),
            BlankValue: $("#txtBlankValue").val(),
            BlankHigh: $("#txtBlankHigh").val(),
            BlankLow: $("#txtBlankLow").val(),
            SampleScale: $("#txtSampleScale").val(),
        }
        $.ajax({
            method: "POST",
            url:'/TestParameter/SaveTestParameter',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result) {
                    if ($("#hdTest_ID").val() > 0) {
                        Message("success", "Updated Data Successfully");
                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    gTestCode = '';
                    ClearTestParameter();
                    BindTableTestParameter()
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }

        });
    }
}

function UpdateShowTestParameter(TestId) {
    $("#preloader").css("display", "block");
    $.ajax({
        method: "GET",
        url:'/TestParameter/GetByIdTestParameter',
        data: { Id: TestId },
        crossDomain: true,
        success: function (result) {
            if (result != null) {
                $("#hdTest_ID").val(result.testId);
                $("#txtUniqueTestId").val(result.uniqueTestId);
                $("#ddlTestCode").find("option:selected").val(result.testId);
                $("#ddlTestCode").find("option:selected").text(result.testCode);
           /*     $("#ddlTestCode").val(result.testCode);*/
                $("#txtTestName").val(result.testName);
                $("#ddlMethod").val(result.method);
                $("#ddlW1").val(result.w1);
                $("#ddlW2").val(result.w2);
                $("#txtDecimals").val(result.decimals);
                $("#ddlUnit").val(result.unit);
                $("#ddlDirection").val(result.direction);
                $("#ddlReagent").val(result.reagent);
                funReagenct();
                $("#txtA").val(result.a);
                $("#txtB").val(result.b);
                $("#txtSampleVolume").val(result.sampleVolume);

                $("#txtR1Volume").val(result.r1volume);
                $("#txtR2Volume").val(result.r2volume);
                $("#ddlR1IncubationTime").val(result.r1incubationTime);
                $("#txtR1IncubationTimeSec").val(result.r1incubationTimeSec);
                $("#ddlR2IncubationTime").val(result.r2incubationTime);
                $("#txtR2IncubationTimeSec").val(result.r2incubationTimeSec);
                $("#ddlR1StrSpeed").val(result.r1strSpeed);
                $("#ddlR2StrSpeed").val(result.r2strSpeed);
                $("#txtReadTime").val(result.readTime);
                $("#txtSubDpltionLmt").val(result.subDpltionLmt);
                $("#txtLinearity").val(result.linearity);
               // $("#txtEquationBConstantC").val(result.EquationBConstantC);
                $("#chkAutoDilute").prop("checked", result.autoDilute);
                if (result.autoDilute) {
                    $("#divAutoDilute").css("display", "flex");
                }
                else {
                    $("#divAutoDilute").css("display", "none");
                }
                $("#txtBlankValue").val(result.blankValue);
                $("#txtBlankLow").val(result.blankLow);
                $("#txtBlankHigh").val(result.blankHigh);
                $("#txtSampleScale").val(result.sampleScale);
                $("#btnTestSave").html("Update");
                $("#modelTestParameter").modal('show');
            }
            else {

              /*  Message("error", "Somthing went wrong");*/
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function DeleteTestParameter() {
    $("#preloader").css("display", "block");

    var TestId = $("#hdTest_ID").val();
    if (TestId > 0) {
        $.ajax({
            method: "GET",
            url:'/TestParameter/DeleteTestParameter',
            data: { TestId: TestId },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    ClearTestParameter();
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }

        });
    }
}

function ClearTestParameter() {
    $("#hdTest_ID").val('');
    $("#txtUniqueTestId").val('');
   /* $("#ddlTestCode").val('');*/
    $("#txtTestName").val('');
    $("#ddlMethod").val('');
    $("#ddlW1").val('');
    $("#ddlW2").val('');
    $("#txtDecimals").val('');
    $("#ddlUnit").val('');
    $("#ddlDirection").val('');
    $("#ddlReagent").val('');
    $("#txtA").val('');
    $("#txtB").val('');
    $("#txtSampleVolume").val('');
    $("#txtR1Volume").val('');
    $("#txtR2Volume").val('');
    $("#ddlR1IncubationTime").val('');
    $("#txtR1IncubationTimeSec").val('');
    $("#ddlR2IncubationTime").val('');
    $("#txtR2IncubationTimeSec").val('');
    $("#ddlR1StrSpeed").val('');
    $("#ddlR2StrSpeed").val('');
    $("#txtReadTime").val('');
    $("#txtSubDpltionLmt").val('');
    $("#txtLinearity").val('');
    $("#txtEquationBConstantC").val('');
    $("#chkAutoDilute").prop('checked', false);
    $("#txtBlankValue").val('');
    $("#txtBlankHigh").val('');
    $("#txtBlankLow").val('');
    $("#txtSampleScale").val('');
    $("#divAutoDilute").css("display", "none");
    $("#btnTestSave").html("Save");
    GetAllMaster();
}



//End Test Parameter


//Start Reference Range

function GetSampleType() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url:'/DataDirectory/GetAllSampleType',
        crossDomain: true,
        success: function (result) {
            if (result != null) {
                var data = '<option value""></option>';
                for (var i = 0; i < result.length; i++) {
                    data += '<option value="' + result[i].smpId + '">' + result[i].smpType + '</option>'
                }
                $("#ddlSampleType").append(data);
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function BindTableReferenceRange() {
    $("#preloader").css("display", "block");

    $.ajax({
        
       // url: "/TestParameter/GetAllReferenceRange?TestCode=DLE001",
       url: "/TestParameter/GetAllReferenceRange?TestCode=" + $("#ddlTestCode").val(),
        method: 'get',
        success: OnsuccessReferenceRange
    });
}

function OnsuccessReferenceRange(response) {
    $('#tblTestReferenceRange').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblRefrence").css("display", "block");
        $("#divRefrenceNoRecordFound").empty();
        $('#tblTestReferenceRange').DataTable({
            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 300, // used for fixed Hieght
            //bProcessing: true,
            //bLenghtChange: true,
            //retrieve: true,
            //bfilter: true,
            //bSort: true,
            //bPaging: true,
            data: response,
            columns: [
                { data: 'smpType', title: 'Sample Type' },
                {
                    data: "gender", title: 'Gender',
                    render: function (data, type, row, meta) {
                        if (row.gender == 1) {
                            return 'Male';
                        }
                        else {
                            return 'Female';
                        }
                    }
                },
                {
                    data: "ageMin", title: 'Age Unit',
                    render: function (data, type, row, meta) {
                        return row.ageMin + "-" + row.ageMax + " " + row.ageUnit
                    }
                },

                { data: 'refLow', title: 'Low' },
                { data: 'refHigh', title: 'High' },
             
                {
                    data: "refId", title: 'Action',
                    render: function (data, type, row, meta) {
                        return '<a type="button" style="color:#656FEA;" class="" href="#" onclick= UpdateShowReferenceRange(' + row.refId + ') data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Refrence Range"><i class="bi bi-pencil-fill"></i></a>&nbsp;&nbsp;<a type="button" class="" href="#" style="color:red" onclick="DeleteConfirmationReferenceRange(' + row.refId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Refrence Range"><i class="bi bi-trash"></i></a>';
                        //return '<button type="button" class="btn btn-primary" onclick= UpdateShowReferenceRange(' + row.refId + ') ><b><i class="bi bi-pencil-fill"></i></b></button>&nbsp;<button type="button" class="btn btn-danger" onclick="DeleteConfirmationReferenceRange(' + row.refId + ')"><b><i class="bi bi-trash"></i></b></button>';
                    }
                },
            ],


            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblRefrence").css("display", "none");
        $("#divRefrenceNoRecordFound").empty();
        $("#divRefrenceNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
    $("#preloader").css("display", "none");

    //tbl_TestRefPrint - Print

    $('#tbl_TestRefPrint').DataTable().destroy();
    $('#tbl_TestRefPrint').DataTable({
        //scrollX: true,
        //scroll: true, // used for fixed header
        //scrollY: 300, // used for fixed Hieght
        bProcessing: false,
        bLenghtChange: false,
        retrieve: false,
        bfilter: false,
        bSort: false,
        bPaging: false,
        bPaginate: false,
        bInfo: false,
        bsearching: false,
        data: response,
        columns: [
            { data: 'smpType', title: 'Sample Type' },
            {
                data: "gender", title: 'Gender',
                render: function (data, type, row, meta) {
                    if (row.gender == 1) {
                        return 'Male';
                    }
                    else {
                        return 'Female';
                    }
                }
            },

            { data: 'refLow', title: 'Lower' },
            { data: 'refHigh', title: 'Higher' },
            {
                data: "ageMin", title: 'Age Unit',
                render: function (data, type, row, meta) {
                    return row.ageMin + "-" + row.ageMax + " " + row.ageUnit
                }
            },
            
        ],
    });
}

function SaveReferenceRange() {
    var returnValue = true;
    if ($("#ddlGender").val() == "") {
        $("#ddlGender").focus();
        Message("error", "Gender is required");
        returnValue = false;
    }
    else if ($("#ddlSampleType").val() == "") {
        $("#ddlSampleType").focus();
        Message("error", "Sample Type is required");
        returnValue = false;
    }
    else if ($("#txtMinAge").val() == "") {
        $("#txtMinAge").focus();
        Message("error", "Minimum Age is required");
        returnValue = false;
    }
    else if ($("#txtMaxAge").val() == "") {
        $("#txtMaxAge").focus();
        Message("error", "Maximum Age is required");
        returnValue = false;
    }
    else if ($("#txtMaxAge").val() < $("#txtMinAge").val()) {
        $("#txtMaxAge").focus();
        Message("error", "Should be maximum age greater then or equal to minimum age");
        returnValue = false;
    }
    else if ($("#ddlAgeUnit").val() == "") {
        $("#ddlAgeUnit").focus();
        Message("error", "Unit is required");
        returnValue = false;
    }
    else if ($("#txtLower").val() == "") {
        $("#txtLower").focus();
        Message("error", "Lower is required");
        returnValue = false;
    }
    else if ($("#txtHigh").val() == "") {
        $("#txtHigh").focus();
        Message("error", "High is required");
        returnValue = false;
    }
    else if ($("#txtHigh").val() < $("#txtLower").val()) {
        $("#txtHigh").focus();
        Message("error", "Should be Higher greater then or equal to Lower");
        returnValue = false;
    }

    else {
        $("#preloader").css("display", "block");

        var obj = {
            refId: $("#hdRefRange_Id").val(),
            TestId : $("#hdTest_ID").val(),
            Gender: $("#ddlGender").val(),
            SmpTyId: $("#ddlSampleType").val(),
            AgeMin: $("#txtMinAge").val(),
            AgeMax: $("#txtMaxAge").val(),
            AgeUnit: $("#ddlAgeUnit").val(),
            RefLow: $("#txtLower").val(),
            RefHigh: $("#txtHigh").val(),

        }

        $.ajax({
            method: "POST",
            url:'/TestParameter/SaveReferenceRange',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result) {
                    if ($("#hdDr_Id").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    BindTableReferenceRange()
                    ClearReferenceRange();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function UpdateShowReferenceRange(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/TestParameter/GetByIdReferenceRange?Id=' + Id,
        success: function (result) {
            if (result != null) {
                $("#hdRefRange_Id").val(result.refId);
                $("#hdTest_ID").val(result.testId);
                $("#ddlGender").val(result.gender),
                    $("#ddlSampleType").val(result.smpTyId),
                    $("#txtMinAge").val(result.ageMin),
                    $("#txtMaxAge").val(result.ageMax),
                    $("#ddlAgeUnit").val(result.ageUnit),
                    $("#txtLower").val(result.refLow),
                    $("#txtHigh").val(result.refHigh),


                    $("#btnRefRange").html("Update");
                $("#modelReferenceRange").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationReferenceRange(Id) {
    $("#modelDeleteConfirmationReferenceRange").modal('show');
    $("#hdDeleteReferenceRangeId").val(Id);
}

function DeleteReferenceRange() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteReferenceRangeId").val();
    $("#modelDeleteConfirmationReferenceRange").modal('hide');
    $("#hdDeleteReferenceRangeId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url:'/TestParameter/DeleteReferenceRange',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableReferenceRange();
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function ClearReferenceRange() {
    $("#ddlGender").val(''),
        $("#ddlSampleType").val(''),
        $("#txtMinAge").val(''),
        $("#txtMaxAge").val(''),
        $("#ddlAgeUnit").val(''),
        $("#txtLower").val(''),
        $("#txtHigh").val(''),
        $("#modelReferenceRange").modal('hide');
    $("#btnRefRange").html("Save");
}

//End Reference Range

//Start Calibration


function GetMonthYear() {
    $.ajax({
        method: "GET",
        url: '/QualityControl/MonthYear',
        success: function (result) {
        
            if (result != null) {
                $("#ddlExpMonth").empty();
                $("#ddlExpYear").empty();

                var month = '';
                for (var i = 0; i < result.months.length; i++) {
                    month += '<option value="' + result.months[i].monValue + '">' + result.months[i].monName + '</option>';
                }

                var year = '';
                for (var i = 0; i < result.year.length; i++) {
                    year += '<option value="' + result.year[i] + '">' + result.year[i] + '</option>';
                }

                $("#ddlExpMonth").append(month);
                $("#ddlExpYear").append(year);
            }
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
        }
    });
}

function GetCaliLotNo() {
    var TestId = $("#ddlTestCode").val();
    if (TestId != "") {
        $.ajax({
            method: "GET",
            url: '/TestParameter/GetAllCalibrationLotNo?TestId=' + TestId,
            success: function (result) {
                if (result != null) {
                    $(".es-list").empty();
                    var calLotNo = '';
                    for (var i = 0; i < result.length; i++) {
                        calLotNo += '<li class="es-visible">' + result[i].lotNo + '</li>';
                    }
                    $(".es-list").append(calLotNo);
                    $('.editable-select').editableSelect();
                }
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
            }
        });
    }
}

function BindTableClibrationLotNo(TestId) {
    if (TestId != "") {
        $("#preloader").css("display", "block");

        $.ajax({
            url: "/TestParameter/GetAllCalibrationLotNo?TestId=" + TestId,
            method: 'get',
            success: OnsuccessClibrationLotNo
        });
    }
}

function OnsuccessClibrationLotNo(response) {
    $('#tblClibrationLotNo').DataTable().destroy();
    $('#tblClibrationLotNo').DataTable({
        //scrollX: true,
        //scroll: true, // used for fixed header
        //scrollY: 300, // used for fixed Hieght
        //bProcessing: true,
        //bLenghtChange: true,
        //retrieve: true,
        //bfilter: true,
        //bSort: true,
        //bPaging: true,
        data: response,
        columns: [
            { data: 'lotNo', title: 'Lot No' },
            {
                data: "expMonth", title: 'Expiry Date',
                render: function (data, type, row, meta) {
                    if (row.expMonth < 10) {
                        return "0"+row.expMonth + "/" + row.expYear;
                    }
                    else {
                        return row.expMonth + "/" + row.expYear;
                    }
                }
            },
            {
                data: "factorMax", title: 'Factor Range',
                render: function (data, type, row, meta) {
                    return row.factorMin + "-" + row.factorMax;
                   
                }
            },
            {
                data: "calibLotId", title: 'Action',
                render: function (data, type, row, meta) {
                    return '<div style="display: flex;"><a class="" href="#" style="color:green;" onclick="UpdateShowCalibrationLotNo(\'' + row.calibLotId + '\',\'Select\')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Select Lot Detail"><b><i class="bi-check2-square"></i></b></a>&nbsp;&nbsp;<a class="" href="#" style="color:#656FEA;" onclick="UpdateShowCalibrationLotNo(\'' + row.calibLotId + '\',\'Update\')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Lot Detail"><b><i class="bi bi-pencil-fill"></i></b></a>&nbsp;&nbsp;<a class="" href="#" style="color:red;" onclick="DeleteConfirmationCalibrationLotNo(' + row.calibLotId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Lot Detail"><b><i class="bi bi-trash"></i></b></a></div>';
                }
            },
        ],
        "drawCallback": function (settings) {
            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new bootstrap.Tooltip(tooltipTriggerEl)
            })
        },
    });
    $("#preloader").css("display", "none");

}

function funAddNewCalibrationLot() {
    $("#modelAddCalibrationLotNo").modal("show");
}

function saveCalibrationLotNo() {
    if ($("#ddlTestCode").val() == "") {
        $("#ddlTestCode").focus();
        Message("error", "Test Code is required");
        returnValue = false;
    }
    else if ($("#ddlCalLotNo").val() == "") {
        $("#ddlCalLotNo").focus();
        Message("error", "Calibration Lot No is required");
        returnValue = false;
    }
    else if ($("#ddlExpMonth").val() == "") {
        $("#ddlExpMonth").focus();
        Message("error", "Expiry Month is required");
        returnValue = false;
    }
    else if ($("#ddlExpYear").val() == "") {
        $("#ddlExpYear").focus();
        Message("error", "Expiry Year is required");
        returnValue = false;
    }
    else if ($("#txtFactorMin").val() == "") {
        $("#txtFactorMin").focus();
        Message("error", "Factor Min is required");
        returnValue = false;
    }
    else if ($("#txtFactorMax").val() == "") {
        $("#txtFactorMax").focus();
        Message("error", "Factor Max is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            CalibLotId: $("#hdCalLotId").val(),
            TestId: $("#ddlTestCode").val(),
            LotNo: $("#ddlCalLotNo").val(),
            EXPMonth: $("#ddlExpMonth").val(),
            EXPYear: $("#ddlExpYear").val(),
            FactorMin: $("#txtFactorMin").val(),
            FactorMax: $("#txtFactorMax").val(),
        }

        $.ajax({
            method: "POST",
            url: '/TestParameter/SaveCalibrationLotNo',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result) {
                    if ($("#hdCalLotId").val() > 0) {
                        Message("success", "Updated Data Successfully");
                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    BindTableClibrationLotNo($("#ddlTestCode").val());
                    ClearCalibrationLotNo();
                    GetTestProfile()
                    $("#modelAddCalibrationLotNo").modal("hide");

                }
                else {

                    Message("error", "Profile name is already exist");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function UpdateShowCalibrationLotNo(Id, Type) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/TestParameter/GetByIdCalibrationLotNo?Id=' + Id,
        success: function (result) {
            if (result != null) {
                if (Type == "Update") {
                    $("#hdCalLotId").val(result.calibLotId);
                    $("#ddlTestCode").val(result.testId);
                    $("#ddlCalLotNo").val(result.lotNo);
                    $("#ddlExpMonth").val(result.expMonth);
                    $("#ddlExpYear").val(result.expYear);
                    $("#txtFactorMin").val(result.factorMin);
                    $("#txtFactorMax").val(result.factorMax);
                    $("#btnCalibrationLotNo").html("Update");
                    $("#modelAddCalibrationLotNo").modal("show");

                }
                else {
                    
                    if (result.expMonth < 10) {                        

                        $("#txtCalibrationExpDt").val("0" + result.expMonth + "/" + result.expYear); //hdCalibrationExpDt
                        $("#hdCalibrationExpDt").val("0" + result.expMonth + "/" + result.expYear);
                    }
                    else {
                        $("#txtCalibrationExpDt").val(result.expMonth + "/" + result.expYear);
                        $("#hdCalibrationExpDt").val(result.expMonth + "/" + result.expYear);

                    }
                    
                    

                    $("#hdCalibrationLotId").val(result.calibLotId);
                    $("#txtCalibrationLotNo").val(result.lotNo);
                    $("#modelCalibrationLotNo").modal('hide');
                }
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function DeleteConfirmationCalibrationLotNo(Id) {
    $("#modelDeleteConfirmationCalibrationLotNo").modal('show');
    $("#hdDeleteCalibrationLotNoId").val(Id);
}

function DeleteCalibrationLotNo() {
    $("#preloader").css("display", "block");
    var Id = $("#hdDeleteCalibrationLotNoId").val();
    $("#modelDeleteConfirmationCalibrationLotNo").modal('hide');
    $("#hdDeleteCalibrationLotNoId").val('');
    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/TestParameter/DeleteCalibrationLotNo',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    GetCalibrationLotNo();
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}
function ClearCalibrationLotNo() {
    $("#hdCalLotId").val('');
    $("#ddlCalLotNo").val('');
    $("#ddlExpMonth").val('');
    $("#ddlExpYear").val('');
    $("#txtFactorMin").val('');
    $("#txtFactorMax").val('');
    $("#btnCalibrationLotNo").html("Save");

}

function GetAllCalibrationMst() {
    $("#preloader").css("display", "block");
    $.ajax({
        method: "GET",
        url: '/TestParameter/GetAllCaliMaster?TestId=' + $("#ddlTestCode").val(),
        success: function (result) {
        
            if (result != null) {        
                //Unit
                $("#ddlCalibrationCurveType").empty();
                var curveType = '<option value=""></option>';
                for (var i = 0; i < result.curveType.length; i++) {
                    curveType += '<option value="' + result.curveType[i].crvTyId + '">' + result.curveType[i].crvType + '</option>'
                }
                $("#ddlCalibrationCurveType").append(curveType);

                if (result.caliHisCurveType != null) {

                    $("#ddlCalHisCurveType").empty();
                    var caliHisCurveType = '<option value=""></option>';
                    for (var i = 0; i < result.caliHisCurveType.length; i++) {
                        caliHisCurveType += '<option value="' + result.caliHisCurveType[i].crvTyId + '">' + result.caliHisCurveType[i].crvType + '</option>'
                    }
                    $("#ddlCalHisCurveType").append(caliHisCurveType);
                }
                //$("#ddCalibrationCupOfTube").empty();
                //var sampleTypeContainer = '<option value=""></option>';
                //for (var i = 0; i < result.sampleTypeContainer.length; i++) {
                //    sampleTypeContainer += '<option value="' + result.sampleTypeContainer[i].smpConId + '">' + result.sampleTypeContainer[i].smpConName + '</option>'
                //}
                //$("#ddCalibrationCupOfTube").append(sampleTypeContainer);
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function saveCalibration() {
    $("#preloader").css("display", "block");

    CalId = $("#hdCalibrationId").val();
    TestId = $("#ddlTestCode").val();
    CurveType = $("#ddlCalibrationCurveType").val();
    NoOfStds = $("#txtCalibrationStdNo").val();

    Factor = $("#txtCalibrationK").text();
    Factor1 = $("#txtCalibrationA").text();
    Factor2 = $("#txtCalibrationB").text();
    Factor3 = $("#txtCalibrationC").text();
    Factor4 = $("#txtCalibrationD").text();
    Factor5 = $("#txtCalibrationRo").text();


    var CaliDetl = [];
    var NoOfStanderd = $("#txtCalibrationStdNo").val();
    for (var i = 1; i < Number(NoOfStanderd) + 1; i++) {
        if ($("#txtCalibrationStdVal" + i).val() != "") {
            CaliDetl.push({ calDetId: $("#hdCalibrationStdId" + i).val(), Conc: $("#txtCalibrationStdVal" + i).val(), actualOd: $("#txtCalibrationAbsorbance" + i).val(), Lot: $("#hdCalibrationLotId").val(), DilRatio: $("#txtCalibrationDiluRatio").val() });
        }
    } 

    var obj = {
        calId: $("#hdCalibrationId").val(),
        testId: $("#ddlTestCode").val(),
        curveType: $("#ddlCalibrationCurveType").val(),
        noOfStds: $("#txtCalibrationStdNo").val(),
        factor: $("#txtCalibrationK").text(),
        factor1: $("#txtCalibrationA").text(),
        factor2: $("#txtCalibrationB").text(),
        factor3: $("#txtCalibrationC").text(),
        factor4: $("#txtCalibrationD").text(),
        factor5: $("#txtCalibrationRo").text(),
    }

    $.ajax({
        method: "POST",
        url: '/TestParameter/SaveCalibration',
        data: {
            calibration: obj, CaliDetl: CaliDetl
        },
        crossDomain: true,
        success: function (result) {
            if (result) {
                if ($("#hdTestProfileId").val() > 0) {
                    Message("success", "Updated Data Successfully");
                }
                else {
                    Message("success", "Saved Data Successfully");
                }
                ClearTestProfile();
                GetTestProfile()
            }
            else {

                Message("error", "Profile name is already exist");
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });

}
var mCurveTypeId = "";

function BindTableCalHisCurveType(ddlId) {
    $("#preloader").css("display", "block");
    var CurveTypeId = $("#" + ddlId).val();
    mCurveTypeId = CurveTypeId;
    $("#ddlCalHisCurveType").val(mCurveTypeId);

    $.ajax({
        url: "/TestParameter/GetAllCalibration?TestId=" + $("#ddlTestCode").val() + "&&CurveTypeId=" + CurveTypeId + "&&FormDate=" + $("#txtCalibrationFromDate").val() + "&&ToDate=" + $("#txtCalibrationToDate").val(),
        method: 'get',
        success: OnsuccessCalibrationHist
    });
}
function getBase64FromImageUrl(url) {
    var img = new Image();
    img.crossOrigin = "anonymous";
    img.onload = function () {
        var canvas = document.createElement("canvas");
        canvas.width = this.width;
        canvas.height = this.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(this, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    };
    img.src = url;
}

function OnsuccessCalibrationHist(response) {
    $('#tblCalibHis').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblCalHis").css("display", "block");
        $("#divCalHisNoRecordFound").empty();

        var table = $('#tblCalibHis').DataTable({
            //scrollX: true,
            "bProcessing": true,
            bLenghtChange: true,
            retrieve: true,
            bfilter: true,
            bSort: true,
            bPaging: true,
            data: response,
            //dom: '<"dt-buttons"Bf><"clear">lirtp',
            dom: 'Bfrtip',

            paging: true,
            autoWidth: true,
            columns: [
                {
                    data: "calDate", title: 'Run Date Time',
                    render: function (data, type, row, meta) {

                        if (row.calTime != null) {
                            return row.calDate + " " + new Date('1970-01-01T' + row.calTime + 'Z')
                                .toLocaleTimeString('en-US',
                                    { timeZone: 'UTC', hour12: true, hour: 'numeric', minute: 'numeric' }
                                );
                        }
                        else {
                            return row.calDate
                        }
                    }
                },
                { data: 'calValidity', title: 'State' },
                { data: 'curveType', title: 'Curve Type' },
                { data: 'conc', title: 'Stander Value' },
                { data: 'factor', title: 'k' },
                { data: 'factor1', title: 'a' },
                { data: 'factor2', title: 'b' },
                { data: 'factor3', title: 'c' },
                { data: 'factor4', title: 'd' },
                { data: 'factor5', title: 'RO' },
                { data: 'lotNo', title: 'Lot No.' },

                {
                    data: "expMonth", title: 'Expiry Date',
                    render: function (data, type, row, meta) {
                        if (row.expMonth < 10) {
                            return "0" + row.expMonth + "/" + row.expYear;
                        }
                        else {
                            return row.expMonth + "/" + row.expYear;
                        }
                    }
                },
                //{
                //    data: "calDetId", title: 'Action',
                //    render: function (data, type, row, meta) {
                //        return '<div style="display: flex;"><a class="" href="#" style="color:green;" onclick="UpdateShowCalibration(\'' + row.calId + '\',\'Select\')"><b><i class="bi-check2-square"></i></b></a>&nbsp;&nbsp;<a class="" href="#" style="color:red;" onclick="DeleteConfirmationCalibration(' + row.calId + ')"><b><i class="bi bi-trash"></i></b></a></div>';

                //    }
                //},
            ],

            buttons: [
                {
                    text: 'Print PDF',
                    extend: 'pdfHtml5',
                    download: 'open',

                    filename: 'Calibration',
                    orientation: 'portrait', //landscape
                    pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                    exportOptions: {
                        columns: ':visible',
                        search: 'applied',
                        order: 'applied'
                    },
                    customize: function (doc) {
                        //Remove the title created by datatTables
                        doc.content.splice(0, 1);
                        //Create a date string that we use in the footer. Format is dd-mm-yyyy
                        var logo = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAICAgICAQICAgIDAgIDAwYEAwMDAwcFBQQGCAcJCAgHCAgJCg0LCQoMCggICw8LDA0ODg8OCQsQERAOEQ0ODg7/2wBDAQIDAwMDAwcEBAcOCQgJDg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg7/wAARCAAwADADASIAAhEBAxEB/8QAGgAAAwEAAwAAAAAAAAAAAAAABwgJBgIFCv/EADUQAAEDAgQDBgUDBAMAAAAAAAECAwQFBgAHESEIEjEJEyJBUXEUI0JhgRVSYhYXMpEzcrH/xAAYAQADAQEAAAAAAAAAAAAAAAAEBQYHAv/EAC4RAAEDAgMGBQQDAAAAAAAAAAECAxEABAUGEhMhMUFRcSIyYaHBFkKB0ZGx8P/aAAwDAQACEQMRAD8Avy44hlhTrqw22kEqUo6BIG5JPkMSxz67RlFPzFquWnDParOaN4QVlmqXDKcKKLS19CCsf8qh6A6e+OfaK573LDTanDJllVV0q8r3ZVIuGqR1fMpdJSdHCCOinN0j7e+FjymydjRKdSbGsikpbSlG5O3/AHfeX5nU6knck6DFdg+DovkquLlWllHE8yeg+f4FBPvluEpEqNC657/4yr4ecm3ZxH1OghzxfptpQERI7X8QrqdPXGNpucXGLltU0SbZ4jazW0tHX4C6IiJcd37HUEj8YoHNtTKOzwuHVPj79rTfhkfCudxEbUOqQQd9Pc4HlaoGRt2JVAcptRsOe54WZZkd6yFHpzakgD3098ahYWuVVDQ/YrKD9wJnvGqfb8UAHH584npWw4eu0+iVO+6Vl3xO2zHy1uKa4GafdcBwqos5w7AOE6lgk+epT68uK8MvNPxmnmHEvMuJCm3EKCkqSRqCCNiCPPHmbzdyWcozkq1rpitVSkzGyqHNbT4HU+S0H6Vp22/9Bw8XZkcQ1wuzLg4V8yqq5U69a0X42zalJXq5NpeuhZJO5LWo0/idPpxI5ryszgyG77D3Nrau+U8weh/cDgQRI3sGXi54VCCKXK6Ku5fnbOcTt2znO/8A0SfFtymcx17llpGqgPTUjDj5WOIOUmYFPpLgjXQ5ES627r43I6R40I9D16fuGEfzPZeyq7afiRtec0W03O/GuSj82wdbdb8ZB89FEjb0xvrIzGk2pmnSrgcdUttl3lkoB2UyrZadPbf8DFFhGHuX+W0bASUyY6kKJg96XPK0XJmt9MrkFuIQw2XNup8IwFbruVaWXkttMgadCCcEfNuPTbbzPkiK87+jVRsTqctlIKVNubkD2J/0RgBVFDVQUpTTEksjdTjpG4xc4TYOvBu5AhB3yf8AcfmgTIUUmiMxcs27+CG42Koy3JqFqym3YLytebuVfRr9gVD2AwvOWt5u2f2qXDle0FK4UhVwijzgFbPMSUlBSftqdcMAqN/TfCVV0yGBDl3O+huMwvZXw6Oqzr67n8jC85VWw/fnakZD2tAaL/wtwGsSuTfu2YyCeY+6ikY5x1yzVlDECB4C8Nn3lEx6SFe9MWtW3R1jfVTu0l4a7lv6wbaz8yqp6p2Z2X6FmXT2U6uVelq8TrQA3UtG6gPMFQG+mJe2Xf8ASL5s1qp0p35qfDLhuHR2M4P8kLT5aH/ePUSpIUnQjUemJh8SXZs2fmVf8/MvJevKyfzNkEuTPhGeamVNZ3JeZGnKonqpPXqQTjE8tZmdwF4hSdbSjvHMHqP1zo24tw8J4EUn9MvWz7iymo9tX27PgTqQ4tMCfGY735SuiFdenTTTyGOIrGV1DSJLCqndb7Z1aamIDEZJHQqGg5vyDga3Fw28bVhS1wqrlHAzAjtkhFSt2sIQHR5HkXoQftjrqJw5cYt81BESDkuxaCVnRU24K0Fpb+/I3qT7Y1b6kygptSi88lKiSWxIEkyRygE8tUUDsbieA71mM2M0mZxlVytTQ0w0jkQlIIQ2PpabR1JJ6Abk4oP2bHDhW6O9WuITMKlLplxV9hMeg06Sn5lPgjdIUPJayedX4HljvOHvs16VbF7Uy/c86/8A3DuyIoOwoAaDdPgL66ts7gqH7lan2xVaJEjQaezFiMIjx2khLbaBoEgYyzMmZTjWi2t0bK3b8qfk+v8AW/jNMGWdn4lGVGv/2SAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA=';
                        doc.pageMargins = [20, 60, 20, 60];
                        // Set the font size fot the entire document
                        doc.defaultStyle.fontSize = 10;
                        // Set the fontsize for the table header
                        doc.styles.tableHeader.fontSize = 13;
                        doc.content[0].table.widths = ['*', '5%', '15%', '13%', '5%', '5%', '5%', '5%', '5%', '5%', '10%', '10%'];
                        // Create a header object with 3 columns
                        // Left side: Logo
                        // Middle: brandname
                        // Right side: A document title
                        doc['header'] = (function () {
                            return {
                                columns: [
                                    {
                                        image: logo,
                                        width: 24
                                    },
                                    {
                                        alignment: 'left',
                                        italics: true,
                                        text: 'Calibration Record',
                                        fontSize: 18,
                                        margin: [10, 0]
                                    },

                                    {
                                        alignment: 'right',
                                        fontSize: 14,
                                        text: 'Test Code : ' + $("#ddlTestCode option:selected").text()
                                    },
                                    {
                                        alignment: 'right',
                                        fontSize: 14,
                                        text: '   Curve Type : ' + $("#ddlCalibrationCurveType option:selected").text()
                                    } 
                                ],
                                margin: 20
                            }
                        });

                        // Create a footer object with 2 columns
                        // Left side: report creation date
                        // Right side: current page and total pages

                        doc['footer'] = (function (page, pages) {
                            return {
                                columns: [
                                    {
                                        alignment: 'left',
                                        //  text: 'Report Date : 12/12/2023'
                                        text: ['Report Date : ', { text: moment(new Date()).format('DD MMM YYYY').toString() }]
                                    },
                                    {
                                        alignment: 'center',
                                        text: ['This test has been conducted on FAA 200.']
                                    },
                                    {
                                        alignment: 'right',
                                        text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                    }
                                ],
                                margin: 20
                            }
                        });
                        // Change dataTable layout (Table styling)
                        // To use predefined layouts uncomment the line below and comment the custom lines below
                        // doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
                        var objLayout = {};
                        objLayout['width'] = function (i) { return 800; };
                        objLayout['hLineWidth'] = function (i) { return .5; };
                        objLayout['vLineWidth'] = function (i) { return .5; };
                        objLayout['hLineColor'] = function (i) { return '#aaa'; };
                        objLayout['vLineColor'] = function (i) { return '#aaa'; };
                        objLayout['paddingLeft'] = function (i) { return 4; };
                        objLayout['paddingRight'] = function (i) { return 4; };
                        doc.content[0].layout = objLayout;
                    }
                }],


        });
    }
    else {
        $("#divtblCalHis").css("display", "none");
        $("#divCalHisNoRecordFound").empty();
        $("#divCalHisNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }

    $(".buttons-pdf").css("background", "#656FEA");
    $(".buttons-pdf").css("color", "white");


    //table.columns.adjust().draw();

    //if (mCurveTypeId != "") {
    //    $("#ddlCalHisCurveType").val(mCurveTypeId);
    //    switch (mCurveTypeId) {
    //        case '1':
    //            table.column(4).visible(true);
    //            table.column(5).visible(false);
    //            table.column(6).visible(false);
    //            table.column(7).visible(false);
    //            table.column(8).visible(false);
    //            table.column(9).visible(false);
    //            break;
    //        case '2':
    //            table.column(4).visible(false);
    //            table.column(5).visible(true);
    //            table.column(6).visible(true);
    //            table.column(7).visible(false);
    //            table.column(8).visible(false);
    //            table.column(9).visible(false);
    //            break;
    //        case '3':
    //            table.column(4).visible(false);
    //            table.column(5).visible(true);
    //            table.column(6).visible(true);
    //            table.column(7).visible(false);
    //            table.column(8).visible(false);
    //            table.column(9).visible(false);
    //            break;
    //        case '4':
    //            table.column(4).visible(true);
    //            table.column(5).visible(true);
    //            table.column(6).visible(true);
    //            table.column(7).visible(false);
    //            table.column(8).visible(false);
    //            table.column(9).visible(true);
    //            break;
    //        case '5':
    //            table.column(4).visible(true);
    //            table.column(5).visible(true);
    //            table.column(6).visible(true);
    //            table.column(7).visible(true);
    //            table.column(8).visible(false);
    //            table.column(9).visible(true);
    //        case '6':
    //            table.column(4).visible(true);
    //            table.column(5).visible(true);
    //            table.column(6).visible(true);
    //            table.column(7).visible(true);
    //            table.column(8).visible(false);
    //            table.column(9).visible(true);
    //            break;
    //        case '7':
    //            table.column(4).visible(false);
    //            table.column(5).visible(true);
    //            table.column(6).visible(true);
    //            table.column(7).visible(true);
    //            table.column(8).visible(false);
    //            table.column(9).visible(true);
    //            break;
    //        case '8':
    //            table.column(4).visible(false);
    //            table.column(5).visible(true);
    //            table.column(6).visible(true);
    //            table.column(7).visible(true);
    //            table.column(8).visible(false);
    //            table.column(9).visible(false);
    //            break;
    //        case '9':
    //            table.column(4).visible(false);
    //            table.column(5).visible(false);
    //            table.column(6).visible(false);
    //            table.column(7).visible(false);
    //            table.column(8).visible(false);
    //            table.column(9).visible(false);
    //            break;
    //    }
    //}
    $("#preloader").css("display", "none");
}

function UpdateShowCalibration(Id, Type) {
    $("#preloader").css("display", "block");
    $.ajax({
        method: "GET",
        url: '/TestParameter/GetByIdCalibration?Id=' + Id,
        success: function (result) {
            if (result != null) {
                UpdateShowBindCalibration(result);
            }
            else {
                ClearCalibration();
                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function UpdateShowByTestIdCalibration(TestId) {
    $("#preloader").css("display", "block");
    $.ajax({
        method: "GET",
        url: '/TestParameter/GetByTestIdCalibration?TestId=' + TestId,
        success: function (result) {
            console.log(result);
            if (result != null) {
                UpdateShowBindCalibration(result);
                BindTableCalHisCurveType('ddlCalibrationCurveType');
            }
            else {
                ClearCalibration();
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function UpdateShowBindCalibration(result) {
    $("#preloader").css("display", "none");

    $("#hdCalibrationId").val(result.calibration.calId);
    $("#ddlTestCode").val(result.calibration.testId);
    $("#ddlCalibrationCurveType").val(result.calibration.curveType);
    $("#txtCalibrationStdNo").val(result.calibration.noOfStds);
    Fun_CurveType();
    var CalTime = "";
    var CalDate = "";
    if (result.calibration.calTime != null) {
        CalTime = new Date('1970-01-01T' + result.calibration.calTime + 'Z')
            .toLocaleTimeString('en-US',
                { timeZone: 'UTC', hour12: true, hour: 'numeric', minute: 'numeric' }
            );
    }
    if (result.calibration.calDate != null) {
        CalDate = moment(result.calibration.calDate).format("DD-MMM-YYYY");
    }

    $("#txtCalibrationDateTime").val(CalDate + " " + CalTime);
    $("#txtCalibrationK").text(result.calibration.factor);
    $("#txtCalibrationA").text(result.calibration.factor1);
    $("#txtCalibrationB").text(result.calibration.factor2);
    $("#txtCalibrationC").text(result.calibration.factor3);
    $("#txtCalibrationD").text(result.calibration.factor4);
    $("#txtCalibrationRo").text(result.calibration.factor5);

    if (result.calibrationLot != null || typeof (result.calibrationLot) !== "undefined") {
        if (result.calibrationLot.expMonth < 10) {
            //$("#txtCalibrationExpDt").val("0" + result.calibrationLot.expMonth + "/" + result.calibrationLot.expYear);
            $("#hdCalibrationExpDt").val("0" + result.calibrationLot.expMonth + "/" + result.calibrationLot.expYear);
        }
        else {
            //$("#txtCalibrationExpDt").val(result.calibrationLot.expMonth + "/" + result.calibrationLot.expYear);
            $("#hdCalibrationExpDt").val(result.calibrationLot.expMonth + "/" + result.calibrationLot.expYear);
        }

        if (result.calibrationLot.expMonth == 1) { $("#txtCalibrationExpDt").val("Jan-" + result.calibrationLot.expYear); }
        else if (result.calibrationLot.expMonth == 2) { $("#txtCalibrationExpDt").val("Feb-" + result.calibrationLot.expYear); }
        else if (result.calibrationLot.expMonth == 3) { $("#txtCalibrationExpDt").val("Mar-" + result.calibrationLot.expYear); }
        else if (result.calibrationLot.expMonth == 4) { $("#txtCalibrationExpDt").val("Apr-" + result.calibrationLot.expYear); }
        else if (result.calibrationLot.expMonth == 5) { $("#txtCalibrationExpDt").val("May-" + result.calibrationLot.expYear); }
        else if (result.calibrationLot.expMonth == 6) { $("#txtCalibrationExpDt").val("Jun-" + result.calibrationLot.expYear); }
        else if (result.calibrationLot.expMonth == 7) { $("#txtCalibrationExpDt").val("July-" + result.calibrationLot.expYear); }
        else if (result.calibrationLot.expMonth == 8) { $("#txtCalibrationExpDt").val("Aug-" + result.calibrationLot.expYear); }
        else if (result.calibrationLot.expMonth == 9) { $("#txtCalibrationExpDt").val("Sep-" + result.calibrationLot.expYear); }
        else if (result.calibrationLot.expMonth == 10) { $("#txtCalibrationExpDt").val("Oct-" + result.calibrationLot.expYear); }
        else if (result.calibrationLot.expMonth == 11) { $("#txtCalibrationExpDt").val("Nov-" + result.calibrationLot.expYear); }
        else if (result.calibrationLot.expMonth == 12) { $("#txtCalibrationExpDt").val("Dec-" + result.calibrationLot.expYear); }
        else { $("#txtCalibrationExpDt").val(result.calibrationLot.expYear); }

        $("#hdCalibrationLotId").val(result.calibrationLot.calibLotId);
        $("#txtCalibrationLotNo").val(result.calibrationLot.lotNo);
    }
    var StanVal = [];
    var Abo = [];
    if (result.calibrationDetail.length > 0) {
        for (var i = 0; i < result.calibrationDetail.length; i++) {
            StanVal.push(result.calibrationDetail[i].conc);
            Abo.push(result.calibrationDetail[i].actualOd);
            $("#hdCalibrationStdId" + (i + 1)).val(result.calibrationDetail[i].calDetId);
            $("#txtCalibrationStdVal" + (i + 1)).val(result.calibrationDetail[i].conc);
            $("#txtCalibrationAbsorbance" + (i + 1)).val(result.calibrationDetail[i].actualOd);

            if ((result.calibrationDetail[i].dilRatio) > 0) {
                $("#CalibrationChkDilution").prop("checked", true);
                showDilRation();
                $("#txtCalibrationDiluRatio").val(result.calibrationDetail[i].dilRatio);
            }
            else {
                $("#CalibrationChkDilution").prop("checked", false);
            }
        }
    }
    CreateLineGraph("LineChartCalibration", "Calibration", "#3b8bba", StanVal, Abo);
    //Graph("LineChartCalibration", "Calibration", "#3b8bba", StanVal, Abo);

    funAddStdNo();
    $("#btnCalibration").text("Update");
    $("#modelCalHist").modal('hide');
    $("#preloader").css("display", "block");

}

function Graph(LineChartId, Label, Color, LabelContent, DataContent) {
    var salesGraphChartData = {
        labels: LabelContent,
        datasets: [{
            label: Label,
            fill: false,
            borderWidth: 2,
            lineTension: 0,
            spanGaps: true,
            borderColor: Color,
            pointRadius: 3,
            pointHoverRadius: 7,
            pointColor: Color,
            pointBackgroundColor: Color,
            data: DataContent
        }]
    }
    var chart = new CanvasJS.Chart("chartContainer",
            {

                title: {
                    text: "Earthquakes - per month"
                },

                axisX: {
                    valueFormatString: "MMM",
                    interval: 1,
                    intervalType: "month",
                    title: "Primary"

                },
                axisY: {
                    includeZero: false

                },
                data: [
                    {
                        type: "line",

                        data: salesGraphChartData,
                    }
                ]
            });

        chart.render();
}

function CreateLineGraph(LineChartId, Label, Color, LabelContent, DataContent) {
    $(".div" + LineChartId).empty();
    $(".div" + LineChartId).append('<canvas class="chart chartjs-render-monitor" id="' + LineChartId +'" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block;" width="360" height="450"></canvas>');

    var salesGraphChartCanvas = $('#' + LineChartId).get(0).getContext('2d')
 
    var salesGraphChartData = {
        labels: LabelContent,
        datasets: [{
            label: Label,
           /* fill: false,*/
            borderWidth: 2,
            lineTension: 0,
            spanGaps: true,
            borderColor: Color,
            pointRadius: 3,
            pointHoverRadius: 7,
            pointColor: Color,
            pointBackgroundColor: Color,
            data: DataContent
        }]
    }

    var salesGraphChartOptions = {
      /*  maintainAspectRatio: false,*/
        responsive: true,
        legend: {
            display: false
        },
        //tooltips: {
        //    enabled: false
        //},
  
        //scales: {
        //    xAxes: [{
        //        ticks: {
        //            fontColor: '#333'
        //        },
        //        gridLines: {
        //            display: false,
        //            color: '#',
        //            drawBorder: false
        //        },
              
        //    }],
        //    yAxes: [{
        //        ticks: {
        //            stepSize: 5000,
        //            fontColor: '#333'
        //        },
        //        gridLines: {
        //            display: true,
        //            color: Color,
        //            drawBorder: false
        //        }

        //    }]
        //}
    }


    var salesGraphChart = new Chart(salesGraphChartCanvas, {
        type: 'line',
        data: salesGraphChartData,
        options: salesGraphChartOptions,
        axisX: {
            title: "Primary"
        },
        axisY: {
            title: "Primary"
        },
    })

}


function DeleteConfirmationCalibration(Id) {
    $("#modelDeleteConfirmationCalibration").modal('show');
    $("#hdDeleteCalibrationId").val(Id);
}

function DeleteCalibration() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteCalibrationId").val();
    $("#modelDeleteConfirmationCalibration").modal('hide');
    $("#hdDeleteCalibrationId").val('');
    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/TestParameter/DeleteCalibration',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    GetCalibration();
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function ModifyAbsorbance() {
    if ($("#btnModiAbsorbance").attr("accesskey") == "Modify") {
        document.getElementById("txtCalibrationAbsorbance1").disabled = false;
        document.getElementById("txtCalibrationAbsorbance2").disabled = false;
        document.getElementById("txtCalibrationAbsorbance3").disabled = false;
        document.getElementById("txtCalibrationAbsorbance4").disabled = false;
        document.getElementById("txtCalibrationAbsorbance5").disabled = false;
        document.getElementById("txtCalibrationAbsorbance6").disabled = false;
        document.getElementById("txtCalibrationAbsorbance7").disabled = false;
        document.getElementById("txtCalibrationAbsorbance8").disabled = false;
        document.getElementById("txtCalibrationAbsorbance9").disabled = false;
        document.getElementById("txtCalibrationAbsorbance10").disabled = false;

        $("#btnModiAbsorbance").text("Update");
        $("#btnModiAbsorbance").attr("accesskey", "Update");
    }
    else {
        $("#preloader").css("display", "block");
        var CaliDetl = [];
        for (var i = 1; i < 11; i++) {
       
            if ($("#txtCalibrationStdVal" + i).val() != "") {
                CaliDetl.push({ calDetId: $("#hdCalibrationStdId" + i).val(), Conc: $("#txtCalibrationStdVal" + i).val(), actualOd: $("#txtCalibrationAbsorbance" + i).val(), Lot: $("#hdCalibrationLotId").val(), DilRatio: $("#txtCalibrationDiluRatio").val() });
            }
        } 
        alert(CaliDetl);
        $.ajax({
            method: "POST",
            url: '/TestParameter/UpdateAbsorbanceCalibration',
            data: {
                CalId: $("#hdCalibrationId").val(), CaliDetl: CaliDetl
            },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Updated Data Successfully");
                    $("#btnModiAbsorbance").text("Modify Absorbance");
                    $("#btnModiAbsorbance").attr("accesskey", "Modify");
                    document.getElementById("txtCalibrationAbsorbance1").disabled = true;
                    document.getElementById("txtCalibrationAbsorbance2").disabled = true;
                    document.getElementById("txtCalibrationAbsorbance3").disabled = true;
                    document.getElementById("txtCalibrationAbsorbance4").disabled = true;
                    document.getElementById("txtCalibrationAbsorbance5").disabled = true;
                    document.getElementById("txtCalibrationAbsorbance6").disabled = true;
                    document.getElementById("txtCalibrationAbsorbance7").disabled = true;
                    document.getElementById("txtCalibrationAbsorbance8").disabled = true;
                    document.getElementById("txtCalibrationAbsorbance9").disabled = true;
                    document.getElementById("txtCalibrationAbsorbance10").disabled = true;

                }
                else {
                    Message("error", "Profile name is already exist");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
 
}

function ClearCalibration() {
    $("#hdCalibrationId").val('');
    //$("#ddlTestCode").val(result.calibration.testId);
    //$("#ddlCalibrationCurveType").val(result.calibration.curveType);
    $("#txtCalibrationStdNo").val('');
    $("#txtCalibrationDateTime").val('');

    $("#txtCalibrationK").text('');
    $("#txtCalibrationA").text('');
    $("#txtCalibrationB").text('');
    $("#txtCalibrationC").text('');
    $("#txtCalibrationD").text('');
    $("#txtCalibrationRo").text('');
    $("#txtCalibrationLotNo").val('');
    $("#txtCalibrationExpDt").val('');
    $("#hdCalibrationExpDt").val('');

    $("#hdCalibrationLotId").val('');
    $("#txtCalibrationDiluRatio").val('');
    for (var i = 1; i < 11; i++) {
        $("#hdCalibrationStdId" + i).val('');
        $("#txtCalibrationStdVal" + i).val('');
        $("#txtCalibrationAbsorbance" + i).val('');
      
    }
}


//End Calibration

//End Programming

//Start Test Profile

function myGeeks() {
    document.querySelector(".button")
        .addEventListener("click", function () {
            alert("Button Clicked");
        });
}
function funProfileSelect(e) {

    if (e.style.background == "#fff" || e.style.background == "" || e.style.background != "rgb(133, 137, 135)") { //039b54

        e.style.background = "#858987";  //'rgb(3, 155, 84)

    }
    else if (e.style.background == "#858987" || e.style.background != "" || e.style.background == "rgb(133, 137, 135)") {
        e.style.background = "#fff";
    }  
    
    }


/*$('.activeprofile').on('click', function (e) {
    alert("hi");
});*/

function funBtnSelect(e) {

    var Id = $(e).parent().prev("div").attr('id');
   // Reflex
    if (Id == "Reflex") {
        var ReflexTestCode = [];
        $("#divRflxTestCode div button").each(function () {
            if (($(this).attr("accesskey")) == "check") {
                ReflexTestCode.push({
                    TestId: $(this).attr("id"),
                });
            }
        });

            if ((e.style.background == "" || e.style.background == "#858987" || e.style.background == "rgb(133, 137, 135)") && ReflexTestCode.length < 8) {
                e.style.background = "#039b54"; // Green
                $(e).attr("accesskey", "check");
            }
            else if (e.style.background == "#039b54" || e.style.background == "rgb(3, 155, 84)") {
                e.style.background = "#858987"; //Gray
                $(e).attr("accesskey", "");
            }
            else if (ReflexTestCode.length >= 8) {
                Message("error", "Max 8 reflex tests are allowed.");
                return false;
            }
      
    }
    else {

        if (e.style.background == "" || e.style.background == "#858987" || e.style.background == "rgb(133, 137, 135)") {
            e.style.background = "#039b54"; // Green
            $(e).attr("accesskey", "check");
        }
        else if (e.style.background == "#039b54" || e.style.background == "rgb(3, 155, 84)") {
            e.style.background = "#858987"; //Gray
            $(e).attr("accesskey", "");
        }
    }
}

function GetTestCode() {
    $("#preloader").css("display", "block");
    $.ajax({
        method: "GET",
        url: '/TestParameter/GetTestCode',
        success: function (result) {
            if (result != null) {
                //Test Code 
                $("#divTestCodeBind").empty();
                //Test Reflex
                $("#divRflxTestCode").empty();
                $("#ddlRflxTestName").empty();
          
                //Carry Over
                $("#ddlTest1").empty();
                $("#ddlTest2").empty();

                var proTestCode = '';
                var reflexTextCode = '';
                var ddlTestCode = '<option value=""></option>';
                for (var i = 0; i < result.testCode.length; i++) {
                    ddlTestCode += '<option value="' + result.testCode[i].testId + '">' + result.testCode[i].testCode + '</option>'
                    proTestCode += '<div class="col-lg-2 mb-2 mt-1"><button type="button" id="' + result.testCode[i].testId +'" class="btn colorCode_toggle rounded-pill" style="background:#858987;width:102px;margin:1px"  onclick = "funBtnSelect(this)" >' + result.testCode[i].testCode +' </button ></div>'
                    //reflexTextCode += '<div class="col-lg-3 mb-2" id="Reflex"><button type="button" id="' + result.testCode[i].testId + '" class="btn colorCode_toggle rounded-pill" style="background:#858987;width:135px;"  onclick = "funBtnSelect(this)" >' + result.testCode[i].testCode + ' </button ></div>'
                    reflexTextCode += '<div class="col-lg-2 mb-2 mt-1" id="Reflex"><button type="button" id="' + result.testCode[i].testId + '" class="btn colorCode_toggle rounded-pill" style="background:#858987;width:102px;;margin:1px"  onclick = "funBtnSelect(this)" >' + result.testCode[i].testCode + ' </button ></div>'

                }

                $("#divTestCodeBind").append(proTestCode);
                $("#divRflxTestCode").append(reflexTextCode);
                $("#ddlRflxTestName").append(ddlTestCode);

                $("#ddlRflxTestName").select2({
                    dropdownParent: $("#modelTestReflex"),
                    width: "100%"
                });

         
                $("#ddlTest1").append(ddlTestCode);
                $("#ddlTest2").append(ddlTestCode);
                $("#ddlTest1").select2({
                    dropdownParent: $("#modelCarryOver"),
                    width: "100%"
                });
                $("#ddlTest2").select2({
                    dropdownParent: $("#modelCarryOver"),
                    width: "100%"
                });
                $("#ddlRflxTestName").select2({
                    dropdownParent: $("#modelTestReflex"),
                    width: "100%"
                });
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function AddProfileName() {
    if ($("#txtAddProfileName").val() == "") {
        $("#txtAddProfileName").focus();
        Message("error", "Test Profile Name is required");
        return false;
    }
    else {
        var obj = {
            ProfileName: $("#txtAddProfileName").val()
        }
        $.ajax({
            method: "GET",
            url: '/TestParameter/DuplicateCheckTestProfile',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result > 0) {
                    if ($("#hdNewTestId").val() != "" && $("#txtAddProfileName").val() != "") {
                        $("#txtTestProfile").val($("#txtAddProfileName").val());
                        $("#modelAddProfile").modal('hide');
                        Message("success", "Profile Name is updated. Select Test Code and then save.");
                        ClearTestProfile();
                        GetTestProfile()
                    }
                    else if ($("#txtAddProfileName").val() != "") {
                        $("#txtTestProfile").val($("#txtAddProfileName").val());
                        $("#modelAddProfile").modal('hide');
                        Message("success", "Profile Name is saved. Select Test Code and then update.");
                        ClearTestProfile();
                        GetTestProfile()
                    }
                    else {
                        $("#txtAddProfileName").focus();
                        Message("error", "Profile Name is required");
                        return false;
                    }
                    $("#hdTestProfileId").val(result);
                    $("#txtTestProfile").val($("#txtAddProfileName").val())

                    $("#txtAddProfileName").val('');
                }
                else {
                    $("#txtAddProfileName").focus();
                    Message("error", "Profile Name is already exist");
                    return false;
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function GetTestProfile() {
    $("#preloader").css("display", "block");
    $.ajax({
        method: "GET",
        url: '/TestParameter/GetAllTestProfile',
        success: function (result) {
            if (result != null) {
                //Test Code 
                $("#tblTestProfile").empty();
                var tblTestProfile = '';
                //tblTestProfile += '<ul id="ulpofilelist">';
                for (var i = 0; i < result.length; i++) { // <td scope="row"> ' + result[i].profileName + '</td >
                    //tblTestProfile += '<tr><td scope="row" colspan="1" style="text-align: left;"><li class="hrefprofile"><a href="#"   onclick="updateShowTestProfile(' + result[i].profileId + ')"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Profile"><b>' + result[i].profileName + '</b></a></td><td><a href="#" onclick="DeleteTestProfile(' + result[i].profileId + ')" class="btn" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Profile"><i class="bi bi-trash" style="color:red"></i></a></li></td></tr >'
                    //tblTestProfile += '<tr><td scope="row" colspan="1" style="text-align: left;"><a href="#"  onclick="updateShowTestProfile(' + result[i].profileId + ')" class="btn" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Profile"><b><i><button type="button" class="btn colorCode_toggle high" onclick = "funProfileSelect(this)" >' + result[i].profileName + ' </button ></i></b></a></td><td><a href="#" onclick="DeleteTestProfile(' + result[i].profileId + ')" class="btn" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Profile"><i class="bi bi-trash" style="color:red"></i></a></td></tr >'

                    tblTestProfile += '<tr><td scope="row" colspan="1" style="text-align: left;"><a id="' + result[i].profileId + '" onclick="updateShowTestProfile(' + result[i].profileId + ')" class="btn ProfileId" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Test Profile"><b>' + result[i].profileName + '</b></a></td><td><a href="#" id="' + result[i].profileId + '" onclick="DeleteTestProfile(' + result[i].profileId + ')" class="btn" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete profile" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Test Profile"><i class="bi bi-trash" style="color:red"></i></a></td></tr >'
                }
                //tblTestProfile += '</ul>';
                $("#tblTestProfile").append(tblTestProfile);
            }
            $("#preloader").css("display", "none");

        },

        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function SaveTestProfile() {
    var returnValue = true;
    var TestCode = [];
    $("#divTestCodeBind div button").each(function () {
        if (($(this).attr("accesskey")) == "check") {
            TestCode.push({
                TestId:   $(this).attr("id"),
            });
        }
    });


    if ($("#txtTestProfile").val() == "") {
        $("#txtTestProfile").focus();
        Message("error", "Test Profile is required");
        returnValue = false;
    }
    else if (TestCode.length == 0) {
        Message("error", "Atleast one test code is select");
        returnValue = false;
    }

    else {
        $("#preloader").css("display", "block");
        var obj = {
            ProfileId: $("#hdTestProfileId").val(),
            ProfileName: $("#txtTestProfile").val(),
        }

        $.ajax({
            method: "POST",
            url: '/TestParameter/SaveTestProfile',
            data: { profileDets: TestCode, testProfile: obj },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    if ($("#hdTestProfileId").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearTestProfile();
                    GetTestProfile()
                }
                else {

                    Message("error", "Profile name is already exist");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function updateShowTestProfile(Id) {
    $("#preloader").css("display", "block");
    $.ajax({

        method: "GET",
        url: '/TestParameter/GetByIdTestProfile?Id=' + Id,
        success: function (result) {
            if (result != null) {
                ClearTestProfile();
                $("#hdTestProfileId").val(result.testProfile.profileId);
                $("#txtTestProfile").val(result.testProfile.profileName);
                $("#btnSaveTestProfile").html("Update");

                for (var i =0; i < result.profileDetail.length; i++) {
                    $("#divTestCodeBind div button").each(function () {
                        if (result.profileDetail[i].testId == $(this).attr("id")) {
                            $(this).attr("accesskey", "check");
                            this.style.background = "#039b54";
                        }
                    });
                }

                $("#tblTestProfile tr").each(function () {
                    if ($($(this).closest('tr')).find('td').find('.ProfileId').attr('id') == Id) {
                        $($(this).closest('tr')).find('td').css("background-color", "#d0d3f6");
                    }
                    else {
                        $($(this).closest('tr')).find('td').css("background-color", "");
                    }
                });
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function DeleteTestProfile(Id) {
    $("#preloader").css("display", "block");

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/TestParameter/DeleteTestProfile',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    GetTestProfile();
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function ClearTestProfile() {
    $("#hdTestProfileId").val(''),
    $("#txtTestProfile").val(''),
    $("#divTestCodeBind div button").each(function () {
        $(this).attr("accesskey", "");
        this.style.background = "#858987";
    });
    $("#btnSaveTestProfile").text("Save");
}

//End Test Profile


//Start Test Sequence
function GetTestSequence() {
    $("#preloader").css("display", "block");
    $.ajax({
        method: "GET",
        url: '/TestParameter/GetTestSequence',
        success: function (result) {
            if (result != null) {

                //Test Squence
                $("#liSqunceDisplayTestCode").empty();
                $("#liSquncePrintTestCode").empty();
                $("#liSqunceRunTestCode").empty();

                var squnceDisplayTestCode = ''
                var squncePrintTestCode = '';
                var squnceRunTestCode = '';

                for (var i = 0; i < result.sqnceDisplayIndx.length; i++) {
                    squnceDisplayTestCode += '<li class="list-item form-control btn_Color btn rounded-pill" id="' + result.sqnceDisplayIndx[i].testSqunceId + '"  data-value="' + result.sqnceDisplayIndx[i].testId + '" draggable="true" ondragover="dragOver(event)" ondragstart="dragStart(event)" ondrop="drop(event)" style="margin: 0.5em auto;width:200px">' + result.sqnceDisplayIndx[i].testCode + '</li>';
                }
                for (var i = 0; i < result.sqncePrintIndx.length; i++) {
                    squncePrintTestCode += '<li class="list-item form-control btn_Color btn rounded-pill" id="' + result.sqncePrintIndx[i].testSqunceId + '"  data-value="' + result.sqncePrintIndx[i].testId + '" draggable="true" ondragover="dragOver(event)" ondragstart="dragStart(event)" ondrop="drop(event)" style="margin: 0.5em auto;width:200px">' + result.sqncePrintIndx[i].testCode + '</li>';
                }
                for (var j = 0; j < result.sqnceRunIndx.length; j++) {
                    squnceRunTestCode += '<li class="list-item form-control btn_Color btn rounded-pill" id="' + result.sqnceRunIndx[j].testSqunceId + '"  data-value="' + result.sqnceRunIndx[j].testId + '" draggable="true" ondragover="dragOver(event)" ondragstart="dragStart(event)" ondrop="drop(event)" style="margin: 0.5em auto;width:200px">' + result.sqnceRunIndx[j].testCode + '</li>';
                }
                $("#liSqunceDisplayTestCode").append(squnceDisplayTestCode);
                $("#liSquncePrintTestCode").append(squncePrintTestCode);
                $("#liSqunceRunTestCode").append(squnceRunTestCode);

            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}
function saveTestSequence() {
    $("#preloader").css("display", "block");

    var testSequence = [];
    $("#liSquncePrintTestCode li").each(function () {
        var Id = 0;
        var RunIndex = 0;
        var DisplayIndex = 0;
        Id = $(this).attr('data-value');
        $("#liSqunceRunTestCode li").filter(function () {
            if ($(this).attr('data-value') == Id) {
                RunIndex = ($(this).index() + 1);
            }
        })

        $("#liSqunceDisplayTestCode li").filter(function () {
            if ($(this).attr('data-value') == Id) {
                DisplayIndex = ($(this).index() + 1);
            }
        })

        testSequence.push({
            TestSqunceId: $(this).attr('id'),
            TestId: $(this).attr('data-value'),
            PrintIndex : ($(this).index() + 1),
            RunIndex: RunIndex,
            DispIndex: DisplayIndex
        });
    });


        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/TestParameter/SaveTestSequence',
            data: { testSequence: testSequence },
            success: function (result) {
                if (result) {
                    Message("success", "Updated Data Successfully");
                    GetTestSequence();
                }
                else {
                    Message("error", "Somthing went wrong");
                }  
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });


        //var Id = 0;
        //Id = $(this).attr('id');
        //($(this).index() + 1) + " , " + $(this).attr('id');

 
        //$("#liSqunceRunTestCode li").filter(function () {
        //    if (this.id == Id) {
        //        return this.id;
        //    }
        //});
}

//End Test Sequence



//Start Test Reflex
function funPanicType() {
    PanicType = $("#ddlPanicType").val();
    if (PanicType == "Low") {
        $("#divLow").css("display", "flex");
        $("#divHigh").css("display", "none");
        $(".lblResult").css("display", "flex");
        $("#lblSymbol").text("<");
        $(".lblAndSymbol").css("display", "none");
    }
    else if (PanicType == "High") {
        $("#divLow").css("display", "none");
        $("#divHigh").css("display", "block");
        $(".lblResult").css("display", "flex");
        $("#lblSymbol").text(">");
        $(".lblAndSymbol").css("display", "none");

    }
    else if (PanicType == "Range") {
        $("#divLow").css("display", "flex");
        $("#divHigh").css("display", "block");
        $(".lblResult").css("display", "flex");
        $("#lblSymbol").text("Between");
        $(".lblAndSymbol").css("display", "block");

    }
    else {
        $("#divLow").css("display", "none");
        $("#divHigh").css("display", "none");
        $(".lblResult").css("display", "none");
        $("#lblSymbol").text("");
        $(".lblAndSymbol").css("display", "none");

    }
}
function funReflexTestCode() {

    var TestId = $("#ddlRflxTestName").val();
    if (TestId > 0) {
        $("#preloader").css("display", "block");

        $.ajax({
            method: "GET",
            url: '/TestParameter/GetUnitByTestId',
            data: { TestId: TestId },
            crossDomain: true,
            success: function (result) {
                if (result != null) {
          
                    $(".spnReflexUnit").text(result.unit);
                }
                else {

                    /*  Message("error", "Somthing went wrong");*/
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function GetTestReflex() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/TestParameter/GetAllTestReflex',
        success: function (result) {
            if (result != null) {
                //Test Code 
                $("#tblTestProfile").empty();
                var tblTestProfile = '';
                for (var i = 0; i < result.length; i++) { 

                    tblTestProfile += tblTestProfile += '<tr><td scope="row">' + result[i].profileName + '</td ><td scope="row" style="text-align: center;"><a href="#"  onclick="updateShowTestProfile(' + result[i].profileId + ')" class="btn" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Profile"><b><i class="bi bi-pencil" style="color:#656FEA' + result[i].profileName + ' </i></b></a><a href="#" onclick="DeleteTestProfile(' + result[i].profileId + ')" class="btn" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Profile"><i class="bi bi-trash" style="color:red"></i></a></td></tr >'
                }
                $("#tblTestProfile").append(tblTestProfile);
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function BindTableTestReflex() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/TestParameter/GetAllTestReflex",
        method: 'get',
        success: OnsuccessTestReflex
    });
}

function OnsuccessTestReflex(response) {
    $('#tblTestReflex').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblTestReflex").css("display", "block");
        $("#divTestReflexNoRecordFound").empty();
        $('#tblTestReflex').DataTable({
            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 300, // used for fixed Hieght
            //bProcessing: true,
            //bLenghtChange: true,
            //retrieve: true,
            //bfilter: true,
            //bSort: true,
            //bPaging: true,
            data: response,
            columns: [
                { data: 'testCode', title: 'Test Code', width: '10%' },
                { data: 'reflexType', title: 'Panic Type', width:'10%' },
                {
                    data: "lowValue", title: 'Panic Value', width:'10%',
                    render: function (data, type, row, meta) {
                        if (row.reflexType == "Low") {
                            return row.lowValue;
                        }
                        else if (row.reflexType == "High") {
                            return row.highValue;
                        }
                        else {
                            return row.lowValue + " - " + row.highValue;
                        }
                    }
                },
                {
                    data: "reflexDetail", title: 'Reflex Tests', width:'60%',
                    render: function (data, type, row, meta) {
                        var contect = '';
                        for (var i = 0; i < row.reflexDetail.length; i++) {
                            contect += row.reflexDetail[i].testCode + ", "
                        }
                        return contect
                    }
                },

                {
                    data: "reflexId", title: 'Action', width:'10%',
                    render: function (data, type, row, meta) {
                        return '<div style="display: flex;"><a class="" href="#" style="color:#656FEA;" onclick="UpdateShowTestReflex(' + row.reflexId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Test Reflex"><b><i class="bi bi-pencil-fill"></i></b></a>&nbsp;&nbsp;<a class="" href="#" style="color:red;" onclick="DeleteConfirmationTestReflex(' + row.reflexId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Test Reflex"><b><i class="bi bi-trash"></i></b></a></div>';
                    }
                },
            ],

            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblTestReflex").css("display", "none");
        $("#divTestReflexNoRecordFound").empty();
        $("#divTestReflexNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
    $("#preloader").css("display", "none");

}

function SaveTestReflex() {
    var returnValue = true;
    var ReflexTestCode = [];
    $("#divRflxTestCode div button").each(function () {
        if (($(this).attr("accesskey")) == "check") {
            ReflexTestCode.push({
                TestId: $(this).attr("id"),
            });
        }
    });

    if ($("#ddlRflxTestName").val() == "") {
        $("#ddlRflxTestName").focus();
        Message("error", "Test Profile is required");
        returnValue = false;
    }
    else if ($("#ddlPanicType").val() == "") {
        $("#ddlPanicType").focus();
        Message("error", "Panic Type is required");
        returnValue = false;
    }
    else if ($("#ddlPanicType").val() == "High" && $("#txtHighValue").val() == "") {
        $("#txtHighValue").focus();
        Message("error", "High value is required");
        returnValue = false;
    }
    else if ($("#ddlPanicType").val() == "Low" && $("#txtLowValue").val() == "") {
        $("#txtLowValue").focus();
        Message("error", "Low value is required");
        returnValue = false;
    }
    else if ($("#ddlPanicType").val() == "Range" && ($("#txtLowValue").val() == "" || $("#txtHighValue").val() == "")) {
        $("#txtLowValue").focus();
        Message("error", "Low and High value is required");
        returnValue = false;
    }

    else if (ReflexTestCode.length == 0) {
        Message("error", "Atleast one test code is select");
        returnValue = false;
    }

    else {
        $("#preloader").css("display", "block");

        var obj = {
            ReflexId: $("#hdReflexId").val(),
            TestId: $("#ddlRflxTestName").val(),
            ReflexType: $("#ddlPanicType").val(),
            LowValue: $("#txtLowValue").val(),
            HighValue: $("#txtHighValue").val(),
        }

        $.ajax({
            method: "POST",
            url: '/TestParameter/SaveTestReflex',
            data: { reflexDets: ReflexTestCode, testReflex: obj },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    if ($("#hdReflexId").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearTestReflex();
                    BindTableTestReflex()
                }
                else {
                    Message("error", "Test code and Panic type is already exist");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function UpdateShowTestReflex(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/TestParameter/GetByIdTestReflex?Id=' + Id,
        success: function (result) {
            if (result != null) {
                /*ClearTestReflex();*/
                
                $("#hdReflexId").val(result.testReflex.reflexId);
                $("#ddlRflxTestName").val(result.testReflex.testId).trigger("change");
                $("#ddlPanicType").val(result.testReflex.reflexType);
                funPanicType();
                $("#txtLowValue").val(result.testReflex.lowValue);
                $("#txtHighValue").val(result.testReflex.highValue);

                $("#btnSaveTestReflex").html("Update");
                $("#modelTestReflex").modal('show');
                for (var i = 0; i < result.reflexDetail.length; i++) {
                    $("#divRflxTestCode div button").each(function () {
                        if (result.reflexDetail[i].testId == $(this).attr("id")) {
                            $(this).attr("accesskey", "check");
                            this.style.background = "#039b54";
                        }

                    });
                }
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}


function DeleteConfirmationTestReflex(Id) {
    $("#modelDeleteConfirmationTestReflex").modal('show');
    $("#hdDeleteTestReflexId").val(Id);
}
function DeleteTestReflex() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteTestReflexId").val();
    $("#modelDeleteConfirmationTestReflex").modal('hide');
    $("#hdDeleteTestReflexId").val('');
    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/TestParameter/DeleteTestReflex',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableTestReflex();
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}


function ClearTestReflex() {
    $("#hdReflexId").val('');
    $("#ddlRflxTestName").val('').trigger('change');
    $("#ddlPanicType").val('');
    $("#txtLowValue").val('');
    $("#txtHighValue").val('');
    funPanicType();
    $("#btnSaveTestReflex").html("Save");
    $("#divRflxTestCode div button").each(function () {
        $(this).attr("accesskey", "");
        this.style.background = "#858987";
    });
    $("#modelTestReflex").modal('hide');
}

//End Test Reflex


//Start Carry Over

function funWashType() {
    var WashingType = $("input[name='chkWashingType']:checked").val();
    if (WashingType == "Detergent" || WashingType == "Reagent") {
        $("#divWashVolume").css("display", "block");
        $("#lblWashVolume").text(WashingType +" Volume")
    }
    else {
        $("#divWashVolume").css("display", "none");
    }
}

function BindTableCarryOver() {
    $("#preloader").css("display", "block");
    $.ajax({
        url: "/TestParameter/GetAllCarryOver",
        method: 'get',
        success: OnsuccessCarryOver
    });
}

function OnsuccessCarryOver(response) {
    $('#tblCarryOver').DataTable().destroy();
    $('#tblCarryOver').DataTable({
        //scrollX: true,
        //scroll: true, // used for fixed header
        //scrollY: 300, // used for fixed Hieght
        //bProcessing: true,
        //bLenghtChange: true,
        //retrieve: true,
        //bfilter: true,
        //bSort: true,
        //bPaging: true,
        data: response,
        columns: [
            { data: 'test1', title: 'Test 1' },
            { data: 'test2', title: 'Test 2' },
            {
                data: "washType", title: 'Wash Type',
                render: function (data, type, row, meta) {
                    if (row.washType != "") {
                        return row.washType + " Wash";
                    }
                   
                }
            },

            {
                data: "washVolume", title: 'Wash Volume',
                render: function (data, type, row, meta) {
                    if (row.washVolume != "") {
                        return row.washVolume + ' µl' ;
                    }

                }
            },

            //{ data: 'washVolume' + '& micro; l', title: 'Wash Volume' },

            {
                data: "Idpk", title: 'Action',
                render: function (data, type, row, meta) {
                    return '<a class="" href="#" onclick = UpdateShowCarryOver(' + row.idpk + ') style="color:#656FEA;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Carry Over"> <b><i class="bi bi-pencil-fill"></i></b></a>&nbsp;&nbsp;<a class="" href="#" onclick="DeleteConfirmationCarryOver(' + row.idpk + ')" style="color:red;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Carry Over"><b><i class="bi bi-trash"></i></b></a>';
                }
            },
        ],
        "drawCallback": function (settings) {
            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new bootstrap.Tooltip(tooltipTriggerEl)
            })
        },

    });
    $("#preloader").css("display", "none");

}

function SaveCarryOver() {
    var returnValue = true;
    if ($("#ddlTest1").val() == "") {
        $("#ddlTest1").focus();
        Message("error", "Test 1 is required");
        returnValue = false;
    }
    else if ($("#ddlTest2").val() == "") {
        $("#ddlTest2").focus();
        Message("error", "Test 2 is required");
        returnValue = false;
    }
    else if ($("input[name='chkWashingType']:checked").val() == "") {
        $("#Normal").focus();
        Message("error", "Washing Type is required");
        returnValue = false;
    }
    else if (($("input[name='chkWashingType']:checked").val() == "Detergent" || $("input[name='chkWashingType']:checked").val() == "Reagent") && $("#txtWashVolume").val() == "") {
        $("#txtWashVolume").focus();
        Message("error", "Wash Volume is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            Idpk: $("#hdCarryOverId").val(),
            TestId: $("#ddlTest1").val(),
            AfectedTestId: $("#ddlTest2").val(),
            WashType: $("input[name='chkWashingType']:checked").val(),
            WashVolume: $("#txtWashVolume").val(),
        }
        $.ajax({
            method: "POST",
            url: '/TestParameter/SaveCarryOver',
            data: obj,
            success: function (result) {
                if (result > 0) {
                    if ($("#hdUnt_Id").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearCarryOver();
                    BindTableCarryOver()
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function UpdateShowCarryOver(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/TestParameter/GetByIdCarryOver',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {

            if (result != null) {
                $("#hdCarryOverId").val(result[0].idpk),
                    $("#ddlTest1").val(result[0].testId).trigger("change"),
                    $("#ddlTest2").val(result[0].afectedTestId).trigger("change"),
                    $("input[name=chkWashingType][value=" + result[0].washType + "]").prop('checked', true);

                    funWashType();
                $("#txtWashVolume").val(result[0].washVolume),

                    $("#btnCarryOver").html("Update");
                $("#modelCarryOver").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationCarryOver(Id) {
    $("#modelDeleteConfirmationCarryOver").modal('show');
    $("#hdDeleteCarryOverId").val(Id);
}

function DeleteCarryOver() {
    $("#preloader").css("display", "block");
    var Id = $("#hdDeleteCarryOverId").val();
    $("#modelDeleteConfirmationCarryOver").modal('hide');
    $("#hdDeleteCarryOverId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/TestParameter/DeleteCarryOver',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableCarryOver();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function ClearCarryOver() {
    $("#hdCarryOverId").val('');
    $("#ddlTest1").val('').trigger('change');
    $("#ddlTest2").val('').trigger('change');
    $("input[name=chkWashingType][value='Normal']").prop('checked', true);
    funWashType();
    $("#txtWashVolume").val(''),
        $("#btnCarryOver").html("Save");
    $("#modelCarryOver").modal('hide');
}
//End Carry Over

//Start Calculate

function funCalTestCode() {
    if ($("#ddlCalTestId").val() > 0) {
        if ($("#ddlCalTestId").find("option:selected").text() != "") {
            $("#txtCalExpression").append($("#ddlCalTestId").find("option:selected").text());
        }
    }
}
function BindTableTestCalculated() {
    $("#preloader").css("display", "block");

    $.ajax({
         url: "/TestParameter/GetAllTestCalculated",
        method: 'get',
        success: OnsuccessTestCalculated
    });
}

function OnsuccessTestCalculated(response) {
    $('#tblTestCalculate').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblTestCalculate").css("display", "block");
        $("#divTestCalculateNoRecordFound").empty();
        $('#tblTestCalculate').DataTable({
            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 300, // used for fixed Hieght
            //bProcessing: true,
            //bLenghtChange: true,
            //retrieve: true,
            //bfilter: true,
            //bSort: true,
            //bPaging: true,
            data: response,
            columns: [
                { data: 'calTestCode', title: 'Test Code' },
                { data: 'calExpression', title: 'Expression' },


                {
                    data: "RefLow", title: 'Range',
                    render: function (data, type, row, meta) {
                        return row.refLow + " - " + row.refHigh;
                    }
                },
                { data: 'decimals', title: 'Decimals' },
                { data: 'untName', title: 'Unit Name' },

                {
                    data: "CalId", title: 'Action',
                    render: function (data, type, row, meta) {
                        return '<a class="" href="#" onclick = UpdateShowTestCalculated(' + row.calId + ') style="color:#656FEA;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Calculate Test"> <b><i class="bi bi-pencil-fill"></i></b></a>&nbsp;&nbsp;<a class="" href="#" onclick="DeleteConfirmationTestCalculated(' + row.calId + ')" style="color:red;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Calculate Test"><b><i class="bi bi-trash"></i></b></a>';
                    }
                },
            ],
            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblTestCalculate").css("display", "none");
        $("#divTestCalculateNoRecordFound").empty();
        $("#divTestCalculateNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
    $("#preloader").css("display", "none");

}

function SaveTestCalculated() {
    var returnValue = true;
    if ($("#txtCalTestCode").val() == "") {
        $("#txtCalTestCode").focus();
        Message("error", "Cal Code is required");
        returnValue = false;
    }
    else if ($("#txtCalTestName").val() == "") {
        $("#txtCalTestName").focus();
        Message("error", "Cal Name is required");
        returnValue = false;
    }
    else if ($("#txtCalDecimal").val() == "") {
        $("#txtCalDecimal").focus();
        Message("error", "Decimal is required");
        returnValue = false;
    }
    else if ($("#ddlCalUnit").val() == "") {
        $("#ddlCalUnit").focus();
        Message("error", "Unit is required");
        returnValue = false;
    }
    else if ($("#txtCalRefLow").val() == "") {
        $("#txtCalRefLow").focus();
        Message("error", "Low is required");
        returnValue = false;
    }
    else if ($("#txtCalRefHigh").val() == "") {
        $("#txtCalRefHigh").focus();
        Message("error", "High is required");
        returnValue = false;
    }
    else if ($("#ddlCalTestId").val() == "") {
        $("#ddlCalTestId").focus();
        Message("error", "Test is required");
        returnValue = false;
    }
    else if ($("#txtCalExpression").val() == "") {
        $("#txtCalExpression").focus();
        Message("error", "Expression is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");
        var obj = {
            CalId: $("#hdCalId").val(),
            CalTestCode: $("#txtCalTestCode").val(),
            CalTestName: $("#txtCalTestName").val(),
            Decimals: $("#txtCalDecimal").val(),
            Unit: $("#ddlCalUnit").val(),
            RefLow: $("#txtCalRefLow").val(),
            RefHigh: $("#txtCalRefHigh").val(),
            CalTestId: $("#ddlCalTestId").val(),
            CalExpression: $("#txtCalExpression").val(),
        }

        $.ajax({
            method: "POST",
            url: '/TestParameter/SaveTestCalculated',
            data: obj,
            success: function (result) {
                if (result > 0) {
                    if ($("#hdCalId").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearTestCalculated();
                    BindTableTestCalculated()
                }
                else {

                    Message("error", "Calculated Test Code is already exist");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function UpdateShowTestCalculated(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/TestParameter/GetByIdTestCalculated',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {

            if (result != null) {
                $("#hdCalId").val(result[0].calId);
                $("#txtCalTestCode").val(result[0].calTestCode);
                $("#txtCalTestName").val(result[0].calTestName);
                $("#txtCalDecimal").val(result[0].decimals);
                $("#ddlCalUnit").val(result[0].unit);
                $("#txtCalRefLow").val(result[0].refLow);
                $("#txtCalRefHigh").val(result[0].refHigh);
                $("#ddlCalTestId").val(result[0].calTestId).trigger("change");
                $("#txtCalExpression").val(result[0].calExpression);
                    $("#btnTestCalculated").html("Update");
                $("#modelTestCalculate").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function DeleteConfirmationTestCalculated(Id) {
    $("#modelDeleteConfirmationTestCalculated").modal('show');
    $("#hdDeleteTestCalculatedId").val(Id);
}

function DeleteTestCalculated() {
    $("#preloader").css("display", "block");
    var Id = $("#hdDeleteTestCalculatedId").val();
    $("#modelDeleteConfirmationTestCalculated").modal('hide');
    $("#hdDeleteTestCalculatedId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/TestParameter/DeleteTestCalculated',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableTestCalculated();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function ClearTestCalculated() {
     $("#hdCalId").val(''),
        $("#txtCalTestCode").val(''),
        $("#txtCalTestName").val(''),
        $("#txtCalDecimal").val(''),
        $("#ddlCalUnit").val(''),
        $("#txtCalRefLow").val(''),
         $("#txtCalRefHigh").val(''),
         $("#ddlCalTestId").val('').trigger("change"),
       $("#txtCalExpression").val(''),
        $("#btnTestCalculated").html("Save");
    $("#modelTestCalculate").modal('hide');
}
//End Calculate



//Start External

function funExternalResultType(Type) {
    if (Type == "Digital") {
        $("#divDigitalDetl").css("display", "block");
    }
    else {
        $("#divDigitalDetl").css("display", "none");
        $("#txtExtrnDecimal").val('');
        $("#ddlExtrnUnit").val('').trigger("change");
        $("#txtExtrnRefLow").val('');
        $("#txtExtrnRefHigh").val('');
    }
}

function BindTableTestExternal() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/TestParameter/GetAllTestExternal",
        method: 'get',
        success: OnsuccessTestExternal
    });
}

function OnsuccessTestExternal(response) {
    $('#tblTestExternal').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblExternal").css("display", "block");
        $("#divExternalNoRecordFound").empty();
        $('#tblTestExternal').DataTable({
            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 300, // used for fixed Hieght
            //bProcessing: true,
            //bLenghtChange: true,
            //retrieve: true,
            //bfilter: true,
            //bSort: true,
            //bPaging: true,
            data: response,
            columns: [
                { data: 'testCode', title: 'Test Code' },
                { data: 'testName', title: 'Test Name' },


                {
                    data: "refLow", title: 'Range',
                    render: function (data, type, row, meta) {
                        if (row.refLow != null) {
                            return row.refLow + " - " + row.refHigh;
                        }
                        else {
                            return '';
                        }
                    }
                },
                {
                    data: "decimals", title: 'Decimals',
                    render: function (data, type, row, meta) {
                        if (row.resultType != "Character") {
                            return row.decimals;
                        }
                        else {
                            return '';
                        }
                    }
                },
                { data: 'untName', title: 'Unit Name' },
                {
                    data: "externTestId", title: 'Action',
                    render: function (data, type, row, meta) {
                        return '<a class="" href="#" onclick = UpdateShowTestExternal(' + row.externTestId + ') style="color:#656FEA;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Test External"> <b><i class="bi bi-pencil-fill"></i></b></a>&nbsp;&nbsp;<a class="" href="#" onclick="DeleteConfirmationTestExternal(' + row.externTestId + ')" style="color:red;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Test External"><b><i class="bi bi-trash"></i></b></a>';
                    }
                },
            ],
            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblExternal").css("display", "none");
        $("#divExternalNoRecordFound").empty();
        $("#divExternalNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');

    }
    $("#preloader").css("display", "none");

}

function SaveTestExternal() {
    var returnValue = true;
    if ($("#txtExtrnTestCode").val() == "") {
        $("#txtExtrnTestCode").focus();
        Message("error", "External Test Code is required");
        returnValue = false;
    }
    else if ($("#txtExtrnTestName").val() == "") {
        $("#txtExtrnTestName").focus();
        Message("error", "External Name is required");
        returnValue = false;
    }
    else if ($("input[name='chkExtrnResultType']:checked").val() == "") {
        $("#chkExtrnResultType").focus();
        Message("error", "Result type is required");
        returnValue = false;
    }
    else if ($("#txtExtrnDecimal").val() == "" && $("input[name='chkExtrnResultType']:checked").val() == "Digital") {
        $("#txtExtrnDecimal").focus();
        Message("error", "Decimal is required");
        returnValue = false;
    }
    else if ($("#ddlExtrnUnit").val() == "" && $("input[name='chkExtrnResultType']:checked").val() == "Digital") {
        $("#ddlExtrnUnit").focus();
        Message("error", "Unit is required");
        returnValue = false;
    }
    else if ($("#txtExtrnRefLow").val() == "" && $("input[name='chkExtrnResultType']:checked").val() == "Digital") {
        $("#txtExtrnRefLow").focus();
        Message("error", "Low is required");
        returnValue = false;
    }
    else if ($("#txtExtrnRefHigh").val() == "" && $("input[name='chkExtrnResultType']:checked").val() == "Digital") {
        $("#txtExtrnRefHigh").focus();
        Message("error", "High is required");
        returnValue = false;
    }

    else {
        $("#preloader").css("display", "block");

        var obj = {
            ExternTestId: $("#hdExtrnId").val(),
            TestCode: $("#txtExtrnTestCode").val(),
            TestName: $("#txtExtrnTestName").val(),
            ResultType : $("input[name='chkExtrnResultType']:checked").val(),
            Decimals: $("#txtExtrnDecimal").val(),
            Unit: $("#ddlExtrnUnit").val(),
            RefLow: $("#txtExtrnRefLow").val(),
            RefHigh: $("#txtExtrnRefHigh").val(),
        }

        $.ajax({
            method: "POST",
            url: '/TestParameter/SaveTestExternal',
            data: obj,
            success: function (result) {
                if (result > 0) {
                    if ($("#hdExtrnId").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearTestExternal();
                    BindTableTestExternal()
                }
                else {

                    Message("error", "External Test Code is already exist");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function UpdateShowTestExternal(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/TestParameter/GetByIdTestExternal',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {

            if (result != null) {

                $("#hdExtrnId").val(result.externTestId);
                $("#txtExtrnTestCode").val(result.testCode);
                $("#txtExtrnTestName").val(result.testName);
                $("input[name=chkExtrnResultType][value=" + result.resultType + "]").prop('checked', true);
                funExternalResultType(result.resultType);
                $("#txtExtrnDecimal").val(result.decimals);
                $("#ddlExtrnUnit").val(result.unit).trigger("change");
                $("#txtExtrnRefLow").val(result.refLow);
                $("#txtExtrnRefHigh").val(result.refHigh);

                $("#btnTestExternal").html("Update");
                $("#modelTestExternalPmtr").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationTestExternal(Id) {
    $("#modelDeleteConfirmationTestExternal").modal('show');
    $("#hdDeleteTestExternalId").val(Id);
}

function DeleteTestExternal() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteTestExternalId").val();
    $("#modelDeleteConfirmationTestExternal").modal('hide');
    $("#hdDeleteTestExternalId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/TestParameter/DeleteTestExternal',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableTestExternal();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function ClearTestExternal() {
    $("#hdExtrnId").val('');
    $("#txtExtrnTestCode").val('');
    $("#txtExtrnTestName").val('');
//    $("input[name='chkExtrnResultType']:checked").val('');
    $("#txtExtrnDecimal").val('');
    $("#ddlExtrnUnit").val('').trigger("change");
    $("#txtExtrnRefLow").val('');
    $("#txtExtrnRefHigh").val('');

    $("#btnTestExternal").html("Save");
    $("#modelTestExternalPmtr").modal('hide');
}

function printData() {

    var doc = new jsPDF('p', 'pt', 'letter');

    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };

    var divid = printData.arguments[0];

    var currentdate = new Date();
    var dt = currentdate.getDate() + "/"
        + (currentdate.getMonth() + 1) + "/"
        + currentdate.getFullYear() + ":"
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds();

    margins = {
        top: 80,
        bottom: 60,
        left: 80,
        width: 900
    };
    //divTestRefPrint

    doc.text(276, 25, "Reference Range");
    doc.fromHTML($('#divTestRefPrint').html(),
        margins.top,
        margins.left, {
        'width': margins.width,
        'elementHandlers': specialElementHandlers
    });

    

    const pageCount = doc.internal.getNumberOfPages();

    for (let i = 1; i <= pageCount; i++) {
        doc.setPage(i);
        const pageSize = doc.internal.pageSize;
        const pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
        const pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
        const header = ""; //'Report 2014';
        const footer = 'This test has been conducted on AutoQuant 200i   ' + `       Page ${i} of ${pageCount}`;

        // Header
        doc.text(header, 40, 55, { baseline: 'top' });

        // Footer
        doc.text(footer, pageWidth / 2 - (doc.getTextWidth(footer) / 2), pageHeight - 15, { baseline: 'bottom' });
    }
    doc.save("TestRef" + dt + '.pdf');
}

//End External
