//const { data } = require("jquery");

//Start Shivani
var mResultVal = '';

$(document).ready(function () {
  

    $('.js-example-basic-multiple').select2();
    $('.js-example-basic-multiple1').select2();

    $("#txtPatientId").keypress(function (e) {
        var PatientId = $("#txtPatientId").val();
        SearchPatientById();
    });
    $("#txtPatName").keypress(function (e) {
        SearchPatientByName();
    });
    GetReportMst();
    GetAllPatientMaster();
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
});

function SearchPatientById() {
    var PatientId = $("#txtPatientId").val();
    if (PatientId.length > 2) {

        $("#preloader").css("display", "block");

        $.ajax({
            method: "GET",
            url: '/Schedule/SearchPatientById?PatientId=' + $("#txtPatientId").val(),
            success: function (result) {
                if (result != null) {
                    $("#divPatientId .es-list").empty();
                    var PatientIdList = '';
                    for (var i = 0; i < result.length; i++) {
                        PatientIdList += '<li class="es-visible" value="' + result[i].patId + '">' + result[i].patientId + '</li>';
                    }
                    $("#divPatientId .es-list").append(PatientIdList);
                    $('#divPatientId .editable-select').editableSelect();
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function SearchPatientByName() {
    var PatName = $("#txtPatName").val();
    if (PatName.length > 1) {
        $("#preloader").css("display", "block");

        $.ajax({
            method: "GET",
            url: '/Schedule/SearchPatientByName?PatientName=' + $("#txtPatName").val(),
            success: function (result) {
                if (result != null) {
                    $("#divPatientName .es-list").empty();
                    var PatientIdList = '';
                    for (var i = 0; i < result.length; i++) {
                        PatientIdList += '<li class="es-visible" value="' + result[i].patId + '">' + result[i].patName + '</li>';
                    }
                    $("#divPatientName .es-list").append(PatientIdList);
                    $('#divPatientName .editable-select').editableSelect();
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function GetAllPatientMaster() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Schedule/GetPaitentMst',
        success: function (result) {
            if (result != null) {


                //Doctor 
                $("#ddlDoctorId").empty();
                var doctor = '<option value=""></option>';
                for (var i = 0; i < result.doctor.length; i++) {
                    doctor += '<option value="' + result.doctor[i].drId + '">' + result.doctor[i].drName + '</option>'
                }
                $("#ddlDoctorId").append(doctor);

                //Sample Type 
                $("#ddlSmpType").empty();
                var SmpType = '<option value=""></option>';
                for (var i = 0; i < result.sample.length; i++) {
                    SmpType += '<option value="' + result.sample[i].smpId + '">' + result.sample[i].smpType + '</option>'
                }
                $("#ddlSmpType").append(SmpType);

                //Sample Container Type 
                $("#ddlSmpConType").empty();
                SampleContainer = '<option value="">select sample container</option>';
                for (var i = 0; i < result.sampleCon.length; i++) {
                    SampleContainer += '<option value="' + result.sampleCon[i].smpConId + '">' + result.sampleCon[i].smpConName + '</option>'
                }
                $("#ddlSmpConType").append(SampleContainer);
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function GetReportMst() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Reports/GetReportMst',
        success: function (result) {
            if (result != null) {
                //Test Code 
                $("#ddlTestCode").empty();
                $("#ddlPatient").empty();
                $("#ddlSample").empty();

                $("#ddlExtTestId").empty();

                var ddlTestCode = '';
                var ddlPatient = '';
                var ddlSample = '';

                var ddlQualitative = '<option value="">Select Qualitative</option>';
                var ddlExtTestId = '<option value="">Select External Test</option>';
                if (result.testCode.length > 0) {
                    for (var i = 0; i < result.testCode.length; i++) {
                        ddlTestCode += '<option value="' + result.testCode[i].testId + '">' + result.testCode[i].testCode + '</option>'
                    }
                }

                if (result.patient.length > 0) {
                    for (var i = 0; i < result.patient.length; i++) {
                        ddlPatient += '<option value="' + result.patient[i].patId + '">' + result.patient[i].patName + ' ( ' + result.patient[i].patientId + ' ) ' + '</option>'
                    }
                }
                if (result.sample.length > 0) {
                    for (var i = 0; i < result.sample.length; i++) {
                        ddlSample += '<option value="' + result.sample[i].smpId + '">' + result.sample[i].smpBarcode + ' ( ' + result.sample[i].smpId + ' ) ' + '</option>'
                    }
                }
                if (result.testExternal.length > 0) {
                    for (var i = 0; i < result.testExternal.length; i++) {
                        ddlExtTestId += '<option value="' + result.testExternal[i].testId + '">' + result.testExternal[i].testCode + '</option>'
                    }
                }
                if (result.qualitative.length > 0) {
                    for (var i = 0; i < result.qualitative.length; i++) {
                        ddlQualitative += '<option value="' + result.qualitative[i].qualtId + '">' + result.qualitative[i].qualtName + '</option>'
                    }
                }


                $("#ddlPatient").append(ddlPatient);
                $("#ddlSample").append(ddlSample);
                $("#ddlTestCode").append(ddlTestCode);
                sessionStorage.setItem("ExtTest", ddlExtTestId);
                sessionStorage.setItem("ddlQualitative", ddlQualitative);

                $(".select2-container--default").css("width", "100%");
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function SearchResult() {
    var TestId = $("#ddlTestCode").val();
    var PatientId = $("#ddlPatient").val();
    var SampleId = $("#ddlSample").val();

    if ($("#hdSearchPatientId").val() != "") {
        PatientId = $("#hdSearchPatientId").val();
    }
    var Type = [];
    $.each($("input[name='chkType']:checked"), function () {
        Type.push($(this).val());
    });

    $.ajax({
        //  url: "/Reports/GetResultReport?TestId=" + numbers + "&&PatientId=" + $("#ddlPatient").val() + "&&FormDate=" + $("#dtFromDate").val() + "&&ToDate=" + $("#txtToDate").val() + "&&Type=",
        url: '/Reports/GetResultReport',
        data: { TestId: TestId, PatientId: PatientId, SampleId: SampleId, FormDate: $("#dtFromDate").val(), ToDate: $("#txtToDate").val() },
        method: 'POST',
        success: OnsuccessBindTableRes
    });
    $("#hdSearchPatientId").val('');
}
var chart = "";
function OnsuccessBindTableRes(response) {
    $('#tblTabResult').DataTable().destroy();
    if (response.length > 0) {
        $("#divResultNoRecordFound").empty();

        $('#tblTabResult').DataTable({
            scrollX: true,
            scroll: true, // used for fixed header
            scrollY: 800, // used for fixed Hieght
            bProcessing: true,
            bLenghtChange: true,
            retrieve: true,
            bfilter: true,
            bSort: true,
            bPaging: true,
            dom: 'Bfrtip',
            data: response,
            columns: [
                {
                    data: "runDate", title: 'Run Date Time',
                    render: function (data, type, row, meta) {
                        if (row.runDate != null) {
                            if (moment(row.runDate).format("HH") == "00") {
                                return moment(row.runDate).format("DD MMM YYYY")
                            }
                            else {
                                return moment(row.runDate).format("DD MMM YYYY HH:mm:ss A")
                            }
                        }
                        else {
                            return ' ';
                        }
                    }
                },



                {
                    data: "patientName", visible: false, title: 'Patient',
                    render: function (data, type, row, meta) {

                        //return data + ' ( ' + row.p_barcode + ')';
                        return row.patientName;

                    }
                },


                //{
                //    data: "patientId", visible: false, title: 'Print',
                //    render: function (data, type, row, meta) {
                //        return '<a class="" href="#" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Patient Details" onclick = "ShowmodelPatient(' + row.p_id + ',' + row.samId + ')" data-bs-toggle="modal" data-bs-target="#modelEditPatient" style="color:#656FEA;"> <b><i class="bi bi-pencil-fill"></i></b></a><a class="" href="#" onclick="ShowmodelPatient(' + row.p_id + ')" style="color:red;"><b><i class="bi bi-printer"></i></b></a>';
                //    }  
                //},

                { data: 'testCode', title: 'Test Code' },
                {
                    data: 'resultValue', title: 'Result',
                    render: function (data, type, row, meta) {
                        return row.resultValue + " " + row.unit
                    }
                },
                //{ data: 'unit', title: 'Unit' },
                { data: 'flag', title: 'Flag' },
                { data: 'refLow', title: 'Ref Low' },
                { data: 'refHigh', title: 'Ref High' },
                { data: 'remark', title: 'Remark' },
                {
                    data: "testCode", title: 'Action',
                    render: function (data, type, row, meta) {
                        return '<a class="btn" href="#" onclick="ShowResultGraphChart(' + row.resultId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Show Reaction Curve" style="color:#656FEA;"><i class="bi bi-graph-up"></i></a>';
                    }
                },
            ],

            drawCallback: function (settings) {

                var api = this.api();
                var rows = api.rows({ page: 'current' }).nodes();
                var last = null;

                api.column(1, { page: 'current' }).data().each(function (group, i) {

                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="group"><td colspan="10" style="BACKGROUND-COLOR:rgb(237, 208, 0);font-weight:700;color:#006232;">' + group + '</td></tr>'
                        );
                        last = group;
                    }
                });
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })

            },
            dom: 'Brtip',
            buttons: [
                {
                    text: 'Print PDF',
                    extend: 'pdfHtml5',
                    /*extend: 'pdf',*/
                    download: 'open',
                    filename: 'Patient Report',
                    orientation: 'portrait', // landscape
                    pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7],
                        search: 'applied',
                        order: 'applied',
                        grouped_array_index: [1] //note the brackets, i think this is so you can group by multiple columns.
                    },
                    customize: function (doc) {
                        doc.content.splice(0, 1);
                        doc.pageMargins = [20, 50, 20, 60];
                        doc.defaultStyle.fontSize = 10;
                        doc.styles.tableHeader.fontSize = 13;
                     
                        doc['header'] = (function () {
                            return {
                                columns: [
                                 
                                    {
                                        alignment: 'center',
                                        italics: true,
                                        text: 'Patients Report',
                                        fontSize: 18,
                                    },
                                ],
                                margin: 20
                            }
                        });
                        var now = new Date();
                        var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();


                        doc['footer'] = (function (page, pages) {
                            return {
                                columns: [
                                    {
                                        alignment: 'left',
                                        //  text: 'Report Date : 12/12/2023'
                                        text: ['Report Date : ', { text: moment(new Date()).format('DD MMM YYYY').toString() }]
                                    },
                                    {
                                        alignment: 'center',
                                        text: ['This test has been conducted on FAA 200.']
                                    },
                                    {
                                        alignment: 'right',
                                        text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                    }
                                ],
                                margin: 20,
                              
                            }
                        });
                        var objLayout = {};
                        objLayout['width'] = function (i) { return 800; };
                        objLayout['hLineWidth'] = function (i) { return .5; };
                        objLayout['vLineWidth'] = function (i) { return .5; };
                        objLayout['hLineColor'] = function (i) { return '#aaa'; };
                        objLayout['vLineColor'] = function (i) { return '#aaa'; };
                        objLayout['paddingLeft'] = function (i) { return 4; };
                        objLayout['paddingRight'] = function (i) { return 4; };
                        doc.content[0].layout = objLayout;
                    }
                },

            ],
            order: [[1, 'asc']],
            rowGroup: {
                dataSrc: 1
            },

            //   "order": [[1, "desc"]],
            "responsive": true,
        });

        $(".buttons-pdf").css("background", "#656FEA");
        $(".buttons-pdf").css("color", "white");
        $('#tblTabResult').on('draw.dt', function () {
            $('[data-bs-toggle="tooltip"]').tooltip();
        });
        $(".divResultTable").css("display", "block");
    }
    else {
        $("#divResultNoRecordFound").empty();
        $("#divResultNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');

        $(".divResultTable").css("display", "none");
    }
}

function UpdateShowPatient(SampleId) {
    GetTestCode();
    GetTestProfile();
    UpdateShowSample(SampleId);
}

function GetTestCode() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/TestParameter/GetTestCode',
        success: function (result) {
            if (result != null) {
                //Test Code 
                $("#divTestCodeBind").empty();
                //$("#ddlRgtTestCode").empty();

                var proTestCode = '';
                var rgtTestCode = '';
                for (var i = 0; i < result.testCode.length; i++) {
                    proTestCode += '<div class="col-lg-2 mb-2 btn_Color btn rounded-pill" style="width:145px" id="' + result.testCode[i].testId + '" onclick = "funBtnSelect(this)"><button type="button"  class=""  >' + result.testCode[i].testCode + ' </button ></div>';
                    rgtTestCode += '<option value="' + result.testCode[i].testId + '">' + result.testCode[i].testCode + '</option>';
                }

                $("#divTestCodeBind").append(proTestCode);
                //$("#ddlRgtTestCode").append(rgtTestCode);
                $("#preloader").css("display", "none");

            }
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function GetTestProfile() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/TestParameter/GetAllTestProfile',
        success: function (result) {
            if (result != null) {

                //Test Profile 
                $("#divTestProfileBind").empty();


                var testProfile = '';
                for (var i = 0; i < result.length; i++) {
                    testProfile += '<div class="col-lg-2 mb-2 btn_Color btn rounded-pill" style="width:140px" id="' + result[i].profileId + '" onclick = "funTestProfileSelect(this)"><button type="button"  class="">' + result[i].profileName + ' </button ></div>';
                }

                $("#divTestProfileBind").append(testProfile);
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function UpdateShowSample(Id) {
    $("#preloader").css("display", "block");
    $.ajax({
        method: "GET",
        url: '/Schedule/GetByIdSample?Id=' + Id,
        success: function (result) {
            if (result != null) {
                if (result.patient != null) {

                    $("#divPatientId .es-list").append('<li class="es-visible" value="' + result.patient.patId + '">' + result.patient.patientId + '</li>');
                    $('#divPatientId .editable-select').editableSelect();

                    $("#divPatientName .es-list").append('<li class="es-visible" value="' + result.patient.patId + '">' + result.patient.patName + '</li>');
                    $('#divPatientName .editable-select').editableSelect();


                    $("#hdPatId").val(result.patient.patId);
                    $("#txtPatientId").val(result.patient.patientId);
                    $("#txtPatName").val(result.patient.patName);
                    if (result.patient.dob != null) {
                        $("#dtDOB").val(moment(result.patient.dob).format("YYYY-MM-DD"));
                        $("#dtDOB").change();
                    }
                    $("#txtHeight").val(result.patient.height);
                    $("#txtWeight").val(result.patient.weight);
                    $("#ddlGender").val(result.patient.sex);
                    $("#txtAddress").val(result.patient.address);
                    $("#divPatientId .es-list").append('<li class="es-visible" value="' + result.patient.patId + '">' + result.patient.patientId + '</li>');
                    $('#divPatientId .editable-select').editableSelect();


                }
                if (result.sample != null) {
                    $('#divPatientId .editable-select').editableSelect();
                    $('#divPatientName .editable-select').editableSelect();

                    $("#hdsmpId").val(result.sample.smpId).attr('disabled', true);
                    $("#txtSmpAge").val(result.sample.age).attr('disabled', true);
                    $("#txtSmpHeight").val(result.sample.height).attr('disabled', true);
                    $("#txtSmpWeight").val(result.sample.weight).attr('disabled', true);
                    $("#txtSmpBedNo").val(result.sample.bedNumber).attr('disabled', true);


                    $("#divSmpBarCode .es-list").append('<li class="es-visible" value="' + result.sample.smpId + '">' + result.sample.smpBarcode + '</li>');
                    $('#divSmpBarCode .editable-select').editableSelect();

                    $("#txtSmpBarCode").val(result.sample.smpBarcode).attr('disabled', true);

                    $("#ddlDoctorId").val(result.sample.drId).attr('disabled', true);
                    $("#txtLeb_HospitalName").val(result.sample.lebHospitalName).attr('disabled', true);
                    if (result.sample.registrationDate != null) {
                        $("#dtRegistrationDate").val(moment(result.sample.registrationDate).format("YYYY-MM-DD"));
                        $("#dtRegistrationDate").change().attr('disabled', true);
                    }
                    if (result.sample.collectionDate != null) {
                        $("#dtCollectionDate").val(moment(result.sample.collectionDate).format("YYYY-MM-DD"));
                        $("#dtCollectionDate").change().attr('disabled', true);
                    }
                    //$("#dtRegistrationDate").val(result.sample.registrationDate);
                    //$("#dtCollectionDate").val(result.sample.collectionDate);
                    $("#ddlSmpType").val(result.sample.smpTypeId).attr('disabled', true);
                    $("#ddlSmpConType").val(result.sample.smpContTypeId).attr('disabled', true);
                    $("#ddlSmpPosition").append('<option value="' + result.sample.position + '">' + result.sample.position + '</option>');
                    $("#ddlSmpPosition").val(result.sample.position).attr('disabled', true);

                    $("#txtHospitalArea").val(result.sample.hospitalArea).attr('disabled', true);
                    $("#txtSmpRemark").val(result.sample.smpRemark).attr('disabled', true);

                    if (result.sample.sampleTest.length > 0) {
                        var sampleTest = result.sample.sampleTest;
                        for (var i = 0; i < sampleTest.length; i++) {
                            $("#divTestCodeBind div").each(function () {
                                if (sampleTest[i].testId == $(this).attr("id")) {
                                    $(this).attr("accesskey", "check");
                                    this.style.background = "#039b54";
                                }
                            });
                        }
                    }
                }

                $("#btnSaveTestProfile").html("Update");
                $("#modelSample").modal("show");

            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function SavePatient() {
    var PatId = 0;
    var returnValue = true;
    if ($("#txtUnt_Name").val() == "") {
        $("#txtUnt_Name").focus();
        Message("error", "Unit Name is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            PatId: $("#hdPatId").val(),
            patientId: $("#txtPatientId").val(),
            PatName: $("#txtPatName").val(),
            DOB: $("#dtDOB").val(),
            Height: $("#txtHeight").val(),
            Weight: $("#txtWeight").val(),
            sex: $("#ddlGender").val(),
            Address: $("#txtAddress").val(),

        }
        $.ajax({
            method: "POST",
            url: '/Schedule/SavePatient',
            data: obj,
            success: function (data) {

                if (data.result) {
                    if (data.patId > 0) {
                        var SampleId = $("#hdsmpId").val();
                        var PatientId = data.patId;
                        $.ajax({
                            method: "POST",
                            url: '/Schedule/UpdatePatientIdSample',
                            data: { SampleId: SampleId, PatientId: PatientId },

                            success: function (data) {
                            },
                            error: function () {

                            }
                        });
                        if ($("#hdPatId").val() > 0) {

                            Message("success", "Updated Data Successfully");

                        }
                        else {
                            Message("success", "Saved Data Successfully");
                        }
                        SearchResult();

                        $("#modelSample").modal('hide');
                    }
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearSample() {
    $("#hdPatId").val('');
    $("#txtPatientId").val('');
    $("#txtPatName").val('');

    $("#dtDOB").attr("data-date", '').trigger("change")
    //$("#dtDOB").val(format2);

    $("#txtHeight").val('');
    $("#txtWeight").val('');
    $("#ddlGender").val('');
    $("#txtAddress").val('');

    $("#btnSavePatient").html("Save");
    $("#lblPatient").html("New Patient");

    $("#hdsmpId").val('');
    $("#txtSmpAge").val('');
    $("#txtSmpHeight").val('');
    $("#txtSmpWeight").val('');
    $("#txtSmpBedNo").val('');
    $("#txtSmpBarCode").val('');
    $("#ddlDoctorId").val('');
    $("#txtLeb_HospitalName").val('');
    $("#dtRegistrationDate").val('');
    $("#dtCollectionDate").val('');
    $("#ddlSmpType").val('');
    $("#ddlSmpConType").val('');
    $("#ddlSmpPosition").val('');

    $("#txtHospitalArea").val('');
    $("#txtSmpRemark").val('');
    ClearSelectTestCode();
    $("#divTestProfileBind div").each(function () {
        $(this).attr("accesskey", "");
        this.style.background = "#707274";
    });
    $("#modelSample").modal('hide');
}

function ResultPDF(SampleId) {
    $("#preloader").css("display", "block");
    $.ajax({
        method: "GET",
        url: '/Reports/ResultReportPDF?SampleId=' + SampleId,
        success: function (result) {
            if (result != null) {
                if (result.reportPDF.length > 0) {


                    const doc = new jsPDF('p', 'pt', 'A4');
                    doc.setFontSize(15);
                    doc.text(250, 30, 'Patient Reports');  //1 Left Margin, 2. Top margin, 3 Text
                    doc.setFontSize(10);

                    doc.text(20, 60, 'Name : ' + result.reportPDF[0].patientName);
                    doc.text(20, 75, 'Patient ID : ' + result.reportPDF[0].patientId);
                    doc.text(20, 90, 'Sample Type : ' + result.reportPDF[0].sampleType);
                    doc.text(20, 105, 'Patient Remark : ' + result.reportPDF[0].patientName);

                    doc.text(250, 60, 'Age : ' + result.reportPDF[0].age);
                    doc.text(250, 75, 'Gender : ' + result.reportPDF[0].gender);
                    doc.text(250, 90, 'Lab Name : ' + result.reportPDF[0].labName);
                    doc.text(250, 105, 'Department : ' + result.reportPDF[0].department);

                    doc.text(450, 60, 'Doctor : ' + result.reportPDF[0].doctor);
                    doc.text(450, 75, 'ID : ' + result.reportPDF[0].patId);
                    doc.text(450, 90, 'Check Date : ' + moment(result.reportPDF[0].runDate).format('DD MMM YYYY'));

                    doc.line(20, 110, 570, 110); // buttom line
                    doc.setFontSize(15);
                    doc.text(250, 130, 'Test Result');  //1 Left Margin, 2. Top margin, 3 Text
                    doc.setFontSize(10);

                    const col = ['Test Code', 'Test Name', 'Result Value', 'Flag', 'Refrence Range'];

                    const rows = [];

                    /* The following array of object as response from the API req  */

                    const itemNew = result.reportPDF;

                    itemNew.forEach(element => {

                        const temp = [element.testCode, element.testName, element.resultValue + " " + element.unit, element.flag, element.refLow + " - " + element.refHigh];
                        rows.push(temp);
                        doc.autoTable(col, rows, { margin: { top: 140 }, height: 'auto' });
                    });
                   

                    doc.line(20, 800, 570, 800); // buttom line
                    doc.text(20, 820, 'Operator : Admin');
                    doc.text(450, 820, 'Report Date : ' + moment(new Date()).format('DD MMM YYYY').toString());
                    doc.setFontType("bold");
                    doc.text(200, 830, 'This test has been conducted on FAA 200.');

                    doc.save('Report.pdf');
                    window.open(doc.output('bloburl'), '_blank');

                }
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function AddExternalResult(SampleId, PatientName, PatientBarCode) {
    $("#hdExtPatientId").val();
    $("#txtExtPatientName").val(PatientName + " (" + PatientBarCode + ")");
    $("#txtExtSampleId").val(SampleId);

    $.ajax({
        method: "GET",
        url: '/Reports/GetResultExternalBySampleId?SampleId=' + SampleId,
        success: function (result) {
            if (result != null) {
                ExtTstCounter = 0;
                $("#divExternalRow").empty();
                if (result.resultExternal.length > 0) {
                    for (var i = 0; i < result.resultExternal.length; i++) {
                        AddRowExternalResult();
                        $("#hdScheduleId_" + i).val(result.resultExternal[i].scheduleId);
                        $("#ddlExtTestId_" + i).val(result.resultExternal[i].externTestId).trigger('change');
                        mResultVal = result.resultExternal[i].result;
                        if (result.resultExternal[i].resultDate != null) {
                            $("#dtExtRunDate").val(moment(result.resultExternal[i].resultDate).format("YYYY-MM-DD"));
                            $("#dtExtRunDate").change();
                        }
                        //  $("#txtExtResult_" + i).val(result.resultExternal[i].result);
                    }
                }
                else {
                    AddRowExternalResult();
                }
            }
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });

    $("#modelExternalResult").modal("show");
}

var ExtTstCounter = 0;

function AddRowExternalResult() {
    var returnValue = true;
    if (ExtTstCounter != 0 && $("#ddlExtTestId_" + (ExtTstCounter - 1)).val() == "" && $("#divExtNewRow_" + (ExtTstCounter - 1)).attr("accesskey") != "Delete") {
        $("#ddlExtTestId_" + (ExtTstCounter - 1)).focus();
        Message("warning", "Please select external test code");
        returnValue = false;
    }
    else if (ExtTstCounter != 0 && $("#txtExtResult_" + (ExtTstCounter - 1)).val() == "" && $("#divExtNewRow_" + (ExtTstCounter - 1)).attr("accesskey") != "Delete") {
        $("#txtExtResult_" + (ExtTstCounter - 1)).focus();
        Message("warning", "Please enter result");
        returnValue = false;
    }
    else {
        $("#divExternalRow").append('<div class="row divExtNewRow" accesskey="" id="divExtNewRow_' + ExtTstCounter + '"><div class= "col-lg-4"><div class="cd-position-relative  mb-3"><input type="hidden" id="hdScheduleId_' + ExtTstCounter + '" class="hdScheduleId"><select id="ddlExtTestId_' + ExtTstCounter + '" onchange="GetExternalTestById(' + ExtTstCounter + ')" class="form-select select floating-label-control cd-form-control ddlExtTestId">' + sessionStorage.getItem('ExtTest') + '</select><label class="floating-label" for="inputNameDept">External Test Code</label></div></div><div class="col-lg-4"><p class="lblExtTestName" id="lblExtTestName_' + ExtTstCounter + '" style="margin-top: 10px;text-align: center;"></p></div><div class="col-lg-3"><div class= "input-group cd-position-relative" id ="divResult_' + ExtTstCounter + '"></div></div><div class="col-lg-1"><button type="button" id="btnAddExternal_' + ExtTstCounter + '" class="btn btn-danger" onclick="DeleteRowExternalResult(' + ExtTstCounter + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete External Row"><i class="bi-trash bi"></i></button></div></div> ');
        if (ExtTstCounter > 0) {
            for (var i = ExtTstCounter; i >= 0; i--) {
                if (ExtTstCounter != i) {
                    $("#btnAddExternal_" + i).removeClass('colorCode');
                    $("#btnAddExternal_" + i + " i").removeClass('bi bi-plus');

                    $("#btnAddExternal_" + i).addClass('btn-danger');
                    $("#btnAddExternal_" + i + " i").addClass('bi bi-trash');


                    $("#btnAddExternal_" + i).removeAttr("onclick");
                    $("#btnAddExternal_" + i).attr("onclick", "DeleteRowExternalResult('" + i + "')");
                }
                if ($("#divExtNewRow_" + (i - 1)).attr("accesskey") != "Delete") {

                    $('#ddlExtTestId_' + ExtTstCounter + ' option[value="' + $('#ddlExtTestId_' + (i - 1)).val() + '"]').detach();
                }
            }
        }
        $("#ddlExtTestId_" + ExtTstCounter).select2({
            dropdownParent: $("#modelExternalResult"),
            width: "100%"
        });

        ExtTstCounter++;
    }
}

function DeleteRowExternalResult(counter) {
    $("#divExtNewRow_" + counter).attr("accesskey", "Delete");
    $("#divExtNewRow_" + counter).css("display", "none");
}

function GetExternalTestById(Counter) {
    var ExternTestId = $("#ddlExtTestId_" + Counter).val();
    var SmpId = $("#txtExtSampleId").val();
    if (ExternTestId != "") {
        $.ajax({
            method: "GET",
            url: '/Reports/GetExternalTestById?ExternTestId=' + ExternTestId + "&&SampleId=" + $("#txtExtSampleId").val(),
            success: function (result) {
                if (result != null) {
                    if (result.externalTest != null) {
                        $("#divResult_" + Counter).empty();
                        if (result.externalTest.testCode != "EXTTESTQUALI") {
                            $("#divResult_" + Counter).append('<input type = "number" class= "form-control floating-label-control cd-form-control txtExtResult" id = "txtExtResult_' + Counter + '" placeholder = "" > <input type="hidden" class="hdUnitId" id="hdUnitId_' + Counter + '"><span class="input-group-text lblExtUnit" id="lblExtUnit_' + Counter + '"></span><label class="floating-label" for="floatingSVolume">Result</label>');
                        }
                        else {
                            $("#divResult_" + Counter).append('<select id="txtExtResult_' + Counter + '" class="txtExtResult form-control form-select select floating-label-control cd-form-control select2">' + sessionStorage.getItem("ddlQualitative") + '</select><label class="floating-label" for="inputtxtTest">Result</label><input type="hidden" id="lblExtUnit_' + Counter + '" class="lblExtUnit">');
                        }
                        $("#lblExtTestName_" + Counter).text(result.externalTest.testName);
                        //$("#txtExtResult_" + Counter).val(result.externalTest);
                        $("#hdUnitId_" + Counter).val(result.externalTest.unit);
                        $("#lblExtUnit_" + Counter).html(result.externalTest.untName);
                        $("#txtExtResult_" + Counter).val(result.externalTest.result);
                    }
                    else {
                        $("#lblExtTestName_" + Counter).text('');
                        // $("#txtExtResult_" + Counter).val(result.externalTest);
                        $("#lblExtUnit_" + Counter).html('');
                    }
                }
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function SaveExternalTestResult() {
    var list = [];
    var returnValue = true;
    if ($("#dtExtRunDate").val() == "") {
        $("#dtExtRunDate").focus();
        returnValue = false;
        Message("warning", "Run Date is required");
    }
    else {
        $("#divExternalRow .divExtNewRow").each(function () {
            if ($(this).find('.ddlExtTestId').val() != "" && $(this).find('.txtExtResult').val() != "") {
                list.push({
                    scheduleId: $(this).find('.hdScheduleId').val(),
                    ExternTestId: $(this).find('.ddlExtTestId').val(),
                    smpId: $("#txtExtSampleId").val(),
                    resultDate: $("#dtExtRunDate").val(),
                    result: $(this).find('.txtExtResult').val(),
                    unit: $(this).find('.hdUnitId').val(),
                    flag: $(this).attr("accesskey")
                });
            }
        });
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/Reports/SaveExternalTestResult',
            data: { resultsExterns: list },
            success: function (result) {
                if (result) {
                    Message("success", "Saved Data Successfully");
                    $("#modelExternalResult").modal('hide');

                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearExternalResult() {

}

function showchartModal(sID, flag, res) {

    //var name = flag.toString();
    var fval = flag.toString();
    var rval = res.toString();
    var mval = sID.toString();

    //var str = "You Have Entered "
    //    + "Name: " + name;
    //+ " and Marks: " + marks;
    //$("#modal_body").html(str);

    $("#txtFlag").val(fval);
    $("#txtResult").val(rval);
    $("#txtMethod").val(mval);


    $("divLineChartId").empty();

    var salesGraphChartCanvas = $("#LineChartId").get(0).getContext("2d");

    var salesGraphChartData = {
        labels: "line",
        datasets: [{
            label: ['', ''],
            /* fill: false,*/
            borderWidth: 2,
            lineTension: 0,
            spanGaps: true,
            borderColor: "black",
            pointRadius: 3,
            pointHoverRadius: 7,
            pointColor: "black",
            pointBackgroundColor: "black",
            data: [65, 59, 80, 81, 56, 55, 40]
        }]
    }

    var salesGraphChartOptions = {
        /*  maintainAspectRatio: false,*/
        responsive: true,
        legend: {
            display: false
        },

    }
    var salesGraphChart = new Chart(salesGraphChartCanvas, {
        type: 'line',
        data: salesGraphChartData,
        options: salesGraphChartOptions,
        axisX: {
            title: "Primary"
        },
        axisY: {
            title: "Primary"
        },
    })


    $("#modelResultChartGraph").modal('show');
}

function ShowResultGraphChart(ResultId) {
    if (ResultId != "") {
        $.ajax({
            method: "GET",
            url: '/Reports/GetGraphChatByResultId?ResultId=' + ResultId,
            success: function (result) {
                if (result != null) {
                    $("#lblTestName").text(result.testCode);
                    $("#lblPatientName").text(result.patName);
                    $("#lblSampleId").text(result.smpId);
                    $("#lblResultDateTime").text(moment(result.resultDateTime).format("DD-MMM-YYYY HH:mm:ss A"));

                    $("#lblMethodType").text(result.mthdType);
                    $("#lblStartPoint").text(result.startPoint);
                    if (result.endPoint != "") {
                        $("#lblEndPoint").text(parseFloat(result.endPoint).toFixed(2));
                    }
                    else {
                        $("#lblEndPoint").text('');
                    }
                    $("#lblResult").text(result.resultValue + " " + result.unit);
                    $("#lblReadTime").text(result.readTime + " Sec");
                  
                    var response = [];
                  //  if (result.readResult.length > 0) {
                        // for (var i = 0; i < result.readResult.length; i++) {
                        for (var i = 0; i < result.readResult.length; i++) {
                            response.push({ x: 1, y: result.readResult[i].r1 })
                            response.push({ x: 2, y: result.readResult[i].r2 })
                            response.push({ x: 3, y: result.readResult[i].r3 })
                            response.push({ x: 4, y: result.readResult[i].r4 })
                            response.push({ x: 5, y: result.readResult[i].r5 })
                            response.push({ x: 6, y: result.readResult[i].r6 })
                            response.push({ x: 7, y: result.readResult[i].r7 })
                            response.push({ x: 8, y: result.readResult[i].r8 })
                            response.push({ x: 9, y: result.readResult[i].r9 })
                            response.push({ x: 10, y: result.readResult[i].r10 })
                            response.push({ x: 11, y: result.readResult[i].r11 })
                            response.push({ x: 12, y: result.readResult[i].r12 })
                            response.push({ x: 13, y: result.readResult[i].r13 })
                            response.push({ x: 14, y: result.readResult[i].r14 })
                            response.push({ x: 15, y: result.readResult[i].r15 })
                            response.push({ x: 16, y: result.readResult[i].r16 })
                            response.push({ x: 17, y: result.readResult[i].r17 })
                            response.push({ x: 18, y: result.readResult[i].r18 })
                            response.push({ x: 19, y: result.readResult[i].r19 })
                            response.push({ x: 20, y: result.readResult[i].r20 })
                            response.push({ x: 21, y: result.readResult[i].r21 })
                            response.push({ x: 22, y: result.readResult[i].r22 })
                            response.push({ x: 23, y: result.readResult[i].r23 })
                            response.push({ x: 24, y: result.readResult[i].r24 })
                            response.push({ x: 25, y: result.readResult[i].r25 })
                            response.push({ x: 26, y: result.readResult[i].r26 })
                            response.push({ x: 27, y: result.readResult[i].r27 })
                            response.push({ x: 28, y: result.readResult[i].r28 })
                            response.push({ x: 29, y: result.readResult[i].r29 })
                            response.push({ x: 30, y: result.readResult[i].r30 })
                            response.push({ x: 31, y: result.readResult[i].r31 })
                            response.push({ x: 32, y: result.readResult[i].r32 })
                            response.push({ x: 33, y: result.readResult[i].r33 })
                            response.push({ x: 34, y: result.readResult[i].r34 })
                            response.push({ x: 35, y: result.readResult[i].r35 })
                            response.push({ x: 36, y: result.readResult[i].r36 })
                            response.push({ x: 37, y: result.readResult[i].r37 })
                            response.push({ x: 38, y: result.readResult[i].r38 })
                            response.push({ x: 39, y: result.readResult[i].r39 })
                            response.push({ x: 40, y: result.readResult[i].r40 })
                            response.push({ x: 41, y: result.readResult[i].r41 })
                            response.push({ x: 42, y: result.readResult[i].r42 })
                            response.push({ x: 43, y: result.readResult[i].r43 })
                            response.push({ x: 44, y: result.readResult[i].r44 })
                            response.push({ x: 45, y: result.readResult[i].r45 })
                            response.push({ x: 46, y: result.readResult[i].r46 })
                            response.push({ x: 47, y: result.readResult[i].r47 })
                            response.push({ x: 48, y: result.readResult[i].r48 })
                            response.push({ x: 49, y: result.readResult[i].r49 })
                            response.push({ x: 50, y: result.readResult[i].r50 })
                            response.push({ x: 51, y: result.readResult[i].r51 })
                            response.push({ x: 52, y: result.readResult[i].r52 })
                            response.push({ x: 53, y: result.readResult[i].r53 })
                            response.push({ x: 54, y: result.readResult[i].r54 })
                            response.push({ x: 55, y: result.readResult[i].r55 })
                            response.push({ x: 56, y: result.readResult[i].r56 })
                            response.push({ x: 57, y: result.readResult[i].r57 })
                            response.push({ x: 58, y: result.readResult[i].r58 })
                            response.push({ x: 59, y: result.readResult[i].r59 })
                            response.push({ x: 60, y: result.readResult[i].r60 })
                            response.push({ x: 61, y: result.readResult[i].r61 })
                            response.push({ x: 62, y: result.readResult[i].r62 })
                            response.push({ x: 63, y: result.readResult[i].r63 })
                            response.push({ x: 64, y: result.readResult[i].r64 })
                            response.push({ x: 65, y: result.readResult[i].r65 })
                            response.push({ x: 66, y: result.readResult[i].r66 })
                            response.push({ x: 67, y: result.readResult[i].r67 })
                            response.push({ x: 68, y: result.readResult[i].r68 })
                            response.push({ x: 69, y: result.readResult[i].r69 })
                            response.push({ x: 70, y: result.readResult[i].r70 })
                            response.push({ x: 71, y: result.readResult[i].r71 })
                            response.push({ x: 72, y: result.readResult[i].r72 })
                            response.push({ x: 73, y: result.readResult[i].r73 })
                            response.push({ x: 74, y: result.readResult[i].r74 })
                            response.push({ x: 75, y: result.readResult[i].r75 })


                        }
                        CreateLineGraph("LineChartReport", "Result", "#3b8bba", response);
                   // }
                    var arrayOfReading = "";
                    for (var i = 0; i < response.length; i++) {
                        if (response[i].y != 0 && response[i].y != null)
                            arrayOfReading += response[i].y+", ";
                    }

                    $("#lblRstTestName").text(result.testCode);
                   // $("#lblRstPatientName").text(result.patName);
                    $("#lblRstMethodType").text(result.mthdType);
                    $("#lblArrayOfRead").text(arrayOfReading);
                    $("#lblRstStartPoint").text(result.startPoint);

                    if (result.endPoint != "") {
                        $("#lblRstEndPoint").text(parseFloat(result.endPoint).toFixed(2));
                    }
                    else {
                        $("#lblRstEndPoint").text('');
                    }
                    $("#modelResultChartGraph").modal("show");
                    $('#modelResultChartGraph').on('shown.bs.modal', function () {
                        chart.render();
                    });
                }
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}



var chart = "";

function CreateLineGraph(LineChartId, Label, Color, Result) {
    $(".div" + LineChartId).empty();
    $(".div" + LineChartId).append('<div id="' + LineChartId + '" style="height: 400px; width: 800;"></div>');
    //var rst = [];
    //var sumResult = 0;
    //for (var i = 0; i < Result.length; i++) {
    //    rst.push({ x: (i + 1), y: Result[i] });
    //    sumResult += Result[i];
    //}
    chart = new CanvasJS.Chart(LineChartId, {
        animationEnabled: true,
        title: {
            label: "Daily QC",
            text: "Report",
            fontSize: 15,
            fontFamily: "tahoma",
        },
        theme: "light1",
        axisX: {
            interval: 5,
        },
        axisY: {
            title: "Result",
            gridThickness: 0,
            //minimum: -3,
            //maximum: 13,
            //valueFormatString: "#0,,.",
            //suffix: "mn",
        },
        toolTip: {
            shared: true
        },
        chartArea: {
            // leave room for y-axis labels
            width: '94%'
        },

        /*      legend: false,*/
        //legend: {
        //    cursor: "pointer",
        //    verticalAlign: "top",
        //    horizontalAlign: "center",
        //    dockInsidePlotArea: true,
        //    itemclick: toogleDataSeries
        //},
        note: {

        },
        data: [{
            type: "spline",
            indexLabelFontSize: 16,
            showInLegend: false,
            //visible: false,
            //yValueFormatString: "##.00mn",
            name: "Result",
            dataPoints: Result,
        },
        ]
    });
    //$('#modelRecalculateResult').on('shown.bs.modal', function () {
    //    chart.render();
    // });
    chart.render();
    //chart.axisY[0].set("minimum", (-4 * SD) + Mean);
    //chart.axisY[0].set("maximum", (4 * SD) + Mean);


    function toogleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        chart.render();
    }
}


function RecalculatePoint() {
    $("#modelRecalculateResult").modal("show");
}

function funRecalculateResult() {
    $("#preloader").css("display", "block");

}

//End Shivani







function DisplayCurrentTime(date) {
    //var date = "2023-10-23 14:03:01"; // new Date();

    var vartm = date.split(" ");
    var tmsplit = vartm[1].toString();

    varhr = tmsplit.split(":");
    varhrsplit = varhr[0].toString();

    var hours = varhrsplit > 12 ? varhrsplit - 12 : varhrsplit;
    var am_pm = varhrsplit >= 12 ? "PM" : "AM";
    hours = hours < 10 ? "0" + hours : hours;
    var minutes = varhr[1].toString();// < 10 ? "0" + varhr[1].toString() : varhr[1].toString();
    var seconds = varhr[2].toString();// < 10 ? "0" + varhr[2].toString() : varhr[2].toString();
    time = hours + ":" + minutes + ":" + seconds + " " + am_pm;
};

function GetPatientNameDetail() {
    UpdateShowPatient($("#divPatientName .es-list .es-visible").val());
}

function Graph(LineChartId, Label, Color, LabelContent, DataContent) {
    var salesGraphChartData = {
        labels: LabelContent,
        datasets: [{
            label: Label,
            fill: false,
            borderWidth: 2,
            lineTension: 0,
            spanGaps: true,
            borderColor: Color,
            pointRadius: 3,
            pointHoverRadius: 7,
            pointColor: Color,
            pointBackgroundColor: Color,
            data: DataContent
        }]

    }
    var chart = new CanvasJS.Chart("chartContainer",
        {

            title: {
                text: "Earthquakes - per month"
            },

            axisX: {
                valueFormatString: "MMM",
                interval: 1,
                intervalType: "month",
                title: "Primary"

            },
            axisY: {
                includeZero: false

            },
            data: [
                {
                    type: "line",

                    data: salesGraphChartData,
                }
            ]
        });

    chart.render();
}


/* ----------------------------- data table groping code ------------------ */

function grpTbl() {
    var groupColumn = 2;
    var table = $('#tblTabResult').DataTable({
        columnDefs: [{ visible: false, targets: groupColumn }],
        order: [[groupColumn, 'asc']],
        displayLength: 25,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({ page: 'current' }).nodes();
            var last = null;

            api.column(groupColumn, { page: 'current' })
                .data()
                .each(function (group, i) {
                    if (last !== group) {
                        $(rows)
                            .eq(i)
                            .before(
                                '<tr class="group"><td colspan="5">' +
                                group +
                                '</td></tr>'
                            );

                        last = group;
                    }
                });
        }
    });

    // Order by the grouping
    $('#example tbody').on('click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if (currentOrder[0] === groupColumn && currentOrder[1] === 'asc') {
            table.order([groupColumn, 'desc']).draw();
        }
        else {
            table.order([groupColumn, 'asc']).draw();
        }
    });
}