﻿
$(document).ready(function () {
    if (sessionStorage.getItem("ManuTabId") == null) {
        $("#Menu_nav_Id li .Tab2").addClass("active");
        $("#TabNav_2").addClass("show active");
        $("#Menu-nav" + $("#Menu_nav_Id").attr("accesskey") + " li a").removeClass("active");
        $("#Menu-nav" + $("#Menu_nav_Id").attr("accesskey") + " #Tab_Menu2" + " a").addClass("active");

    }

    GetRole();
    UpdateShowHospital();
    BindTableDoctor();
    BindTableOperator();
    BindTableSampleType();
    BindTableUnitType();
    BindTableQualitative();
    BindTableResult();
    BindTableClinical();
    BindTableSampleContainer();
    BindTableReagentBottleType();
    BindTableQcMaterial();
    BindTableManufacturer();

    var MenuData = sessionStorage.getItem("MenuBind");
    if (MenuData != null) {

    }

    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })

});

//Start With Customer Detail
//Start Hospital
function SaveHospital() {
    var returnValue = true;
    if ($("#txtHstl_Name").val() == "") {
        $("#txtHstl_Name").focus();
        Message("error", "Hospital Name is required");
        returnValue = false;
    }
    else if ($("#txtHstl_PhoneNo").val() == "") {
        $("#txtHstl_PhoneNo").focus();
        Message("error", "Hospital Phone No is required");
        returnValue = false;
    }
    else if ($("#txtHstl_Address").val() == "") {
        $("#txtHstl_Address").focus();
        Message("error", "Hospital Address is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            HstlId: $("#txtHstl_Id").val(),
            HstlName: $("#txtHstl_Name").val(),
            HstlPhoneNo: $("#txtHstl_PhoneNo").val(),
            HstlAddress: $("#txtHstl_Address").val()
        }
        $.ajax({
            method: "POST",
            url: '/DataDirectory/SaveHospitalinfo',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result) {
                    if ($("#txtHstl_Id").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    UpdateShowHospital()
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function UpdateShowHospital() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/DataDirectory/GetAllHospitalinfo',
        crossDomain: true,
        success: function (result) {
            if (result != null) {
                $("#txtHstl_Id").val(result.hstlId);
                $("#txtHstl_Name").val(result.hstlName);
                $("#txtHstl_PhoneNo").val(result.hstlPhoneNo);
                $("#txtHstl_Address").val(result.hstlAddress);
                $("#btnHstl_Save").text("Update");
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}
//End Hospital
//Start Doctor
function BindTableDoctor() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/DataDirectory/GetAllDoctor",
        method: 'get',
        success: OnsuccessDoctor
    });
}
var table = "";
function OnsuccessDoctor(response) {


    if (response.length > 0) {
        $("#divtblDoctor").css("display", "block");
        $("#divDoctorNoRecordFound").empty();
        $('#tblDoctor').DataTable().destroy();
     table =   $('#tblDoctor').DataTable({
            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 900, // used for fixed header
            //scrollCollapse: true, // used for fixed header
            //bProcessing: true,
            //bLenghtChange: true,
            //retrieve: true,
            //bfilter: true,
            //bSort: true,
            //bPaging: false,
            data: response,
            columns: [
                //  { data: 'drId', name: 'sr no' },
                { data: 'drName', title: 'Doctor Name' },
                { data: 'drDepartment', title: 'Department' },
                { data: 'mobileNo', title: 'Mobile Number' },
                { data: 'drRemark', title: 'Remarks' },
                {
                    data: "drId", title: 'Action',
                    render: function (data, type, row, meta) {
                      //  return '<button type="button" class="UpdateDoctor" onclick="UpdateShowDoctor(' + row.drId + ') " data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Doctor Detail"><i class="bi bi-pencil-fill"></i></button>'
                        return '<a type="button" style="color:#656FEA;" class="" href="#" onclick = UpdateShowDoctor(' + row.drId + ')  data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Doctor Detail"> <i class="bi bi-pencil-fill"></i> </a> &nbsp;<a type="button" class="DeleteDoctor" href="#" style="color:red" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Doctor Detail"  onclick="DeleteConfirmationDoctor(' + row.drId + ')"><i class="bi bi-trash"></i></a>';
                    }
                },
            ],
         "drawCallback": function (settings) {
             var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
             var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                 return new bootstrap.Tooltip(tooltipTriggerEl)
             })
         },
        });
 
    }
    else {
        $("#divtblDoctor").css("display", "none");
        $("#divDoctorNoRecordFound").empty();
        $("#divDoctorNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');

    }
    $("#preloader").css("display", "none");
}



function SaveDoctor() {
    var returnValue = true;
    if ($("#txtDr_Name").val() == "") {
        $("#txtDr_Name").focus();
        Message("error", "Doctor Name is required");
        returnValue = false;
    }
    else if ($("#txtDr_Department").val() == "") {
        $("#txtDr_Department").focus();
        Message("error", "Doctor Department is required");
        returnValue = false;
    }
    //else if ($("#txtDr_MobileNo").val() == "") {
    //    $("#txtDr_MobileNo").focus();
    //    Message("error", "Doctor Mobile Number is required");
    //    returnValue = false;
    //}
    else if ($("#txtDr_Remark").val() == "") {
        $("#txtDr_Remark").focus();
        Message("error", "Doctor Remarks is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            DrId: $("#hdDr_Id").val(),
            DrName: $("#txtDr_Name").val(),
            DrDepartment: $("#txtDr_Department").val(),
            MobileNo: $("#txtDr_MobileNo").val(),
            DrRemark: $("#txtDr_Remark").val(),
        }
        $.ajax({
            method: "POST",
            url: '/DataDirectory/SaveDoctor',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result) {
                    if ($("#hdDr_Id").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }

                    BindTableDoctor()
                    ClearDoctor();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function UpdateShowDoctor(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/DataDirectory/GetByIdDoctor',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {

            if (result != null) {
                $("#hdDr_Id").val(result.drId);
                $("#txtDr_Name").val(result.drName);
                $("#txtDr_Department").val(result.drDepartment);
                $("#txtDr_MobileNo").val(result.mobileNo);
                $("#txtDr_Remark").val(result.drRemark);
                $("#btnDrSave").html("Update");
                $("#btnDrSave").attr("data-bs-original-title", "Update Doctor Detail");
                // $("#lblDrTitle").html("Update Doctor");

                $("#modelDoctor").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function DeleteConfirmationDoctor(Id) {
    $("#modelDeleteConfirmationDoctor").modal('show');
    $("#hdDeleteDoctorId").val(Id);
}

function DeleteDoctor() {
    $("#preloader").css("display", "block");
    var Id = $("#hdDeleteDoctorId").val();
    $("#modelDeleteConfirmationDoctor").modal('hide');
    $("#hdDeleteDoctorId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/DataDirectory/DeleteDoctor',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableDoctor();
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearDoctor() {
    $("#hdDr_Id").val('');
    $("#txtDr_Name").val('');
    $("#txtDr_Department").val('');
    $("#txtDr_MobileNo").val('');
    $("#txtDr_Remark").val('');
    $("#btnDrSave").html("Save");
    $("#modelDoctor").modal('hide');


}
//End Doctor
//Start With Customer Detail

//Start Operator Directory

function GetRole() {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/Admin/GetAllRole',
        crossDomain: true,
        success: function (result) {
            //console.log(result);
            if (result != null) {
                var data = '<option value="">select Role</option>';
                for (var i = 0; i < result.length; i++) {
                    data += '<option value="' + result[i].roleId + '">' + result[i].role + '</option>'
                }
                $("#ddlOpr_Role").append(data);
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}


function BindTableOperator() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/DataDirectory/GetAllOperator",
        method: 'get',
        success: OnsuccessOperator
    });
}

function OnsuccessOperator(response) {
    $('#tblOperator').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblOperator").css("display", "block");
        $("#divOperatorNoRecordFound").empty();
        $('#tblOperator').DataTable({
            //scrollX: true, 
            //scroll: true, // used for fixed header
            //scrollY: 300, // used for fixed Hieght

            // height: '100px',
            //scrollCollapse: true, // used for fixed header
            //bProcessing: true,
            //bLenghtChange: true,
            //retrieve: true,
            //bfilter: true,
            //bSort: true,
            //bPaging: false,
            data: response,
            columnDefs: [{ type: 'hidden', 'targets': [3] }],
            order: [[3, 'desc']],
            columns: [
                { data: 'oprName', title: 'Name' },
                { data: 'oprEmail', title: 'Email' },
                { data: 'oprMobileNo', title: 'Mobile Number' },
                {
                    data: "oprId", title: 'Action',
                    render: function (data, type, row, meta) {
                        return '<div style="display: inline-flex;"><input type="hidden" value="' + row.oprId + '"><a class="UpdateOperator" href="#" style="color:#ffcd39;" onclick = modelResetPasswordOperator(' + row.oprId + ')  data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Reset Password"><i class="bi bi-bootstrap-reboot"></i></a> &nbsp;<a type="button" style="color:#656FEA;" class="" href="#" onclick = UpdateShowOperator(' + row.oprId + ')  data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Operator Detail"> <i class="bi bi-pencil-fill"></i> </a> &nbsp;<a type="button" class="" href="#" style="color:red" onclick="DeleteConfirmationOperator(' + row.oprId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Operator Detail"><i class="bi bi-trash"></i></a></div>';
                    }
                },
            ],
      
            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblOperator").css("display", "none");
        $("#divOperatorNoRecordFound").empty();
        $("#divOperatorNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');

    }
    $("#preloader").css("display", "none");

}

$(document).ready(function () {
    $("#fleOpr_Image").change(function (event) {
        var files = event.target.files;
        if (files[0].size <= 2000000) {
            $(".divUserImage").css("display", "flex");
            $("#ImgOprImage").attr("src", window.URL.createObjectURL(files[0]));
        }
        else {
            $(".divUserImage").css("display", "none");
            $("#ImgOprImage").attr("src", '');
            Message("error", "Max up to 2MB.");

        }
    })
})
function SaveOperator() {
    var returnValue = true;
    if ($("#ddlOpr_Role").val() == "") {
        $("#ddlOpr_Role").focus();
        Message("error", "Role  is required");
        returnValue = false;
    }
    else if ($("#txtOpr_Name").val() == "") {
        $("#txtOpr_Name").focus();
        Message("error", "Name is required");
        returnValue = false;
    }
    else if ($("#txtOpr_Email").val() == "") {
        $("#txtOpr_Email").focus();
        Message("error", "Email is required");
        returnValue = false;
    }
    else if (!IsEmail($("#txtOpr_Email").val())) {
        $("#txtOpr_Email").focus();
        Message("error", "Invalid email address");
        returnValue = false;
    }
    else if ($("#txtOpr_MobileNo").val() == "") {
        $("#txtOpr_MobileNo").focus();
        Message("error", "Mobile is required");
        returnValue = false;
    }
    else if ($("#txtOpr_LoginId").val() == "") {
        $("#txtOpr_LoginId").focus();
        Message("error", "Login Id is required");
        returnValue = false;
    }
    else if ($("#txtOpr_LoginId").val().length < 5) {
        $("#txtOpr_LoginId").focus();
        Message("error", "Login Id minimum 5 charactor is required");
        returnValue = false;
    }
    else if ($("#hdOpr_Id").val() == "" && $("#txtOpr_NewPassword").val() == "") {
        $("#txtOpr_NewPassword").focus();
        Message("error", "New Password is required");
        returnValue = false;
    }
    else if ($("#hdOpr_Id").val() == "" && $("#txtOpr_ConfirmPassword").val() == "") {
        $("#txtOpr_ConfirmPassword").focus();
        Message("error", "Confirm Password is required");
        returnValue = false;
    }
    else if ($("#hdOpr_Id").val() == "" && $("#txtOpr_NewPassword").val() != $("#txtOpr_ConfirmPassword").val()) {
        $("#txtOpr_ConfirmPassword").focus();
        Message("error", "Invalid your Confirm Password");
        returnValue = false;
    }

    if (returnValue != true) {

    }
    else {
        $("#preloader").css("display", "block");
        var formData = new FormData();

 
        var files = $("#fleOpr_Image").prop("files");
        for (var i = 0; i < files.length; i++) {
            formData.append('formFile', files[i]); // myFile is the input type="file" control
        }
        var OperatorObj = {
            oprId: $("#hdOpr_Id").val() == "" ? '0' : $("#hdOpr_Id").val(),
            oprRoleId: $("#ddlOpr_Role").val(),
            oprName: $("#txtOpr_Name").val(),
            oprEmail: $("#txtOpr_Email").val(),
            oprMobileNo: $("#txtOpr_MobileNo").val(),
            oprLoginId: $("#txtOpr_LoginId").val(),
            oprPassword: $("#txtOpr_NewPassword").val(),

        }
    
        formData.append('OperatorObj', JSON.stringify(OperatorObj));

        //formData.append("OprId", $("#hdOpr_Id").val());
        //formData.append("OprRoleId", $("#ddlOpr_Role").val());
        //formData.append("OprName", $("#txtOpr_Name").val());
        //formData.append("OprEmail", $("#txtOpr_Email").val());
        //formData.append("OprMobileNo", $("#txtOpr_MobileNo").val());
        //formData.append("OprLoginId", $("#txtOpr_LoginId").val());
        //formData.append("OprPassword", $("#txtOpr_NewPassword").val());

        $.ajax({
            url: '/DataDirectory/SaveOperator',
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (result) {
                if (result) {
                    if ($("#hdDr_Id").val() > 0) {
                        Message("success", "Updated Data Successfully");
                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    if ($("#hdUserProfileId").val() == $("#hdOpr_Id").val()) {
                        location.reload(true);
                    }
                    ClearOperator();
                    BindTableOperator()
                }
                else {

                    Message("error", $("#txtOpr_LoginId").val() + " this login id is already exist");
                }
                $("#preloader").css("display", "none");

            },
            error: function (jqXHR) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            },
            complete: function (jqXHR, status) {
                $("#preloader").css("display", "none");

            }
        });

  
        //$.ajax({
        //    method: "POST",
        //    url: '/DataDirectory/SaveOperator',
        //    data: obj,
        //    crossDomain: true,
        //    success: function (result) {
        //        if (result) {
        //            if ($("#hdDr_Id").val() > 0) {
        //                Message("success", "Updated Data Successfully");
        //            }
        //            else {
        //                Message("success", "Saved Data Successfully");
        //            }
        //            ClearOperator();
        //            BindTableOperator()
        //        }
        //        else {

        //            Message("error", $("#txtOpr_LoginId").val() + " this login id is already exist");
        //        }
        //    },
        //    error: function (e) {
        //        Message("warning", "Somthing went wrong");

        //    }
        //});
    }
}

function UpdateShowOperator(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/DataDirectory/GetByIdOperator',
        data: { Id: Id },
        crossDomain: true,
        success: function (data) {
            if (data != null) {
                var result = data.obj;
                $("#hdOpr_Id").val(result.oprId);
                $("#ddlOpr_Role").val(result.oprRoleId);
                $("#txtOpr_Name").val(result.oprName);
                $("#txtOpr_Email").val(result.oprEmail);
                $("#txtOpr_MobileNo").val(result.oprMobileNo);
                $("#txtOpr_LoginId").val(result.oprLoginId);
                $("#txtOpr_NewPassword").val(result.oprPassword);

                $("#divNewPassword").css("display", "none");
                $("#divConfirmPassword").css("display", "none");
                if (result.oprImage != null && result.oprImage != "") {
                    $(".divUserImage").css("display", "flex");
                    $("#ImgOprImage").attr("src", "data:image/jpg;base64," + result.oprImage + "");
                }
                else {
                    $(".divUserImage").css("display", "flex");

                    $("#ImgOprImage").attr("src", "/img/defaultuser.png");
                }
                $("#btnOprSave").html("Update");
                // $("#lblDrTitle").html("Update Doctor");

                $("#modelOperator").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function DeleteConfirmationOperator(Id) {
    $("#modelDeleteConfirmationOperator").modal('show');
    $("#hdDeleteOperatorId").val(Id);
}

function DeleteOperator() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteOperatorId").val();
    $("#modelDeleteConfirmationOperator").modal('hide');
    $("#hdDeleteOperatorId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/DataDirectory/DeleteOperator',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableOperator();
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function modelResetPasswordOperator(Id) {
    $("#modelResetPasswordOperator").modal('show');
    $("#hdResetpwdOpr_Id").val(Id);
}

function ResetPasswordOperator() {
    var returnValue = true;
   if ($("#txtResetpwdOpr_NewPassword").val() == "") {
        $("#txtResetpwdOpr_NewPassword").focus();
        Message("error", "New Password  is required");
        returnValue = false;
    }
    else if ($("#txtResetpwdOpr_ConfirmPassword").val() == "") {
        $("#txtResetpwdOpr_ConfirmPassword").focus();
        Message("error", "Confirm Password  is required");
        returnValue = false;
    }
    else if ($("#txtResetpwdOpr_ConfirmPassword").val() != $("#txtResetpwdOpr_NewPassword").val()) {
        $("#txtResetpwdOpr_ConfirmPassword").focus();
        Message("error", "Invalid Confirm Password");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        $.ajax({
            method: "POST",
            url: '/DataDirectory/ResetPasswordOperator',
            data: { Id: $("#hdResetpwdOpr_Id").val(), NewPassword: $("#txtResetpwdOpr_NewPassword").val() },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Reset password changed Successfully");
                    $("#hdResetpwdOpr_Id").val('');
                    $("#txtResetpwdOpr_NewPassword").val('');
                    $("#txtResetpwdOpr_ConfirmPassword").val('');
                    $("#modelResetPasswordOperator").modal('hide');
                    $("#btnOprSave").html("Save");
                    BindTableOperator()
                }
                else {

                    Message("error", "Old password is invalid");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }

}

function ClearOperator() {
    $("#hdOpr_Id").val('');
    $("#ddlOpr_Role").val('');
    $("#txtOpr_Name").val('');
    $("#txtOpr_Email").val('');
    $("#txtOpr_MobileNo").val('');
    $("#txtOpr_LoginId").val('');
    $("#txtOpr_NewPassword").val('');
    $("#ImgOprImage").attr("src", "");
    $("#divNewPassword").css("display", "block");
    $("#divConfirmPassword").css("display", "block");
    $(".divUserImage").css("display", "none");

    $("#modelOperator").modal('hide');
    $("#btnOprSave").html("Save");
}
//End Operator Directory


//Start With Data Directory
//Start Sample Type
function BindTableSampleType() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/DataDirectory/GetAllSampleType",
        method: 'get',
        success: OnsuccessSample
    });
}

function OnsuccessSample(response) {
    $('#tblSampleType').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblSample").css("display", "block");
        $("#divSampleNoRecordFound").empty();
        $('#tblSampleType').DataTable({

            data: response,
            columns: [
                //   { data: 'smpId', title: 'emp_no' },
                {
                    data: "smpType", title: 'Sample Type', width:"95%",
                    render: function (data, type, row, meta) {
                        return row.smpType
                    }
                },
                {
                    data: "smpId", title: 'Action', width:"5%",
                    render: function (data, type, row, meta) {
                        return '<a type="button" style="color:#656FEA;" class="" href="#" onclick = UpdateShowSamleType(' + row.smpId + ') data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Sample Type"> <i class="bi bi-pencil-fill"></i> </a> &nbsp;<a type="button" class="" href="#" style="color:red"   onclick="DeleteConfirmationSampleType(' + row.smpId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Sample Type"><i class="bi bi-trash"></i></a>';
                    }
                },
            ],
            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblSample").css("display", "none");
        $("#divSampleNoRecordFound").empty();
        $("#divSampleNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
    $("#preloader").css("display", "none");

}

function SaveSampleType() {
    var returnValue = true;
    if ($("#txtSMP_Type").val() == "") {
        $("#txtSMP_Type").focus();
        Message("error", "Sample Type is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            SmpId: $("#hdSMP_Id").val(),
            SmpType: $("#txtSMP_Type").val(),
        }
        $.ajax({
            method: "POST",
            url: '/DataDirectory/SaveSampleType',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result > 0) {
                    if ($("#hdSMP_Id").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearSampleType();
                    BindTableSampleType()
                }
                else {
                    Message("error", $("#txtSMP_Type").val() +" sample type already exist");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function UpdateShowSamleType(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/DataDirectory/GetByIdSampleType',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {
            if (result != null) {
                $("#hdSMP_Id").val(result[0].smpId);
                $("#txtSMP_Type").val(result[0].smpType);
                $("#btnSave1").html("Update");
                $("#lblTitle1").html("Update Sample Type");

                $("#modelSampleType").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationSampleType(Id) {
    $("#modelDeleteConfirmationSampleType").modal('show');
    $("#hdDeleteSampleTypeId").val(Id);
}

function DeleteSampleType() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteSampleTypeId").val();
    $("#modelDeleteConfirmationSampleType").modal('hide');
    $("#hdDeleteSampleTypeId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/DataDirectory/DeleteSampleType',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableSampleType();
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearSampleType() {
    $("#hdSMP_Id").val('');
    $("#txtSMP_Type").val('');
    $("#modelSampleType").modal('hide');
    $("#btnSave1").html("Save");
    $("#lblTitle1").html("New Sample Type");
}
// End Sample Type

//Start Unit

function BindTableUnitType() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/DataDirectory/GetAllUnit",
        method: 'get',
        success: OnsuccessUnitType
    });
}

function OnsuccessUnitType(response) {
    $('#tblUnit').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblUnit").css("display", "block");
        $("#divUnitNoRecordFound").empty();
        $('#tblUnit').DataTable({
            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 900, // used for fixed header
            //scrollCollapse: true, // used for fixed header
            //destroy: true,
            //retrieve: true,
            //paging: false,

            data: response,
            columns: [
                //  { data: 'untId', title: 'S no' },

                {
                    data: "untName", title: 'Result Unit', width:"95%",
                    render: function (data, type, row, meta) {
                        return row.untName
                    }
                },
                {
                    data: "untId", title: 'Action', width:"5%",
                    render: function (data, type, row, meta) {
                        return '<a type="button" style="color:#656FEA;" class="" href="#" onclick = UpdateShowUnitType(' + row.untId + ') data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Result Unit"> <i class="bi bi-pencil-fill"></i> </a> &nbsp;<a type="button" class="" href="#" style="color:red"   onclick="DeleteConfirmationUnitType(' + row.untId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Result Unit"><i class="bi bi-trash"></i></a>';
                        //return '<button type="button" class="btn btn-primary" onclick = UpdateShowUnitType(' + row.untId + ') > Update </button> &nbsp;<button type="button" class="btn btn-danger" onclick="DeleteConfirmationUnitType(' + row.untId + ')">Delete</button>';
                    }
                },
            ],
            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblUnit").css("display", "none");
        $("#divUnitNoRecordFound").empty();
        $("#divUnitNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
    $("#preloader").css("display", "none");

}

function SaveUnitType() {
    var returnValue = true;
    if ($("#txtUnt_Name").val() == "") {
        $("#txtUnt_Name").focus();
        Message("error", "Unit Name is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            UntId: $("#hdUnt_Id").val(),
            UntName: $("#txtUnt_Name").val(),
        }
        $.ajax({
            method: "POST",
            url: '/DataDirectory/SaveUnit',
            data: obj,
            success: function (result) {
                if (result) {
                    if ($("#hdUnt_Id").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearUnitType();
                    BindTableUnitType()
                }
                else {

                    Message("error", $("#txtUnt_Name").val() +" this unit name already exist");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function UpdateShowUnitType(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/DataDirectory/GetByIdUnit',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {
            if (result != null) {
                $("#hdUnt_Id").val(result[0].untId);
                $("#txtUnt_Name").val(result[0].untName);
                $("#btnSave2").html("Update");
                $("#lblTitle2").html("Update Unit");

                $("#modelUnitType").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationUnitType(Id) {
    $("#modelDeleteConfirmationUnitType").modal('show');
    $("#hdDeleteUnitTypeId").val(Id);
}

function DeleteUnitType() {
    $("#preloader").css("display", "block");
    var Id = $("#hdDeleteId").val();
    $("#modelDeleteConfirmationUnitType").modal('hide');
    $("#hdDeleteUnitTypeId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/DataDirectory/DeleteUnit',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableUnitType();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearUnitType() {
    $("#hdUnt_Id").val('');
    $("#txtUnt_Name").val('');
    $("#modelUnitType").modal('hide');
    $("#btnSave2").html("Save");
    $("#lblTitle2").html("New Unit");
}
//End Unit

//Start Qualitative

function BindTableQualitative() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/DataDirectory/GetAllQualitative",
        method: 'get',
        success: OnsuccessQualitative
    });
}

function OnsuccessQualitative(response) {
    $('#tblQualitative').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblQualitative").css("display", "block");
        $("#divQualitativeNoRecordFound").empty();
        $('#tblQualitative').DataTable({
            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 900, // used for fixed header
            //scrollCollapse: true, // used for fixed header
            //destroy: true,
            //retrieve: true,
            //paging: false,
            data: response,
            columns: [
                //   { data: 'qualtId', title: 'S no' },

                {
                    data: "qualtName", title: 'Qualitative Result', width:"95%",
                    render: function (data, type, row, meta) {
                        return row.qualtName
                    }
                },
                {
                    data: "qualtId", title: 'Action', width:"5%",
                    render: function (data, type, row, meta) {
                        return '<a type="button" style="color:#656FEA;" class="" href="#" onclick = UpdateShowQualitative(' + row.qualtId + ')  data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Qualitative Result"> <i class="bi bi-pencil-fill"></i> </a> &nbsp;<a type="button" class="" href="#" style="color:red"   onclick="DeleteConfirmationQualitative(' + row.qualtId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Qualitative Result"><i class="bi bi-trash"></i></a>';
                        //return '<button type="button" class="btn btn-primary" onclick = UpdateShowQualitative(' + row.qualtId + ') > Update </button> &nbsp;<button type="button" class="btn btn-danger" onclick="DeleteConfirmationQualitative(' + row.qualtId + ')">Delete</button>';
                    }
                },
            ],
            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblQualitative").css("display", "none");
        $("#divQualitativeNoRecordFound").empty();
        $("#divQualitativeNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
    $("#preloader").css("display", "none");

}

function SaveQualitative() {
    var returnValue = true;
    if ($("#txtQualtName").val() == "") {
        $("#txtQualtName").focus();
        Message("error", "Qualitative Name is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            QualtId: $("#hdQualtId").val(),
            QualtName: $("#txtQualtName").val(),
        }
        $.ajax({
            method: "POST",
            url: '/DataDirectory/SaveQualitative',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result > 0) {
                    if ($("#hdQualtId").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearQualitative();
                    BindTableQualitative()
                }
                else {

                    Message("error", $("#txtQualtName").val() +" Qualitative name already exist");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function UpdateShowQualitative(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/DataDirectory/GetByIdQualitative',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {
            if (result != null) {
                $("#hdQualtId").val(result[0].qualtId);
                $("#txtQualtName").val(result[0].qualtName);
                $("#btnSave3").html("Update");
                $("#lblTitle3").html("Update Qualitative");

                $("#modelQualitative").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationQualitative(Id) {
    $("#modelDeleteConfirmationQualitative").modal('show');
    $("#hdDeleteQualitativeId").val(Id);
}

function DeleteQualitative() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteQualitativeId").val();
    $("#modelDeleteConfirmationQualitative").modal('hide');
    $("#hdDeleteQualitativeId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/DataDirectory/DeleteQualitative',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableQualitative();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function ClearQualitative() {
    $("#hdQualtId").val('');
    $("#txtQualtName").val('');
    $("#modelQualitative").modal('hide');
    $("#btnSave3").html("Save");
    $("#lblTitle3").html("New Qualitative");
}
//End Qualitative

//Start Result

function BindTableResult() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/DataDirectory/GetAllResult",
        method: 'get',
        success: OnsuccessResult
    });
}

function OnsuccessResult(response) {
    $('#tblResult').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblResult").css("display", "block");
        $("#divResultNoRecordFound").empty();
        $('#tblResult').DataTable({
            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 900, // used for fixed header
            //scrollCollapse: true, // used for fixed header
            //destroy: true,
            //retrieve: true,
            //paging: false,
            data: response,
            columns: [
                // { data: 'rsultId', title: 'S no' },

                {
                    data: "rsultName", title: 'Result Type', width:"95%",
                    render: function (data, type, row, meta) {
                        return row.rsultName
                    }
                },
                {
                    data: "rsultId", title: 'Action', width:"5%",
                    render: function (data, type, row, meta) {
                        return '<a type="button" style="color:#656FEA;" class="" href="#" onclick = UpdateShowResult(' + row.rsultId + ')  data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Result Type"> <i class="bi bi-pencil-fill"></i> </a> &nbsp;<a type="button" class="" href="#" style="color:red"   onclick="DeleteConfirmationResult(' + row.rsultId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Result Type"><i class="bi bi-trash"></i></a>';
                        //return '<button type="button" class="btn btn-primary" onclick = UpdateShowResult(' + row.rsultId + ') > Update </button> &nbsp;<button type="button" class="btn btn-danger" onclick="DeleteConfirmationResult(' + row.rsultId + ')">Delete</button>';
                    }
                },
            ],
            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblResult").css("display", "none");
        $("#divResultNoRecordFound").empty();
        $("#divResultNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
    $("#preloader").css("display", "none");

}

function SaveResult() {
    var returnValue = true;
    if ($("#txtRsltName").val() == "") {
        $("#txtRsltName").focus();
        Message("error", "Result Name is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            RsultId: $("#hdRsltId").val(),
            RsultName: $("#txtRsltName").val(),
        }
        $.ajax({
            method: "POST",
            url: '/DataDirectory/SaveResult',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result > 0) {
                    if ($("#hdRsltId").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearResult();
                    BindTableResult()
                }
                else {

                    Message("error", $("#txtRsltName").val() +"Result name already exist");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function UpdateShowResult(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/DataDirectory/GetByIdResult',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {
            if (result != null) {
                $("#hdRsltId").val(result[0].rsultId);
                $("#txtRsltName").val(result[0].rsultName);
                $("#btnSave4").html("Update");
                $("#lblTitle4").html("Update Result");

                $("#modelResult").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationResult(Id) {
    $("#modelDeleteConfirmationResult").modal('show');
    $("#hdDeleteResultId").val(Id);
}

function DeleteResult() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteResultId").val();
    $("#modelDeleteConfirmationResult").modal('hide');
    $("#hdDeleteResultId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/DataDirectory/DeleteResult',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableResult();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function ClearResult() {
    $("#hdRsltId").val('');
    $("#txtRsltName").val('');
    $("#modelResult").modal('hide');
    $("#btnSave4").html("Save");
    $("#lblTitle4").html("New Result");
}
//End Result

//Start Clinical

function BindTableClinical() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/DataDirectory/GetAllClinical",
        method: 'get',
        success: OnsuccessClinical
    });
}

function OnsuccessClinical(response) {
    $('#tblClinical').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblClinical").css("display", "block");
        $("#divClinicalNoRecordFound").empty();
        $('#tblClinical').DataTable({
            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 900, // used for fixed header
            //scrollCollapse: true, // used for fixed header
            //destroy: true,
            //retrieve: true,
            //paging: false,
            data: response,
            columns: [
                // { data: 'clnId', title: 'S no' },

                {
                    data: "clnName", title: 'Clinical Type', width:"95%",
                    render: function (data, type, row, meta) {
                        return row.clnName
                    }
                },
                {
                    data: "clnId", title: 'Action', width:"5%",
                    render: function (data, type, row, meta) {
                        return '<a type="button" style="color:#656FEA;" class="" href="#" onclick = UpdateShowClinical(' + row.clnId + ') data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Clinical Type"> <i class="bi bi-pencil-fill"></i> </a> &nbsp;<a type="button" class="" href="#" style="color:red"   onclick="DeleteConfirmationClinical(' + row.clnId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Clinical Type"><i class="bi bi-trash"></i></a>';
                        //return '<button type="button" class="btn btn-primary" onclick = UpdateShowClinical(' + row.clnId + ') > Update </button> &nbsp;<button type="button" class="btn btn-danger" onclick="DeleteConfirmationClinical(' + row.clnId + ')">Delete</button>';
                    }
                },
            ],

            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblClinical").css("display", "none");
        $("#divClinicalNoRecordFound").empty();
        $("#divClinicalNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
    $("#preloader").css("display", "none");

}

function SaveClinical() {
    var returnValue = true;
    if ($("#txtClnName").val() == "") {
        $("#txtClnName").focus();
        Message("error", "Clinical Name is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            ClnId: $("#hdClnId").val(),
            ClnName: $("#txtClnName").val(),
        }
        $.ajax({
            method: "POST",
            url: '/DataDirectory/SaveClinical',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result > 0) {
                    if ($("#hdCLnId").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearClinical();
                    BindTableClinical();
                }
                else {

                    Message("error", $("#txtClnName").val() +" clinical name already exist");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function UpdateShowClinical(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/DataDirectory/GetByIdClinical',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {
            if (result != null) {
                $("#hdClnId").val(result[0].clnId);
                $("#txtClnName").val(result[0].clnName);
                $("#btnSave5").html("Update");
                $("#lblTitle5").html("Update Clinical");

                $("#modelClinical").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationClinical(Id) {
    $("#modelDeleteConfirmationClinical").modal('show');
    $("#hdDeleteClinicalId").val(Id);
}

function DeleteClinical() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteClinicalId").val();
    $("#modelDeleteConfirmationClinical").modal('hide');
    $("#hdDeleteClinicalId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/DataDirectory/DeleteClinical',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableClinical();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function ClearClinical() {
    $("#hdClnId").val('');
    $("#txtCLnName").val('');
    $("#modelClinical").modal('hide');
    $("#btnSave5").html("Save");
    $("#lblTitle5").html("New Clinical");
}
//End Clinical


//Start Sample Container

function BindTableSampleContainer() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/DataDirectory/GetAllSampleContainer",
        method: 'get',
        success: OnsuccessSampleContainer
    });
}

function OnsuccessSampleContainer(response) {
    $('#tblSampleContainer').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblSampleContainer").css("display", "block");
        $("#divSampleContainerNoRecordFound").empty();
        $('#tblSampleContainer').DataTable({
            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 900, // used for fixed header
            //scrollCollapse: true, // used for fixed header
            //destroy: true,
            //retrieve: true,
            //paging: false,
            data: response,
            columns: [
                //  { data: 'smpConId', title: 'S no' },

                {
                    data: "smpConName", title: 'Sample Container Name', width:"95%",
                    render: function (data, type, row, meta) {
                        return row.smpConName
                    }
                },
                {
                    data: "smpConId", title: 'Action', width:"5%",
                    render: function (data, type, row, meta) {
                        //return '<button type="button" class="btn btn-primary" onclick = UpdateShowSampleContainer(' + row.smpConId + ') > Update </button> &nbsp;<button type="button" class="btn btn-danger" onclick="DeleteConfirmationSampleContainer(' + row.smpConId + ')">Delete</button>';
                        return '<a type="button" style="color:#656FEA;" class="" href="#" onclick = UpdateShowSampleContainer(' + row.smpConId + ') data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Sample Container"> <i class="bi bi-pencil-fill"></i> </a> &nbsp;<a type="button" class="" href="#" style="color:red"   onclick="DeleteConfirmationSampleContainer(' + row.smpConId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Sample Container"><i class="bi bi-trash"></i></a>';
                    }
                },
            ],


            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },

        });
    }
    else {
        $("#divtblSampleContainer").css("display", "none");
        $("#divSampleContainerNoRecordFound").empty();
        $("#divSampleContainerNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
    $("#preloader").css("display", "none");
}

function SaveSampleContainer() {
    var returnValue = true;
    if ($("#txtSmpCon_Name").val() == "") {
        $("#txtSmpCon_Name").focus();
        Message("error", "Sample Container Name is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            SmpConId: $("#hdSmpCon_Id").val(),
            SmpConName: $("#txtSmpCon_Name").val(),
        }
        $.ajax({
            method: "POST",
            url: '/DataDirectory/SaveSampleContainer',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result) {
                    if ($("#hdSmpCon_Id").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearSampleContainer();
                    BindTableSampleContainer()
                }
                else {

                    Message("error", $("#txtSmpCon_Name").val() + " this container name already exist");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function UpdateShowSampleContainer(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/DataDirectory/GetByIdSampleContainer',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {
            if (result != null) {
                $("#hdSmpCon_Id").val(result[0].smpConId);
                $("#txtSmpCon_Name").val(result[0].smpConName);
                $("#btnSave6").html("Update");
                $("#lblTitle6").html("Update Sample Container");

                $("#modelSampleContainer").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");
        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");
        }
    });
}

function DeleteConfirmationSampleContainer(Id) {
    $("#modelDeleteConfirmationSampleContainer").modal('show');
    $("#hdDeleteSampleContainerId").val(Id);
}

function DeleteSampleContainer() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteSampleContainerId").val();
    $("#modelDeleteConfirmationSampleContainer").modal('hide');
    $("#hdDeleteSampleContainerId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/DataDirectory/DeleteSampleContainer',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableSampleContainer();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearSampleContainer() {
    $("#hdSmpCon_Id").val('');
    $("#txtSmpCon_Name").val('');
    $("#modelSampleContainer").modal('hide');
    $("#btnSave6").html("Save");
    $("#lblTitle6").html("New Sample Container");
}
//End Sample Container

//Start Reagent Bottle Typle

function BindTableReagentBottleType() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/DataDirectory/GetAllReagentBottleType",
        method: 'get',
        success: OnsuccessReagentBottleType
    });
}

function OnsuccessReagentBottleType(response) {
    $('#tblReagentBottleType').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblBottleType").css("display", "block");
        $("#divBottleTypeNoRecordFound").empty();
        $('#tblReagentBottleType').DataTable({
            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 900, // used for fixed header
            //scrollCollapse: true, // used for fixed header
            //destroy: true,
            //retrieve: true,
            //paging: false,
            data: response,
            columns: [
                //  { data: 'rgntBtlId', title: 'S no' },

                {
                    data: "rgntBtlSize", title: 'Bottle Size (ml)', width:"95%",
                    render: function (data, type, row, meta) {
                        return row.rgntBtlSize
                    }
                },
                {
                    data: "rgntBtlId", title: 'Action', width:"5%",
                    render: function (data, type, row, meta) {
                        return '<a type="button" style="color:#656FEA;" class="" href="#" onclick = UpdateShowReagentBottleType(' + row.rgntBtlId + ') data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Reagent Bottle Type"> <i class="bi bi-pencil-fill"></i> </a> &nbsp;<a type="button" class="" href="#" style="color:red"   onclick="DeleteConfirmationReagentBottleType(' + row.rgntBtlId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Reagent Bottle Type"><i class="bi bi-trash"></i></a>';
                        //return '<button type="button" class="btn btn-primary" onclick = UpdateShowReagentBottleType(' + row.rgntBtlId + ') > Update </button> &nbsp;<button type="button" class="btn btn-danger" onclick="DeleteConfirmationReagentBottleType(' + row.rgntBtlId + ')">Delete</button>';
                    }
                },
            ],


            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblBottleType").css("display", "none");
        $("#divBottleTypeNoRecordFound").empty();
        $("#divBottleTypeNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
    $("#preloader").css("display", "none");

}

function SaveReagentBottleType() {
    var returnValue = true;
    if ($("#txtRgntBtl_Size").val() == "") {
        $("#txtRgntBtl_Size").focus();
        Message("error", "Reagent Bottle Size is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            RgntBtlId: $("#hdRgntBtl_Id").val(),
            RgntBtlSize: $("#txtRgntBtl_Size").val(),
        }
        $.ajax({
            method: "POST",
            url: '/DataDirectory/SaveReagentBottleType',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result) {
                    if ($("#hdRgntBtl_Id").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearReagentBottleType();
                    BindTableReagentBottleType()
                }
                else {

                    Message("error", $("#txtRgntBtl_Size").val() + " this bottle size is already exist");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");
            }
        });
    }
}

function UpdateShowReagentBottleType(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/DataDirectory/GetByIdReagentBottleType',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {
            if (result != null) {
                $("#hdRgntBtl_Id").val(result[0].rgntBtlId);
                $("#txtRgntBtl_Size").val(result[0].rgntBtlSize);
                $("#btnSave7").html("Update");
                $("#lblTitle7").html("Update Reagent Bottle Type");

                $("#modelReagentBottleType").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationReagentBottleType(Id) {
    $("#modelDeleteConfirmationReagentBottleType").modal('show');
    $("#hdDeleteReagentBottleTypeId").val(Id);
}

function DeleteReagentBottleType() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteReagentBottleTypeId").val();
    $("#modelDeleteConfirmationReagentBottleType").modal('hide');
    $("#hdDeleteReagentBottleTypeId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/DataDirectory/DeleteReagentBottleType',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableReagentBottleType();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearReagentBottleType() {
    $("#hdRgntBtl_Id").val('');
    $("#txtRgntBtl_Size").val('');
    $("#modelReagentBottleType").modal('hide');
    $("#btnSave7").html("Save");
    $("#lblTitle7").html("New Reagent Bottle Type");
}
//End Reagent Bottle Typle


//Start Qc Material

function BindTableQcMaterial() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/DataDirectory/GetAllQcMaterial",
        method: 'get',
        success: OnsuccessQcMaterial
    });
}

function OnsuccessQcMaterial(response) {
    $('#tblQcMaterial').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblQCMetarial").css("display", "block");
        $("#divQcMetarialNoRecordFound").empty();
        $('#tblQcMaterial').DataTable({
            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 900, // used for fixed header
            //scrollCollapse: true, // used for fixed header
            //destroy: true,
            //retrieve: true,
            //paging: false,
            data: response,
            columns: [
                {
                    data: "qcMtrlName", title: 'QC Material Name', width:"95%",
                    render: function (data, type, row, meta) {
                        return row.qcMtrlName
                    }
                },
                {
                    data: "qcMtrlId", title: 'Action', width:"5%",
                    render: function (data, type, row, meta) {
                        return '<a type="button" style="color:#656FEA;" class="" href="#" onclick = UpdateShowQcMaterial(' + row.qcMtrlId + ') data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update QC Material"> <i class="bi bi-pencil-fill"></i> </a> &nbsp;<a type="button" class="" href="#" style="color:red"   onclick="DeleteConfirmationQcMaterial(' + row.qcMtrlId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete QC Material"><i class="bi bi-trash"></i></a>';
                    }
                },
            ],
            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblQCMetarial").css("display", "none");
        $("#divQcMetarialNoRecordFound").empty();
        $("#divQcMetarialNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
    $("#preloader").css("display", "none");

}

function SaveQcMaterial() {
    var returnValue = true;
    if ($("#txtQcMtrl_Name").val() == "") {
        $("#txtQcMtrl_Name").focus();
        Message("error", "Qc Material Name is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var obj = {
            QcMtrlId: $("#hdQcMtrl_Id").val(),
            QcMtrlName: $("#txtQcMtrl_Name").val(),
        }
        $.ajax({
            method: "POST",
            url: '/DataDirectory/SaveQcMaterial',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result > 0) {
                    if ($("#hdQcMtrl_Id").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearQcMaterial();
                    BindTableQcMaterial()
                }
                else {
                    Message("error", $("#txtQcMtrl_Name").val() +" Quality material name already exist");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function UpdateShowQcMaterial(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/DataDirectory/GetByIdQcMaterial',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {
            if (result != null) {
                $("#hdQcMtrl_Id").val(result[0].qcMtrlId);
                $("#txtQcMtrl_Name").val(result[0].qcMtrlName);
                $("#btnSave8").html("Update");
                $("#lblTitle8").html("Update Qc Material");

                $("#modelQcMaterial").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },

        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationQcMaterial(Id) {
    $("#modelDeleteConfirmationQcMaterial").modal('show');
    $("#hdDeleteQcMaterialId").val(Id);
}

function DeleteQcMaterial() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteQcMaterialId").val();
    $("#modelDeleteConfirmationQcMaterial").modal('hide');
    $("#hdDeleteQcMaterialId").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/DataDirectory/DeleteQcMaterial',
            data: { Id: Id },
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableQcMaterial();
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");
            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearQcMaterial() {
    $("#hdQcMtrl_Id").val('');
    $("#txtQcMtrl_Name").val('');
    $("#modelQcMaterial").modal('hide');
    $("#btnSave8").html("Save");
    $("#lblTitle8").html("New Qc Material");
}
//End Qc Material
//End With Data Directory


function IsEmail(email) {
  //  var emailReg = /^([\w-\.]+@@([\w-]+\.)+[\w-]{2,4})?$/;
    var emailReg = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
    return emailReg.test(email);
}

//#reagion "Manufacturer"


function SaveManufacturer() {
    var returnValue = true;
    if ($("#txtMnfName").val() == "") {
        $("#txtMnfName").focus();
        Message("error", "Manufacturer Name is required");
        returnValue = false;
    }
    else {
        $("#preloader").css("display", "block");

        var QCType = "false";
        var RegType = "false";
        var CaliType = "false";
        var mtype = "false";

        if (document.getElementById("chkQC").checked == true) {
            QCType = 'QC';
            mtype = 'QC';
        }
        if (document.getElementById("chkReg").checked ==true) {
            RegType = 'Rgt';
            mtype = 'Rgt';
        }
        if (document.getElementById("chkCali").checked ==true) {
            CaliType = 'Cali';
            mtype = 'Cali';
        }

        var obj = {
           
            //MnfId: $("#hdMnf_Id").val(),
            mnfId: $("#hdMnf_Id").val(),
            mnfName: $("#txtMnfName").val(),
            MnfReg: RegType,
            MnfQC: QCType,
            MnfCali: CaliType,
        }
        
        $.ajax({
            method: "POST",
            url: '/DataDirectory/SaveManufacturer',
            data: obj,
            crossDomain: true,
            success: function (result) {
                if (result > 0) {
                    if ($("#hdSMP_Id").val() > 0) {
                        Message("success", "Updated Data Successfully");

                    }
                    else {
                        Message("success", "Saved Data Successfully");
                    }
                    ClearManufacturer();
                    BindTableManufacturer()
                }
                else {

                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function BindTableManufacturer() {
    $("#preloader").css("display", "block");

    $.ajax({
        url: "/DataDirectory/GetAllManufacturer",
        method: 'get',
        success: OnsuccessManufacturer
    });
}

function OnsuccessManufacturer(response) {
    $('#tblManufacturer').DataTable().destroy();
    if (response.length > 0) {
        $("#divtblManufacturer").css("display", "block");
        $("#divQcManufacturerNoRecordFound").empty();
        $('#tblManufacturer').DataTable({

            //scrollX: true,
            //scroll: true, // used for fixed header
            //scrollY: 900, // used for fixed header
            //scrollCollapse: true, // used for fixed header
            //destroy: true,
            //retrieve: true,
            //paging: false,
            data: response,


            columns: [

                {
                    data: "mnfName", title: 'Manufacturer',
                    render: function (data, type, row, meta) {
                        return row.mnfName
                    }
                },


                {
                    data: "mnfCali", title: 'Calibration',
                    render: function (data, type, row, meta) {

                        if (row.mnfCali == 'Cali') {
                            return '<div style="display: flex;text-align:center;"><a class="" href="#" style="color:blue;" ><b><i class="bi-check-square"></i></b></a></div>';
                        }
                        else {
                            return "";
                        }
                    }
                },
                {
                    data: "mnfReg", title: ' Reagent ',
                    render: function (data, type, row, meta) {

                        if (row.mnfReg == 'Rgt') {
                            return '<div style="display: flex;text-align:center;"><a class="" href="#" style="color:blue;" ><b><i class="bi-check-square"></i></b></a></div>';
                        }
                        else {
                            return "";
                        }
                    }
                },
                {
                    data: "mnfQc", title: ' QC ',
                    render: function (data, type, row, meta) {

                        if (row.mnfQc == 'QC') {
                            return '<div style="display: flex;text-align:center;"><a class="" href="#" style="color:blue;" ><b><i class="bi-check-square"></i></b></a></div>';
                        }
                        else {
                            return "";
                        }
                    }
                },



                {
                    data: "mnfId", title: 'Action',
                    //data: "", title: 'Action',
                    render: function (data, type, row, meta) {
                        return '<a type="button" style="color:#656FEA;" class="" href="#"  onclick = UpdateShowManufacturer(' + row.mnfId + ') data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Update Manufacturer"><i class="bi bi-pencil-fill"></i> </a> &nbsp;<a type="button" class="" href="#" style="color:red"  onclick="DeleteConfirmationManufacturer(' + row.mnfId + ')" data-bs-toggle="tooltip" data-bs-html="true" data-bs-original-title="Delete Menufacturer"><i class="bi bi-trash"></i></a>';
                        //'<button type="button" class="btn btn-primary" onclick = UpdateShowManufacturer(' + row.mnfId + ') > <b><i class="bi bi-pencil-fill"></i></b> </button> &nbsp;<button type="button" class="btn btn-danger" onclick="DeleteConfirmationManufacturer(' + row.mnfId + ')"><b><i class="bi bi-trash"></i></b></button>';
                    }
                },
            ],
            "drawCallback": function (settings) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            },
        });
    }
    else {
        $("#divtblManufacturer").css("display", "none");
        $("#divQcManufacturerNoRecordFound").empty();
        $("#divQcManufacturerNoRecordFound").append('<img src="/img/norecordfound.gif" class="center" style="width:40%; margin-left:30%"/>');
    }
    $("#preloader").css("display", "none");
}

function UpdateShowManufacturer(Id) {
    $("#preloader").css("display", "block");

    $.ajax({
        method: "GET",
        url: '/DataDirectory/GetByIdManufacturer',
        data: { Id: Id },
        crossDomain: true,
        success: function (result) {
            if (result != null) {

                if (result[0].mnfQC == "QC") {
                    document.getElementById("chkQC").checked = true;
                }
                else {
                    document.getElementById("chkQC").checked = false;
                }
                if (result[0].mnfReg == "Rgt") {
                    document.getElementById("chkReg").checked = true;
                }
                else {
                    document.getElementById("chkReg").checked = false;
                }
                if (result[0].mnfCali == "Cali") {
                    document.getElementById("chkCali").checked = true;
                }
                else {
                    document.getElementById("chkCali").checked = false;
                }

                $("#hdMnf_Id").val(result[0].mnfId);
                $("#txtMnfName").val(result[0].mnfName);               

                $("#btnSave1").html("Update");
                $("#lblTitle1").html("Update Manufacturer Type");

                $("#modelManufacturer").modal('show');
            }
            else {

                Message("error", "Somthing went wrong");
            }
            $("#preloader").css("display", "none");

        },
        error: function (e) {
            Message("warning", "Somthing went wrong");
            $("#preloader").css("display", "none");

        }
    });
}

function DeleteConfirmationManufacturer(Id) {
    $("#modelDeleteConfirmationManufacturer").modal('show');
    $("#hdDeleteManufacturer").val(Id);
    //modelDeleteConfirmationManufacturer
}

function DeleteManufacturer() {
    $("#preloader").css("display", "block");

    var Id = $("#hdDeleteManufacturer").val();
    $("#modelDeleteConfirmationManufacturer").modal('hide');
    $("#hdDeleteManufacturer").val('');

    if (Id > 0) {
        $.ajax({
            method: "GET",
            url: '/DataDirectory/DeleteManufacturer',
            data: { Id: Id },
            crossDomain: true,
            crossDomain: true,
            success: function (result) {
                if (result) {
                    Message("success", "Deleted Data Successfully");
                    BindTableManufacturer();
                }
                else {
                    Message("error", "Somthing went wrong");
                }
                $("#preloader").css("display", "none");

            },
            error: function (e) {
                Message("warning", "Somthing went wrong");
                $("#preloader").css("display", "none");

            }
        });
    }
}

function ClearManufacturer() {
    $("#hdMnf_Id").val('');
    $("#txtMnfName").val('');
    $("#modelSampleType").modal('hide');
    $("#btnSave1").html("Save");
    $("#lblTitle1").html("New Manufacturer Type");

    document.getElementById("chkQC").checked = false;
    document.getElementById("chkReg").checked = false;
    document.getElementById("chkCali").checked = false;
}
//#endregion