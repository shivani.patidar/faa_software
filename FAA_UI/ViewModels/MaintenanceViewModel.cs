using System;
using FAA_UI.Models;
using System.Collections.Generic;

#nullable disable
namespace FAA_UI.ViewModels
{
    public class MaintenanceViewModel
    {
        public TblInstrumentSetting instrumentSetting{get;set;}
        public TblSystemSetting systemSetting{get;set;}
    }
}