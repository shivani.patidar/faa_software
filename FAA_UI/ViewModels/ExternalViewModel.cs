using FAA_UI.Models;
using System.Collections.Generic;
#nullable disable
namespace FAA_UI.ViewModels
{
    public class ExternalViewModel
    {
        public IEnumerable<TblTestExtern> tblExternParaList{get;set;}
        public TblTestExtern tblExternPara{get;set;}
    }
}