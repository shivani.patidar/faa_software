using System;
using FAA_UI.Models;
using System.Collections.Generic;

#nullable disable
namespace FAA_UI.ViewModels
{
    public class TestViewModel
    {
        public IEnumerable<TblTest> tblTests{get;set;}
        public TblTest selectedTest{get;set;}
        public TblTestCalib tblTestCalibration{get;set;}
        public TblTestDetail tblTestDetail{get;set;}
        public TblTestVolume tblTestVolume{get;set;}
        public IEnumerable<TblRefRange> tblRefRange{get;set;}
        public TblRefRange selectedRefRange{get;set;}
        public int selectedTestID{get;set;}

    }
}