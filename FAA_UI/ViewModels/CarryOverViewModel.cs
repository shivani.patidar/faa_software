using FAA_UI.Models;
using System.Collections.Generic;

#nullable disable
namespace FAA_UI.ViewModels
{
    public class CarryOverViewModel
    {
        public IEnumerable<TblTestCarryOver> tblCarryOvers{get;set;}

        public TblTestCarryOver tblCarryOver{get;set;}

        public IEnumerable<TblProfileDet> tblProfileDet{get;set;}

        public TestListViewModel tblTestListModel{get;set;}

        public int selectedCarryOverID{get;set;}

        public string strTestCode;

        public string straffectedTestCode;
    }
}