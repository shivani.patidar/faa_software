﻿using FAA_UI.Models;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace FAA_UI.ViewModels
{
    public class DataDirectoryViewModel
    {

        public static bool SaveOperator(TblOperator tblOperator)
        {
            int result = 0;
            try
            {
                using var connection = new MySqlConnection("Server=localhost;port=3306;user=root;password=Biosense23;Database=DB_FAA_Software;");
                {
                    string query = "";
                    if (tblOperator.OprId == 0)
                    {
                         query = "INSERT INTO tbl_operator (Opr_Name, Opr_RoleId, Opr_Email, Opr_MobileNo, Opr_LoginId, Opr_Password, Opr_Image) VALUES (@Opr_Name, @Opr_RoleId, @Opr_Email, @Opr_MobileNo, @Opr_LoginId, @Opr_Password, @Opr_Image)";
                    }
                    else
                    {
                         query = "UPDATE tbl_operator SET Opr_Name=@Opr_Name, Opr_RoleId=@Opr_RoleId, Opr_Email=@Opr_Email, Opr_MobileNo=@Opr_MobileNo, Opr_LoginId=@Opr_LoginId, Opr_Password=@Opr_Password, Opr_Image=@Opr_Image WHERE Opr_Id=@Opr_Id";
                    }
                    MySqlCommand myCommand = new MySqlCommand(query, connection);
                    connection.Open();
                    myCommand.Parameters.AddWithValue("@Opr_Id", tblOperator.OprId);
                    myCommand.Parameters.AddWithValue("@Opr_Name", tblOperator.OprName);
                    myCommand.Parameters.AddWithValue("@Opr_RoleId", tblOperator.OprRoleId);
                    myCommand.Parameters.AddWithValue("@Opr_Email", tblOperator.OprEmail);
                    myCommand.Parameters.AddWithValue("@Opr_MobileNo", tblOperator.OprMobileNo);
                    myCommand.Parameters.AddWithValue("@Opr_LoginId", tblOperator.OprLoginId);
                    myCommand.Parameters.AddWithValue("@Opr_Password", tblOperator.OprPassword);
                    myCommand.Parameters.AddWithValue("@Opr_Image", tblOperator.OprImage);

                    myCommand.ExecuteNonQuery();
                    connection.Close();
                }
                return true;
            }
            catch
            {
                throw;
                return false;
            }
        }




        public static TblOperator GetOperatorById(int? Id)
        {
            TblOperator list = new TblOperator();
            try
            {
                using var connection = new MySqlConnection("Server=localhost;port=3306;user=root;password=Biosense23;Database=DB_FAA_Software;");
                {
                    connection.Open();
                    using var command = new MySqlCommand("SELECT Opr_Id, Opr_Name, Opr_RoleId, Opr_Email, Opr_MobileNo, Opr_LoginId, Opr_Password, Opr_Image FROM tbl_operator WHERE Opr_Id=@id;", connection);
                    command.Parameters.AddWithValue("@id", Id);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.OprId = Convert.ToInt32(reader["Opr_Id"]);
                            list.OprName = reader["Opr_Name"].ToString();
                            list.OprRoleId = Convert.ToInt16(reader["Opr_RoleId"]);
                            list.OprEmail = reader["Opr_Email"].ToString();
                            list.OprMobileNo = reader["Opr_MobileNo"].ToString();
                            list.OprLoginId = reader["Opr_LoginId"].ToString();
                            list.OprPassword = reader["Opr_Password"].ToString();
                            list.OprImage = (byte[])reader["Opr_Image"];
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return list;
        }
    }
}
