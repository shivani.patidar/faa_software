using System;
using FAA_UI.Models;
using System.Collections.Generic;

#nullable disable
namespace FAA_UI.ViewModels
{
    public class ReagentViewModel
    {
        public IEnumerable<TblRgtbotle> tblReagentBottleList{get;set;}

        public TblRgtbotle tblReagentBottle{get;set;}

        public TblRgtlot tblReagentLot{get;set;}

        public TblRgtpo tblReagentPos{get;set;}

        public IEnumerable<TblRgtpo> tblReagentPosList{get;set;}
        
        public TestListViewModel tblTestListModel{get;set;}
        public int selectedBottleID{get;set;}

    }
    
}