using System;
using FAA_UI.Models;
using System.Collections.Generic;

#nullable disable

namespace FAA_UI.ViewModels
{
    public class ScheduleViewModel
    {
        public IEnumerable<TblSchedule> tblSchedules{get;set;}

        public TblSchedule tblSchedule{get;set;}

        public TblPatient tblPatient{get;set;}

        public IEnumerable<TblScheduledet> tblScheduledets{get;set;}

        public TestListViewModel tblTestListModel{get;set;}

        public int selectedScheduleId{get;set;}
    }
    
}