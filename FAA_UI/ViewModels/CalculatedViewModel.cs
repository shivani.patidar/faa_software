using FAA_UI.Models;
using System.Collections.Generic;

#nullable disable
namespace FAA_UI.ViewModels
{
    public class CalculatedViewModel
    {
        public IEnumerable<TblTestCalcu> tblCalcuTestList{get;set;}
        public TblTestCalcu tblCalcuTest{get;set;}
        public TestListViewModel tblTestListViewModel{get;set;}
    }
}