using System;
using FAA_UI.Models;
using System.Collections.Generic;

#nullable disable

namespace FAA_UI.ViewModels
{
    public class ProfileViewModel
    {
        public IEnumerable<TblTestProfile> tblTestProfiles{get;set;}

        public TblTestProfile tblProfile{get;set;}

        public IEnumerable<TblProfileDet> tblProfileDet{get;set;}

        public TestListViewModel tblTestListModel{get;set;}

        public int selectedProfileID{get;set;}
    }
    
}