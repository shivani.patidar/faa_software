using System;
using FAA_UI.Models;
using System.Collections.Generic;

#nullable disable

namespace FAA_UI.ViewModels
{
    public class ReflexViewModel
    {
        public IEnumerable<TblTestReflex> tblReflexs{get;set;}

        public TblTestReflex tblReflex{get;set;}

        public IEnumerable<TblReflexDet> tblReflexDet{get;set;}

        public TestListViewModel tblTestListModel{get;set;}

        public int selectedReflexID{get;set;}
    }
    
}