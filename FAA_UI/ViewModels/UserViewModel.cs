using System.Collections.Generic;
using FAA_UI.Models;
using System.ComponentModel.DataAnnotations;

#nullable disable
namespace FAA_UI.ViewModels
{
    public class UserViewModel
    {
        public List<TblUser> tblUsers{get;set;}
        public TblUser tblUser{get;set;}
        public TblPerson tblPerson{get;set;}
        public TblUserright tblUserRight{get;set;}
        
        [Display(Name = "Confirm Password")]
        //[Compare("tblUser.UPwd", ErrorMessage = "Password and Confirmation Password must match.")]
        [StringLength(10)]
        public string ConfirmPassword{get;set;}

    }
}