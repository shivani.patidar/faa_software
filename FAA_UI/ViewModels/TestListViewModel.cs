using System.Collections.Generic;
using System.Linq;
using FAA_UI.Models;

#nullable disable

namespace FAA_UI.ViewModels
{
    public partial class TestListViewModel
    {
        public  List<TblTestList> TestList{get;set;}
        FAA_DbContext dbContext;
        public TestListViewModel()
        {
            dbContext = new FAA_DbContext();
            {
                //var Tests = from Test in dbContext.TblTests.Where(tbl => (tbl.IsActive == true && tbl.IsVisible == true))                            
                var Tests = from Test in dbContext.TblTests

                            select new TblTestList
                    {
                        TestId = Test.TestId,
                        UniqueTestId = Test.UniqueTestId,
                        TestCode = Test.TestCode,
                        TestName = Test.TestName,
                        isSelected = false ,  
                    };
                    TestList=Tests.ToList();
            }          
        }        
    }
    public partial class TblTestList
    {
        public short TestId { get; set; }
        public short UniqueTestId { get; set; }
        public string TestCode { get; set; }
        public string TestName { get; set; }
        public bool isSelected { get; set; }
        
    }

    
}