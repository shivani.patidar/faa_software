﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblTest
{
    public short TestId { get; set; }

    /// <summary>
    /// Uniqe Id given by Company for every test parameter for upload/download Testdeatils/standards/Reagents
    /// 
    /// </summary>
    public short UniqueTestId { get; set; }

    public string TestCode { get; set; }

    public string TestName { get; set; }

    public sbyte Method { get; set; }

    public sbyte W1 { get; set; }

    public sbyte W2 { get; set; }

    /// <summary>
    /// for Result Display only
    /// </summary>
    public float Decimals { get; set; }

    public sbyte Unit { get; set; }

    /// <summary>
    /// Increasing/Decreasing
    /// </summary>
    public sbyte Direction { get; set; }

    public float? A { get; set; }

    public float? B { get; set; }

    public float R1volume { get; set; }

    public float R2volume { get; set; }

    public float SampleVolume { get; set; }

    public sbyte R1incubationTime { get; set; }

    public float? R1incubationTimeSec { get; set; }

    public sbyte R2incubationTime { get; set; }

    public float? R2incubationTimeSec { get; set; }

    public sbyte R1strSpeed { get; set; }

    public sbyte R2strSpeed { get; set; }

    public float ReadTime { get; set; }

    public float? SubDpltionLmt { get; set; }

    public float? Linearity { get; set; }

    public bool AutoDilute { get; set; }

    public float? BlankValue { get; set; }

    public float? BlankLow { get; set; }

    public float? BlankHigh { get; set; }

    public float? SampleScale { get; set; }

    public sbyte IsVisible { get; set; }

    public sbyte IsActive { get; set; }

    public bool SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }

    public sbyte? Reagent { get; set; }

    public virtual ICollection<TblProfileDet> TblProfileDets { get; set; } = new List<TblProfileDet>();

    public virtual ICollection<TblSampleTest> TblSampleTests { get; set; } = new List<TblSampleTest>();
}
