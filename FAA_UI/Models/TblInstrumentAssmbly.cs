﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblInstrumentAssmbly
{
    public short AssemblyId { get; set; }

    public sbyte? InstrumentId { get; set; }

    public string Assembly { get; set; }

    public sbyte? AswCode { get; set; }

    public sbyte? EswCode { get; set; }

    public string AswName { get; set; }

    public DateTime? InstalledDate { get; set; }

    public sbyte? IsActive { get; set; }

    public sbyte? Uid { get; set; }
}
