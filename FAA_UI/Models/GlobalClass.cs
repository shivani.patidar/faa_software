﻿using Org.BouncyCastle.Crypto.Tls;

namespace FAA_UI.Models
{
    public class GlobalClass
    {
 

        public static bool UserActivityLogFile(string UserName, string PageName, string Action)
        {
            string strMessage = "";
            try
            {
                strMessage = String.Format("{0}, UserName = {1}, PageName = {2}, Action = {3}", DateTime.Now, UserName, PageName, Action);
                string strFileName = "UserActivityLog " + DateTime.Now.ToString("dd") +" "+ DateTime.Now.ToString("MMM") +" "+ DateTime.Now.ToString("yyyy") + ".txt";
                //Path.GetTempPath()

                //if (!Directory.Exists(@"C:/Temp/UserActivityLog"))
                //{
                //    Directory.CreateDirectory(@"C:/Temp/UserActivityLog");
                //}
                string mPath = @"C:/Temp/UserActivityLog";
                FileStream objFilestream = new FileStream(string.Format("{0}\\{1}", mPath, strFileName), FileMode.Append, FileAccess.Write);
                StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
                objStreamWriter.WriteLine(strMessage);
                objStreamWriter.Close();
                objFilestream.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static bool ErrorLogFile(string UserName, string PageName, string Action, string Message)
        {
            string strMessage = "";
            try
            {
                strMessage = String.Format("{0}, UserName = {1}, PageName = {2}, Action = {3}, Error = {4}", DateTime.Now, UserName, PageName, Action, Message);
                string strFileName = "ErrorLogFile " + DateTime.Now.ToString("dd") + " " + DateTime.Now.ToString("MMM") + " " + DateTime.Now.ToString("yyyy") + ".txt";
                //Path.GetTempPath()
                if (!Directory.Exists(@"C:/Temp/ErrorLogFile"))
                {
                    Directory.CreateDirectory(@"C:/Temp/ErrorLogFile");
                }
                string mPath = @"C:/Temp/ErrorLogFile";
                FileStream objFilestream = new FileStream(string.Format("{0}\\{1}", mPath, strFileName), FileMode.Append, FileAccess.Write);
                StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
                objStreamWriter.WriteLine(strMessage);
                objStreamWriter.Close();
                objFilestream.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
