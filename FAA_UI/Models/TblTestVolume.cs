﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblTestVolume
{
    public short TestId { get; set; }

    public float SvolN { get; set; }

    public float SvolI { get; set; }

    public float SvolD { get; set; }

    public sbyte StdVol { get; set; }

    public sbyte DilRatioN { get; set; }

    public sbyte DilRatioI { get; set; }

    public sbyte DilRatioD { get; set; }

    public float R1vol { get; set; }

    public float R2vol { get; set; }

    public float R3vol { get; set; }
}
