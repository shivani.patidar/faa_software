﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblUser
{
    public sbyte UidPk { get; set; }

    public string ULoginId { get; set; }

    public string UPwd { get; set; }

    public string UPwdHint { get; set; }

    public sbyte UType { get; set; }

    public short PersonId { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
