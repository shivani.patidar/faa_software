﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblProfileDet
{
    public int ProDetId { get; set; }

    public int ProId { get; set; }

    public short TestId { get; set; }

    public virtual TblTest Test { get; set; }
}
