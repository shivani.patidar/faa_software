﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblTestCarryOver
{
    public short Idpk { get; set; }

    public short TestId { get; set; }

    public short AfectedTestId { get; set; }

    public sbyte Action { get; set; }

    public string WashType { get; set; }

    public short WashVolume { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
