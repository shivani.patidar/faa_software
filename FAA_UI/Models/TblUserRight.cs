﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblUserright
{
    public short Seqid { get; set; }

    public sbyte UserId { get; set; }

    public sbyte FormId { get; set; }

    public sbyte FunctionId { get; set; }

    public sbyte IsAllowed { get; set; }
}
