﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblDoctordetail
{
    public int DrId { get; set; }

    public string DrName { get; set; }

    public string DrDepartment { get; set; }

    public string MobileNo { get; set; }

    public string DrRemark { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }

    public virtual ICollection<TblSample> TblSamples { get; set; } = new List<TblSample>();
}
