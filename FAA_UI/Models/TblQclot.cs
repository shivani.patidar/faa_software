﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblQclot
{
    public int LotId { get; set; }

    public sbyte? ConsumableType { get; set; }

    public string LotNo { get; set; }

    public sbyte? MfgId { get; set; }

    public sbyte? MfgMonth { get; set; }

    public short? MfgYear { get; set; }

    public sbyte? ExpMonth { get; set; }

    public short? ExpYear { get; set; }

    public short? OpenWhileStability { get; set; }

    public DateTime? StabilityEnd { get; set; }

    public float? Conc { get; set; }

    public float? Od { get; set; }

    public sbyte? IsActive { get; set; }

    public sbyte? IsMultiPoint { get; set; }

    public float? Min { get; set; }

    public float? Max { get; set; }

    public float? Mean { get; set; }

    public float? Sd { get; set; }

    public float? LabMean { get; set; }

    public float? LabSd { get; set; }

    public sbyte? NoOfStd { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
