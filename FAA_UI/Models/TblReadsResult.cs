﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblReadsResult
{
    public int ReadId { get; set; }

    public int ResultId { get; set; }

    public int BlankCount { get; set; }

    public float BlankOd { get; set; }

    public sbyte ReadCount { get; set; }

    public sbyte InstrumentId { get; set; }

    public float? R1 { get; set; }

    public float? R2 { get; set; }

    public float? R3 { get; set; }

    public float? R4 { get; set; }

    public float? R5 { get; set; }

    public float? R6 { get; set; }

    public float? R7 { get; set; }

    public float? R8 { get; set; }

    public float? R9 { get; set; }

    public float? R10 { get; set; }

    public float? R11 { get; set; }

    public float? R12 { get; set; }

    public float? R13 { get; set; }

    public float? R14 { get; set; }

    public float? R15 { get; set; }

    public float? R16 { get; set; }

    public float? R17 { get; set; }

    public float? R18 { get; set; }

    public float? R19 { get; set; }

    public float? R20 { get; set; }

    public float? R21 { get; set; }

    public float? R22 { get; set; }

    public float? R23 { get; set; }

    public float? R24 { get; set; }

    public float? R25 { get; set; }

    public float? R26 { get; set; }

    public float? R27 { get; set; }

    public float? R28 { get; set; }

    public float? R29 { get; set; }

    public float? R30 { get; set; }

    public float? R31 { get; set; }

    public float? R32 { get; set; }

    public float? R33 { get; set; }

    public float? R34 { get; set; }

    public float? R35 { get; set; }

    public float? R36 { get; set; }

    public float? R37 { get; set; }

    public float? R38 { get; set; }

    public float? R39 { get; set; }

    public float? R40 { get; set; }

    public float? R41 { get; set; }

    public float? R42 { get; set; }

    public float? R43 { get; set; }

    public float? R44 { get; set; }

    public float? R45 { get; set; }

    public float? R46 { get; set; }

    public float? R47 { get; set; }

    public float? R48 { get; set; }

    public float? R49 { get; set; }

    public float? R50 { get; set; }

    public float? R51 { get; set; }

    public float? R52 { get; set; }

    public float? R53 { get; set; }

    public float? R54 { get; set; }

    public float? R55 { get; set; }

    public float? R56 { get; set; }

    public float? R57 { get; set; }

    public float? R58 { get; set; }

    public float? R59 { get; set; }

    public float? R60 { get; set; }

    public float? R61 { get; set; }

    public float? R62 { get; set; }

    public float? R63 { get; set; }

    public float? R64 { get; set; }

    public float? R65 { get; set; }

    public float? R66 { get; set; }

    public float? R67 { get; set; }

    public float? R68 { get; set; }

    public float? R69 { get; set; }

    public float? R70 { get; set; }

    public float? R71 { get; set; }

    public float? R72 { get; set; }

    public float? R73 { get; set; }

    public float? R74 { get; set; }

    public float? R75 { get; set; }

    public sbyte IsActive { get; set; }
}
