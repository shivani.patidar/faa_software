﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblSystemSetting
{
    public short SystemSettingId { get; set; }

    public string InstrumentPort { get; set; }

    public sbyte? InstrumentBaudRate { get; set; }
}
