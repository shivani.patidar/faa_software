﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblError
{
    public short ErrorId { get; set; }

    public string ErrorCode { get; set; }

    public string ErrorDesc { get; set; }

    public string ErrorAction { get; set; }

    public string ErrorFlag { get; set; }

    public sbyte? IsStopError { get; set; }

    public short? AssemblyId { get; set; }

    public sbyte? InstrumentId { get; set; }

    /// <summary>
    /// 1 if need to log this error in Database else 0
    /// </summary>
    public sbyte? LogError { get; set; }

    /// <summary>
    /// No of times software should try to auto recover due to this error
    /// 
    /// </summary>
    public sbyte? RecoverTryCnt { get; set; }

    /// <summary>
    /// 1 . If error displayed to user.
    /// 
    /// </summary>
    public sbyte? DisplayToUser { get; set; }
}
