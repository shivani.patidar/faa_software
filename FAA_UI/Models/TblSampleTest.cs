﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblSampleTest
{
    public int SmpTstId { get; set; }

    public int? SampleId { get; set; }

    public short? TestId { get; set; }

    public virtual TblSample Sample { get; set; }

    public virtual TblTest Test { get; set; }
}
