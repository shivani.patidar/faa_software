﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblHospitalinfo
{
    public int HstlId { get; set; }

    public string HstlName { get; set; }

    public string HstlPhoneNo { get; set; }

    public string HstlAddress { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
