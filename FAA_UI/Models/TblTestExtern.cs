﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblTestExtern
{
    public short ExternTestId { get; set; }

    public string TestCode { get; set; }

    public string TestName { get; set; }

    /// <summary>
    /// 1. Numeric(Quantitative)\n2. chars(Qualitative)
    /// </summary>
    public string ResultType { get; set; }

    public sbyte? Decimals { get; set; }

    public int? Unit { get; set; }

    public float? RefLow { get; set; }

    public float? RefHigh { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
