﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblQclotDet
{
    public int QcdetId { get; set; }

    public int LotId { get; set; }

    public int? TestId { get; set; }

    public int? Position { get; set; }

    public float Conc { get; set; }

    public float StdOd { get; set; }
}
