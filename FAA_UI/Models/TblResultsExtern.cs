﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblResultsExtern
{
    public int ScheduleId { get; set; }

    public short ExternTestId { get; set; }

    public int? SmpId { get; set; }

    public DateTime ResultDate { get; set; }

    public string Result { get; set; }

    public int? Unit { get; set; }

    public string Flag { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
