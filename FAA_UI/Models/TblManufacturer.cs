﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

/// <summary>
/// manufacturer details
/// </summary>
public partial class TblManufacturer
{
    public int MnfId { get; set; }

    public string MnfName { get; set; }

    public string MnfType { get; set; }

    public string MnfCali { get; set; }

    public string MnfReg { get; set; }

    public string MnfQc { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreatedBy { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? ModifyDate { get; set; }

    public int? ModifyBy { get; set; }
}
