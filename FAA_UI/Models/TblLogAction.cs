﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblLogAction
{
    public long LogAId { get; set; }

    public string UId { get; set; }

    public DateTime? ADate { get; set; }

    public TimeSpan? ATime { get; set; }

    /// <summary>
    /// 1. USER_LOGIN
    /// 2. USER_LOGOUT
    /// 3. SAVE
    /// 4. UPDATE
    /// 5. DELETE
    /// 6. SERVICE_CHECK
    /// 7. MAINTENACE
    /// 8..AUTOMAINTENANCE
    /// 9. OPEN_Window
    /// 10.CLOSE_WINDOW
    /// 11. WARNING
    /// 12. 
    /// </summary>
    public sbyte? LogAType { get; set; }

    public string Functionality { get; set; }

    public string Action { get; set; }
}
