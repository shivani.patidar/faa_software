﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblLogSetting
{
    public int SetId { get; set; }

    public sbyte? UId { get; set; }

    /// <summary>
    /// ARM/TRAY/ISE/STR/CRU
    /// </summary>
    public sbyte AssemblyId { get; set; }

    public DateTime? SetDate { get; set; }

    public TimeSpan? SetTime { get; set; }

    public sbyte Path { get; set; }

    public sbyte OldValue { get; set; }

    public sbyte NewValue { get; set; }
}
