﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblTestCalcu
{
    public int CalId { get; set; }

    public string CalTestCode { get; set; }

    public string CalTestName { get; set; }

    public sbyte CalTestId { get; set; }

    public string CalExpression { get; set; }

    public sbyte? Decimals { get; set; }

    public float? RefLow { get; set; }

    public float? RefHigh { get; set; }

    public int? Unit { get; set; }

    public sbyte? SendToHost { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
