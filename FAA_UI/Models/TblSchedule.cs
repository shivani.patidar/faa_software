﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblSchedule
{
    public int ScheduleId { get; set; }

    public int SeqId { get; set; }

    public sbyte Position { get; set; }

    public int PatientId { get; set; }

    public int SampleId { get; set; }

    public short PrevCalId { get; set; }

    public sbyte Container { get; set; }

    public sbyte ScheduleType { get; set; }

    public sbyte SchStatus { get; set; }

    public DateTime SchTime { get; set; }

    public sbyte DiskNo { get; set; }
}
