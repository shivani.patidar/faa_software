﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblOperator
{
    public int OprId { get; set; }

    public string OprName { get; set; }

    public int? OprRoleId { get; set; }

    public string OprEmail { get; set; }

    public string OprMobileNo { get; set; }

    public string OprLoginId { get; set; }

    public string OprPassword { get; set; }

    public byte[] OprImage { get; set; }

    public DateTime? LoginTime { get; set; }

    public DateTime? LogoutTime { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
