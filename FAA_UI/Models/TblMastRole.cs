﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblMastRole
{
    public long RoleId { get; set; }

    public string Role { get; set; }

    public string CreatedBy { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string ModifiedBy { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public bool? SoftDelete { get; set; }

    public virtual ICollection<TblMenuRoleAccess> TblMenuRoleAccesses { get; set; } = new List<TblMenuRoleAccess>();

    public virtual ICollection<TblUsermaster> TblUsermasters { get; set; } = new List<TblUsermaster>();
}
