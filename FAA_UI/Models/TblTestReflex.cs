﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblTestReflex
{
    public sbyte ReflexId { get; set; }

    public short TestId { get; set; }

    public string ReflexType { get; set; }

    public float LowValue { get; set; }

    public float HighValue { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
