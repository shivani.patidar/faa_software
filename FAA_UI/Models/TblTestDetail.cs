﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblTestDetail
{
    public int TestSqunceId { get; set; }

    public short TestId { get; set; }

    /// <summary>
    /// will be TestName by default
    /// 
    /// </summary>
    public string PrintName { get; set; }

    /// <summary>
    /// will be TestCode by default
    /// 
    /// </summary>
    public string LisName { get; set; }

    public short RunIndex { get; set; }

    public short PrintIndex { get; set; }

    public short? DispIndex { get; set; }

    public sbyte? AutoDil { get; set; }

    public sbyte? SendToHost { get; set; }

    public sbyte? ReplicateSmp { get; set; }

    public sbyte? ReplicateStd { get; set; }

    public sbyte? ReplicateCntrl { get; set; }

    public int? CalLot { get; set; }

    public sbyte? ControlInterval { get; set; }

    public bool? OnlineCalib { get; set; }

    public sbyte? CuvWashB { get; set; }

    public sbyte? CuvWashA { get; set; }

    public bool? SpecialDil { get; set; }

    public sbyte? CalBlanking { get; set; }

    public float? Cost { get; set; }

    public float? CalVarFactor { get; set; }

    public float? RbLow { get; set; }

    public float? RbHigh { get; set; }

    public float? AbsLimitLow { get; set; }

    public float? AbsLimitHigh { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }

    public string TblTestDetailscol { get; set; }
}
