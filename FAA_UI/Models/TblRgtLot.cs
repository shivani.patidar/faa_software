﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblRgtlot
{
    public int RlotId { get; set; }

    public sbyte ConsumableType { get; set; }

    public short TestId { get; set; }

    public string LotNo { get; set; }

    public sbyte MfgId { get; set; }

    public sbyte MfgMonth { get; set; }

    public short MfgYear { get; set; }

    public sbyte ExpMonth { get; set; }

    public short ExpYear { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
