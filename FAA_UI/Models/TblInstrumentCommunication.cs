﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblInstrumentCommunication
{
    public sbyte InstrumentId { get; set; }

    /// <summary>
    /// 1 Serial Port
    /// 2 TCP/IP
    /// 
    /// </summary>
    public sbyte? ComType { get; set; }

    public sbyte? ComPort { get; set; }

    public short? BaudRate { get; set; }

    /// <summary>
    /// 1- if LIS Settings are ON
    /// 0- if LIS Settings are OFF
    /// </summary>
    public sbyte? LisEnabled { get; set; }

    public sbyte? LisComType { get; set; }

    public sbyte? LisComPort { get; set; }

    public short? LisBaudRate { get; set; }

    public string LisServerIp { get; set; }

    public sbyte LisTcpPort { get; set; }

    /// <summary>
    /// 1- Single Packet at a time
    /// 2. Multiple Packets at a time
    ///  
    /// 
    /// 
    /// </summary>
    public sbyte? LisDataType { get; set; }

    public sbyte? LisSot { get; set; }

    public sbyte? LisEot { get; set; }

    public sbyte? LisStx { get; set; }

    public sbyte? LisEtx { get; set; }

    public sbyte? LisAck { get; set; }

    public sbyte? LisNack { get; set; }

    public sbyte? LisEnq { get; set; }

    public sbyte? LisPacketSeperator { get; set; }

    public sbyte? LisParmeterSeperator { get; set; }

    public sbyte? LisDelimeter { get; set; }
}
