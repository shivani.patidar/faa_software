﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblAddress
{
    public int AddId { get; set; }

    public string AddLine1 { get; set; }

    public string AddLine2 { get; set; }

    public string AddLine3 { get; set; }

    public int? AddPin { get; set; }

    public short? AddCity { get; set; }

    public sbyte? AddState { get; set; }

    public sbyte? AddCountry { get; set; }

    public sbyte CUid { get; set; }

    public sbyte LastModUid { get; set; }

    public sbyte DUid { get; set; }
}
