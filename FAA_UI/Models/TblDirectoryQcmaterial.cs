﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblDirectoryQcmaterial
{
    public int QcMtrlId { get; set; }

    public string QcMtrlName { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
