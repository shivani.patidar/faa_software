﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblUsermaster
{
    public long UserId { get; set; }

    public long? RoleId { get; set; }

    public string Name { get; set; }

    public string Email { get; set; }

    public string UserName { get; set; }

    public string Password { get; set; }

    public byte[] UserImage { get; set; }

    public DateTime? LoginTime { get; set; }

    public DateTime? LogoutTime { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreatedBy { get; set; }

    public DateTime? CreatedDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }

    public virtual TblMastRole Role { get; set; }
}
