﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblLogActionDet
{
    public long LogAId { get; set; }

    public string OldValue { get; set; }

    public string NwValue { get; set; }

    public string ColumnName { get; set; }
}
