﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblScheduledet
{
    public int ScheduleDetId { get; set; }

    public int ScheduleId { get; set; }

    public sbyte ScheduleType { get; set; }

    public short TestId { get; set; }

    public sbyte ReplicateNo { get; set; }

    public sbyte DilRatio { get; set; }

    public int LotId { get; set; }

    public sbyte RunStatus { get; set; }

    public sbyte TestType { get; set; }

    public sbyte Position { get; set; }
}
