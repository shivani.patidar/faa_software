﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblInstrumentSetting
{
    public int InstrumentSettingsId { get; set; }

    public int InstrumentId { get; set; }

    public int ArmPosHome { get; set; }

    public int ArmPosRgtIn { get; set; }

    public int ArmPosRgtOut { get; set; }

    public int ArmPosRgtSmp { get; set; }

    public int ArmPosCuv { get; set; }

    public int ArmDepthCuv { get; set; }

    public int ArmDepthHome { get; set; }

    public int ArmDepthRgtIn { get; set; }

    public int ArmDepthRgtOut { get; set; }

    public int CruDepthWash { get; set; }

    public int CruDepthWater { get; set; }

    public int StrPosHome { get; set; }

    public int StrPosCuv { get; set; }

    public int StrDepthCuv { get; set; }

    public int StrDepthHome { get; set; }

    public int StrSpeed { get; set; }

    public int RgtPosIn { get; set; }

    public int RgtPosOut { get; set; }

    public int RgtPosSmp { get; set; }

    public int RgtScanIn { get; set; }

    public int RgtScanOut { get; set; }

    public int RgtScanSmp { get; set; }

    public int SyringeOffset { get; set; }

    public int AddWaterTime { get; set; }

    public int RemoveWaterTime { get; set; }

    public decimal PathLength { get; set; }
}
