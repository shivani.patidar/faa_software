﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblTestReflexDet
{
    public int RflxDetlId { get; set; }

    public sbyte ReflexId { get; set; }

    public short TestId { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
