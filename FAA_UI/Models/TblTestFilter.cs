﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblTestFilter
{
    public int FltrId { get; set; }

    public string Filter { get; set; }
}
