﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblPerson
{
    public short PersonId { get; set; }

    public string PTitle { get; set; }

    public string PFirstName { get; set; }

    public string PMidleName { get; set; }

    public string PLastName { get; set; }

    public string PDisplayName { get; set; }

    public DateTime? PDob { get; set; }

    public string PAgeDisplay { get; set; }

    public short? PAgeYear { get; set; }

    public short? PAgeMonth { get; set; }

    public short? PAgeDays { get; set; }

    public string PIdentityMark { get; set; }

    public short? PHeightInCm { get; set; }

    public float? PWeightInKg { get; set; }

    public short PAddId { get; set; }

    public sbyte CUid { get; set; }

    public sbyte LastModUid { get; set; }

    public sbyte DUid { get; set; }
}
