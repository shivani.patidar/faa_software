﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblCurvetype
{
    public int CrvTyId { get; set; }

    public string CrvType { get; set; }
}
