﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblDirectoryDet
{
    public short DictItemId { get; set; }

    public short DictId { get; set; }

    public string DictItem { get; set; }

    public string ItemValue { get; set; }

    public string DictItemDesc { get; set; }

    public bool? IsEditable { get; set; }

    public sbyte IsVisible { get; set; }

    public sbyte IsActive { get; set; }

    public sbyte CUid { get; set; }

    public sbyte LastModUid { get; set; }

    public sbyte DUid { get; set; }
}
