﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblReadsBlank
{
    public sbyte CuvNo { get; set; }

    public sbyte FilterId { get; set; }

    public int Rcount { get; set; }

    public DateTime? ReadTime { get; set; }

    public float? Read { get; set; }

    public sbyte? ReadStatus { get; set; }
}
