﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblResult
{
    public int ResultId { get; set; }

    public short TestId { get; set; }

    public int SampleId { get; set; }

    public int DaySeqId { get; set; }

    public int ScheduleId { get; set; }

    public sbyte SmpPos { get; set; }

    public sbyte Cuv { get; set; }

    public int SchType { get; set; }

    public int CalId { get; set; }

    public int RgtLot { get; set; }

    public int CalLot { get; set; }

    public int Qclot { get; set; }

    public float Od0 { get; set; }

    public float RgtBlank { get; set; }

    public float Od { get; set; }

    public float ResultValue { get; set; }

    public string ResultDescription { get; set; }

    public string Flag { get; set; }

    public DateTime ResultDateTime { get; set; }

    public int TestType { get; set; }

    public string TblResultcol { get; set; }
}
