﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblImage
{
    public int Id { get; set; }

    public byte[] Img { get; set; }
}
