﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblMenuMapping
{
    public long Id { get; set; }

    public string Page { get; set; }

    public string SubPage { get; set; }

    public string Icon { get; set; }

    public int? OrderId { get; set; }

    public int? ParentId { get; set; }

    public virtual ICollection<TblMenuRoleAccess> TblMenuRoleAccesses { get; set; } = new List<TblMenuRoleAccess>();
}
