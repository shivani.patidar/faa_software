﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblDirectoryQualitative
{
    public int QualtId { get; set; }

    public string QualtName { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
