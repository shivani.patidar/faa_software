﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblFlag
{
    public int FlagId { get; set; }

    /// <summary>
    /// 1. Absent
    /// 2. Instrument_Warning.
    /// 3. Normal_Warning
    /// 4..RESULT
    /// 5.  Instrument_Error
    ///  
    /// </summary>
    public sbyte? FlagType { get; set; }

    public string FlagCode { get; set; }

    public string FlagMsg { get; set; }

    public string TblFlagscol { get; set; }
}
