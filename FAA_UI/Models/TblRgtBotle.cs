﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblRgtbotle
{
    public int RbtlId { get; set; }

    public short TestId { get; set; }

    public int? RlotId { get; set; }

    public sbyte RgtType { get; set; }

    public sbyte BtlNo { get; set; }

    public sbyte BtlType { get; set; }

    public float BtlSize { get; set; }

    public float Volume { get; set; }

    public short TestCnt { get; set; }

    public float RgtBlank { get; set; }

    public short OpenWhileStability { get; set; }

    public DateTime StabilityEnd { get; set; }

    public string Barcode { get; set; }

    public bool IsExhausted { get; set; }

    public bool IsActive { get; set; }

    public sbyte? Position { get; set; }

    public sbyte? ExpMonth { get; set; }

    public short? ExpYear { get; set; }

    public sbyte? MfgId { get; set; }

    public sbyte? MfgMonth { get; set; }

    public short? MfgYear { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
