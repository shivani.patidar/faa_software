﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class AllImageUpload
{
    public long Id { get; set; }

    public byte[] ImageFile { get; set; }
}
