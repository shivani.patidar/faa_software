﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblSampletype
{
    public int SmpId { get; set; }

    public string SmpType { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }

    public virtual ICollection<TblRefRange> TblRefRanges { get; set; } = new List<TblRefRange>();
}
