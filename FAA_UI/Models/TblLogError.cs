﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblLogError
{
    public long ErrorId { get; set; }

    public sbyte? UId { get; set; }

    /// <summary>
    /// 1. AUTO MAINTENANCE_STARTUP
    /// 2. AUTO MAINTENANCE_SHUTDOWN
    /// 3. SERVICE_CHECK
    /// 4. MAINTENANCE
    /// 5.PRE_RUN
    /// 6. RUN
    /// 7.POST_RUN
    /// 8.USER_ACTIONS(Barcode-scan/volume-check)
    /// </summary>
    public sbyte? ErrorDuring { get; set; }

    public short? ErrorCode { get; set; }

    public DateTime? ErrDate { get; set; }

    public TimeSpan? ErrTime { get; set; }

    /// <summary>
    /// RUN/CuvWash/tempsetting/servicecheck/preobe-clean
    /// </summary>
    public string Functionality { get; set; }

    /// <summary>
    /// armrotate/updn/rgt rotate
    /// </summary>
    public string Action { get; set; }

    /// <summary>
    /// ARM/TRAY/ISE/STR/CRU
    /// </summary>
    public string Assembly { get; set; }

    /// <summary>
    /// RGT/SMP Pos
    /// </summary>
    public sbyte? ForPosition { get; set; }

    /// <summary>
    /// For which CUV(STR/ARM HIT)
    /// </summary>
    public sbyte? ForCuv { get; set; }
}
