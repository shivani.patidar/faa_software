﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblOperatorPermission
{
    public int OperId { get; set; }

    public string OperName { get; set; }

    public bool? SoftDelete { get; set; }
}
