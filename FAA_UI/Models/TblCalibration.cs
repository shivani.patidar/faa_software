﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblCalibration
{
    public int CalId { get; set; }

    public short TestId { get; set; }

    public sbyte CurveType { get; set; }

    public DateTime? CalExpiry { get; set; }

    public DateTime? CalValidity { get; set; }

    public short? CalValidityDays { get; set; }

    public sbyte? NoOfStds { get; set; }

    public sbyte? IsModified { get; set; }

    public float? Factor { get; set; }

    public float? Factor1 { get; set; }

    public float? Factor2 { get; set; }

    public float? Factor3 { get; set; }

    public float? Factor4 { get; set; }

    public float? Factor5 { get; set; }

    public DateTime? CalDate { get; set; }

    public TimeSpan? CalTime { get; set; }

    public int? PrevCalIdRgtBlnking { get; set; }

    public sbyte? CalStatus { get; set; }

    public sbyte? IsActive { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
