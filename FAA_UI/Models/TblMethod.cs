﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblMethod
{
    public int MthdId { get; set; }

    public string MthdType { get; set; }
}
