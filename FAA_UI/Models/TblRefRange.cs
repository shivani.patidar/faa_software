﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblRefRange
{
    public short RefId { get; set; }

    public short TestId { get; set; }

    public sbyte? Gender { get; set; }

    public float RefLow { get; set; }

    public float RefHigh { get; set; }

    public short AgeMin { get; set; }

    public short AgeMax { get; set; }

    public string AgeUnit { get; set; }

    /// <summary>
    /// FLAG SHOULD BE CALCULATED ON THIS COLOUM
    /// </summary>
    public short? AgeMinDays { get; set; }

    public short? AgeMaxDays { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }

    public int? SmpTyId { get; set; }

    public virtual TblSampletype SmpTy { get; set; }
}
