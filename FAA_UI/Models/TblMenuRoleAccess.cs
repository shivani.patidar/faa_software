﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblMenuRoleAccess
{
    public long AccessId { get; set; }

    public long? RoleId { get; set; }

    public long? MenuMappingId { get; set; }

    public bool? Added { get; set; }

    public bool? Edited { get; set; }

    public bool? Viewed { get; set; }

    public bool? Deleted { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public virtual TblMenuMapping MenuMapping { get; set; }

    public virtual TblMastRole Role { get; set; }
}
