﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblRgtpo
{
    public int? RbtlId { get; set; }

    public sbyte TrayId { get; set; }

    public sbyte TrayPos { get; set; }

    public short? TestId { get; set; }
}
