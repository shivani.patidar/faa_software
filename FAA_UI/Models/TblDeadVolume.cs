﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblDeadvolume
{
    public int DictItemId { get; set; }

    public int? Steps { get; set; }

    public float? PerStepVol { get; set; }

    public int? DvVolume { get; set; }

    public DateTime? DvTime { get; set; }

    public sbyte? Uid { get; set; }
}
