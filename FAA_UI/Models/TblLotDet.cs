﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblLotDet
{
    public int LotId { get; set; }

    public float Conc { get; set; }

    public float StdOd { get; set; }
}
