﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblSample
{
    public int SmpId { get; set; }

    public string SmpBarcode { get; set; }

    public int PatientId { get; set; }

    public int? SmpTypeId { get; set; }

    public int? DrId { get; set; }

    public float? Age { get; set; }

    public float? Height { get; set; }

    public float? Weight { get; set; }

    public string LebHospitalName { get; set; }

    public string HospitalArea { get; set; }

    public int? BedNumber { get; set; }

    public string SmpRemark { get; set; }

    public DateTime? RegistrationDate { get; set; }

    public DateTime? CollectionDate { get; set; }

    public short? Position { get; set; }

    public short? SmpContTypeId { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }

    public virtual TblDoctordetail Dr { get; set; }

    public virtual ICollection<TblSampleTest> TblSampleTests { get; set; } = new List<TblSampleTest>();
}
