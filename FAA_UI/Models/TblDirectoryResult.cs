﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblDirectoryResult
{
    public int RsultId { get; set; }

    public string RsultName { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
