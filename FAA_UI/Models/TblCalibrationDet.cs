﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblCalibrationdet
{
    public int CalDetId { get; set; }

    public int CalId { get; set; }

    public sbyte IsBlank { get; set; }

    public int Pos { get; set; }

    public int? ContainerTypeId { get; set; }

    public float Conc { get; set; }

    public float ActualOd { get; set; }

    public float Od { get; set; }

    public int Lot { get; set; }

    public int DilRatio { get; set; }

    public int Cuv { get; set; }

    public sbyte IsModified { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }

}

