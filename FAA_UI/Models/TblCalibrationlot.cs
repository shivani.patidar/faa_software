﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblCalibrationlot
{
    public int CalibLotId { get; set; }

    public int TestId { get; set; }

    public string LotNo { get; set; }

    public sbyte? ExpMonth { get; set; }

    public short? ExpYear { get; set; }

    public float? FactorMin { get; set; }

    public float? FactorMax { get; set; }

    public bool? SoftDelete { get; set; }

    public int? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public int? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
