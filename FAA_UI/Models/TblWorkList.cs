﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblWorklist
{
    public short SeqNo { get; set; }

    public sbyte Cuv { get; set; }

    public int ScheduleId { get; set; }

    public sbyte ReplicateNo { get; set; }

    public short TestId { get; set; }

    public sbyte ScheduleType { get; set; }

    public sbyte RunTestType { get; set; }

    public sbyte DilRatio { get; set; }

    public int? LotId { get; set; }

    public sbyte SmpPos { get; set; }

    public sbyte R1pos { get; set; }

    public sbyte R2pos { get; set; }

    public sbyte R3pos { get; set; }

    public sbyte SampleVol { get; set; }

    public short R1vol { get; set; }

    public short R2vol { get; set; }

    public short R3vol { get; set; }

    public int DilPos { get; set; }

    public int WashPos { get; set; }

    public sbyte R1short { get; set; }

    public sbyte R2short { get; set; }

    public sbyte R3short { get; set; }

    public sbyte SmpShort { get; set; }

    public sbyte RunStatus { get; set; }

    public float Od { get; set; }

    public float ResultValue { get; set; }

    public decimal Result { get; set; }

    public string Flag { get; set; }

    public DateTime R1time { get; set; }

    public DateTime R2time { get; set; }

    public DateTime R3time { get; set; }

    public DateTime SmpTime { get; set; }

    public int ResultId { get; set; }

    public sbyte LisStatus { get; set; }

    public sbyte DilCuv { get; set; }

    public sbyte DilSeqNo { get; set; }
}
