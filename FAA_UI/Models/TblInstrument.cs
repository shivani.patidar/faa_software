﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblInstrument
{
    public int InstrumentId { get; set; }

    public string InstrumentName { get; set; }

    public sbyte CurInstrument { get; set; }

    public sbyte RgtPositions { get; set; }

    /// <summary>
    /// no of sample positions
    /// </summary>
    public sbyte SmpPositions { get; set; }

    public sbyte CuvPosition { get; set; }

    public sbyte CycleTime { get; set; }

    public sbyte EswVersion { get; set; }

    public sbyte AswVersion { get; set; }

    public sbyte IseModule { get; set; }

    /// <summary>
    /// no of overall cycles before wash
    /// </summary>
    public sbyte MaxReadCycles { get; set; }

    /// <summary>
    /// avalible number of filters
    /// </summary>
    public sbyte FilterCnt { get; set; }

    public string InstrumentSerialNo { get; set; }

    public sbyte InitPhotoCpos { get; set; }

    public sbyte? InitDryerCpos { get; set; }

    public sbyte InitR1Cpos { get; set; }

    public sbyte InitR2Cpos { get; set; }

    public sbyte InitR3Cpos { get; set; }

    public sbyte InitSmpCpos { get; set; }

    public sbyte InitStr1Cpos { get; set; }

    public sbyte InitStr2Cpos { get; set; }

    public sbyte InitStr3Cpos { get; set; }

    public sbyte SmpOutPosS { get; set; }

    public sbyte SmpMidPosS { get; set; }

    public sbyte SmpInrPosS { get; set; }

    public sbyte SmpOutPosE { get; set; }

    public sbyte SmpMidPosE { get; set; }

    public sbyte SmpInrPosE { get; set; }
}
