﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblDoctor
{
    public short DrId { get; set; }

    public int PersonId { get; set; }

    public short? HospitalId { get; set; }

    public string DispName { get; set; }

    public sbyte? CUid { get; set; }

    public sbyte? LastModUid { get; set; }

    public sbyte? DUid { get; set; }

    public sbyte IsActive { get; set; }
}
