﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace FAA_UI.Models;

public partial class DbFaaSoftwareContext : DbContext
{
    public DbFaaSoftwareContext()
    {
    }

    public DbFaaSoftwareContext(DbContextOptions<DbFaaSoftwareContext> options)
        : base(options)
    {
    }

    public virtual DbSet<AllImageUpload> AllImageUploads { get; set; }

    public virtual DbSet<TblAddress> TblAddresses { get; set; }

    public virtual DbSet<TblCalibration> TblCalibrations { get; set; }

    public virtual DbSet<TblCalibrationdet> TblCalibrationdets { get; set; }

    public virtual DbSet<TblCalibrationlot> TblCalibrationlots { get; set; }

    public virtual DbSet<TblCurvetype> TblCurvetypes { get; set; }

    public virtual DbSet<TblDeadvolume> TblDeadvolumes { get; set; }

    public virtual DbSet<TblDirectory> TblDirectories { get; set; }

    public virtual DbSet<TblDirectoryClinical> TblDirectoryClinicals { get; set; }

    public virtual DbSet<TblDirectoryDet> TblDirectoryDets { get; set; }

    public virtual DbSet<TblDirectoryQcmaterial> TblDirectoryQcmaterials { get; set; }

    public virtual DbSet<TblDirectoryQualitative> TblDirectoryQualitatives { get; set; }

    public virtual DbSet<TblDirectoryReagentbottletype> TblDirectoryReagentbottletypes { get; set; }

    public virtual DbSet<TblDirectoryResult> TblDirectoryResults { get; set; }

    public virtual DbSet<TblDirectorySamplecontainer> TblDirectorySamplecontainers { get; set; }

    public virtual DbSet<TblDoctor> TblDoctors { get; set; }

    public virtual DbSet<TblDoctordetail> TblDoctordetails { get; set; }

    public virtual DbSet<TblError> TblErrors { get; set; }

    public virtual DbSet<TblFlag> TblFlags { get; set; }

    public virtual DbSet<TblHospitalinfo> TblHospitalinfos { get; set; }

    public virtual DbSet<TblImage> TblImages { get; set; }

    public virtual DbSet<TblInstrument> TblInstruments { get; set; }

    public virtual DbSet<TblInstrumentAssmbly> TblInstrumentAssmblies { get; set; }

    public virtual DbSet<TblInstrumentCommunication> TblInstrumentCommunications { get; set; }

    public virtual DbSet<TblInstrumentSetting> TblInstrumentSettings { get; set; }

    public virtual DbSet<TblLogAction> TblLogActions { get; set; }

    public virtual DbSet<TblLogActionDet> TblLogActionDets { get; set; }

    public virtual DbSet<TblLogError> TblLogErrors { get; set; }

    public virtual DbSet<TblLogSetting> TblLogSettings { get; set; }

    public virtual DbSet<TblManufacturer> TblManufacturers { get; set; }

    public virtual DbSet<TblMastRole> TblMastRoles { get; set; }

    public virtual DbSet<TblMenuMapping> TblMenuMappings { get; set; }

    public virtual DbSet<TblMenuRoleAccess> TblMenuRoleAccesses { get; set; }

    public virtual DbSet<TblMethod> TblMethods { get; set; }

    public virtual DbSet<TblOperator> TblOperators { get; set; }

    public virtual DbSet<TblOperatorPermission> TblOperatorPermissions { get; set; }

    public virtual DbSet<TblPatient> TblPatients { get; set; }

    public virtual DbSet<TblProfileDet> TblProfileDets { get; set; }

    public virtual DbSet<TblQclot> TblQclots { get; set; }

    public virtual DbSet<TblQclotDet> TblQclotDets { get; set; }

    public virtual DbSet<TblReadsBlank> TblReadsBlanks { get; set; }

    public virtual DbSet<TblReadsResult> TblReadsResults { get; set; }

    public virtual DbSet<TblRefRange> TblRefRanges { get; set; }

    public virtual DbSet<TblResult> TblResults { get; set; }

    public virtual DbSet<TblResultsExtern> TblResultsExterns { get; set; }

    public virtual DbSet<TblRgtbotle> TblRgtbotles { get; set; }

    public virtual DbSet<TblRgtlot> TblRgtlots { get; set; }

    public virtual DbSet<TblRgtpo> TblRgtpos { get; set; }

    public virtual DbSet<TblSample> TblSamples { get; set; }

    public virtual DbSet<TblSampleTest> TblSampleTests { get; set; }

    public virtual DbSet<TblSampletype> TblSampletypes { get; set; }

    public virtual DbSet<TblSchedule> TblSchedules { get; set; }

    public virtual DbSet<TblScheduledet> TblScheduledets { get; set; }

    public virtual DbSet<TblSystemSetting> TblSystemSettings { get; set; }

    public virtual DbSet<TblTest> TblTests { get; set; }

    public virtual DbSet<TblTestCalcu> TblTestCalcus { get; set; }

    public virtual DbSet<TblTestCalib> TblTestCalibs { get; set; }

    public virtual DbSet<TblTestCarryOver> TblTestCarryOvers { get; set; }

    public virtual DbSet<TblTestDetail> TblTestDetails { get; set; }

    public virtual DbSet<TblTestExtern> TblTestExterns { get; set; }

    public virtual DbSet<TblTestFilter> TblTestFilters { get; set; }

    public virtual DbSet<TblTestProfile> TblTestProfiles { get; set; }

    public virtual DbSet<TblTestReflex> TblTestReflexes { get; set; }

    public virtual DbSet<TblTestReflexDet> TblTestReflexDets { get; set; }

    public virtual DbSet<TblTestVolume> TblTestVolumes { get; set; }

    public virtual DbSet<TblUnit> TblUnits { get; set; }

    public virtual DbSet<TblUser> TblUsers { get; set; }

    public virtual DbSet<TblUsermaster> TblUsermasters { get; set; }

    public virtual DbSet<TblUserright> TblUserrights { get; set; }

    public virtual DbSet<TblWorklist> TblWorklists { get; set; }

    //  protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
    //      => optionsBuilder.UseMySQL("server=localhost;port=3306;user=root;password=Biosense23;database=db_faa_software");

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();
        optionsBuilder.UseMySQL(configuration.GetConnectionString("AppConnection"));
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AllImageUpload>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("all_image_upload");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ImageFile).HasColumnName("image_file");
        });

        modelBuilder.Entity<TblAddress>(entity =>
        {
            entity.HasKey(e => e.AddId).HasName("PRIMARY");

            entity.ToTable("tbl_address");

            entity.Property(e => e.AddId)
                .HasColumnType("mediumint")
                .HasColumnName("Add_ID");
            entity.Property(e => e.AddCity)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Add_City");
            entity.Property(e => e.AddCountry)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Add_Country");
            entity.Property(e => e.AddLine1)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnName("Add_Line1");
            entity.Property(e => e.AddLine2)
                .HasMaxLength(40)
                .HasColumnName("Add_Line2");
            entity.Property(e => e.AddLine3)
                .HasMaxLength(30)
                .HasColumnName("Add_Line3");
            entity.Property(e => e.AddPin)
                .HasColumnType("mediumint")
                .HasColumnName("Add_Pin");
            entity.Property(e => e.AddState)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Add_State");
            entity.Property(e => e.CUid).HasColumnName("C_UID");
            entity.Property(e => e.DUid).HasColumnName("D_UID");
            entity.Property(e => e.LastModUid).HasColumnName("LastMod_UID");
        });

        modelBuilder.Entity<TblCalibration>(entity =>
        {
            entity.HasKey(e => e.CalId).HasName("PRIMARY");

            entity.ToTable("tbl_calibration");

            entity.Property(e => e.CalId)
                .HasColumnType("mediumint")
                .HasColumnName("Cal_ID");
            entity.Property(e => e.CalDate)
                .HasColumnType("date")
                .HasColumnName("Cal_Date");
            entity.Property(e => e.CalExpiry)
                .HasColumnType("date")
                .HasColumnName("Cal_Expiry");
            entity.Property(e => e.CalStatus).HasColumnName("Cal_Status");
            entity.Property(e => e.CalTime)
                .HasColumnType("time")
                .HasColumnName("Cal_Time");
            entity.Property(e => e.CalValidity)
                .HasColumnType("date")
                .HasColumnName("Cal_Validity");
            entity.Property(e => e.CalValidityDays).HasColumnName("Cal_Validity_Days");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.PrevCalIdRgtBlnking)
                .HasColumnType("mediumint")
                .HasColumnName("PrevCalID_RgtBlnking");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
        });

        modelBuilder.Entity<TblCalibrationdet>(entity =>
        {
            entity.HasKey(e => e.CalDetId).HasName("PRIMARY");

            entity.ToTable("tbl_calibrationdet");

            entity.HasIndex(e => e.CalId, "Cal_ID_idx");

            entity.Property(e => e.CalDetId).HasColumnName("CalDet_Id");
            entity.Property(e => e.ActualOd).HasColumnName("ActualOD");
            entity.Property(e => e.CalId)
                .HasColumnType("mediumint")
                .HasColumnName("Cal_ID");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.Od).HasColumnName("OD");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblCalibrationlot>(entity =>
        {
            entity.HasKey(e => e.CalibLotId).HasName("PRIMARY");

            entity.ToTable("tbl_calibrationlot");

            entity.Property(e => e.CalibLotId).HasColumnName("CalibLot_Id");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ExpMonth).HasColumnName("EXP_Month");
            entity.Property(e => e.ExpYear).HasColumnName("EXP_Year");
            entity.Property(e => e.FactorMax).HasColumnName("Factor_Max");
            entity.Property(e => e.FactorMin).HasColumnName("Factor_Min");
            entity.Property(e => e.LotNo)
                .IsRequired()
                .HasMaxLength(45);
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.TestId).HasColumnName("Test_Id");
        });

        modelBuilder.Entity<TblCurvetype>(entity =>
        {
            entity.HasKey(e => e.CrvTyId).HasName("PRIMARY");

            entity.ToTable("tbl_curvetype");

            entity.Property(e => e.CrvTyId).HasColumnName("CrvTy_Id");
            entity.Property(e => e.CrvType)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("Crv_Type");
        });

        modelBuilder.Entity<TblDeadvolume>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("tbl_deadvolume");

            entity.Property(e => e.DictItemId).HasColumnName("Dict_ItemID");
            entity.Property(e => e.DvTime)
                .HasColumnType("datetime")
                .HasColumnName("DV_TIME");
            entity.Property(e => e.DvVolume)
                .HasColumnType("mediumint")
                .HasColumnName("DV_Volume");
            entity.Property(e => e.Steps).HasColumnType("mediumint");
            entity.Property(e => e.Uid).HasColumnName("UID");
        });

        modelBuilder.Entity<TblDirectory>(entity =>
        {
            entity.HasKey(e => e.DictId).HasName("PRIMARY");

            entity.ToTable("tbl_directory");

            entity.Property(e => e.DictId).HasColumnName("Dict_ID");
            entity.Property(e => e.CUid).HasColumnName("C_UID");
            entity.Property(e => e.DUid).HasColumnName("D_UID");
            entity.Property(e => e.DictDesc)
                .HasMaxLength(20)
                .HasColumnName("Dict_Desc");
            entity.Property(e => e.DictRemark)
                .HasMaxLength(15)
                .HasColumnName("Dict_Remark");
            entity.Property(e => e.LastModUid).HasColumnName("LastMod_UID");
        });

        modelBuilder.Entity<TblDirectoryClinical>(entity =>
        {
            entity.HasKey(e => e.ClnId).HasName("PRIMARY");

            entity.ToTable("tbl_directory_clinical");

            entity.Property(e => e.ClnId).HasColumnName("Cln_Id");
            entity.Property(e => e.ClnName)
                .IsRequired()
                .HasMaxLength(500)
                .HasColumnName("Cln_Name");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblDirectoryDet>(entity =>
        {
            entity.HasKey(e => e.DictItemId).HasName("PRIMARY");

            entity.ToTable("tbl_directory_det");

            entity.Property(e => e.DictItemId).HasColumnName("Dict_ItemID");
            entity.Property(e => e.CUid).HasColumnName("C_UID");
            entity.Property(e => e.DUid).HasColumnName("D_UID");
            entity.Property(e => e.DictId).HasColumnName("Dict_ID");
            entity.Property(e => e.DictItem)
                .IsRequired()
                .HasMaxLength(30)
                .HasColumnName("Dict_Item");
            entity.Property(e => e.DictItemDesc)
                .HasMaxLength(30)
                .HasColumnName("Dict_Item_Desc");
            entity.Property(e => e.ItemValue).HasMaxLength(25);
            entity.Property(e => e.LastModUid).HasColumnName("LastMod_UID");
        });

        modelBuilder.Entity<TblDirectoryQcmaterial>(entity =>
        {
            entity.HasKey(e => e.QcMtrlId).HasName("PRIMARY");

            entity.ToTable("tbl_directory_qcmaterial");

            entity.Property(e => e.QcMtrlId).HasColumnName("QcMtrl_Id");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.QcMtrlName)
                .IsRequired()
                .HasMaxLength(500)
                .HasColumnName("QcMtrl_Name");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblDirectoryQualitative>(entity =>
        {
            entity.HasKey(e => e.QualtId).HasName("PRIMARY");

            entity.ToTable("tbl_directory_qualitative");

            entity.Property(e => e.QualtId).HasColumnName("Qualt_Id");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.QualtName)
                .IsRequired()
                .HasMaxLength(500)
                .HasColumnName("Qualt_Name");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblDirectoryReagentbottletype>(entity =>
        {
            entity.HasKey(e => e.RgntBtlId).HasName("PRIMARY");

            entity.ToTable("tbl_directory_reagentbottletype");

            entity.Property(e => e.RgntBtlId).HasColumnName("RgntBtl_Id");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.RgntBtlSize).HasColumnName("RgntBtl_Size");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblDirectoryResult>(entity =>
        {
            entity.HasKey(e => e.RsultId).HasName("PRIMARY");

            entity.ToTable("tbl_directory_result");

            entity.Property(e => e.RsultId).HasColumnName("Rsult_Id");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.RsultName)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("Rsult_Name");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblDirectorySamplecontainer>(entity =>
        {
            entity.HasKey(e => e.SmpConId).HasName("PRIMARY");

            entity.ToTable("tbl_directory_samplecontainer");

            entity.Property(e => e.SmpConId).HasColumnName("SmpCon_Id");
            entity.Property(e => e.CreateBy)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.SmpConName)
                .IsRequired()
                .HasMaxLength(500)
                .HasColumnName("SmpCon_Name");
            entity.Property(e => e.SoftDelete).HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblDoctor>(entity =>
        {
            entity.HasKey(e => e.DrId).HasName("PRIMARY");

            entity.ToTable("tbl_doctor");

            entity.Property(e => e.DrId).HasColumnName("DR_ID");
            entity.Property(e => e.CUid).HasColumnName("C_UID");
            entity.Property(e => e.DUid).HasColumnName("D_UID");
            entity.Property(e => e.DispName).HasMaxLength(20);
            entity.Property(e => e.HospitalId).HasColumnName("Hospital_ID");
            entity.Property(e => e.IsActive).HasDefaultValueSql("'1'");
            entity.Property(e => e.LastModUid).HasColumnName("LastMod_UID");
            entity.Property(e => e.PersonId)
                .HasColumnType("mediumint")
                .HasColumnName("Person_ID");
        });

        modelBuilder.Entity<TblDoctordetail>(entity =>
        {
            entity.HasKey(e => e.DrId).HasName("PRIMARY");

            entity.ToTable("tbl_doctordetail");

            entity.Property(e => e.DrId).HasColumnName("Dr_Id");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.DrDepartment)
                .HasMaxLength(500)
                .HasColumnName("Dr_Department");
            entity.Property(e => e.DrName)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("Dr_Name");
            entity.Property(e => e.DrRemark)
                .HasMaxLength(2000)
                .HasColumnName("Dr_Remark");
            entity.Property(e => e.MobileNo).HasMaxLength(45);
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblError>(entity =>
        {
            entity.HasKey(e => e.ErrorId).HasName("PRIMARY");

            entity.ToTable("tbl_errors");

            entity.Property(e => e.ErrorId).HasColumnName("Error_ID");
            entity.Property(e => e.AssemblyId).HasColumnName("Assembly_ID");
            entity.Property(e => e.DisplayToUser)
                .HasDefaultValueSql("'1'")
                .HasComment("1 . If error displayed to user.\n");
            entity.Property(e => e.ErrorAction)
                .HasMaxLength(45)
                .HasColumnName("Error_Action");
            entity.Property(e => e.ErrorCode)
                .HasMaxLength(5)
                .HasColumnName("Error_Code");
            entity.Property(e => e.ErrorDesc)
                .HasMaxLength(50)
                .HasColumnName("Error_Desc");
            entity.Property(e => e.ErrorFlag)
                .HasMaxLength(5)
                .HasColumnName("Error_Flag");
            entity.Property(e => e.InstrumentId).HasColumnName("Instrument_ID");
            entity.Property(e => e.IsStopError)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Is_StopError");
            entity.Property(e => e.LogError)
                .HasDefaultValueSql("'1'")
                .HasComment("1 if need to log this error in Database else 0")
                .HasColumnName("Log_Error");
            entity.Property(e => e.RecoverTryCnt)
                .HasComment("No of times software should try to auto recover due to this error\n")
                .HasColumnName("Recover_Try_Cnt");
        });

        modelBuilder.Entity<TblFlag>(entity =>
        {
            entity.HasKey(e => e.FlagId).HasName("PRIMARY");

            entity.ToTable("tbl_flags");

            entity.Property(e => e.FlagId).HasColumnName("Flag_ID");
            entity.Property(e => e.FlagCode)
                .HasMaxLength(7)
                .HasColumnName("Flag_Code");
            entity.Property(e => e.FlagMsg)
                .HasMaxLength(50)
                .HasColumnName("Flag_Msg");
            entity.Property(e => e.FlagType)
                .HasComment("1. Absent\n2. Instrument_Warning.\n3. Normal_Warning\n4..RESULT\n5.  Instrument_Error\n ")
                .HasColumnName("Flag_Type");
            entity.Property(e => e.TblFlagscol)
                .HasMaxLength(45)
                .HasColumnName("Tbl_Flagscol");
        });

        modelBuilder.Entity<TblHospitalinfo>(entity =>
        {
            entity.HasKey(e => e.HstlId).HasName("PRIMARY");

            entity.ToTable("tbl_hospitalinfo");

            entity.Property(e => e.HstlId).HasColumnName("Hstl_Id");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.HstlAddress)
                .HasMaxLength(1000)
                .HasColumnName("Hstl_Address");
            entity.Property(e => e.HstlName)
                .IsRequired()
                .HasMaxLength(500)
                .HasColumnName("Hstl_Name");
            entity.Property(e => e.HstlPhoneNo)
                .HasMaxLength(45)
                .HasColumnName("Hstl_PhoneNo");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblImage>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("tbl_image");

            entity.Property(e => e.Id).HasColumnName("ID");
            entity.Property(e => e.Img)
                .IsRequired()
                .HasMaxLength(5);
        });

        modelBuilder.Entity<TblInstrument>(entity =>
        {
            entity.HasKey(e => e.InstrumentId).HasName("PRIMARY");

            entity.ToTable("tbl_instrument");

            entity.Property(e => e.InstrumentId).HasColumnName("Instrument_ID");
            entity.Property(e => e.AswVersion).HasColumnName("ASW_Version");
            entity.Property(e => e.CurInstrument).HasColumnName("Cur_Instrument");
            entity.Property(e => e.CuvPosition).HasColumnName("Cuv_Position");
            entity.Property(e => e.CycleTime).HasColumnName("Cycle_Time");
            entity.Property(e => e.EswVersion).HasColumnName("ESW_Version");
            entity.Property(e => e.FilterCnt)
                .HasComment("avalible number of filters")
                .HasColumnName("Filter_Cnt");
            entity.Property(e => e.InitDryerCpos).HasColumnName("Init_Dryer_CPos");
            entity.Property(e => e.InitPhotoCpos).HasColumnName("Init_Photo_CPos");
            entity.Property(e => e.InitR1Cpos).HasColumnName("Init_R1_CPos");
            entity.Property(e => e.InitR2Cpos).HasColumnName("Init_R2_CPos");
            entity.Property(e => e.InitR3Cpos).HasColumnName("Init_R3_CPos");
            entity.Property(e => e.InitSmpCpos).HasColumnName("Init_Smp_CPos");
            entity.Property(e => e.InitStr1Cpos).HasColumnName("Init_Str1_CPos");
            entity.Property(e => e.InitStr2Cpos).HasColumnName("Init_Str2_CPos");
            entity.Property(e => e.InitStr3Cpos).HasColumnName("Init_Str3_CPos");
            entity.Property(e => e.InstrumentName)
                .IsRequired()
                .HasMaxLength(20)
                .HasColumnName("Instrument_Name");
            entity.Property(e => e.InstrumentSerialNo)
                .HasMaxLength(15)
                .HasColumnName("Instrument_Serial_no");
            entity.Property(e => e.IseModule).HasColumnName("ISE_MODULE");
            entity.Property(e => e.MaxReadCycles)
                .HasComment("no of overall cycles before wash")
                .HasColumnName("Max_Read_Cycles");
            entity.Property(e => e.RgtPositions).HasColumnName("Rgt_Positions");
            entity.Property(e => e.SmpInrPosE).HasColumnName("Smp_Inr_Pos_E");
            entity.Property(e => e.SmpInrPosS).HasColumnName("Smp_Inr_Pos_S");
            entity.Property(e => e.SmpMidPosE).HasColumnName("Smp_Mid_Pos_E");
            entity.Property(e => e.SmpMidPosS).HasColumnName("Smp_Mid_Pos_S");
            entity.Property(e => e.SmpOutPosE).HasColumnName("Smp_Out_Pos_E");
            entity.Property(e => e.SmpOutPosS).HasColumnName("Smp_Out_Pos_S");
            entity.Property(e => e.SmpPositions)
                .HasComment("no of sample positions")
                .HasColumnName("Smp_positions");
        });

        modelBuilder.Entity<TblInstrumentAssmbly>(entity =>
        {
            entity.HasKey(e => e.AssemblyId).HasName("PRIMARY");

            entity.ToTable("tbl_instrument_assmbly");

            entity.Property(e => e.AssemblyId).HasColumnName("Assembly_ID");
            entity.Property(e => e.Assembly).HasMaxLength(20);
            entity.Property(e => e.AswCode).HasColumnName("ASW_CODE");
            entity.Property(e => e.AswName)
                .HasMaxLength(10)
                .HasColumnName("ASW_NAME");
            entity.Property(e => e.EswCode).HasColumnName("ESW_CODE");
            entity.Property(e => e.InstalledDate).HasColumnType("datetime");
            entity.Property(e => e.InstrumentId).HasColumnName("Instrument_ID");
            entity.Property(e => e.IsActive).HasDefaultValueSql("'1'");
            entity.Property(e => e.Uid)
                .HasDefaultValueSql("'1'")
                .HasColumnName("UID");
        });

        modelBuilder.Entity<TblInstrumentCommunication>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("tbl_instrument_communication");

            entity.HasIndex(e => e.InstrumentId, "Instrument_ID_UNIQUE").IsUnique();

            entity.Property(e => e.ComPort).HasColumnName("Com_Port");
            entity.Property(e => e.ComType)
                .HasComment("1 Serial Port\n2 TCP/IP\n")
                .HasColumnName("Com_Type");
            entity.Property(e => e.InstrumentId).HasColumnName("Instrument_ID");
            entity.Property(e => e.LisAck).HasColumnName("LIS_ACK");
            entity.Property(e => e.LisBaudRate).HasColumnName("LIS_BaudRate");
            entity.Property(e => e.LisComPort).HasColumnName("LIS_Com_Port");
            entity.Property(e => e.LisComType).HasColumnName("LIS_Com_Type");
            entity.Property(e => e.LisDataType)
                .HasComment("1- Single Packet at a time\n2. Multiple Packets at a time\n \n\n")
                .HasColumnName("LIS_Data_Type");
            entity.Property(e => e.LisDelimeter).HasColumnName("LIS_DELIMETER");
            entity.Property(e => e.LisEnabled)
                .HasComment("1- if LIS Settings are ON\n0- if LIS Settings are OFF")
                .HasColumnName("LIS_Enabled");
            entity.Property(e => e.LisEnq).HasColumnName("LIS_ENQ");
            entity.Property(e => e.LisEot).HasColumnName("LIS_EOT");
            entity.Property(e => e.LisEtx).HasColumnName("LIS_ETX");
            entity.Property(e => e.LisNack).HasColumnName("LIS_NACK");
            entity.Property(e => e.LisPacketSeperator).HasColumnName("LIS_PACKET_SEPERATOR");
            entity.Property(e => e.LisParmeterSeperator).HasColumnName("LIS_PARMETER_SEPERATOR");
            entity.Property(e => e.LisServerIp)
                .HasMaxLength(15)
                .HasColumnName("LIS_Server_IP");
            entity.Property(e => e.LisSot).HasColumnName("LIS_SOT");
            entity.Property(e => e.LisStx).HasColumnName("LIS_STX");
            entity.Property(e => e.LisTcpPort).HasColumnName("LIS_TCP_Port");
        });

        modelBuilder.Entity<TblInstrumentSetting>(entity =>
        {
            entity.HasKey(e => e.InstrumentSettingsId).HasName("PRIMARY");

            entity.ToTable("tbl_instrument_settings");

            entity.Property(e => e.InstrumentSettingsId).HasColumnName("Instrument_Settings_ID");
            entity.Property(e => e.AddWaterTime).HasColumnName("Add_Water_Time");
            entity.Property(e => e.ArmDepthCuv).HasColumnName("Arm_Depth_Cuv");
            entity.Property(e => e.ArmDepthHome).HasColumnName("Arm_Depth_Home");
            entity.Property(e => e.ArmDepthRgtIn).HasColumnName("Arm_Depth_RGT_In");
            entity.Property(e => e.ArmDepthRgtOut).HasColumnName("Arm_Depth_RGT_Out");
            entity.Property(e => e.ArmPosCuv).HasColumnName("Arm_Pos_Cuv");
            entity.Property(e => e.ArmPosHome).HasColumnName("Arm_Pos_Home");
            entity.Property(e => e.ArmPosRgtIn).HasColumnName("Arm_Pos_RGT_In");
            entity.Property(e => e.ArmPosRgtOut).HasColumnName("Arm_Pos_RGT_Out");
            entity.Property(e => e.ArmPosRgtSmp).HasColumnName("Arm_Pos_RGT_Smp");
            entity.Property(e => e.CruDepthWash).HasColumnName("Cru_Depth_Wash");
            entity.Property(e => e.CruDepthWater).HasColumnName("Cru_Depth_Water");
            entity.Property(e => e.InstrumentId).HasColumnName("Instrument_ID");
            entity.Property(e => e.PathLength)
                .HasPrecision(2)
                .HasDefaultValueSql("'0.65'")
                .HasColumnName("Path_Length");
            entity.Property(e => e.RemoveWaterTime).HasColumnName("Remove_Water_Time");
            entity.Property(e => e.RgtPosIn).HasColumnName("Rgt_Pos_In");
            entity.Property(e => e.RgtPosOut).HasColumnName("Rgt_Pos_Out");
            entity.Property(e => e.RgtPosSmp).HasColumnName("Rgt_Pos_Smp");
            entity.Property(e => e.RgtScanIn).HasColumnName("Rgt_Scan_In");
            entity.Property(e => e.RgtScanOut).HasColumnName("Rgt_Scan_Out");
            entity.Property(e => e.RgtScanSmp).HasColumnName("Rgt_Scan_Smp");
            entity.Property(e => e.StrDepthCuv).HasColumnName("Str_Depth_Cuv");
            entity.Property(e => e.StrDepthHome).HasColumnName("Str_Depth_Home");
            entity.Property(e => e.StrPosCuv).HasColumnName("Str_Pos_Cuv");
            entity.Property(e => e.StrPosHome).HasColumnName("Str_Pos_Home");
            entity.Property(e => e.StrSpeed).HasColumnName("Str_Speed");
            entity.Property(e => e.SyringeOffset).HasColumnName("Syringe_Offset");
        });

        modelBuilder.Entity<TblLogAction>(entity =>
        {
            entity.HasKey(e => e.LogAId).HasName("PRIMARY");

            entity.ToTable("tbl_log_action");

            entity.Property(e => e.LogAId).HasColumnName("LogA_ID");
            entity.Property(e => e.ADate)
                .HasColumnType("date")
                .HasColumnName("A_Date");
            entity.Property(e => e.ATime)
                .HasColumnType("time")
                .HasColumnName("A_Time");
            entity.Property(e => e.Action).HasMaxLength(20);
            entity.Property(e => e.Functionality).HasMaxLength(20);
            entity.Property(e => e.LogAType)
                .HasComment("1. USER_LOGIN\n2. USER_LOGOUT\n3. SAVE\n4. UPDATE\n5. DELETE\n6. SERVICE_CHECK\n7. MAINTENACE\n8..AUTOMAINTENANCE\n9. OPEN_Window\n10.CLOSE_WINDOW\n11. WARNING\n12. ")
                .HasColumnName("LogA_Type");
            entity.Property(e => e.UId)
                .HasMaxLength(45)
                .HasColumnName("U_ID");
        });

        modelBuilder.Entity<TblLogActionDet>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("tbl_log_action_det");

            entity.Property(e => e.ColumnName)
                .HasMaxLength(30)
                .HasColumnName("Column_Name");
            entity.Property(e => e.LogAId).HasColumnName("LogA_ID");
            entity.Property(e => e.NwValue)
                .HasMaxLength(30)
                .HasColumnName("NW_VALUE");
            entity.Property(e => e.OldValue)
                .HasMaxLength(30)
                .HasColumnName("OLD_VALUE");
        });

        modelBuilder.Entity<TblLogError>(entity =>
        {
            entity.HasKey(e => e.ErrorId).HasName("PRIMARY");

            entity.ToTable("tbl_log_error");

            entity.Property(e => e.ErrorId).HasColumnName("Error_ID");
            entity.Property(e => e.Action)
                .HasMaxLength(30)
                .HasComment("armrotate/updn/rgt rotate");
            entity.Property(e => e.Assembly)
                .HasMaxLength(30)
                .HasComment("ARM/TRAY/ISE/STR/CRU");
            entity.Property(e => e.ErrDate)
                .HasColumnType("date")
                .HasColumnName("Err_Date");
            entity.Property(e => e.ErrTime)
                .HasColumnType("time")
                .HasColumnName("Err_Time");
            entity.Property(e => e.ErrorCode).HasColumnName("Error_Code");
            entity.Property(e => e.ErrorDuring)
                .HasComment("1. AUTO MAINTENANCE_STARTUP\n2. AUTO MAINTENANCE_SHUTDOWN\n3. SERVICE_CHECK\n4. MAINTENANCE\n5.PRE_RUN\n6. RUN\n7.POST_RUN\n8.USER_ACTIONS(Barcode-scan/volume-check)")
                .HasColumnName("Error_During");
            entity.Property(e => e.ForCuv).HasComment("For which CUV(STR/ARM HIT)");
            entity.Property(e => e.ForPosition).HasComment("RGT/SMP Pos");
            entity.Property(e => e.Functionality)
                .HasMaxLength(30)
                .HasComment("RUN/CuvWash/tempsetting/servicecheck/preobe-clean");
            entity.Property(e => e.UId).HasColumnName("U_ID");
        });

        modelBuilder.Entity<TblLogSetting>(entity =>
        {
            entity.HasKey(e => e.SetId).HasName("PRIMARY");

            entity.ToTable("tbl_log_settings");

            entity.Property(e => e.SetId).HasColumnName("Set_ID");
            entity.Property(e => e.AssemblyId)
                .HasComment("ARM/TRAY/ISE/STR/CRU")
                .HasColumnName("Assembly_ID");
            entity.Property(e => e.NewValue).HasColumnName("New_Value");
            entity.Property(e => e.OldValue).HasColumnName("Old_Value");
            entity.Property(e => e.Path).HasColumnName("PATH");
            entity.Property(e => e.SetDate)
                .HasColumnType("date")
                .HasColumnName("Set_Date");
            entity.Property(e => e.SetTime)
                .HasColumnType("time")
                .HasColumnName("Set_Time");
            entity.Property(e => e.UId).HasColumnName("U_ID");
        });

        modelBuilder.Entity<TblManufacturer>(entity =>
        {
            entity.HasKey(e => e.MnfId).HasName("PRIMARY");

            entity.ToTable("tbl_manufacturer", tb => tb.HasComment("manufacturer details"));

            entity.Property(e => e.MnfId).HasColumnName("Mnf_Id");
            entity.Property(e => e.CreatedBy).HasColumnName("Created_By");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("Created_Date");
            entity.Property(e => e.MnfCali)
                .HasMaxLength(50)
                .HasColumnName("Mnf_Cali");
            entity.Property(e => e.MnfName)
                .HasMaxLength(200)
                .HasColumnName("Mnf_Name");
            entity.Property(e => e.MnfQc)
                .HasMaxLength(50)
                .HasColumnName("Mnf_Qc");
            entity.Property(e => e.MnfReg)
                .HasMaxLength(50)
                .HasColumnName("Mnf_Reg");
            entity.Property(e => e.MnfType)
                .HasMaxLength(5)
                .HasColumnName("Mnf_Type");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.SoftDelete).HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblMastRole>(entity =>
        {
            entity.HasKey(e => e.RoleId).HasName("PRIMARY");

            entity.ToTable("tbl_mast_role");

            entity.Property(e => e.RoleId).HasColumnName("role_id");
            entity.Property(e => e.CreatedBy)
                .HasMaxLength(50)
                .HasColumnName("created_by");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.ModifiedBy)
                .HasMaxLength(50)
                .HasColumnName("modified_by");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.Role)
                .HasMaxLength(100)
                .HasColumnName("role");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("soft_delete");
        });

        modelBuilder.Entity<TblMenuMapping>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("tbl_menu_mapping");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Icon)
                .HasMaxLength(100)
                .HasColumnName("icon");
            entity.Property(e => e.OrderId).HasColumnName("order_id");
            entity.Property(e => e.Page)
                .HasMaxLength(100)
                .HasColumnName("page");
            entity.Property(e => e.ParentId)
                .HasDefaultValueSql("'0'")
                .HasColumnName("parent_id");
            entity.Property(e => e.SubPage)
                .HasMaxLength(100)
                .HasColumnName("sub_page");
        });

        modelBuilder.Entity<TblMenuRoleAccess>(entity =>
        {
            entity.HasKey(e => e.AccessId).HasName("PRIMARY");

            entity.ToTable("tbl_menu_role_access");

            entity.HasIndex(e => e.MenuMappingId, "Menu_Mapping_Id_idx");

            entity.HasIndex(e => e.RoleId, "RoleId_idx");

            entity.Property(e => e.AccessId).HasColumnName("Access_Id");
            entity.Property(e => e.CreatedDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Created_Date");
            entity.Property(e => e.MenuMappingId).HasColumnName("Menu_Mapping_Id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("Modified_Date");

            entity.HasOne(d => d.MenuMapping).WithMany(p => p.TblMenuRoleAccesses)
                .HasForeignKey(d => d.MenuMappingId)
                .HasConstraintName("Menu_Mapping_Id");

            entity.HasOne(d => d.Role).WithMany(p => p.TblMenuRoleAccesses)
                .HasForeignKey(d => d.RoleId)
                .HasConstraintName("RoleId");
        });

        modelBuilder.Entity<TblMethod>(entity =>
        {
            entity.HasKey(e => e.MthdId).HasName("PRIMARY");

            entity.ToTable("tbl_method");

            entity.Property(e => e.MthdId).HasColumnName("Mthd_Id");
            entity.Property(e => e.MthdType)
                .IsRequired()
                .HasMaxLength(500)
                .HasColumnName("Mthd_Type");
        });

        modelBuilder.Entity<TblOperator>(entity =>
        {
            entity.HasKey(e => e.OprId).HasName("PRIMARY");

            entity.ToTable("tbl_operator");

            entity.Property(e => e.OprId).HasColumnName("Opr_Id");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.LoginTime)
                .HasColumnType("timestamp(6)")
                .HasColumnName("Login_Time");
            entity.Property(e => e.LogoutTime)
                .HasColumnType("timestamp(6)")
                .HasColumnName("Logout_Time");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.OprEmail)
                .HasMaxLength(200)
                .HasColumnName("Opr_Email");
            entity.Property(e => e.OprImage).HasColumnName("Opr_Image");
            entity.Property(e => e.OprLoginId)
                .HasMaxLength(200)
                .HasColumnName("Opr_LoginId");
            entity.Property(e => e.OprMobileNo)
                .HasMaxLength(100)
                .HasColumnName("Opr_MobileNo");
            entity.Property(e => e.OprName)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("Opr_Name");
            entity.Property(e => e.OprPassword)
                .HasMaxLength(100)
                .HasColumnName("Opr_Password");
            entity.Property(e => e.OprRoleId).HasColumnName("Opr_RoleId");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblOperatorPermission>(entity =>
        {
            entity.HasKey(e => e.OperId).HasName("PRIMARY");

            entity.ToTable("tbl_operator_permission");

            entity.Property(e => e.OperId).HasColumnName("OPer_Id");
            entity.Property(e => e.OperName)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("OPer_Name");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblPatient>(entity =>
        {
            entity.HasKey(e => e.PatId).HasName("PRIMARY");

            entity.ToTable("tbl_patient");

            entity.Property(e => e.PatId)
                .HasColumnType("mediumint")
                .HasColumnName("Pat_ID");
            entity.Property(e => e.Address).HasMaxLength(85);
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime");
            entity.Property(e => e.Dob)
                .HasColumnType("datetime")
                .HasColumnName("DOB");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.PatName)
                .HasMaxLength(20)
                .HasColumnName("Pat_Name");
            entity.Property(e => e.PatientId).HasMaxLength(30);
            entity.Property(e => e.Sex)
                .IsRequired()
                .HasMaxLength(10);
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblProfileDet>(entity =>
        {
            entity.HasKey(e => e.ProDetId).HasName("PRIMARY");

            entity.ToTable("tbl_profile_det");

            entity.HasIndex(e => e.TestId, "Test_ID_idx");

            entity.Property(e => e.ProDetId).HasColumnName("ProDet_Id");
            entity.Property(e => e.ProId).HasColumnName("Pro_ID");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");

            entity.HasOne(d => d.Test).WithMany(p => p.TblProfileDets)
                .HasForeignKey(d => d.TestId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Test_ID");
        });

        modelBuilder.Entity<TblQclot>(entity =>
        {
            entity.HasKey(e => e.LotId).HasName("PRIMARY");

            entity.ToTable("tbl_qclot");

            entity.Property(e => e.LotId).HasColumnName("Lot_ID");
            entity.Property(e => e.Conc).HasColumnName("CONC");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ExpMonth).HasColumnName("EXP_Month");
            entity.Property(e => e.ExpYear).HasColumnName("EXP_Year");
            entity.Property(e => e.LabMean).HasColumnName("Lab_Mean");
            entity.Property(e => e.LabSd).HasColumnName("Lab_SD");
            entity.Property(e => e.LotNo)
                .IsRequired()
                .HasMaxLength(20);
            entity.Property(e => e.Mean).HasColumnName("MEAN");
            entity.Property(e => e.MfgId).HasColumnName("MFG_ID");
            entity.Property(e => e.MfgMonth).HasColumnName("MFG_Month");
            entity.Property(e => e.MfgYear).HasColumnName("MFG_Year");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.Od).HasColumnName("OD");
            entity.Property(e => e.Sd).HasColumnName("SD");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.StabilityEnd).HasColumnType("date");
        });

        modelBuilder.Entity<TblQclotDet>(entity =>
        {
            entity.HasKey(e => e.QcdetId).HasName("PRIMARY");

            entity.ToTable("tbl_qclot_det");

            entity.Property(e => e.QcdetId).HasColumnName("QCDetId");
            entity.Property(e => e.Conc).HasColumnName("CONC");
            entity.Property(e => e.LotId)
                .HasColumnType("mediumint")
                .HasColumnName("Lot_ID");
            entity.Property(e => e.StdOd).HasColumnName("STD_OD");
            entity.Property(e => e.TestId).HasColumnName("Test_Id");
        });

        modelBuilder.Entity<TblReadsBlank>(entity =>
        {
            entity.HasKey(e => new { e.CuvNo, e.FilterId }).HasName("PRIMARY");

            entity.ToTable("tbl_reads_blank");

            entity.Property(e => e.FilterId).HasColumnName("Filter_ID");
            entity.Property(e => e.Rcount).HasColumnName("RCount");
            entity.Property(e => e.ReadStatus)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Read_Status");
            entity.Property(e => e.ReadTime).HasColumnType("datetime");
        });

        modelBuilder.Entity<TblReadsResult>(entity =>
        {
            entity.HasKey(e => e.ReadId).HasName("PRIMARY");

            entity.ToTable("tbl_reads_results");

            entity.Property(e => e.ReadId)
                .HasColumnType("mediumint")
                .HasColumnName("Read_ID");
            entity.Property(e => e.BlankCount)
                .HasColumnType("mediumint")
                .HasColumnName("Blank_Count");
            entity.Property(e => e.BlankOd).HasColumnName("Blank_OD");
            entity.Property(e => e.InstrumentId).HasColumnName("Instrument_ID");
            entity.Property(e => e.IsActive).HasDefaultValueSql("'1'");
            entity.Property(e => e.ReadCount).HasColumnName("Read_Count");
        });

        modelBuilder.Entity<TblRefRange>(entity =>
        {
            entity.HasKey(e => e.RefId).HasName("PRIMARY");

            entity.ToTable("tbl_ref_ranges");

            entity.HasIndex(e => e.SmpTyId, "SmpTy_Id_idx");

            entity.Property(e => e.RefId).HasColumnName("Ref_ID");
            entity.Property(e => e.AgeMax).HasColumnName("Age_Max");
            entity.Property(e => e.AgeMaxDays).HasColumnName("Age_Max_Days");
            entity.Property(e => e.AgeMin).HasColumnName("Age_Min");
            entity.Property(e => e.AgeMinDays)
                .HasDefaultValueSql("'0'")
                .HasComment("FLAG SHOULD BE CALCULATED ON THIS COLOUM")
                .HasColumnName("Age_Min_Days");
            entity.Property(e => e.AgeUnit)
                .IsRequired()
                .HasMaxLength(20)
                .HasDefaultValueSql("'1'")
                .HasColumnName("Age_Unit");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.RefHigh)
                .HasDefaultValueSql("'100'")
                .HasColumnName("Ref_High");
            entity.Property(e => e.RefLow).HasColumnName("Ref_Low");
            entity.Property(e => e.SmpTyId).HasColumnName("SmpTy_Id");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");

            entity.HasOne(d => d.SmpTy).WithMany(p => p.TblRefRanges)
                .HasForeignKey(d => d.SmpTyId)
                .HasConstraintName("SmpTy_Id");
        });

        modelBuilder.Entity<TblResult>(entity =>
        {
            entity.HasKey(e => e.ResultId).HasName("PRIMARY");

            entity.ToTable("tbl_result");

            entity.Property(e => e.ResultId).HasColumnName("Result_ID");
            entity.Property(e => e.CalId).HasColumnName("Cal_ID");
            entity.Property(e => e.DaySeqId).HasColumnName("DaySeqID");
            entity.Property(e => e.Flag)
                .IsRequired()
                .HasMaxLength(20);
            entity.Property(e => e.Od).HasColumnName("OD");
            entity.Property(e => e.Od0).HasColumnName("OD0");
            entity.Property(e => e.Qclot).HasColumnName("QCLot");
            entity.Property(e => e.ResultDateTime)
                .HasColumnType("datetime")
                .HasColumnName("Result_DateTime");
            entity.Property(e => e.ResultDescription)
                .IsRequired()
                .HasMaxLength(20);
            entity.Property(e => e.SampleId).HasColumnName("Sample_ID");
            entity.Property(e => e.ScheduleId).HasColumnName("ScheduleID");
            entity.Property(e => e.SmpPos).HasColumnName("Smp_Pos");
            entity.Property(e => e.TblResultcol)
                .HasMaxLength(45)
                .HasColumnName("tbl_resultcol");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
            entity.Property(e => e.TestType).HasColumnName("Test_Type");
        });

        modelBuilder.Entity<TblResultsExtern>(entity =>
        {
            entity.HasKey(e => e.ScheduleId).HasName("PRIMARY");

            entity.ToTable("tbl_results_extern");

            entity.Property(e => e.ScheduleId).HasColumnName("ScheduleID");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ExternTestId).HasColumnName("Extern_TestID");
            entity.Property(e => e.Flag).HasMaxLength(7);
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.Result).HasMaxLength(15);
            entity.Property(e => e.ResultDate).HasColumnType("datetime");
            entity.Property(e => e.SmpId).HasColumnName("SmpID");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblRgtbotle>(entity =>
        {
            entity.HasKey(e => e.RbtlId).HasName("PRIMARY");

            entity.ToTable("tbl_rgtbotle");

            entity.Property(e => e.RbtlId)
                .HasColumnType("mediumint")
                .HasColumnName("RBtl_ID");
            entity.Property(e => e.Barcode).HasMaxLength(20);
            entity.Property(e => e.BtlNo)
                .HasDefaultValueSql("'1'")
                .HasColumnName("Btl_No");
            entity.Property(e => e.BtlSize).HasColumnName("Btl_Size");
            entity.Property(e => e.BtlType).HasColumnName("Btl_Type");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ExpMonth).HasColumnName("Exp_Month");
            entity.Property(e => e.ExpYear).HasColumnName("Exp_Year");
            entity.Property(e => e.MfgId).HasColumnName("MFG_Id");
            entity.Property(e => e.MfgMonth).HasColumnName("MFG_Month");
            entity.Property(e => e.MfgYear).HasColumnName("MFG_Year");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.RgtBlank).HasColumnName("Rgt_blank");
            entity.Property(e => e.RgtType)
                .HasDefaultValueSql("'1'")
                .HasColumnName("Rgt_Type");
            entity.Property(e => e.RlotId)
                .HasColumnType("mediumint")
                .HasColumnName("RLot_ID");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.StabilityEnd).HasColumnType("date");
            entity.Property(e => e.TestCnt).HasColumnName("Test_Cnt");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
        });

        modelBuilder.Entity<TblRgtlot>(entity =>
        {
            entity.HasKey(e => e.RlotId).HasName("PRIMARY");

            entity.ToTable("tbl_rgtlot");

            entity.Property(e => e.RlotId)
                .HasColumnType("mediumint")
                .HasColumnName("RLot_ID");
            entity.Property(e => e.ConsumableType).HasDefaultValueSql("'1'");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ExpMonth).HasColumnName("EXP_Month");
            entity.Property(e => e.ExpYear).HasColumnName("EXP_Year");
            entity.Property(e => e.LotNo)
                .IsRequired()
                .HasMaxLength(10);
            entity.Property(e => e.MfgId).HasColumnName("MFG_ID");
            entity.Property(e => e.MfgMonth).HasColumnName("MFG_Month");
            entity.Property(e => e.MfgYear).HasColumnName("MFG_Year");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
        });

        modelBuilder.Entity<TblRgtpo>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("tbl_rgtpos");

            entity.Property(e => e.RbtlId)
                .HasColumnType("mediumint")
                .HasColumnName("RBtl_ID");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
            entity.Property(e => e.TrayId)
                .HasDefaultValueSql("'1'")
                .HasColumnName("Tray_ID");
            entity.Property(e => e.TrayPos).HasColumnName("Tray_Pos");
        });

        modelBuilder.Entity<TblSample>(entity =>
        {
            entity.HasKey(e => e.SmpId).HasName("PRIMARY");

            entity.ToTable("tbl_sample");

            entity.HasIndex(e => e.DrId, "DrId_idx");

            entity.Property(e => e.SmpId).HasColumnName("Smp_Id");
            entity.Property(e => e.CollectionDate).HasColumnType("datetime");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.HospitalArea)
                .HasMaxLength(1000)
                .HasColumnName("Hospital_Area");
            entity.Property(e => e.LebHospitalName)
                .HasMaxLength(50)
                .HasColumnName("Leb_HospitalName");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.PatientId).HasColumnName("Patient_Id");
            entity.Property(e => e.RegistrationDate).HasColumnType("datetime");
            entity.Property(e => e.SmpBarcode).HasMaxLength(40);
            entity.Property(e => e.SmpRemark).HasMaxLength(1000);
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");

            entity.HasOne(d => d.Dr).WithMany(p => p.TblSamples)
                .HasForeignKey(d => d.DrId)
                .HasConstraintName("DrId");
        });

        modelBuilder.Entity<TblSampleTest>(entity =>
        {
            entity.HasKey(e => e.SmpTstId).HasName("PRIMARY");

            entity.ToTable("tbl_sample_test");

            entity.HasIndex(e => e.SampleId, "SampleId_idx");

            entity.HasIndex(e => e.TestId, "TestId_idx");

            entity.Property(e => e.SmpTstId).HasColumnName("SmpTst_Id");

            entity.HasOne(d => d.Sample).WithMany(p => p.TblSampleTests)
                .HasForeignKey(d => d.SampleId)
                .HasConstraintName("SampleId");

            entity.HasOne(d => d.Test).WithMany(p => p.TblSampleTests)
                .HasForeignKey(d => d.TestId)
                .HasConstraintName("TestId");
        });

        modelBuilder.Entity<TblSampletype>(entity =>
        {
            entity.HasKey(e => e.SmpId).HasName("PRIMARY");

            entity.ToTable("tbl_sampletype");

            entity.Property(e => e.SmpId).HasColumnName("Smp_Id");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.SmpType)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("SMP_Type");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblSchedule>(entity =>
        {
            entity.HasKey(e => e.ScheduleId).HasName("PRIMARY");

            entity.ToTable("tbl_schedule");

            entity.Property(e => e.ScheduleId).HasColumnName("Schedule_ID");
            entity.Property(e => e.PatientId).HasColumnName("Patient_ID");
            entity.Property(e => e.PrevCalId).HasColumnName("PrevCalID");
            entity.Property(e => e.SampleId).HasColumnName("Sample_ID");
            entity.Property(e => e.SchTime).HasColumnType("datetime");
            entity.Property(e => e.SeqId).HasColumnName("SeqID");
        });

        modelBuilder.Entity<TblScheduledet>(entity =>
        {
            entity.HasKey(e => e.ScheduleDetId).HasName("PRIMARY");

            entity.ToTable("tbl_scheduledet");

            entity.Property(e => e.ScheduleDetId).HasColumnName("ScheduleDet_ID");
            entity.Property(e => e.LotId)
                .HasColumnType("mediumint")
                .HasColumnName("Lot_ID");
            entity.Property(e => e.ScheduleId)
                .HasColumnType("mediumint")
                .HasColumnName("Schedule_ID");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
        });

        modelBuilder.Entity<TblSystemSetting>(entity =>
        {
            entity.HasKey(e => e.SystemSettingId).HasName("PRIMARY");

            entity.ToTable("tbl_system_settings");

            entity.Property(e => e.SystemSettingId).HasColumnName("System_SettingID");
            entity.Property(e => e.InstrumentPort).HasMaxLength(20);
        });

        modelBuilder.Entity<TblTest>(entity =>
        {
            entity.HasKey(e => e.TestId).HasName("PRIMARY");

            entity.ToTable("tbl_test");

            entity.Property(e => e.TestId).HasColumnName("Test_ID");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.Decimals).HasComment("for Result Display only");
            entity.Property(e => e.Direction).HasComment("Increasing/Decreasing");
            entity.Property(e => e.IsActive).HasDefaultValueSql("'1'");
            entity.Property(e => e.IsVisible).HasDefaultValueSql("'1'");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.R1incubationTime).HasColumnName("R1IncubationTime");
            entity.Property(e => e.R1incubationTimeSec).HasColumnName("R1IncubationTimeSec");
            entity.Property(e => e.R1strSpeed).HasColumnName("R1StrSpeed");
            entity.Property(e => e.R1volume).HasColumnName("R1Volume");
            entity.Property(e => e.R2incubationTime).HasColumnName("R2IncubationTime");
            entity.Property(e => e.R2incubationTimeSec).HasColumnName("R2IncubationTimeSec");
            entity.Property(e => e.R2strSpeed).HasColumnName("R2StrSpeed");
            entity.Property(e => e.R2volume).HasColumnName("R2Volume");
            entity.Property(e => e.SoftDelete).HasColumnName("Soft_Delete");
            entity.Property(e => e.TestCode)
                .IsRequired()
                .HasMaxLength(10);
            entity.Property(e => e.TestName)
                .IsRequired()
                .HasMaxLength(20);
            entity.Property(e => e.UniqueTestId)
                .HasComment("Uniqe Id given by Company for every test parameter for upload/download Testdeatils/standards/Reagents\n")
                .HasColumnName("Unique_Test_ID");
        });

        modelBuilder.Entity<TblTestCalcu>(entity =>
        {
            entity.HasKey(e => e.CalId).HasName("PRIMARY");

            entity.ToTable("tbl_test_calcu");

            entity.HasIndex(e => e.CalTestCode, "Cal_TestCode_UNIQUE").IsUnique();

            entity.Property(e => e.CalId).HasColumnName("Cal_Id");
            entity.Property(e => e.CalExpression)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("Cal_Expression");
            entity.Property(e => e.CalTestCode)
                .HasMaxLength(10)
                .HasColumnName("Cal_Test_Code");
            entity.Property(e => e.CalTestId).HasColumnName("Cal_Test_ID");
            entity.Property(e => e.CalTestName)
                .HasMaxLength(20)
                .HasColumnName("Cal_Test_Name");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.RefHigh).HasColumnName("Ref_High");
            entity.Property(e => e.RefLow).HasColumnName("Ref_Low");
            entity.Property(e => e.SendToHost).HasDefaultValueSql("'1'");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblTestCalib>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("tbl_test_calib");

            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.Od).HasColumnName("OD");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
        });

        modelBuilder.Entity<TblTestCarryOver>(entity =>
        {
            entity.HasKey(e => e.Idpk).HasName("PRIMARY");

            entity.ToTable("tbl_test_carry_over");

            entity.Property(e => e.Idpk).HasColumnName("IDPK");
            entity.Property(e => e.AfectedTestId).HasColumnName("Afected_Test_ID");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
            entity.Property(e => e.WashType)
                .IsRequired()
                .HasMaxLength(20);
        });

        modelBuilder.Entity<TblTestDetail>(entity =>
        {
            entity.HasKey(e => e.TestSqunceId).HasName("PRIMARY");

            entity.ToTable("tbl_test_details");

            entity.Property(e => e.TestSqunceId).HasColumnName("Test_Squnce_Id");
            entity.Property(e => e.AbsLimitHigh)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Abs_Limit_High");
            entity.Property(e => e.AbsLimitLow)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Abs_Limit_Low");
            entity.Property(e => e.AutoDil)
                .HasDefaultValueSql("'1'")
                .HasColumnName("Auto_Dil");
            entity.Property(e => e.CalBlanking).HasDefaultValueSql("'0'");
            entity.Property(e => e.CalLot).HasDefaultValueSql("'0'");
            entity.Property(e => e.ControlInterval).HasDefaultValueSql("'0'");
            entity.Property(e => e.Cost).HasDefaultValueSql("'0'");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.CuvWashA).HasDefaultValueSql("'0'");
            entity.Property(e => e.CuvWashB).HasDefaultValueSql("'0'");
            entity.Property(e => e.DispIndex).HasDefaultValueSql("'0'");
            entity.Property(e => e.LisName)
                .HasMaxLength(20)
                .HasComment("will be TestCode by default\n")
                .HasColumnName("LIS_Name");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.OnlineCalib).HasDefaultValueSql("'0'");
            entity.Property(e => e.PrintName)
                .HasMaxLength(30)
                .HasComment("will be TestName by default\n");
            entity.Property(e => e.RbHigh)
                .HasDefaultValueSql("'0'")
                .HasColumnName("RB_HIGH");
            entity.Property(e => e.RbLow)
                .HasDefaultValueSql("'0'")
                .HasColumnName("RB_LOW");
            entity.Property(e => e.ReplicateCntrl)
                .HasDefaultValueSql("'2'")
                .HasColumnName("Replicate_Cntrl");
            entity.Property(e => e.ReplicateSmp)
                .HasDefaultValueSql("'2'")
                .HasColumnName("Replicate_Smp");
            entity.Property(e => e.ReplicateStd)
                .HasDefaultValueSql("'2'")
                .HasColumnName("Replicate_Std");
            entity.Property(e => e.SendToHost).HasDefaultValueSql("'1'");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.SpecialDil).HasDefaultValueSql("'0'");
            entity.Property(e => e.TblTestDetailscol)
                .HasMaxLength(45)
                .HasColumnName("tbl_test_detailscol");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
        });

        modelBuilder.Entity<TblTestExtern>(entity =>
        {
            entity.HasKey(e => e.ExternTestId).HasName("PRIMARY");

            entity.ToTable("tbl_test_extern");

            entity.Property(e => e.ExternTestId).HasColumnName("Extern_TestID");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.Decimals).HasDefaultValueSql("'3'");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.ResultType)
                .HasMaxLength(20)
                .HasComment("1. Numeric(Quantitative)\\n2. chars(Qualitative)")
                .HasColumnName("Result_Type");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.TestCode)
                .HasMaxLength(20)
                .HasColumnName("Test_Code");
            entity.Property(e => e.TestName)
                .HasMaxLength(45)
                .HasColumnName("Test_Name");
        });

        modelBuilder.Entity<TblTestFilter>(entity =>
        {
            entity.HasKey(e => e.FltrId).HasName("PRIMARY");

            entity.ToTable("tbl_test_filter");

            entity.Property(e => e.FltrId).HasColumnName("Fltr_Id");
            entity.Property(e => e.Filter)
                .IsRequired()
                .HasMaxLength(45);
        });

        modelBuilder.Entity<TblTestProfile>(entity =>
        {
            entity.HasKey(e => e.ProfileId).HasName("PRIMARY");

            entity.ToTable("tbl_test_profile");

            entity.Property(e => e.ProfileId).HasColumnName("Profile_ID");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.ProfileCode).HasMaxLength(10);
            entity.Property(e => e.ProfileName)
                .IsRequired()
                .HasMaxLength(20);
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
        });

        modelBuilder.Entity<TblTestReflex>(entity =>
        {
            entity.HasKey(e => e.ReflexId).HasName("PRIMARY");

            entity.ToTable("tbl_test_reflex");

            entity.Property(e => e.ReflexId).HasColumnName("Reflex_ID");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.ReflexType)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnName("Reflex_Type");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
        });

        modelBuilder.Entity<TblTestReflexDet>(entity =>
        {
            entity.HasKey(e => e.RflxDetlId).HasName("PRIMARY");

            entity.ToTable("tbl_test_reflex_det");

            entity.Property(e => e.RflxDetlId).HasColumnName("RflxDetl_Id");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.ReflexId).HasColumnName("Reflex_ID");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
        });

        modelBuilder.Entity<TblTestVolume>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("tbl_test_volumes");

            entity.Property(e => e.DilRatioD)
                .HasDefaultValueSql("'1'")
                .HasColumnName("DilRatio_D");
            entity.Property(e => e.DilRatioI)
                .HasDefaultValueSql("'1'")
                .HasColumnName("DilRatio_I");
            entity.Property(e => e.DilRatioN)
                .HasDefaultValueSql("'1'")
                .HasColumnName("DilRatio_N");
            entity.Property(e => e.R1vol)
                .HasDefaultValueSql("'150'")
                .HasColumnName("R1Vol");
            entity.Property(e => e.R2vol)
                .HasDefaultValueSql("'50'")
                .HasColumnName("R2Vol");
            entity.Property(e => e.R3vol)
                .HasDefaultValueSql("'50'")
                .HasColumnName("R3Vol");
            entity.Property(e => e.StdVol).HasDefaultValueSql("'2'");
            entity.Property(e => e.SvolD)
                .HasDefaultValueSql("'2'")
                .HasColumnName("SVol_D");
            entity.Property(e => e.SvolI)
                .HasDefaultValueSql("'2'")
                .HasColumnName("SVol_I");
            entity.Property(e => e.SvolN)
                .HasDefaultValueSql("'2'")
                .HasColumnName("SVol_N");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
        });

        modelBuilder.Entity<TblUnit>(entity =>
        {
            entity.HasKey(e => e.UntId).HasName("PRIMARY");

            entity.ToTable("tbl_unit");

            entity.Property(e => e.UntId).HasColumnName("Unt_Id");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.SoftDelete).HasColumnName("Soft_Delete");
            entity.Property(e => e.UntName)
                .IsRequired()
                .HasMaxLength(500)
                .HasColumnName("Unt_Name");
        });

        modelBuilder.Entity<TblUser>(entity =>
        {
            entity.HasKey(e => e.UidPk).HasName("PRIMARY");

            entity.ToTable("tbl_user");

            entity.Property(e => e.UidPk).HasColumnName("UID_PK");
            entity.Property(e => e.CreateBy).HasColumnName("Create_By");
            entity.Property(e => e.CreateDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Create_Date");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.PersonId).HasColumnName("Person_ID");
            entity.Property(e => e.SoftDelete)
                .HasDefaultValueSql("'0'")
                .HasColumnName("Soft_Delete");
            entity.Property(e => e.ULoginId)
                .IsRequired()
                .HasMaxLength(15)
                .HasColumnName("U_LoginID");
            entity.Property(e => e.UPwd)
                .IsRequired()
                .HasMaxLength(10)
                .HasColumnName("U_Pwd");
            entity.Property(e => e.UPwdHint)
                .HasMaxLength(30)
                .HasColumnName("U_PwdHint");
            entity.Property(e => e.UType).HasColumnName("U_Type");
        });

        modelBuilder.Entity<TblUsermaster>(entity =>
        {
            entity.HasKey(e => e.UserId).HasName("PRIMARY");

            entity.ToTable("tbl_usermaster");

            entity.HasIndex(e => e.RoleId, "Role_Id_idx");

            entity.Property(e => e.UserId).HasColumnName("User_Id");
            entity.Property(e => e.CreatedBy).HasColumnName("Created_By");
            entity.Property(e => e.CreatedDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasColumnType("datetime")
                .HasColumnName("Created_Date");
            entity.Property(e => e.Email).HasMaxLength(100);
            entity.Property(e => e.LoginTime)
                .HasColumnType("timestamp(6)")
                .HasColumnName("Login_Time");
            entity.Property(e => e.LogoutTime)
                .HasColumnType("timestamp(6)")
                .HasColumnName("Logout_Time");
            entity.Property(e => e.ModifyBy).HasColumnName("Modify_By");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("Modify_Date");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(200);
            entity.Property(e => e.Password).HasMaxLength(100);
            entity.Property(e => e.RoleId).HasColumnName("Role_Id");
            entity.Property(e => e.SoftDelete).HasColumnName("Soft_Delete");
            entity.Property(e => e.UserImage)
                .HasColumnType("blob")
                .HasColumnName("User_Image");
            entity.Property(e => e.UserName)
                .HasMaxLength(200)
                .HasColumnName("User_Name");

            entity.HasOne(d => d.Role).WithMany(p => p.TblUsermasters)
                .HasForeignKey(d => d.RoleId)
                .HasConstraintName("Role_Id");
        });

        modelBuilder.Entity<TblUserright>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("tbl_userrights");

            entity.Property(e => e.FormId).HasColumnName("FormID");
            entity.Property(e => e.FunctionId).HasColumnName("FunctionID");
            entity.Property(e => e.Seqid).HasColumnName("SEQID");
            entity.Property(e => e.UserId).HasColumnName("UserID");
        });

        modelBuilder.Entity<TblWorklist>(entity =>
        {
            entity.HasKey(e => e.SeqNo).HasName("PRIMARY");

            entity.ToTable("tbl_worklist");

            entity.Property(e => e.Flag).HasMaxLength(30);
            entity.Property(e => e.LotId)
                .HasColumnType("mediumint")
                .HasColumnName("Lot_ID");
            entity.Property(e => e.Od).HasColumnName("OD");
            entity.Property(e => e.R1pos).HasColumnName("R1Pos");
            entity.Property(e => e.R1short).HasColumnName("R1Short");
            entity.Property(e => e.R1time)
                .HasColumnType("datetime")
                .HasColumnName("R1Time");
            entity.Property(e => e.R1vol).HasColumnName("R1Vol");
            entity.Property(e => e.R2pos).HasColumnName("R2Pos");
            entity.Property(e => e.R2short).HasColumnName("R2Short");
            entity.Property(e => e.R2time)
                .HasColumnType("datetime")
                .HasColumnName("R2Time");
            entity.Property(e => e.R2vol).HasColumnName("R2Vol");
            entity.Property(e => e.R3pos).HasColumnName("R3Pos");
            entity.Property(e => e.R3short).HasColumnName("R3Short");
            entity.Property(e => e.R3time)
                .HasColumnType("datetime")
                .HasColumnName("R3Time");
            entity.Property(e => e.R3vol).HasColumnName("R3Vol");
            entity.Property(e => e.Result).HasPrecision(10);
            entity.Property(e => e.ResultId).HasColumnName("ResultID");
            entity.Property(e => e.ScheduleId).HasColumnName("Schedule_ID");
            entity.Property(e => e.SmpPos).HasColumnName("Smp_Pos");
            entity.Property(e => e.SmpShort).HasColumnName("SMP_Short");
            entity.Property(e => e.SmpTime).HasColumnType("datetime");
            entity.Property(e => e.TestId).HasColumnName("Test_ID");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
