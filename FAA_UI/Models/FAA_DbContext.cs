﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace FAA_UI.Models
{
    public partial class FAA_DbContext : DbContext
    {
        public FAA_DbContext()
        {
        }

        public FAA_DbContext(DbContextOptions<FAA_DbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblAddress> TblAddresses { get; set; }
        public virtual DbSet<TblCalibration> TblCalibrations { get; set; }
        public virtual DbSet<TblCalibrationdet> TblCalibrationdets { get; set; }
        public virtual DbSet<TblDeadvolume> TblDeadvolumes { get; set; }
        public virtual DbSet<TblDirectory> TblDirectories { get; set; }
        public virtual DbSet<TblDirectoryDet> TblDirectoryDets { get; set; }
        public virtual DbSet<TblDoctor> TblDoctors { get; set; }
        public virtual DbSet<TblError> TblErrors { get; set; }
        public virtual DbSet<TblFlag> TblFlags { get; set; }
        public virtual DbSet<TblInstrument> TblInstruments { get; set; }
        public virtual DbSet<TblInstrumentAssmbly> TblInstrumentAssmblies { get; set; }
        public virtual DbSet<TblInstrumentCommunication> TblInstrumentCommunications { get; set; }
        public virtual DbSet<TblInstrumentSetting> TblInstrumentSettings { get; set; }
        public virtual DbSet<TblLogAction> TblLogActions { get; set; }
        public virtual DbSet<TblLogActionDet> TblLogActionDets { get; set; }
        public virtual DbSet<TblLogError> TblLogErrors { get; set; }
        public virtual DbSet<TblLogSetting> TblLogSettings { get; set; }
        public virtual DbSet<TblLot> TblLots { get; set; }
        public virtual DbSet<TblLotDet> TblLotDets { get; set; }
        public virtual DbSet<TblPatient> TblPatients { get; set; }
        public virtual DbSet<TblPerson> TblPeople { get; set; }
        public virtual DbSet<TblProfileDet> TblProfileDets { get; set; }
        public virtual DbSet<TblReadsBlank> TblReadsBlanks { get; set; }
        public virtual DbSet<TblReadsResult> TblReadsResults { get; set; }
        public virtual DbSet<TblRefRange> TblRefRanges { get; set; }
        public virtual DbSet<TblReflexDet> TblReflexDets { get; set; }
        public virtual DbSet<TblResult> TblResults { get; set; }
        public virtual DbSet<TblResultsExtern> TblResultsExterns { get; set; }
        public virtual DbSet<TblRgtbotle> TblRgtbotles { get; set; }
        public virtual DbSet<TblRgtlot> TblRgtlots { get; set; }
        public virtual DbSet<TblRgtpo> TblRgtpos { get; set; }
        public virtual DbSet<TblSchedule> TblSchedules { get; set; }
        public virtual DbSet<TblScheduledet> TblScheduledets { get; set; }
        public virtual DbSet<TblSystemSetting> TblSystemSettings { get; set; }
        public virtual DbSet<TblTest> TblTests { get; set; }
        public virtual DbSet<TblTestCalcu> TblTestCalcus { get; set; }
        public virtual DbSet<TblTestCalib> TblTestCalibs { get; set; }
        public virtual DbSet<TblTestCarryOver> TblTestCarryOvers { get; set; }
        public virtual DbSet<TblTestDetail> TblTestDetails { get; set; }
        public virtual DbSet<TblTestExtern> TblTestExterns { get; set; }
        public virtual DbSet<TblTestProfile> TblTestProfiles { get; set; }
        public virtual DbSet<TblTestReflex> TblTestReflices { get; set; }
        public virtual DbSet<TblTestVolume> TblTestVolumes { get; set; }
        public virtual DbSet<TblUser> TblUsers { get; set; }
        public virtual DbSet<TblUserright> TblUserrights { get; set; }
        public virtual DbSet<TblWorklist> TblWorklists { get; set; }


        protected readonly IConfiguration Configuration;
        public FAA_DbContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
            optionsBuilder.UseMySQL(configuration.GetConnectionString("AppConnection"));
        }

       // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
       //{
       //     if (!optionsBuilder.IsConfigured)
       //     {
       //         //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
       //         var connectionString = Configuration.GetConnectionString("AppConnection");
       //         optionsBuilder.UseMySQL("Server=localhost;port=3306;user=root;password=Biosense23;Database=DB_FAA_Software;");
       //        //       => optionsBuilder.UseMySQL("server=localhost;port=3306;user=root;password=user1234;database=demo;");

       //         // optionsBuilder.UseMySQL(connectionString);
       //     }
       // }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblAddress>(entity =>
            {
                entity.HasKey(e => e.AddId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_address");

                entity.Property(e => e.AddId)
                    .HasColumnType("mediumint")
                    .HasColumnName("Add_ID");

                entity.Property(e => e.AddCity)
                    .HasColumnName("Add_City")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.AddCountry)
                    .HasColumnType("tinyint")
                    .HasColumnName("Add_Country")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.AddLine1)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("Add_Line1");

                entity.Property(e => e.AddLine2)
                    .HasMaxLength(40)
                    .HasColumnName("Add_Line2");

                entity.Property(e => e.AddLine3)
                    .HasMaxLength(30)
                    .HasColumnName("Add_Line3");

                entity.Property(e => e.AddPin)
                    .HasColumnType("mediumint")
                    .HasColumnName("Add_Pin");

                entity.Property(e => e.AddState)
                    .HasColumnType("tinyint")
                    .HasColumnName("Add_State")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.CUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("C_UID");

                entity.Property(e => e.DUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("D_UID");

                entity.Property(e => e.LastModUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("LastMod_UID");
            });

            modelBuilder.Entity<TblCalibration>(entity =>
            {
                entity.HasKey(e => e.CalId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_calibration");

                entity.Property(e => e.CalId)
                    .HasColumnType("mediumint")
                    .HasColumnName("Cal_ID");

   

                entity.Property(e => e.CalDate)
                    .HasColumnType("date")
                    .HasColumnName("Cal_Date");

                entity.Property(e => e.CalExpiry)
                    .HasColumnType("date")
                    .HasColumnName("Cal_Expiry");

                entity.Property(e => e.CalStatus)
                    .HasColumnType("tinyint")
                    .HasColumnName("Cal_Status");

                entity.Property(e => e.CalTime).HasColumnName("Cal_Time");

                entity.Property(e => e.CalValidity)
                    .HasColumnType("date")
                    .HasColumnName("Cal_Validity");

                entity.Property(e => e.CalValidityDays).HasColumnName("Cal_Validity_Days");

                entity.Property(e => e.CurveType).HasColumnType("tinyint");

          

                entity.Property(e => e.IsActive).HasColumnType("tinyint");

                entity.Property(e => e.IsModified).HasColumnType("tinyint");

             
                entity.Property(e => e.NoOfStds).HasColumnType("tinyint");

                entity.Property(e => e.PrevCalIdRgtBlnking)
                    .HasColumnType("mediumint")
                    .HasColumnName("PrevCalID_RgtBlnking");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");
            });

            modelBuilder.Entity<TblCalibrationdet>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_calibrationdet");

                entity.Property(e => e.ActualOd).HasColumnName("ActualOD");

                entity.Property(e => e.CalId).HasColumnName("Cal_ID");

                entity.Property(e => e.IsBlank).HasColumnType("tinyint");

                entity.Property(e => e.IsModified).HasColumnType("tinyint");

                entity.Property(e => e.Od).HasColumnName("OD");
            });

            modelBuilder.Entity<TblDeadvolume>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_deadvolume");

                entity.Property(e => e.DictItemId).HasColumnName("Dict_ItemID");

                entity.Property(e => e.DvTime).HasColumnName("DV_TIME");

                entity.Property(e => e.DvVolume)
                    .HasColumnType("mediumint")
                    .HasColumnName("DV_Volume");

                entity.Property(e => e.Steps).HasColumnType("mediumint");

                entity.Property(e => e.Uid)
                    .HasColumnType("tinyint")
                    .HasColumnName("UID");
            });

            modelBuilder.Entity<TblDirectory>(entity =>
            {
                entity.HasKey(e => e.DictId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_directory");

                entity.Property(e => e.DictId).HasColumnName("Dict_ID");

                entity.Property(e => e.CUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("C_UID");

                entity.Property(e => e.DUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("D_UID");

                entity.Property(e => e.DictDesc)
                    .HasMaxLength(20)
                    .HasColumnName("Dict_Desc");

                entity.Property(e => e.DictRemark)
                    .HasMaxLength(15)
                    .HasColumnName("Dict_Remark");

                entity.Property(e => e.IsActive).HasColumnType("tinyint");

                entity.Property(e => e.LastModUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("LastMod_UID");
            });

            modelBuilder.Entity<TblDirectoryDet>(entity =>
            {
                entity.HasKey(e => e.DictItemId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_directory_det");

                entity.Property(e => e.DictItemId).HasColumnName("Dict_ItemID");

                entity.Property(e => e.CUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("C_UID");

                entity.Property(e => e.DUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("D_UID");

                entity.Property(e => e.DictId).HasColumnName("Dict_ID");

                entity.Property(e => e.DictItem)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("Dict_Item");

                entity.Property(e => e.DictItemDesc)
                    .HasMaxLength(30)
                    .HasColumnName("Dict_Item_Desc");

                entity.Property(e => e.IsActive).HasColumnType("tinyint");

                entity.Property(e => e.IsVisible).HasColumnType("tinyint");

                entity.Property(e => e.ItemValue).HasMaxLength(25);

                entity.Property(e => e.LastModUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("LastMod_UID");
            });

            modelBuilder.Entity<TblDoctor>(entity =>
            {
                entity.HasKey(e => e.DrId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_doctor");

                entity.Property(e => e.DrId).HasColumnName("DR_ID");

                entity.Property(e => e.CUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("C_UID");

                entity.Property(e => e.DUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("D_UID");

                entity.Property(e => e.DispName).HasMaxLength(20);

                entity.Property(e => e.HospitalId).HasColumnName("Hospital_ID");

                entity.Property(e => e.IsActive)
                    .HasColumnType("tinyint")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.LastModUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("LastMod_UID");

                entity.Property(e => e.PersonId)
                    .HasColumnType("mediumint")
                    .HasColumnName("Person_ID");
            });

            modelBuilder.Entity<TblError>(entity =>
            {
                entity.HasKey(e => e.ErrorId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_errors");

                entity.Property(e => e.ErrorId).HasColumnName("Error_ID");

                entity.Property(e => e.AssemblyId).HasColumnName("Assembly_ID");

                entity.Property(e => e.DisplayToUser)
                    .HasColumnType("tinyint")
                    .HasDefaultValueSql("'1'")
                    .HasComment("1 . If error displayed to user.\n");

                entity.Property(e => e.ErrorAction)
                    .HasMaxLength(45)
                    .HasColumnName("Error_Action");

                entity.Property(e => e.ErrorCode)
                    .HasMaxLength(5)
                    .HasColumnName("Error_Code");

                entity.Property(e => e.ErrorDesc)
                    .HasMaxLength(50)
                    .HasColumnName("Error_Desc");

                entity.Property(e => e.ErrorFlag)
                    .HasMaxLength(5)
                    .HasColumnName("Error_Flag");

                entity.Property(e => e.InstrumentId)
                    .HasColumnType("tinyint")
                    .HasColumnName("Instrument_ID");

                entity.Property(e => e.IsStopError)
                    .HasColumnType("tinyint")
                    .HasColumnName("Is_StopError")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.LogError)
                    .HasColumnType("tinyint")
                    .HasColumnName("Log_Error")
                    .HasDefaultValueSql("'1'")
                    .HasComment("1 if need to log this error in Database else 0");

                entity.Property(e => e.RecoverTryCnt)
                    .HasColumnType("tinyint")
                    .HasColumnName("Recover_Try_Cnt")
                    .HasComment("No of times software should try to auto recover due to this error\n");
            });

            modelBuilder.Entity<TblFlag>(entity =>
            {
                entity.HasKey(e => e.FlagId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_flags");

                entity.Property(e => e.FlagId).HasColumnName("Flag_ID");

                entity.Property(e => e.FlagCode)
                    .HasMaxLength(7)
                    .HasColumnName("Flag_Code");

                entity.Property(e => e.FlagMsg)
                    .HasMaxLength(50)
                    .HasColumnName("Flag_Msg");

                entity.Property(e => e.FlagType)
                    .HasColumnType("tinyint")
                    .HasColumnName("Flag_Type")
                    .HasComment("1. Absent\n2. Instrument_Warning.\n3. Normal_Warning\n4..RESULT\n5.  Instrument_Error\n ");

                entity.Property(e => e.TblFlagscol)
                    .HasMaxLength(45)
                    .HasColumnName("Tbl_Flagscol");
            });

            modelBuilder.Entity<TblInstrument>(entity =>
            {
                entity.HasKey(e => e.InstrumentId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_instrument");

                entity.Property(e => e.InstrumentId).HasColumnName("Instrument_ID");

                entity.Property(e => e.AswVersion)
                    .HasColumnType("tinyint")
                    .HasColumnName("ASW_Version");

                entity.Property(e => e.CurInstrument)
                    .HasColumnType("tinyint")
                    .HasColumnName("Cur_Instrument");

                entity.Property(e => e.CuvPosition)
                    .HasColumnType("tinyint")
                    .HasColumnName("Cuv_Position");

                entity.Property(e => e.CycleTime)
                    .HasColumnType("tinyint")
                    .HasColumnName("Cycle_Time");

                entity.Property(e => e.EswVersion)
                    .HasColumnType("tinyint")
                    .HasColumnName("ESW_Version");

                entity.Property(e => e.FilterCnt)
                    .HasColumnType("tinyint")
                    .HasColumnName("Filter_Cnt")
                    .HasComment("avalible number of filters");

                entity.Property(e => e.InitDryerCpos)
                    .HasColumnType("tinyint")
                    .HasColumnName("Init_Dryer_CPos");

                entity.Property(e => e.InitPhotoCpos)
                    .HasColumnType("tinyint")
                    .HasColumnName("Init_Photo_CPos");

                entity.Property(e => e.InitR1Cpos)
                    .HasColumnType("tinyint")
                    .HasColumnName("Init_R1_CPos");

                entity.Property(e => e.InitR2Cpos)
                    .HasColumnType("tinyint")
                    .HasColumnName("Init_R2_CPos");

                entity.Property(e => e.InitR3Cpos)
                    .HasColumnType("tinyint")
                    .HasColumnName("Init_R3_CPos");

                entity.Property(e => e.InitSmpCpos)
                    .HasColumnType("tinyint")
                    .HasColumnName("Init_Smp_CPos");

                entity.Property(e => e.InitStr1Cpos)
                    .HasColumnType("tinyint")
                    .HasColumnName("Init_Str1_CPos");

                entity.Property(e => e.InitStr2Cpos)
                    .HasColumnType("tinyint")
                    .HasColumnName("Init_Str2_CPos");

                entity.Property(e => e.InitStr3Cpos)
                    .HasColumnType("tinyint")
                    .HasColumnName("Init_Str3_CPos");

                entity.Property(e => e.InstrumentName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("Instrument_Name");

                entity.Property(e => e.InstrumentSerialNo)
                    .HasMaxLength(15)
                    .HasColumnName("Instrument_Serial_no");

                entity.Property(e => e.IseModule)
                    .HasColumnType("tinyint")
                    .HasColumnName("ISE_MODULE");

                entity.Property(e => e.MaxReadCycles)
                    .HasColumnType("tinyint")
                    .HasColumnName("Max_Read_Cycles")
                    .HasComment("no of overall cycles before wash");

                entity.Property(e => e.RgtPositions)
                    .HasColumnType("tinyint")
                    .HasColumnName("Rgt_Positions");

                entity.Property(e => e.SmpInrPosE)
                    .HasColumnType("tinyint")
                    .HasColumnName("Smp_Inr_Pos_E");

                entity.Property(e => e.SmpInrPosS)
                    .HasColumnType("tinyint")
                    .HasColumnName("Smp_Inr_Pos_S");

                entity.Property(e => e.SmpMidPosE)
                    .HasColumnType("tinyint")
                    .HasColumnName("Smp_Mid_Pos_E");

                entity.Property(e => e.SmpMidPosS)
                    .HasColumnType("tinyint")
                    .HasColumnName("Smp_Mid_Pos_S");

                entity.Property(e => e.SmpOutPosE)
                    .HasColumnType("tinyint")
                    .HasColumnName("Smp_Out_Pos_E");

                entity.Property(e => e.SmpOutPosS)
                    .HasColumnType("tinyint")
                    .HasColumnName("Smp_Out_Pos_S");

                entity.Property(e => e.SmpPositions)
                    .HasColumnType("tinyint")
                    .HasColumnName("Smp_positions")
                    .HasComment("no of sample positions");
            });

            modelBuilder.Entity<TblInstrumentAssmbly>(entity =>
            {
                entity.HasKey(e => e.AssemblyId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_instrument_assmbly");

                entity.Property(e => e.AssemblyId).HasColumnName("Assembly_ID");

                entity.Property(e => e.Assembly).HasMaxLength(20);

                entity.Property(e => e.AswCode)
                    .HasColumnType("tinyint")
                    .HasColumnName("ASW_CODE");

                entity.Property(e => e.AswName)
                    .HasMaxLength(10)
                    .HasColumnName("ASW_NAME");

                entity.Property(e => e.EswCode)
                    .HasColumnType("tinyint")
                    .HasColumnName("ESW_CODE");

                entity.Property(e => e.InstrumentId)
                    .HasColumnType("tinyint")
                    .HasColumnName("Instrument_ID");

                entity.Property(e => e.IsActive)
                    .HasColumnType("tinyint")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Uid)
                    .HasColumnType("tinyint")
                    .HasColumnName("UID")
                    .HasDefaultValueSql("'1'");
            });

            modelBuilder.Entity<TblInstrumentCommunication>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_instrument_communication");

                entity.HasIndex(e => e.InstrumentId, "Instrument_ID_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.ComPort)
                    .HasColumnType("tinyint")
                    .HasColumnName("Com_Port");

                entity.Property(e => e.ComType)
                    .HasColumnType("tinyint")
                    .HasColumnName("Com_Type")
                    .HasComment("1 Serial Port\n2 TCP/IP\n");

                entity.Property(e => e.InstrumentId)
                    .HasColumnType("tinyint")
                    .HasColumnName("Instrument_ID");

                entity.Property(e => e.LisAck)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_ACK");

                entity.Property(e => e.LisBaudRate).HasColumnName("LIS_BaudRate");

                entity.Property(e => e.LisComPort)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_Com_Port");

                entity.Property(e => e.LisComType)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_Com_Type");

                entity.Property(e => e.LisDataType)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_Data_Type")
                    .HasComment("1- Single Packet at a time\n2. Multiple Packets at a time\n \n\n");

                entity.Property(e => e.LisDelimeter)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_DELIMETER");

                entity.Property(e => e.LisEnabled)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_Enabled")
                    .HasComment("1- if LIS Settings are ON\n0- if LIS Settings are OFF");

                entity.Property(e => e.LisEnq)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_ENQ");

                entity.Property(e => e.LisEot)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_EOT");

                entity.Property(e => e.LisEtx)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_ETX");

                entity.Property(e => e.LisNack)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_NACK");

                entity.Property(e => e.LisPacketSeperator)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_PACKET_SEPERATOR");

                entity.Property(e => e.LisParmeterSeperator)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_PARMETER_SEPERATOR");

                entity.Property(e => e.LisServerIp)
                    .HasMaxLength(15)
                    .HasColumnName("LIS_Server_IP");

                entity.Property(e => e.LisSot)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_SOT");

                entity.Property(e => e.LisStx)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_STX");

                entity.Property(e => e.LisTcpPort)
                    .HasColumnType("tinyint")
                    .HasColumnName("LIS_TCP_Port");
            });

            modelBuilder.Entity<TblInstrumentSetting>(entity =>
            {
                entity.HasKey(e => e.InstrumentSettingsId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_instrument_settings");

                entity.Property(e => e.InstrumentSettingsId).HasColumnName("Instrument_Settings_ID");

                entity.Property(e => e.AddWaterTime).HasColumnName("Add_Water_Time");

                entity.Property(e => e.ArmDepthCuv).HasColumnName("Arm_Depth_Cuv");

                entity.Property(e => e.ArmDepthHome).HasColumnName("Arm_Depth_Home");

                entity.Property(e => e.ArmDepthRgtIn).HasColumnName("Arm_Depth_RGT_In");

                entity.Property(e => e.ArmDepthRgtOut).HasColumnName("Arm_Depth_RGT_Out");

                entity.Property(e => e.ArmPosCuv).HasColumnName("Arm_Pos_Cuv");

                entity.Property(e => e.ArmPosHome)
                    .HasColumnName("Arm_Pos_Home")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ArmPosRgtIn).HasColumnName("Arm_Pos_RGT_In");

                entity.Property(e => e.ArmPosRgtOut).HasColumnName("Arm_Pos_RGT_Out");

                entity.Property(e => e.ArmPosRgtSmp).HasColumnName("Arm_Pos_RGT_Smp");

                entity.Property(e => e.CruDepthWash).HasColumnName("Cru_Depth_Wash");

                entity.Property(e => e.CruDepthWater).HasColumnName("Cru_Depth_Water");

                entity.Property(e => e.InstrumentId).HasColumnName("Instrument_ID");

                entity.Property(e => e.PathLength)
                    .HasColumnType("decimal(2,2)")
                    .HasColumnName("Path_Length")
                    .HasDefaultValueSql("'0.65'");

                entity.Property(e => e.RemoveWaterTime).HasColumnName("Remove_Water_Time");

                entity.Property(e => e.RgtPosIn).HasColumnName("Rgt_Pos_In");

                entity.Property(e => e.RgtPosOut).HasColumnName("Rgt_Pos_Out");

                entity.Property(e => e.RgtPosSmp).HasColumnName("Rgt_Pos_Smp");

                entity.Property(e => e.RgtScanIn).HasColumnName("Rgt_Scan_In");

                entity.Property(e => e.RgtScanOut).HasColumnName("Rgt_Scan_Out");

                entity.Property(e => e.RgtScanSmp).HasColumnName("Rgt_Scan_Smp");

                entity.Property(e => e.StrDepthCuv).HasColumnName("Str_Depth_Cuv");

                entity.Property(e => e.StrDepthHome).HasColumnName("Str_Depth_Home");

                entity.Property(e => e.StrPosCuv).HasColumnName("Str_Pos_Cuv");

                entity.Property(e => e.StrPosHome)
                    .HasColumnName("Str_Pos_Home")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.StrSpeed).HasColumnName("Str_Speed");

                entity.Property(e => e.SyringeOffset).HasColumnName("Syringe_Offset");
            });

            modelBuilder.Entity<TblLogAction>(entity =>
            {
                entity.HasKey(e => e.LogAId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_log_action");

                entity.Property(e => e.LogAId).HasColumnName("LogA_ID");

                entity.Property(e => e.ADate)
                    .HasColumnType("date")
                    .HasColumnName("A_Date");

                entity.Property(e => e.ATime).HasColumnName("A_Time");

                entity.Property(e => e.Action).HasMaxLength(20);

                entity.Property(e => e.Functionality).HasMaxLength(20);

                entity.Property(e => e.LogAType)
                    .HasColumnType("tinyint")
                    .HasColumnName("LogA_Type")
                    .HasComment("1. USER_LOGIN\n2. USER_LOGOUT\n3. SAVE\n4. UPDATE\n5. DELETE\n6. SERVICE_CHECK\n7. MAINTENACE\n8..AUTOMAINTENANCE\n9. OPEN_Window\n10.CLOSE_WINDOW\n11. WARNING\n12. ");

                entity.Property(e => e.UId)
                    .HasMaxLength(45)
                    .HasColumnName("U_ID");
            });

            modelBuilder.Entity<TblLogActionDet>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_log_action_det");

                entity.Property(e => e.ColumnName)
                    .HasMaxLength(30)
                    .HasColumnName("Column_Name");

                entity.Property(e => e.LogAId).HasColumnName("LogA_ID");

                entity.Property(e => e.NwValue)
                    .HasMaxLength(30)
                    .HasColumnName("NW_VALUE");

                entity.Property(e => e.OldValue)
                    .HasMaxLength(30)
                    .HasColumnName("OLD_VALUE");
            });

            modelBuilder.Entity<TblLogError>(entity =>
            {
                entity.HasKey(e => e.ErrorId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_log_error");

                entity.Property(e => e.ErrorId).HasColumnName("Error_ID");

                entity.Property(e => e.Action)
                    .HasMaxLength(30)
                    .HasComment("armrotate/updn/rgt rotate");

                entity.Property(e => e.Assembly)
                    .HasMaxLength(30)
                    .HasComment("ARM/TRAY/ISE/STR/CRU");

                entity.Property(e => e.ErrDate)
                    .HasColumnType("date")
                    .HasColumnName("Err_Date");

                entity.Property(e => e.ErrTime).HasColumnName("Err_Time");

                entity.Property(e => e.ErrorCode).HasColumnName("Error_Code");

                entity.Property(e => e.ErrorDuring)
                    .HasColumnType("tinyint")
                    .HasColumnName("Error_During")
                    .HasComment("1. AUTO MAINTENANCE_STARTUP\n2. AUTO MAINTENANCE_SHUTDOWN\n3. SERVICE_CHECK\n4. MAINTENANCE\n5.PRE_RUN\n6. RUN\n7.POST_RUN\n8.USER_ACTIONS(Barcode-scan/volume-check)");

                entity.Property(e => e.ForCuv)
                    .HasColumnType("tinyint")
                    .HasComment("For which CUV(STR/ARM HIT)");

                entity.Property(e => e.ForPosition)
                    .HasColumnType("tinyint")
                    .HasComment("RGT/SMP Pos");

                entity.Property(e => e.Functionality)
                    .HasMaxLength(30)
                    .HasComment("RUN/CuvWash/tempsetting/servicecheck/preobe-clean");

                entity.Property(e => e.UId)
                    .HasColumnType("tinyint")
                    .HasColumnName("U_ID");
            });

            modelBuilder.Entity<TblLogSetting>(entity =>
            {
                entity.HasKey(e => e.SetId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_log_settings");

                entity.Property(e => e.SetId).HasColumnName("Set_ID");

                entity.Property(e => e.AssemblyId)
                    .HasColumnType("tinyint")
                    .HasColumnName("Assembly_ID")
                    .HasComment("ARM/TRAY/ISE/STR/CRU");

                entity.Property(e => e.NewValue)
                    .HasColumnType("tinyint")
                    .HasColumnName("New_Value");

                entity.Property(e => e.OldValue)
                    .HasColumnType("tinyint")
                    .HasColumnName("Old_Value");

                entity.Property(e => e.Path)
                    .HasColumnType("tinyint")
                    .HasColumnName("PATH");

                entity.Property(e => e.SetDate)
                    .HasColumnType("date")
                    .HasColumnName("Set_Date");

                entity.Property(e => e.SetTime).HasColumnName("Set_Time");

                entity.Property(e => e.UId)
                    .HasColumnType("tinyint")
                    .HasColumnName("U_ID");
            });

            modelBuilder.Entity<TblLot>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_lot");

                entity.Property(e => e.CUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("C_UID");

                entity.Property(e => e.Conc).HasColumnName("CONC");

                entity.Property(e => e.ConsumableType).HasColumnType("tinyint");

                entity.Property(e => e.DUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("D_UID");

                entity.Property(e => e.ExpMonth)
                    .HasColumnType("tinyint")
                    .HasColumnName("EXP_Month");

                entity.Property(e => e.ExpYear).HasColumnName("EXP_Year");

                entity.Property(e => e.IsActive).HasColumnType("tinyint");

                entity.Property(e => e.IsMultiPoint).HasColumnType("tinyint");

                entity.Property(e => e.LabMean).HasColumnName("Lab_Mean");

                entity.Property(e => e.LabSd).HasColumnName("Lab_SD");

                entity.Property(e => e.LastModUid)
                    .HasColumnType("tinyint")
                    .HasColumnName("LastMod_UID");

                entity.Property(e => e.LotId)
                    .HasColumnType("mediumint")
                    .HasColumnName("Lot_ID");

                entity.Property(e => e.LotNo)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Mean).HasColumnName("MEAN");

                entity.Property(e => e.MfgId)
                    .HasColumnType("tinyint")
                    .HasColumnName("MFG_ID");

                entity.Property(e => e.MfgMonth)
                    .HasColumnType("tinyint")
                    .HasColumnName("MFG_Month");

                entity.Property(e => e.MfgYear).HasColumnName("MFG_Year");

                entity.Property(e => e.NoOfStd).HasColumnType("tinyint");

                entity.Property(e => e.Od).HasColumnName("OD");

                entity.Property(e => e.Sd).HasColumnName("SD");

                entity.Property(e => e.StabilityEnd).HasColumnType("date");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");
            });

            modelBuilder.Entity<TblLotDet>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_lot_det");

                entity.Property(e => e.Conc).HasColumnName("CONC");

                entity.Property(e => e.LotId)
                    .HasColumnType("mediumint")
                    .HasColumnName("Lot_ID");

                entity.Property(e => e.StdOd).HasColumnName("STD_OD");
            });

            modelBuilder.Entity<TblPatient>(entity =>
            {
         


                entity.Property(e => e.Sex).HasColumnType("tinyint");
            });

            modelBuilder.Entity<TblPerson>(entity =>
            {
                entity.HasKey(e => e.PersonId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_person");

                entity.Property(e => e.PersonId).HasColumnName("Person_ID");



                entity.Property(e => e.PAddId).HasColumnName("P_AddID");

                entity.Property(e => e.PAgeDays)
                    .HasColumnName("P_AgeDays")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PAgeDisplay)
                    .HasMaxLength(20)
                    .HasColumnName("P_AgeDisplay");

                entity.Property(e => e.PAgeMonth)
                    .HasColumnName("P_AgeMonth")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PAgeYear)
                    .HasColumnName("P_AgeYear")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PDisplayName)
                    .HasMaxLength(20)
                    .HasColumnName("P_DisplayName");

                entity.Property(e => e.PDob)
                    .HasColumnType("date")
                    .HasColumnName("P_DOB");

                entity.Property(e => e.PFirstName)
                    .HasMaxLength(15)
                    .HasColumnName("P_FirstName");

                entity.Property(e => e.PHeightInCm)
                    .HasColumnName("P_HeightInCm")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PIdentityMark)
                    .HasMaxLength(30)
                    .HasColumnName("P_IdentityMark");

                entity.Property(e => e.PLastName)
                    .HasMaxLength(15)
                    .HasColumnName("P_LastName");

                entity.Property(e => e.PMidleName)
                    .HasMaxLength(15)
                    .HasColumnName("P_MidleName");

                entity.Property(e => e.PTitle)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("P_Title");

                entity.Property(e => e.PWeightInKg)
                    .HasColumnName("P_WeightInKG")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<TblProfileDet>(entity =>
            {
                //entity.HasKey(e => e.ProfileDetId)
                //    .HasName("PRIMARY");

                entity.ToTable("tbl_profile_det");

                //entity.Property(e => e.ProfileDetId).HasColumnName("Profile_Det_ID");

                entity.Property(e => e.ProId).HasColumnName("Profile_ID");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");
            });

            modelBuilder.Entity<TblReadsBlank>(entity =>
            {
                entity.HasKey(e => new { e.CuvNo, e.FilterId })
                    .HasName("PRIMARY");

                entity.ToTable("tbl_reads_blank");

                entity.Property(e => e.CuvNo).HasColumnType("tinyint");

                entity.Property(e => e.FilterId)
                    .HasColumnType("tinyint")
                    .HasColumnName("Filter_ID");

                entity.Property(e => e.Rcount).HasColumnName("RCount");

                entity.Property(e => e.ReadStatus)
                    .HasColumnType("tinyint")
                    .HasColumnName("Read_Status")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<TblReadsResult>(entity =>
            {
                entity.HasKey(e => e.ReadId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_reads_results");

                entity.Property(e => e.ReadId)
                    .HasColumnType("mediumint")
                    .HasColumnName("Read_ID");

                entity.Property(e => e.BlankCount)
                    .HasColumnType("mediumint")
                    .HasColumnName("Blank_Count");

                entity.Property(e => e.BlankOd).HasColumnName("Blank_OD");

                entity.Property(e => e.InstrumentId)
                    .HasColumnType("tinyint")
                    .HasColumnName("Instrument_ID");

                entity.Property(e => e.IsActive)
                    .HasColumnType("tinyint")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.ReadCount)
                    .HasColumnType("tinyint")
                    .HasColumnName("Read_Count");
            });

            modelBuilder.Entity<TblRefRange>(entity =>
            {
                entity.HasKey(e => e.RefId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_ref_ranges");

                entity.Property(e => e.RefId).HasColumnName("Ref_ID");

                entity.Property(e => e.AgeMax).HasColumnName("Age_Max");

                entity.Property(e => e.AgeMaxDays).HasColumnName("Age_Max_Days");

                entity.Property(e => e.AgeMin).HasColumnName("Age_Min");

                entity.Property(e => e.AgeMinDays)
                    .HasColumnName("Age_Min_Days")
                    .HasComment("FLAG SHOULD BE CALCULATED ON THIS COLOUM");

                entity.Property(e => e.AgeUnit)
                    .HasColumnType("tinyint")
                    .HasColumnName("Age_Unit")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Gender).HasColumnType("tinyint");

                entity.Property(e => e.RefHigh)
                    .HasColumnName("Ref_High")
                    .HasDefaultValueSql("'100'");

                entity.Property(e => e.RefLow).HasColumnName("Ref_Low");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");
            });

            modelBuilder.Entity<TblReflexDet>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_reflex_det");

                entity.Property(e => e.ReflexId)
                    .HasColumnType("tinyint")
                    .HasColumnName("Reflex_ID");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");
            });

            modelBuilder.Entity<TblResult>(entity =>
            {
                entity.HasKey(e => e.ResultId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_result");

                entity.Property(e => e.ResultId).HasColumnName("Result_ID");

                entity.Property(e => e.CalId).HasColumnName("Cal_ID");

                entity.Property(e => e.Cuv).HasColumnType("tinyint");

                entity.Property(e => e.DaySeqId).HasColumnName("DaySeqID");

                entity.Property(e => e.Flag)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Od).HasColumnName("OD");

                entity.Property(e => e.Od0).HasColumnName("OD0");

              

                entity.Property(e => e.SampleId).HasColumnName("Sample_ID");

                entity.Property(e => e.ScheduleId).HasColumnName("ScheduleID");

                entity.Property(e => e.SmpPos)
                    .HasColumnType("tinyint")
                    .HasColumnName("Smp_Pos");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");

                entity.Property(e => e.TestType).HasColumnName("Test_Type");
            });

            modelBuilder.Entity<TblResultsExtern>(entity =>
            {
                entity.HasKey(e => e.ScheduleId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_results_extern");

                entity.Property(e => e.ScheduleId).HasColumnName("ScheduleID");

          

                entity.Property(e => e.ExternTestId).HasColumnName("Extern_TestID");

                entity.Property(e => e.Flag).HasMaxLength(7);

           
                entity.Property(e => e.Result).HasMaxLength(15);
            });

            modelBuilder.Entity<TblRgtbotle>(entity =>
            {
                entity.HasKey(e => e.RbtlId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_rgtbotle");

                entity.Property(e => e.RbtlId)
                    .HasColumnType("mediumint")
                    .HasColumnName("RBtl_ID");

                entity.Property(e => e.Barcode).HasMaxLength(20);

                entity.Property(e => e.BtlNo)
                    .HasColumnType("tinyint")
                    .HasColumnName("Btl_No")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.BtlSize).HasColumnName("Btl_Size");

                entity.Property(e => e.BtlType)
                    .HasColumnType("tinyint")
                    .HasColumnName("Btl_Type");

                entity.Property(e => e.RgtBlank).HasColumnName("Rgt_blank");

                entity.Property(e => e.RgtType)
                    .HasColumnType("tinyint")
                    .HasColumnName("Rgt_Type")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.RlotId)
                    .HasColumnType("mediumint")
                    .HasColumnName("RLot_ID");

                entity.Property(e => e.StabilityEnd).HasColumnType("date");

                entity.Property(e => e.TestCnt).HasColumnName("Test_Cnt");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");
            });

            modelBuilder.Entity<TblRgtlot>(entity =>
            {
                entity.HasKey(e => e.RlotId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_rgtlot");

                entity.Property(e => e.RlotId)
                    .HasColumnType("mediumint")
                    .HasColumnName("RLot_ID");

                //entity.Property(e => e.CUid)
                //    .HasColumnType("tinyint")
                //    .HasColumnName("C_UID");

                entity.Property(e => e.ConsumableType)
                    .HasColumnType("tinyint")
                    .HasDefaultValueSql("'1'");

                //entity.Property(e => e.DUid)
                //    .HasColumnType("tinyint")
                //    .HasColumnName("D_UID");

                entity.Property(e => e.ExpMonth)
                    .HasColumnType("tinyint")
                    .HasColumnName("EXP_Month");

                entity.Property(e => e.ExpYear).HasColumnName("EXP_Year");

                //entity.Property(e => e.LastModUid)
                //    .HasColumnType("tinyint")
                //    .HasColumnName("LastMod_UID");

                entity.Property(e => e.LotNo)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.MfgId)
                    .HasColumnType("tinyint")
                    .HasColumnName("MFG_ID");

                entity.Property(e => e.MfgMonth)
                    .HasColumnType("tinyint")
                    .HasColumnName("MFG_Month");

                entity.Property(e => e.MfgYear).HasColumnName("MFG_Year");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");
            });

            modelBuilder.Entity<TblRgtpo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_rgtpos");

                entity.Property(e => e.RbtlId)
                    .HasColumnType("mediumint")
                    .HasColumnName("RBtl_ID");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");

                entity.Property(e => e.TrayId)
                    .HasColumnType("tinyint")
                    .HasColumnName("Tray_ID")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.TrayPos)
                    .HasColumnType("tinyint")
                    .HasColumnName("Tray_Pos");
            });

            modelBuilder.Entity<TblSchedule>(entity =>
            {
                entity.HasKey(e => e.ScheduleId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_schedule");

                entity.Property(e => e.ScheduleId).HasColumnName("Schedule_ID");

                entity.Property(e => e.Container).HasColumnType("tinyint");

                entity.Property(e => e.DiskNo).HasColumnType("tinyint");

                entity.Property(e => e.PatientId).HasColumnName("Patient_ID");

                entity.Property(e => e.Position).HasColumnType("tinyint");

                entity.Property(e => e.PrevCalId).HasColumnName("PrevCalID");

                entity.Property(e => e.SampleId).HasColumnName("Sample_ID");

                entity.Property(e => e.SchStatus).HasColumnType("tinyint");

                entity.Property(e => e.ScheduleType).HasColumnType("tinyint");

                entity.Property(e => e.SeqId).HasColumnName("SeqID");
            });

            modelBuilder.Entity<TblScheduledet>(entity =>
            {
                entity.HasKey(e => e.ScheduleDetId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_scheduledet");

                entity.Property(e => e.ScheduleDetId).HasColumnName("ScheduleDet_ID");

                entity.Property(e => e.DilRatio).HasColumnType("tinyint");

                entity.Property(e => e.LotId)
                    .HasColumnType("mediumint")
                    .HasColumnName("Lot_ID");

                entity.Property(e => e.Position).HasColumnType("tinyint");

                entity.Property(e => e.ReplicateNo).HasColumnType("tinyint");

                entity.Property(e => e.RunStatus).HasColumnType("tinyint");

                entity.Property(e => e.ScheduleId)
                    .HasColumnType("mediumint")
                    .HasColumnName("Schedule_ID");

                entity.Property(e => e.ScheduleType).HasColumnType("tinyint");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");

                entity.Property(e => e.TestType).HasColumnType("tinyint");
            });

            modelBuilder.Entity<TblSystemSetting>(entity =>
            {
                entity.HasKey(e => e.SystemSettingId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_system_settings");

                entity.Property(e => e.SystemSettingId).HasColumnName("System_SettingID");

                entity.Property(e => e.InstrumentBaudRate).HasColumnType("tinyint");

                entity.Property(e => e.InstrumentPort).HasMaxLength(20);
            });

            modelBuilder.Entity<TblTest>(entity =>
            {
                entity.HasKey(e => e.TestId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_test");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");

                //entity.Property(e => e.BlankingType)
                //    .HasColumnType("tinyint")
                //    .HasColumnName("Blanking_Type");

                //entity.Property(e => e.CUid)
                //    .HasColumnType("tinyint")
                //    .HasColumnName("C_UID");

                //entity.Property(e => e.CurveType).HasColumnType("tinyint");

                //entity.Property(e => e.DUid)
                //    .HasColumnType("tinyint")
                //    .HasColumnName("D_UID");

                entity.Property(e => e.Decimals)
                    .HasColumnType("tinyint")
                    .HasComment("for Result Display only");

                entity.Property(e => e.Direction)
                    .HasColumnType("tinyint")
                    .HasComment("Increasing/Decreasing");

                //entity.Property(e => e.LastModUid)
                //    .HasColumnType("tinyint")
                //    .HasColumnName("LastMod_UID");

                entity.Property(e => e.Method).HasColumnType("tinyint");

                entity.Property(e => e.R1strSpeed)
                    .HasColumnType("tinyint")
                    .HasColumnName("R1StrSpeed");

                entity.Property(e => e.R2strSpeed)
                    .HasColumnType("tinyint")
                    .HasColumnName("R2StrSpeed");

                //entity.Property(e => e.ReadTime1E)
                //    .HasColumnType("tinyint")
                //    .HasColumnName("ReadTime1_E");

                //entity.Property(e => e.ReadTime1S)
                //    .HasColumnType("tinyint")
                //    .HasColumnName("ReadTime1_S");

                //entity.Property(e => e.ReadTime2E)
                //    .HasColumnType("tinyint")
                //    .HasColumnName("ReadTime2_E");

                //entity.Property(e => e.ReadTime2S)
                //    .HasColumnType("tinyint")
                //    .HasColumnName("ReadTime2_S");

                //entity.Property(e => e.Reagents).HasColumnType("tinyint");

                //entity.Property(e => e.StdCnt)
                //    .HasColumnType("tinyint")
                //    .HasColumnName("Std_Cnt")
                //    .HasComment("No of Standards used for new calibration\n");

                entity.Property(e => e.TestCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.TestName)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.UniqueTestId)
                    .HasColumnName("Unique_Test_ID")
                    .HasComment("Uniqe Id given by Company for every test parameter for upload/download Testdeatils/standards/Reagents\n");

                entity.Property(e => e.Unit).HasColumnType("tinyint");

                entity.Property(e => e.W1).HasColumnType("tinyint");

                entity.Property(e => e.W2).HasColumnType("tinyint");
            });

            modelBuilder.Entity<TblTestCalcu>(entity =>
            {
                entity.HasKey(e => e.CalTestId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_test_calcu");

            

                entity.Property(e => e.CalTestId).HasColumnName("Cal_Test_ID");

                entity.Property(e => e.CalExpression)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("Cal_Expression");

              

        

                entity.Property(e => e.Decimals).HasColumnType("tinyint");

                entity.Property(e => e.RefHigh).HasColumnName("Ref_High");

                entity.Property(e => e.RefLow).HasColumnName("Ref_Low");

                entity.Property(e => e.Unit).HasColumnType("tinyint");
            });

            modelBuilder.Entity<TblTestCalib>(entity =>
            {
                //entity.HasKey(e => e.TestCalId)
                //    .HasName("PRIMARY");

                entity.ToTable("tbl_test_calib");

                //entity.Property(e => e.TestCalId).HasColumnName("Test_Cal_ID");

                entity.Property(e => e.DilRatio).HasColumnType("tinyint");

                entity.Property(e => e.IsBlank).HasColumnType("tinyint");

                entity.Property(e => e.Od).HasColumnName("OD");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");
            });

            modelBuilder.Entity<TblTestCarryOver>(entity =>
            {
                entity.HasKey(e => e.Idpk)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_test_carry_over");

                entity.Property(e => e.Idpk).HasColumnName("IDPK");

                entity.Property(e => e.Action).HasColumnType("tinyint");

                entity.Property(e => e.AfectedTestId).HasColumnName("Afected_Test_ID");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");

                entity.Property(e => e.WashType).HasColumnType("tinyint");
            });

            modelBuilder.Entity<TblTestDetail>(entity =>
            {
                //entity.HasKey(e => e.TestDetId)
                //    .HasName("PRIMARY");

                entity.ToTable("tbl_test_details");

                //entity.Property(e => e.TestDetId).HasColumnName("Test_Det_ID");

                entity.Property(e => e.AbsLimitHigh).HasColumnName("Abs_Limit_High");

                entity.Property(e => e.AbsLimitLow).HasColumnName("Abs_Limit_Low");

                entity.Property(e => e.AutoDil).HasColumnName("Auto_Dil");

                entity.Property(e => e.CalBlanking).HasColumnType("tinyint");

                entity.Property(e => e.ControlInterval).HasColumnType("tinyint");

                entity.Property(e => e.LisName)
                    .HasMaxLength(20)
                    .HasColumnName("LIS_Name")
                    .HasComment("will be TestCode by default\n");

                entity.Property(e => e.PrintName)
                    .HasMaxLength(30)
                    .HasComment("will be TestName by default\n");

                entity.Property(e => e.RbHigh).HasColumnName("RB_HIGH");

                entity.Property(e => e.RbLow).HasColumnName("RB_LOW");

                entity.Property(e => e.ReplicateCntrl)
                    .HasColumnType("tinyint")
                    .HasColumnName("Replicate_Cntrl")
                    .HasDefaultValueSql("'2'");

                entity.Property(e => e.ReplicateSmp)
                    .HasColumnType("tinyint")
                    .HasColumnName("Replicate_Smp")
                    .HasDefaultValueSql("'2'");

                entity.Property(e => e.ReplicateStd)
                    .HasColumnType("tinyint")
                    .HasColumnName("Replicate_Std")
                    .HasDefaultValueSql("'2'");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");
            });

            modelBuilder.Entity<TblTestExtern>(entity =>
            {
                entity.HasKey(e => e.ExternTestId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_test_extern");

                entity.Property(e => e.ExternTestId).HasColumnName("Extern_TestID");

                entity.Property(e => e.Decimals)
                    .HasColumnType("tinyint")
                    .HasDefaultValueSql("'3'");

                entity.Property(e => e.ResultType)
                    .HasColumnType("tinyint")
                    .HasColumnName("Result_Type")
                    .HasComment("1. Numeric(Quantitative)\n2. chars(Qualitative)");

                entity.Property(e => e.TestCode)
                    .HasMaxLength(20)
                    .HasColumnName("Test_Code");

                entity.Property(e => e.TestName)
                    .HasMaxLength(45)
                    .HasColumnName("Test_Name");
            });

            modelBuilder.Entity<TblTestProfile>(entity =>
            {
                entity.HasKey(e => e.ProfileId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_test_profile");

                entity.Property(e => e.ProfileId)
                    .HasColumnType("tinyint")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("Profile_ID");

             

                entity.Property(e => e.ProfileCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.ProfileName).HasMaxLength(20);
            });

            modelBuilder.Entity<TblTestReflex>(entity =>
            {
                entity.HasKey(e => e.ReflexId)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_test_reflex");

                entity.Property(e => e.ReflexId)
                    .HasColumnType("tinyint")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("Reflex_ID");

                entity.Property(e => e.ReflexType)
                    .HasColumnType("tinyint")
                    .HasColumnName("Reflex_Type");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");
            });

            modelBuilder.Entity<TblTestVolume>(entity =>
            {
                //entity.HasKey(e => e.TestVolId)
                //    .HasName("PRIMARY");

                entity.ToTable("tbl_test_volumes");

                //entity.Property(e => e.TestVolId).HasColumnName("Test_Vol_ID");

                entity.Property(e => e.DilRatioD)
                    .HasColumnType("tinyint")
                    .HasColumnName("DilRatio_D")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.DilRatioI)
                    .HasColumnType("tinyint")
                    .HasColumnName("DilRatio_I")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.DilRatioN)
                    .HasColumnType("tinyint")
                    .HasColumnName("DilRatio_N")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.R1vol)
                    .HasColumnName("R1Vol")
                    .HasDefaultValueSql("'150'");

                entity.Property(e => e.R2vol)
                    .HasColumnName("R2Vol")
                    .HasDefaultValueSql("'50'");

                entity.Property(e => e.R3vol)
                    .HasColumnName("R3Vol")
                    .HasDefaultValueSql("'50'");

                entity.Property(e => e.StdVol)
                    .HasColumnType("tinyint")
                    .HasDefaultValueSql("'2'");

                entity.Property(e => e.SvolD)
                    .HasColumnName("SVol_D")
                    .HasDefaultValueSql("'2'");

                entity.Property(e => e.SvolI)
                    .HasColumnName("SVol_I")
                    .HasDefaultValueSql("'2'");

                entity.Property(e => e.SvolN)
                    .HasColumnName("SVol_N")
                    .HasDefaultValueSql("'2'");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");
            });

            modelBuilder.Entity<TblUser>(entity =>
            {
                entity.HasKey(e => e.UidPk)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_user");

                entity.Property(e => e.UidPk)
                    .HasColumnType("tinyint")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("UID_PK");

                
                entity.Property(e => e.PersonId).HasColumnName("Person_ID");

                entity.Property(e => e.ULoginId)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("U_LoginID");

                entity.Property(e => e.UPwd)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("U_Pwd");

                entity.Property(e => e.UPwdHint)
                    .HasMaxLength(30)
                    .HasColumnName("U_PwdHint");

                entity.Property(e => e.UType)
                    .HasColumnType("tinyint")
                    .HasColumnName("U_Type");
            });

            modelBuilder.Entity<TblUserright>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_userrights");

                entity.Property(e => e.FormId)
                    .HasColumnType("tinyint")
                    .HasColumnName("FormID");

                entity.Property(e => e.FunctionId)
                    .HasColumnType("tinyint")
                    .HasColumnName("FunctionID");

                entity.Property(e => e.IsAllowed).HasColumnType("tinyint");

                entity.Property(e => e.Seqid).HasColumnName("SEQID");

                entity.Property(e => e.UserId)
                    .HasColumnType("tinyint")
                    .HasColumnName("UserID");
            });

            modelBuilder.Entity<TblWorklist>(entity =>
            {
                entity.HasKey(e => e.SeqNo)
                    .HasName("PRIMARY");

                entity.ToTable("tbl_worklist");

                entity.Property(e => e.Cuv).HasColumnType("tinyint");

                entity.Property(e => e.DilCuv).HasColumnType("tinyint");

                entity.Property(e => e.DilRatio).HasColumnType("tinyint");

                entity.Property(e => e.DilSeqNo).HasColumnType("tinyint");

                entity.Property(e => e.Flag).HasMaxLength(30);

                entity.Property(e => e.LisStatus).HasColumnType("tinyint");

                entity.Property(e => e.LotId)
                    .HasColumnType("mediumint")
                    .HasColumnName("Lot_ID");

                entity.Property(e => e.Od).HasColumnName("OD");

                entity.Property(e => e.R1pos)
                    .HasColumnType("tinyint")
                    .HasColumnName("R1Pos");

                entity.Property(e => e.R1short)
                    .HasColumnType("tinyint")
                    .HasColumnName("R1Short");

                entity.Property(e => e.R1time).HasColumnName("R1Time");

                entity.Property(e => e.R1vol).HasColumnName("R1Vol");

                entity.Property(e => e.R2pos)
                    .HasColumnType("tinyint")
                    .HasColumnName("R2Pos");

                entity.Property(e => e.R2short)
                    .HasColumnType("tinyint")
                    .HasColumnName("R2Short");

                entity.Property(e => e.R2time).HasColumnName("R2Time");

                entity.Property(e => e.R2vol).HasColumnName("R2Vol");

                entity.Property(e => e.R3pos)
                    .HasColumnType("tinyint")
                    .HasColumnName("R3Pos");

                entity.Property(e => e.R3short)
                    .HasColumnType("tinyint")
                    .HasColumnName("R3Short");

                entity.Property(e => e.R3time).HasColumnName("R3Time");

                entity.Property(e => e.R3vol).HasColumnName("R3Vol");

                entity.Property(e => e.ReplicateNo).HasColumnType("tinyint");

                entity.Property(e => e.Result).HasColumnType("decimal(10,0)");

                entity.Property(e => e.ResultId).HasColumnName("ResultID");

                entity.Property(e => e.RunStatus).HasColumnType("tinyint");

                entity.Property(e => e.RunTestType).HasColumnType("tinyint");

                entity.Property(e => e.SampleVol).HasColumnType("tinyint");

                entity.Property(e => e.ScheduleId).HasColumnName("Schedule_ID");

                entity.Property(e => e.ScheduleType).HasColumnType("tinyint");

                entity.Property(e => e.SmpPos)
                    .HasColumnType("tinyint")
                    .HasColumnName("Smp_Pos");

                entity.Property(e => e.SmpShort)
                    .HasColumnType("tinyint")
                    .HasColumnName("SMP_Short");

                entity.Property(e => e.TestId).HasColumnName("Test_ID");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
