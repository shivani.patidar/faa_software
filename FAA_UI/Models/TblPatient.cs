﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblPatient
{
    public int PatId { get; set; }

    public string PatientId { get; set; }

    public string PatName { get; set; }

    public DateTime? Dob { get; set; }

    public string Sex { get; set; }

    public float? Height { get; set; }

    public float? Weight { get; set; }

    public string Address { get; set; }

    public bool? SoftDelete { get; set; }

    public sbyte? CreateBy { get; set; }

    public DateTime? CreateDate { get; set; }

    public sbyte? ModifyBy { get; set; }

    public DateTime? ModifyDate { get; set; }
}
