﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblReflexDet
{
    public int RflxDetlId { get; set; }

    public sbyte ReflexId { get; set; }

    public short TestId { get; set; }
}
