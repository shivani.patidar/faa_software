﻿using System;
using System.Collections.Generic;

namespace FAA_UI.Models;

public partial class TblDirectory
{
    public short DictId { get; set; }

    public string DictDesc { get; set; }

    public string DictRemark { get; set; }

    public bool IsEditable { get; set; }

    public sbyte IsActive { get; set; }

    public sbyte CUid { get; set; }

    public sbyte LastModUid { get; set; }

    public sbyte DUid { get; set; }
}
