global using System.ComponentModel.DataAnnotations;
global using Microsoft.EntityFrameworkCore;
global using FAA_UI.Models;
using MySql.EntityFrameworkCore.Extensions;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

//var connectionString = builder.Configuration.GetConnectionString("AppConnection");

//builder.Services.AddDbContext<FAA_DbContext>(options =>
//{
//    options.UseMySQL(connectionString);
//});


builder.Services.AddSession();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
    app.UseHttpsRedirection();
}
static DbContextOptions GetOptions(string connectionString)
{
    return SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionString).Options;
}


//static void ConfigureServices(IServiceCollection services)
//{

//    services.AddDistributedMemoryCache();
//    services.AddSession(options =>
//    {
//        options.IdleTimeout = TimeSpan.FromMinutes(1);//You can set Time   
//    });
//    services.AddMvc();
//}

app.UseSession();

app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

//app.UseSession();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Admin}/{action=Login}/{id?}"
);
app.Run();
