using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using FAA_UI.Models;
using FAA_UI.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace FAA_UI.Controllers
{
    public class ReflexController:Controller
    {
        FAA_DbContext dbContext=new FAA_DbContext(); 
            
        public async Task<IActionResult> Index(int? ReflexID)
        {
            var viewModel=new ReflexViewModel();
            if(ReflexID==null)ReflexID=1;
            viewModel.tblTestListModel=new TestListViewModel();
            viewModel.tblReflexs= dbContext.TblTestReflices.DefaultIfEmpty();
            viewModel.tblReflex=await dbContext.TblTestReflices.Where(x=>x.ReflexId==ReflexID).FirstOrDefaultAsync();
            viewModel.tblReflexDet= await dbContext.TblReflexDets.Where(x=>x.ReflexId==ReflexID).ToListAsync();
            foreach(var detail in viewModel.tblReflexDet)
            {
                foreach(var test in viewModel.tblTestListModel.TestList)
                {
                    if(detail.TestId==test.TestId)
                    {
                        test.isSelected=true;
                    }
                }
            }

            viewModel.selectedReflexID=Convert.ToInt32(ReflexID);
            return View(viewModel);
        }

        [HttpPost]
        public async void Update(string viewData)
        {
            dynamic data=JObject.Parse(viewData);
            var tblReflex=new TblTestReflex();
            var reflexid=0;
            short[] selectedIds=new short[150];
            int i=0;
            foreach(var id in data["selectedIds"])
            {
                selectedIds[i]=Convert.ToInt16(id.Value);
                i++;
            }
            if(Convert.ToString(data["reflexId"]).Length>0)
            {
                tblReflex.ReflexId=data["reflexId"];
                reflexid=tblReflex.ReflexId;
            }
            else
                tblReflex.ReflexId=0;
            tblReflex.TestId=data["testId"];
            tblReflex.LowValue=data["lowValue"];
            tblReflex.HighValue=data["highValue"];
            tblReflex.ReflexType=data["reflexType"];  
            //tblReflex.LastModUid=1;
            //tblReflex.CUid=1;
            //tblReflex.DUid=1;
            
            if(tblReflex.ReflexId==0)
            {
                dbContext.TblTestReflices.Add(tblReflex);
                dbContext.SaveChanges();
                reflexid= (await dbContext.TblTestReflices.Where(x=>x.ReflexId==tblReflex.ReflexId).FirstOrDefaultAsync()).ReflexId;
                foreach(var id in selectedIds)
                {
                    if(id==0)break;
                    var tblDetail=new TblReflexDet();
                    //tblDetail.ReflexId=Convert.ToByte(reflexid);
                    tblDetail.TestId=id;
                    dbContext.TblReflexDets.Add(tblDetail);                   
                }
            }
            else
            {
                dbContext.Entry(tblReflex).State = EntityState.Modified;
                foreach(var id in selectedIds)
                {
                    if(id==0)break;
                    if(dbContext.TblReflexDets.Where(x=>x.ReflexId==tblReflex.ReflexId && x.TestId==id).Count()<=0)
                    {
                        var tblDetail=new TblReflexDet();
                        //tblDetail.ReflexId=Convert.ToByte(reflexid);
                        tblDetail.TestId=id;
                        dbContext.TblReflexDets.Add(tblDetail); 
                    }
                }
                var tbl=dbContext.TblReflexDets.Where(x=>x.ReflexId==tblReflex.ReflexId);
                var tblRemove=tbl.Where(x => !selectedIds.Contains(x.TestId)).ToList();
                dbContext.TblReflexDets.RemoveRange(tblRemove);
            }
            dbContext.SaveChanges();
            //return RedirectToAction("Index","TestProfile",new { ProfileID = profid});
        }

    }
}