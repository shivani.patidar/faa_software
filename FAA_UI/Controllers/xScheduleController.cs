using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using FAA_UI.Models;
using FAA_UI.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using SerialCommunication;
using FAA_INSTRUMENT;
using System.Threading.Tasks;
using FAA_COMMON;


namespace FAA_UI.Controllers
{
    public class xScheduleController:Controller
    {
        FAA_DbContext dbContext=new FAA_DbContext();
           
        public async Task<IActionResult> Index(int? ScheduleID)
        {
            Instrument_Constants.ReagentCount = 40;
            Instrument_Constants.CuvetCount = 70;
            Instrument_Constants.SampleCount = 40;

            Cls_Common.seqId=1;

            FillDropDowns();

            var viewModel=new ScheduleViewModel();
            if(ScheduleID==null)ScheduleID=1;
            viewModel.tblTestListModel=new TestListViewModel();
            viewModel.tblSchedules= dbContext.TblSchedules.DefaultIfEmpty();
            viewModel.tblSchedule=await dbContext.TblSchedules.Where(x=>x.ScheduleId==ScheduleID).FirstOrDefaultAsync();
            viewModel.tblScheduledets= await dbContext.TblScheduledets.Where(x=>x.ScheduleId==ScheduleID).ToListAsync();
            if(viewModel.tblSchedule!=null)
            {
                //viewModel.tblSample=await dbContext.TblSamples.Where(x=>x.SampleId==viewModel.tblSchedule.SampleId).FirstOrDefaultAsync();
                //viewModel.tblPatient=await dbContext.TblPatients.Where(x=>x.PatientId==viewModel.tblSchedule.PatientId).FirstOrDefaultAsync();    
            }
           
            foreach(var detail in viewModel.tblScheduledets)
            {
                foreach(var test in viewModel.tblTestListModel.TestList)
                {
                    if(detail.TestId==test.TestId)
                    {
                        test.isSelected=true;
                    }
                }
            }
            viewModel.selectedScheduleId=Convert.ToInt32(ScheduleID);
            return View(viewModel);
        }

        public void FillDropDowns()
        {
            DataDictionaryController objDataDictionary=new DataDictionaryController();
            ViewBag.SamplePos=objDataDictionary.GetDirList(ENM_Dict_Type.DT_SamplePos);
            ViewBag.SampleType=objDataDictionary.GetDirList(ENM_Dict_Type.DT_SampleType);
            ViewBag.ContainerType=objDataDictionary.GetDirList(ENM_Dict_Type.DT_ContainerType);
            List<SelectListItem> listSelectListItems = new List<SelectListItem>(); 
            listSelectListItems.Add(new SelectListItem(){Value="PAT1",Text="PAT1"});  
            listSelectListItems.Add(new SelectListItem(){Value="PAT2",Text="PAT2"});
            ViewBag.PatientIdList= listSelectListItems;
        }

        public async Task<ActionResult> Details(int Id)  
        {  
            TblPatient patient = new TblPatient();  
            patient = await dbContext.TblPatients.Where(x=>x.PatId==Id).FirstOrDefaultAsync(); 
            return PartialView("_AddPatient",patient);  
        }

        [HttpPost]
        public async void Update(string viewData) 
        {
            dynamic data=JObject.Parse(viewData);
            var tblSchedule=new TblSchedule();
            var schid=0;
            short[] selectedIds=new short[300];
            int i=0;
            foreach(var id in data["selectedIds"])
            {
                selectedIds[i]=Convert.ToInt16(id.Value);
                i++;
            }
            if(data["ScheduleId"]!=null )
            {
                if(Convert.ToString(data["ScheduleId"]).Length>0)
                {
                    tblSchedule.ScheduleId=data["ScheduleId"];
                    schid=tblSchedule.ScheduleId;
                }
            }
            else
                tblSchedule.ScheduleId=0;

            //tblSchedule.PatientId=data["PatientId"];
            tblSchedule.PatientId=1;
            tblSchedule.Position=data["Position"];  
            //tblSchedule.ScheduleType=Convert.ToByte(ENUM_SCHEDULETYPE.NORMAL);
            tblSchedule.Container=data["Container"];
            tblSchedule.SchStatus=0;
            tblSchedule.SchTime=DateTime.Now;
            tblSchedule.SampleId=data["SampleId"];
            //tblSchedule.PrevCalId=data["PrevCalId"];
            tblSchedule.SeqId=Cls_Common.seqId++;
        
            
            if(tblSchedule.ScheduleId==0)
            {
                dbContext.TblSchedules.Add(tblSchedule);
                dbContext.SaveChanges();
                schid= (await dbContext.TblSchedules.Where(x=>x.ScheduleId==tblSchedule.ScheduleId).FirstOrDefaultAsync()).ScheduleId;
                foreach(var id in selectedIds)
                {
                    if(id==0)break;
                    var tblDetail=new TblScheduledet();
                    //tblDetail.LotId=data["LotId"];
                    //tblDetail.DilRatio=data["DilRatio"];
                    //tblDetail.ReplicateNo=data["ReplicateNo"];

                    tblDetail.DilRatio=1;
                    tblDetail.ReplicateNo=1;
                    //tblDetail.RunStatus=;
                   // tblDetail.ScheduleType=data["ScheduleType"];
                    tblDetail.ScheduleType=1;

                    //tblDetail.TestType=data["TestType"];
                     tblDetail.TestType=0;
                    tblDetail.ScheduleId=schid;
                    tblDetail.TestId=id;
                    dbContext.TblScheduledets.Add(tblDetail);                   
                }
            }
            else
            {
                dbContext.Entry(tblSchedule).State = EntityState.Modified;
                foreach(var id in selectedIds)
                {
                    if(id==0)break;
                    if(dbContext.TblScheduledets.Where(x=>x.ScheduleId==tblSchedule.ScheduleId && x.TestId==id).Count()<=0)
                    {
                        var tblDetail=new TblScheduledet();
                        tblDetail.ScheduleId=tblSchedule.ScheduleId;
                        tblDetail.TestId=id;
                       // tblDetail.LotId=data["LotId"];
                        //tblDetail.DilRatio=data["DilRatio"];
                        tblDetail.DilRatio=1;
                       // tblDetail.ReplicateNo=data["ReplicateNo"];
                        tblDetail.ReplicateNo=1;
                        //tblDetail.RunStatus=data["RunStatus"];
                        //tblDetail.ScheduleType=data["ScheduleType"];
                        tblDetail.ScheduleType=1;
                        //tblDetail.TestType=data["TestType"];
                        tblDetail.TestType=0;
                        dbContext.TblScheduledets.Add(tblDetail);
                    }
                }
                var tbl=dbContext.TblScheduledets.Where(x=>x.ScheduleId==tblSchedule.ScheduleId);
                var tblRemove=tbl.Where(x => !selectedIds.Contains(x.TestId)).ToList();
                dbContext.TblScheduledets.RemoveRange(tblRemove);
            }
            dbContext.SaveChanges();
            //return RedirectToAction("Index","TestProfile",new { ProfileID = profid});
        } 
    }
}