﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Filters;
using MySqlX.XDevAPI.Common;
using Org.BouncyCastle.Crypto.Tls;
using System.Collections;
using System.Linq.Expressions;
using System.Security.Cryptography;
using NuGet.Protocol;
using System.IO;
using static System.Net.Mime.MediaTypeNames;
using MySqlX.XDevAPI.Relational;
using static System.Runtime.InteropServices.JavaScript.JSType;
using Org.BouncyCastle.Utilities;
using FAA_UI.Models;
using System.Linq;

namespace FAA_UI.Controllers
{
    public class ReportsController : Controller
    {
        DbFaaSoftwareContext dbContext = new DbFaaSoftwareContext();
        private IConfiguration configuration;

        public static int? UserId = 0;
        public static string UserName = "";

        public ReportsController(IConfiguration con)
        {
            configuration = con;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetReportMst()
        {
            try
            {
                var Test = (from e in dbContext.TblTests
                                join s in dbContext.TblTestDetails
                                on e.TestId equals s.TestId
                                select new
                                {
                                    TestId =  e.TestId,
                                    TestCode = e.TestCode,
                                    DispIndex = s.DispIndex,
                                    SoftDelete = (bool?)e.SoftDelete
                                }).Where(x => x.SoftDelete != true).OrderBy(x => x.DispIndex).ToList();


                var TestExternal = (from TstExter in dbContext.TblTestExterns
                                    select new
                                    {
                                        TestId = TstExter.ExternTestId,
                                        TestCode = TstExter.TestCode,
                                        DispIndex = (short?)0,
                                        SoftDelete = TstExter.SoftDelete
                                    }).Where(x => x.SoftDelete != true).OrderBy(x => x.DispIndex).ToList();
                var TestCode = Test.Concat(TestExternal).ToList();


                //  var TestCode = await dbContext.TblTests.Where(x => x.SoftDelete != true).ToListAsync();
                var Patient = await dbContext.TblPatients.Where(x => x.SoftDelete != true).ToListAsync();
              
                var Sample = await dbContext.TblSamples.Where(x => x.SoftDelete != true).ToListAsync();

                var Qualitative = await dbContext.TblDirectoryQualitatives.Where(x => x.SoftDelete != true).ToListAsync();


                //GlobalClass.UserActivityLogFile(UserName, "Profile Get Test Code List", "Exit");
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reports", "Entry");
                return Json(new { TestCode, Patient, Sample, TestExternal, Qualitative });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Reports", "Exist", ex.Message);
                return Json(false);
            }
        }

        [HttpPost]
        public async Task<JsonResult> GetResultReport(int[] TestId, int[] PatientId, int[] SampleId, DateTime? FormDate, DateTime? ToDate)
        {
            try
            {
                int fd = TestId.Length;
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reports", "Entry");

                if (FormDate == null && ToDate != null) FormDate = ToDate;
                if (FormDate != null && ToDate == null) ToDate = FormDate;
                //FormDate != null  ? Rst.ResultDateTime == FormDate :
                //ToDate != null ? Rst.ResultDateTime == ToDate :
                //Rst.ResultId != 0)

                var Result =  (from Rst in dbContext.TblResults
                                      join Tst in dbContext.TblTests
                                      on Rst.TestId equals Tst.TestId
                                      join Ref in dbContext.TblRefRanges
                                     on Rst.TestId equals Ref.TestId
                                      join Smp in dbContext.TblSamples
                                         on Rst.SampleId equals Smp.SmpId

                               join it in dbContext.TblPatients
                              on Smp.PatientId equals it.PatId into ut
                               from Patnt in ut.DefaultIfEmpty()
                                   //join Patnt in dbContext.TblPatients
                                   //     on Smp.PatientId equals Patnt.PatId

                               join unt in dbContext.TblUnits
                                    on Tst.Unit equals unt.UntId
                                      where (TestId.Length > 0 ? TestId.Contains(Rst.TestId) : Rst.ResultId != 0) && 
                                      (PatientId.Length > 0 ? PatientId.Contains(Smp.PatientId) : Rst.ResultId != 0) &&
                                      (SampleId.Length > 0 ? SampleId.Contains(Smp.SmpId) : Rst.ResultId != 0) &&
                                      (
                                     (FormDate != null && ToDate != null) ? (Rst.ResultDateTime.Date >= Convert.ToDateTime(FormDate).Date && Rst.ResultDateTime.Date <= Convert.ToDateTime(ToDate).Date) : Rst.ResultId != 0)
                                      select new
                                      {
                                          ResultId = Rst.ResultId,
                                          RunDate = Rst.ResultDateTime,
                                          PatientId = Smp.PatientId,
                                          TestCode = Tst.TestCode,
                                          ResultValue = Rst.ResultValue.ToString(),
                                          Flag = Rst.Flag,
                                          Unit = unt.UntName,
                                          Remark = Smp.SmpRemark,
                                          RefLow = (Nullable<float>)Ref.RefLow,
                                          RefHigh = (Nullable<float>)Ref.RefHigh,
                                          SmpId = Smp.SmpId ,

                                          //  PatientName = Patnt.PatName + "(" + Patnt.PatientId + "), Sample ID "+ Smp.SmpId + "<a class='btn' data-bs-toggle='tooltip' data-bs-html='true' data-bs-original-title='Edit Patient Details' onclick = UpdateShowPatient(" + Patnt.PatId + "," + Smp.SmpId + ") style='color:#656FEA'> <b><i class='bi bi-pencil-fill'></i></b></a><a class='btn' onclick=ResultPDF(" + Patnt.PatId + "," + Smp.SmpId + ") data-bs-toggle='tooltip' data-bs-html='true' data-bs-original-title='Genrate PDF' style='color:red'><b><i class='bi bi-printer'></i></b></a><a class='btn' style='color:green' onclick=AddExternalResult(" + Patnt.PatId + "," + Smp.SmpId + ",'" + Patnt.PatientId + "') data-bs-toggle='tooltip' data-bs-html='true' title='Add External Result'><b><i class='bi bi-plus-lg'></i></b></a>",
                                          PatientName = Patnt != null ? Patnt.PatName + "(" + Patnt.PatientId + "), Sample ID " + Smp.SmpId + "<a class='btn' data-bs-toggle='tooltip' data-bs-html='true' data-bs-original-title='Edit Patient Details' onclick = UpdateShowPatient(" + Smp.SmpId + ") style='color:#656FEA'> <b><i class='bi bi-pencil-fill'></i></b></a><a class='btn' onclick=ResultPDF(" + Smp.SmpId + ") data-bs-toggle='tooltip' data-bs-html='true' data-bs-original-title='Genrate PDF' style='color:red'><b><i class='bi bi-printer'></i></b></a><a class='btn' style='color:green' onclick=AddExternalResult(" + Smp.SmpId + ",'" + Patnt.PatientId + "') data-bs-toggle='tooltip' data-bs-html='true' title='Add External Result'><b><i class='bi bi-plus-lg'></i></b></a>" : "Sample ID " + Smp.SmpId + "<a class='btn' data-bs-toggle='tooltip' data-bs-html='true' data-bs-original-title='Edit Patient Details' onclick = UpdateShowPatient(" + Smp.SmpId + ") style='color:#656FEA'> <b><i class='bi bi-pencil-fill'></i></b></a><a class='btn' onclick=ResultPDF(" + Smp.SmpId + ") data-bs-toggle='tooltip' data-bs-html='true' data-bs-original-title='Genrate PDF' style='color:red'><b><i class='bi bi-printer'></i></b></a><a class='btn' style='color:green' onclick=AddExternalResult(" + Smp.SmpId + ",'" + Patnt.PatientId + "') data-bs-toggle='tooltip' data-bs-html='true' title='Add External Result'><b><i class='bi bi-plus-lg'></i></b></a>",
                                          p_barcode = Patnt.PatientId,
                                      }).ToList();



                var ExternalResult = (from ExtRst in dbContext.TblResultsExterns
                                      join ExtTst in dbContext.TblTestExterns
                                      on ExtRst.ExternTestId equals ExtTst.ExternTestId
                                      // join Ref in dbContext.TblRefRanges
                                      //on Rst.TestId equals Ref.TestId
                                      join Smp in dbContext.TblSamples
                                         on ExtRst.SmpId equals Smp.SmpId
                                      join Patnt in dbContext.TblPatients
                                    on Smp.PatientId equals Patnt.PatId
                                      join unt in dbContext.TblUnits
                                      on ExtTst.Unit equals unt.UntId
                                      where (TestId.Length > 0 ? TestId.Contains(ExtRst.ExternTestId) : ExtRst.ScheduleId != 0) && (PatientId.Length > 0 ? PatientId.Contains(Smp.PatientId) : ExtRst.ScheduleId != 0) && (SampleId.Length > 0 ? SampleId.Contains(Smp.SmpId) : ExtRst.ScheduleId != 0) && ExtRst.SoftDelete != true && (
                                     (FormDate != null && ToDate != null) ? (ExtRst.ResultDate.Date >= Convert.ToDateTime(FormDate).Date && ExtRst.ResultDate.Date <= Convert.ToDateTime(ToDate).Date) : ExtRst.ScheduleId != 0)
                                      select new
                                      {
                                          ResultId = ExtRst.ScheduleId,
                                          RunDate = ExtRst.ResultDate,
                                          PatientId = Smp.PatientId,
                                          TestCode = ExtTst.TestCode,
                                          ResultValue = ExtRst.Result,
                                          Flag = ExtRst.Flag,
                                          Unit = unt.UntName,
                                          Remark = Smp.SmpRemark,
                                          RefLow = ExtTst.RefLow,
                                          RefHigh = ExtTst.RefHigh,
                                          SmpId = Smp.SmpId,
                                          //PatientName = Patnt.PatName + "(" + Patnt.PatientId + "), Sample ID " + Smp.SmpId + "<a class='btn' data-bs-toggle='tooltip' data-bs-html='true' data-bs-original-title='Edit Patient Details' onclick = UpdateShowPatient(" + Patnt.PatId + "," + Smp.SmpId + ") style='color:#656FEA'> <b><i class='bi bi-pencil-fill'></i></b></a><a class='btn' onclick=ResultPDF(" + Patnt.PatId + "," + Smp.SmpId + ") data-bs-toggle='tooltip' data-bs-html='true' data-bs-original-title='Genrate PDF' style='color:red'><b><i class='bi bi-printer'></i></b></a><a class='btn' style='color:green' onclick=AddExternalResult(" + Patnt.PatId + "," + Smp.SmpId + ",'" + Patnt.PatientId + "') data-bs-toggle='tooltip' data-bs-html='true' title='Add External Result'><b><i class='bi bi-plus-lg'></i></b></a>",
                                          PatientName = Patnt != null ? Patnt.PatName + "(" + Patnt.PatientId + "), Sample ID " + Smp.SmpId + "<a class='btn' data-bs-toggle='tooltip' data-bs-html='true' data-bs-original-title='Edit Patient Details' onclick = UpdateShowPatient(" + Smp.SmpId + ") style='color:#656FEA'> <b><i class='bi bi-pencil-fill'></i></b></a><a class='btn' onclick=ResultPDF(" + Smp.SmpId + ") data-bs-toggle='tooltip' data-bs-html='true' data-bs-original-title='Genrate PDF' style='color:red'><b><i class='bi bi-printer'></i></b></a><a class='btn' style='color:green' onclick=AddExternalResult(" + Smp.SmpId + ",'" + Patnt.PatientId + "') data-bs-toggle='tooltip' data-bs-html='true' title='Add External Result'><b><i class='bi bi-plus-lg'></i></b></a>" : "Sample ID " + Smp.SmpId + "<a class='btn' data-bs-toggle='tooltip' data-bs-html='true' data-bs-original-title='Edit Patient Details' onclick = UpdateShowPatient(" + Smp.SmpId + ") style='color:#656FEA'> <b><i class='bi bi-pencil-fill'></i></b></a><a class='btn' onclick=ResultPDF(" + Smp.SmpId + ") data-bs-toggle='tooltip' data-bs-html='true' data-bs-original-title='Genrate PDF' style='color:red'><b><i class='bi bi-printer'></i></b></a><a class='btn' style='color:green' onclick=AddExternalResult(" + Smp.SmpId + ",'" + Patnt.PatientId + "') data-bs-toggle='tooltip' data-bs-html='true' title='Add External Result'><b><i class='bi bi-plus-lg'></i></b></a>",
                                          p_barcode = Patnt.PatientId,
                                      }).ToList();

                var Response = Result.Concat(ExternalResult).ToList();


                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reports", "Exist");
                return Json(Response);
                //}
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Reports", "Exist", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> ResultReportPDF(int? SampleId)
        {
            try
            {
                var ReportPDF = (from Rst in dbContext.TblResults
                                 join Tst in dbContext.TblTests
                                 on Rst.TestId equals Tst.TestId
                                 join Ref in dbContext.TblRefRanges
                                on Rst.TestId equals Ref.TestId
                                 join Smp in dbContext.TblSamples
                                    on Rst.SampleId equals Smp.SmpId
                                 join Patnt in dbContext.TblPatients
                               on Smp.PatientId equals Patnt.PatId
                                 join SmpTy in dbContext.TblSampletypes
                              on Smp.SmpTypeId equals SmpTy.SmpId
                                 join Dr in dbContext.TblDoctordetails
                              on Smp.DrId equals Dr.DrId
                                 join unt in dbContext.TblUnits
                                      on Tst.Unit equals unt.UntId
                                 where Rst.SampleId == SampleId 
                              //&& Tst.SoftDelete != true && Ref.SoftDelete != true && Smp.SoftDelete != true && Patnt.SoftDelete != true && SmpTy.SoftDelete != true && Dr.SoftDelete != true

                                 select new
                                 {
                                     RunDate = Rst.ResultDateTime,
                                     PatId = Patnt.PatId,
                                     PatientName = Patnt.PatName,
                                     PatientId = Patnt.PatientId,
                                     //SampleType = SmpTy.SmpType,
                                     PatientRemark = "",
                                     Gender = Patnt.Sex,
                                     Age = Smp.Age,
                                     LabName = Smp.LebHospitalName,
                                     Doctor = Dr.DrName,
                                     Department = Dr.DrDepartment,
                                     TestCode = Tst.TestCode,
                                     TestName = Tst.TestName,
                                     ResultValue = Rst.ResultValue,
                                     Flag = Rst.Flag,
                                     Unit = unt.UntName,
                                     Remark = Smp.SmpRemark,
                                     RefLow = Ref.RefLow,
                                     RefHigh = Ref.RefHigh,
                                     SmpId = Smp.SmpId,
                                 }).ToList();


                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reports", "Entry");
                return Json(new { ReportPDF });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Reports", "Exist", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> GetExternalTestById(int? ExternTestId, int? SampleId)
        {
            var ExternalTest = await (from Ext in dbContext.TblTestExterns
                                      join unit in dbContext.TblUnits
                                      on Ext.Unit equals unit.UntId into ut
                                      from unt in ut.DefaultIfEmpty()
                                      join rstExt in dbContext.TblResultsExterns
                                    on Ext.ExternTestId equals rstExt.ExternTestId into RstExtrn
                                      from rstExt in RstExtrn.Where(y => y.SmpId == SampleId).DefaultIfEmpty()
                                      where Ext.ExternTestId == ExternTestId && unt.SoftDelete != true 
                                      select new
                                      {
                                          Ext.ExternTestId,
                                          Ext.TestCode,
                                          Ext.TestName,
                                          Ext.Unit,
                                          unt.UntName,
                                          rstExt.Result
                                      }).FirstOrDefaultAsync();
            return Json(new { ExternalTest });
        }

        [HttpPost]
        public async Task<JsonResult> SaveExternalTestResult(IEnumerable<TblResultsExtern> resultsExterns)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Results Externs Save Data", "Entry");
            try
            {
                foreach (var item in resultsExterns)
                {
                    var data = dbContext.TblResultsExterns.Where(x => x.ScheduleId == item.ScheduleId && x.SoftDelete != true).FirstOrDefault();
                    if (data == null && item.Flag != "Delete")
                    {

                        TblResultsExtern obj = new TblResultsExtern();

                        obj.ExternTestId = item.ExternTestId;
                        obj.SmpId = item.SmpId;
                        obj.ResultDate = item.ResultDate;
                        obj.Result = item.Result;
                        obj.Unit = item.Unit;
                        obj.CreateBy = (int)HttpContext.Session.GetInt32("UserId");
                        dbContext.TblResultsExterns.Add(obj);
                        await dbContext.SaveChangesAsync();
                    }

                    else if(item.Flag != "Delete")
                    {
                        data.ExternTestId = item.ExternTestId;
                        data.SmpId = item.SmpId;
                        data.ResultDate = item.ResultDate;
                        data.Result = item.Result;
                        data.Unit = item.Unit;
                        data.ModifyBy = (int)HttpContext.Session.GetInt32("UserId");
                        data.ModifyDate = DateTime.Now;

                        dbContext.Entry(data).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                    }
                    else if(data != null)
                    {
                        data.SoftDelete = true;
                        data.ModifyBy = (int)HttpContext.Session.GetInt32("UserId");
                        data.ModifyDate = DateTime.Now;
                        dbContext.Entry(data).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                    }
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save Data", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> GetResultExternalBySampleId(int? SampleId)
        {
            var ResultExternal = await (from RstExt in dbContext.TblResultsExterns
                                          //join unit in dbContext.TblUnits
                                          //on Ext.Unit equals unit.UntId into ut
                                          //from unt in ut.DefaultIfEmpty()
                                      where RstExt.SmpId == SampleId && RstExt.SoftDelete != true
                                      select new
                                      {
                                          RstExt.ScheduleId,
                                          RstExt.ExternTestId,
                                          RstExt.SmpId,
                                          RstExt.ResultDate,
                                          RstExt.Result,
                                          RstExt.Unit,
                                          RstExt.Flag,
                                      }).ToListAsync();
            return Json(new { ResultExternal });
        }

        public async Task<JsonResult> GetGraphChatByResultId(int? ResultId)
        {
            var Response = await (from Rst in dbContext.TblResults
                                        join test in dbContext.TblTests
                                        on Rst.TestId equals test.TestId
                                        join method in dbContext.TblMethods
                                        on test.Method equals method.MthdId
                                        join smp in dbContext.TblSamples
                                        on Rst.SampleId equals smp.SmpId
                                        join Ptnt in dbContext.TblPatients
                                        on smp.PatientId equals Ptnt.PatId
                                        join unt in dbContext.TblUnits
                                        on test.Unit equals unt.UntId
                                            //join unit in dbContext.TblUnits
                                            //on Ext.Unit equals unit.UntId into ut
                                            //from unt in ut.DefaultIfEmpty()
                                        where Rst.ResultId == ResultId
                                        select new
                                        {
                                            Rst.ResultId,
                                            test.TestCode,
                                            PatName = Ptnt.PatName +"("+Ptnt.PatientId+")",
                                            smp.SmpId,
                                            Rst.ResultDateTime,
                                            method.MthdType,
                                            Rst.ResultValue,
                                            Unit = unt.UntName,
                                            test.ReadTime,
                                          StartPoint =  test.R1incubationTime,
                                          test.R1incubationTime,
                                          EndPoint = Convert.ToDouble(test.R1incubationTimeSec) / Convert.ToDouble(configuration.GetSection("MySettings").GetSection("R1IncubationTimeCycle").Value),
                                            ReadResult = dbContext.TblReadsResults.Where(y => y.ResultId == Rst.ResultId).ToList(),

                                        }).FirstOrDefaultAsync();
            return Json( Response );
        }

    }

}
