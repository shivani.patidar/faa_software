﻿using FAA_UI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Differencing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Options;
using Org.BouncyCastle.Bcpg;
using System.Configuration;
using System.Security.Cryptography;
using System.Security.Policy;

namespace FAA_UI.Controllers
{
    public class AdminController : Controller
    {
        public static int? UserId = 0;
        public static int? RoleId = 0;
        public static string UserName = "";


        DbFaaSoftwareContext dbContext = new DbFaaSoftwareContext();
   
        public IActionResult Login()
        {
            return View();
        }

        #region User Login

        [HttpPost]
        public async Task<JsonResult> UserLogin(string LoginId, string Password)
        {
            bool returnValue = false;
            GlobalClass.UserActivityLogFile(UserName, "Reference Range Get List", "Entry");
            try
            {
                TblOperator tblOperator = await dbContext.TblOperators.Where(x => x.OprLoginId == LoginId && x.OprPassword == Password && x.SoftDelete != true).FirstOrDefaultAsync();
               if(tblOperator != null)
               {
                    HttpContext.Session.SetString("UserName", tblOperator.OprName);
                    HttpContext.Session.SetInt32("UserId", tblOperator.OprId);
                    HttpContext.Session.SetInt32("RoleId", (int)tblOperator.OprRoleId);
                    if (tblOperator.OprImage != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToBase64String(tblOperator.OprImage)))
                        {
                            tblOperator.OprImage = Convert.FromBase64String(Convert.ToBase64String(tblOperator.OprImage));
                          
                            string imreBase64Data = Convert.ToBase64String(tblOperator.OprImage);
                            string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                            HttpContext.Session.SetString("UserImage", imgDataURL);
                        }
                    }
                 
                    tblOperator.LoginTime = DateTime.Now;
                    dbContext.Entry(tblOperator).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();

                    TblMastRole role = await dbContext.TblMastRoles.Where(y => y.RoleId == tblOperator.OprRoleId && y.SoftDelete != true).FirstOrDefaultAsync();
                    if (role != null)
                    {
                        HttpContext.Session.SetString("Role", role.Role);
                    }
                    returnValue = true;

                    if (!Directory.Exists(@"C:/Temp/UserActivityLog"))
                    {
                        Directory.CreateDirectory(@"C:/Temp/UserActivityLog");
                    }
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Login", "Entry");

                }
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Login", "Exit", ex.Message);
                return Json(returnValue);
            }

        }


        [HttpGet]
        public async Task<JsonResult> GetUserProfile(int? UserId)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "User Profile Get", "Entry");
            try
            {
                TblOperator tblOperator = await dbContext.TblOperators.Where(x => x.OprId == UserId && x.SoftDelete != true).FirstOrDefaultAsync();
                if (tblOperator != null)
                {
                    if (tblOperator.OprImage != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToBase64String(tblOperator.OprImage)))
                        {
                            tblOperator.OprImage = Convert.FromBase64String(Convert.ToBase64String(tblOperator.OprImage));
                        }
                    }
                }
                GlobalClass.UserActivityLogFile(UserName, "User Profile Get", "Exit");
                return Json(tblOperator);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "User Profile Get", "Exit", ex.Message);
                return Json(false);
            }

        }

        public IActionResult Logout()
        {
            //Removes all keys and values from the session-state collection.
            HttpContext.Session.Clear();
            return RedirectToAction("Login", "Admin");
        }



        
              [HttpGet]
        public async Task<JsonResult> CheckOldPassword(int? UserId, string OldPassword)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "User Profile Get", "Entry");
            try
            {
                TblOperator tblOperator = await dbContext.TblOperators.Where(x => x.OprId == UserId && x.OprPassword == OldPassword && x.SoftDelete != true).FirstOrDefaultAsync();
                GlobalClass.UserActivityLogFile(UserName, "User Profile Get", "Exit");
                return Json(tblOperator);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "User Profile Get", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> UpdateChangePassword(int? UserId, string NewPassword)
        {
            var returnValue = false;
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "User Profile Get", "Entry");
            try
            {

                TblOperator tblOperator = await dbContext.TblOperators.Where(x => x.OprId == UserId && x.SoftDelete != true).FirstOrDefaultAsync();
                if (tblOperator != null)
                {
                    tblOperator.OprPassword = NewPassword;
                    tblOperator.ModifyBy = HttpContext.Session.GetInt32("UserId");
                    tblOperator.ModifyDate = DateTime.Now;
                    dbContext.Entry(tblOperator).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();

                    returnValue = true;
                }
                GlobalClass.UserActivityLogFile(UserName, "User Profile Get", "Exit");
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "User Profile Get", "Exit", ex.Message);
                return Json(false);
            }

        }


#endregion


#region Role

[HttpGet]
        public async Task<JsonResult> GetAllRole()
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Role Get List", "Entry");
            try
            {
                var Response = await dbContext.TblMastRoles.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Role Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Role Get List", "Exit", ex.Message);
                return Json(false);
            }
        }

        [HttpPost]
        public async Task<JsonResult> SaveRole(TblMastRole mastRole)
        {
            bool returnValue = false;
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");
            GlobalClass.UserActivityLogFile(UserName, "Role Save or Update", "Entry");

            try
            {
                if (mastRole.RoleId == 0)
                {
                    TblMastRole data = dbContext.TblMastRoles.Where(x => x.Role == mastRole.Role).FirstOrDefault();
                    if (data == null)
                    {
                        mastRole.CreatedBy = UserName;
                        mastRole.CreatedDate = DateTime.Now;
                        dbContext.TblMastRoles.Add(mastRole);
                        returnValue = true;
                    }
                }
                else
                {
                    TblMastRole data = dbContext.TblMastRoles.Where(x => x.RoleId != mastRole.RoleId && x.Role == mastRole.Role).FirstOrDefault();
                    if (data == null)
                    {
                        mastRole.ModifiedBy = UserName;
                        mastRole.ModifiedDate = DateTime.Now;
                        dbContext.Entry(mastRole).State = EntityState.Modified;
                        returnValue = true;
                    }
                }
                await dbContext.SaveChangesAsync();
                GlobalClass.UserActivityLogFile(UserName, "Role Save or Update", "Exit");
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Role Save or Update", "Exit", ex.Message);

                return Json(returnValue);
            }

        }

        #endregion

        #region Menu Right
        public IActionResult MenuRight()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetAllMenuRight(int? RoleId)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Reference Range Get List", "Entry");
            try
            {

                var Response = await (from menu in dbContext.TblMenuMappings
                                      join roleaccess in dbContext.TblMenuRoleAccesses.Where(x => x.RoleId == RoleId)
                                      on menu.Id equals roleaccess.MenuMappingId into Details
                                      from r in Details.DefaultIfEmpty()

                                      select new
                                      {

                                          MenuMappingId = menu.Id,
                                          AccessId = r != null ? r.AccessId : 0,
                                          RoleId = r != null ? r.RoleId : 0,
                                          Page = menu.Page,
                                          Added = r != null ? r.Added : false,
                                          Edited = r != null ? r.Edited : false,
                                          Viewed = r != null ? r.Viewed : false,
                                          Deleted = r != null ? r.Deleted : false
                                      }).ToListAsync();

                GlobalClass.UserActivityLogFile(UserName, "Reference Range Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Reference Range Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveMenuRight(IEnumerable<TblMenuRoleAccess> model)
        {

            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Menu Right Save Data", "Entry");
            try
            {
                foreach (var item in model)
                {
                    var data = dbContext.TblMenuRoleAccesses.Where(x => x.AccessId == item.AccessId && x.RoleId == item.RoleId).FirstOrDefault();
                    if (data == null)
                    {
                        if (item.Added == true || item.Edited == true || item.Viewed == true || item.Deleted == true)
                        {
                            TblMenuRoleAccess obj = new TblMenuRoleAccess();

                            obj.AccessId = item.AccessId;
                            obj.MenuMappingId = item.MenuMappingId;
                            obj.RoleId = item.RoleId;
                            obj.Added = item.Added;
                            obj.Edited = item.Edited;
                            obj.Viewed = item.Viewed;
                            obj.Deleted = item.Deleted;
                            obj.ModifiedDate = DateTime.Now;
                            dbContext.TblMenuRoleAccesses.Add(obj);
                            await dbContext.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        data.Added = item.Added;
                        data.Edited = item.Edited;
                        data.Viewed = item.Viewed;
                        data.Deleted = item.Deleted;
                        data.ModifiedDate = DateTime.Now;
                        dbContext.Entry(data).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();

                    }

                }
                GlobalClass.UserActivityLogFile(UserName, "Menu Right Save Data", "Exit");
                return Json(true);
            }
            catch(Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "MenuRight Save", "Exit", ex.Message);
                return Json(false);
            }
        }

        #endregion


        #region Menu Bind
        
        [HttpGet]
       
        public async Task<JsonResult> MenuBind()
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Reference Range Get List", "Entry");
            try
            {
                var Response = await (from menu in dbContext.TblMenuMappings
                                    join roleaccess in dbContext.TblMenuRoleAccesses
                                    on menu.Id equals roleaccess.MenuMappingId
                                    where roleaccess.RoleId == HttpContext.Session.GetInt32("RoleId") && (roleaccess.Added == true || roleaccess.Edited == true || roleaccess.Viewed == true || roleaccess.Deleted == true)
                                    select new
                                    {
                                        MenuMappingId = menu.Id,
                                        ParentId = menu.ParentId,
                                        Page = menu.Page,
                                        Url = menu.SubPage,
                                        Icon = menu.Icon,
                                        SubMenu = (from menu1 in dbContext.TblMenuMappings
                                                  join roleaccess1 in dbContext.TblMenuRoleAccesses
                                                  on menu1.Id equals roleaccess1.MenuMappingId
                                                  where roleaccess1.RoleId == HttpContext.Session.GetInt32("RoleId") && menu1.ParentId == menu.Id  && (roleaccess1.Added == true || roleaccess1.Edited == true || roleaccess1.Viewed == true || roleaccess1.Deleted == true)
                                                  select new
                                                  {
                                                      MenuMappingId = menu1.Id,
                                                      ParentId = menu1.ParentId,
                                                      Page = menu1.Page,
                                                      Url = menu1.SubPage,
                                                      Icon = menu1.Icon,
                                                      Added = roleaccess1.Added == true ? true : false,
                                                      Edited = roleaccess1.Edited == true ? true : false,
                                                      Viewed = roleaccess1.Viewed == true ? true : false,
                                                      Deleted = roleaccess1.Deleted == true ? true : false,
                                                  }).ToList(),


                                        //SubMenu = dbContext.TblMenuMappings.Where(x => x.ParentId == menu.Id ).ToList(),
                                        Added = roleaccess.Added == true ? true : false,
                                        Edited = roleaccess.Edited == true ? true : false,
                                        Viewed = roleaccess.Viewed == true ? true : false,
                                        Deleted = roleaccess.Deleted == true ? true : false,
                                    }).Where(x=> x.ParentId == 0).ToListAsync();

                GlobalClass.UserActivityLogFile(UserName, "Reference Range Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Reference Range Get List", "Exit", ex.Message);
                return Json(false);
            }
        }
        #endregion

        public async Task<JsonResult> SearchMenu(string searchval)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Reference Range Get List", "Entry");
            try
            {
               // var Response = await dbContext.TblMenuMappings.Where(x => x.Page.Contains(searchval)).ToListAsync();
                var Menu =  (from menu in dbContext.TblMenuMappings
                                      join roleaccess in dbContext.TblMenuRoleAccesses
                                      on menu.Id equals roleaccess.MenuMappingId
                                      where menu.Page.Contains(searchval) && roleaccess.RoleId == HttpContext.Session.GetInt32("RoleId") && (roleaccess.Added == true || roleaccess.Edited == true || roleaccess.Viewed == true || roleaccess.Deleted == true)
                                      select new
                                      {
                                          MenuMappingId = menu.Id.ToString(),
                                          ParentId = menu.ParentId.ToString(),
                                          Page = menu.Page,
                                          Url = menu.SubPage == null ? "/"+ menu.Page.Replace(" ","") +"/Index" : menu.SubPage,
                                          SubMenu = "Menu",
                                          //SubMenu = dbContext.TblMenuMappings.Where(x => x.ParentId == menu.Id ).ToList(),
                                          //Added = roleaccess.Added == true ? true : false,
                                          //Edited = roleaccess.Edited == true ? true : false,
                                          //Viewed = roleaccess.Viewed == true ? true : false,
                                          //Deleted = roleaccess.Deleted == true ? true : false,
                                      }).ToList();


                var Patient = (from pant in dbContext.TblPatients
                               where pant.PatName.Contains(searchval) && pant.SoftDelete != true
                           select new
                           {
                               MenuMappingId = pant.PatId.ToString(),
                               ParentId = pant.PatientId.ToString(),
                               Page = pant.PatName + "(" + pant.PatientId + ")",
                               Url = "/Reports/Index",
                               SubMenu = "Patient",
                           }).ToList();

       
                var Response = Patient.Concat(Menu).ToList();


                GlobalClass.UserActivityLogFile(UserName, "Reference Range Get List", "Exit");
               return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Reference Range Get List", "Exit", ex.Message);
                return Json(false);
            }
        }
    }
}
