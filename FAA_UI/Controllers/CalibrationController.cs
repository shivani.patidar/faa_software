using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FAA_UI.Models;
using FAA_UI.ViewModels;
using System.Threading.Tasks;
using System.Linq;

namespace FAA_UI.Controllers
{
    public class CalibrationController:Controller
    {
        FAA_DbContext dbContext=new FAA_DbContext(); 
            
        public async Task<IActionResult> Index(int? CalTestID)
        {
            var viewmodel=new CalculatedViewModel();
            FillDropDowns();
            viewmodel.tblTestListViewModel=new TestListViewModel();
            viewmodel.tblCalcuTestList=dbContext.TblTestCalcus.DefaultIfEmpty();
            if(CalTestID!=null)
            {
                viewmodel.tblCalcuTest=await dbContext.TblTestCalcus.Where(x=>x.CalTestId==CalTestID).FirstOrDefaultAsync();
            }
            else
            {
                viewmodel.tblCalcuTest=new TblTestCalcu();
                viewmodel.tblCalcuTest.CalTestId=0;
            }
            return View(viewmodel);
        }
        [HttpPost]
        public IActionResult Index(CalculatedViewModel viewModel)
        {
            int? id=0;
            FillDropDowns();
            viewModel.tblTestListViewModel=new TestListViewModel();
            viewModel.tblCalcuTestList=dbContext.TblTestCalcus.DefaultIfEmpty();
            if(ModelState.IsValid)
            {             
                if(dbContext.TblTestCalcus.Where(x=>x.CalTestCode==viewModel.tblCalcuTest.CalTestCode).Count()<=0)
                {
                    viewModel.tblCalcuTest.CalTestId=0;
                    dbContext.TblTestCalcus.Add(viewModel.tblCalcuTest);
                    dbContext.SaveChanges();
                    id=dbContext.TblTestCalcus.Max(x=>x.CalTestId);
                }
                else
                {
                    dbContext.Entry(viewModel.tblCalcuTest).State = EntityState.Modified;
                    id=viewModel.tblCalcuTest.CalTestId;
                }
                dbContext.SaveChanges();
                return RedirectToAction("Index",new { CalTestID=id});
            }
            return View(viewModel);
        }

        public void FillDropDowns()
        {
            DataDictionaryController objDataDictionary=new DataDictionaryController();
            ViewBag.Decimals=objDataDictionary.GetDirList(ENM_Dict_Type.DT_Decimals);
            ViewBag.Unit=objDataDictionary.GetDirList(ENM_Dict_Type.DT_Unit);
            ViewBag.ResultType=objDataDictionary.GetDirList(ENM_Dict_Type.DT_ResultType);
        }
    }
}