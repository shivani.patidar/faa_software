﻿using Microsoft.AspNetCore.Mvc;

namespace FAA_UI.Controllers
{
    public class UnitController : Controller
    {
        //public static string UserName = "Shivani";
        DbFaaSoftwareContext dbContext = new DbFaaSoftwareContext();
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetAllUnit()
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Unit Get List", "Entry");
            try
            {
                var Response = await dbContext.TblUnits.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Unit Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Unit Get List", "Exit", ex.Message);
                return Json(false);
            }

        }


        [HttpPost]
        public async Task<JsonResult> SaveUnit(TblUnit unit)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            try
            {
                if (unit.UntId == 0)
                {
               
                    GlobalClass.UserActivityLogFile(UserName, "Unit Save Data", "Entry");
                    dbContext.TblUnits.Add(unit);
                    await dbContext.SaveChangesAsync();
                    GlobalClass.UserActivityLogFile(UserName, "Unit Save Data", "Exit");
                }
                else
                {
                    GlobalClass.UserActivityLogFile(UserName, "Unit Update Data", "Entry");
                    dbContext.Entry(unit).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                    GlobalClass.UserActivityLogFile(UserName, "Unit Update Data", "Exit");
                }
            
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Unit Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }


        public async Task<JsonResult> GetByIdUnit(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Unit Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblUnits.Where(x => x.UntId == Id).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Unit Get By Id", "Exit");
                return Json(obj);
            }
            catch(Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Unit Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteUnit(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Unit Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblUnit obj = dbContext.TblUnits.Where(x => x.UntId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                }
          
                GlobalClass.UserActivityLogFile(UserName, "Unit Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Unit Delete", "Exit", ex.Message);
                return Json(false);
            }

        }
    }
}
