﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using MySqlX.XDevAPI.Common;
using Org.BouncyCastle.Crypto.Tls;
using System.Collections;
using System.Linq.Expressions;

namespace FAA_UI.Controllers
{
    public class Month
    {
        public int monValue { get; set; }
        public string monName { get; set; }
    }
    public class QualityControlController : Controller
    {
        DbFaaSoftwareContext dbContext = new DbFaaSoftwareContext();
        private IConfiguration configuration;

        public QualityControlController(IConfiguration con)
        {
            configuration = con;
        }
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public async Task<JsonResult> MonthYear()
        {

            List<Month> months = new List<Month>();

            months.Add(new Month { monValue = 01, monName = "January" });
            months.Add(new Month { monValue = 02, monName = "February" });
            months.Add(new Month { monValue = 03, monName = "March" });
            months.Add(new Month { monValue = 04, monName = "April" });
            months.Add(new Month { monValue = 05, monName = "May" });
            months.Add(new Month { monValue = 06, monName = "June" });
            months.Add(new Month { monValue = 07, monName = "July" });
            months.Add(new Month { monValue = 08, monName = "August" });
            months.Add(new Month { monValue = 09, monName = "September" });
            months.Add(new Month { monValue = 10, monName = "October" });
            months.Add(new Month { monValue = 11, monName = "November" });
            months.Add(new Month { monValue = 12, monName = "December" });

    
            ArrayList Year = new ArrayList();
            //Dictionary<int, string> Months = Enumerable.Range(1, 12).Select(i => new KeyValuePair<int, string>(i, System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(i))).ToDictionary(x => x.Key, x => x.Value);
            int CurntYear = DateTime.Now.Year;
            for (int i = CurntYear; i< (CurntYear+5); i++)
            {
                Year.Add(i);
            }
            return Json(new { months, Year });
        }

        #region QC lot

        [HttpGet]
        public async Task<JsonResult> DuplicateCheckQCLotNo(int? LotId, string LotNo)
        {
            bool returnValue = false;
            if (LotId == 0)
            {
                TblQclot data = dbContext.TblQclots.Where(x => x.LotNo == LotNo).FirstOrDefault();
                if (data == null)
                {
                    returnValue = true;
                }
            }
            else
            {
                TblQclot data = dbContext.TblQclots.Where(x => x.LotId != LotId && x.LotNo == LotNo).FirstOrDefault();
                if (data == null)
                {
                    returnValue = true;
                }
            }
            return Json(returnValue);
        }


        [HttpGet]
        public async Task<JsonResult> GetQCLot()
        {
            try
            {
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Entry");
                var Response = await dbContext.TblQclots.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist", ex.Message);
                return Json(false);
            }
        }

        [HttpGet]
        public async Task<JsonResult> GetAllQCLot()
        {
            try
            {
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Entry");

                var Response = await (from lot in dbContext.TblQclots
                                      join qcDet in dbContext.TblQclotDets
                                      on lot.LotId equals qcDet.LotId
                                      join tst in dbContext.TblTests
                                      on qcDet.TestId equals tst.TestId
                                      //on qcDet.Test_Id equals tst.TestId

                                      join unt in dbContext.TblUnits
                                    on tst.Unit equals unt.UntId
                                    where lot.SoftDelete != true && tst.SoftDelete != true && unt.SoftDelete != true
                                      select new
                                      {
                                          LotDetId = qcDet.QcdetId,
                                          TestId = qcDet.TestId,
                                          LotId = lot.LotId,
                                          LotNo = lot.LotNo,
                                          ExpMonth = lot.ExpMonth,
                                          ExpYear = lot.ExpYear,
                                          Mean = lot.Mean,
                                          Sd = lot.Sd,
                                          TestCode = tst.TestCode,
                                          Decimals = tst.Decimals,
                                          Unit = unt.UntName,
                                          TestName = tst.TestName,
                                          min = lot.Min,
                                          max = lot.Max,

                                      }).ToListAsync();

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist");

                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveQCLot(TblQclot lot, TblQclotDet qclotDet)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Lot Save or Update", "Entry");
            try
            {
                if (lot.LotId == 0)
                {
                    lot.CreateBy = HttpContext.Session.GetInt32("UserId");
                    dbContext.TblQclots.Add(lot);
                }
                else
                {
                    var dataqcLot = dbContext.TblQclots.Where(x => x.LotId == lot.LotId).FirstOrDefault();
                    if(dataqcLot != null)
                    {
                        dataqcLot.LotNo = lot.LotNo;
                        dataqcLot.LotId = lot.LotId;
                        dataqcLot.Conc = lot.Conc;
                        dataqcLot.ConsumableType = lot.ConsumableType;
                        dataqcLot.ExpYear = lot.ExpYear;
                        dataqcLot.ExpMonth = lot.ExpMonth;
                        dataqcLot.Mean = lot.Mean;
                        dataqcLot.Max = lot.Max;
                        dataqcLot.Min = lot.Min;
                        dataqcLot.NoOfStd = lot.NoOfStd;

                        dataqcLot.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        dataqcLot.ModifyDate = DateTime.Now;
                    }
                  
                }
                await dbContext.SaveChangesAsync();

                var data = await dbContext.TblQclotDets.Where(x => x.LotId == lot.LotId).FirstOrDefaultAsync();
                if(data == null)
                {
                    qclotDet.LotId = lot.LotId;
                    dbContext.TblQclotDets.Add(qclotDet);
               
                }
                else
                {
                    data.Conc = qclotDet.Conc;
                    data.TestId = qclotDet.TestId; 
                    //qclotDet.QcdetId = data.QcdetId;
                    //qclotDet.LotId = data.LotId;
                    dbContext.Entry(data).State = EntityState.Modified;
                }

                await dbContext.SaveChangesAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Lot Save or Update", "Exit");

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Lot Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }
   
        public async Task<JsonResult> GetByIdQClot(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "lot Get By Id", "Entry");
           // var obj = await dbContext.TblQclots.Where(x => x.LotId == Id).FirstOrDefaultAsync();

                var obj = await (from lot in dbContext.TblQclots
                                      join qcDet in dbContext.TblQclotDets
                                      on lot.LotId equals qcDet.LotId
                                      where lot.SoftDelete != true && lot.LotId == Id
                                      select new
                                      {
                                          LotDetId = qcDet.QcdetId,
                                          TestId = qcDet.TestId,
                                          Conc = qcDet.Conc,
                                          LotId = lot.LotId,
                                          LotNo = lot.LotNo,
                                          ExpMonth = lot.ExpMonth,
                                          ExpYear = lot.ExpYear,
                                          Mean = lot.Mean,
                                          Sd = lot.Sd,
                                          min = lot.Min,
                                          max = lot.Max,

                                      }).FirstOrDefaultAsync();
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "lot Get By Id", "Exit");
            return Json(obj);
        }

        //QCLot

        public async Task<JsonResult> DeleteQClot(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "lot Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblQclot obj = dbContext.TblQclots.Where(x => x.LotId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.ModifyBy = (sbyte)HttpContext.Session.GetInt32("UserId");
                        obj.ModifyDate = DateTime.Now;
                        obj.SoftDelete = true;
                        dbContext.Entry(obj).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                    }
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "lot Delete", "Exit");

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "lot Delete", "Exit", ex.Message);
                return Json(false);
            }
        }

        #endregion


        #region Daily QC

        public async Task<JsonResult> GetTestCode()
        {          
            try
            {
                var TestCode = (from e in dbContext.TblTests
                                join s in dbContext.TblTestDetails
                                on e.TestId equals s.TestId
                                select new
                                {
                                    e.TestId,
                                    e.TestCode,
                                    s.DispIndex,
                                    e.SoftDelete
                                }).Where(x => x.SoftDelete != true).OrderBy(x => x.DispIndex).ToList();

                //GlobalClass.UserActivityLogFile(UserName, "Profile Get Test Code List", "Exit");
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Entry");
                return Json(new { TestCode });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist", ex.Message);
                return Json(false);
            }
        }
        

        /* Get Lot No as per Test Code - Daily QC */
        public async Task<JsonResult> GetLotNoByTestId(int? TestId)
        {
            try
            {
                var DailyQCLotNoType = await (from lot in dbContext.TblQclots
                                              join qcDet in dbContext.TblQclotDets
                                              on lot.LotId equals qcDet.LotId
                                              join tst in dbContext.TblTests
                                              on qcDet.TestId equals tst.TestId
                                              where tst.TestId == TestId && lot.SoftDelete != true && tst.SoftDelete != true
                                              select new
                                              {
                                                 // LotId = lot.LotId,
                                                  LotNo = lot.LotNo
                                              }).Distinct().ToListAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist");
                return Json(DailyQCLotNoType);
            }
            catch (Exception ex) 
            {
                return Json(false);
            }
        }

        public async Task<JsonResult> GetAllDailyQC(string lotNo, int concentrationVal,string rundateVal)
         {
            try
            {
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Entry");
                var Response = await (from rst in dbContext.TblResults
                                      join tst in dbContext.TblTests
                                      on rst.TestId equals tst.TestId
                                      join lot in dbContext.TblQclots
                                      on rst.Qclot equals lot.LotId
                                      where (rst.SchType == 2) && (lot.LotNo == lotNo) && (lot.Conc == concentrationVal) 
                                      && rst.ResultDateTime.Date == Convert.ToDateTime(rundateVal)
                                      select new
                                      {
                                          LotId = lot.LotId,
                                          LotNo = lot.LotNo,
                                          ExpMonth = lot.ExpMonth,
                                          ExpYear = lot.ExpYear,
                                          Mean = lot.Mean,
                                          Sd = lot.Sd,
                                          TestCode = tst.TestCode,
                                          ResultValue = rst.ResultValue,
                                          Flag = rst.Flag,
                                          Rundate = rst.ResultDateTime, //.ToString("dd-MMM-yyyy"),
                                      }).ToListAsync();

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist");

                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist", ex.Message);
                return Json(false);
            }

        }
      
        /* following comment to be removed
        public async Task<JsonResult> DeleteDailyQClot(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "lot Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblLot obj = dbContext.TblQclots.Where(x => x.LotId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        dbContext.TblQclots.Remove(obj);
                        await dbContext.SaveChangesAsync();
                    }
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "lot Delete", "Exit");

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "lot Delete", "Exit", ex.Message);
                return Json(false);
            }
        }
        */
        #endregion

      

        /* Monthly QC Get Data  */
        public async Task<JsonResult> GetAllMonthlyQC(string lotNo, int concentrationVal, string rundateVal)
        {
            try
            {
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Select");

                DateTime rundate = Convert.ToDateTime(rundateVal);
                
                var Response = (from rst in dbContext.TblResults
                                join tst in dbContext.TblTests on rst.TestId equals tst.TestId
                                join lot in dbContext.TblQclots on rst.Qclot equals lot.LotId
                                where rst.SchType == 2 && 
                                lot.LotNo == lotNo && lot.Conc == concentrationVal && 
                                rst.ResultDateTime.Month == rundate.Month && rst.ResultDateTime.Year == rundate.Year
                                select new { rst, tst,lot } into t1
                group t1 by t1.rst.ResultDateTime.Date into g
                select new
                {
                    LotId = g.FirstOrDefault().lot.LotId,
                    LotNo = g.FirstOrDefault().lot.LotNo,
                    ExpMonth = g.FirstOrDefault().lot.ExpMonth.ToString(),
                    ExpYear = g.FirstOrDefault().lot.ExpYear.ToString(),
                    Mean = g.FirstOrDefault().lot.Mean.ToString(),
                    Sd = g.FirstOrDefault().lot.Sd.ToString(),
                    TestCode = g.FirstOrDefault().tst.TestCode,
                    ResultValue = g.Average(m => m.rst.ResultValue),
                    Flag = g.FirstOrDefault().rst.Flag,
                    Rundate = g.FirstOrDefault().rst.ResultDateTime,
                   
                }).ToList();

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist", ex.Message);
                return Json(false);
            }
        }

        /*QC Value Get Data */
        public async Task<JsonResult> GetAllValueQC(int concentrationVal,DateTime? fromdateVal, DateTime? todateVal)
        {
            try
            {
                //where lot.Conc == concentrationVal && tst.SoftDelete != true

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Entry");

                var Response = await (from rst in dbContext.TblResults
                                      join tst in dbContext.TblTests
                                      on rst.TestId equals tst.TestId
                                      join lot in dbContext.TblQclots
                                      on rst.Qclot equals lot.LotId
                                      where rst.SchType == 2 && lot.Conc== concentrationVal && (rst.ResultDateTime >= fromdateVal && rst.ResultDateTime <= todateVal)  //rst.ResultDateTime BETWEEN fromdateVal AND todateVal)
                                      select new
                                      {
                                          LotId = lot.LotId,
                                          LotNo = lot.LotNo,
                                          ExpMonth = lot.ExpMonth,
                                          ExpYear = lot.ExpYear,
                                          Mean = lot.Mean,
                                          Sd = lot.Sd,
                                          TestCode = tst.TestCode,
                                          ResultValue = rst.ResultValue,
                                          Flag = rst.Flag,
                                          Rundate = rst.ResultDateTime,

                                      }).ToListAsync();
                

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist");

                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> GetAllQCValuePrint_pdf(int concentrationVal, DateTime? fromdateVal, DateTime? todateVal, string[] lotidList)
        {           
            try
            {
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Entry");
                var Response = await (from rst in dbContext.TblResults
                                      join tst in dbContext.TblTests
                                      on rst.TestId equals tst.TestId
                                      join lot in dbContext.TblQclots
                                      on rst.Qclot equals lot.LotId
                                      where rst.SchType == 2 && lot.Conc== concentrationVal && (rst.ResultDateTime >= fromdateVal && rst.ResultDateTime <= todateVal) && lotidList.Contains(lot.LotId.ToString()) //rst.ResultDateTime BETWEEN fromdateVal AND todateVal)
                                      select new
                                      {
                                          LotId = lot.LotId,
                                          LotNo = lot.LotNo,
                                          ExpMonth = lot.ExpMonth,
                                          ExpYear = lot.ExpYear,
                                          Mean = lot.Mean,
                                          Sd = lot.Sd,
                                          resu = rst.ResultValue,
                                          flag = rst.Flag,
                                          TestCode = tst.TestCode
                                      }).ToListAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> GetAllQCValuePrint_print_pdf(int concentrationVal, string fromdateVal, string todateVal)
        {

            try
            {
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Entry");

                var Response = await (from lot in dbContext.TblQclots
                                      join qcDet in dbContext.TblQclotDets
                                      on lot.LotId equals qcDet.LotId
                                      join tst in dbContext.TblTests
                                      on qcDet.TestId equals tst.TestId
                                      //join unt in dbContext.TblUnits
                                      //on tst.Unit equals unt.UntId
                                      where lot.Conc == concentrationVal && tst.SoftDelete != true
                                      select new
                                      {
                                          LotId = lot.LotId,
                                          LotNo = lot.LotNo,
                                          ExpMonth = lot.ExpMonth,
                                          ExpYear = lot.ExpYear,
                                          Mean = lot.Mean,
                                          Sd = lot.Sd,

                                          TestCode = tst.TestCode

                                      }).ToListAsync();

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist");

                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality Control", "Exist", ex.Message);
                return Json(false);
            }

        }

    }
}
