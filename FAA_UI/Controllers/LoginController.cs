using System;
using Microsoft.AspNetCore.Mvc;
using FAA_UI.Models;
using FAA_UI.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.IO;
using MySqlX.XDevAPI;
using Microsoft.AspNetCore.Http;
#nullable disable
namespace FAA_UI.Controllers
{

    public class LoginController: Controller
    {
        FAA_DbContext dbContext=new FAA_DbContext();    //Intialize DB context for entity framework.
        public async Task<IActionResult> Index()
        {
         
            var viewModel=new UserViewModel();
            viewModel.tblUsers= await dbContext.TblUsers.ToListAsync();
           
            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Validate(string ULoginId,string UPwd)          //@MT-Validate user login based on LoginID and Password
        {
            var _user = await dbContext.TblUsers.Where(s=>s.ULoginId==ULoginId).ToListAsync();
            GlobalClass.UserActivityLogFile(ULoginId, "Login_Controller", "Validate");

            if(_user.Any()){    
                if(_user.Where(s => s.UPwd == UPwd).Any()){
                    
                    return Json(new { status = true, message = "Login Successfull!"}); //@MT-Send login status and message to view as JSON data.
                }
                else
                {
                    return Json(new { status = false, message = "Invalid Password !!!"}); //@MT-Password mismatch
                }
            }
            else
            {
                    if(ULoginId==null && UPwd=="Admin123")
                    {
                        return Json(new { status = true, message = "Login Successfull!"});
                    }
        
                return Json(new { status = false, message = "User not available !!!"});     //@MT-Login ID does not exist
            }
       
        } 
    }
}
