﻿using FAA_UI.Models;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Utilities;
using System.Configuration;
using System.Numerics;
using System.Security.Cryptography;
using static System.Net.Mime.MediaTypeNames;

namespace FAA_UI.Controllers
{
    public class TestParameterController : Controller
    {
        DbFaaSoftwareContext dbContext = new DbFaaSoftwareContext();
        public static int? UserId = 0;
        public static string UserName = "";
        private IConfiguration configuration;

        public TestParameterController(IConfiguration con)
        {
            configuration = con;
        }
        public IActionResult Index()
        {
            ViewBag.R1IncubationTimeCycle = configuration.GetSection("MySettings").GetSection("R1IncubationTimeCycle").Value;
            ViewBag.R2IncubationTimeCycle = configuration.GetSection("MySettings").GetSection("R2IncubationTimeCycle").Value;

            ViewBag.TestCode = dbContext.TblTests.Where(x => x.SoftDelete != true).ToListAsync();
            return View();
        }


        #region Basic Programming

        #region Test Parameter

        [HttpGet]
        public async Task<JsonResult> GetAllMaster()
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Test Parameter Get All Master List", "Entry");
            try
            {
                int UniqueTestId = dbContext.TblTests.Count() + 1;
                //var testParameter = dbContext.TblTests.Where(x => x.SoftDelete != true).ToList();
                var testParameter = (from e in dbContext.TblTests
                            join s in dbContext.TblTestDetails
                            on e.TestId equals s.TestId
                            select new
                            {
                                e.TestId,
                                e.TestCode,
                                s.DispIndex,
                                e.SoftDelete
                            }).Where(x => x.SoftDelete != true).OrderBy(x => x.DispIndex).ToList();

                var method = await dbContext.TblMethods.ToListAsync();
                var filter = await dbContext.TblTestFilters.ToListAsync();
                var unit = await dbContext.TblUnits.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Test Parameter Get All Master List", "Exit");
                return Json(new { UniqueTestId, testParameter, method, filter, unit });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Parameter Get All Master List", "Exit", ex.Message);
                return Json(false);
            }
        }

        [HttpGet]
        public async Task<JsonResult> DuplicateCheckTestCode(int? TestId, string TestCode)
        {
            bool returnValue = false;
            if (TestId == 0)
            {
                TblTest data = dbContext.TblTests.Where(x => x.TestCode == TestCode).FirstOrDefault();
                if (data == null)
                {
                    returnValue = true;
                }
            }
            else
            {
                TblTest data = dbContext.TblTests.Where(x => x.TestId != TestId && x.TestCode == TestCode).FirstOrDefault();
                if (data == null)
                {
                    returnValue = true;
                }
            }
            return Json(returnValue);
        }

        [HttpGet]
        public async Task<JsonResult> GetAllTestParameter()
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Test Parameter Get List", "Entry");
            try
            {
                var Response = await dbContext.TblTests.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Test Parameter Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Parameter Get List", "Exit", ex.Message);
                return Json(false);
            }

        }


        [HttpPost]
        public async Task<JsonResult> SaveTestParameter(TblTest test)
        {
            bool returnValue = false;
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");
            try
            {
                // TblTest data = dbContext.TblTests.Where(x => x.TestId == test.TestId).FirstOrDefault();

                if (test.TestId == 0)
                {
                    GlobalClass.UserActivityLogFile(UserName, "Test Parameter Save Data", "Entry");
                    TblTest data = dbContext.TblTests.Where(x => x.TestCode == test.TestCode).FirstOrDefault();
                    if (data == null)
                    {
                        test.CreateBy = UserId;
                        dbContext.TblTests.Add(test);
                        await dbContext.SaveChangesAsync();
                        if (test.TestId > 0)
                        {
                            TblTestDetail testDetail = new TblTestDetail();
                            testDetail.TestId = test.TestId;
                            dbContext.TblTestDetails.Add(testDetail);
                            await dbContext.SaveChangesAsync();
                        }
                        returnValue = true;
                        GlobalClass.UserActivityLogFile(UserName, "Test Parameter Save Data", "Exit");
                    }
                }
                else
                {
                    GlobalClass.UserActivityLogFile(UserName, "Test Parameter Update Data", "Entry");
                    //test.CreateBy = data.CreateBy;
                    //test.CreateDate = data.CreateDate;
                    TblTest data = dbContext.TblTests.Where(x => x.TestId != test.TestId && x.TestCode == test.TestCode).FirstOrDefault();
                    if (data == null)
                    {
                        test.ModifyBy = UserId;
                        test.ModifyDate = DateTime.Now;
                        dbContext.Entry(test).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                        returnValue = true;
                    }
                    GlobalClass.UserActivityLogFile(UserName, "Test Parameter Update Data", "Exit");
                }

                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Parameter Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }


        public async Task<JsonResult> SaveCloneTestParameter(int? CloneTestCodeId, string CloneTestCode, string CloneTestCodeLabel)
        {
            int returnValue = 0;
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Clone Test Parameter Save Data", "Entry");
            try
            {
                var obj = await dbContext.TblTests.Where(x => x.TestCode == CloneTestCodeLabel && x.SoftDelete != true).FirstOrDefaultAsync();
                if (obj == null)
                {
                    var test = await dbContext.TblTests.Where(x => x.TestCode == CloneTestCode).FirstOrDefaultAsync();
                    test.TestCode = CloneTestCodeLabel;
                    test.TestId = 0;
                    test.CreateBy = UserId;
                    test.UniqueTestId = (short)(dbContext.TblTests.Count() + 1);

                    dbContext.TblTests.Add(test);
                    await dbContext.SaveChangesAsync();
                    if (test.TestId > 0)
                    {
                        short runIndex = (short)(dbContext.TblTestDetails.Count() + 1);
                        TblTestDetail testDetail = new TblTestDetail();
                        testDetail.TestId = test.TestId;
                        testDetail.PrintIndex = runIndex;
                        testDetail.RunIndex = runIndex;
                        testDetail.DispIndex = runIndex;
                        dbContext.TblTestDetails.Add(testDetail);
                        await dbContext.SaveChangesAsync();
                    }
                    returnValue = test.TestId;
                    GlobalClass.UserActivityLogFile(UserName, "Clone Test Parameter Save Data", "Exit");

                }
                GlobalClass.UserActivityLogFile(UserName, "Clone Test Parameter Save Data", "Exit");
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Parameter Save Data", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> GetByTestCodeTestParameter(string TestCode)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Test Parameter Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblTests.Where(x => x.TestCode == TestCode).FirstOrDefaultAsync();
                GlobalClass.UserActivityLogFile(UserName, "Test Parameter Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Parameter Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }


        public async Task<JsonResult> GetByIdTestParameter(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Test Parameter Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblTests.Where(x => x.TestId == Id).FirstOrDefaultAsync();
                GlobalClass.UserActivityLogFile(UserName, "Test Parameter Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Parameter Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteTestParameter(int? TestId)
        {
            bool returnValue = false;
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");

            GlobalClass.UserActivityLogFile(UserName, "Test Parameter Delete", "Entry");
            try
            {
                if (TestId > 0)
                {
                    TblTest obj = dbContext.TblTests.Where(x => x.TestId == TestId).FirstOrDefault();
                    var refRange = dbContext.TblRefRanges.Where(x => x.TestId == TestId && x.SoftDelete != true).ToList();
                    if (obj != null && refRange.Count() == 0)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;
                        dbContext.Entry(obj).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();

                        var testDetail = dbContext.TblTestDetails.Where(x => x.TestId == TestId).FirstOrDefault();
                        if (testDetail != null)
                        {
                            dbContext.TblTestDetails.Remove(testDetail);
                            await dbContext.SaveChangesAsync();
                        }
                        returnValue = true;
                    }
                }

                GlobalClass.UserActivityLogFile(UserName, "Test Parameter Delete", "Exit");
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Parameter Delete", "Exit", ex.Message);
                return Json(false);
            }

        }
        #endregion

        #region Reference Range
        [HttpGet]
        public async Task<JsonResult> GetAllReferenceRange(Int16 TestCode)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Reference Range Get List", "Entry");
            try
            {
                var Response = await (from e in dbContext.TblRefRanges
                                      join s in dbContext.TblSampletypes
                                      on e.SmpTyId equals s.SmpId

                                      join t in dbContext.TblTests
                                      on e.TestId equals t.TestId
                                      where t.TestId == TestCode && e.SoftDelete != true && s.SoftDelete != true
                                      select new
                                      {
                                          e.RefId,
                                          s.SmpType,
                                          e.Gender,
                                          e.AgeUnit,
                                          e.AgeMin,
                                          e.AgeMax,
                                          e.RefLow,
                                          e.RefHigh
                                      }).Distinct().ToListAsync(); //.ToListAsync();

                //var Response = await dbContext.TblRefRanges.Where(x => x.SoftDelete != true).OrderByDescending(x =>x.RefId).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Reference Range Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Reference Range Get List", "Exit", ex.Message);
                return Json(false);
            }
        }

        [HttpPost]
        public async Task<JsonResult> SaveReferenceRange(TblRefRange refRange)
        {
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");
            try
            {
                // TblRefRange data = dbContext.TblRefRanges.Where(x => x.RefId == refRange.RefId).FirstOrDefault();

                if (refRange.RefId == 0)
                {

                    GlobalClass.UserActivityLogFile(UserName, "Reference Range Save Data", "Entry");
                    refRange.CreateBy = UserId;
                    dbContext.TblRefRanges.Add(refRange);
                    await dbContext.SaveChangesAsync();
                    GlobalClass.UserActivityLogFile(UserName, "Reference Range Save Data", "Exit");
                }
                else
                {
                    GlobalClass.UserActivityLogFile(UserName, "Reference Range Update Data", "Entry");

                    var data = dbContext.TblRefRanges.Where(x => x.RefId == refRange.RefId);
                    foreach (var obj in data)
                    {
                        obj.TestId = refRange.TestId;
                        obj.SmpTyId = refRange.SmpTyId;
                        obj.Gender = refRange.Gender;
                        obj.AgeMin = refRange.AgeMin;
                        obj.AgeMax = refRange.AgeMax;
                        obj.AgeUnit = refRange.AgeUnit;
                        obj.RefLow = refRange.RefLow;
                        obj.RefHigh = refRange.RefHigh;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;
                    }
                    await dbContext.SaveChangesAsync();
                    GlobalClass.UserActivityLogFile(UserName, "Reference Range Update Data", "Exit");
                }

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Reference Range Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }
        [HttpGet]
        public async Task<JsonResult> GetByIdReferenceRange(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Reference Range Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblRefRanges.Where(x => x.RefId == Id).FirstOrDefaultAsync();
                GlobalClass.UserActivityLogFile(UserName, "Reference Range Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Reference Range Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteReferenceRange(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");

            GlobalClass.UserActivityLogFile(UserName, "Reference Range Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblRefRange obj = dbContext.TblRefRanges.Where(x => x.RefId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;

                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                }

                GlobalClass.UserActivityLogFile(UserName, "Reference Range Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Reference Range Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #region Calibration



        [HttpGet]
        public async Task<JsonResult> GetAllCalibrationLotNo(int? TestId)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Calibration Lot No List", "Entry");
            try
            {
                if (TestId != null)
                {
                    var calibrationLot = await dbContext.TblCalibrationlots.Where(x => x.TestId == TestId && x.SoftDelete != true).ToListAsync();
                    GlobalClass.UserActivityLogFile(UserName, "Calibration Lot No List", "Exit");
                    return Json(calibrationLot);
                }
                else
                {
                    return Json(false);
                }
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Calibration Lot No List", "Exit", ex.Message);
                return Json(false);
            }
        }

        [HttpPost]
        public async Task<JsonResult> SaveCalibrationLotNo(TblCalibrationlot calibrationlot)
        {
            bool returnValue = false;
            try
            {
                // TblTest data = dbContext.TblTests.Where(x => x.TestId == test.TestId).FirstOrDefault();

                if (calibrationlot.CalibLotId == 0)
                {
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test calibration lot Save Data", "Entry");
                    calibrationlot.CreateBy = HttpContext.Session.GetInt32("UserId");
                    dbContext.TblCalibrationlots.Add(calibrationlot);
                    await dbContext.SaveChangesAsync();
                    returnValue = true;
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test calibration lot Save Data", "Exit");

                }
                else
                {
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test calibration lot Update Data", "Entry");
                    calibrationlot.ModifyBy = HttpContext.Session.GetInt32("UserId");
                    calibrationlot.ModifyDate = DateTime.Now;
                    dbContext.Entry(calibrationlot).State = EntityState.Modified;


                    //var data = dbContext.TblCalibrationlots.Where(x => x.RefId == refRange.RefId);
                    //foreach (var obj in data)
                    //{
                    //    obj.TestId = refRange.TestId;
                    //    obj.SmpTyId = refRange.SmpTyId;
                    //    obj.Gender = refRange.Gender;
                    //    obj.AgeMin = refRange.AgeMin;
                    //    obj.AgeMax = refRange.AgeMax;
                    //    obj.AgeUnit = refRange.AgeUnit;
                    //    obj.RefLow = refRange.RefLow;
                    //    obj.RefHigh = refRange.RefHigh;
                    //    obj.ModifyBy = UserId;
                    //    obj.ModifyDate = DateTime.Now;
                    //}
                    await dbContext.SaveChangesAsync();

                    await dbContext.SaveChangesAsync();
                    returnValue = true;

                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test calibration lot Update Data", "Exit");
                }

                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Test calibration lot Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> GetByIdCalibrationLotNo(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test calibration lot Get By Id", "Entry");
            var obj = await dbContext.TblCalibrationlots.Where(x => x.CalibLotId == Id && x.SoftDelete != true).FirstOrDefaultAsync();
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test calibration lot Get By Id", "Exit");
            return Json(obj);
        }

        public async Task<JsonResult> DeleteCalibrationLotNo(int? Id)
        {
            bool returnValue = false;

            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Calibration Lot No Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblCalibrationlot obj = dbContext.TblCalibrationlots.Where(x => x.CalibLotId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        obj.ModifyDate = DateTime.Now;
                        dbContext.Entry(obj).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                        returnValue = true;
                    }
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Calibration Lot No Delete", "Exit");
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Calibration Lot No Delete", "Exit", ex.Message);
                return Json(false);
            }

        }


        [HttpGet]
        public async Task<JsonResult> GetAllCaliMaster(int? TestId)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Calibration Get All Master List", "Entry");
            try
            {

                var curveType = await dbContext.TblCurvetypes.ToListAsync();
                //var sampleTypeContainer = await dbContext.TblDirectorySamplecontainers.Where(x => x.SoftDelete != true).ToListAsync();

                if (TestId > 0)
                {
                    var CaliHisCurveType = await (from c in dbContext.TblCalibrations
                                                  join cty in dbContext.TblCurvetypes
                                                  on c.CurveType equals cty.CrvTyId

                                                  where c.TestId == TestId && c.SoftDelete != true
                                                  select new
                                                  {
                                                      cty.CrvTyId,
                                                      cty.CrvType
                                                  }).Distinct().ToListAsync();
                    GlobalClass.UserActivityLogFile(UserName, "Calibration Get All Master List", "Exit");
                    return Json(new { curveType, CaliHisCurveType });

                }
                else
                {
                    GlobalClass.UserActivityLogFile(UserName, "Calibration Get All Master List", "Exit");
                    return Json(new { curveType });
                }
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "TCalibration Get All Master List", "Exit", ex.Message);
                return Json(false);
            }

        }


        [HttpGet]
        public async Task<JsonResult> GetAllCalibration(int? TestId, int? CurveTypeId, string FormDate, string ToDate)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Get Calibration History List", "Entry");
            try
            {
                if (FormDate == null && ToDate == null)
                {
                    var calibration = await (from c in dbContext.TblCalibrations
                                             join CTy in dbContext.TblCurvetypes
                                             on c.CurveType equals CTy.CrvTyId
                                             let cd = dbContext.TblCalibrationdets.Where(p2 => c.CalId == p2.CalId).OrderBy(x => x.CalDetId).FirstOrDefault()
                                             let cl = dbContext.TblCalibrationlots.Where(q2 => cd.Lot == q2.CalibLotId).OrderBy(x => x.CalibLotId).FirstOrDefault()
                                             where c.TestId == TestId && (c.CurveType == CurveTypeId) && c.SoftDelete != true
                                             select new
                                             {
                                                 CalId = c.CalId,
                                                 CalDetId = cd.CalDetId,
                                                 CurveType = CTy.CrvType,
                                                 CurveTypeId = c.CurveType,
                                                 Pos = cd.Pos,
                                                 Conc = cd.Conc,
                                                 CalDate = c.CalDate.Value.ToString("dd-MMM-yyyy"),
                                                 CalTime = c.CalTime,
                                                 CalValidity = c.CalValidity,
                                                 Factor = c.Factor,
                                                 Factor1 = c.Factor1,
                                                 Factor2 = c.Factor2,
                                                 Factor3 = c.Factor3,
                                                 Factor4 = c.Factor4,
                                                 Factor5 = c.Factor5,
                                                 LotNo = cl.LotNo,
                                                 ExpMonth = cl.ExpMonth,
                                                 ExpYear = cl.ExpYear
                                             }).ToListAsync();

                    GlobalClass.UserActivityLogFile(UserName, "Get Calibration History List", "Exit");
                    return Json(calibration);
                }
                else
                {
                    var calibration = await (from c in dbContext.TblCalibrations
                                             join CTy in dbContext.TblCurvetypes
                                             on c.CurveType equals CTy.CrvTyId
                                             let cd = dbContext.TblCalibrationdets.Where(p2 => c.CalId == p2.CalId).OrderBy(x => x.CalDetId).FirstOrDefault()
                                             let cl = dbContext.TblCalibrationlots.Where(q2 => cd.Lot == q2.CalibLotId).OrderBy(x => x.CalibLotId).FirstOrDefault()
                                             where c.TestId == TestId && c.CurveType == CurveTypeId && (c.CalDate >= Convert.ToDateTime(FormDate) && c.CalDate <= Convert.ToDateTime(ToDate)) && c.SoftDelete != true
                                             select new
                                             {
                                                 CalId = c.CalId,
                                                 CalDetId = cd.CalDetId,
                                                 CurveType = CTy.CrvType,
                                                 CurveTypeId = c.CurveType,
                                                 Pos = cd.Pos,
                                                 Conc = cd.Conc,
                                                 CalDate = c.CalDate.Value.ToString("dd-MMM-yyyy"),
                                                 CalTime = c.CalTime,
                                                 CalValidity = c.CalValidity,
                                                 Factor = c.Factor,
                                                 Factor1 = c.Factor1,
                                                 Factor2 = c.Factor2,
                                                 Factor3 = c.Factor3,
                                                 Factor4 = c.Factor4,
                                                 Factor5 = c.Factor5,
                                                 LotNo = cl.LotNo,
                                                 ExpMonth = cl.ExpMonth,
                                                 ExpYear = cl.ExpYear

                                             }).ToListAsync();

                    GlobalClass.UserActivityLogFile(UserName, "Get Calibration History List", "Exit");
                    return Json(calibration);
                }
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Get Calibration History List", "Exit", ex.Message);
                return Json(false);
            }
        }

        [HttpPost]
        public async Task<JsonResult> SaveCalibration(TblCalibration calibration, IList<TblCalibrationdet> CaliDetl)
        {
            bool returnValue = false;
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");
            try
            {
                // TblCalibration data = await dbContext.TblCalibrations.Where(x => x.CalId == calibration.CalId).FirstOrDefaultAsync();
                if (calibration.CalId == 0)
                {
                    GlobalClass.UserActivityLogFile(UserName, "Test calibration Save Data", "Entry");

                    calibration.CreateBy = HttpContext.Session.GetInt32("UserId");
                    dbContext.TblCalibrations.Add(calibration);
                    await dbContext.SaveChangesAsync();
                    returnValue = true;
                    GlobalClass.UserActivityLogFile(UserName, "Test calibration Save Data", "Exit");

                }
                else
                {
                    int CalId = calibration.CalId;
                    var caliDetail = dbContext.TblCalibrationdets.Where(x => x.CalId == CalId && x.SoftDelete != true).ToList();
                    foreach (var item in caliDetail)
                    {
                        item.SoftDelete = true;
                        item.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        item.ModifyDate = DateTime.Now;
                        dbContext.Entry(item).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                    }
                    //calibration.CalDate = data.CalDate;
                    //calibration.CalTime = data.CalTime;
                    //calibration.CreateBy = data.CreateBy;
                    //calibration.CreateDate = data.CreateDate;
                    calibration.ModifyBy = HttpContext.Session.GetInt32("UserId");
                    calibration.ModifyDate = DateTime.Now;
                    dbContext.Entry(calibration).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();


                    returnValue = true;
                }


                foreach (var item in CaliDetl)
                {
                    if (item.CalDetId == 0)
                    {

                        item.CalId = calibration.CalId;
                        item.CreateBy = HttpContext.Session.GetInt32("UserId");
                        dbContext.TblCalibrationdets.Add(item);
                    }
                    else
                    {
                        var data = dbContext.TblCalibrationdets.Where(x => x.CalDetId == item.CalDetId).FirstOrDefault();
                        if (data != null)
                        {
                            data.SoftDelete = false;
                            data.CalId = calibration.CalId;
                            data.Conc = item.Conc;
                            data.ActualOd = item.ActualOd;
                            data.Od = item.Od;
                            data.ContainerTypeId = item.ContainerTypeId;
                            data.ModifyBy = HttpContext.Session.GetInt32("UserId");
                            data.ModifyDate = DateTime.Now;
                            dbContext.Entry(data).State = EntityState.Modified;
                        }
                    }
                    await dbContext.SaveChangesAsync();
                }
                returnValue = true;
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test calibration Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> GetByIdCalibration(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test calibration Get By Id", "Entry");
            var calibration = await dbContext.TblCalibrations.Where(x => x.CalId == Id && x.SoftDelete != true).FirstOrDefaultAsync();

            var calibrationDetail = await dbContext.TblCalibrationdets.Where(x => x.CalId == Id && x.SoftDelete != true).OrderBy(x => x.CalDetId).ToListAsync();
            if (calibrationDetail.Count() > 0)
            {
                var CalibrationLot = await dbContext.TblCalibrationlots.Where(x => x.CalibLotId == calibrationDetail[0].Lot && x.SoftDelete != true).FirstOrDefaultAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test calibration Get By Id", "Exit");
                return Json(new { calibration, calibrationDetail, CalibrationLot });
            }
            else
            {
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test calibration Get By Id", "Exit");
                return Json(new { calibration, calibrationDetail });
            }
        }
        public async Task<JsonResult> GetByTestIdCalibration(int? TestId)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test calibration Get By Id", "Entry");
            var calibration = await dbContext.TblCalibrations.Where(x => x.TestId == TestId && x.SoftDelete != true).OrderByDescending(x => x.CalId).FirstOrDefaultAsync();
            if (calibration != null)
            {
                var calibrationDetail = await dbContext.TblCalibrationdets.Where(x => x.CalId == calibration.CalId && x.SoftDelete != true).OrderBy(x => x.CalDetId).ToListAsync();
                if (calibrationDetail.Count() > 0)
                {
                    var CalibrationLot = await dbContext.TblCalibrationlots.Where(x => x.CalibLotId == calibrationDetail[0].Lot && x.SoftDelete != true).FirstOrDefaultAsync();
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test calibration Get By Id", "Exit");
                    return Json(new { calibration, calibrationDetail, CalibrationLot });
                }
                else
                {
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test calibration Get By Id", "Exit");
                    return Json(new { calibration, calibrationDetail });
                }
            }
            else
            {
                return Json(null);

            }
        }
        public async Task<JsonResult> DeleteCalibration(int? Id)
        {
            bool returnValue = false;
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Calibration Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblCalibration obj = dbContext.TblCalibrations.Where(x => x.CalId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        obj.ModifyDate = DateTime.Now;
                        dbContext.Entry(obj).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();

                        var caliDetail = dbContext.TblCalibrationdets.Where(x => x.CalId == Id).ToList();
                        foreach (var item in caliDetail)
                        {
                            item.SoftDelete = true;
                            item.ModifyBy = HttpContext.Session.GetInt32("UserId");
                            item.ModifyDate = DateTime.Now;
                            dbContext.Entry(item).State = EntityState.Modified;
                            await dbContext.SaveChangesAsync();
                        }

                        returnValue = true;
                    }
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Calibration Delete", "Exit");
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Calibration Delete", "Exit", ex.Message);
                return Json(false);
            }

        }



        [HttpPost]
        public async Task<JsonResult> UpdateAbsorbanceCalibration(int? CalId, IList<TblCalibrationdet> CaliDetl)
        {
            bool returnValue = false;
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Update Absorbance Calibration", "Exit");
            try
            {

                foreach (var item in CaliDetl)
                {
                    int Id = item.CalDetId;
                    var caliDetail = await dbContext.TblCalibrationdets.Where(x => x.CalDetId == Id).FirstOrDefaultAsync();
                    if (caliDetail != null)
                    {
                        caliDetail.ActualOd = item.ActualOd;
                        caliDetail.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        caliDetail.ModifyDate = DateTime.Now;
                        // dbContext.Update(item);
                        dbContext.Entry(caliDetail).State = EntityState.Modified;
                    }
                    else
                    {
                        item.CalId = (int)CalId;
                        item.CreateBy = HttpContext.Session.GetInt32("UserId");
                        dbContext.TblCalibrationdets.Add(item);
                    }
                    await dbContext.SaveChangesAsync();
                }

                returnValue = true;
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Update Absorbance Calibration", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #endregion


        #region Test Profile

        [HttpGet]
        public async Task<JsonResult> GetTestCode()
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Profile Get Test Code List", "Entry");
            try
            {
                var TestCode = (from e in dbContext.TblTests
                                     join s in dbContext.TblTestDetails
                                     on e.TestId equals s.TestId
                                     select new
                                     {
                                         e.TestId,
                                         e.TestCode,
                                         s.DispIndex,
                                         e.SoftDelete
                                     }).Where(x => x.SoftDelete != true).OrderBy(x => x.DispIndex).ToList();

                GlobalClass.UserActivityLogFile(UserName, "Profile Get Test Code List", "Exit");
                return Json(new { TestCode });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Profile Get Test Code List", "Exit", ex.Message);
                return Json(false);
            }
        }

        [HttpGet]
        public async Task<JsonResult> GetAllTestProfile()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Profile Get List", "Entry");
            try
            {
                var Response = await dbContext.TblTestProfiles.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Profile Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Test Profile Get List", "Exit", ex.Message);
                return Json(false);
            }
        }



        [HttpGet]
        public async Task<JsonResult> DuplicateCheckTestProfile(TblTestProfile testProfile)
        {
            int returnValue = 0;
            if (testProfile.ProfileId == 0)
            {
                TblTestProfile data = dbContext.TblTestProfiles.Where(x => x.ProfileName == testProfile.ProfileName && x.SoftDelete != true).FirstOrDefault();
                if (data == null)
                {
                    dbContext.TblTestProfiles.Add(testProfile);
                    await dbContext.SaveChangesAsync();
                    returnValue = testProfile.ProfileId;
                }
            }
            else
            {
                TblTestProfile data = dbContext.TblTestProfiles.Where(x => x.ProfileId != testProfile.ProfileId && x.ProfileCode == testProfile.ProfileCode).FirstOrDefault();
                if (data == null)
                {
                    dbContext.Entry(testProfile).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                    returnValue = testProfile.ProfileId;
                }
            }
            return Json(returnValue);
        }

        public async Task<JsonResult> SaveTestProfile(IEnumerable<TblProfileDet> profileDets, TblTestProfile testProfile)
        {

            bool returnValue = false;
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");
            try
            {
                if (testProfile.ProfileId == 0)
                {
                    TblTestProfile data = dbContext.TblTestProfiles.Where(x => x.ProfileName == testProfile.ProfileName && x.SoftDelete != true).FirstOrDefault();
                    if (data == null)
                    {
                        testProfile.CreateBy = UserId;
                        testProfile.CreateDate = DateTime.Now;
                        dbContext.TblTestProfiles.Add(testProfile);
                        await dbContext.SaveChangesAsync();
                        if (testProfile.ProfileId > 0)
                        {
                            foreach (var pro in profileDets)
                            {
                                TblProfileDet profileDet = new TblProfileDet();
                                profileDet.ProId = testProfile.ProfileId;
                                profileDet.TestId = pro.TestId;
                                dbContext.TblProfileDets.Add(profileDet);
                                await dbContext.SaveChangesAsync();
                            }
                        }
                        returnValue = true;
                    }

                }

                else
                {
                    GlobalClass.UserActivityLogFile(UserName, "Test Profile Update Data", "Entry");
                    TblTestProfile data = dbContext.TblTestProfiles.Where(x => x.ProfileId != testProfile.ProfileId && x.ProfileName == testProfile.ProfileName && x.SoftDelete != true).FirstOrDefault();
                    if (data == null)
                    {
                        var ProDetail = dbContext.TblProfileDets.Where(x => x.ProId == testProfile.ProfileId).ToList();
                        foreach (var item in ProDetail)
                        {
                            dbContext.TblProfileDets.Remove(item);
                            await dbContext.SaveChangesAsync();
                        }

                        var dataTestProfile = dbContext.TblTestProfiles.Where(x => x.ProfileId == testProfile.ProfileId);
                        foreach (var obj in dataTestProfile)
                        {
                            obj.ProfileCode = testProfile.ProfileCode;
                            obj.ProfileName = testProfile.ProfileName;
                            obj.ModifyBy = UserId;
                            obj.ModifyDate = DateTime.Now;
                        }
                        await dbContext.SaveChangesAsync();
                        foreach (var pro in profileDets)
                        {
                            TblProfileDet profileDet = new TblProfileDet();
                            profileDet.ProId = testProfile.ProfileId;
                            profileDet.TestId = pro.TestId;
                            dbContext.TblProfileDets.Add(profileDet);
                            await dbContext.SaveChangesAsync();
                        }
                        returnValue = true;
                    }
                    GlobalClass.UserActivityLogFile(UserName, "Test Profile Update Data", "Exit");
                }
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Profile Save or Update", "Exit", ex.Message);
                return Json(returnValue);
            }
        }


        public async Task<JsonResult> GetByIdTestProfile(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Test Profile Get By Id", "Entry");
            try
            {
                var testProfile = await dbContext.TblTestProfiles.Where(x => x.ProfileId == Id && x.SoftDelete != true).FirstOrDefaultAsync();
                var profileDetail = await dbContext.TblProfileDets.Where(x => x.ProId == Id).ToListAsync();

                GlobalClass.UserActivityLogFile(UserName, "Test Profile Get By Id", "Exit");
                return Json(new { testProfile, profileDetail });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Profile Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteTestProfile(int? Id)
        {
            bool returnValue = false;

            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Profile Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblTestProfile obj = dbContext.TblTestProfiles.Where(x => x.ProfileId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;
                        dbContext.Entry(obj).State = EntityState.Modified;
                        // dbContext.TblTestProfiles.Remove(obj);
                        await dbContext.SaveChangesAsync();

                        var ProfileDetail = dbContext.TblProfileDets.Where(x => x.ProId == Id).ToList();
                        foreach (var item in ProfileDetail)
                        {
                            dbContext.TblProfileDets.Remove(item);
                            await dbContext.SaveChangesAsync();
                        }
                        returnValue = true;
                    }
                }

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Profile Delete", "Exit");
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Test Profile Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #region Test Sequence


        [HttpGet]
        public async Task<JsonResult> GetTestSequence()
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Test Sequence Get Test Code List", "Entry");
            try
            {
                var TestCode = await dbContext.TblTests.Where(x => x.SoftDelete != true).ToListAsync();
                var SqnceRunIndx = await (from Sqnce in dbContext.TblTestDetails
                                          join tst in dbContext.TblTests
                                          on Sqnce.TestId equals tst.TestId
                                          where tst.SoftDelete != true
                                          select new
                                          {
                                              TestSqunceId = Sqnce.TestSqunceId,
                                              TestId = Sqnce.TestId,
                                              TestCode = tst.TestCode,
                                              RunIndex = Sqnce.RunIndex,

                                          }).OrderBy(x => x.RunIndex).ToListAsync();

                var SqncePrintIndx = await (from Sqnce in dbContext.TblTestDetails
                                            join tst in dbContext.TblTests
                                            on Sqnce.TestId equals tst.TestId
                                            where tst.SoftDelete != true
                                            select new
                                            {
                                                TestSqunceId = Sqnce.TestSqunceId,
                                                TestId = Sqnce.TestId,
                                                TestCode = tst.TestCode,
                                                PrintIndex = Sqnce.PrintIndex,

                                            }).OrderBy(x => x.PrintIndex).ToListAsync();

                var SqnceDisplayIndx = await (from Sqnce in dbContext.TblTestDetails
                                              join tst in dbContext.TblTests
                                              on Sqnce.TestId equals tst.TestId
                                              where tst.SoftDelete != true
                                              select new
                                              {
                                                  TestSqunceId = Sqnce.TestSqunceId,
                                                  TestId = Sqnce.TestId,
                                                  TestCode = tst.TestCode,
                                                  DispIndex = Sqnce.DispIndex,

                                              }).OrderBy(x => x.DispIndex).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Test Sequence Get Test Code List", "Exit");
                return Json(new { SqnceRunIndx, SqncePrintIndx, SqnceDisplayIndx });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Sequence Get Test Code List", "Exit", ex.Message);
                return Json(false);
            }
        }

        [HttpPost]
        public async Task<JsonResult> SaveTestSequence(IEnumerable<TblTestDetail> testSequence)
        {

            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Test Sequence Save Data", "Entry");
            try
            {
                foreach (var item in testSequence)
                {
                    TblTestDetail obj = new TblTestDetail();
                    dbContext.TblTestDetails.UpdateRange(item);
                    dbContext.SaveChanges();

                    //obj.TestId = item.TestId;
                    //obj.RunIndex = item.RunIndex;
                    //obj.PrintIndex = item.PrintIndex;
                    //dbContext.TblTestDetails.Add(obj);
                    //await dbContext.SaveChangesAsync();

                }

                //foreach (var item in testSequence)
                //{
                //    var feature = await dbContext.TblTestDetails
                //                         .Where(h => h.TestId == item.TestId).ToListAsync();

                //    feature.ForEach(c =>
                //    {
                //        c.RunIndex = item.RunIndex;
                //        c.PrintIndex = item.PrintIndex;
                //    });
                //}





                GlobalClass.UserActivityLogFile(UserName, "Test Sequence Save Data", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Sequence Save", "Exit", ex.Message);
                return Json(false);
            }
        }


        #endregion

        #region Test Reflex

        public async Task<JsonResult> GetUnitByTestId(int? TestId)
        {
            try
            {

                var Response = await (from tst in dbContext.TblTests
                                      join unt in dbContext.TblUnits
                                      on tst.Unit equals unt.UntId
                                      where tst.TestId == TestId
                                      select new
                                      {
                                          Unit = unt.UntName
                                      }).FirstOrDefaultAsync();

                return Json(Response);
            }
            catch (Exception ex)
            {
                return Json(false);
            }

        }

        [HttpGet]
        public async Task<JsonResult> GetAllTestReflex()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Reflex Get List", "Entry");
            try
            {
                var Response = await (from rflx in dbContext.TblTestReflexes
                                      join tst in dbContext.TblTests
                                      on rflx.TestId equals tst.TestId
                                      where tst.SoftDelete != true && rflx.SoftDelete != true
                                      select new
                                      {
                                          ReflexId = rflx.ReflexId,
                                          ReflexType = rflx.ReflexType,
                                          LowValue = rflx.LowValue,
                                          HighValue = rflx.HighValue,
                                          TestCode = tst.TestCode,
                                          ReflexDetail = (from rflxDt in dbContext.TblTestReflexDets
                                                          join test in dbContext.TblTests
                                                          on rflxDt.TestId equals test.TestId
                                                          where rflxDt.ReflexId == rflx.ReflexId
                                                          select new
                                                          {
                                                              RflxDetlId = rflxDt.RflxDetlId,
                                                              TestCode = test.TestCode,
                                                          }).ToList(),

                                      }).ToListAsync();

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Reflex Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Test Reflex Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> SaveTestReflex(IEnumerable<TblReflexDet> reflexDets, TblTestReflex testReflex)
        {

            bool returnValue = false;
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");
            try
            {
                if (testReflex.ReflexId == 0)
                {
                    GlobalClass.UserActivityLogFile(UserName, "Test Reflex Save Data", "Entry");
                    TblTestReflex data = dbContext.TblTestReflexes.Where(x => x.TestId == testReflex.TestId && x.ReflexType == testReflex.ReflexType).FirstOrDefault();
                    if (data == null)
                    {
                        testReflex.CreateBy = UserId;
                        testReflex.CreateDate = DateTime.Now;
                        dbContext.TblTestReflexes.Add(testReflex);
                        await dbContext.SaveChangesAsync();
                        if (testReflex.ReflexId > 0)
                        {
                            foreach (var reflx in reflexDets)
                            {
                                TblTestReflexDet reflexDet = new TblTestReflexDet();
                                reflexDet.ReflexId = testReflex.ReflexId;
                                reflexDet.TestId = reflx.TestId;
                                dbContext.TblTestReflexDets.Add(reflexDet);
                                await dbContext.SaveChangesAsync();
                            }
                        }
                        returnValue = true;
                    }
                }

                else
                {
                    GlobalClass.UserActivityLogFile(UserName, "Test Reflex Update Data", "Entry");
                    TblTestReflex data = dbContext.TblTestReflexes.Where(x => x.ReflexId == testReflex.ReflexId && x.TestId != testReflex.TestId && x.ReflexType != testReflex.ReflexType).FirstOrDefault();
                    if (data == null)
                    {
                        var reflxDtl = dbContext.TblTestReflexDets.Where(x => x.ReflexId == testReflex.ReflexId).ToList();
                        foreach (var item in reflxDtl)
                        {
                            dbContext.TblTestReflexDets.Remove(item);
                            await dbContext.SaveChangesAsync();
                        }


                        var dataTestReflex = dbContext.TblTestReflexes.Where(x => x.ReflexId == testReflex.ReflexId);
                        foreach (var obj in dataTestReflex)
                        {
                            obj.TestId = testReflex.TestId;
                            obj.ReflexType = testReflex.ReflexType;
                            obj.ModifyBy = UserId;
                            obj.ModifyDate = DateTime.Now;
                        }
                        await dbContext.SaveChangesAsync();



                        foreach (var pro in reflexDets)
                        {
                            TblTestReflexDet reflexDet = new TblTestReflexDet();
                            reflexDet.ReflexId = testReflex.ReflexId;
                            reflexDet.TestId = pro.TestId;
                            dbContext.TblTestReflexDets.Add(reflexDet);
                            await dbContext.SaveChangesAsync();
                        }
                        returnValue = true;
                    }
                    GlobalClass.UserActivityLogFile(UserName, "Test Reflex Update Data", "Exit");

                }
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Reflex Save or Update", "Exit", ex.Message);
                return Json(returnValue);
            }
        }

        public async Task<JsonResult> GetByIdTestReflex(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Test Reflex Get By Id", "Entry");
            try
            {
                var testReflex = await dbContext.TblTestReflexes.Where(x => x.ReflexId == Id && x.SoftDelete != true).FirstOrDefaultAsync();
                var reflexDetail = await dbContext.TblTestReflexDets.Where(x => x.ReflexId == Id).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Test Reflex Get By Id", "Exit");
                return Json(new { testReflex, reflexDetail });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Reflex Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteTestReflex(int? Id)
        {
            bool returnValue = false;

            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Reflex Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblTestReflex obj = dbContext.TblTestReflexes.Where(x => x.ReflexId == Id).FirstOrDefault();
                    if (obj != null)
                    {

                        obj.SoftDelete = true;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;
                        dbContext.Entry(obj).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();


                        var reflexDetail = dbContext.TblTestReflexDets.Where(x => x.ReflexId == Id).ToList();
                        foreach (var item in reflexDetail)
                        {
                            dbContext.TblTestReflexDets.Remove(item);
                            await dbContext.SaveChangesAsync();
                        }
                        returnValue = true;
                    }
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Reflex Delete", "Exit");
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Test Reflex Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #region Carry Over
        [HttpGet]
        public async Task<JsonResult> GetAllCarryOver()
        {
            UserName = HttpContext.Session.GetString("UserName");
            try
            {
                GlobalClass.UserActivityLogFile(UserName, "Carry Over", "Entry");
                var Response = await (from carryOvr in dbContext.TblTestCarryOvers

                                      select new
                                      {
                                          Idpk = carryOvr.Idpk,
                                          Test1 = dbContext.TblTests.Where(x => x.TestId == carryOvr.TestId).FirstOrDefault().TestCode,
                                          Test2 = dbContext.TblTests.Where(x => x.TestId == carryOvr.AfectedTestId).FirstOrDefault().TestCode,
                                          WashType = carryOvr.WashType,
                                          WashVolume = carryOvr.WashVolume,

                                      }).ToListAsync();



                GlobalClass.UserActivityLogFile(UserName, "Carry Over", "Exist");

                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Carry Over", "Exist", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveCarryOver(TblTestCarryOver carryOver)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Carry Over Save or Update", "Entry");
            try
            {
                if (carryOver.Idpk == 0)
                {
                    carryOver.CreateBy = HttpContext.Session.GetInt32("UserId");
                    dbContext.TblTestCarryOvers.Add(carryOver);
                }
                else
                {
                    var data = dbContext.TblTestCarryOvers.Where(x => x.Idpk == carryOver.Idpk).FirstOrDefault();
                    if (data != null)
                    {
                        data.TestId = carryOver.TestId;
                        data.AfectedTestId = carryOver.AfectedTestId;
                        data.WashType = carryOver.WashType;
                        data.WashVolume = carryOver.WashVolume;
                        data.ModifyBy = UserId;
                        data.ModifyDate = DateTime.Now;
                    }
                }
                await dbContext.SaveChangesAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Carry Over Save or Update", "Exit");

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Carry Over Save or Update", "Exit", ex.Message);

                return Json(false);
            }

        }
        public async Task<JsonResult> GetByIdCarryOver(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Carry Over Get By Id", "Entry");
            var obj = await dbContext.TblTestCarryOvers.Where(x => x.Idpk == Id).ToListAsync();
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Carry Over Get By Id", "Exit");
            return Json(obj);
        }

        public async Task<JsonResult> DeleteCarryOver(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Carry Over Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblTestCarryOver obj = dbContext.TblTestCarryOvers.Where(x => x.Idpk == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        dbContext.TblTestCarryOvers.Remove(obj);
                        await dbContext.SaveChangesAsync();
                    }

                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Carry Over Delete", "Exit");

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Carry Over Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #region Test Calculated
        [HttpGet]
        public async Task<JsonResult> GetAllTestCalculated()
        {
            UserName = HttpContext.Session.GetString("UserName");
            try
            {
                GlobalClass.UserActivityLogFile(UserName, "Test Calculated Get List", "Entry");

                var Response = await (from calcu in dbContext.TblTestCalcus
                                      join unt in dbContext.TblUnits
                                     on calcu.Unit equals unt.UntId
                                      where calcu.SoftDelete != true && unt.SoftDelete != true
                                      select new
                                      {
                                          CalId = calcu.CalId,
                                          CalTestCode = calcu.CalTestCode,
                                          CalTestName = calcu.CalTestName,
                                          CalExpression = calcu.CalExpression,
                                          UntName = unt.UntName,
                                          Decimals = calcu.Decimals,
                                          RefLow = calcu.RefLow,
                                          RefHigh = calcu.RefHigh,

                                      }).ToListAsync();


                GlobalClass.UserActivityLogFile(UserName, "Test Calculated Get List", "Exist");

                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test Calculated Get List", "Exist", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveTestCalculated(TblTestCalcu testCalcu)
        {
            bool returnValue = false;
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Calculated Save or Update", "Entry");
            try
            {
                if (testCalcu.CalId == 0)
                {
                    TblTestCalcu data = dbContext.TblTestCalcus.Where(x => x.CalTestCode == testCalcu.CalTestCode).FirstOrDefault();
                    if (data == null)
                    {
                        //testCalcu.CUid = (sbyte)HttpContext.Session.GetInt32("UserId");
                        dbContext.TblTestCalcus.Add(testCalcu);
                        await dbContext.SaveChangesAsync();
                        returnValue = true;
                    }
                }
                else
                {
                    TblTestCalcu data = dbContext.TblTestCalcus.Where(x => x.CalId != testCalcu.CalId && x.CalTestCode == testCalcu.CalTestCode).FirstOrDefault();
                    if (data == null)
                    {
                        var dataTestCalcu = dbContext.TblTestCalcus.Where(x => x.CalId == testCalcu.CalId).FirstOrDefault();
                        if (dataTestCalcu != null)
                        {
                            dataTestCalcu.CalTestCode = testCalcu.CalTestCode;
                            dataTestCalcu.CalTestName = testCalcu.CalTestName;
                            dataTestCalcu.CalTestId = testCalcu.CalTestId;
                            dataTestCalcu.Decimals = testCalcu.Decimals;
                            dataTestCalcu.Unit = testCalcu.Unit;
                            dataTestCalcu.RefLow = testCalcu.RefLow;
                            dataTestCalcu.RefHigh = testCalcu.RefHigh;
                            dataTestCalcu.CalExpression = testCalcu.CalExpression;
                            dataTestCalcu.SendToHost = testCalcu.SendToHost;



                            //data1.TestId = carryOver.TestId;
                            //data1.AfectedTestId = carryOver.AfectedTestId;
                            //data.WashType = carryOver.WashType;
                            //data.WashVolume = carryOver.WashVolume;
                            //data.ModifyBy = UserId;
                            //data.ModifyDate = DateTime.Now;

                        }
                    }
                    await dbContext.SaveChangesAsync();
                    returnValue = true;
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Calculated Save or Update", "Exit");
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Test Calculated Save or Update", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> GetByIdTestCalculated(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Calculated Get By Id", "Entry");
            var obj = await dbContext.TblTestCalcus.Where(x => x.CalId == Id && x.SoftDelete != true).ToListAsync();
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Calculated Get By Id", "Exit");
            return Json(obj);
        }

        public async Task<JsonResult> DeleteTestCalculated(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Calculated Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblTestCalcu obj = dbContext.TblTestCalcus.Where(x => x.CalId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;
                        await dbContext.SaveChangesAsync();
                    }

                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test Calculated Delete", "Exit");

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Test Calculated Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #region Test External
        [HttpGet]
        public async Task<JsonResult> GetAllTestExternal()
        {
            UserName = HttpContext.Session.GetString("UserName");
            try
            {
                GlobalClass.UserActivityLogFile(UserName, "Test External Get List", "Entry");
                var Response = await (from extrn in dbContext.TblTestExterns
                                      join unt in dbContext.TblUnits
                                      on extrn.Unit equals unt.UntId into units
                                      from u in units.DefaultIfEmpty()
                                      where u.SoftDelete != true
                                      select new
                                      {
                                          ExternTestId = extrn.ExternTestId,
                                          TestName = extrn.TestName,
                                          TestCode = extrn.TestCode,
                                          ResultType = extrn.ResultType,
                                          Decimals = extrn.Decimals,
                                          UntName = u.UntName,
                                          RefLow = extrn.RefLow,
                                          RefHigh = extrn.RefHigh,
                                      }).ToListAsync();

                GlobalClass.UserActivityLogFile(UserName, "Test External Get List", "Exist");

                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Test External Get List", "Exist", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveTestExternal(TblTestExtern testExtern)
        {

            bool returnValue = false;
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test External Save or Update", "Entry");
            try
            {
                if (testExtern.ExternTestId == 0)
                {
                    TblTestExtern data = dbContext.TblTestExterns.Where(x => x.TestCode == testExtern.TestCode).FirstOrDefault();
                    if (data == null)
                    {
                        testExtern.CreateBy = HttpContext.Session.GetInt32("UserId");
                        dbContext.TblTestExterns.Add(testExtern);
                        await dbContext.SaveChangesAsync();
                        returnValue = true;
                    }
                }
                else
                {
                    TblTestExtern data = dbContext.TblTestExterns.Where(x => x.ExternTestId != testExtern.ExternTestId && x.TestCode == testExtern.TestCode).FirstOrDefault();
                    if (data == null)
                    {
                        data.TestName = testExtern.TestName;
                        data.TestCode = testExtern.TestCode;
                        data.ResultType = testExtern.ResultType;
                        data.Unit = testExtern.Unit;
                        data.Decimals = testExtern.Decimals;
                        data.RefLow = testExtern.RefLow;
                        data.RefHigh = testExtern.RefHigh;
                        data.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        data.ModifyDate = DateTime.Now;
                        await dbContext.SaveChangesAsync();
                        returnValue = true;
                    }
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test External Save or Update", "Exit");
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Test External Save or Update", "Exit", ex.Message);

                return Json(false);
            }

        }

        public async Task<JsonResult> GetByIdTestExternal(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test External Get By Id", "Entry");
            var obj = await dbContext.TblTestExterns.Where(x => x.ExternTestId == Id).FirstOrDefaultAsync();
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test External Get By Id", "Exit");
            return Json(obj);
        }

        public async Task<JsonResult> DeleteTestExternal(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test External Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblTestExtern obj = dbContext.TblTestExterns.Where(x => x.ExternTestId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        obj.ModifyDate = DateTime.Now;
                        dbContext.TblTestExterns.Remove(obj);
                        await dbContext.SaveChangesAsync();
                    }
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Test External Delete", "Exit");

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Test External Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

    }
}
