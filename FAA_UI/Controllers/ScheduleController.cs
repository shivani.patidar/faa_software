﻿using Microsoft.AspNetCore.Mvc;
using System.Collections;
using static System.Net.Mime.MediaTypeNames;

namespace FAA_UI.Controllers
{
    public class ScheduleController : Controller
    {
        DbFaaSoftwareContext dbContext = new DbFaaSoftwareContext();

        public IActionResult Index()
        {
            return View();
        }

        #region Patient
        [HttpGet]
        public async Task<JsonResult> GetPaitentMst()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Paitent Master List", "Entry");
            try
            {
                var doctor = await dbContext.TblDoctordetails.Where(x => x.SoftDelete != true).ToListAsync();
                var sample = await dbContext.TblSampletypes.Where(x => x.SoftDelete != true).ToListAsync();
                var sampleCon = await dbContext.TblDirectorySamplecontainers.Where(x => x.SoftDelete != true).ToListAsync();

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Paitent Master List", "Exit");
                return Json(new { doctor, sample, sampleCon });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Get Paitent Master List", "Exit", ex.Message);
                return Json(false);
            }
        }
    
        [HttpGet]
        public async Task<JsonResult> GetAllPatient()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Patient Get List", "Entry");
            try
            {
                var Response = await dbContext.TblPatients.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Patient Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Patient Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SavePatient(TblPatient patient)
        {
            int PatId = 0;
            try
            {
                if (patient.PatId == 0)
                {
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Patient Save Data", "Entry");

                    TblPatient obj = dbContext.TblPatients.Where(x => x.PatientId == patient.PatientId).FirstOrDefault();
                    if (obj == null)
                    {
                        patient.CreateBy = (sbyte?)HttpContext.Session.GetInt32("UserId");
                        dbContext.TblPatients.Add(patient);
                        await dbContext.SaveChangesAsync();
                        PatId = patient.PatId;
                        GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Patient Save Data", "Exit");
                    }
                }
                else
                {
                    TblPatient data = await dbContext.TblPatients.Where(x => x.PatId != patient.PatId && x.PatientId == patient.PatientId).FirstOrDefaultAsync();
                    if (data == null)
                    {
                        GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Patient Update Data", "Entry");

                        var dataPatient = dbContext.TblPatients.Where(x => x.PatId == patient.PatId).FirstOrDefault();
                        if (dataPatient != null)
                        {
                            dataPatient.PatientId = patient.PatientId;
                            dataPatient.PatName = patient.PatName;
                            dataPatient.Dob = patient.Dob;
                            dataPatient.Height = patient.Height;
                            dataPatient.Weight = patient.Weight;
                            dataPatient.Sex = patient.Sex;
                            dataPatient.Address = patient.Address;

                            dataPatient.ModifyBy = (sbyte?)HttpContext.Session.GetInt32("UserId");
                            dataPatient.ModifyDate = DateTime.Now;
                        }

                        await dbContext.SaveChangesAsync();
                        PatId = patient.PatId;
                        GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Patient Update Data", "Exit");
                    }
                }
                return Json(new { result = true, PatId });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "patient Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> GetByIdPatient(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "patient Get By Id", "Entry");
            try
            {
                var patient = await dbContext.TblPatients.Where(x => x.PatId == Id).FirstOrDefaultAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "patient Get By Id", "Exit");
                return Json(new { patient });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "patient Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeletePatient(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "patient Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblPatient obj = dbContext.TblPatients.Where(x => x.PatId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = (sbyte?)HttpContext.Session.GetInt32("UserId");
                        obj.ModifyDate = DateTime.Now;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                }

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "patient Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "patient Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> SearchPatientById(string PatientId)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "patient Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblPatients.Where(x => x.PatientId.ToLower().StartsWith(PatientId.ToLower())).ToListAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "patient Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "patient Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> SearchPatientByName(string PatientName)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "patient Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblPatients.Where(x => x.PatName.ToLower().StartsWith(PatientName.ToLower())).ToListAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "patient Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "patient Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        #endregion


        #region Sample

        [HttpGet]
        public async Task<JsonResult> GetProfileTestCode(int? ProfileId)
        {
            var profileTestCode = await dbContext.TblProfileDets.Where(x => x.ProId == ProfileId).ToListAsync();
            return Json(profileTestCode);
        }

        [HttpGet]
        public async Task<JsonResult> GetAllWorkList()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Get List", "Entry");
            try
            {


                //var materialTypes = (from u in dbContext.TblQclots
                //                     select new { u.LotId }).ToList()
                // .Union(from u in dbContext.TblCalibrationdets
                //        select  u.Lot)

                // .ToList();

          

                var sample = (from smp in dbContext.TblSamples
                              join pat in dbContext.TblPatients
                              on smp.PatientId equals pat.PatId
                              join stype in dbContext.TblSampletypes
                           on smp.SmpTypeId equals stype.SmpId
                              where smp.SoftDelete != true && pat.SoftDelete != true && stype.SoftDelete != true
                              select new
                              {
                                  PkId = smp.SmpId.ToString(),
                                  Type = "Sample",
                                  Position = "",
                                  LotNo = "",
                                  TestCode = (from smpTest in dbContext.TblSampleTests
                                              join test in dbContext.TblTests
                                              on smpTest.TestId equals test.TestId
                                              where smpTest.SampleId == smp.SmpId
                                              select new
                                              {
                                                  Position = smp.Position.ToString(),
                                                  TestCode = test.TestCode,
                                              }).ToList(),
                                  Patient = pat.PatName + " (" + pat.PatientId.ToString() + ")",
                                  SampleType = stype.SmpType,
                                  SampleId = smp.SmpBarcode.ToString(),
                                  LotId = "",

                              }).ToList();



                var qualtityControl = (from qc in dbContext.TblQclots
                                       where (qc.ExpYear >= DateTime.Now.Year) && qc.SoftDelete != true
                                       // where ((qc.ExpYear > DateTime.Now.Year) || ((qc.ExpYear == DateTime.Now.Year) && (Convert.ToInt16(qc.ExpMonth) >= DateTime.Now.Month))) && qc.SoftDelete != true
                                       select new
                                       {
                                           PkId = qc.LotId.ToString(),
                                           Type = "QC",
                                           Position = dbContext.TblQclotDets.Where(y => y.LotId == qc.LotId).FirstOrDefault() != null ? dbContext.TblQclotDets.Where(y => y.LotId == qc.LotId).FirstOrDefault().Position.ToString() : "",
                                           LotNo = qc.LotNo.ToString(),
                                           TestCode = (from qclot in dbContext.TblQclots
                                                       join qcDet in dbContext.TblQclotDets
                                                       on qclot.LotId equals qcDet.LotId
                                                       join test in dbContext.TblTests
                                                       on qcDet.TestId equals test.TestId
                                                       where qc.LotNo == qclot.LotNo && qclot.SoftDelete != true
                                                       select new
                                                       {
                                                           Position = dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault() != null ? dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault().Position.ToString() : "",
                                                           TestCode = test.TestCode,
                                                       }).ToList(),

                                           Patient = "",
                                           SampleType = "QC",
                                           SampleId = "",
                                           LotId = "",
                                       }).Where(x => x.Position != "0").ToList();


                //var calibration = (from cl in dbContext.TblCalibrationdets
                //                   where cl.SoftDelete != tr
                //                   ue && cl.Pos > 0
                //                   select new
                //                   {

                //                       PkId = cl.CalId.ToString(),

                //                       Type = "Calibration",
                //                       LotNo = (from lot in dbContext.TblCalibrationlots
                //                                where cl.Lot == lot.CalibLotId && lot.SoftDelete != true
                //                                select new
                //                                {
                //                                    LotNo = lot.LotNo,
                //                                }).FirstOrDefault().LotNo,

                //                       TestCode = (from cla in dbContext.TblCalibrationdets
                //                                   join clb in dbContext.TblCalibrations
                //                                   on cla.CalId equals clb.CalId
                //                                   join tst in dbContext.TblTests
                //                                   on clb.TestId equals tst.TestId
                //                                   where cla.Lot == cl.Lot && cla.Pos > 0 && cla.SoftDelete != true && clb.SoftDelete != true && tst.SoftDelete != true
                //                                   select new
                //                                   {
                //                                       Position = cla.Pos.ToString(),
                //                                       TestCode = tst.TestCode
                //                                   }).ToList(),
                //                       Patient = "",
                //                       SampleType = "Calibration",
                //                       SampleId = "",
                //                       LotId = cl.Lot.ToString(),
                //                   }).ToList();

                //var CalQc = qualtityControl.Concat(calibration).ToList().DistinctBy(x => x.LotNo).ToList();
                var calibration = (from cl in dbContext.TblCalibrationdets
                                   join clb in dbContext.TblCalibrations
                                   on cl.CalId equals clb.CalId
                                   join tst in dbContext.TblTests
                                   on clb.TestId equals tst.TestId
                                    where cl.Pos > 0 && cl.SoftDelete != true && clb.SoftDelete != true && tst.SoftDelete != true
                                   select new
                                   {

                                       PkId = cl.CalId.ToString(),
                                       Type = "Cal",
                                       Position = cl.Pos.ToString(),
                                       LotNo = (from lot in dbContext.TblCalibrationlots
                                                where cl.Lot == lot.CalibLotId && lot.SoftDelete != true
                                                select new
                                                {
                                                    LotNo = lot.LotNo,
                                                }).FirstOrDefault().LotNo,

                                       TestCode = (from cla in dbContext.TblCalibrationdets
                                                   where cla.CalDetId == cl.CalDetId
                                                   select new
                                                   {
                                                       Position = cl.Pos.ToString(),
                                                       TestCode = tst.TestCode
                                                   }).ToList(),
                                       Patient = "",
                                       SampleType = "Calibration",
                                       SampleId = "",
                                       LotId = cl.Lot.ToString(),
                                   }).ToList();
                var CalQc = qualtityControl.Concat(calibration).ToList();

                var Response = CalQc.Concat(sample).ToList();


                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Sample Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpGet]
        public async Task<JsonResult> GetAllSample()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Get List", "Entry");
            try
            {
                var Response = await (from smp in dbContext.TblSamples
                                      join stype in dbContext.TblSampletypes
                                      on smp.SmpTypeId equals stype.SmpId
                                      join pat in dbContext.TblPatients
                                      on smp.PatientId equals pat.PatId
                                      where smp.SoftDelete != true && stype.SoftDelete != true && pat.SoftDelete != true
                                      select new
                                      {
                                          PkId = smp.SmpId.ToString(),
                                          Type = "Sample",
                                          LotNo = "",
                                          TestCode = (from smpTest in dbContext.TblSampleTests
                                                      join test in dbContext.TblTests
                                                      on smpTest.TestId equals test.TestId
                                                      where smpTest.SampleId == smp.SmpId
                                                      select new
                                                      {
                                                          Position = smp.Position.ToString(),
                                                          TestCode = test.TestCode,
                                                      }).ToList(),

                                          Patient = pat.PatName + " (" + pat.PatientId.ToString() + ")",
                                          SampleType = stype.SmpType,
                                          SampleId = smp.SmpBarcode.ToString(),

                                      }).ToListAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Sample Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveSample(IEnumerable<TblSampleTest> sampleTest, TblSample sample)
        {
            bool returnValue = false;
            try
            {
                if (sample.SmpId == 0)
                {
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Test Save Data", "Entry");
                    sample.CreateBy = (sbyte?)HttpContext.Session.GetInt32("UserId");
                    dbContext.TblSamples.Add(sample);
                    await dbContext.SaveChangesAsync();
                    if (sample.SmpId > 0)
                    {
                        foreach (var test in sampleTest)
                        {
                            TblSampleTest smpTest = new TblSampleTest();
                            smpTest.SampleId = sample.SmpId;
                            smpTest.TestId = test.TestId;
                            dbContext.TblSampleTests.Add(smpTest);
                            await dbContext.SaveChangesAsync();
                        }
                    }
                    returnValue = true;
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Test Save Data", "Exit");
                }
                else
                {

                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Test Update Data", "Entry");
                    var data = dbContext.TblSamples.Where(x => x.SmpId == sample.SmpId).FirstOrDefault();
                    if (data != null)
                    {
                        data.SmpBarcode = sample.SmpBarcode;
                        data.PatientId = sample.PatientId;
                        data.SmpTypeId = sample.SmpTypeId;
                        data.DrId = sample.DrId;
                        data.Age = sample.Age;
                        data.Height = sample.Height;
                        data.Weight = sample.Weight;
                        data.LebHospitalName = sample.LebHospitalName;
                        data.HospitalArea = sample.HospitalArea;
                        data.BedNumber = sample.BedNumber;
                        data.SmpRemark = sample.SmpRemark;
                        data.RegistrationDate = sample.RegistrationDate;
                        data.CollectionDate = sample.CollectionDate;
                        data.Position = sample.Position;
                        data.SmpContTypeId = sample.SmpContTypeId;
                        data.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        data.ModifyDate = DateTime.Now;
                    }

                    await dbContext.SaveChangesAsync();


                    var SmpTest = dbContext.TblSampleTests.Where(x => x.SampleId == sample.SmpId).ToList();
                    foreach (var test in SmpTest)
                    {
                        dbContext.TblSampleTests.Remove(test);
                        await dbContext.SaveChangesAsync();
                    }
                    foreach (var test in sampleTest)
                    {
                        TblSampleTest obj = new TblSampleTest();
                        obj.SampleId = sample.SmpId;
                        obj.SmpTstId = test.SmpTstId;
                        obj.TestId = test.TestId;
                        dbContext.TblSampleTests.Add(obj);
                        await dbContext.SaveChangesAsync();
                    }

                    returnValue = true;
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Update Data", "Exit");
                }


            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Sample Save or Update", "Exit", ex.Message);

            }
            return Json(returnValue);
        }

        public async Task<JsonResult> GetByIdSample(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Get By Id", "Entry");
            try
            {

                var sample = (from smp in dbContext.TblSamples
                              where smp.SmpId == Id
                              select new
                              {
                                  SmpId = smp.SmpId,
                                  SmpBarcode = smp.SmpBarcode,
                                  DrId = smp.DrId,
                                  Height = smp.Height,
                                  LebHospitalName = smp.LebHospitalName,
                                  Weight = smp.Weight,
                                  Position = smp.Position,
                                  PatientId = smp.PatientId,
                                  RegistrationDate = smp.RegistrationDate,
                                  CollectionDate = smp.CollectionDate,
                                  SmpContTypeId = smp.SmpContTypeId,
                                  SmpRemark = smp.SmpRemark,
                                  HospitalArea = smp.HospitalArea,
                                  SmpTypeId = smp.SmpTypeId,
                                  Age = smp.Age,
                                  BedNumber = smp.BedNumber,
                                  sampleTest = (from smpTst in dbContext.TblSampleTests
                                                where smp.SmpId == smpTst.SampleId
                                                select new
                                                {
                                                    smpTst.SmpTstId,
                                                    smpTst.TestId,
                                                }).ToList()

                              }).FirstOrDefault();


                // var sample = await dbContext.TblSamples.Where(x => x.SmpId == Id).FirstOrDefaultAsync();
                // var sampleTest = await dbContext.TblSampleTests.Where(x => x.SampleId == Id).ToListAsync();
                if (sample != null)
                {
                    var patient = await dbContext.TblPatients.Where(x => x.PatId == sample.PatientId).FirstOrDefaultAsync();
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Get By Id", "Exit");
                    return Json(new { sample, patient });
                }
                else
                {
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Get By Id", "Exit");
                    return Json(new { sample });
                }

            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Sample Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteSample(int? Id)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblSample obj = dbContext.TblSamples.Where(x => x.SmpId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = (sbyte?)HttpContext.Session.GetInt32("UserId");
                        obj.ModifyDate = DateTime.Now;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                    var SmpTest = dbContext.TblSampleTests.Where(x => x.SmpTstId == Id).ToList();
                    foreach (var test in SmpTest)
                    {
                        dbContext.TblSampleTests.Remove(test);
                        await dbContext.SaveChangesAsync();
                    }
                }

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Sample Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> SearchSampleBySmpBarCode(string SmpBarCode)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "patient Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblSamples.Where(x => x.SmpBarcode.ToLower().StartsWith(SmpBarCode.ToLower())).ToListAsync();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "patient Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "patient Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> UpdatePatientIdSample(int? SampleId, int PatientId)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Delete", "Entry");
            try
            {
                if (SampleId > 0)
                {
                    TblSample obj = dbContext.TblSamples.Where(x => x.SmpId == SampleId && x.SoftDelete != true).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.PatientId = PatientId;
                        obj.ModifyBy = (sbyte?)HttpContext.Session.GetInt32("UserId");
                        obj.ModifyDate = DateTime.Now;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                  
                }

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Update", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Sample Update", "Exit", ex.Message);
                return Json(false);
            }

        }


        #endregion


        [HttpGet]
        public async Task<JsonResult> GetCalibrationQCMst()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Paitent Master List", "Entry");
            try
            {
                List<int> SampleNumber = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60 };
                List<int> ReagentNumber = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80 };

                var SamplePoistion = (from s in SampleNumber
                                      where !dbContext.TblCalibrationdets.Any(es => (es.Pos == s)) && !dbContext.TblQclotDets.Any(es => (es.Position == s)) && !dbContext.TblSamples.Any(es => (es.Position == s))
                                      select s).ToList();

                var CalibrationLotNo = (from cl in dbContext.TblCalibrationdets
                                        select new
                                        {
                                            CalId = cl.CalId,
                                            LotId = cl.Lot,
                                            LotNo = (from lot in dbContext.TblCalibrationlots
                                                     where cl.Lot == lot.CalibLotId
                                                     select new
                                                     {
                                                         lot.LotNo
                                                     }).FirstOrDefault().LotNo,

                                        }).ToList().DistinctBy(x => x.LotId).ToList();

                var ContainerType = await dbContext.TblDirectorySamplecontainers.Where(x => x.SoftDelete != true).ToListAsync();

                var QcLotNo = dbContext.TblQclots.Where(x => x.SoftDelete != true).ToList().DistinctBy(x => x.LotNo).ToList();

                // Reagent
                var RegLotNo = dbContext.TblRgtlots.Where(x => x.SoftDelete != true).ToList().DistinctBy(x => x.LotNo).ToList();

                var RPoistion = (from r in ReagentNumber
                                 where (!dbContext.TblRgtbotles.Any(es => (es.Position == r)))
                                 select r).ToList();

                var rgtManufacturer = dbContext.TblManufacturers.Where(x => x.SoftDelete != true && x.MnfReg != null).ToList();

                //var ReagentType = await dbContext.TblRgtbotles.Where(x => x.SoftDelete != true).ToListAsync();

                var bottlesize = await dbContext.TblDirectoryReagentbottletypes.Where(x => x.SoftDelete != true).ToListAsync();


                // month Year

                List<Month> months = new List<Month>();

                months.Add(new Month { monValue = 01, monName = "January" });
                months.Add(new Month { monValue = 02, monName = "February" });
                months.Add(new Month { monValue = 03, monName = "March" });
                months.Add(new Month { monValue = 04, monName = "April" });
                months.Add(new Month { monValue = 05, monName = "May" });
                months.Add(new Month { monValue = 06, monName = "June" });
                months.Add(new Month { monValue = 07, monName = "July" });
                months.Add(new Month { monValue = 08, monName = "August" });
                months.Add(new Month { monValue = 09, monName = "September" });
                months.Add(new Month { monValue = 10, monName = "October" });
                months.Add(new Month { monValue = 11, monName = "November" });
                months.Add(new Month { monValue = 12, monName = "December" });


                ArrayList Year = new ArrayList();
                //Dictionary<int, string> Months = Enumerable.Range(1, 12).Select(i => new KeyValuePair<int, string>(i, System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(i))).ToDictionary(x => x.Key, x => x.Value);
                int CurntYear = DateTime.Now.Year;
                for (int i = CurntYear; i < (CurntYear + 5); i++)
                {
                    Year.Add(i);
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Paitent Master List", "Exit");
                return Json(new { ContainerType, CalibrationLotNo, QcLotNo, SamplePoistion, RegLotNo, RPoistion, rgtManufacturer, bottlesize, months, Year });

            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Get Paitent Master List", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> MonthYear()
        {

            List<Month> months = new List<Month>();

            months.Add(new Month { monValue = 01, monName = "January" });
            months.Add(new Month { monValue = 02, monName = "February" });
            months.Add(new Month { monValue = 03, monName = "March" });
            months.Add(new Month { monValue = 04, monName = "April" });
            months.Add(new Month { monValue = 05, monName = "May" });
            months.Add(new Month { monValue = 06, monName = "June" });
            months.Add(new Month { monValue = 07, monName = "July" });
            months.Add(new Month { monValue = 08, monName = "August" });
            months.Add(new Month { monValue = 09, monName = "September" });
            months.Add(new Month { monValue = 10, monName = "October" });
            months.Add(new Month { monValue = 11, monName = "November" });
            months.Add(new Month { monValue = 12, monName = "December" });


            ArrayList Year = new ArrayList();
            //Dictionary<int, string> Months = Enumerable.Range(1, 12).Select(i => new KeyValuePair<int, string>(i, System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(i))).ToDictionary(x => x.Key, x => x.Value);
            int CurntYear = DateTime.Now.Year;
            for (int i = CurntYear; i < (CurntYear + 5); i++)
            {
                Year.Add(i);
            }
            return Json(new { months, Year });
        }

        #region Calibration
        [HttpGet]
        public async Task<JsonResult> GetListLot()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Calibration Master List", "Entry");
            try
            {
                var calibrationLot = await dbContext.TblCalibrationlots.Where(x => x.SoftDelete != true).ToListAsync();
                var qcLot = dbContext.TblQclots.Where(x => x.SoftDelete != true).ToList().DistinctBy(x => x.LotNo).ToList();
                var rgLot = dbContext.TblRgtlots.Where(x => x.SoftDelete != true).ToList().DistinctBy(x => x.LotNo).ToList();
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Calibration Master List", "Exit");
                return Json(new { calibrationLot, qcLot, rgLot });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Get Calibration Master List", "Exit", ex.Message);
                return Json(false);
            }
        }

        [HttpGet]
        public async Task<JsonResult> GetAllCalibration()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Calibration List", "Entry");
            try
            {
                var calibration = (from cl in dbContext.TblCalibrationdets
                                   select new
                                   {
                                       CalId = cl.CalId,
                                       LotId = cl.Lot,
                                       LotNo = (from lot in dbContext.TblCalibrationlots
                                                where cl.Lot == lot.CalibLotId
                                                select new
                                                {
                                                    LotNo = lot.LotNo,
                                                    Expiry = lot.ExpMonth + "/" + lot.ExpYear,
                                                }).FirstOrDefault(),

                                       TestCode = (from cla in dbContext.TblCalibrationdets
                                                   join clb in dbContext.TblCalibrations
                                                   on cla.CalId equals clb.CalId

                                                   join tst in dbContext.TblTests
                                                   on clb.TestId equals tst.TestId
                                                   where cla.Lot == cl.Lot && cla.SoftDelete != true && clb.SoftDelete != true
                                                   select new
                                                   {
                                                       CalId = cla.CalId,
                                                       CalDetId = cla.CalDetId,
                                                       Position = cla.Pos,
                                                       ContainerTypeId = cla.ContainerTypeId,
                                                       conc = cla.Conc,
                                                       Lot = cla.Lot,
                                                       TestCode = tst.TestCode
                                                   }).OrderBy(x => x.CalId).ToList(),
                                   }).ToList().DistinctBy(x => x.LotId).ToList();

                //dbContext.tblcal .Where(t => t.EmployeeId == payr)
                //.DefaultIfEmpty(yourDefaultObject)

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Calibration List", "Exit");
                return Json(calibration);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Get Calibration List", "Exit", ex.Message);
                return Json(false);
            }
        }

        [HttpPost]
        public async Task<JsonResult> SaveCalibration(IEnumerable<TblCalibrationdet> calibrationdets)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save Data", "Entry");
            try
            {
                foreach (var item in calibrationdets)
                {
                    var data = dbContext.TblCalibrationdets.Where(x => x.CalDetId == item.CalDetId).FirstOrDefault();
                    if (data != null)
                    {
                        data.SoftDelete = false;
                        data.Pos = item.Pos;
                        data.ContainerTypeId = item.ContainerTypeId;
                        data.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        data.ModifyDate = DateTime.Now;
                        await dbContext.SaveChangesAsync();
                    }
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save Data", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save", "Exit", ex.Message);
                return Json(false);
            }
        }


        public async Task<JsonResult> DeleteCal(int? LotId)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save Data", "Entry");
            try
            {
                var calibrationdets = dbContext.TblCalibrationdets.Where(x => x.Lot == LotId).ToList();
                foreach (var item in calibrationdets)
                {

                    var data = dbContext.TblCalibrationdets.Where(x => x.CalDetId == item.CalDetId && x.SoftDelete != true).FirstOrDefault();
                    if (data != null)
                    {
                        data.Pos = 0;
                        data.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        data.ModifyDate = DateTime.Now;
                        //data.ContainerTypeId = item.ContainerTypeId;
                        await dbContext.SaveChangesAsync();
                    }
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save Data", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> GetCalibrationByLotId(int? LotId)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Get By Id", "Entry");
            try
            {
                var CalDetail = dbContext.TblCalibrationdets.Where(x => x.Lot == LotId).ToList().DistinctBy(x => x.CalId).ToList();

                //var calibration = (from calDt in CalDetail
                //                   join cal in dbContext.TblCalibrations
                //                   on calDt.CalId equals cal.CalId
                //                   where calDt.Lot == LotId && cal.SoftDelete != true
                //                   select new
                //                   {
                //                       cal.CalId,
                //                       calDt.Lot,
                //                       calDt.Pos,
                //                       TestCode = (from clDeta in CalDetail
                //                                   join clb in dbContext.TblCalibrations
                //                                   on clDeta.CalId equals clb.CalId
                //                                   join tst in dbContext.TblTests
                //                                   on clb.TestId equals tst.TestId
                //                                   where clDeta.Lot == calDt.Lot
                //                                   select new
                //                                   {
                //                                       CalDetId = clDeta.CalDetId,
                //                                       TestCode = tst.TestCode,
                //                                       Position = clDeta.Pos,
                //                                       ContainerTypeId = clDeta.ContainerTypeId,

                //                                   }).ToList(),

                //                   }).ToList();



                var calibration = (from calDt in CalDetail
                                   join cal in dbContext.TblCalibrations
                                   on calDt.CalId equals cal.CalId
                                   where calDt.Lot == LotId && cal.SoftDelete != true
                                   select new
                                   {
                                       cal.CalId,
                                       calDt.Lot,
                                       calDt.Pos,
                                       TestCode = (from clDeta in dbContext.TblCalibrationdets
                                                   join clb in dbContext.TblCalibrations
                                                   on clDeta.CalId equals clb.CalId
                                                   join tst in dbContext.TblTests
                                                   on clb.TestId equals tst.TestId
                                                   where clDeta.Lot == calDt.Lot && clDeta.SoftDelete != true
                                                   select new
                                                   {
                                                       CalDetId = clDeta.CalDetId,
                                                       TestCode = tst.TestCode,
                                                       Position = clDeta.Pos,
                                                       ContainerTypeId = clDeta.ContainerTypeId,

                                                   }).ToList(),

                                   }).ToList();



                return Json(new { calibration });

            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Sample Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }





        #endregion

        /* -------------------------------- Reagent starts here ---------------------------------*/
        #region Reagent

        /* following function added by jayashri - fill reagent position and tests */
        public async Task<JsonResult> GetRegByLotId(int? LotId)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Get By Id", "Entry");
            try
            {
                var response = await (from rgtLot in dbContext.TblRgtlots
                                      join tst in dbContext.TblTests
                                      on rgtLot.TestId equals tst.TestId
                                      where rgtLot.SoftDelete != true && rgtLot.RlotId == LotId
                                      select new
                                      {
                                          LotId = rgtLot.RlotId,
                                          TestCode = tst.TestCode,
                                          ExpMonth = rgtLot.ExpMonth,
                                          ExpYear = rgtLot.ExpYear,
                                          Position = dbContext.TblRgtbotles.Where(y => y.SoftDelete != true && y.RlotId == rgtLot.RlotId).FirstOrDefault() != null ? (int)dbContext.TblRgtbotles.Where(y => y.SoftDelete != true && y.RlotId == rgtLot.RlotId).FirstOrDefault().Position : 0,
                                          Volume = dbContext.TblRgtbotles.Where(y => y.SoftDelete != true && y.RlotId == rgtLot.RlotId).FirstOrDefault() != null ? (int)dbContext.TblRgtbotles.Where(y => y.SoftDelete != true && y.RlotId == rgtLot.RlotId).FirstOrDefault().Volume : 0
                                      }).FirstOrDefaultAsync();

                return Json(response);

            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Sample Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> GetAllReagent()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Calibration List", "Entry");
            try
            {
                var resreagent = (from rg in dbContext.TblRgtbotles
                                  join tst in dbContext.TblTests
                                  on rg.TestId equals tst.TestId

                                  select new
                                  {
                                      LotId = rg.RlotId,
                                      testid = tst.TestId,
                                      position = rg.Position,
                                      btype = rg.RgtType,
                                      bvolume = rg.Volume,

                                  }).ToList();


                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Reagent List", "Exit");
                return Json(resreagent);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Get Calibration List", "Exit", ex.Message);
                return Json(false);
            }
        }

        #endregion

        #region QC
        [HttpGet]
        public async Task<JsonResult> GetAllQC()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Quality Control List", "Entry");
            try
            {


                var qualtityControl = (from qc in dbContext.TblQclots
                                       where qc.ExpYear >= DateTime.Now.Year && qc.SoftDelete != true
                                       select new
                                       {
                                           LotNo = qc.LotNo,
                                           Expiry = qc.ExpMonth + "/" + qc.ExpYear,
                                           TestCode = (from qclot in dbContext.TblQclots
                                                       join qcDet in dbContext.TblQclotDets
                                                       on qclot.LotId equals qcDet.LotId
                                                       join test in dbContext.TblTests
                                                       on qcDet.TestId equals test.TestId
                                                       where qc.LotNo == qclot.LotNo && qclot.SoftDelete != true
                                                       select new
                                                       {
                                                           LotId = qclot.LotId,
                                                           TestCode = test.TestCode,
                                                           qcLotDetId = dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault() != null ? dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault().QcdetId : 0,
                                                           Position = dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault() != null ? dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault().Position : 0,
                                                           Concentration = dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault() != null ? dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault().Conc : 0,
                                                       }).ToList(),
                                       }).ToList().DistinctBy(x => x.LotNo).ToList();


                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Quality Control List", "Exit");
                return Json(qualtityControl);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Get Quality Control List", "Exit", ex.Message);
                return Json(false);
            }
        }

        [HttpPost]
        public async Task<JsonResult> SaveQC(IEnumerable<TblQclotDet> qcDet)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save Data", "Entry");
            try
            {
                foreach (var item in qcDet)
                {
                    var data = dbContext.TblQclotDets.Where(x => x.QcdetId == item.QcdetId && x.LotId == item.LotId).FirstOrDefault();
                    if (data != null)
                    {
                        data.Position = 0;
                        //data.Conc = 0;
                        dbContext.Entry(data).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                    }
                }

                foreach (var item in qcDet)
                {
                    var data = dbContext.TblQclotDets.Where(x => x.QcdetId == item.QcdetId && x.LotId == item.LotId).FirstOrDefault();
                    if (data == null)
                    {

                        TblQclotDet obj = new TblQclotDet();

                        obj.LotId = item.LotId;
                        obj.Position = item.Position;
                        obj.Conc = item.Conc;
                        dbContext.TblQclotDets.Add(obj);
                        await dbContext.SaveChangesAsync();
                    }
                    else
                    {
                        data.Position = item.Position;
                        data.Conc = item.Conc;
                        dbContext.Entry(data).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();

                    }
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save Data", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteQC(string LotNo)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save Data", "Entry");
            try
            {


                var qc = dbContext.TblQclots.Where(x => x.LotNo == LotNo).ToList();
                foreach (var item in qc)
                {
                    var data = dbContext.TblQclotDets.Where(x => x.LotId == item.LotId).FirstOrDefault();
                    if (data != null)
                    {
                        data.Position = 0;
                        //data.Conc = 0;
                        dbContext.Entry(data).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                    }
                }


                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save Data", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> GetQcByLotId(int? LotId)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Get By Id", "Entry");
            try
            {
                var qualtityControl = (from qc in dbContext.TblQclots
                                       where qc.LotId == LotId && qc.ExpYear >= DateTime.Now.Year && qc.SoftDelete != true
                                       select new
                                       {
                                           LotNo = qc.LotNo,
                                           TestCode = (from qclot in dbContext.TblQclots
                                                       join qcDet in dbContext.TblQclotDets
                                                      on qclot.LotId equals qcDet.LotId
                                                       join test in dbContext.TblTests
                                                       on qcDet.TestId equals test.TestId
                                                       where qc.LotNo == qclot.LotNo && qclot.SoftDelete != true
                                                       select new
                                                       {
                                                           LotId = qclot.LotId,
                                                           TestCode = test.TestCode,
                                                           qcLotDetId = dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault() != null ? dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault().QcdetId : 0,
                                                           Position = dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault() != null ? dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault().Position : 0,
                                                           Concentration = dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault() != null ? dbContext.TblQclotDets.Where(y => y.LotId == qclot.LotId).FirstOrDefault().Conc : 0,
                                                       }).ToList(),
                                       }).ToList().DistinctBy(x => x.LotNo).ToList();

                return Json(new { qualtityControl });

            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Sample Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        #endregion


        #region instrument
        [HttpGet]
        public async Task<JsonResult> GetAllSampleTray()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Sample Tray List", "Entry");
            try
            {
                var sample = await dbContext.TblSamples.Where(x => x.Position > 0 && x.SoftDelete != true).ToListAsync();

                //  var qualtityControl = await dbContext.TblQclotDets.Where(x => x.Position != 0 ).ToListAsync();
                var qualtityControl = (from qcdet in dbContext.TblQclotDets
                                       join qc in dbContext.TblQclots
                                       on qcdet.LotId equals qc.LotId
                                       where qc.SoftDelete != true
                                       select new
                                       {
                                           qc.LotId,
                                           qcdet.Position,
                                           qc.LotNo

                                       }).ToList().DistinctBy(x => x.LotNo).ToList();
                //    var calibration = dbContext.TblCalibrationdets.Where(x => x.Pos > 0 && x.SoftDelete != true).ToList().DistinctBy(x => x.Pos).ToList();

                var calibration = dbContext.TblCalibrationdets.Where(x => x.Pos > 0 && x.SoftDelete != true).ToList().DistinctBy(x => x.Lot).ToList();


                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Sample Tray List", "Exit");
                return Json(new { sample, qualtityControl, calibration });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Get Sample Tray List", "Exit", ex.Message);
                return Json(false);
            }
        }


        [HttpGet]
        public async Task<JsonResult> GetAllReagentTray()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Reagent Tray List", "Entry");
            try
            {

                //  var qualtityControl = await dbContext.TblQclotDets.Where(x => x.Position != 0 ).ToListAsync();
                var reagent = (from rgtBtl in dbContext.TblRgtbotles
                               join rgtLot in dbContext.TblRgtlots
                               on rgtBtl.RlotId equals rgtLot.RlotId
                               join tst in dbContext.TblTests
                               on rgtLot.TestId equals tst.TestId
                               where rgtBtl.SoftDelete != true && rgtLot.SoftDelete != true
                               select new
                               {
                                   rgtBtl.RbtlId,
                                   rgtBtl.RlotId,
                                   rgtLot.LotNo,
                                   rgtBtl.Position,
                                   rgtBtl.RgtType,
                                   rgtBtl.BtlSize,

                                   rgtLot.ExpMonth,
                                   rgtLot.ExpYear,
                                   tst.TestCode,
                                   rgtBtl.Volume
                               }).ToList();


                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Reagent Tray List", "Exit");
                return Json(new { reagent });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Get Sample Tray List", "Exit", ex.Message);
                return Json(false);
            }
        }


        public async Task<JsonResult> DeleteSampleTray(string Type, int? Positions)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Delete", "Entry");
            try
            {
                if (Type == "Sample")
                {
                    TblSample obj = dbContext.TblSamples.Where(x => x.Position == Positions && x.SoftDelete != true).FirstOrDefault();
                    int SampleId = obj.SmpId;
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = (sbyte?)HttpContext.Session.GetInt32("UserId");
                        obj.ModifyDate = DateTime.Now;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                    var SmpTest = dbContext.TblSampleTests.Where(x => x.SmpTstId == SampleId).ToList();
                    foreach (var test in SmpTest)
                    {
                        dbContext.TblSampleTests.Remove(test);
                        await dbContext.SaveChangesAsync();
                    }
                }
                else if (Type == "Cal")
                {
                    var caldt = dbContext.TblCalibrationdets.Where(x => x.Pos == Positions && x.SoftDelete != true).ToList();
                    foreach (var obj in caldt)
                    {

                        obj.SoftDelete = true;
                        obj.Pos = 0;
                        obj.ModifyBy = (sbyte?)HttpContext.Session.GetInt32("UserId");
                        obj.ModifyDate = DateTime.Now;
                        dbContext.Entry(obj).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                    }
                }
                else if (Type == "QC")
                {
                    var qcdt = dbContext.TblQclotDets.Where(x => x.Position == Positions).ToList();

                    foreach (var obj in qcdt)
                    {
                        int qcLotId = obj.LotId;
                        //dbContext.TblQclotDets.Remove(obj);
                        //await dbContext.SaveChangesAsync();

                        //var qc = dbContext.TblQclots.Where(x => x.LotId == qcLotId).FirstOrDefault();
                        //qc.SoftDelete = true;
                        //qc.ModifyBy = (sbyte?)HttpContext.Session.GetInt32("UserId");
                        //qc.ModifyDate = DateTime.Now;
                        //dbContext.Entry(qc).State = EntityState.Modified;
                        //await dbContext.SaveChangesAsync();

                        obj.Position = 0;
                        dbContext.Entry(obj).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();

                        
                    }
                }

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Sample Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Sample Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> DeleteReagentTray(string Type, int? Positions)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reagent Tray Delete", "Entry");
            try
            {
                if (Type == "Rgt")
                {
                    var rgtBtl = dbContext.TblRgtbotles.Where(x => x.Position == Positions).ToList();

                    foreach (var obj in rgtBtl)
                    {
                        obj.Position = 0;
                        obj.SoftDelete = true;
                        obj.ModifyBy = (sbyte?)HttpContext.Session.GetInt32("UserId");
                        obj.ModifyDate = DateTime.Now;
                        dbContext.Entry(obj).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                    }
                }

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reagent Tray Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Sample Delete", "Exit", ex.Message);
                return Json(false);
            }

        }
        #endregion

        //-------------------------- Reagent code ------------------------------

        public async Task<JsonResult> GetReagentPositions()// GetAllSampleTray()
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Sample Tray List", "Entry");
            try
            {
                var RegPosition = (from rglot in dbContext.TblRgtlots
                                   join rgbottle in dbContext.TblRgtbotles
                                   on rglot.RlotId equals rgbottle.RbtlId

                                   select new
                                   {
                                       rglot.LotNo,
                                       rgbottle.Position,
                                       rgbottle.RbtlId

                                   }).ToList().DistinctBy(x => x.LotNo).ToList();

                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Get Sample Tray List", "Exit");
                return Json(new { RegPosition });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Get Sample Tray List", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> Savereagent(IEnumerable<TblRgtbotle> rgtbotles)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reagent Save Data", "Entry");
            try
            {
                foreach (var item in rgtbotles)
                {
                    var data = dbContext.TblRgtbotles.Where(x => x.RbtlId == item.RbtlId).FirstOrDefault();
                    if (data != null)
                    {
                        data.Position = 0;
                        data.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        data.ModifyDate = DateTime.Now;
                        dbContext.Entry(data).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                    }
                }

                foreach (var item in rgtbotles)
                {
                    var dataRgtLot = dbContext.TblRgtlots.Where(y => y.RlotId == item.RlotId).FirstOrDefault();
                    if (dataRgtLot != null)
                    {
                        dataRgtLot.ExpMonth = (sbyte)item.ExpMonth;
                        dataRgtLot.ExpYear = (short)item.ExpYear;
                        dataRgtLot.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        dataRgtLot.ModifyDate = DateTime.Now;
                        await dbContext.SaveChangesAsync();

                    }
                    var data = dbContext.TblRgtbotles.Where(x => x.RbtlId == item.RbtlId).FirstOrDefault();
                    if (data == null)
                    {

                        TblRgtbotle obj = new TblRgtbotle();
                        obj.RlotId = item.RlotId;
                        obj.TestId = item.TestId;
                        obj.Position = item.Position;
                        obj.RgtType = item.RgtType;
                        obj.BtlSize = item.BtlSize;
                        obj.CreateBy = HttpContext.Session.GetInt32("UserId");
                        //obj.ExpMonth = item.ExpMonth;
                        //obj.ExpYear = item.ExpYear;
                        dbContext.TblRgtbotles.Add(obj);
                        await dbContext.SaveChangesAsync();
                    }
                    else
                    {
                        data.Position = item.Position;
                        data.RgtType = item.RgtType;
                        data.BtlSize = item.BtlSize;
                        //data.ExpMonth = item.ExpMonth;
                        //data.ExpYear = item.ExpYear;
                        data.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        data.ModifyDate = DateTime.Now;
                        dbContext.Entry(data).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();

                    }
                }
                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reagent Save Data", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Reagent Save", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> Savereagentbottle(IEnumerable<TblRgtbotle> rgtlotdetailbtl)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reagent Bottle Save Data", "Entry");
            try
            {
                foreach (var item in rgtlotdetailbtl)
                {
                    var data = dbContext.TblRgtbotles.Where(x => x.RlotId == item.RlotId).FirstOrDefault();
                    //var data = dbContext.TblRgtlots.Where(x => x.RlotId == item.RlotId).FirstOrDefault();


                    if (data != null)
                    {
                        //data.ExpMonth= item.ExpMonth;
                        //data.ExpYear = item.ExpYear;
                        //data.TestId = item.TestId;
                        data.Position = item.Position;
                        data.RgtType = item.RgtType;
                        data.BtlSize = item.BtlSize;
                        data.ModifyBy = (sbyte?)HttpContext.Session.GetInt32("UserId");
                        data.ModifyDate = DateTime.Now;

                        dbContext.Entry(data).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();

                    }

                }


                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reagent Bottle Save Data", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Reagent Bottle Save", "Exit", ex.Message);
                return Json(false);
            }
        }
        public async Task<JsonResult> DeleteReg(string LotNo)
        {
            GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save Data", "Entry");
            try
            {


                //var qc = dbContext.TblQclots.Where(x => x.LotNo == LotNo).ToList();
                var reg = dbContext.TblRgtlots.Where(x => x.LotNo == LotNo).ToList();

                foreach (var item in reg)
                {
                    //var data = dbContext.TblRgtbotles.Where(x => x.RbtlId == item.RlotId).FirstOrDefault();
                    var data = dbContext.TblRgtbotles.Where(x => x.RlotId == item.RlotId && x.SoftDelete != true).FirstOrDefault();
                    if (data != null)
                    {
                        data.Position = 0;
                        data.SoftDelete = true;
                        data.ModifyBy = HttpContext.Session.GetInt32("UserId");
                        data.ModifyDate = DateTime.Now;
                        //data.Conc = 0;
                        dbContext.Entry(data).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                    }
                }


                GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save Data", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "Quality control Save", "Exit", ex.Message);
                return Json(false);
            }
        }


        [HttpPost]
        public async Task<JsonResult> saveReagentLotNo(TblRgtlot rgtlot)
        {
            int RgtLotId = 0;
            string RgtLotNo = "";
            bool returnValue = false;
            try
            {
                if (rgtlot.RlotId == 0)
                {

                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reagent Lot Number Save Data", "Entry");
                    var data = await dbContext.TblRgtlots.Where(x => x.LotNo == rgtlot.LotNo).FirstOrDefaultAsync();
                    if (data == null)
                    {
                        rgtlot.CreateBy = (int)HttpContext.Session.GetInt32("UserId");
                        dbContext.TblRgtlots.Add(rgtlot);
                        await dbContext.SaveChangesAsync();
                        returnValue = true;
                    }
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reagent Lot Number Save Data", "Exit");
                }
                else
                {
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reagent Lot Number Update Data", "Entry");
                    rgtlot.ModifyBy = (int)HttpContext.Session.GetInt32("UserId");
                    rgtlot.ModifyDate = DateTime.Now;
                    dbContext.Entry(rgtlot).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                    returnValue = true;
                    GlobalClass.UserActivityLogFile(HttpContext.Session.GetString("UserName"), "Reagent Lot Number Update Data", "Exit");
                }
                return Json(new { returnValue, rgtlot.RlotId, rgtlot.LotNo });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(HttpContext.Session.GetString("UserName"), "patient Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }
    }
}
