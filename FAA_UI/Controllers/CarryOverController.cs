using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FAA_UI.Models;
using FAA_UI.ViewModels;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Linq;
#nullable disable 
namespace FAA_UI.Controllers
{
    public class CarryOverController:Controller
    {
        FAA_DbContext dbContext=new FAA_DbContext(); 
            
        public async Task<IActionResult> Index(int? idpk)
        {
            var viewModel=new CarryOverViewModel();
            if(idpk==null)idpk=1;
            viewModel.tblTestListModel=new TestListViewModel();
            viewModel.tblCarryOvers= dbContext.TblTestCarryOvers.DefaultIfEmpty();
            viewModel.tblCarryOver=await dbContext.TblTestCarryOvers.Where(x=>x.Idpk==idpk).FirstOrDefaultAsync();
            viewModel.selectedCarryOverID=Convert.ToInt32(idpk);
            if(viewModel.tblCarryOver!=null)
            {
            viewModel.strTestCode=GetTestCode(viewModel.tblCarryOver.TestId);
            viewModel.straffectedTestCode=GetTestCode(viewModel.tblCarryOver.AfectedTestId);
            }
            return View(viewModel);
        }
        [HttpPost]
        public async Task<IActionResult> Update(string viewData)
        {
            dynamic data=JObject.Parse(viewData);
            var tblCarryOver=new TblTestCarryOver();
            var carryid=0;
            short[] selectedIds=new short[150];
            int i=0;
            foreach(var id in data["selectedIds"])
            {
                selectedIds[i]=Convert.ToInt16(id.Value);
                i++;
            }
            if(Convert.ToString(data["Id"]).Length>0)
            {
                tblCarryOver.Idpk=data["Id"];
                carryid=tblCarryOver.Idpk;
            }
            else
                tblCarryOver.Idpk=0;
            tblCarryOver.TestId=data["Test"];
            tblCarryOver.AfectedTestId=data["AffectedTest"];
            tblCarryOver.WashVolume=data["WashVolume"]; 
            tblCarryOver.WashType=data["WashType"];   
            //tblCarryOver.LastModUid=1;
            //tblCarryOver.CUid=1;
            //tblCarryOver.DUid=1;
            
            if(tblCarryOver.Idpk==0)
            {
                await dbContext.TblTestCarryOvers.AddAsync(tblCarryOver);
                await dbContext.SaveChangesAsync();
            }
            else
            {
                dbContext.Entry(tblCarryOver).State = EntityState.Modified;              
            }
            await dbContext.SaveChangesAsync();
            //return await RedirectToAction("Index","CarryOver",new { idpk = carryid});
            carryid=dbContext.TblTestCarryOvers.Max(x=>x.Idpk);
            return RedirectToAction("Index",new { idpk=carryid});
        }
        [HttpGet]
        public int GetTestId(string testCode)
        {
            int test_Id=0;
            TblTest tblTest=dbContext.TblTests.Where(x=>x.TestCode==testCode).FirstOrDefault();
            test_Id=tblTest.TestId;
            return test_Id;
        }

        [HttpGet]
        public string GetTestCode(int testID)
        {
            string strtest=string.Empty;
            TblTest tblTest=dbContext.TblTests.Where(x=>x.TestId==testID).FirstOrDefault();
            strtest=tblTest.TestCode;
            return strtest;
        }

        [HttpGet]
         public async Task<IActionResult> Delete(int idpk)
        {
            TblTestCarryOver tblCarry=await dbContext.TblTestCarryOvers.Where(x=>x.Idpk==idpk).FirstOrDefaultAsync();
            dbContext.TblTestCarryOvers.Remove(tblCarry);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("Index",new { idpk=1});   
        }
    }
}