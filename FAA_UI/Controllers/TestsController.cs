﻿using Microsoft.AspNetCore.Mvc;

namespace FAA_UI.Controllers
{
    public class TestsController : Controller
    {
        FAA_DbContext dbContext = new FAA_DbContext();
        public IActionResult Index()
        {
            return View();
        }

        public JsonResult saveTest(TblTest tblTest)
        {
            if (tblTest.TestId == 0)
            {
                dbContext.TblTests.Add(tblTest);
            }
            else
            {
                dbContext.Entry(tblTest).State = EntityState.Modified;
            }
            dbContext.SaveChanges();
            return Json(tblTest);
        }

        public IActionResult Index1()
        {
            return View();
        }
    }
}
