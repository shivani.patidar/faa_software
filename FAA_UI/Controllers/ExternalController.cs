using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FAA_UI.Models;
using FAA_UI.ViewModels;
using System.Threading.Tasks;
using System.Linq;

namespace FAA_UI.Controllers
{
    public class ExternalController:Controller
    {
        FAA_DbContext dbContext=new FAA_DbContext(); 
            
        public IActionResult Index(int? ExternID)
        {
            var viewmodel=new ExternalViewModel();
            FillDropDowns();
            viewmodel.tblExternParaList=dbContext.TblTestExterns.DefaultIfEmpty();
            if(ExternID!=null)
            {
                viewmodel.tblExternPara=dbContext.TblTestExterns.Where(x=>x.ExternTestId==ExternID).FirstOrDefault();
            }
            else
            {
                viewmodel.tblExternPara=new TblTestExtern();
                viewmodel.tblExternPara.ExternTestId=0;
            }
            return View(viewmodel);
        }
        [HttpPost]
        public IActionResult Index(ExternalViewModel viewModel)
        {
            int? id=0;
            FillDropDowns();
            viewModel.tblExternParaList=dbContext.TblTestExterns.DefaultIfEmpty();
            if(ModelState.IsValid)
            {             
                if(dbContext.TblTestExterns.Where(x=>x.TestCode==viewModel.tblExternPara.TestCode).Count()<=0)
                {
                    viewModel.tblExternPara.ExternTestId=0;
                    dbContext.TblTestExterns.Add(viewModel.tblExternPara);
                    dbContext.SaveChanges();
                    id=dbContext.TblTestExterns.Max(x=>x.ExternTestId);
                }
                else
                {
                    dbContext.Entry(viewModel.tblExternPara).State = EntityState.Modified;
                    id=viewModel.tblExternPara.ExternTestId;
                }
                dbContext.SaveChanges();
                return RedirectToAction("Index",new { ExternID=id});
            }
            return View(viewModel);
        }

        public void FillDropDowns()
        {
            DataDictionaryController objDataDictionary=new DataDictionaryController();
            ViewBag.Decimals=objDataDictionary.GetDirList(ENM_Dict_Type.DT_Decimals);
            ViewBag.Unit=objDataDictionary.GetDirList(ENM_Dict_Type.DT_Unit);
            ViewBag.ResultType=objDataDictionary.GetDirList(ENM_Dict_Type.DT_ResultType);
        }
    }
}