using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FAA_UI.Models;
using FAA_UI.ViewModels;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace FAA_UI.Controllers
{
    public class ReagentController : Controller
    {
        FAA_DbContext dbContext=new FAA_DbContext();  
         public async Task<IActionResult> Index(int? BottleID)
        {
            var viewModel=new ReagentViewModel();
            if(BottleID==null)BottleID=1;
            viewModel.tblReagentPosList=await dbContext.TblRgtpos.ToListAsync();
            FillDropDowns();
            if(BottleID!=null)
            {
                viewModel.selectedBottleID=Convert.ToInt16(BottleID);               
                viewModel.tblReagentBottle=await dbContext.TblRgtbotles.Where(x=>x.RbtlId==BottleID).FirstOrDefaultAsync();
                viewModel.tblReagentPos=await dbContext.TblRgtpos.Where(x=>x.RbtlId==BottleID).FirstOrDefaultAsync();

                if(viewModel.tblReagentBottle==null)
                {
                    viewModel.tblReagentBottle=new TblRgtbotle();
                    viewModel.tblReagentBottle.RbtlId=0;
                    viewModel.tblReagentBottle.RlotId=0;
                    viewModel.tblReagentBottle.TestId=0;
                    viewModel.tblReagentPos=new TblRgtpo();
                    viewModel.tblReagentPos.RbtlId=0;
                    viewModel.tblReagentLot=new TblRgtlot();
                    viewModel.tblReagentLot.TestId=0;
                    viewModel.tblReagentLot.RlotId=0;
                }
                else
                {
                    viewModel.tblReagentBottle=await dbContext.TblRgtbotles.Where(x=>x.RbtlId==BottleID).FirstOrDefaultAsync();
                    viewModel.tblReagentPos=await dbContext.TblRgtpos.Where(x=>x.RbtlId==BottleID).FirstOrDefaultAsync();
                    viewModel.tblReagentLot=await dbContext.TblRgtlots.Where(x=>x.RlotId==viewModel.tblReagentBottle.RlotId).FirstOrDefaultAsync();
                }
            }            
            return View(viewModel);
        }

        public void FillDropDowns()
        {
            DataDictionaryController objDataDictionary=new DataDictionaryController();

            ViewBag.BottleSizeList=objDataDictionary.GetDirList(ENM_Dict_Type.DT_BottleSize);
            ViewBag.BottleTypeList=objDataDictionary.GetDirList(ENM_Dict_Type.DT_BottleType);

            List<SelectListItem> listSelectListItems = new List<SelectListItem>();
            //var tblTestList = dbContext.TblTests.Where(s => s.IsActive == true);
            var tblTestList = dbContext.TblTests;

            foreach (TblTest test in tblTestList)
            {
                SelectListItem selectList = new SelectListItem()
                {
                    Text = test.TestCode,
                    Value = test.TestId.ToString(),    //@MT- Select Item Value is used as IDs of the of dropdown to be taken from ItemValue 
                };
                listSelectListItems.Add(selectList);
            }

            ViewBag.TestList=listSelectListItems;

            List<SelectListItem> listSelectListLot = new List<SelectListItem>();   
           var tblRgtLotList = dbContext.TblRgtlots.Where(s => s.SoftDelete == true);
            foreach (TblRgtlot lot in tblRgtLotList)
            {
                SelectListItem selectList = new SelectListItem()
                {
                    Text = lot.LotNo,
                    Value = lot.RlotId.ToString(),    //@MT- Select Item Value is used as IDs of the of dropdown to be taken from ItemValue 
                };
                listSelectListLot.Add(selectList);
            }

            ViewBag.ReagentLotList=listSelectListLot;




        }
    }
}