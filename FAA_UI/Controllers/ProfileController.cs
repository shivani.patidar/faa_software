using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using FAA_UI.Models;
using FAA_UI.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace FAA_UI.Controllers
{
    public class ProfileController:Controller
    {
        FAA_DbContext dbContext=new FAA_DbContext(); 
            
        public async Task<IActionResult> Index(int? ProfileID)
        {
            var viewModel=new ProfileViewModel();
            if(ProfileID==null)ProfileID=1;
            viewModel.tblTestListModel=new TestListViewModel();
            viewModel.tblTestProfiles= dbContext.TblTestProfiles.DefaultIfEmpty();
            viewModel.tblProfile=await dbContext.TblTestProfiles.Where(x=>x.ProfileId==ProfileID).FirstOrDefaultAsync();
            viewModel.tblProfileDet= await dbContext.TblProfileDets.Where(x=>x.ProId==ProfileID).ToListAsync();
            foreach(var detail in viewModel.tblProfileDet)
            {
                foreach(var test in viewModel.tblTestListModel.TestList)
                {
                    if(detail.TestId==test.TestId)
                    {
                        test.isSelected=true;
                    }
                }
            }
            viewModel.selectedProfileID=Convert.ToInt32(ProfileID);
            return View(viewModel);
        }
        [HttpPost]
        public async void Update(string viewData)
        {
            dynamic data=JObject.Parse(viewData);
            var tblProfile=new TblTestProfile();
            var profid=0;
            short[] selectedIds=new short[150];
            int i=0;
            foreach(var id in data["selectedIds"])
            {
                selectedIds[i]=Convert.ToInt16(id.Value);
                i++;
            }
            if(Convert.ToString(data["ProfileId"]).Length>0)
            {
                tblProfile.ProfileId=data["ProfileId"];
                profid=tblProfile.ProfileId;
            }
            else
                tblProfile.ProfileId=0;
            tblProfile.ProfileCode=data["ProfileCode"];
            tblProfile.ProfileName=data["ProfileName"];  
            //tblProfile.LastModUid=1;
            //tblProfile.CUid=1;
            //tblProfile.DUid=1;
            
            if(tblProfile.ProfileId==0)
            {
                dbContext.TblTestProfiles.Add(tblProfile);
                dbContext.SaveChanges();
                profid= (await dbContext.TblTestProfiles.Where(x=>x.ProfileCode==tblProfile.ProfileCode).FirstOrDefaultAsync()).ProfileId;
                foreach(var id in selectedIds)
                {
                    if(id==0)break;
                    var tblDetail=new TblProfileDet();
                    tblDetail.ProId = profid;
                    tblDetail.TestId=id;
                    dbContext.TblProfileDets.Add(tblDetail);                   
                }
            }
            else
            {
                dbContext.Entry(tblProfile).State = EntityState.Modified;
                foreach(var id in selectedIds)
                {
                    if(id==0)break;
                    if(dbContext.TblProfileDets.Where(x=>x.ProId==tblProfile.ProfileId && x.TestId==id).Count()<=0)
                    {
                        var tblDetail=new TblProfileDet();
                        tblDetail.ProId = tblProfile.ProfileId;
                        tblDetail.TestId=id;
                        dbContext.TblProfileDets.Add(tblDetail);
                    }
                }
                var tbl=dbContext.TblProfileDets.Where(x=>x.ProId == tblProfile.ProfileId);
                var tblRemove=tbl.Where(x => !selectedIds.Contains(x.TestId)).ToList();
                dbContext.TblProfileDets.RemoveRange(tblRemove);
            }
            dbContext.SaveChanges();
            //return RedirectToAction("Index","TestProfile",new { ProfileID = profid});
        }

    }
}