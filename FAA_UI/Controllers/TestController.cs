using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FAA_UI.Models;
using FAA_UI.ViewModels;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace FAA_UI.Controllers
{
    public class TestController : Controller
    {
        FAA_DbContext dbContext=new FAA_DbContext();  
        // GET: Test
        public async Task<IActionResult> Index(int? TestID)
        {
            var viewModel=new TestViewModel();
            if(TestID==null)TestID=1;
            viewModel.tblTests=await dbContext.TblTests.ToListAsync();
            FillDropDowns();
            if(TestID!=null)
            {
                viewModel.selectedTestID=Convert.ToInt16(TestID);               
                viewModel.selectedTest=await dbContext.TblTests.Where(x=>x.TestId==TestID).FirstOrDefaultAsync();

                if(viewModel.selectedTest==null)
                {
                    viewModel.selectedTest=new TblTest();
                    viewModel.selectedTest.TestId=0;
                    viewModel.tblTestDetail=new TblTestDetail();
                    viewModel.tblTestDetail.TestId=0;
                    //viewModel.tblTestDetail.TestDetId=0;
                    viewModel.tblTestVolume=new TblTestVolume();
                    viewModel.tblTestVolume.TestId=0;
                    //viewModel.tblTestVolume.TestVolId=0;
                    viewModel.selectedRefRange=new TblRefRange();
                    viewModel.selectedRefRange.RefId=0;
                    viewModel.selectedRefRange.TestId=0;
                }
                else
                {
                    viewModel.tblTestDetail=await dbContext.TblTestDetails.Where(x=>x.TestId==TestID).FirstOrDefaultAsync();
                    viewModel.tblTestVolume=await dbContext.TblTestVolumes.Where(x=>x.TestId==TestID).FirstOrDefaultAsync();
                    viewModel.tblRefRange=await dbContext.TblRefRanges.Where(x=>x.TestId==TestID).ToListAsync();
                    viewModel.selectedRefRange=new TblRefRange();
                    viewModel.selectedRefRange.TestId=Convert.ToInt16(TestID);
                }
                if(viewModel.tblRefRange==null)
                {
                    viewModel.tblRefRange= dbContext.TblRefRanges.DefaultIfEmpty();
                }
            }            
            return View(viewModel);
        }
        
        [HttpPost]
        public async Task<IActionResult> Index(TestViewModel viewModel)
        {
            FillDropDowns();
            var test=viewModel.selectedTest;
            var testDetail=viewModel.tblTestDetail;          
            var testvolume=viewModel.tblTestVolume;
            var testRefRange=await dbContext.TblRefRanges.Where(x=>x.TestId==viewModel.selectedTest.TestId).ToListAsync();
            var testCalib=viewModel.tblTestCalibration;

            if(ModelState.IsValid)
            {
                if(dbContext.TblTests.AsNoTracking().Where(x=>x.TestCode==viewModel.selectedTest.TestCode).ToList().Count<=0)
                {
                    test.TestId=0;
                    //testvolume.TestVolId=0;
                    //testDetail.TestDetId=0;
                    await dbContext.TblTests.AddAsync(test);
                    await dbContext.SaveChangesAsync();
                    var testID=await dbContext.TblTests.MaxAsync(x=>x.TestId);
                    testDetail.TestId=testID;
                    await dbContext.TblTestDetails.AddAsync(testDetail);
                    testvolume.TestId=testID;
                    await dbContext.TblTestVolumes.AddAsync(testvolume);                                  
                }
                else
                {
                    dbContext.Entry(viewModel.selectedTest).State = EntityState.Modified;
                    dbContext.Entry(viewModel.tblTestDetail).State = EntityState.Modified;
                    dbContext.Entry(viewModel.tblTestVolume).State = EntityState.Modified;       
                }
                await dbContext.SaveChangesAsync();
                return RedirectToAction("Index",new { TestID = viewModel.selectedTest.TestId });
            }
            return View(viewModel);  
        }

        public void FillDropDowns()
        {
            DataDictionaryController objDataDictionary=new DataDictionaryController();

            ViewBag.CurveList=objDataDictionary.GetDirList(ENM_Dict_Type.DT_CurveType);
            ViewBag.FilterList=objDataDictionary.GetDirList(ENM_Dict_Type.DT_Filters);
            ViewBag.MethodList=objDataDictionary.GetDirList(ENM_Dict_Type.DT_Method);
            ViewBag.DirectionList=objDataDictionary.GetDirList(ENM_Dict_Type.DT_Direction);
            ViewBag.BlankTypeList=objDataDictionary.GetDirList(ENM_Dict_Type.DT_BlankType);
            ViewBag.GenderList=objDataDictionary.GetDirList(ENM_Dict_Type.DT_Gender);
            ViewBag.AgeUnitList=objDataDictionary.GetDirList(ENM_Dict_Type.DT_AgeUnit);  
            ViewBag.ReagentCountList=  objDataDictionary.GetDirList(ENM_Dict_Type.DT_Reagent);
            ViewBag.Decimals=objDataDictionary.GetDirList(ENM_Dict_Type.DT_Decimals);
            ViewBag.Unit=objDataDictionary.GetDirList(ENM_Dict_Type.DT_Unit);

        }

        [HttpPost]
        public void AddEditRefRange(TblRefRange tblmodel)
        {
            if(tblmodel.RefId==0)
            {
                dbContext.TblRefRanges.Add(tblmodel);
            }
            else
            {
                dbContext.Entry(tblmodel).State = EntityState.Modified;
            }
            dbContext.SaveChanges();
        }
      
    }
}

