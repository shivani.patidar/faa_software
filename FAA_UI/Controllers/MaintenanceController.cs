using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using FAA_UI.Models;
using FAA_UI.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using SerialCommunication;
using FAA_INSTRUMENT;
using System.Threading.Tasks;

namespace FAA_UI.Controllers
{
    public class RealTimeData
    {
        public DateTime TimeStamp { get; set; }
        public double DataValue { get; set; }
    }   
    public class MaintenanceController:Controller
    {
        FAA_DbContext dbContext=new FAA_DbContext(); 
        public static SerialPort objSPort;
        public static Initialize objPortInit;
        public static Cls_Instrument objInstrument;

        public static List<SelectListItem> listBaudItems;

        public static List<SelectListItem> listComItems;

        public static CUVET_CLEAN_OPERATION CurrentOperation;
        public static CUVET_CLEAN_OPERATION SubOperation;

        public static ENUM_BTN_SELECTION  CurrentBtnSelection;

        bool IsPause;
        int Begin_Cup;
        int End_Cup;
        int Cup_Num;
        int iCount;

        int iFluid_Pos;
        int iFluid_Vol;
        int Fluid_Cuv_Num;
            
        public async Task<IActionResult> Index()
        {
            int InstrumentId=1;
            LOAD_INST_STEPS_FROM_DB();
            FillDropDowns();
            var viewModel=new MaintenanceViewModel();
            viewModel.instrumentSetting=await dbContext.TblInstrumentSettings.Where(x=>x.InstrumentId==InstrumentId).FirstOrDefaultAsync();
            viewModel.systemSetting=await dbContext.TblSystemSettings.FirstOrDefaultAsync();
            objPortInit = new Initialize();
            objPortInit.DataReceived += ObjSPort_DataReceived;
            //objPortInit.iDataReceived += ObjSPort_DataReceived;
            objSPort = new SerialPort();
            if(viewModel.systemSetting.InstrumentPort==null)viewModel.systemSetting.InstrumentPort="COM2";
            
            Port_Configuration.strSerialPort=Convert.ToString(listComItems.ElementAt(Convert.ToInt32(viewModel.systemSetting.InstrumentPort)).Text);
            Port_Configuration.BaudRate=Convert.ToInt32(listBaudItems.ElementAt(Convert.ToInt32(viewModel.systemSetting.InstrumentBaudRate)).Text);
            objPortInit.OpenPort(ref objSPort, Port_Configuration.strSerialPort, Port_Configuration.BaudRate);
            objInstrument = new Cls_Instrument(ref objPortInit);
            return View(viewModel);
        }
        [HttpPost]
        public void SaveSystemSettings(TblSystemSetting systemSettings)
        {   
            if(systemSettings.InstrumentPort==null)systemSettings.InstrumentPort="0";
            dbContext.Entry(systemSettings).State = EntityState.Modified;
            dbContext.SaveChanges();   
        }

        public void FillDropDowns()
        {
            string[] COMList = new string[SerialPort.GetPortNames().Count()];
            int i = 0;
            foreach (string s in SerialPort.GetPortNames())
            {
                COMList[i] = s;
                i++;
            }

            listComItems = new List<SelectListItem>();
            i = 0;
            foreach (string s in SerialPort.GetPortNames())
            {
                 SelectListItem selectList = new SelectListItem()
                    {
                        Text = s,
                        Value = i.ToString(),    //@MT- Select Item Value is used as IDs of the of dropdown to be taken from ItemValue 
                    };
                    listComItems.Add(selectList);
                    i++;
            }
            if(listComItems.Count==0)
            {
                SelectListItem selectList = new SelectListItem()
                    {
                        Text = "COM1",
                        Value = "0",    //@MT- Select Item Value is used as IDs of the of dropdown to be taken from ItemValue 
                    };
                    listComItems.Add(selectList);
            }

            ViewBag.ComPortList=listComItems;
            i=0;
            listBaudItems = new List<SelectListItem>();    
            foreach (string s in Initialize.SupportedBaudRates)
            {
                 SelectListItem selectList = new SelectListItem()
                    {
                        Text = s,
                        Value = i.ToString(),    //@MT- Select Item Value is used as IDs of the of dropdown to be taken from ItemValue 
                    };
                    listBaudItems.Add(selectList);
                    i++;
            }
            ViewBag.BaudRateList=listBaudItems;   
        }
        [HttpPost]
        public void Init()
        {   
           
            INST_STATUS.CURRENT_BARCODE = string.Empty;
            objInstrument.INIT_INSTRUMENT();
            objInstrument.SendEnd();
        }
        
        [HttpPost]
        public void AllArmUp()
        {   
            objInstrument.ALL_ARM_UP();
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void WashAllCuvette()
        {
            objInstrument.iBitNum = 0;
            CurrentOperation = CUVET_CLEAN_OPERATION.NO_OPERATION;
            Begin_Cup = 1;
            Cup_Num = 1;
            CurrentOperation = CUVET_CLEAN_OPERATION.INIT;
            SubOperation = CUVET_CLEAN_OPERATION.WASH_ALL_CUVET;
            PerformAction();
        }

        private void ObjSPort_DataReceived(byte[] RecievedData)
        {
            try
            {
                objInstrument.ProcessRawData(RecievedData);

                if (INST_STATUS.IS_INST_BUSY == false)
                {
                    PerformAction();
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                //MessageBox.Show(ex.ToString());
            }
        }

         private void PerformAction()
        {
            if (IsPause == true)
                return;
            bool m_OutWater = false;
            switch (CurrentOperation)
            {
                case CUVET_CLEAN_OPERATION.INIT:                   
                    objInstrument.INIT_INSTRUMENT();
                    objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                    //objControlOrder.WashNeedle();    //pending for implementation
                    switch (SubOperation)
                    {
                        case CUVET_CLEAN_OPERATION.PROBE_WASH:

                            break;
                        case CUVET_CLEAN_OPERATION.WASH_CNT_CUVET:
                            objInstrument.RCT_ROTATE(Begin_Cup);
                            objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                            break;
                        case CUVET_CLEAN_OPERATION.WASH_ALL_CUVET:
                            objInstrument.RCT_ROTATE(Begin_Cup);
                            objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                            break;
                        case CUVET_CLEAN_OPERATION.WASH_WITH_FLUID:
                            objInstrument.RCT_ROTATE(Fluid_Cuv_Num);
                            objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                            break;
                    }
                    CurrentOperation = SubOperation;
                    break;
                case CUVET_CLEAN_OPERATION.PROBE_WASH:
                    break;
                case CUVET_CLEAN_OPERATION.WASH_CNT_CUVET:
                    objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                    objInstrument.CLEANING_PUMP(ON_OFF.ON, 0);
                    objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.DOWN);
                    objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                    iCount++;
                    if (iCount <= Cup_Num + 8)
                    {
                        objInstrument.CRU_DISPENSE_PUMP(ON_OFF.ON, 0);
                        objInstrument.Delay_Time(10);
                        objInstrument.CRU_DISPENSE_PUMP(ON_OFF.OFF, 0);
                        m_OutWater = true;
                    }
                    else objInstrument.Delay_Time(10);
                    objInstrument.Delay_Time(40);
                    objInstrument.CLEANING_PUMP(ON_OFF.OFF, 0);
                    if (iCount <= Cup_Num + 8)
                    {
                        objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.DOWN_MID1);
                        objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                        objInstrument.CRU_DISPENSE_PUMP(ON_OFF.ON, 0);
                        objInstrument.Delay_Time(CRU_STEPS.CRU_ADD_WATER_TIME);
                        objInstrument.CRU_DISPENSE_PUMP(ON_OFF.OFF, 0);
                    }
                    else
                    {
                        objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.DOWN_MID1);
                        objInstrument.Delay_Time(CRU_STEPS.CRU_ADD_WATER_TIME);
                    }
                    if (m_OutWater) objInstrument.CLEANING_PUMP(ON_OFF.ON, 0);
                    objInstrument.CheckMotor(ENUM_MOTOR200.CRU);

                    if (m_OutWater)
                    {
                        objInstrument.DI_BACK_PUMP(ON_OFF.ON, CRU_STEPS.CRU_PUMP_BACK_TIME);
                        objInstrument.Delay_Time(50);//The negative pressure of the water tank ³é¸ºÑ¹¸×ÖÐµÄË®
                        objInstrument.CLEANING_PUMP(ON_OFF.OFF, 0);
                    }
                    objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                    objInstrument.Delay_Time(20);
                    Begin_Cup++;
                    if (Begin_Cup > Instrument_Constants.CuvetCount) Begin_Cup = 1;
                    if (iCount < Cup_Num + CRU_STEPS.CRU_WASH_HANDS + 8) objInstrument.RCT_ROTATE(Begin_Cup);
                    if (iCount >= Cup_Num + CRU_STEPS.CRU_WASH_HANDS + 8)
                    {
                        /*Wp = 0;
                        nIDEventP = 6;
                        EnableDisableBtns(TRUE);
                        {
                            CRegKey Rek;
                            DWORD cbA = 0;
                            //Rek.Open(HKEY_CURRENT_USER,"MDPL\\System");
                            Rek.Open(HKEY_CURRENT_USER, AppRegSystem); //@AR 28-JUN-2016 Dynamic Registry Creation

                            Rek.SetValue(cbA, "HaveWater");
                            cbA = 40;
                            Rek.SetValue(cbA, "ReactionCup");
                            Rek.Close();
                        }*/

                        objInstrument.RCT_ROTATE(1);
                        objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                        CurrentOperation = CUVET_CLEAN_OPERATION.NO_OPERATION;
                    }
                    break;
                case CUVET_CLEAN_OPERATION.WASH_ALL_CUVET:

                    objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                    objInstrument.CLEANING_PUMP(ON_OFF.ON,0);
                    objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.DOWN);
                    objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                    if (Begin_Cup <= Instrument_Constants.CuvetCount)
                    {
                        objInstrument.CRU_DISPENSE_PUMP(ON_OFF.ON, 0);
                        objInstrument.Delay_Time(10);
                        objInstrument.CRU_DISPENSE_PUMP(ON_OFF.OFF, 0);
                        m_OutWater = true;
                    }
                    else objInstrument.Delay_Time(10);
                    objInstrument.Delay_Time(40);
                    objInstrument.CLEANING_PUMP(ON_OFF.OFF, 0);
                    if (Begin_Cup <= Instrument_Constants.CuvetCount)
                    {
                        objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.DOWN_MID1);
                        objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                        objInstrument.CRU_DISPENSE_PUMP(ON_OFF.ON, 0);
                        objInstrument.Delay_Time(CRU_STEPS.CRU_ADD_WATER_TIME);
                        objInstrument.CRU_DISPENSE_PUMP(ON_OFF.OFF, 0);
                    }
                    else
                    {
                        objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.DOWN_MID1);
                        objInstrument.Delay_Time(CRU_STEPS.CRU_ADD_WATER_TIME);
                    }
                    if (m_OutWater) objInstrument.CLEANING_PUMP(ON_OFF.ON, 0);
                    objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                    if (m_OutWater)
                    {
                        objInstrument.DI_BACK_PUMP(ON_OFF.ON, CRU_STEPS.CRU_PUMP_BACK_TIME);
                        objInstrument.Delay_Time(50);
                        objInstrument.CLEANING_PUMP(ON_OFF.OFF, 0);
                    }
                    objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                    objInstrument.Delay_Time(20);
                    Begin_Cup++;
                    Cup_Num++;
                    if (Cup_Num > Instrument_Constants.CuvetCount) Cup_Num = 1;
                    if (Begin_Cup <= Instrument_Constants.CuvetCount + CRU_STEPS.CRU_WASH_HANDS + 1) objInstrument.RCT_ROTATE(Cup_Num);
                    if (Begin_Cup > Instrument_Constants.CuvetCount + CRU_STEPS.CRU_WASH_HANDS + 1)
                    {
                        /*CRegKey Rek;
                        DWORD cbA = 0;
                        //Rek.Open(HKEY_CURRENT_USER,"MDPL\\System");
                        Rek.Open(HKEY_CURRENT_USER, AppRegSystem); //@AR 28-JUN-2016 Dynamic Registry Creation

                        Rek.SetValue(cbA, "HaveWater");
                        Rek.Close();*/
                        CurrentOperation = CUVET_CLEAN_OPERATION.NO_OPERATION;
                       objInstrument.RCT_ROTATE(1);
                       objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                    }
                    break;
                case CUVET_CLEAN_OPERATION.WASH_WITH_FLUID:
                    objInstrument.SYRINGE_VOL_OPERATION(15,ENUM_SYRINGE.ASPIRATE);
                    objInstrument.RGT_ROTATE_RG_POS_IN(iFluid_Pos);//m_Port->ROTATE_Reg_Sam(m_Position);
                    objInstrument.ARM_ROTATE(ENUM_ARM_ROT.RGT_IN);//Go to the location of the reagent area regp1 
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                    objInstrument.CheckMotor(ENUM_MOTOR200.SYRINGE);
                    objInstrument.CheckMotor(ENUM_MOTOR200.RGT);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.LLS);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.CheckMotor(ENUM_MOTOR200.SYRINGE);
                    //objInstrument.CheckReagentRemain();
                    objInstrument.SYRINGE_VOL_OPERATION(iFluid_Vol,ENUM_SYRINGE.ASPIRATE);  //Need to spend more reagents
                    objInstrument.CheckMotor(ENUM_MOTOR200.SYRINGE);
                    objInstrument.Delay_Time(10);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.CheckMotor(ENUM_MOTOR200.SYRINGE);
                    objInstrument.ARM_ROTATE(ENUM_ARM_ROT.CUVET);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                    objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.DOWN_CUV);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.CheckMotor(ENUM_MOTOR200.SYRINGE);
                    objInstrument.SYRINGE_VOL_OPERATION(iFluid_Vol, ENUM_SYRINGE.DISPENSE);
                    objInstrument.CheckMotor(ENUM_MOTOR200.SYRINGE);
                    objInstrument.Delay_Time(10);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.CheckMotor(ENUM_MOTOR200.SYRINGE);
                    Fluid_Cuv_Num++;
                    if (Fluid_Cuv_Num > Instrument_Constants.CuvetCount) Fluid_Cuv_Num = 1;
                    objInstrument.RCT_ROTATE(Instrument_Constants.CuvetCount+2);
                   /* if (m_Wash_Reagent)         //Said cleaning fluid runs out ±íÊ¾ÇåÏ´ÒºÓÃÍêÁË
                    {
                        objControlOrder.ARM_ROTATE(ENUM_ARM_ROT.HOME);
                        objControlOrder.CheckMotor(ENUM_MOTOR200.CRU);
                        objControlOrder.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                        objControlOrder.INIT_REC();
                        //objControlOrder.WashNeedle();
                        objControlOrder.SendEnd();
                        CInformationDlg Dlg;
                        Dlg.DialogTitle = CString(g_LoadString("IDS_CWashBlankCup_3"));
                        Dlg.Information = CString(g_LoadString("IDS_CWashBlankCup_4"));
                        Dlg.m_OK = TRUE;
                        if (Dlg.DoModal() != IDOK)
                        {
                            Begin_Cup = 1;
                            Cup_Num = 1;
                            CurrentOperation = CUVET_CLEAN_OPERATION.WASH_ALL_CUVET;
                        }
                        else
                        {
                            Cup_Num++;
                            m_Wash_Reagent = FALSE;
                            if (Cup_Num > 60)
                            {
                                Begin_Cup = 1;
                                Cup_Num = 1;
                               objControlOrder.ARM_ROTATE(ENUM_ARM_ROT.HOME);
                               objControlOrder.CheckMotor(ENUM_MOTOR200.CRU);
                               objControlOrder.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                               objControlOrder.INIT_REC();
                               //objControlOrder.WashNeedle();
                                Wp = 2;
                                CurrentOperation = CUVET_CLEAN_OPERATION.WASH_ALL_CUVET;
                            }
                        }
                        return;
                    }*/
                    Cup_Num++;
                    if (Cup_Num > Instrument_Constants.CuvetCount)
                    {
                        Begin_Cup = 1;
                        Cup_Num = 1;
                        objInstrument.ARM_ROTATE(ENUM_ARM_ROT.HOME);
                        objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                        objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                        objInstrument.INIT_REC();
                        //objControlOrder.WashNeedle();
                        CurrentOperation = CUVET_CLEAN_OPERATION.WASH_ALL_CUVET;
                    }
                    //objControlOrder.SendEnd();
                    break;
                case CUVET_CLEAN_OPERATION.END_OPERATION:
                    break;
                case CUVET_CLEAN_OPERATION.NO_OPERATION:
                    break;
            }
            objInstrument.SendEnd();
        }


        public void LOAD_INST_STEPS_FROM_DB()
        {
            Port_Configuration.strSerialPort = "COM2";
            Port_Configuration.BaudRate = 38400;
            ARMSTEPS.ARM_DN_CUV = 950;
            ARMSTEPS.ARM_DN_DIL_CUV = 950;
            ARMSTEPS.ARM_DN_HOME = 1000;
            ARMSTEPS.ARM_DN_ISE = 575;
            ARMSTEPS.ARM_DN_RGT = 2200;         // Dead Volume of Reagent Bottle
            ARMSTEPS.ARM_DN_SMP = 1400;         // Dead Volume of Sample Container Type
            ARMSTEPS.ARM_ROT_CUV = 38;
            ARMSTEPS.ARM_ROT_DIL_CUV = 38;
            ARMSTEPS.ARM_ROT_ISE = 1720;
            ARMSTEPS.ARM_ROT_RGT_IN = 650;
            ARMSTEPS.ARM_ROT_RGT_OUT = 650;
            ARMSTEPS.ARM_ROT_SMP = 522;
            ARMSTEPS.ARM_UP_MAX = 3500;
            ARMSTEPS.ARM_UP_OFFSET = 100;
            STR_STEPS.STR_DN_CUV = 885;
            STR_STEPS.STR_DN_HOME = 450;
            STR_STEPS.STR_UP_MAX = 1100;
            STR_STEPS.STR_ROT_CUV = 675;
            CRU_STEPS.CRU_DOWN =695;
            CRU_STEPS.CRU_DI_DISPENSE = 220;
            CRU_STEPS.CRU_UP_MAX = 2800;
            CRU_STEPS.CRU_MID2 = 12;
            CRU_STEPS.CRU_MID1 = CRU_STEPS.CRU_DOWN- CRU_STEPS.CRU_DI_DISPENSE- CRU_STEPS.CRU_MID2;            
            CRU_STEPS.CRU_ADD_WATER_TIME =27;
            CRU_STEPS.CRU_PUMP_BACK_TIME = 11;
            RGT_STEPS.RGT_RG_POS_IN_1 = 1460;
            RGT_STEPS.RGT_RG_POS_OUT_1 = 1460;
            RGT_STEPS.RGT_SMP_POS1 = 1475;
            RGT_STEPS.RGT_RG_BARCODE = 3040;
            RGT_STEPS.RGT_SMP_BARCODE = 2950;
            Instrument_Constants.ReagentCount = 40;
            Instrument_Constants.CuvetCount = 70;
            Instrument_Constants.SampleCount = 40;
            Instrument_Constants.OpticalPathLength = 0.65;
            Instrument_Constants.FilterCount = 9;
            Instrument_Constants.Filters = new int[Instrument_Constants.FilterCount];
            Instrument_Constants.Filters[0] = 340;
            Instrument_Constants.Filters[1] = 405;
            Instrument_Constants.Filters[2] = 450;
            Instrument_Constants.Filters[3] = 510;
            Instrument_Constants.Filters[4] = 546;
            Instrument_Constants.Filters[5] = 578;
            Instrument_Constants.Filters[6] = 620;
            Instrument_Constants.Filters[7] = 670;
            Instrument_Constants.Filters[8] = 700;
            Instrument_Constants.Signal_Threshold = 35000;
            SYRINGE.SYRINGE_TYPE = ENUM_SYRINGE_TYPE.KEYTO_SYRINGE;
            SYRINGE.SYRINGE_OFFSET = 20;
            SYRINGE.SYRINGE_WASHTIME = 150;
            INST_STATUS.LAMP_STATUS = ON_OFF.ON;
            INST_STATUS.IS_INST_BUSY = false;
            CRU_STEPS.CRU_WASH_HANDS = 8;
            Instrument_Constants.CRUHands = new int[CRU_STEPS.CRU_WASH_HANDS];
            Instrument_Constants.ConnectedInstrument = ENUM_INSTRUMENTS.Nano200;
            Instrument_Constants.Cuv_WL_GWC = 16;
            Instrument_Constants.Cuv_WL_WASH = 12;
            Instrument_Constants.ISE_CALIBRATOR_POS = 30;
            Instrument_Constants.ISE_CLEANER_POS = 29;
            Instrument_Constants.ISE_CONDITIONER_POS = 28;

        } 

        [HttpGet]
        public JsonResult  SaveInstrumentSettings(TblInstrumentSetting  instrumentSetting)
        {
            dbContext.Entry(instrumentSetting).State = EntityState.Modified;
            dbContext.SaveChanges();
            return Json(instrumentSetting);
        }

        [HttpGet]
        public JsonResult CheckSetting(string selectedID,string Steps)
        {     
            if (selectedID ==  "RgtPosIn")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.RGT_REAGENTPOS_IN;
                RGT_STEPS.RGT_RG_POS_IN_1= Convert.ToInt32(Steps);
            }
            else if(selectedID=="RgtPosOut")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.RGT_REAGENTPOS_OUT;
                RGT_STEPS.RGT_RG_POS_OUT_1=Convert.ToInt32(Steps);
            }
            else if(selectedID=="RgtPosSmp")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.RGT_SAMPLEPOS;
                RGT_STEPS.RGT_SMP_POS1= Convert.ToInt32(Steps);
            }
            else if (selectedID == "ArmPosHome")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_HOMEPOS;
            }
            else if (selectedID == "ArmDepthHome")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_HOMEDEEP;
                ARMSTEPS.ARM_DN_HOME=Convert.ToInt32(Steps);
            }
            else if (selectedID == "ArmPosCuv")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_CUVETPOS;
                ARMSTEPS.ARM_ROT_CUV=Convert.ToInt32(Steps);
            }
            else if (selectedID == "ArmDepthCuv")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_CUVETDEEP;
                ARMSTEPS.ARM_DN_CUV=Convert.ToInt32(Steps);
            }
            else if (selectedID == "ArmPosRgtIn")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_REG1POS_IN;
                ARMSTEPS.ARM_ROT_RGT_IN=Convert.ToInt32(Steps);
            }
            else if (selectedID == "ArmPosRgtOut")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_REG1POS_OUT;
                ARMSTEPS.ARM_ROT_RGT_OUT=Convert.ToInt32(Steps);
            }
            else if (selectedID == "ArmPosRgtSmp")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_SMP1POS;
                ARMSTEPS.ARM_ROT_SMP=Convert.ToInt32(Steps);
            }
            //else if (selectedID == "ArmDvReg")
            //    CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_REGDEADVOL;
            //else if (selectedID =="ArmDvSmp")
            //    CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_SMPDEADVOL;
            else if (selectedID == "ArmDepthRgtIn")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_REGDEEP;
                ARMSTEPS.ARM_DN_RGT=Convert.ToInt32(Steps);
            }
            /*else if (selectedID == "ArmDepthSmp")
                CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_SMPDEEP;*/
           /* else if (selectedID == "ArmPosIse")
                CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_ISEPOS;
            else if (selectedID == "ArmDepthIse")
                CurrentBtnSelection = ENUM_BTN_SELECTION.ARM_ISEDEEP;*/
            else if (selectedID == "StrPosHome")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.STR_HOMEPOS;
            }
            else if (selectedID == "StrDepthHome")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.STR_HOMEDEEP;
                STR_STEPS.STR_DN_HOME=Convert.ToInt32(Steps);
            }
            else if (selectedID == "StrPosCuv")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.STR_CUVETPOS;
                STR_STEPS.STR_ROT_CUV=Convert.ToInt32(Steps);
            }
            else if (selectedID == "StrDepthCuv")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.STR_CUVETDEEP;
                STR_STEPS.STR_DN_CUV=Convert.ToInt32(Steps);
            }


            else if (selectedID == "CruDepthWash")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.CRU_DEEP;
                CRU_STEPS.CRU_DOWN=Convert.ToInt32(Steps);
            }
            else if (selectedID == "CruDepthWater")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.CRU_DIDISPENSE;
                CRU_STEPS.CRU_DI_DISPENSE=Convert.ToInt32(Steps);
            }
            else if (selectedID == "RgtScanIn")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.BAR_REGPOS1_IN;
                RGT_STEPS.RGT_RG_BARCODE=Convert.ToInt32(Steps);
            }
            else if (selectedID == "RgtScanOut")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.BAR_REGPOS1_OUT;
                RGT_STEPS.RGT_RG_BARCODE=Convert.ToInt32(Steps);
            }
            else if (selectedID == "RgtScanSmp")
            {
                CurrentBtnSelection = ENUM_BTN_SELECTION.BAR_SMPPOS1;
                RGT_STEPS.RGT_SMP_BARCODE=Convert.ToInt32(Steps);
            }

            objInstrument.SetSpeed(ENUM_MOTOR200.ARM_ROTATE, 27, 0);
            objInstrument.SetSpeed(ENUM_MOTOR200.CRU, 9, 0);
            objInstrument.SetSpeed(ENUM_MOTOR200.STR_ROTATE, 26, 0);//m_Port->SetSpeed(m_Port->m_Instrument[12],9,0);
            objInstrument.SetSpeed(ENUM_MOTOR200.STR_UPDOWN, 20, 0);
            objInstrument.SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 22, 0);//Cp arm down speed made as per run
            
            switch (CurrentBtnSelection)
            {
                case ENUM_BTN_SELECTION.RGT_REAGENTPOS_IN:
                    objInstrument.RGT_INIT();
                    objInstrument.CheckMotor(ENUM_MOTOR200.RGT);
                    objInstrument.Delay_Time(20);
                    objInstrument.RGT_ROTATE_STEPS(RGT_STEPS.RGT_RG_POS_IN_1);
                    break;
                case ENUM_BTN_SELECTION.RGT_REAGENTPOS_OUT:
                    objInstrument.RGT_INIT();
                    objInstrument.CheckMotor(ENUM_MOTOR200.RGT);
                    objInstrument.Delay_Time(20);
                    objInstrument.RGT_ROTATE_STEPS(RGT_STEPS.RGT_RG_POS_OUT_1);
                    break;
                case ENUM_BTN_SELECTION.RGT_SAMPLEPOS: 
                    objInstrument.RGT_INIT();
                    objInstrument.CheckMotor(ENUM_MOTOR200.RGT);
                   // objInstrument.MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_ANTICLOCKWISE, RGT_STEPS.RGT_SMP_POS1);
                    objInstrument.RGT_ROTATE_SMP_POS(1);
                    break;
                case ENUM_BTN_SELECTION.ARM_HOMEPOS:
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.ARM_INIT();
                    break;
                case ENUM_BTN_SELECTION.ARM_HOMEDEEP:
                    if (objInstrument.ARM_HORZONTAL_POS == ENUM_ARM_ROT.HOME)
                    {
                        objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
                        objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                        objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.DOWN);
                        objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    }
                    break;
                case ENUM_BTN_SELECTION.ARM_CUVETPOS:
                    if(objInstrument.STR_HORIZONTAL_POS==ENUM_STR_ROT.CUVET)
                    {
                        objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.UP_MAX);
                        objInstrument.CheckMotor(ENUM_MOTOR200.STR_ROTATE);
                        objInstrument.Delay_Time(20);
                        objInstrument.STR_ROTATE(ENUM_STR_ROT.HOME);
                        objInstrument.CheckMotor(ENUM_MOTOR200.STR_ROTATE);
                    }
                    if(objInstrument.ARM_HORZONTAL_POS==ENUM_ARM_ROT.CUVET)
                    {
                        objInstrument.ARM_INIT();
                        objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                    }
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.ARM_ROTATE(ENUM_ARM_ROT.CUVET);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                    break;
                case ENUM_BTN_SELECTION.ARM_CUVETDEEP:
                    if (objInstrument.ARM_HORZONTAL_POS == ENUM_ARM_ROT.CUVET)
                    {
                        objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
                        objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                        objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.DOWN);
                        objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    }
                    break;
                case ENUM_BTN_SELECTION.ARM_REG1POS_IN:
                    if (objInstrument.ARM_HORZONTAL_POS == ENUM_ARM_ROT.RGT_IN)
                    {
                        objInstrument.ARM_INIT();
                        objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                    }
                    objInstrument.RGT_INIT();
                    objInstrument.CheckMotor(ENUM_MOTOR200.RGT);
                    objInstrument.RGT_ROTATE_STEPS(RGT_STEPS.RGT_RG_POS_IN_1);
                    //objInstrument.RGT_ROTATE_RG_POS(1);
                    objInstrument.CheckMotor(ENUM_MOTOR200.RGT);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.ARM_ROTATE(ENUM_ARM_ROT.RGT_IN);
                    break;
                case ENUM_BTN_SELECTION.ARM_REG1POS_OUT:
                    if (objInstrument.ARM_HORZONTAL_POS == ENUM_ARM_ROT.RGT_OUT)
                    {
                        objInstrument.ARM_INIT();
                        objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                    }
                    objInstrument.RGT_INIT();
                    objInstrument.CheckMotor(ENUM_MOTOR200.RGT);
                    objInstrument.RGT_ROTATE_STEPS(RGT_STEPS.RGT_RG_POS_OUT_1);
                    //objInstrument.RGT_ROTATE_RG_POS(1);
                    objInstrument.CheckMotor(ENUM_MOTOR200.RGT);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.ARM_ROTATE(ENUM_ARM_ROT.RGT_OUT);
                    break;    
                case ENUM_BTN_SELECTION.ARM_SMP1POS:
                    if (objInstrument.ARM_HORZONTAL_POS == ENUM_ARM_ROT.SMP)
                    {
                        objInstrument.ARM_INIT();
                        objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                    }
                    objInstrument.RGT_INIT();
                    objInstrument.CheckMotor(ENUM_MOTOR200.RGT);
                    objInstrument.RGT_ROTATE_SMP_POS(1);
                    objInstrument.CheckMotor(ENUM_MOTOR200.RGT);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.ARM_ROTATE(ENUM_ARM_ROT.SMP);
                    break;
                case ENUM_BTN_SELECTION.ARM_REGDEADVOL:
                    if (objInstrument.ARM_HORZONTAL_POS == ENUM_ARM_ROT.RGT_IN || objInstrument.ARM_HORZONTAL_POS == ENUM_ARM_ROT.RGT_OUT)
                    {

                    }
                    break;
                case ENUM_BTN_SELECTION.ARM_SMPDEADVOL:
                    if (objInstrument.ARM_HORZONTAL_POS == ENUM_ARM_ROT.SMP)
                    {

                    }
                    break;
                case ENUM_BTN_SELECTION.ARM_REGDEEP:
                    if (objInstrument.ARM_HORZONTAL_POS == ENUM_ARM_ROT.RGT_IN ||objInstrument.ARM_HORZONTAL_POS == ENUM_ARM_ROT.RGT_OUT )
                    {
                        objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.DOWN);
                    }
                    break;
                case ENUM_BTN_SELECTION.ARM_SMPDEEP:
                    if (objInstrument.ARM_HORZONTAL_POS == ENUM_ARM_ROT.SMP)
                    {
                        objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.DOWN);
                    }
                    break;
                case ENUM_BTN_SELECTION.ARM_ISEPOS:
                        objInstrument.ARM_INIT();
                        objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                        objInstrument.ARM_ROTATE(ENUM_ARM_ROT.ISE);
                    break;
                case ENUM_BTN_SELECTION.ARM_ISEDEEP:
                    if (objInstrument.ARM_HORZONTAL_POS == ENUM_ARM_ROT.ISE)
                    {
                        objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.DOWN);
                    }
                    break;
                case ENUM_BTN_SELECTION.STR_HOMEPOS:
                    objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.STR_UPDOWN);
                    objInstrument.STR_ROTATE(ENUM_STR_ROT.HOME);
                    break;
                case ENUM_BTN_SELECTION.STR_HOMEDEEP:
                    if(objInstrument.STR_HORIZONTAL_POS==ENUM_STR_ROT.HOME)
                    {
                        objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.UP_MAX);
                        objInstrument.CheckMotor(ENUM_MOTOR200.STR_UPDOWN);
                        objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.DOWN);
                    }
                    break;
                case ENUM_BTN_SELECTION.STR_CUVETPOS:
                    objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.STR_UPDOWN);
                    objInstrument.STR_ROTATE(ENUM_STR_ROT.CUVET);
                    break;
                case ENUM_BTN_SELECTION.STR_CUVETDEEP:
                    if (objInstrument.STR_HORIZONTAL_POS == ENUM_STR_ROT.CUVET)
                    {
                        objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.UP_MAX);
                        objInstrument.CheckMotor(ENUM_MOTOR200.STR_UPDOWN);
                        objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.DOWN);
                    }
                    break;
                case ENUM_BTN_SELECTION.CRU_DEEP:
                    objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                    objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.DOWN);
                    objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                    break;
                case ENUM_BTN_SELECTION.CRU_DIDISPENSE:
                    objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                    objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.DOWN_DI_DISPENSE);
                    objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                    break;
                case ENUM_BTN_SELECTION.BAR_REGPOS1_IN:
                    INST_STATUS.CURRENT_BARCODE = string.Empty;
                    objInstrument.RGT_INIT_FOR_BARCODE(true);
                    break;
                case ENUM_BTN_SELECTION.BAR_SMPPOS1:
                    INST_STATUS.CURRENT_BARCODE = string.Empty;
                    objInstrument.RGT_INIT_FOR_BARCODE(false);
                    break;
                case ENUM_BTN_SELECTION.SYRINGE_OFFSET:
                    objInstrument.SYRINGE_OFFSET_CAL();
                    break;
            }
            objInstrument.SendEnd();    
            return Json(selectedID);
        }

        [HttpPost]
        public void ProbeValveCheck()
        {   
            objInstrument.INTERNAL_PROBE_WASH(ON_OFF.ON,1000);
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void TroughValveCheck()
        {   
            objInstrument.TROUGH_VALVE(ON_OFF.ON,1000);
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void CruValveCheck()
        {   
            objInstrument.CRU_DISPENSE_PUMP(ON_OFF.ON,1000);
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void PumpCheck()
        {   
            objInstrument.CLEANING_PUMP(ON_OFF.ON,1000);
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void PumpbackCheck()
        {   
            objInstrument.DI_BACK_PUMP(ON_OFF.ON,1000);
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void StirrerCheck()
        {   
            objInstrument.STIRRER_REVOLVE(ON_OFF.ON,1000,1500);
            objInstrument.SendEnd();
        }

        [HttpPost]
        public string SyringeOffsetCheck()
        {   
            string strSyringeOffset=string.Empty;
            objInstrument.SYRINGE_OFFSET_CAL();
            objInstrument.SendEnd();
            while(true)
            {
                if(INST_STATUS.IS_INST_BUSY==false)
                {
                    strSyringeOffset=SYRINGE.SYRINGE_OFFSET.ToString();
                    break;
                }
            }
            return strSyringeOffset;
        }

        [HttpPost]
        public void ArmMoveHorizontal(string From, string To)
        {   
            objInstrument.SYRINGE_OFFSET_CAL();
            objInstrument.SendEnd();
        }

         [HttpPost]
        public void ArmUp()
        {   
            objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void ArmDown()
        {   
            objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.DOWN);
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void ArmInit()
        {   
            objInstrument.ARM_INIT();
            objInstrument.SendEnd();
        }

         [HttpPost]
        public void StrMoveHorizontal(string From, string To)
        {   
            objInstrument.SYRINGE_OFFSET_CAL();
            objInstrument.SendEnd();
        }

         [HttpPost]
        public void StrUp()
        {   
            objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.UP_MAX);
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void StrDown()
        {   
            objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.DOWN);
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void StrInit()
        {   
            objInstrument.STR_INIT();
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void WasteSensorCheck()
        {   
            objInstrument.SENSOR_CHECK();
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void DiSensorCheck()
        {   
            objInstrument.SENSOR_CHECK();
            objInstrument.SendEnd();
            while(true)
            {
                if(INST_STATUS.IS_INST_BUSY==false)
                {
                    bool instSensor=INST_STATUS.IS_DI_EMPTY;
                    break;
                }
            }
        }

        [HttpPost]
        public void DetergentCheck()
        {   
            objInstrument.SENSOR_CHECK();
            objInstrument.SendEnd();
            while(true)
            {
                if(INST_STATUS.IS_INST_BUSY==false)
                {
                    bool instSensor=INST_STATUS.IS_DI_EMPTY;
                    break;
                }
            }
        }

        [HttpPost]
        public string BarcodeCheck()
        {   
            string strBarcode=string.Empty;
            INST_STATUS.CURRENT_BARCODE=string.Empty;
            objInstrument.READ_BARCODE();
            objInstrument.SendEnd();
            while(true)
            {
                if(INST_STATUS.IS_INST_BUSY==false)
                {
                    strBarcode=INST_STATUS.CURRENT_BARCODE.ToString();
                    break;
                }
            }
            return strBarcode;
        }

          [HttpPost]
        public void LampOn()
        {   
            //objInstrument.LAMP_ON_OFF(ON_OFF.ON);
            objInstrument.SendEnd();
        }

        [HttpPost]
        public void LampOff()
        {   
            objInstrument.LAMP_ON_OFF(ON_OFF.OFF);
            objInstrument.SendEnd();
        }

        [HttpPost]
        public JsonResult StartMonitoring()
        {
            float[] AdcCount=new float[10];
            objInstrument.Is_ADValue_Detection=true;
            objInstrument.ReadData();
            objInstrument.SendEnd();
            while(true)
            {
                if(INST_STATUS.IS_INST_BUSY==false)
                {
                    break;
                }
            }
            for(int i =0;i<10;i++)
            {
                AdcCount[i]=INST_STATUS.ADCData[0,i];    
            }

            return Json(AdcCount);
        }

    }

        
}

