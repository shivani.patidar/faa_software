using System;
using Microsoft.AspNetCore.Mvc;
using FAA_UI.Models;
using FAA_UI.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

#nullable disable

namespace FAA_UI.Controllers
{
     public class UserController: Controller
    {
        FAA_DbContext dbContext=new FAA_DbContext();                                 //@MT- Intialize DB context for entity framework.
        public static string originalPassword;
        public async Task<IActionResult> Index(int? UserId)
        {
            var viewModel=new UserViewModel();
            viewModel.tblUsers= await dbContext.TblUsers.ToListAsync();
            if(UserId==null)UserId=1;                                                    //@MT- On Page Load by default select First User.
            if(UserId!=null)
            {               
                viewModel.tblUser= await dbContext.TblUsers.Where(x=>x.UidPk==UserId).FirstOrDefaultAsync();
                if(viewModel.tblUser==null) viewModel.tblUser=new TblUser();            //@MT -If no data exists in Table to avoid null ID_PK
                viewModel.tblPerson=await dbContext.TblPeople.Where(x=>x.PersonId==viewModel.tblUser.PersonId).FirstOrDefaultAsync();
                if(viewModel.tblPerson==null) 
                {
                    viewModel.tblPerson=new TblPerson();      //@MT If no data exists in Table to avoid null ID_PK
                    viewModel.tblPerson.PDisplayName="1";
                    viewModel.tblPerson.PLastName="1";
                    viewModel.tblPerson.PMidleName="1";
                    viewModel.tblPerson.PersonId=viewModel.tblUser.PersonId;
                    viewModel.tblPerson.PTitle="1";
                    viewModel.tblPerson.PHeightInCm=150;
                    viewModel.tblPerson.PWeightInKg=60;
                    viewModel.tblPerson.LastModUid=1;
                    viewModel.tblPerson.CUid=1;
                    viewModel.tblPerson.DUid=1;
                    
                }
                UserController.originalPassword=viewModel.tblUser.UPwd;
            }
            FillDropElements();
            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Index(UserViewModel viewModel)
        {
            int selectedUID=0;
            FillDropElements();         //@MT - Required to fill Combobox
            if(viewModel.tblUser.UPwd==null)            //@MT- If no new password entered then null value is returned from View
                viewModel.tblUser.UPwd= UserController.originalPassword;
            if(ModelState.IsValid)
            {
                var user=viewModel.tblUser;
                var person=viewModel.tblPerson;
                //@MT - If LoginID does not exists then add new User
                if(dbContext.TblUsers.AsNoTracking().Where(x=>x.ULoginId==user.ULoginId).ToList().Count<=0)           
                {
                    person.PersonId=0;                  //@MT- avoiding same ID_PK entry while inserting new user
                    user.UidPk=0;
                    await dbContext.TblPeople.AddAsync(person);
                    await dbContext.SaveChangesAsync();
                    user.PersonId=await dbContext.TblPeople.MaxAsync(x=>x.PersonId);                  
                    await dbContext.TblUsers.AddAsync(user);
                    selectedUID= await dbContext.TblUsers.MaxAsync(x=>x.UidPk);
                                 
                }
                else if(dbContext.TblPeople.AsNoTracking().Where(x=>x.PersonId==user.PersonId).ToList().Count<=0)
                {
                    person.PersonId=0;                  //@MT- avoiding same ID_PK entry while inserting new user
                    await dbContext.TblPeople.AddAsync(person);
                    await dbContext.SaveChangesAsync();
                    user.PersonId=await dbContext.TblPeople.MaxAsync(x=>x.PersonId);  
                    selectedUID= viewModel.tblUser.UidPk;                
                }
                else                                    //@MT- Else update exiting User details
                {  
                    dbContext.Entry(viewModel.tblUser).State = EntityState.Modified;
                    dbContext.Entry(viewModel.tblPerson).State = EntityState.Modified;  
                    selectedUID=viewModel.tblUser.UidPk;                 
                }
                await dbContext.SaveChangesAsync();
                viewModel.tblUsers= await dbContext.TblUsers.AsNoTracking().ToListAsync();
                return RedirectToAction("Index", new { UserId = selectedUID });
            }
            viewModel.tblUsers= await dbContext.TblUsers.AsNoTracking().ToListAsync();
            return View(viewModel);    
        }

        public void FillDropElements()
        {
            DataDictionaryController objDataDictionary=new DataDictionaryController();
            ViewBag.UserType=objDataDictionary.GetDirList(ENM_Dict_Type.DT_UserType);
            ViewBag.TitleList=objDataDictionary.GetDirList(ENM_Dict_Type.DT_Titles);
        }
    }
}