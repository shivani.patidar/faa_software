using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using FAA_UI.Models;
using System.Linq;
using System.Collections.Generic;
using FAA_INSTRUMENT;
#nullable disable

namespace FAA_UI
{
    public enum ENM_Dict_Type
    {
        DT_UserType = 1,
        DT_CurveType = 2,
        DT_Filters = 3,
        DT_Method = 4,
        DT_Direction = 5,
        DT_BlankType = 6,
        DT_ReflexType = 7,
        DT_Gender = 8,
        DT_AgeUnit = 9,
        DT_Titles = 10,
        DT_ResultType=11,
        DT_Unit=12,
        DT_BottleSize=13,
        DT_BottleType=14,
        DT_SampleType=15,
        DT_ContainerType=16,
        DT_Decimals=101,		//IDs after 100 used for list items that are not in database
        DT_Reagent=102,
        DT_SamplePos=103,
        DT_ReagentPosIn=104,
        DT_ReagentPosOut=105,
        
    }
     public enum ENM_Active_Type
    {
        DT_Active = 1,
        DT_InActive=0
        
    }

}
namespace FAA_UI.Controllers
{
    public class DataDictionaryController : Controller
    {
        FAA_DbContext dbContext = new FAA_DbContext();    //@MT Intialize DB context for entity framework.
        public IEnumerable<SelectListItem> DictList { get; set; }
        public List<SelectListItem> GetDirList(ENM_Dict_Type DictType)
        {

            List<SelectListItem> listSelectListItems = new List<SelectListItem>();          
            if(System.Convert.ToInt16(DictType)<100)
            {
                var Dit_Pk = dbContext.TblDirectoryDets.Where(s => s.DictId == System.Convert.ToInt16(DictType));
                foreach (TblDirectoryDet directory in Dit_Pk)
                {
                    SelectListItem selectList = new SelectListItem()
                    {
                        Text = directory.DictItem,
                        Value = directory.ItemValue.ToString(),    //@MT- Select Item Value is used as IDs of the of dropdown to be taken from ItemValue 
                    };
                    listSelectListItems.Add(selectList);
                }
            }
            else
            {
                switch(DictType)				//@MT- added decimal and reagent count to select list , these are not required in database
                {
                    case ENM_Dict_Type.DT_Decimals:  
                        listSelectListItems.Add(new SelectListItem(){Value=System.Convert.ToString(1),Text="1"});
                        listSelectListItems.Add(new SelectListItem(){Value=System.Convert.ToString(2),Text="2"}); 
                        listSelectListItems.Add(new SelectListItem(){Value=System.Convert.ToString(3),Text="3"});
                        listSelectListItems.Add(new SelectListItem(){Value=System.Convert.ToString(4),Text="4"});
                    break;
                    case ENM_Dict_Type.DT_Reagent:
                        listSelectListItems.Add(new SelectListItem(){Value=System.Convert.ToString(1),Text="1"});
                        listSelectListItems.Add(new SelectListItem(){Value=System.Convert.ToString(2),Text="2"}); 
                        listSelectListItems.Add(new SelectListItem(){Value=System.Convert.ToString(3),Text="3"});
                    break;
                    case ENM_Dict_Type.DT_SamplePos:
                        for(int i=1;i<=Instrument_Constants.SampleCount;i++)
                        {
                            listSelectListItems.Add(new SelectListItem(){Value=System.Convert.ToString(i),Text=i.ToString()});
                        }
                    break;
                    case ENM_Dict_Type.DT_ReagentPosIn:
                        for(int i=1;i<=Instrument_Constants.ReagentCount;i++)
                        {
                            listSelectListItems.Add(new SelectListItem(){Value=System.Convert.ToString(i),Text=i.ToString()});
                        }
                    break;
                    case ENM_Dict_Type.DT_ReagentPosOut:
                        for(int i=1;i<=Instrument_Constants.ReagentCount;i++)
                        {
                            listSelectListItems.Add(new SelectListItem(){Value=System.Convert.ToString(i),Text=i.ToString()});
                        }
                    break;
                }
            }
            return listSelectListItems;
        }    
    }
}               
  
