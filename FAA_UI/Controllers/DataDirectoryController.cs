﻿using FAA_UI.Models;
using FAA_UI.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.BouncyCastle.Utilities;
using System.Data;
using System.Reflection.Metadata;
using System.Text;

namespace FAA_UI.Controllers
{
    public class DataDirectoryController : Controller
    {
        DbFaaSoftwareContext dbContext = new DbFaaSoftwareContext();


        public static int? UserId = 0;
        public static string UserName = "";

        public static byte[] LOperatorImage = null;
        public IActionResult Index()
        {
            if (HttpContext.Session.GetInt32("UserId") > 0)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Admin");
            }
          
        }

        //Start Data Directory
     
        #region Sample Type
        [HttpGet]
        public async Task<JsonResult> GetAllSampleType()
        {
            UserName = HttpContext.Session.GetString("UserName");
            try
            {
                GlobalClass.UserActivityLogFile(UserName, "SampleType", "Entry");
                var Response = await dbContext.TblSampletypes.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "SampleType", "Exist");

                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "SampleType", "Exist", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveSampleType(TblSampletype tblSample)
        {
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");
            var returnValue = false;
            //var data = await dbContext.TblSampletypes.Where(x => x.SmpId == tblSample.SmpId).FirstOrDefaultAsync();

            GlobalClass.UserActivityLogFile(UserName, "SampleType Save or Update", "Entry");

            try
            {

                if (tblSample.SmpId == 0)
                {
                    TblSampletype data = dbContext.TblSampletypes.Where(x => x.SmpType == tblSample.SmpType && x.SoftDelete != true).FirstOrDefault();
                    if (data == null)
                    {
                        tblSample.CreateBy = UserId;
                        dbContext.TblSampletypes.Add(tblSample);
                        await dbContext.SaveChangesAsync();
                        returnValue = true;
                    }
                }
                else
                {
                    TblSampletype data = dbContext.TblSampletypes.Where(x => x.SmpType == tblSample.SmpType && x.SmpId != tblSample.SmpId && x.SoftDelete != true).FirstOrDefault();
                    if (data == null)
                    {
                        var obj = dbContext.TblSampletypes.Where(x => x.SmpId == tblSample.SmpId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.SmpType = tblSample.SmpType; // (String.Compare(drpLogoAlign.SelectedValue, String.Empty) == 0 ? "NULL" : "'" + drpLogoAlign.SelectedValue + "'");
                            obj.ModifyBy = UserId;
                            obj.ModifyDate = DateTime.Now;
                        }
                        await dbContext.SaveChangesAsync();
                        returnValue = true;

                    }
                }
                GlobalClass.UserActivityLogFile(UserName, "SampleType Save or Update", "Exit");
                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "SampleType Save or Update", "Exit", ex.Message);

                return Json(false);
            }

        }
        public async Task<JsonResult> GetByIdSampleType(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "SampleType Get By Id", "Entry");
            var obj = await dbContext.TblSampletypes.Where(x => x.SmpId == Id).ToListAsync();
            GlobalClass.UserActivityLogFile(UserName, "SampleType Get By Id", "Exit");

            return Json(obj);
        }

        public async Task<JsonResult> DeleteSampleType(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Sample Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblSampletype obj = dbContext.TblSampletypes.Where(x => x.SmpId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                }
                else
                {
                }
                await dbContext.SaveChangesAsync();
                GlobalClass.UserActivityLogFile(UserName, "Sample Delete", "Exit");

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Sample Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #region Unit
        [HttpGet]
        public async Task<JsonResult> GetAllUnit()
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Unit Get List", "Entry");
            try
            {
                var Response = await dbContext.TblUnits.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Unit Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Unit Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveUnit(TblUnit unit)
        {
            bool returnValue = false;
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");

            try
            {
                if (unit.UntId == 0)
                {
                    TblUnit data = dbContext.TblUnits.Where(x => x.UntName == unit.UntName && x.SoftDelete != true).FirstOrDefault();
                    if (data == null)
                    {
                        GlobalClass.UserActivityLogFile(UserName, "Unit Save Data", "Entry");
                        unit.CreateBy = UserId;
                        dbContext.TblUnits.Add(unit);
                        await dbContext.SaveChangesAsync();
                        returnValue = true;
                        GlobalClass.UserActivityLogFile(UserName, "Unit Save Data", "Exit");
                    }
                }
                else
                {
                    TblUnit data = await dbContext.TblUnits.Where(x => x.UntName == unit.UntName && x.UntId != unit.UntId && x.SoftDelete != true).FirstOrDefaultAsync();
                    if (data == null)
                    {
                        GlobalClass.UserActivityLogFile(UserName, "Unit Update Data", "Entry");
                        var obj = dbContext.TblUnits.Where(x => x.UntId == unit.UntId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.UntName = unit.UntName;
                            obj.ModifyBy = UserId;
                            obj.ModifyDate = DateTime.Now;
                            await dbContext.SaveChangesAsync();
                            returnValue = true;
                        }
                        GlobalClass.UserActivityLogFile(UserName, "Unit Update Data", "Exit");
                    }
                }

                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Unit Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> GetByIdUnit(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Unit Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblUnits.Where(x => x.UntId == Id).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Unit Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Unit Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteUnit(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Unit Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblUnit obj = dbContext.TblUnits.Where(x => x.UntId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                }

                GlobalClass.UserActivityLogFile(UserName, "Unit Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Unit Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #region Qualitative
       
        [HttpGet]
        public async Task<JsonResult> GetAllQualitative()
        {
            UserName = HttpContext.Session.GetString("UserName");

            GlobalClass.UserActivityLogFile(UserName, "Qualitative Get List", "Entry");
            try
            {
                var Response = await dbContext.TblDirectoryQualitatives.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Qualitative Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Qualitative Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveQualitative(TblDirectoryQualitative directoryQualitative)
        {
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");

            try
            {
                if (directoryQualitative.QualtId == 0)
                {

                    GlobalClass.UserActivityLogFile(UserName, "Qualitative Save Data", "Entry");
                    TblDirectoryQualitative data = dbContext.TblDirectoryQualitatives.Where(x => x.QualtName == directoryQualitative.QualtName && x.SoftDelete != true).FirstOrDefault();
                    if (data == null)
                    {
                        directoryQualitative.CreateBy = UserId;
                        dbContext.TblDirectoryQualitatives.Add(directoryQualitative);
                        await dbContext.SaveChangesAsync();
                        GlobalClass.UserActivityLogFile(UserName, "Qualitative Save Data", "Exit");
                    }
                }
                else
                {
                    GlobalClass.UserActivityLogFile(UserName, "Qualitative Update Data", "Entry");
                    TblDirectoryQualitative data = dbContext.TblDirectoryQualitatives.Where(x => x.QualtName == directoryQualitative.QualtName && x.QualtId != directoryQualitative.QualtId && x.SoftDelete != true).FirstOrDefault();
                  if(data == null)
                    {
                        directoryQualitative.ModifyBy = UserId;
                        directoryQualitative.ModifyDate = DateTime.Now;
                        dbContext.Entry(directoryQualitative).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                    }
                    GlobalClass.UserActivityLogFile(UserName, "Qualitative Update Data", "Exit");
                }

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Qualitative Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> GetByIdQualitative(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Qualitative Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblDirectoryQualitatives.Where(x => x.QualtId == Id).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Qualitative Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Qualitative Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteQualitative(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Qualitative Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblDirectoryQualitative obj = dbContext.TblDirectoryQualitatives.Where(x => x.QualtId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                }

                GlobalClass.UserActivityLogFile(UserName, "Qualitative Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Qualitative Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion


        #region Result

        [HttpGet]
        public async Task<JsonResult> GetAllResult()
        {
            UserName = HttpContext.Session.GetString("UserName");

            GlobalClass.UserActivityLogFile(UserName, "Result Get List", "Entry");
            try
            {
                var Response = await dbContext.TblDirectoryResults.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Result Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Result Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveResult(TblDirectoryResult directoryResult)
        {
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");

            try
            {
          
                if (directoryResult.RsultId == 0)
                {

                    GlobalClass.UserActivityLogFile(UserName, "Result Save Data", "Entry");
                    TblDirectoryResult data = dbContext.TblDirectoryResults.Where(x => x.RsultName == directoryResult.RsultName && x.SoftDelete != true).FirstOrDefault();
                    if (data == null)
                    {
                        directoryResult.CreateBy = UserId;
                        dbContext.TblDirectoryResults.Add(directoryResult);
                        await dbContext.SaveChangesAsync();
                    }
                    GlobalClass.UserActivityLogFile(UserName, "Result Save Data", "Exit");
                }
                else
                {
                    GlobalClass.UserActivityLogFile(UserName, "Result Update Data", "Entry");
                    TblDirectoryResult data = await dbContext.TblDirectoryResults.Where(x => x.RsultName == directoryResult.RsultName && x.RsultId != directoryResult.RsultId && x.SoftDelete != true).FirstOrDefaultAsync();
                    if (data == null)
                    {
                        var obj = dbContext.TblDirectoryResults.Where(x => x.RsultId == directoryResult.RsultId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.RsultName = directoryResult.RsultName;
                            obj.ModifyBy = UserId;
                            obj.ModifyDate = DateTime.Now;
                        }
                        await dbContext.SaveChangesAsync();
                    }
                    GlobalClass.UserActivityLogFile(UserName, "Result Update Data", "Exit");
                }

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Result Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> GetByIdResult(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Result Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblDirectoryResults.Where(x => x.RsultId == Id).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Result Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Result Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteResult(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Result Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblDirectoryResult obj = dbContext.TblDirectoryResults.Where(x => x.RsultId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                }

                GlobalClass.UserActivityLogFile(UserName, "Result Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Result Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #region Clinical

        [HttpGet]
        public async Task<JsonResult> GetAllClinical()
        {
            UserName = HttpContext.Session.GetString("UserName");

            GlobalClass.UserActivityLogFile(UserName, "Clinical Get List", "Entry");
            try
            {
                var Response = await dbContext.TblDirectoryClinicals.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Clinical Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Clinical Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveClinical(TblDirectoryClinical directoryClinical)
        {
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");

            try
            {
                if (directoryClinical.ClnId == 0)
                {

                    GlobalClass.UserActivityLogFile(UserName, "Clinical Save Data", "Entry");
                    directoryClinical.CreateBy = UserId;
                    dbContext.TblDirectoryClinicals.Add(directoryClinical);
                    await dbContext.SaveChangesAsync();
                    GlobalClass.UserActivityLogFile(UserName, "Clinical Save Data", "Exit");
                }
                else
                {
                    GlobalClass.UserActivityLogFile(UserName, "Clinical Update Data", "Entry");
                    var data = dbContext.TblDirectoryClinicals.Where(x => x.ClnId == directoryClinical.ClnId);
                    foreach (var obj in data)
                    {
                        obj.ClnName = directoryClinical.ClnName;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;
                    }
                    await dbContext.SaveChangesAsync();
                    GlobalClass.UserActivityLogFile(UserName, "Clinical Update Data", "Exit");
                }

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Clinical Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> GetByIdClinical(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Clinical Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblDirectoryClinicals.Where(x => x.ClnId == Id).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Clinical Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Clinical Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteClinical(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Clinical Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblDirectoryClinical obj = dbContext.TblDirectoryClinicals.Where(x => x.ClnId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                }

                GlobalClass.UserActivityLogFile(UserName, "Clinical Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Clinical Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #region Sample Container

        [HttpGet]
        public async Task<JsonResult> GetAllSampleContainer()
        {
            UserName = HttpContext.Session.GetString("UserName");

            GlobalClass.UserActivityLogFile(UserName, "Sample Container Get List", "Entry");
            try
            {
                var Response = await dbContext.TblDirectorySamplecontainers.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Sample Container Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Sample Container Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveSampleContainer(TblDirectorySamplecontainer samplecontainer)
        {
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");
            bool returnValue = false;
            try
            {
                if (samplecontainer.SmpConId == 0)
                {
                    TblDirectorySamplecontainer data = dbContext.TblDirectorySamplecontainers.Where(x => x.SmpConName == samplecontainer.SmpConName && x.SoftDelete != true).FirstOrDefault();
                    if (data == null)
                    {
                        GlobalClass.UserActivityLogFile(UserName, "Sample Container Save Data", "Entry");
                        samplecontainer.CreateBy = UserId;
                        dbContext.TblDirectorySamplecontainers.Add(samplecontainer);
                        await dbContext.SaveChangesAsync();
                        GlobalClass.UserActivityLogFile(UserName, "Sample Container Save Data", "Exit");
                        returnValue = true;
                    }
                }
                else
                {
                    TblDirectorySamplecontainer data = await dbContext.TblDirectorySamplecontainers.Where(x => x.SmpConName == samplecontainer.SmpConName && x.SmpConId != samplecontainer.SmpConId && x.SoftDelete != true).FirstOrDefaultAsync();
                    if (data == null)
                    {
                        GlobalClass.UserActivityLogFile(UserName, "Sample Container Update Data", "Entry");

                        var obj = dbContext.TblDirectorySamplecontainers.Where(x => x.SmpConId == samplecontainer.SmpConId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.SmpConName = samplecontainer.SmpConName;
                            obj.ModifyBy = UserId;
                            obj.ModifyDate = DateTime.Now;
                            await dbContext.SaveChangesAsync();
                            returnValue = true;
                        }
                    }
                    GlobalClass.UserActivityLogFile(UserName, "Sample Container Update Data", "Exit");
                }

                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Sample Container Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> GetByIdSampleContainer(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Sample Container Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblDirectorySamplecontainers.Where(x => x.SmpConId == Id).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "SampleContainer Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Sample Container Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteSampleContainer(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Sample Container Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblDirectorySamplecontainer obj = dbContext.TblDirectorySamplecontainers.Where(x => x.SmpConId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                }

                GlobalClass.UserActivityLogFile(UserName, "Sample Container Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Sample Container Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #region Reagent Bottle Type

        [HttpGet]
        public async Task<JsonResult> GetAllReagentBottleType()
        {
            UserName = HttpContext.Session.GetString("UserName");

            GlobalClass.UserActivityLogFile(UserName, "Reagent Bottle Type Get List", "Entry");
            try
            {
                var Response = await dbContext.TblDirectoryReagentbottletypes.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Reagent Bottle Type Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Reagent Bottle Type Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveReagentBottleType(TblDirectoryReagentbottletype reagentbottletype)
        {
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");
            bool returnValue = false;
            try
            {
                if (reagentbottletype.RgntBtlId == 0)
                {
                    TblDirectoryReagentbottletype data = dbContext.TblDirectoryReagentbottletypes.Where(x => x.RgntBtlSize == reagentbottletype.RgntBtlSize && x.SoftDelete != true).FirstOrDefault();
                    if (data == null)
                    {
                        GlobalClass.UserActivityLogFile(UserName, "Reagent Bottle Type Save Data", "Entry");
                        reagentbottletype.CreateBy = UserId;
                        dbContext.TblDirectoryReagentbottletypes.Add(reagentbottletype);
                        await dbContext.SaveChangesAsync();
                        returnValue = true;
                        GlobalClass.UserActivityLogFile(UserName, "Reagent Bottle Type Save Data", "Exit");
                    }
                }
                else
                {
                    TblDirectoryReagentbottletype data = await dbContext.TblDirectoryReagentbottletypes.Where(x => x.RgntBtlSize == reagentbottletype.RgntBtlSize && x.RgntBtlId != reagentbottletype.RgntBtlId && x.SoftDelete != true).FirstOrDefaultAsync();
                    if (data == null)
                    {
                        GlobalClass.UserActivityLogFile(UserName, "Reagent Bottle Type Update Data", "Entry");
                        var obj = dbContext.TblDirectoryReagentbottletypes.Where(x => x.RgntBtlId == reagentbottletype.RgntBtlId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.RgntBtlSize = reagentbottletype.RgntBtlSize;
                            obj.ModifyBy = UserId;
                            obj.ModifyDate = DateTime.Now;
                        }
                        await dbContext.SaveChangesAsync();
                        returnValue = true;
                        GlobalClass.UserActivityLogFile(UserName, "Reagent Bottle Type Update Data", "Exit");
                    }
                }

                return Json(returnValue);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Reagent Bottle Type Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> GetByIdReagentBottleType(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Reagent Bottle Type Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblDirectoryReagentbottletypes.Where(x => x.RgntBtlId == Id).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Reagent Bottle Type Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Reagent Bottle Type Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteReagentBottleType(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Reagent Bottle Type Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblDirectoryReagentbottletype obj = dbContext.TblDirectoryReagentbottletypes.Where(x => x.RgntBtlId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                }

                GlobalClass.UserActivityLogFile(UserName, "Reagent Bottle Type Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Reagent Bottle Type Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #region QC Metarial

        [HttpGet]
        public async Task<JsonResult> GetAllQcMaterial()
        {
            UserName = HttpContext.Session.GetString("UserName");

            GlobalClass.UserActivityLogFile(UserName, "Qc Material Get List", "Entry");
            try
            {
                var Response = await dbContext.TblDirectoryQcmaterials.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Qc Material Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Qc Material Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveQcMaterial(TblDirectoryQcmaterial qcmaterial)
        {
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");

            try
            {
                if (qcmaterial.QcMtrlId == 0)
                {

                    GlobalClass.UserActivityLogFile(UserName, "Qc Material Save Data", "Entry");
                    TblDirectoryQcmaterial data = dbContext.TblDirectoryQcmaterials.Where(x => x.QcMtrlName == qcmaterial.QcMtrlName && x.SoftDelete != true).FirstOrDefault();
                    if (data == null)
                    {
                        qcmaterial.CreateBy = UserId;
                        dbContext.TblDirectoryQcmaterials.Add(qcmaterial);
                        await dbContext.SaveChangesAsync();
                    }
                    GlobalClass.UserActivityLogFile(UserName, "Qc Material Save Data", "Exit");
                }
                else
                {
                    GlobalClass.UserActivityLogFile(UserName, "Qc Material Update Data", "Entry");
                    TblDirectoryQcmaterial data = await dbContext.TblDirectoryQcmaterials.Where(x => x.QcMtrlName == qcmaterial.QcMtrlName && x.QcMtrlId != qcmaterial.QcMtrlId && x.SoftDelete != true).FirstOrDefaultAsync();
                    if (data == null)
                    {
                        var obj = dbContext.TblDirectoryQcmaterials.Where(x => x.QcMtrlId == qcmaterial.QcMtrlId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.QcMtrlName = qcmaterial.QcMtrlName;
                            obj.ModifyBy = UserId;
                            obj.ModifyDate = DateTime.Now;
                        }
                        await dbContext.SaveChangesAsync();
                        GlobalClass.UserActivityLogFile(UserName, "Qc Material Update Data", "Exit");
                    }
                }
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Qc Material Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> GetByIdQcMaterial(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Qc Material Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblDirectoryQcmaterials.Where(x => x.QcMtrlId == Id).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Qc Material Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Qc Material Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteQcMaterial(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Qc Material Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblDirectoryQcmaterial obj = dbContext.TblDirectoryQcmaterials.Where(x => x.QcMtrlId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                }

                GlobalClass.UserActivityLogFile(UserName, "Qc Material Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Qc Material Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        //End Data Directory

        //Start Customer Detail

        #region Hospital Information

        [HttpGet]
        public async Task<JsonResult> GetAllHospitalinfo()
        {
            UserName = HttpContext.Session.GetString("UserName");

            GlobalClass.UserActivityLogFile(UserName, "Hospital info Get List", "Entry");
            try
            {
                var Response = await dbContext.TblHospitalinfos.Where(x => x.SoftDelete != true).FirstOrDefaultAsync();
                GlobalClass.UserActivityLogFile(UserName, "Result Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Result Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveHospitalinfo(TblHospitalinfo hospitalinfo)
        {
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");

            try
            {

                if (hospitalinfo.HstlId == 0)
                {

                    GlobalClass.UserActivityLogFile(UserName, "Hospital information Save Data", "Entry");
                    hospitalinfo.CreateBy = UserId;
                    dbContext.TblHospitalinfos.Add(hospitalinfo);
                    await dbContext.SaveChangesAsync();
                    GlobalClass.UserActivityLogFile(UserName, "Hospital information Save Data", "Exit");
                }
                else
                {
                    GlobalClass.UserActivityLogFile(UserName, "Hospital information Update Data", "Entry");

                    var data = dbContext.TblHospitalinfos.Where(x => x.HstlId == hospitalinfo.HstlId);
                    foreach (var obj in data)
                    {
                        obj.HstlName = hospitalinfo.HstlName;
                        obj.HstlPhoneNo = hospitalinfo.HstlPhoneNo;
                        obj.HstlAddress = hospitalinfo.HstlAddress;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;
                    }

                    await dbContext.SaveChangesAsync();
                    GlobalClass.UserActivityLogFile(UserName, "Hospital information Update Data", "Exit");
                }

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Hospital information Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        #region Doctor Detail
        [HttpGet]
        public async Task<JsonResult> GetAllDoctor()
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Doctor Get List", "Entry");
            try
            {
                var Response = await dbContext.TblDoctordetails.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Doctor Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Doctor Get List", "Exit", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveDoctor(TblDoctordetail doctordetail)
        {
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");

            try
            {
                if (doctordetail.DrId == 0)
                {

                    GlobalClass.UserActivityLogFile(UserName, "Doctor Save Data", "Entry");
                    doctordetail.CreateBy = UserId;
                    dbContext.TblDoctordetails.Add(doctordetail);
                    await dbContext.SaveChangesAsync();
                    GlobalClass.UserActivityLogFile(UserName, "Doctor Save Data", "Exit");
                }
                else
                {
                    GlobalClass.UserActivityLogFile(UserName, "Doctor Update Data", "Entry");

                    var data = dbContext.TblDoctordetails.Where(x => x.DrId == doctordetail.DrId);
                    foreach (var obj in data)
                    {
                        obj.DrName = doctordetail.DrName;
                        obj.DrDepartment = doctordetail.DrDepartment;
                        obj.MobileNo = doctordetail.MobileNo;
                        obj.DrRemark = doctordetail.DrRemark;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;
                    }
                    await dbContext.SaveChangesAsync();
                    GlobalClass.UserActivityLogFile(UserName, "Doctor Update Data", "Exit");
                }

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Doctor Save or Update", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> GetByIdDoctor(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Doctor Get By Id", "Entry");
            try
            {
                var obj = await dbContext.TblDoctordetails.Where(x => x.DrId == Id).FirstOrDefaultAsync();
                GlobalClass.UserActivityLogFile(UserName, "Doctor Get By Id", "Exit");
                return Json(obj);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Doctor Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteDoctor(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Doctor Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblDoctordetail obj = dbContext.TblDoctordetails.Where(x => x.DrId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                }

                GlobalClass.UserActivityLogFile(UserName, "Doctor Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Doctor Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        #endregion

        //End Customer Detail

        //Start Operator Permission
  
        //End Operator Permission

        //Start Operator Directory
        #region Operator Directory
        [HttpGet]
        public async Task<JsonResult> GetAllOperator()
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Operator Get List", "Entry");
            try
            {
                var Response = await dbContext.TblOperators.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Operator Get List", "Exit");
                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Operator Get List", "Exit", ex.Message);
                return Json(false);
            }
        }

        [HttpPost]
        //public async Task<JsonResult> SaveOperator(OperatorUser Operator)
        //{
        //    bool returnValue = false;
        //    UserName = HttpContext.Session.GetString("UserName");
        //    UserId = HttpContext.Session.GetInt32("UserId");

        //    try
        //    {
        //        TblImage tblOperator = JsonConvert.DeserializeObject<TblImage>(Operator.OperatorObj);
        //        if (Operator.formFile != null)
        //        {
        //            if (Operator.formFile.Length > 0)
        //            {
        //                using (var ms = new MemoryStream())
        //                {
        //                    Operator.formFile.CopyTo(ms);
        //                    byte[] fileBytes = ms.ToArray();
        //                    tblOperator.Img = fileBytes;
        //                }
        //            }
        //        }
        //        if (tblOperator.Id == 0)
        //        {

        //            GlobalClass.UserActivityLogFile(UserName, "Operator Save Data", "Entry");
        //            TblImage data = dbContext.TblImages.Where(x => x.Id == tblOperator.Id).FirstOrDefault();
        //            if (data == null)
        //            {
        //                tblOperator.Id = 0;
        //                dbContext.TblImages.Add(tblOperator);
        //                await dbContext.SaveChangesAsync();

        //                GlobalClass.UserActivityLogFile(UserName, "Operator Save Data", "Exit");
        //                returnValue = true;
        //            }
        //        }
        //        //else
        //        //{
        //        //    GlobalClass.UserActivityLogFile(UserName, "Operator Update Data", "Entry");
        //        //    TblOperator data = await dbContext.TblOperators.Where(x => x.OprLoginId == tblOperator.OprLoginId && x.OprId != tblOperator.OprId).FirstOrDefaultAsync();
        //        //    if (data == null)
        //        //    {
        //        //        var obj = dbContext.TblOperators.Where(x => x.OprId == tblOperator.OprId).FirstOrDefault();
        //        //        if (obj != null)
        //        //        {
        //        //            obj.OprName = tblOperator.OprName;
        //        //            obj.OprEmail = tblOperator.OprEmail;
        //        //            obj.OprMobileNo = tblOperator.OprMobileNo;
        //        //            obj.OprLoginId = tblOperator.OprLoginId;
        //        //            obj.OprRoleId = tblOperator.OprRoleId;
        //        //            obj.OprImage = tblOperator.OprImage;
        //        //            if (tblOperator.OprImage == null)
        //        //            {
        //        //                obj.OprImage = LOperatorImage;
        //        //            }
        //        //            else
        //        //            {
        //        //                obj.OprImage = tblOperator.OprImage;
        //        //            }
        //        //            obj.ModifyBy = UserId;
        //        //            obj.ModifyDate = DateTime.Now;
        //        //        }
        //        //        await dbContext.SaveChangesAsync();
        //        //        returnValue = true;
        //        //    }
        //        //    GlobalClass.UserActivityLogFile(UserName, "Operator Update Data", "Exit");
        //        //}
        //        //if (HttpContext.Session.GetInt32("UserId") == tblOperator.OprId)
        //        //{
        //        //    HttpContext.Session.SetString("UserName", tblOperator.OprName);
        //        //    HttpContext.Session.SetInt32("UserId", tblOperator.OprId);
        //        //    HttpContext.Session.SetInt32("RoleId", (int)tblOperator.OprRoleId);

        //        //    tblOperator.OprImage = Convert.FromBase64String(Convert.ToBase64String(tblOperator.OprImage));

        //        //    string imreBase64Data = Convert.ToBase64String(tblOperator.OprImage);
        //        //    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
        //        //    HttpContext.Session.SetString("UserImage", imgDataURL);
        //        //}


        //    }
        //    catch (Exception ex)
        //    {
        //        GlobalClass.ErrorLogFile(UserName, "Operator Save or Update", "Exit", ex.Message);
        //    }
        //    return Json(returnValue);
        //}



        public async Task<JsonResult> SaveOperator(OperatorUser Operator)
        {
            bool returnValue = false;
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");

            try
            {
                TblOperator tblOperator = JsonConvert.DeserializeObject<TblOperator>(Operator.OperatorObj);
                if (Operator.formFile != null)
                {
                    if (Operator.formFile.Length > 0)
                    {
                        using (var ms = new MemoryStream())
                        {
                            Operator.formFile.CopyTo(ms);
                            byte[] fileBytes = ms.ToArray();
                            tblOperator.OprImage = fileBytes;
                        }
                    }
                }
                if (tblOperator.OprImage == null)
                {
                    tblOperator.OprImage = LOperatorImage;
                }

                returnValue = DataDirectoryViewModel.SaveOperator(tblOperator);

                //if (tblOperator.OprId == 0)
                //{

                //    GlobalClass.UserActivityLogFile(UserName, "Operator Save Data", "Entry");
                //    TblOperator data = dbContext.TblOperators.Where(x => x.OprLoginId == tblOperator.OprLoginId).FirstOrDefault();
                //    if (data == null)
                //    {
                //        tblOperator.CreateBy = UserId;
                //        dbContext.TblOperators.Add(tblOperator);
                //        await dbContext.SaveChangesAsync();

                //        GlobalClass.UserActivityLogFile(UserName, "Operator Save Data", "Exit");
                //        returnValue = true;
                //    }
                //}
                //else
                //{
                //    GlobalClass.UserActivityLogFile(UserName, "Operator Update Data", "Entry");
                //    TblOperator data = await dbContext.TblOperators.Where(x => x.OprLoginId == tblOperator.OprLoginId && x.OprId != tblOperator.OprId).FirstOrDefaultAsync();
                //    if (data == null)
                //    {
                //        //// tblOperator.CreateBy = data.CreateBy;
                //        //// tblOperator.CreateDate = data.CreateDate;
                //        //if(tblOperator.OprImage == null)
                //        //{
                //        //    tblOperator.OprImage = LOperatorImage;
                //        //}
                //        //tblOperator.ModifyBy = UserId;
                //        //tblOperator.ModifyDate = DateTime.Now;
                //        //dbContext.Entry(tblOperator).State = EntityState.Modified;
                //        ////dbContext.Update(tblOperator);

                //        var obj = dbContext.TblOperators.Where(x => x.OprId == tblOperator.OprId).FirstOrDefault();
                //        if (obj != null)
                //        {
                //            obj.OprName = tblOperator.OprName;
                //            obj.OprEmail = tblOperator.OprEmail;
                //            obj.OprMobileNo = tblOperator.OprMobileNo;
                //            obj.OprLoginId = tblOperator.OprLoginId;
                //            obj.OprRoleId = tblOperator.OprRoleId;
                //            obj.OprImage = tblOperator.OprImage;
                //            if (tblOperator.OprImage == null)
                //            {
                //                obj.OprImage = LOperatorImage;
                //            }
                //            else
                //            {
                //                obj.OprImage = tblOperator.OprImage;
                //            }
                //            obj.ModifyBy = UserId;
                //            obj.ModifyDate = DateTime.Now;
                //        }
                //        await dbContext.SaveChangesAsync();
                //        returnValue = true;
                //    }
                //    GlobalClass.UserActivityLogFile(UserName, "Operator Update Data", "Exit");
                //}
                if (HttpContext.Session.GetInt32("UserId") == tblOperator.OprId)
                {
                    HttpContext.Session.SetString("UserName", tblOperator.OprName);
                    HttpContext.Session.SetInt32("UserId", tblOperator.OprId);
                    HttpContext.Session.SetInt32("RoleId", (int)tblOperator.OprRoleId);

                    tblOperator.OprImage = Convert.FromBase64String(Convert.ToBase64String(tblOperator.OprImage));

                    string imreBase64Data = Convert.ToBase64String(tblOperator.OprImage);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    HttpContext.Session.SetString("UserImage", imgDataURL);
                }


            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Operator Save or Update", "Exit", ex.Message);
            }
            return Json(returnValue);
        }

        //public async Task<JsonResult> GetByIdOperator(int? Id)
        //{
        //    UserName = HttpContext.Session.GetString("UserName");
        //    GlobalClass.UserActivityLogFile(UserName, "Operator Get By Id", "Entry");
        //    try
        //    {
        //        var obj = await dbContext.TblOperators.Where(x => x.OprId == Id).FirstOrDefaultAsync();
        //        if(obj != null)
        //        {
        //            if (obj.OprImage != null)
        //            {
        //                if (!string.IsNullOrEmpty(Convert.ToBase64String(obj.OprImage)))
        //                {
        //                    LOperatorImage = obj.OprImage;
        //                    obj.OprImage = Convert.FromBase64String(Convert.ToBase64String(obj.OprImage));
        //                }
        //            }
        //        }
        //        GlobalClass.UserActivityLogFile(UserName, "Operator Get By Id", "Exit");
        //        return Json(new { obj });
        //    }
        //    catch (Exception ex)
        //    {
        //        GlobalClass.ErrorLogFile(UserName, "Operator Get By Id", "Exit", ex.Message);
        //        return Json(false);
        //    }
        //}



        public async Task<JsonResult> GetByIdOperator(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Operator Get By Id", "Entry");
            try
            {
                TblOperator obj = new TblOperator();
                obj = DataDirectoryViewModel.GetOperatorById(Id);
                if (obj != null)
                {
                    if (obj.OprImage != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToBase64String(obj.OprImage)))
                        {
                            LOperatorImage = obj.OprImage;
                            obj.OprImage = Convert.FromBase64String(Convert.ToBase64String(obj.OprImage));
                        }
                    }
                }


                GlobalClass.UserActivityLogFile(UserName, "Operator Get By Id", "Exit");
                return Json(new { obj });
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Operator Get By Id", "Exit", ex.Message);
                return Json(false);
            }
        }

        public async Task<JsonResult> DeleteOperator(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Operator Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblOperator obj = dbContext.TblOperators.Where(x => x.OprId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                    await dbContext.SaveChangesAsync();
                }

                GlobalClass.UserActivityLogFile(UserName, "Operator Delete", "Exit");
                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Operator Delete", "Exit", ex.Message);
                return Json(false);
            }

        }

        public async Task<JsonResult> ResetPasswordOperator(int? Id, string NewPassword)
        {
            bool returnVal = false;
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Operator Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    TblOperator obj = dbContext.TblOperators.Where(x => x.OprId == Id && x.SoftDelete != true).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.OprPassword = NewPassword;
                        dbContext.Entry(obj).State = EntityState.Modified;
                        await dbContext.SaveChangesAsync();
                        returnVal = true;
                    }
                    else
                    {
                        returnVal = false;
                    }
                }
                

                GlobalClass.UserActivityLogFile(UserName, "Operator Delete", "Exit");
                return Json(returnVal);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Operator Delete", "Exit", ex.Message);
                return Json(false);
            }

        }


        #endregion
        //End Operator Directory

        #region Manufacturer

        [HttpGet]
        public async Task<JsonResult> GetAllManufacturer()
        {
            UserName = HttpContext.Session.GetString("UserName");
            try
            {
                GlobalClass.UserActivityLogFile(UserName, "Manufacturer", "Entry");
                var Response = await dbContext.TblManufacturers.Where(x => x.SoftDelete != true).ToListAsync();
                GlobalClass.UserActivityLogFile(UserName, "Manufacturer", "Exist");

                return Json(Response);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Manufacturer", "Exist", ex.Message);
                return Json(false);
            }

        }

        [HttpPost]
        public async Task<JsonResult> SaveManufacturer(TblManufacturer TblManufacturers)
        {
            UserName = HttpContext.Session.GetString("UserName");
            UserId = HttpContext.Session.GetInt32("UserId");

            GlobalClass.UserActivityLogFile(UserName, "Manufacturer Save or Update", "Entry");

            try
            {
                Console.WriteLine(TblManufacturers.MnfId);
                if (TblManufacturers.MnfId == 0)
                {
                    TblManufacturers.CreatedBy = UserId;
                    dbContext.TblManufacturers.Add(TblManufacturers);

                }
                else
                {


                    var data = dbContext.TblManufacturers.Where(x => x.MnfId == TblManufacturers.MnfId);
                    foreach (var obj in data)
                    {
                        obj.MnfName = TblManufacturers.MnfName;
                        obj.MnfQc = TblManufacturers.MnfQc;
                        obj.MnfReg = TblManufacturers.MnfReg;
                        obj.MnfCali = TblManufacturers.MnfCali;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;
                    }
                }
                await dbContext.SaveChangesAsync();
                GlobalClass.UserActivityLogFile(UserName, "Manufacturer Save or Update", "Exit");

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Manufacturer Save or Update", "Exit", ex.Message);

                return Json(false);
            }
        }

        public async Task<JsonResult> GetByIdManufacturer(int? Id)
        {
            UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Manufacturer Get By Id", "Entry");
            var obj = await dbContext.TblManufacturers.Where(x => x.MnfId == Id).ToListAsync();
            GlobalClass.UserActivityLogFile(UserName, "Manufacturer Get By Id", "Exit");

            return Json(obj);
        }

        public async Task<JsonResult> DeleteManufacturer(int? Id)
        {
            string UserName = HttpContext.Session.GetString("UserName");
            GlobalClass.UserActivityLogFile(UserName, "Manufacturer Delete", "Entry");
            try
            {
                if (Id > 0)
                {
                    
                    TblManufacturer obj = dbContext.TblManufacturers.Where(x => x.MnfId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.SoftDelete = true;
                        obj.ModifyBy = UserId;
                        obj.ModifyDate = DateTime.Now;
                    }
                    dbContext.Entry(obj).State = EntityState.Modified;
                }
                else
                {
                }
                await dbContext.SaveChangesAsync();
                GlobalClass.UserActivityLogFile(UserName, "Manufacturer Delete", "Exit");

                return Json(true);
            }
            catch (Exception ex)
            {
                GlobalClass.ErrorLogFile(UserName, "Manufacturer Delete", "Exit", ex.Message);
                return Json(false);
            }

        }
        #endregion
    }


   
}
