﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.IO;

namespace SerialCommunication
{
    public interface IBaseCommunication
    {
        #region INITIALIZE COM PORT
        /// <summary>
        /// This function is used to configure Port property for serial communication.
        /// </summary>
        /// <param name="PortName">Com Port name</param>
        /// <param name="BaudRate">Com Port Baud Rate</param>
        void ConfigurePort(String PortName, int BaudRate);
        /// <summary>
        /// This function is used to Open Com port for communication.
        /// </summary>
        /// <param name="SrlPort">Serial Port object</param>
        /// <param name="strPortName">Com Port name</param>
        /// <param name="BaudRate">Baud Rate</param>
        void OpenPort(ref System.IO.Ports.SerialPort SrlPort, string strPortName, int BaudRate);
        /// <summary>
        /// This function is used to selected com port close.
        /// </summary>
        void ClosePort();
        /// <summary>
        /// This function is used to write data on Port.
        /// </summary>
        /// <param name="dataToSend">Data send to instrument</param>
        /// <param name="lenData">Length of data</param>
        void WriteToPort(byte[] dataToSend, int lenData);
        /// <summary>
        /// This function is used to close port
        /// </summary>
        void Dispose();
        #endregion
    }
#nullable disable
    public class Initialize : IBaseCommunication
    {
        #region VAR_DECLARATION
        public static SerialPort objSPort;
        public static string[] COMList;
        public byte[] ReceivedBytes;
        public string ReceivedString;
        public Action<byte[]> DataReceived;
        public Action<byte> iDataReceived;
        #endregion
        #region PORT_INIT
        public Initialize()
        {
            GetAvailablePorts();
        }
        public void OpenPort(ref SerialPort SrlPort, string strPortName, int BaudRate)
        {
            try
            {
                ClosePort();
                objSPort = SrlPort;
                ConfigurePort(strPortName, BaudRate);
                objSPort.Open();
                SrlPort = objSPort;
            }
            catch (Exception ex)
            {
                //CommunicationFailed(this, null);

            }
        }

        public void ClosePort()
        {
            if (objSPort != null)
            {
                if (objSPort.IsOpen)
                    objSPort.Close();
            }
        }
        public void Dispose()
        {
            ClosePort();
        }

        public void ConfigurePort(string PortName, int BaudRate)
        {
            if (objSPort.IsOpen)
            {
                objSPort.Close();
            }

            objSPort.BaudRate = BaudRate;
            objSPort.Parity = Parity.None;
            objSPort.DataBits = 8;
            objSPort.StopBits = StopBits.One;
            objSPort.PortName = PortName;
            objSPort.Handshake = Handshake.None;
            objSPort.DtrEnable = true;
            objSPort.RtsEnable = true;
            objSPort.ReadTimeout = 1000;
            objSPort.WriteTimeout = 1000;
            //objSPort.Encoding = System.Text.Encoding.GetEncoding(28591);
            objSPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);

        }
        #endregion

        #region WRITE_TO_PORT
        public void WriteToPort(byte[] dataToSend, int lenData)
        {
            if (objSPort.IsOpen)
            {
                objSPort.Write(dataToSend, 0, lenData);
            }
        }
        #endregion

        #region READ_FROM_PORT

        void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            /*byte[] buffer = new byte[3000];
            Action kickoffRead = null;
            kickoffRead = delegate {
                objSPort.BaseStream.BeginRead(buffer, 0, buffer.Length, delegate (IAsyncResult ar) {
                    try
                    {
                        int actualLength = objSPort.BaseStream.EndRead(ar);
                        byte[] received = new byte[actualLength];
                        Buffer.BlockCopy(buffer, 0, received, 0, actualLength);
                        var copy = DataReceived;
                        if (copy != null) copy(received);
                    }
                    catch (IOException exc)
                    {
                        handleAppSerialError(exc);
                    }
                    kickoffRead();
                }, null);
            };
            kickoffRead();*/
            SerialPort SPort = (sender as SerialPort);
            int BufferSize = SPort.BytesToRead;
            ReceivedBytes = new byte[BufferSize];
            SPort.Read(ReceivedBytes, 0, BufferSize);
            var copy = DataReceived;
            if (copy != null) copy(ReceivedBytes);


            /*byte iRecieved = 0;
            iRecieved = (byte)SPort.ReadByte();
            var copy = iDataReceived;
            if (copy != null) copy(iRecieved);*/
        }
        #endregion

        public void GetAvailablePorts()
        {
            COMList = new string[SerialPort.GetPortNames().Count()];
            int i = 0;
            foreach (string s in SerialPort.GetPortNames())
            {
                COMList[i] = s;
                i++;
            }
        }

        public static readonly List<string> SupportedBaudRates = new List<string>
        {
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200",
        };
    }
}