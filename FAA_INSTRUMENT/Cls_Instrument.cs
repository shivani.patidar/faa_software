﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Reflection;
using System.IO;
using SerialCommunication;
namespace FAA_INSTRUMENT;

#nullable disable
public static class HEXCOMMANDS
{
    #region ARM_HEXCOMMANDS
    public const byte ARM_ROT_HOD_CLOCKWISE = 0x84;
    public const byte ARM_ROT_HOD_ANTICLOCKWISE = 0xC4;
    public const byte ARM_ROT_CLOCKWISE = 0x80;
    public const byte ARM_ROT_ANTICLOCKWISE = 0xC0;
    public const byte ARM_ROT_OPTO_CLOCKWISE = 0xA1;
    public const byte ARM_DN = 0x80;
    public const byte ARM_DN_LLS = 0x82;
    public const byte ARM_DN_VOD = 0x84;
    public const byte ARM_DN_LLS_VOD = 0x86;
    public const byte ARM_UP = 0xE1;
    #endregion

    #region STR_HEXCOMMANDS
    public const byte STR_ROT_CLOCKWISE = 0xA1;
    public const byte STR_ROT_ANTICLOCKWISE = 0xC0;
    public const byte STR_UP = 0xA1;
    public const byte STR_DN = 0xC0;
    #endregion

    #region CRU_HEXCOMMANDS
    public const byte CRU_UP = 0xA1;
    public const byte CRU_DN = 0xC0;
    public const byte CRU_DN_1 = 0x80;
    #endregion

    #region RGT_HEXCOMMANDS
    public const byte RGT_ROT_CLOCKWISE = 0xC0;
    public const byte RGT_ROT_ANTICLOCKWISE = 0x80;
    public const byte RGT_ROT_UNKNOWN = 0x81;
    #endregion

    #region SYRINGE_HEXCOMMANDS
    public const byte SYRINGE_OPTO = 0xA1;
    public const byte SYRINGE_ASPIRATE = 0xE0;
    public const byte SYRINGE_DISPENSE = 0x80;
    public const byte SYRINGE_SWITCH = 0xC1;
    public const byte SYRINGE_ESW_KEYTO = 0x31;
    public const byte SYRINGE_ESW_MERIL = 0x30;
    #endregion

    #region OTHERS
    public const byte VALVES_PUMPS = 0x0A;
    public const byte SENSOR_CHECK = 0x11;

    #endregion
}
public static class Instrument_Constants
{
    public static int ReagentCount { get; set; }
    public static int SampleCount { get; set; }
    public static int CuvetCount { get; set; }
    public static double OpticalPathLength { get; set; }
    public static int FilterCount { get; set; }
    public static double Default_Syringe_Scale { get; set; }   //8
    public static double Keyto_Syringe_Scale { get; set; }    //16
    public static int[] Filters { get; set; }
    public static int Signal_Threshold { get; set; }
    public static int[] CRUHands { get; set; }
    public static ENUM_INSTRUMENTS ConnectedInstrument { get; set; }
    public static int Cuv_WL_WASH { get; set; }            //WL_WASH=12
    public static int Cuv_WL_GWC { get; set; }            //WL_GWC=16
    public static int ISE_CLEANER_POS { get; set; }
    public static int ISE_CONDITIONER_POS { get; set; }
    public static int ISE_CALIBRATOR_POS { get; set; }

}
public static class ARMSTEPS
{
    public static int ARM_ROT_SMP { get; set; }
    public static int ARM_ROT_RGT_IN { get; set; }
    public static int ARM_ROT_RGT_OUT { get; set; }
    public static int ARM_ROT_CUV { get; set; }
    public static int ARM_ROT_DIL_CUV { get; set; }
    public static int ARM_ROT_ISE { get; set; }
    public static int ARM_DN_HOME { get; set; }
    public static int ARM_DN_SMP { get; set; }
    public static int ARM_DN_RGT { get; set; }
    public static int ARM_DN_CUV { get; set; }
    public static int ARM_DN_DIL_CUV { get; set; }
    public static int ARM_DN_ISE { get; set; }
    public static int ARM_UP_MAX { get; set; }      //FOR INSTRUMENT INIT  ARM_UP_MAX=3500
    public static int ARM_UP_OFFSET { get; set; }   //TO PROVIDE OFFSET BETWEEN INSTRUMENT BODY AND PROBE
}

public static class STR_STEPS
{
    public static int STR_ROT_CUV { get; set; }
    public static int STR_DN_CUV { get; set; }
    public static int STR_DN_HOME { get; set; }
    public static int STR_UP_MAX { get; set; }          //FOR INSTRUMENT INIT  STR_UP_MAX=1100
    public static int STR_UP_OFFSET { get; set; }       //TO PROVIDE OFFSET BETWEEN INSTRUMENT BODY AND STIRRER
}

public static class CRU_STEPS
{
    public static int CRU_DOWN { get; set; }
    public static int CRU_DI_DISPENSE { get; set; }
    public static int CRU_MID1 { get; set; }        //CRU_MID1=CRU_DOWN-CRU_DI_DISPENSE-CRU_MID2
    public static int CRU_MID2 { get; set; }        //CRU_MID2=12
    public static int CRU_UP_MAX { get; set; }      //CRU_UP_MAX=2800
    public static int CRU_ADD_WATER_TIME { get; set; }
    public static int CRU_PUMP_BACK_TIME { get; set; }
    public static int CRU_WASH_HANDS { get; set; }
}

public static class RGT_STEPS
{
    public static int RGT_RG_POS_IN_1 { get; set; }
    public static int RGT_RG_POS_OUT_1 { get; set; }
    public static int RGT_SMP_POS1 { get; set; }
    public static int RGT_RG_BARCODE { get; set; }
    public static int RGT_SMP_BARCODE { get; set; }
}

public static class MOTORSPEEDS
{
    public static int ARM_ROT_SPEED_1 { get; set; }
    public static int ARM_ROT_SPEED_2 { get; set; }
    public static int ARM_DN_SPEED_1 { get; set; }
    public static int ARM_DN_SPEED_2 { get; set; }

}

public static class SYRINGE
{
    public static ENUM_SYRINGE_TYPE SYRINGE_TYPE { get; set; }
    public static int SYRINGE_OFFSET { get; set; }
    public static int SYRINGE_WASHTIME { get; set; }

}

public static class INST_STATUS
{
    public static bool IS_ARM_HOD { get; set; }
    public static bool IS_ARM_VOD { get; set; }
    public static bool IS_DI_EMPTY { get; set; }
    public static bool IS_WASTE_FULL { get; set; }
    public static bool IS_COM_ERROR { get; set; }
    public static ON_OFF LAMP_STATUS { get; set; }
    public static double RCT_TEMP { get; set; }
    public static double RGT_TEMP { get; set; }
    public static double PRESSURE { get; set; }
    public static double TARGET_TEMP { get; set; }
    public static double TARGET_PRESSURE { get; set; }
    public static double Correction_Coefficient { get; set; }
    public static ENUM_MOTOR200 ERROR_MOTOR_NUM { get; set; }
    public static string CURRENT_BARCODE { get; set; }
    public static bool IS_INST_BUSY { get; set; }
    public static float[,] ADCData { get; set; }
    public static bool[] WaterBlankCheck { get; set; }
    public static int iReadCuvCount { get; set; }

}

public static class Port_Configuration
{
    public static string strSerialPort { get; set; }
    public static int BaudRate { get; set; }
}


#region ENUMS

public enum ENUM_INSTRUMENTS
{
    Nano100 = 1,
    Nano200 = 2,
    Nano400 = 3,
}
public enum ENUM_SMP_ARM_ROT
{
    HOME = 0,
    SMP_CUVET = 1,
    DIL_CUVET = 2,
    OUTER = 3,
    MIDDLE = 4,
    INNER = 5
}

public enum ENUM_R2_ARM_ROT
{
    HOME = 0,
    RCT_180 = 1,
    RCT_270 = 2,
    RGT_INNER = 3,
    RGT_OUTER = 4
}

public enum ENUM_R1_ARM_ROT
{
    HOME = 0,
    RCT = 1,
    RGT_INNER = 2,
    RGT_OUTER = 3
}
public enum ENUM_ARM_ROT
{
    HOME = 0,
    CUVET = 1,
    SMP = 2,
    RGT_IN = 3,
    RGT_OUT = 4,
    DIL_CUVET = 5,
    ISE = 6
}

public enum ENUM_STR_ROT
{
    HOME = 0,
    CUVET = 1
}

public enum ENUM_STR_UPDN
{
    UP = 0,
    DOWN = 1,
    UP_MAX = -1
}

public enum ENUM_SYRINGE_TYPE
{
    MERIL_SYRINGE = 0,
    KEYTO_SYRINGE = 1,
}

public enum ENUM_MARKS
{
    SAMPLE = 1,
    R1_S = 2,
    R2 = 3,
    RESULTS = 4,
    R1 = 5,
    DIRTY = 6,
    RERUN = 7,
    SHORT = 8
}

public enum ENUM_ARM_UP_DN
{
    UP = 0,
    DOWN = 1,
    LLS = 2,
    UP_MAX = -1,
    DOWN_CUV = 6,                  //USED while washing cuvet using probe 
    LLS_VOD=10     
}
public enum ENUM_CRU_UPDN
{
    UP = 0,
    DOWN = 1,
    DOWN_DI_DISPENSE = 2,
    UP_MAX = -1,
    DOWN_MID1 = -2,
    DOWN_MID2 = -5
}

public enum ENUM_SYRINGE
{
    OPTO = 0,
    ASPIRATE = 1,
    DISPENSE = 2
}

public enum ENUM_METHODS
{
    ENDPOINT = 1,
    KINETIC = 2,
    TWO_POINT = 3,
}

public enum ENUM_CRU_ARMS
{
    PROBE1 = 0,
    PROBE2 = 1,
    PROBE3 = 2,
    PROBE4 = 3,
    PROBE5 = 4,
    EMPTY = 5,
    SUCTION = 6,
    WIPER = 7,
    HANDCOUNT = 8
}

public enum ENUM_INSTRUMENT_STATE
{
    NORMAL_PACKET = 0xAA,
    COMMUNICATION_FAILURE = 0xBB,
    TASK_COMPLETED = 0x55,
    INVALID_BARCODE_DATA = 0xAB,
    INVALID_DATA = 0xCC
}
[Flags]
public enum ENUM_PROTOCOLHEADER
{
    RECIEVE_HEADER_1 = 0xEB,
    RECIEVE_HEADER_2 = 0x90,
    SEND_HEADER_1 = 0xEB,
    SEND_HEADER_2 = 0x91,
}

public enum ENUM_MOTOR_STATE
{
    HOD_VOD = 0x03,
    RCT_TEMPERATURE = 0x0C,
    RGT_TEMPERATURE = 0x10,
    PRESSURE=0x0E,
    ALARM = 0x11,
    READ_SET_PARAM=0x13
}


public enum ENUM_CURVETYPE
{
    ONE_POINT = 1,
    TWO_POINTS = 2,
    MULTIPLE_POINTS = 3,
    LOGIT_LOG_4P = 4,
    LOGIT_LOG_5P = 5,
    EXPONENTIAL_5P = 6,
    POLYNOMIAL_5P = 7,
    PARABOLE = 8,
    SPLINE = 9,
    K_FACTOR = 10
}

public enum ENUM_ISE_CMD
{
    ACK = 1,                    //Receive Acknowledgement Action Yes  
    INFO = 2,                   // ISE Product Info Status   
    STATUS_CHECK = 3,           // ISE State Status Yes  
    CAL_STATUS = 4,             // Calibration Status Status   
    CAL_START = 5,              // Start Calibration Measurement   
    RUN_SMP_SRM = 6,            // Run Serum Sample Measurement   
    RUN_SMP_URN = 7,            // Run Urine Sample Measurement   
    RUN_QC1 = 8,                // Run QC 1 Measurement   
    RUN_QC2 = 9,                // Run QC 2 Measurement   
    RUN_QC3 = 10,               // Run QC 3 Measurement   
    RUN_STD = 11,               // Run Standard Sample Measurement   
    SET_STAND_BY_TIME = 12,     // Set Auto Standby Time Setting   
    GET_STAND_BY_TIME = 13,     // Get Auto Standby Time Setting   
    START_STAND_BY = 14,        // Activate Standby Setting   
    STOP_STAND_BY = 15,         // Deactivate Standby Setting   
    GET_STAND_BY_STATUS = 16,   // Stand By Status Status   
    SET_PARAM_STNG = 17,        // Set Parameter Setting   
    GET_PARAM_STNG = 18,        // Get Parameter Config Setting   
    PRIME_A = 19,               // Prime A Maintenance   
    PURGE_ALL = 20,             // Purge All Maintenance   
    CLEAN = 21,                 // Clean Maintenance   
    CONDITION = 22,             // Condition Maintenance   
    START_MAINTENANCE = 23,     // Enter Maintenance Maintenance   
    END_MAINTENANCE = 24,       // Exit Maintenance Maintenance   
    WASH_CHAMBER = 25,          // Wash Chamber Maintenance   
    CLEAR_CHAMBER = 26,         // Clear Chamber Maintenance   
    CLEAR_PATH_CLOG = 27,       // Clear Path Clog Maintenance   
    SHUTDOWN = 28,              // Shutdown Maintenance   
    FILL_CUP_FOR_ALLIGN = 29,   // Fill Cup for alignment Maintenance   
                                /*[] ="030";// Last Sample Result Results   
                                [] ="031";// Get Sample By ID Results   
                                [] ="032";// Last Calibration Data Results   
                                [] ="033";// Get Last QC Level 1 Results   
                                [] ="034";// Get Last QC Level 2 Results   
                                [] ="035";// Get Last QC Level 3 Results   
                                [] ="036";// Delete Sample By Id Results   
                                [] ="037";// Delete All Samples Results   
                                [] ="038";// Last Error Codes Results   
                                [] ="039";// Delete All Errors Results   
                                [] ="040";// Set Auto Save Inst Setting   
                                [] ="041";// Get Auto Save Inst Setting   */
    CHANGE_ISE_PACK = 42,       // Change ISE pack Inst Setting
    GET_FLUID_PACK_STATUS = 43, // Get Pack Status Inst Setting      
    CHANGE_ELECTRODE = 44,      // Enter Electrode Barcode Inst Setting   
    GET_ELECTRODE_STATUS = 45,  // Get Electrode Status Inst Setting   
    GET_TEMPERATURE = 46,       // Get Temperature Diagnose   
    CAL_SMP_SENSOR = 47,        // Calibrate Sample Sensor Diagnose   
    CHECK_REF_ELECTRODE = 48,   // Check Reference Electrode Diagnose   
    CHECK_SMP_SENSOR = 49,      // Sample Detector Test Diagnose   

    TEST_REF_PUMP = 50,         // Pump Test Ref Diagnose  
    TEST_STDA_PUMP = 51,        // Pump Test A Diagnose   
    TEST_SMP_PUMP = 52,         // Pump Test Sample Diagnose   
    CHECK_VALVE = 53,           // Reference Valve Test Diagnose   
                                //[] ="054";// Load Software Diagnose   
    RESTART = 54,               // Reinitialize Diagnose Yes  
                                //[] ="056";// Factory Reset Diagnose   
                                //[] ="057";// Next Data Action  Yes 
    WAIT_FOR_SMP = 58,          // Waiting for Sample Action  Yes 
    DISPENSE_ACK = 59,          // Sample / Calibrator Ready Action  Yes 
    TASK_COMPLETED = 60,        // Task Complete Action   
    KILL_TASK = 61,             // Kill Current Task Action Yes  
                                //[] ="062";// Check STD-A mV Diagnose   
                                //[] ="063";// Check External mV Diagnose   


    PREPARE_FOR_SMP = 68,       // Prepare for Sample Measurement
                                //SMP_MODE_SINGLE = "065003DBW";
                                //SMP_MODE_DOUBLE = "065003EBW";
    RELOAD_LAST_CALIB = 71,     // Load Last Calibration
    CHECK_POWER_FAILURE = 72,   // Check Power failure
    RESET_POWER_FLAG = 73       // Reset Power failure flag
}

public enum ENUM_ISE_ERROR
{
    SMP_DETECT_CALIB_FAILED=1,
    CMD_TIMEOUT=2,
    NO_SMP_DISPENSE=3,
    AIR_IN_STD_A=4,
    AIR_IN_STD_B=5,
    ELECTRODES_NOT_CALIBRATED=6,
    PATH_CLOGGED=7,
    AIR_IN_SMP=8,
    ISE_BUS_FAIL=9,
    OUT_OF_STD_A=10,
    FLASH_DATA_SAVE=11,
    REF_ELECTRODE_PRESSURE_FAIL=12,
    FLASH_READ_ERROR=13,
    FLASH_WRITE_ERROR=14,
    FLASH_ERASE_ERROR=15,
    NO_DATA_FOUND=16,
    DATA_INVALID=17,
    DATA_NOT_COMPLETE=18,
    INVALID_FLUID_PACK=19,
    EXPIRED_FLUID_PACK=20,
    DUPLICATE_FLUID_PACK=21,
    EMPTY_FLUID_PACK=22,
    INVALID_ELECTRODE=23,
    MV_RANGE=24,
    NOISE_IN_SIGNAL=25,
    MV_DRIFT=26,
    VALUE_LOW=27,
    VALUE_HIGH=28,
    VALUE_TOO_LOW=29,
    VALUE_TOO_HIGH=30,
    NOT_CALIBRATED=31,
    NOT_CONSISTENT=32,
    CANNOT_MEASURE_VALUE=33
}

public enum ENUM_MOTOR200
{
    RCT = 0,
    SYRINGE = 1,
    ARM_UPDOWN = 2,
    ARM_ROTATE = 3,
    CRU = 4,
    STR_UPDOWN = 5,
    STR_ROTATE = 6,
    RGT = 7,
    BARCODE = 210,
    ISE = 250,
    ISE_NO_RESPONSE = 251
}

public enum VALVES_PUMPS
{
    INTERNAL_PROBE_VALVE = 1,
    TROUGH_VALVE = 2,
    CRU_DISPENSE_PUMP = 3,
    CRU_DI_BACK_PUMP = 4,
    CRU_CLEANING_PUMP = 10,
    LAMP = 11,
    STIRRER_REVOLVE = 12
}

public enum ON_OFF
{
    ON = 1,
    OFF = 0
}

public enum CUVET_CLEAN_OPERATION
{
    INIT = 1,
    PROBE_WASH = 2,
    WASH_CNT_CUVET = 3,
    WASH_ALL_CUVET=4,
    WASH_WITH_FLUID=5,
    END_OPERATION = 20,
    NO_OPERATION = 0
}

 public enum ENUM_BTN_SELECTION
    {
        ARM_HOMEPOS=1,
        ARM_HOMEDEEP=2,
        ARM_CUVETPOS=3,
        ARM_CUVETDEEP=4,
        ARM_REG1POS_IN=5,
        ARM_REG1POS_OUT=6,
        ARM_SMP1POS=7,
        ARM_REGDEADVOL=8,
        ARM_SMPDEADVOL=9,
        ARM_REGDEEP=10,
        ARM_SMPDEEP=11,
        ARM_ISEPOS=12,
        ARM_ISEDEEP=13,
        RGT_REAGENTPOS_IN=14,
        RGT_REAGENTPOS_OUT=15,
        RGT_SAMPLEPOS=16,
        
        STR_HOMEPOS=17,
        STR_HOMEDEEP=18,
        STR_CUVETPOS=19,
        STR_CUVETDEEP=20,
        CRU_DEEP=21,
        CRU_DIDISPENSE=22,
        BAR_REGPOS1_IN=23,
        BAR_REGPOS1_OUT=24,
        BAR_SMPPOS1=25,
        SYRINGE_OFFSET=26
    }
#endregion
public class Cls_Instrument
{
     #region VARIABLE DECLARATION
        short Check_Single;
        int Send_Len = 0;
        int Send_Data_Again_Len = 0;
        byte[] Send_Data = new byte[9000];
        byte[] Send_Data_Again = new byte[9000];
        Initialize portInit;
        public ENUM_ARM_ROT ARM_HORZONTAL_POS;
        public ENUM_ARM_UP_DN ARM_VERTICAL_POS;
        public ENUM_STR_ROT STR_HORIZONTAL_POS;
        public ENUM_STR_UPDN STR_VERTICAL_POS;
        int RGT_Offset_Steps;               //Offset between R1 POS and SMP1 POS
        int RGT_RG_CURRENT_POS;
        int RGT_SMP_CURRENT_POS;
        double RGT_Single_Pos_Steps;
        int iLastRoundUpSteps;
        public bool Is_Syringe_Maintain = false;
        public bool Is_ADValue_Detection = false;
        public bool Is_Read_ADC = false;


        public int iBitNum;
        int iIndex;
        byte[] m_Data_Set;
        int iStart;
        int Set_Step;

        public int ReadIndex = 0;
        string strFullRecievedData;

        byte[] ISE_Received;
        public byte[] ISE_RESULTS;

        ENUM_ISE_CMD LAST_ISE_CMD;

        bool isISEResponseRequired = false;
        public ENUM_ISE_CMD ISE_TASKCOMPLETED;
        public ENUM_ISE_ERROR ISE_CURRENT_ERROR;
        public bool Is_ISE_ReadyForSample = false;



    #endregion

    #region CONSTRUCTOR
        public Cls_Instrument(ref Initialize objPortInit)
        {
            Check_Single = 0;
            Send_Data = new byte[9000];
            Send_Len = 0;
            Send_Data_Again_Len = 0;
            Send_Data_Again = new byte[9000];
            portInit = objPortInit;
            RGT_RG_CURRENT_POS = 1;
            RGT_SMP_CURRENT_POS = 0;
            RGT_Offset_Steps = RGT_STEPS.RGT_RG_POS_OUT_1 - RGT_STEPS.RGT_SMP_POS1;
            iLastRoundUpSteps = 0;
            RGT_Single_Pos_Steps = Get_RGT_SINGLE_POS_STEPS();
            strFullRecievedData = "";

            iBitNum = 0;
            iIndex = 0;
            iStart = -1;
            m_Data_Set = new byte[30];
            Set_Step = 500;


            INST_STATUS.IS_ARM_HOD = false;
            INST_STATUS.IS_ARM_VOD = false;
            INST_STATUS.IS_DI_EMPTY = false;
            INST_STATUS.IS_WASTE_FULL = false;
            INST_STATUS.IS_COM_ERROR = false;

            Is_Syringe_Maintain = false;

            if (SYRINGE.SYRINGE_TYPE == ENUM_SYRINGE_TYPE.KEYTO_SYRINGE)
                Instrument_Constants.Keyto_Syringe_Scale = (float)8;
            else
                Instrument_Constants.Default_Syringe_Scale = (float)16;

            INST_STATUS.ADCData = new float[Instrument_Constants.CuvetCount + 1, Instrument_Constants.FilterCount + 1];
            INST_STATUS.WaterBlankCheck = new bool[Instrument_Constants.CuvetCount];
            ReadIndex = 0;
        }
        #endregion
        #region INSTRUMENT BASE SEND DATA
        public void SendOrder(ENUM_MOTOR200 Motor, byte Order, byte Buf1, byte Buf2, byte Buf3, byte Buf4)
        {
            byte[] strSendData = new byte[10];
            bool m_End;
            strSendData[0] = Convert.ToByte(ENUM_PROTOCOLHEADER.SEND_HEADER_1);
            strSendData[1] = Convert.ToByte(ENUM_PROTOCOLHEADER.SEND_HEADER_2);
            strSendData[2] = (byte)Motor;
            strSendData[3] = Order;
            strSendData[4] = Buf1;
            strSendData[5] = Buf2;
            strSendData[6] = Buf3;
            strSendData[7] = Buf4;
            if (Motor == 0 && Order == 8)
            {
                m_End = true;
                Check_Single++;
                strSendData[4] = Convert.ToByte(Check_Single / 256);
                strSendData[5] = Convert.ToByte(Check_Single % 256);
                strSendData[8] = (byte)(strSendData[2] + strSendData[3] + strSendData[4] + strSendData[5] + strSendData[6] + strSendData[7]);
            }
            else
            {
                m_End = false;
                strSendData[8] = (byte)(strSendData[2] + strSendData[3] + strSendData[4] + strSendData[5] + strSendData[6] + strSendData[7]);
                Check_Single++;
            }
            for (short i = 0; i < 9; i++)
            {
                Send_Data[Send_Len++] = strSendData[i];
            }
            if (m_End)
            {
                LogRecievedData(BitConverter.ToString(Send_Data, 0, Send_Len));
                portInit.WriteToPort(Send_Data, Send_Len);
                for (int ii = 0; ii < Send_Len; ii++)
                {
                    Send_Data_Again[ii] = Send_Data[ii];
                }
                Send_Data_Again_Len = Send_Len;
                Send_Len = 0;
                Check_Single = 0;
                Send_Data = new byte[9000];
                return;
            }
        }

        public void SendOrderAgain()
        {
            portInit.WriteToPort(Send_Data_Again, Send_Data_Again_Len);
        }

        public void CheckMotor(ENUM_MOTOR200 Motor)
        {
            SendOrder(Motor, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0);
        }
        public void MoveMotor(ENUM_MOTOR200 Motor, byte Direction, int Steps)       //Set the number of steps the motor sports ÉèÖÃµç»úÔË¶¯²½Êý
        {
            SendOrder(Motor, (byte)2, Direction, (byte)(Steps / 256), (byte)(Steps % 256), 0);
        }
        public void SendEnd()
        {
            iBitNum = 0;
            INST_STATUS.IS_INST_BUSY = true;
            if (isISEResponseRequired == true)
            {
                isISEResponseRequired = false;
                SendISEDataENQ();
            }
            SendOrder((byte)0, (byte)8, (byte)0, (byte)0, (byte)0, (byte)8);
        }
        public void Rec_Speed(short Speed)
        {
            SendOrder((byte)0, (byte)200, (byte)(Speed / 256), (byte)(Speed % 256), (byte)0, (byte)0);
        }
        public void SetSpeed(ENUM_MOTOR200 Motor, byte Speed1, byte Speed2)	//Set velocity
        {
            byte b1 = 20, b2 = 0;
            if (Speed1 > 30)
            {
                if (Speed1 == 0XF1) b1 = 13;
                if (Speed1 == 0XF2) b1 = 14;
                if (Speed1 == 0XF3) b1 = 15;
                if (Speed1 == 0XF4) b1 = 16;
                if (Speed1 == 0XF5) b1 = 17;
                if (Speed1 == 0XF6) b1 = 18;
                if (Speed1 == 0XF7) b1 = 19;
                if (Speed1 == 0XF8) b1 = 20;
                if (Speed1 == 0XF9) b1 = 21;
                if (Speed1 == 0XFA) b1 = 22;
                if (Speed1 == 0XFB) b1 = 23;
                if (Speed1 == 0XFC) b1 = 24;
                if (Speed1 == 0XFD) b1 = 25;
                if (Speed1 == 0XFE) b1 = 26;
                if (Speed1 == 0XFF) b1 = 27;
            }
            else b1 = Speed1;
            if (Speed2 > 30)
            {
                if (Speed2 == 0XF1) b2 = 13;
                if (Speed2 == 0XF2) b2 = 14;
                if (Speed2 == 0XF3) b2 = 15;
                if (Speed2 == 0XF4) b2 = 16;
                if (Speed2 == 0XF5) b2 = 17;
                if (Speed2 == 0XF6) b2 = 18;
                if (Speed2 == 0XF7) b2 = 19;
                if (Speed2 == 0XF8) b2 = 20;
                if (Speed2 == 0XF9) b2 = 21;
                if (Speed2 == 0XFA) b2 = 22;
                if (Speed2 == 0XFB) b2 = 23;
                if (Speed2 == 0XFC) b2 = 24;
                if (Speed2 == 0XFD) b2 = 25;
                if (Speed2 == 0XFE) b2 = 26;
                if (Speed2 == 0XFF) b2 = 27;
            }
            else b2 = Speed2;
            SendOrder(Motor, 1, b1, b2, 0, 0);
            //Speed[MotorNum - 1, 0] = (char)b1;
            //Speed[MotorNum - 1, 1] = (char)b2;
        }
        public void Delay_Time(int time_ms) //1 is expressed as ti ms 
        {
            time_ms = time_ms * 10;
            SendOrder(0, 2, Convert.ToByte(time_ms / 256), Convert.ToByte(time_ms % 256), 0, 0);
        }
        public void INIT_REC()
        {
            SendOrder(0, 5, 0X00, 0X00, 0, 0X00);
        }

        public void SetFilterOrder()
        {
            SendOrder(0, 0X12, (byte)Instrument_Constants.FilterCount, 0, 0, 0);   //SendOrder(0, 0X12, filters, 0, 0, 0);
        }
        #endregion      
        #region ARM_OPERATIONS
        public void ARM_ROTATE(ENUM_ARM_ROT ARM_NEW_LOCATION)
        {
            int steps = 0;
            if (ARM_VERTICAL_POS == ENUM_ARM_UP_DN.DOWN || ARM_VERTICAL_POS == ENUM_ARM_UP_DN.LLS)  //@MT IF ARM IS DOWN THEN DO NOT ALLOW ARM TO MOVE IN HORIZONTAL DIRECTION
                return;
            switch (ARM_HORZONTAL_POS)
            {
                case ENUM_ARM_ROT.HOME:
                    switch (ARM_NEW_LOCATION)
                    {
                        case ENUM_ARM_ROT.HOME:
                            return;
                        case ENUM_ARM_ROT.SMP:
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, ARMSTEPS.ARM_ROT_SMP);
                            break;
                        case ENUM_ARM_ROT.RGT_IN:
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, ARMSTEPS.ARM_ROT_RGT_IN);
                            break;
                        case ENUM_ARM_ROT.RGT_OUT:
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, ARMSTEPS.ARM_ROT_RGT_OUT);
                            break;    
                        case ENUM_ARM_ROT.CUVET:
                        case ENUM_ARM_ROT.DIL_CUVET:
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, ARMSTEPS.ARM_ROT_CUV);
                            break;
                        case ENUM_ARM_ROT.ISE:
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, ARMSTEPS.ARM_ROT_ISE);
                            break;
                    }
                    break;
                case ENUM_ARM_ROT.RGT_OUT:
                    switch (ARM_NEW_LOCATION)
                    {
                        case ENUM_ARM_ROT.HOME:
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, ARMSTEPS.ARM_ROT_RGT_OUT);
                            break;
                        case ENUM_ARM_ROT.SMP:
                            steps = ARMSTEPS.ARM_ROT_RGT_OUT - ARMSTEPS.ARM_ROT_SMP;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, steps);
                            break;
                        case ENUM_ARM_ROT.RGT_OUT:
                            return;
                        case ENUM_ARM_ROT.RGT_IN:
                            steps = ARMSTEPS.ARM_ROT_RGT_IN - ARMSTEPS.ARM_ROT_RGT_OUT;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, steps);
                            return;    
                        case ENUM_ARM_ROT.CUVET:
                        case ENUM_ARM_ROT.DIL_CUVET:
                            steps = ARMSTEPS.ARM_ROT_RGT_OUT + ARMSTEPS.ARM_ROT_CUV;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, steps);
                            break;
                        case ENUM_ARM_ROT.ISE:
                            steps = ARMSTEPS.ARM_ROT_ISE - ARMSTEPS.ARM_ROT_RGT_OUT;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, steps);
                            break;
                    }
                    break;
                case ENUM_ARM_ROT.RGT_IN:
                    switch (ARM_NEW_LOCATION)
                    {
                        case ENUM_ARM_ROT.HOME:
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, ARMSTEPS.ARM_ROT_RGT_IN);
                            break;
                        case ENUM_ARM_ROT.SMP:
                            steps = ARMSTEPS.ARM_ROT_RGT_IN - ARMSTEPS.ARM_ROT_SMP;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, steps);
                            break;
                        case ENUM_ARM_ROT.RGT_IN:
                            return;
                        case ENUM_ARM_ROT.RGT_OUT:
                            steps = ARMSTEPS.ARM_ROT_RGT_IN - ARMSTEPS.ARM_ROT_RGT_OUT;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, steps);
                            return;    
                        case ENUM_ARM_ROT.CUVET:
                        case ENUM_ARM_ROT.DIL_CUVET:
                            steps = ARMSTEPS.ARM_ROT_RGT_IN + ARMSTEPS.ARM_ROT_CUV;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, steps);
                            break;
                        case ENUM_ARM_ROT.ISE:
                            steps = ARMSTEPS.ARM_ROT_ISE - ARMSTEPS.ARM_ROT_RGT_IN;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, steps);
                            break;
                    }
                    break;    
                case ENUM_ARM_ROT.SMP:
                    switch (ARM_NEW_LOCATION)
                    {
                        case ENUM_ARM_ROT.HOME:
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, ARMSTEPS.ARM_ROT_SMP);
                            break;
                        case ENUM_ARM_ROT.SMP:
                            return;
                        case ENUM_ARM_ROT.RGT_OUT:
                            steps = ARMSTEPS.ARM_ROT_RGT_OUT - ARMSTEPS.ARM_ROT_SMP;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, steps);
                            break;
                        case ENUM_ARM_ROT.RGT_IN:
                             steps = ARMSTEPS.ARM_ROT_RGT_IN - ARMSTEPS.ARM_ROT_SMP;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, steps);
                            return;    
                        case ENUM_ARM_ROT.CUVET:
                        case ENUM_ARM_ROT.DIL_CUVET:
                            steps = ARMSTEPS.ARM_ROT_SMP + ARMSTEPS.ARM_ROT_CUV;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, steps);
                            break;
                        case ENUM_ARM_ROT.ISE:
                            steps = ARMSTEPS.ARM_ROT_ISE - ARMSTEPS.ARM_ROT_SMP;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, steps);
                            break;
                    }
                    break;
                case ENUM_ARM_ROT.CUVET:
                case ENUM_ARM_ROT.DIL_CUVET:
                    switch (ARM_NEW_LOCATION)
                    {
                        case ENUM_ARM_ROT.HOME:
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, ARMSTEPS.ARM_ROT_CUV);
                            break;
                        case ENUM_ARM_ROT.SMP:
                            steps = ARMSTEPS.ARM_ROT_SMP + ARMSTEPS.ARM_ROT_CUV;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, steps);
                            break;
                        case ENUM_ARM_ROT.RGT_OUT:
                            steps = ARMSTEPS.ARM_ROT_RGT_OUT + ARMSTEPS.ARM_ROT_CUV;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, steps);
                            break;
                         case ENUM_ARM_ROT.RGT_IN:
                             steps = ARMSTEPS.ARM_ROT_RGT_IN + ARMSTEPS.ARM_ROT_CUV;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, steps);
                            return;        
                        case ENUM_ARM_ROT.CUVET:
                        case ENUM_ARM_ROT.DIL_CUVET:
                            return;
                        case ENUM_ARM_ROT.ISE:
                            steps = ARMSTEPS.ARM_ROT_ISE + ARMSTEPS.ARM_ROT_CUV;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_ANTICLOCKWISE, steps);
                            break;
                    }
                    break;
                case ENUM_ARM_ROT.ISE:
                    switch (ARM_NEW_LOCATION)
                    {
                        case ENUM_ARM_ROT.HOME:
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, ARMSTEPS.ARM_ROT_ISE);
                            break;
                        case ENUM_ARM_ROT.SMP:
                            steps = ARMSTEPS.ARM_ROT_ISE - ARMSTEPS.ARM_ROT_SMP;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, steps);
                            break;
                        case ENUM_ARM_ROT.RGT_OUT:
                            steps = ARMSTEPS.ARM_ROT_ISE - ARMSTEPS.ARM_ROT_RGT_OUT;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, steps);
                            break;
                        case ENUM_ARM_ROT.RGT_IN:
                             steps = ARMSTEPS.ARM_ROT_ISE - ARMSTEPS.ARM_ROT_RGT_IN;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, steps);
                            return;            
                        case ENUM_ARM_ROT.CUVET:
                        case ENUM_ARM_ROT.DIL_CUVET:
                            steps = ARMSTEPS.ARM_ROT_ISE + ARMSTEPS.ARM_ROT_CUV;
                            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_HOD_CLOCKWISE, steps);
                            break;
                        case ENUM_ARM_ROT.ISE:
                            return;
                    }
                    break;
            }
            ARM_HORZONTAL_POS = ARM_NEW_LOCATION;
        }

        public void ARM_UP_DOWN(ENUM_ARM_UP_DN ARM_NEW_POSITION)
        {
            if (ARM_VERTICAL_POS == ARM_NEW_POSITION)
                return;
            switch (ARM_NEW_POSITION)
            {
                case ENUM_ARM_UP_DN.DOWN:
                    SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 12, 0);
                    switch (ARM_HORZONTAL_POS)
                    {
                        case ENUM_ARM_ROT.HOME:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN, ARMSTEPS.ARM_DN_HOME);
                            break;
                        case ENUM_ARM_ROT.SMP:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN, ARMSTEPS.ARM_DN_SMP);
                            break;
                        case ENUM_ARM_ROT.RGT_IN:
                        case ENUM_ARM_ROT.RGT_OUT:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN, ARMSTEPS.ARM_DN_RGT);
                            break;
                        case ENUM_ARM_ROT.CUVET:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN, ARMSTEPS.ARM_DN_CUV);
                            break;
                        case ENUM_ARM_ROT.DIL_CUVET:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN, ARMSTEPS.ARM_DN_DIL_CUV);
                            break;
                        case ENUM_ARM_ROT.ISE:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN, ARMSTEPS.ARM_DN_ISE);
                            break;
                    }
                    break;
                case ENUM_ARM_UP_DN.UP:
                    SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 18, 0);
                    int Steps = 0;
                    switch (ARM_HORZONTAL_POS)
                    {
                        case ENUM_ARM_ROT.HOME:
                            Steps = ARMSTEPS.ARM_DN_HOME + ARMSTEPS.ARM_UP_OFFSET;
                            break;
                        case ENUM_ARM_ROT.SMP:
                            Steps = ARMSTEPS.ARM_DN_SMP + ARMSTEPS.ARM_UP_OFFSET;
                            break;
                        case ENUM_ARM_ROT.RGT_IN:
                        case ENUM_ARM_ROT.RGT_OUT:
                            Steps = ARMSTEPS.ARM_DN_RGT + ARMSTEPS.ARM_UP_OFFSET;
                            break;
                        case ENUM_ARM_ROT.CUVET:
                            Steps = ARMSTEPS.ARM_DN_CUV + ARMSTEPS.ARM_UP_OFFSET;
                            break;
                        case ENUM_ARM_ROT.DIL_CUVET:
                            Steps = ARMSTEPS.ARM_DN_DIL_CUV + ARMSTEPS.ARM_UP_OFFSET;
                            break;
                        case ENUM_ARM_ROT.ISE:
                            Steps = ARMSTEPS.ARM_DN_ISE + ARMSTEPS.ARM_UP_OFFSET;
                            break;
                    }
                    MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_UP, Steps);
                    break;
                case ENUM_ARM_UP_DN.UP_MAX:
                    SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 18, 0);
                    MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_UP, ARMSTEPS.ARM_UP_MAX);
                    break;
                case ENUM_ARM_UP_DN.LLS:
                    SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 12, 0);
                    switch (ARM_HORZONTAL_POS)
                    {
                        case ENUM_ARM_ROT.HOME:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN_LLS, ARMSTEPS.ARM_DN_HOME);
                            break;
                        case ENUM_ARM_ROT.SMP:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN_LLS, ARMSTEPS.ARM_DN_SMP);
                            break;
                        case ENUM_ARM_ROT.RGT_IN:
                        case ENUM_ARM_ROT.RGT_OUT:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN_LLS, ARMSTEPS.ARM_DN_RGT);
                            break;
                        case ENUM_ARM_ROT.CUVET:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN_LLS, ARMSTEPS.ARM_DN_CUV);
                            break;
                        case ENUM_ARM_ROT.DIL_CUVET:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN_LLS, ARMSTEPS.ARM_DN_DIL_CUV);
                            break;
                        case ENUM_ARM_ROT.ISE:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN_LLS, ARMSTEPS.ARM_DN_ISE);
                            break;
                    }
                    break;
                case ENUM_ARM_UP_DN.LLS_VOD:
                    
                    switch (ARM_HORZONTAL_POS)
                    {
                        case ENUM_ARM_ROT.HOME:
                            SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 6, 6);
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN_LLS_VOD, ARMSTEPS.ARM_DN_HOME);
                            break;
                        case ENUM_ARM_ROT.SMP:
                            SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 4, 4);
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN_LLS_VOD, ARMSTEPS.ARM_DN_SMP);
                            break;
                        case ENUM_ARM_ROT.RGT_IN:
                        case ENUM_ARM_ROT.RGT_OUT:
                            SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 6, 6);
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN_LLS_VOD, ARMSTEPS.ARM_DN_RGT);
                            break;
                        case ENUM_ARM_ROT.CUVET:
                            SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 6, 6);
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN_LLS_VOD, ARMSTEPS.ARM_DN_CUV);
                            break;
                        case ENUM_ARM_ROT.DIL_CUVET:
                            SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 4, 4);
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN_LLS_VOD, ARMSTEPS.ARM_DN_DIL_CUV);
                            break;
                        case ENUM_ARM_ROT.ISE:
                            SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 6, 6);
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN_LLS_VOD, ARMSTEPS.ARM_DN_ISE);
                            break;
                    }
                    break;
                case ENUM_ARM_UP_DN.DOWN_CUV:
                    switch (ARM_HORZONTAL_POS)
                    {
                        case ENUM_ARM_ROT.CUVET:
                            //if (MaxtureSystem == 1) SendOrder(m_Instrument[5], 2, 0X80, (H_Rec - 25) / 256, (H_Rec - 25) % 256, 0); //Light plus reagent ¹â¼ÓÊÔ¼Á
                        case ENUM_ARM_ROT.DIL_CUVET:
                            MoveMotor(ENUM_MOTOR200.ARM_UPDOWN, HEXCOMMANDS.ARM_DN, ARMSTEPS.ARM_DN_CUV - 35);    
                            break;
                    }
                    break;
            }
            ARM_VERTICAL_POS = ARM_NEW_POSITION;
        }
        public void ARM_INIT()
        {
            ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
            CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
            int steps = 0;
            switch (ARM_HORZONTAL_POS)
            {
                case ENUM_ARM_ROT.HOME:
                    //steps = 400;
                    steps =ARMSTEPS.ARM_ROT_RGT_OUT;
                    break;
                case ENUM_ARM_ROT.SMP:
                    steps = ARMSTEPS.ARM_ROT_SMP;
                    break;
                case ENUM_ARM_ROT.RGT_IN:
                    steps = ARMSTEPS.ARM_ROT_RGT_IN;
                    break;
                case ENUM_ARM_ROT.RGT_OUT:
                    steps = ARMSTEPS.ARM_ROT_RGT_OUT;
                    break;    
                case ENUM_ARM_ROT.CUVET:
                case ENUM_ARM_ROT.DIL_CUVET:
                    steps = ARMSTEPS.ARM_ROT_CUV;
                    break;
                case ENUM_ARM_ROT.ISE:
                    steps = ARMSTEPS.ARM_ROT_ISE;
                    break;
                default:
                    steps = 600;
                    break;
            }
            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_ANTICLOCKWISE, steps + 100);
            CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
            Delay_Time(5);
            MoveMotor(ENUM_MOTOR200.ARM_ROTATE, HEXCOMMANDS.ARM_ROT_OPTO_CLOCKWISE, 1500);
            CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
            ARM_HORZONTAL_POS = ENUM_ARM_ROT.HOME;
        }
        #endregion
        #region STIRRER OPERATIONS
        public void STR_INIT()
        {
            SetSpeed(ENUM_MOTOR200.STR_ROTATE, 8, 4);
            //STR_UP_DOWN(ENUM_STR_UPDN.UP_MAX);
            //CheckMotor(ENUM_MOTOR200.STR_UPDOWN);
            MoveMotor(ENUM_MOTOR200.STR_ROTATE, HEXCOMMANDS.STR_ROT_ANTICLOCKWISE, 100);     //MoveMotor(m_Instrument[12],0xC0,400); 
            //Delay_Time(10);
            CheckMotor(ENUM_MOTOR200.STR_ROTATE);
            //Delay_Time(10);
            MoveMotor(ENUM_MOTOR200.STR_ROTATE, HEXCOMMANDS.STR_ROT_CLOCKWISE, STR_STEPS.STR_ROT_CUV + 250);
            CheckMotor(ENUM_MOTOR200.STR_ROTATE);
            SetSpeed(ENUM_MOTOR200.STR_ROTATE, 20, 0);
            //Delay_Time(10);
            MoveMotor(ENUM_MOTOR200.STR_ROTATE, HEXCOMMANDS.STR_ROT_ANTICLOCKWISE, (int)(STR_STEPS.STR_ROT_CUV / 5));//MoveMotor(m_Instrument[12],0xC0,Rot_Max_z1+20); 	//MoveMotor(m_Instrument[12],0xC0,400); 
            //Delay_Time(10);
            CheckMotor(ENUM_MOTOR200.STR_ROTATE);
            //Delay_Time(10);
            MoveMotor(ENUM_MOTOR200.STR_ROTATE, HEXCOMMANDS.STR_ROT_CLOCKWISE, (int)(STR_STEPS.STR_ROT_CUV / 3));
            CheckMotor(ENUM_MOTOR200.STR_ROTATE);
        }

        public void STR_ROTATE(ENUM_STR_ROT STR_NEW_POSITION)
        {
            if (STR_HORIZONTAL_POS == STR_NEW_POSITION)
                return;
            switch (STR_NEW_POSITION)
            {
                case ENUM_STR_ROT.HOME:
                    MoveMotor(ENUM_MOTOR200.STR_ROTATE, HEXCOMMANDS.STR_ROT_CLOCKWISE, STR_STEPS.STR_ROT_CUV + 12);  //200 STR_STEPS.STR_ROT_CUV+12 is use in old ASW
                    break;
                case ENUM_STR_ROT.CUVET:
                    MoveMotor(ENUM_MOTOR200.STR_ROTATE, HEXCOMMANDS.STR_ROT_ANTICLOCKWISE, STR_STEPS.STR_ROT_CUV);
                    break;
            }
            STR_HORIZONTAL_POS = STR_NEW_POSITION;
        }

        public void STR_UP_DOWN(ENUM_STR_UPDN STR_NEW_POSITION)
        {
            int Steps = 0;
            if (STR_VERTICAL_POS == STR_NEW_POSITION)
                return;
            switch (STR_NEW_POSITION)
            {
                case ENUM_STR_UPDN.UP:
                    switch (STR_HORIZONTAL_POS)
                    {
                        case ENUM_STR_ROT.CUVET:
                            Steps = STR_STEPS.STR_DN_CUV + STR_STEPS.STR_UP_OFFSET;
                            break;
                        case ENUM_STR_ROT.HOME:
                            Steps = STR_STEPS.STR_DN_HOME + STR_STEPS.STR_UP_OFFSET;
                            break;
                    }
                    MoveMotor(ENUM_MOTOR200.STR_UPDOWN, HEXCOMMANDS.STR_UP, Steps);
                    break;
                case ENUM_STR_UPDN.DOWN:
                    switch (STR_HORIZONTAL_POS)
                    {
                        case ENUM_STR_ROT.CUVET:
                            Steps = STR_STEPS.STR_DN_CUV;
                            break;
                        case ENUM_STR_ROT.HOME:
                            Steps = STR_STEPS.STR_DN_HOME;
                            break;
                    }
                    MoveMotor(ENUM_MOTOR200.STR_UPDOWN, HEXCOMMANDS.STR_DN, Steps);
                    break;
                case ENUM_STR_UPDN.UP_MAX:
                    MoveMotor(ENUM_MOTOR200.STR_UPDOWN, HEXCOMMANDS.STR_UP, STR_STEPS.STR_UP_MAX);
                    break;
            }
            STR_VERTICAL_POS = STR_NEW_POSITION;
        }
        #endregion
        #region CRU_OPERATIONS
        public void CRU_UP_DOWN(ENUM_CRU_UPDN CRU_NEW_POSITION)
        {
            switch (CRU_NEW_POSITION)
            {
                case ENUM_CRU_UPDN.UP:
                    MoveMotor(ENUM_MOTOR200.CRU, HEXCOMMANDS.CRU_UP, CRU_STEPS.CRU_DOWN + 120);
                    break;
                case ENUM_CRU_UPDN.DOWN:
                    MoveMotor(ENUM_MOTOR200.CRU, HEXCOMMANDS.CRU_DN, CRU_STEPS.CRU_DOWN);
                    break;
                case ENUM_CRU_UPDN.DOWN_DI_DISPENSE:
                    MoveMotor(ENUM_MOTOR200.CRU, HEXCOMMANDS.CRU_DN, CRU_STEPS.CRU_DI_DISPENSE);
                    break;
                case ENUM_CRU_UPDN.DOWN_MID1:
                    MoveMotor(ENUM_MOTOR200.CRU, HEXCOMMANDS.CRU_DN_1, CRU_STEPS.CRU_MID1);
                    break;
                case ENUM_CRU_UPDN.DOWN_MID2:
                    MoveMotor(ENUM_MOTOR200.CRU, HEXCOMMANDS.CRU_DN_1, CRU_STEPS.CRU_MID2);
                    break;
                case ENUM_CRU_UPDN.UP_MAX:
                    MoveMotor(ENUM_MOTOR200.CRU, HEXCOMMANDS.CRU_UP, CRU_STEPS.CRU_UP_MAX);
                    break;
            }
        }
        #endregion
        #region RGT_OPERATIONS
        public void RGT_INIT()
        {
            SetSpeed(ENUM_MOTOR200.RGT, 10, 0);          //SetSpeed(ENUM_MOTOR200.RGT, 15, 0);
            MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_CLOCKWISE, 1600);
            CheckMotor(ENUM_MOTOR200.RGT);
            SetSpeed(ENUM_MOTOR200.RGT, 6, 0);      //SetSpeed(ENUM_MOTOR200.RGT, 8, 0);
            MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_UNKNOWN, 5333);
            CheckMotor(ENUM_MOTOR200.RGT);
            Delay_Time(10);
            SetSpeed(ENUM_MOTOR200.RGT, 8, 0);
            MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_CLOCKWISE, 0);
            CheckMotor(ENUM_MOTOR200.RGT);
            Delay_Time(20);
            SetSpeed(ENUM_MOTOR200.RGT, 23, 6);  //23,6
        }
        public void RGT_ROTATE_RG_POS_OUT(int ReagentPos) //Reagent and sample disk rotation
        {
            if (RGT_SMP_CURRENT_POS == 0 && ReagentPos == RGT_RG_CURRENT_POS) return; //Last position was not Sample & New position as same as Last
            double Pos_Count, stepd;
            int HalfRotation = Instrument_Constants.ReagentCount / 2;
            int FinalSteps;
            if (RGT_SMP_CURRENT_POS == ReagentPos)
            {
                if (RGT_Offset_Steps == 0)
                    return;
                Pos_Count = -1;
                stepd = Math.Abs(RGT_Offset_Steps);
            }
            else
            {
                if (RGT_SMP_CURRENT_POS > 0)
                    RGT_RG_CURRENT_POS = RGT_SMP_CURRENT_POS;

                Pos_Count = ReagentPos - RGT_RG_CURRENT_POS;

                if (Pos_Count > HalfRotation) Pos_Count = (Instrument_Constants.ReagentCount - ReagentPos + RGT_RG_CURRENT_POS) * (-1);
                else if (Pos_Count < (-1 * HalfRotation)) Pos_Count = Instrument_Constants.ReagentCount - RGT_RG_CURRENT_POS + ReagentPos;

                stepd = Math.Abs(Pos_Count * RGT_Single_Pos_Steps);

                if (RGT_SMP_CURRENT_POS > 0) //Adust Offset
                {
                    if (Pos_Count > 0)
                        stepd = stepd - RGT_Offset_Steps;
                    else
                        stepd = stepd + RGT_Offset_Steps;
                }

            }
            FinalSteps = Get_Round_Steps(stepd);
            if (Pos_Count > 0)
                MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_ANTICLOCKWISE, FinalSteps);
            else
                MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_CLOCKWISE, FinalSteps);
            RGT_RG_CURRENT_POS = ReagentPos;
            RGT_SMP_CURRENT_POS = 0;
        }

        public void RGT_ROTATE_RG_POS_IN(int ReagentPos) //Reagent and sample disk rotation
        {
            if (RGT_SMP_CURRENT_POS == 0 && ReagentPos == RGT_RG_CURRENT_POS) return; //Last position was not Sample & New position as same as Last
            double Pos_Count, stepd;
            int HalfRotation = Instrument_Constants.ReagentCount / 2;
            int FinalSteps;
            if (RGT_SMP_CURRENT_POS == ReagentPos)
            {
                if (RGT_Offset_Steps == 0)
                    return;
                Pos_Count = -1;
                stepd = Math.Abs(RGT_Offset_Steps);
            }
            else
            {
                if (RGT_SMP_CURRENT_POS > 0)
                    RGT_RG_CURRENT_POS = RGT_SMP_CURRENT_POS;

                Pos_Count = ReagentPos - RGT_RG_CURRENT_POS;

                if (Pos_Count > HalfRotation) Pos_Count = (Instrument_Constants.ReagentCount - ReagentPos + RGT_RG_CURRENT_POS) * (-1);
                else if (Pos_Count < (-1 * HalfRotation)) Pos_Count = Instrument_Constants.ReagentCount - RGT_RG_CURRENT_POS + ReagentPos;

                stepd = Math.Abs(Pos_Count * RGT_Single_Pos_Steps);

                if (RGT_SMP_CURRENT_POS > 0) //Adust Offset
                {
                    if (Pos_Count > 0)
                        stepd = stepd - RGT_Offset_Steps;
                    else
                        stepd = stepd + RGT_Offset_Steps;
                }

            }
            FinalSteps = Get_Round_Steps(stepd);
            if (Pos_Count > 0)
                MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_ANTICLOCKWISE, FinalSteps);
            else
                MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_CLOCKWISE, FinalSteps);
            RGT_RG_CURRENT_POS = ReagentPos;
            RGT_SMP_CURRENT_POS = 0;
        }

        public void RGT_ROTATE_SMP_POS(int SamplePos)
        {
            try
            {
                if (RGT_RG_CURRENT_POS == 0 && SamplePos == RGT_SMP_CURRENT_POS) return; //Last position was not Sample & New position as same as Last
                double Direction, stepd;//To change the step 	
                int FinalSteps;
                if (RGT_RG_CURRENT_POS == SamplePos)
                {
                    if (RGT_Offset_Steps == 0)
                        return;
                    Direction = 1;
                    stepd = Math.Abs(RGT_Offset_Steps);
                }
                else
                {
                    if (RGT_RG_CURRENT_POS > 0)
                        RGT_SMP_CURRENT_POS = RGT_RG_CURRENT_POS;

                    Direction = SamplePos - RGT_SMP_CURRENT_POS;

                    if (Direction > ((int)(Instrument_Constants.SampleCount / 2))) Direction = (Instrument_Constants.SampleCount - SamplePos + RGT_SMP_CURRENT_POS) * (-1);
                    else if (Direction < (-((int)(Instrument_Constants.SampleCount / 2)))) Direction = Instrument_Constants.SampleCount - RGT_SMP_CURRENT_POS + SamplePos;

                    stepd = Get_Round_Steps(Math.Abs(Direction * RGT_Single_Pos_Steps));

                    if (RGT_RG_CURRENT_POS > 0) //Adust Offset
                    {
                        if (Direction > 0)
                            stepd = Get_Round_Steps(Math.Abs(stepd + RGT_Offset_Steps));
                        else
                            stepd = Get_Round_Steps(Math.Abs(stepd - RGT_Offset_Steps));

                    }
                }
                FinalSteps = Get_Round_Steps(stepd);
                if (Direction > 0)
                    MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_ANTICLOCKWISE, FinalSteps);
                else
                    MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_CLOCKWISE, FinalSteps);
                RGT_SMP_CURRENT_POS = SamplePos;
                RGT_RG_CURRENT_POS = 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void RGT_ROTATE_STEPS(int Steps)
        {
            //ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
           // CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
           // MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_CLOCKWISE, 400);
          //  CheckMotor(ENUM_MOTOR200.RGT);
          //  Delay_Time(10);
          //  SetSpeed(ENUM_MOTOR200.RGT, 8, 0);
          //  MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_UNKNOWN, 4000);
          //  CheckMotor(ENUM_MOTOR200.RGT);
         //   Delay_Time(10);
            SetSpeed(ENUM_MOTOR200.RGT, 15, 0);
            MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_CLOCKWISE, Steps);
            CheckMotor(ENUM_MOTOR200.RGT);
        }

        public void RGT_INIT_FOR_BARCODE(bool IsReagentBarcode)
        {
            int iSteps;
            double CompleteCycleSteps = 5333;   //9600.00;
            if (IsReagentBarcode)
                iSteps = Get_Round_Steps(RGT_STEPS.RGT_RG_BARCODE);
            else
            {
                double OneDegreeSteps = CompleteCycleSteps / 360.0;
                double TryAngle = 0.675; // Degree//0.7
                iSteps = Get_Round_Steps(Math.Abs(RGT_STEPS.RGT_SMP_BARCODE - Math.Abs(TryAngle * OneDegreeSteps)));
            }
            RGT_ROTATE_STEPS(iSteps);
            READ_BARCODE();
        }

        public void READ_BARCODE()
        {
            SendOrder(ENUM_MOTOR200.BARCODE, 1, 1, 0X10, 0, 0);
            SendOrder(ENUM_MOTOR200.BARCODE, 0, 0, 0, 0, 0);
        }

        public void RGT_ROTATE_READ_BARCODE(int iPos, bool IsReagentBarcode)
        {
            double CompleteCycleSteps = 5333;   //9600.00;
            double OneDegreeSteps = CompleteCycleSteps / 360.0;
            double TryAngle = 0.0;
            if (IsReagentBarcode)
                TryAngle = 0.7;
            else
                TryAngle = (0.675); // Degree//0.7
            double RotDegree;
            int Final_Steps;
            if (iPos == 0)
            {
                RotDegree = Math.Abs(TryAngle * OneDegreeSteps);
            }
            else
            {
                RotDegree = Math.Abs((Math.Abs(iPos) - 1) * (TryAngle * OneDegreeSteps));
                RotDegree = Math.Abs(RGT_Single_Pos_Steps - RotDegree);
            }
            Final_Steps = Get_Round_Steps(RotDegree);
            MoveMotor(ENUM_MOTOR200.RGT, HEXCOMMANDS.RGT_ROT_ANTICLOCKWISE, Final_Steps);
            CheckMotor(ENUM_MOTOR200.RGT);
        }

        public double Get_RGT_SINGLE_POS_STEPS()
        {
            double CompleteCycleSteps = 5333;   //9600.00;
            double OnePosSteps;
            double OneDegreePos = CompleteCycleSteps / 360;
            OnePosSteps = Math.Abs((360.0 / Instrument_Constants.ReagentCount) * OneDegreePos);
            return OnePosSteps;
        }

        public int Get_Round_Steps(double Steps)
        {
            int iSteps = Convert.ToInt32(Steps * 100);
            int Remainder = iSteps % 100;
            if (iLastRoundUpSteps != 0)
            {
                iSteps = iSteps + iLastRoundUpSteps;
                iLastRoundUpSteps = iSteps % 100;
                return (int)(iSteps / 100);
            }
            else
            {
                iLastRoundUpSteps = Remainder;
                return (int)(Steps);
            }
        }

        #endregion
        #region RCT_OPERATIONS
        public void RCT_INIT()
        {
            SendOrder(ENUM_MOTOR200.RCT, 5, 0X00, 0X00, 0, 0X00);
           // SendOrder(ENUM_MOTOR200.RCT, 3, 0X47, 0X00, 0, 0X00);
           // SendOrder(ENUM_MOTOR200.RCT, 3, 0X49, 0X01, 0, 0X00);
            CheckMotor(ENUM_MOTOR200.RCT);
        }
        public void RCT_SPEED(int Speed)
        {
            SendOrder(ENUM_MOTOR200.RCT, (byte)200, (byte)(Speed / 256), (byte)(Speed % 256), 0, 0);
            CheckMotor(ENUM_MOTOR200.RCT);
        }

        public void RCT_ROTATE(int CuvetteNum)
        {
            //CuvetteNum+=50;                 //wash should start at 1 
            if(CuvetteNum>70)CuvetteNum-=70; 
            SendOrder(ENUM_MOTOR200.RCT, 3, (byte)CuvetteNum, 0, 0, 0);
        }
        #endregion
        #region SYRINGE_OPERATIONS

        public void SYRINGE_VOL_OPERATION(double VOLUME, ENUM_SYRINGE OPERATION)     //PutReg_Num
        {
            int Steps = 0;
            double AbsVol = Math.Abs(VOLUME);
            if (AbsVol > 450) AbsVol = 450;
            if (AbsVol != 0)
            {
                if (SYRINGE.SYRINGE_TYPE == ENUM_SYRINGE_TYPE.KEYTO_SYRINGE)
                    Steps = Convert.ToInt32(Instrument_Constants.Keyto_Syringe_Scale * AbsVol + 0.5);
                else
                    Steps = Convert.ToInt32(Instrument_Constants.Default_Syringe_Scale * AbsVol + 0.5);
            }
            else
            {
                if (SYRINGE.SYRINGE_TYPE == ENUM_SYRINGE_TYPE.KEYTO_SYRINGE)
                    Steps = Convert.ToInt32(Instrument_Constants.Keyto_Syringe_Scale * 400);
                else
                    Steps = Convert.ToInt32(Instrument_Constants.Default_Syringe_Scale * 400);
            }

            SET_SYRINGE_SPEED(AbsVol, OPERATION);
            if (VOLUME == 0)
            {
                MoveMotor(ENUM_MOTOR200.SYRINGE, HEXCOMMANDS.SYRINGE_OPTO, Steps);
            }
            else
            {
                switch (OPERATION)
                {
                    case ENUM_SYRINGE.ASPIRATE:
                        MoveMotor(ENUM_MOTOR200.SYRINGE, HEXCOMMANDS.SYRINGE_ASPIRATE, Steps);
                        break;
                    case ENUM_SYRINGE.DISPENSE:
                        MoveMotor(ENUM_MOTOR200.SYRINGE, HEXCOMMANDS.SYRINGE_DISPENSE, Steps);
                        break;
                        /*case ENUM_SYRINGE.OPTO:
                            MoveMotor(ENUM_MOTOR200.SYRINGE, HEXCOMMANDS.SYRINGE_OPTO, Steps);
                            break;*/
                }
            }
            //CheckMotor(ENUM_MOTOR200.SYRINGE);            
        }

        public void SYRINGE_STEP_OPERATION(int Steps)           //PutReg_Num_Adjust
        {
            if (SYRINGE.SYRINGE_TYPE == ENUM_SYRINGE_TYPE.KEYTO_SYRINGE)
                SetSpeed(ENUM_MOTOR200.SYRINGE, (byte)28, (byte)5);
            else
                SetSpeed(ENUM_MOTOR200.SYRINGE, (byte)13, (byte)13);
            if (Steps != 0)
            {
                if (Steps > 0)
                {
                    MoveMotor(ENUM_MOTOR200.SYRINGE, HEXCOMMANDS.SYRINGE_ASPIRATE, Steps);
                }
                else MoveMotor(ENUM_MOTOR200.SYRINGE, HEXCOMMANDS.SYRINGE_DISPENSE, Math.Abs(Steps));
            }
            else
            {
                Steps = 1600;
                MoveMotor(ENUM_MOTOR200.SYRINGE, HEXCOMMANDS.SYRINGE_OPTO, Steps);
            }
            CheckMotor(ENUM_MOTOR200.SYRINGE);
        }

        public void SYRINGE_OPTO_SWITCH(int Steps)      //PutReg_Num_Switch
        {
            if (SYRINGE.SYRINGE_TYPE == ENUM_SYRINGE_TYPE.KEYTO_SYRINGE)
                SetSpeed(ENUM_MOTOR200.SYRINGE, (byte)28, (byte)5);
            else
                SetSpeed(ENUM_MOTOR200.SYRINGE, (byte)13, (byte)13);
            MoveMotor(ENUM_MOTOR200.SYRINGE, HEXCOMMANDS.SYRINGE_SWITCH, Steps);
            CheckMotor(ENUM_MOTOR200.SYRINGE);
        }
        public void SYRINGE_OFFSET_CAL()
        {
            Set_Step = 500;
            SYRINGE_STEP_OPERATION(Set_Step);
            Delay_Time(50);
            SYRINGE_STEP_OPERATION(0);
            Delay_Time(50);
            SYRINGE_OPTO_SWITCH(Set_Step);
        }
        public void SET_SYRINGE_SPEED(double VOLUME, ENUM_SYRINGE OPERATION)
        {
            short SYR_SPEED1 = 0, SYR_SPEED2 = 0;
            if (SYRINGE.SYRINGE_TYPE == ENUM_SYRINGE_TYPE.KEYTO_SYRINGE)
            {
                if (VOLUME == 0)
                {
                    SYR_SPEED1 = 10;
                    SYR_SPEED2 = 8;
                }
                else if (OPERATION == ENUM_SYRINGE.DISPENSE)
                {
                    if (VOLUME <= 25)
                    {
                        SYR_SPEED1 = 10;
                        SYR_SPEED2 = 8;
                    }
                    else
                    {
                        SYR_SPEED1 = 28;
                        SYR_SPEED2 = 8;
                    }
                }
                else if (OPERATION == ENUM_SYRINGE.ASPIRATE)
                {
                    if (VOLUME <= 25)
                    {
                        SYR_SPEED1 = 10;
                        SYR_SPEED2 = 5;
                    }
                    else
                    {
                        SYR_SPEED1 = 28;
                        SYR_SPEED2 = 5;
                    }
                }

            }
            else
            {
                if (OPERATION == ENUM_SYRINGE.DISPENSE)
                {
                    if (VOLUME > 0 && VOLUME <= 6.5)
                    {
                        SYR_SPEED1 = 13;
                        SYR_SPEED2 = 13;
                    }
                    else if (VOLUME > 6.5 && VOLUME <= 70)
                    {
                        SYR_SPEED1 = 13;
                        SYR_SPEED2 = 13;
                    }
                    else
                    {
                        SYR_SPEED1 = 24;
                        SYR_SPEED2 = 10;
                    }
                }
                else
                {
                    if (VOLUME > 70)
                    {
                        SYR_SPEED1 = 24;
                        SYR_SPEED2 = 10;
                    }
                    else
                    {
                        if (VOLUME >= 12.5)
                        {
                            SYR_SPEED1 = 13;
                            SYR_SPEED2 = 13;
                        }
                        else
                        {
                            SYR_SPEED1 = 13;
                            SYR_SPEED2 = 13;
                        }
                    }
                }
            }
            CheckMotor(ENUM_MOTOR200.SYRINGE);
            //SetSpeed(ENUM_MOTOR200.SYRINGE, (byte)SYR_SPEED1, (byte)SYR_SPEED2);
            SendOrder(ENUM_MOTOR200.SYRINGE, 1, (byte)SYR_SPEED1, (byte)SYR_SPEED2, 0, 0);    //SET SYRINGE SPEED
        }

        #endregion
        #region INIT_INSTRUMENT
        public void INIT_INSTRUMENT()
        {
            RGT_RG_CURRENT_POS = 0;
            RGT_SMP_CURRENT_POS = 1;
            iLastRoundUpSteps = 0;
            if (SYRINGE.SYRINGE_TYPE == ENUM_SYRINGE_TYPE.KEYTO_SYRINGE)    //Set syringe type in ESW
                SendOrder(ENUM_MOTOR200.SYRINGE, 4, HEXCOMMANDS.SYRINGE_ESW_KEYTO, 0, 0, 0);
            else
                SendOrder(ENUM_MOTOR200.SYRINGE, 4, HEXCOMMANDS.SYRINGE_ESW_MERIL, 0, 0, 0);
            SetSpeed(ENUM_MOTOR200.CRU, 8, 0);
            if (SYRINGE.SYRINGE_TYPE == ENUM_SYRINGE_TYPE.KEYTO_SYRINGE)
                SetSpeed(ENUM_MOTOR200.SYRINGE, 28, 5);
            else
                SetSpeed(ENUM_MOTOR200.SYRINGE, 12, 12);
            SetSpeed(ENUM_MOTOR200.RGT, 15, 0);
            if (Instrument_Constants.ConnectedInstrument == ENUM_INSTRUMENTS.Nano200)
            {
                RCT_SPEED(200);
                SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 20, 0);
                SetSpeed(ENUM_MOTOR200.ARM_ROTATE, 20, 0);//SetSpeed(ENUM_MOTOR200.ARM_ROTATE, 20, 0);
            }
            else
            {
                RCT_SPEED(500);
                SetSpeed(ENUM_MOTOR200.ARM_UPDOWN, 8, 0);          
                SetSpeed(ENUM_MOTOR200.ARM_ROTATE, 8, 0);

            }

            SetSpeed(ENUM_MOTOR200.STR_ROTATE, 20, 0);
            SetSpeed(ENUM_MOTOR200.STR_UPDOWN, 20, 0);
            ALL_ARM_UP();
            
            DI_BACK_PUMP(ON_OFF.OFF, 500);
            TROUGH_VALVE(ON_OFF.OFF, 500);
            INTERNAL_PROBE_WASH(ON_OFF.OFF, 500);
            STR_INIT();
            SYRINGE_VOL_OPERATION(20, ENUM_SYRINGE.ASPIRATE);
            ARM_INIT();
            SYRINGE_VOL_OPERATION(0, ENUM_SYRINGE.OPTO);
            
            RGT_INIT();
            RCT_INIT();
               
        }
        #endregion
        #region VALVES_PUMP
        /// <summary>
        /// Command Fuction for turning Internal Probe Wash On or OFF
        /// </summary>
        /// <param name="OPERATION">ENUM VALVES_PUMPS_OPERATION is equal ON or OFF</param>
        /// <param name="Time">Time for Which Valve should be ON;If Time =0 Then Valve will be continuusly ON until Stopped</param>
        public void INTERNAL_PROBE_WASH(ON_OFF OPERATION, int Time)
        {
            if (OPERATION == ON_OFF.OFF)
                Time = 0;
            SendOrder(ENUM_MOTOR200.RCT, HEXCOMMANDS.VALVES_PUMPS, (byte)VALVES_PUMPS.INTERNAL_PROBE_VALVE, (byte)OPERATION, (byte)(Time / 256), (byte)(Time % 256));
        }

        /// <summary>
        /// Command Fuction for turning Trough Valve On or OFF
        /// </summary>
        /// <param name="OPERATION">ENUM VALVES_PUMPS_OPERATION is equal ON or OFF</param>
        /// <param name="Time">Time for Which Valve should be ON;If Time =0 Then Valve will be continuusly ON until Stopped</param>
        public void TROUGH_VALVE(ON_OFF OPERATION, int Time)
        {
            if (OPERATION == ON_OFF.OFF)
                Time = 0;
            SendOrder(ENUM_MOTOR200.RCT, HEXCOMMANDS.VALVES_PUMPS, (byte)VALVES_PUMPS.TROUGH_VALVE, (byte)OPERATION, (byte)(Time / 256), (byte)(Time % 256));
        }

        public void CRU_DISPENSE_PUMP(ON_OFF OPERATION, int Time)
        {
            if (OPERATION == ON_OFF.OFF)
                Time = 0;
            SendOrder(ENUM_MOTOR200.RCT, HEXCOMMANDS.VALVES_PUMPS, (byte)VALVES_PUMPS.CRU_DISPENSE_PUMP, (byte)OPERATION, (byte)(Time / 256), (byte)(Time % 256));
        }
        /// <summary>
        /// Command Fuction for turning Cleaning Pump On or OFF
        /// </summary>
        /// <param name="OPERATION">ENUM VALVES_PUMPS_OPERATION is equal ON or OFF</param>
        /// <param name="Time">Time for Which Valve should be ON;If Time =0 Then Valve will be continuusly ON until Stopped</param>
        public void CLEANING_PUMP(ON_OFF OPERATION, int Time)
        {
            if (OPERATION == ON_OFF.OFF)
                Time = 0;
            SendOrder(ENUM_MOTOR200.RCT, HEXCOMMANDS.VALVES_PUMPS, (byte)VALVES_PUMPS.CRU_CLEANING_PUMP, (byte)OPERATION, (byte)(Time / 256), (byte)(Time % 256));
        }
        /// <summary>
        /// Command Fuction for turning Cleaning Pump On or OFF
        /// </summary>
        /// <param name="OPERATION">ENUM VALVES_PUMPS_OPERATION is equal ON or OFF</param>
        /// <param name="Time">Time for Which Valve should be ON;If Time =0 Then Valve will be continuusly ON until Stopped</param>
        public void DI_BACK_PUMP(ON_OFF OPERATION, int Time)
        {
            if (OPERATION == ON_OFF.OFF)
                Time = 0;
            SendOrder(ENUM_MOTOR200.RCT, HEXCOMMANDS.VALVES_PUMPS, (byte)VALVES_PUMPS.CRU_DI_BACK_PUMP, (byte)OPERATION, (byte)(Time / 256), (byte)(Time % 256));
        }
        /// <summary>
        /// Command Fuction for turning Stirrer Revove Motor On or OFF
        /// </summary>
        /// <param name="OPERATION">ENUM VALVES_PUMPS_OPERATION is equal ON or OFF</param>
        /// <param name="Time">Time for Which Valve should be ON;If Time =0 Then Valve will be continuusly ON until Stopped</param>
        /// <param name="Speed">Revolutions per second</param>
        public void STIRRER_REVOLVE(ON_OFF OPERATION, int Time, int Speed)
        {
            /*if (m_StrHighSpeed)
            {

                Speed = 1500;
                m_StrHighSpeed = 0;
            }
            else*/
            if (Speed == 0) Speed = 1500;
            int V_leval = Speed;
            if (OPERATION == ON_OFF.OFF)
            {
                Time = 0;
                Speed = 0;
            }
            SendOrder(ENUM_MOTOR200.RCT, HEXCOMMANDS.VALVES_PUMPS, (byte)VALVES_PUMPS.STIRRER_REVOLVE, (byte)(Speed / 256), (byte)(Speed % 256), (byte)Time);
        }

        public void SENSOR_CHECK()
        {
            SendOrder(ENUM_MOTOR200.RCT, HEXCOMMANDS.SENSOR_CHECK, 00, 0, 0, 0);
            Delay_Time(50);
            SendOrder(ENUM_MOTOR200.RCT, HEXCOMMANDS.SENSOR_CHECK, 00, 0, 0, 0);
            Delay_Time(50);
            SendEnd();
        }

        public void READ_TEMP_PRESSURE(ENUM_MOTOR_STATE ReadState)
        {
            SendOrder(ENUM_MOTOR200.RCT, (byte)ReadState, 0, 0, 0, 0);
        }

        public void LAMP_ON_OFF(ON_OFF OPERATION)
        {
            SendOrder(ENUM_MOTOR200.RCT, HEXCOMMANDS.VALVES_PUMPS, (byte)VALVES_PUMPS.LAMP, (byte)OPERATION, 0, 0);
            INST_STATUS.LAMP_STATUS = OPERATION;
        }
        #endregion
        #region PROCESS RECEIVED DATA FROM INSTRUMENT
        byte[] tempBytes;
        bool IsNewData = false;
        public void ProcessRawData(byte[] RecievedBytes)
        {
            try
            {
                if (RecievedBytes == null)
                    return;
                if (tempBytes == null || IsNewData == true)
                {
                    tempBytes = new byte[RecievedBytes.Count()];
                    //LogRecievedData("Temp Start");
                    tempBytes = RecievedBytes;
                    IsNewData = false;
                    //return;
                }
                else
                {
                    IsNewData = false;
                    List<byte> list1 = new List<byte>(tempBytes);
                    List<byte> list2 = new List<byte>(RecievedBytes);
                    list1.AddRange(list2);
                    tempBytes = new byte[tempBytes.Count() + RecievedBytes.Count() + 1];
                    tempBytes = list1.ToArray();
                }
                if (tempBytes.Count() >= 52)
                {
                    int FunctPos = Array.LastIndexOf<byte>(tempBytes, (byte)ENUM_INSTRUMENT_STATE.TASK_COMPLETED);

                    if (FunctPos > 26 && FunctPos == tempBytes.Count() - 7)
                    {
                        if (tempBytes[FunctPos - (2 * 8 + 3)] == (byte)ENUM_PROTOCOLHEADER.RECIEVE_HEADER_1 && tempBytes[FunctPos - (2 * 8 + 2)] == (byte)ENUM_PROTOCOLHEADER.RECIEVE_HEADER_2)
                        {
                            iBitNum = 0;
                            byte[] sf = new byte[26];
                            int k = 0;
                            for (int j = 0; j < tempBytes.Count(); j++)
                            {
                                sf[k] = tempBytes[j];
                                if (k == 25)
                                {
                                    ProcessRecievedData(sf);
                                    k = 0;
                                }
                                else
                                    k++;
                            }
                            //INST_STATUS.IS_INST_BUSY = false;
                            // LogRecievedData("Temp END");
                            IsNewData = true;
                        }
                    }

                    /*
                    for (int i = 0; i < tempBytes.Count(); i++)
                    {
                        if (tempBytes[i] == (byte)ENUM_INSTRUMENT_STATE.TASK_COMPLETED || tempBytes[i] == (byte)ENUM_INSTRUMENT_STATE.COMMUNICATION_FAILURE)
                        {
                            //LogRecievedData(BitConverter.ToString(tempBytes));
                            if (i < 19) return;
                            try
                            {
                                if (tempBytes[i - (2 * 8 + 3)] == (byte)ENUM_PROTOCOLHEADER.RECIEVE_HEADER_1 && tempBytes[i - (2 * 8 + 2)] == (byte)ENUM_PROTOCOLHEADER.RECIEVE_HEADER_2)
                                {
                                    //LogRecievedData(BitConverter.ToString(tempBytes));
                                    iBitNum = 0;
                                    for (int j = 0; j < tempBytes.Count(); j++)
                                    {
                                        ProcessRecievedData(tempBytes[j]);
                                    }
                                    LogRecievedData("Temp END");
                                    IsNewData = true;
                                    //INST_STATUS.IS_INST_BUSY = false;
                                }
                            }
                            catch (Exception ex)
                            {
                                LogRecievedData(ex.ToString());
                            }
                        }
                    }*/
                }
            }
            catch (Exception ex)
            {
                LogRecievedData(ex.ToString());
            }

        }
        public void ProcessRecievedData(byte[] RecievedBytes)
        {
            try
            {
                LogRecievedData(BitConverter.ToString(RecievedBytes));
                //for (int i = 0; i < RecievedBytes.Count(); i++)

                {
                    /* if (RecievedBytes == (byte)ENUM_PROTOCOLHEADER.RECIEVE_HEADER_1 && iBitNum==0)
                     {
                         iBitNum = 1;
                         m_Temp = RecievedBytes;
                         //LogRecievedData("START");
                     }
                     if (m_Temp== (byte)ENUM_PROTOCOLHEADER.RECIEVE_HEADER_1 && RecievedBytes == (byte)ENUM_PROTOCOLHEADER.RECIEVE_HEADER_2 && iBitNum==1)
                     {
                         iIndex = 0;
                         iBitNum = 2;
                         m_Data_Set = new byte[30];
                     }
                     else m_Temp = RecievedBytes;
                     if (iBitNum == 2)*/
                    {
                        m_Data_Set = RecievedBytes;
                        //if (iIndex >= 2 * Instrument_Constants.FilterCount + 7)
                        {
                            iIndex = 0;
                            iBitNum = 0;
                            int iCuvetNumPos = 2;
                            int iFunctionCMDPos = 2 * 8 + 3;
                            //LogRecievedData(BitConverter.ToString(m_Data_Set));
                            if (Is_Read_ADC == true)
                            {
                                if (m_Data_Set[iCuvetNumPos] >= 1 && m_Data_Set[iCuvetNumPos] <= Instrument_Constants.CuvetCount)
                                {
                                    INST_STATUS.iReadCuvCount = m_Data_Set[iCuvetNumPos];

                                    for (int j = 0; j < Instrument_Constants.FilterCount; j++)
                                    {
                                        float tempData;
                                        if (j < 8) tempData = (float)(m_Data_Set[iCuvetNumPos + 2 * j + 1] * 256 + m_Data_Set[iCuvetNumPos + 2 * j + 2]);
                                        else tempData = (float)(m_Data_Set[iCuvetNumPos + 2 * j + 1 + 5] * 256 + m_Data_Set[iCuvetNumPos + 2 * j + 2 + 5]);
                                        if (INST_STATUS.WaterBlankCheck[m_Data_Set[iCuvetNumPos] - 1] == true && INST_STATUS.ADCData[m_Data_Set[iCuvetNumPos] - 1, j] < tempData)
                                            INST_STATUS.ADCData[m_Data_Set[iCuvetNumPos] - 1, j] = tempData;
                                        if (INST_STATUS.ADCData[m_Data_Set[iCuvetNumPos] - 1, j] >= 65535) continue;
                                    }
                                }
                            }
                            if(m_Data_Set[iCuvetNumPos]==(byte)ENUM_MOTOR200.ISE)
                            {
                                ISE_Received = new byte[m_Data_Set.Count()-(iCuvetNumPos+1)];
                                Array.Copy(m_Data_Set, (iCuvetNumPos + 1), ISE_Received,0, ISE_Received.Count());
                                ProcessISE_Data(ISE_Received);
                                return;
                            }
                            if (m_Data_Set[iFunctionCMDPos] == (char)ENUM_INSTRUMENT_STATE.NORMAL_PACKET)              //0XAA Motor No. Function No. High Byte Low Byte 0XAA µç»úºÅ ¹¦ÄÜºÅ ¸ß×Ö½Ú µÍ×Ö½Ú		
                            {
                                byte m_Motor = (byte)m_Data_Set[iFunctionCMDPos + 1];
                                byte State = (byte)m_Data_Set[iFunctionCMDPos + 2];
                                byte A = (byte)m_Data_Set[iFunctionCMDPos + 3];
                                byte B = (byte)m_Data_Set[iFunctionCMDPos + 4];
                                int Step = A * 256 + B;
                                ENUM_MOTOR200 Motor = (ENUM_MOTOR200)m_Motor;
                                switch (Motor)
                                {
                                    case ENUM_MOTOR200.ARM_ROTATE:
                                        if (State <= (byte)ENUM_MOTOR_STATE.HOD_VOD)
                                            INST_STATUS.IS_ARM_HOD = true;
                                        break;
                                    case ENUM_MOTOR200.ARM_UPDOWN:
                                        if (State <= (byte)ENUM_MOTOR_STATE.HOD_VOD)
                                            INST_STATUS.IS_ARM_VOD = true;
                                        break;
                                    case ENUM_MOTOR200.CRU:
                                        break;
                                    case ENUM_MOTOR200.RCT:
                                        switch(State)
                                        {
                                            case (byte)ENUM_MOTOR_STATE.ALARM:
                                                if (B == 1)
                                                    INST_STATUS.IS_DI_EMPTY = true;
                                                else
                                                    INST_STATUS.IS_DI_EMPTY = false;
                                                if (A == 1)
                                                    INST_STATUS.IS_WASTE_FULL = true;
                                                else
                                                    INST_STATUS.IS_WASTE_FULL = false;
                                                break;
                                            case (byte)ENUM_MOTOR_STATE.RCT_TEMPERATURE:
                                                INST_STATUS.RCT_TEMP = Step * 0.1;
                                                break;
                                            case (byte)ENUM_MOTOR_STATE.RGT_TEMPERATURE:
                                                INST_STATUS.RGT_TEMP = Step * 0.1;
                                                break;
                                            case (byte)ENUM_MOTOR_STATE.PRESSURE:
                                                INST_STATUS.PRESSURE = Step/ 10.0;
                                                break;
                                            case (byte)ENUM_MOTOR_STATE.READ_SET_PARAM:
                                                if (ReadIndex == 0)
                                                {
                                                    double Tg;
                                                    byte A1 = m_Data_Set[iFunctionCMDPos + 3], B1;
                                                    B1 = (byte)(A1 & (byte)0X7F);
                                                    Tg = (B1 * 256 + m_Data_Set[iFunctionCMDPos + 4]) * 0.03125;
                                                    if ((A1 & (byte)0X80)>0) Tg = Tg * (-1);
                                                    INST_STATUS.Correction_Coefficient = Tg;
                                                    ReadIndex++;
                                                }
                                                else if (ReadIndex == 1)
                                                {
                                                    INST_STATUS.TARGET_TEMP = Step / 10.0;
                                                    ReadIndex++;
                                                }
                                                else if (ReadIndex == 2)
                                                {
                                                    INST_STATUS.TARGET_PRESSURE = Step / 10.0;
                                                    ReadIndex++;
                                                }
                                                break;
                                            case (byte)ENUM_MOTOR_STATE.HOD_VOD:
                                                break;
                                        }
                                        break;
                                    case ENUM_MOTOR200.RGT:
                                        break;
                                    case ENUM_MOTOR200.STR_ROTATE:
                                        break;
                                    case ENUM_MOTOR200.STR_UPDOWN:
                                        break;
                                    case ENUM_MOTOR200.SYRINGE:
                                        if (State == 7) iStart = 0;
                                        else
                                        {
                                            if (iStart == 0)
                                            {
                                                iStart = -1;
                                                int m_Motor_Step = Set_Step - (A * 256 + B) - 30;       //short m_Motor_Step = Set_Step-short(m_GetChar[2*8+4]*256+m_GetChar[2*8+5])-24;
                                                if (m_Motor_Step < 0) m_Motor_Step = 0;
                                                SYRINGE.SYRINGE_OFFSET = m_Motor_Step;
                                            }
                                        }
                                        break;
                                    case ENUM_MOTOR200.BARCODE:
                                        {
                                            char[] t_Barcode = new char[30];
                                            int length = State - 8, Adju_Value = 0;
                                            if (A == 0XFF && B == 0XFF)
                                            {
                                                INST_STATUS.CURRENT_BARCODE = "BARCODE ERROR1";
                                            }
                                            else
                                            {

                                                for (int iCharPos = 0; iCharPos < length; iCharPos++)
                                                {
                                                    if (iCharPos < 16)
                                                    {
                                                        t_Barcode[iCharPos] = Convert.ToChar(m_Data_Set[iCharPos + 2 + 1]);
                                                        Adju_Value += Convert.ToInt32(m_Data_Set[iCharPos + 2 + 1]);
                                                    }
                                                    else
                                                    {
                                                        t_Barcode[iCharPos] = Convert.ToChar(m_Data_Set[iCharPos + 2 + 6]);
                                                        Adju_Value += Convert.ToInt32(m_Data_Set[iCharPos + 2 + 6]);
                                                    }
                                                }
                                                if (Adju_Value == A * 256 + B) INST_STATUS.CURRENT_BARCODE = new string(t_Barcode, 0, length);
                                                else INST_STATUS.CURRENT_BARCODE = "BARCODE ERROR2";
                                            }
                                        }
                                        break;
                                }

                            }
                            else if (m_Data_Set[iFunctionCMDPos] == (char)ENUM_INSTRUMENT_STATE.TASK_COMPLETED)
                            {
                                if (Is_ADValue_Detection)
                                {
                                    for (int j = 0; j < Instrument_Constants.FilterCount; j++)
                                    {
                                        float tempData;
                                        if (j < 8) tempData = (float)(m_Data_Set[iCuvetNumPos + 2 * j + 1] * 256 + m_Data_Set[iCuvetNumPos + 2 * j + 2]);
                                        else tempData = (float)(m_Data_Set[iCuvetNumPos + 2 * j + 1 + 5] * 256 + m_Data_Set[iCuvetNumPos + 2 * j + 2 + 5]);
                                        INST_STATUS.ADCData[0, j] = tempData;
                                        if (INST_STATUS.ADCData[0, j] >= 65535) continue;
                                    }
                                }

                                if (Is_Syringe_Maintain == true)
                                {
                                    SYRINGE_VOL_OPERATION(0, ENUM_SYRINGE.OPTO);
                                    INTERNAL_PROBE_WASH(ON_OFF.ON, 0);
                                    CheckMotor(ENUM_MOTOR200.SYRINGE);
                                    INTERNAL_PROBE_WASH(ON_OFF.OFF, 0);
                                    SYRINGE_VOL_OPERATION(350, ENUM_SYRINGE.ASPIRATE);
                                    SYRINGE_VOL_OPERATION(0, ENUM_SYRINGE.OPTO);
                                    INTERNAL_PROBE_WASH(ON_OFF.ON, 0);
                                    CheckMotor(ENUM_MOTOR200.SYRINGE);
                                    INTERNAL_PROBE_WASH(ON_OFF.OFF, 0);
                                    SendEnd();
                                }
                                INST_STATUS.IS_INST_BUSY = false;
                            }
                            else if (m_Data_Set[iFunctionCMDPos] == (char)ENUM_INSTRUMENT_STATE.COMMUNICATION_FAILURE)
                            {
                                INST_STATUS.IS_COM_ERROR = true;
                                INST_STATUS.ERROR_MOTOR_NUM = (ENUM_MOTOR200)m_Data_Set[iFunctionCMDPos + 1];
                            }
                            else if (m_Data_Set[iFunctionCMDPos] == (char)ENUM_INSTRUMENT_STATE.INVALID_DATA)
                            {
                                //SEND ORDER AGAIN TO BE IMPLEMENTED
                                SendOrderAgain();
                            }
                            if (m_Data_Set[iFunctionCMDPos] == (char)ENUM_INSTRUMENT_STATE.INVALID_BARCODE_DATA)
                            {  
                                byte m_Motor = (byte)m_Data_Set[iFunctionCMDPos + 1];
                                byte State = (byte)m_Data_Set[iFunctionCMDPos + 2];
                                byte A = (byte)m_Data_Set[iFunctionCMDPos + 3];
                                byte B = (byte)m_Data_Set[iFunctionCMDPos + 4];
                                int Step = A * 256 + B;
                                ENUM_MOTOR200 Motor = (ENUM_MOTOR200)m_Motor;
                                if (Motor == ENUM_MOTOR200.BARCODE)
                                {
                                    char[] t_Barcode = new char[30];
                                    int length = State - 8, Adju_Value = 0;
                                    if (A == 0XFF && B == 0XFF)
                                    {
                                        INST_STATUS.CURRENT_BARCODE = "BARCODE ERROR1";
                                    }
                                    else
                                    {

                                        for (int iCharPos = 0; iCharPos < length; iCharPos++)
                                        {
                                            if (iCharPos < 16)
                                            {
                                                t_Barcode[iCharPos] = Convert.ToChar(m_Data_Set[iCharPos + 2 + 1]);
                                                Adju_Value += Convert.ToInt32(m_Data_Set[iCharPos + 2 + 1]);
                                            }
                                            else
                                            {
                                                t_Barcode[iCharPos] = Convert.ToChar(m_Data_Set[iCharPos + 2 + 6]);
                                                Adju_Value += Convert.ToInt32(m_Data_Set[iCharPos + 2 + 6]);
                                            }
                                        }
                                        if (Adju_Value == A * 256 + B) INST_STATUS.CURRENT_BARCODE = new string(t_Barcode, 0, length);
                                        else INST_STATUS.CURRENT_BARCODE = "BARCODE ERROR2";
                                    }
                                }
                                //INST_STATUS.IS_INST_BUSY = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //INST_STATUS.IS_INST_BUSY = false;
                LogRecievedData(ex.ToString());
            }
        }

        public void ProcessISE_Data(byte[] ReceivedData)
        {
            byte STX = 0x02;
            byte ETX = 0x04;
            ISE_TASKCOMPLETED = new ENUM_ISE_CMD();
            string strTaskCompleted = string.Empty;
            string strError = string.Empty;
            if(ReceivedData[0]==STX)
            {
                ISE_RESULTS = new byte[ReceivedData.Count()];
                ISE_RESULTS = ReceivedData;
            }
            else
            {
                List<byte> list1 = new List<byte>(ISE_RESULTS);
                List<byte> list2 = new List<byte>(ReceivedData);
                list1.AddRange(list2);
                ISE_RESULTS = new byte[tempBytes.Count() + ReceivedData.Count() + 1];
                ISE_RESULTS = list1.ToArray();

            }
            bool IsPacketReceived = false;
            for (int iCnt=0;iCnt< ISE_RESULTS.Count();iCnt++)
            {
                if (ISE_RESULTS[iCnt] == ETX)
                {
                    IsPacketReceived = true; 
                }
            }
            if (IsPacketReceived == true)
            {
                char[] tempch = new char[ISE_RESULTS.Count()];

                tempch = System.Text.Encoding.ASCII.GetString(ISE_RESULTS).ToCharArray();

                string tempstr = new string(tempch);

                int ReceiveCommand = Convert.ToInt32(tempstr.Substring(1, 3));

                if (ReceiveCommand == (int)ENUM_ISE_CMD.WAIT_FOR_SMP)
                {
                    Is_ISE_ReadyForSample = true;
                }
                else if (ReceiveCommand == (int)ENUM_ISE_CMD.ACK)
                {
                    //	SetDlgItemText(lbl_Error,"ACK");

                }
                else if (ReceiveCommand == (int)ENUM_ISE_CMD.TASK_COMPLETED)
                {
                    strTaskCompleted = tempstr.Substring(7, 3);
                    strError = tempstr.Substring(10, 3);
                    // IsError = ProcessErrors(strError);
                    if(Convert.ToInt32(strError)>0 && Convert.ToInt32(strError)<34)
                        ISE_CURRENT_ERROR =(ENUM_ISE_ERROR)Convert.ToInt32(strError);
                    ISE_TASKCOMPLETED = (ENUM_ISE_CMD)Convert.ToInt32(strTaskCompleted);
                }
            }
            
        }
        #endregion
        #region OtherOperations
        public void ALL_ARM_UP()
        {
            ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
            CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
            STR_UP_DOWN(ENUM_STR_UPDN.UP_MAX);
            CheckMotor(ENUM_MOTOR200.STR_UPDOWN);
            CRU_UP_DOWN(ENUM_CRU_UPDN.UP_MAX);
            CheckMotor(ENUM_MOTOR200.CRU);
        }

        public void WASH_PROBE()
        {
            CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
            CheckMotor(ENUM_MOTOR200.SYRINGE);
            ARM_UP_DOWN(ENUM_ARM_UP_DN.DOWN);
            CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
            CheckMotor(ENUM_MOTOR200.CRU);
            INTERNAL_PROBE_WASH(ON_OFF.ON, 0);
            TROUGH_VALVE(ON_OFF.ON, 0);
            SYRINGE_VOL_OPERATION(0, ENUM_SYRINGE.OPTO);
            CheckMotor(ENUM_MOTOR200.SYRINGE);
            Delay_Time(20 + SYRINGE.SYRINGE_WASHTIME);
            TROUGH_VALVE(ON_OFF.OFF, 0);
            INTERNAL_PROBE_WASH(ON_OFF.OFF, 0);
            Delay_Time(20);
            ARM_UP_DOWN(ENUM_ARM_UP_DN.UP);
            CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
            CheckMotor(ENUM_MOTOR200.CRU);
            SYRINGE_VOL_OPERATION(3, ENUM_SYRINGE.ASPIRATE);
            CheckMotor(ENUM_MOTOR200.SYRINGE);
        }

        public void GetCRUHands(int StartNum)
        {
            for (int iCnt = 0; iCnt < CRU_STEPS.CRU_WASH_HANDS; iCnt++)
            {
                Instrument_Constants.CRUHands[iCnt] = StartNum++;
                if (StartNum > Instrument_Constants.CuvetCount) StartNum = 1;
            }
        }
        public void ReadData()
        {
            SendOrder(0, 4, 0, 0, 0, 0);
        }
        public void CheckReagentRemain()
        {
            CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
            Delay_Time(3);
            CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
            Delay_Time(3);
            CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
            Delay_Time(3);
            CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
            Delay_Time(3);
            CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
            Delay_Time(3);
            CheckMotor(ENUM_MOTOR200.SYRINGE);
        }
        public void Probe_Wash()
        {
            //objControlOrder.H_Sam = H_Sam;
            ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
            Delay_Time(10);
            RGT_ROTATE_SMP_POS(1);
            CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
            ARM_ROTATE(ENUM_ARM_ROT.SMP);
            Delay_Time(10);
            CheckMotor(ENUM_MOTOR200.RGT);
            CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
            ARM_UP_DOWN(ENUM_ARM_UP_DN.LLS);        //m_Port->NOD_SAM(2); ////MT,16.Nov.2016 ,HOD/VOD Functionality 
            CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
            SYRINGE_VOL_OPERATION(50, ENUM_SYRINGE.ASPIRATE);
            Delay_Time(40);
            CheckMotor(ENUM_MOTOR200.SYRINGE);
            ARM_UP_DOWN(ENUM_ARM_UP_DN.UP);
            CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
            ARM_ROTATE(ENUM_ARM_ROT.HOME);
            CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
            Delay_Time(100);
        }
        public void Clean_Probes()
        {
            INTERNAL_PROBE_WASH(ON_OFF.ON, 0);
            Delay_Time(100);

            SYRINGE_VOL_OPERATION(0, ENUM_SYRINGE.OPTO);
            Delay_Time(300);
            ARM_UP_DOWN(ENUM_ARM_UP_DN.DOWN);

            CheckMotor(ENUM_MOTOR200.SYRINGE);
            TROUGH_VALVE(ON_OFF.ON, 0);

            Delay_Time(300);  //Delay_Time(600);

            TROUGH_VALVE(ON_OFF.OFF, 0);
            INTERNAL_PROBE_WASH(ON_OFF.OFF, 0);
        }
        public void LogRecievedData(string strDataRecieved)
        {
            string m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            //string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            //message += Environment.NewLine;
            //message += "-----------------------------------------------------------";
            string message = string.Empty;//Environment.NewLine;
            message += string.Format("Data: {0}", strDataRecieved);
            //message += Environment.NewLine;
            // message += "-----------------------------------------------------------";
            //message += Environment.NewLine;
            try
            {
                using (StreamWriter writer = File.AppendText(m_exePath + "\\" + DateTime.Now.ToString("ddMMyyyy") + "_Received_Log.txt"))
                {
                    writer.WriteLine(message);
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
        #region ISE_IMPLEMENTATION
        public void SendISEData(ENUM_ISE_CMD ISE_OPERATION)
        {
            string strCompleteCmd = string.Format("{0}{1}{2}",(char)2, ((int)ISE_OPERATION).ToString("D3"), (char)4);
            SendISEData(strCompleteCmd, true);
            LAST_ISE_CMD = ISE_OPERATION;
        }
         public void SendISEOrder(ENUM_ISE_CMD ISE_OPERATION, int Length, string strData)
         {
            string strCompleteCmd = string.Format("{0}{1}{2}{3}{4}", (char)2, ((int)ISE_OPERATION).ToString("D3"),Length.ToString("D3"), strData, (char)4);
            SendISEData(strCompleteCmd,true);
            LAST_ISE_CMD = ISE_OPERATION;
        }
         public void Set_ISE_ResponseRequired()
         {
             isISEResponseRequired = true;
         }
         public void SendISEDataENQ()
         {
             SendOrder(ENUM_MOTOR200.ISE, 2, 0, 4, 0, 0);
         }
         public void SendISEDataNoResponse(ENUM_ISE_CMD ISE_OPERATION)
         {
            string strCompleteCmd = string.Format("{0}{1}{2}", (char)2, ISE_OPERATION.ToString("D3"), (char)4);
            SendISEData(strCompleteCmd, false);
            LAST_ISE_CMD = ISE_OPERATION;
        }
        private void SendISEData(string strCompleteCmd, bool IsResponseRequired)
        {
            int Length = strCompleteCmd.Count();

            byte[] completeCMD = new byte[Length];

            completeCMD = Encoding.ASCII.GetBytes(strCompleteCmd);
            int iIndex = 0;
            if ((Length % 5) > 0)
            {
                completeCMD = new byte[Length+4];
                string Buffer= strCompleteCmd+"0000";
                completeCMD = Encoding.ASCII.GetBytes(Buffer);
            }
            while ((Length - iIndex) > 0)
            {
                if (IsResponseRequired == false)
                    SendOrder(ENUM_MOTOR200.ISE_NO_RESPONSE, (byte)completeCMD[iIndex], (byte)completeCMD[iIndex + 1], (byte)completeCMD[iIndex + 2], (byte)completeCMD[iIndex + 3], (byte)completeCMD[iIndex + 4]);
                else
                    SendOrder(ENUM_MOTOR200.ISE, (byte)completeCMD[iIndex], (byte)completeCMD[iIndex + 1], (byte)completeCMD[iIndex + 2], (byte)completeCMD[iIndex + 3], (byte)completeCMD[iIndex + 4]);
                iIndex = iIndex + 5;
            }
            //LogRecievedData(BitConverter.ToString(completeCMD));
        }
        #endregion   

}
