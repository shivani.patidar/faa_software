using System;

namespace FaaRunApi.Models;

[Serializable]
public class ClsWorklistBO
    {
        public int ID { get; set; }
        public int Sample_Cup { get; set; }
        public int ContainerID { get; set; }
        public short Item_Count { get; set; }
        public string[] Item { get; set; }
        public string QC_Lot { get; set; }
        public short[] Dilution { get; set; }
        public string PatientID { get; set; }
        public bool[] IsPending { get; set; }
        public bool IsStat { get; set; }
        public bool[] IsISE { get; set; }
        public int SampleTypeID { get; set; }

    }