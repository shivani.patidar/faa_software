using System;
namespace FaaRunApi.Models;
 
 [Serializable]
    public class ClsBasicParameterBO
    {
        public string Item { get; set; }
        public short Method { get; set; }
        public short Primary_Filter { get; set; }
        public short Secondary_Filter { get; set; }
        public float SMP_Volume { get; set; }
        public short R1_Position { get; set; }
        public short R1_Volume { get; set; }
        public short R2_Position { get; set; }
        public short R2_Volume { get; set; }
        public short R1_Remain_Volume { get; set; }
        public short R2_Remain_Volume { get; set; }
        public short StdContainerID{ get; set; }
        public short R1_Incubation { get; set; }
        public short R2_Incubation { get; set; }
        public short Read_Time { get; set; }
        public short Standard_Count { get; set; }
        public short[] Standard_Position { get; set; }
        public float[] Standard_Conc { get; set; }
        public float[] Standard_OD { get; set; }
        public float Standard_Factor_Value { get; set; }
        public float Normal_Low { get; set; }
        public float Normal_High { get; set; }
        public float Blank_Low { get; set; }
        public float Blank_High { get; set; }
        public short Decimal { get; set; }
        public float Linearity { get; set; }
        public string Unit { get; set; }
        public short One_Or_Two { get; set; }
        public short CurveType { get; set; }
        public short SMP_DilutionRatio { get; set; }            //For Pre And AutoDilition of Sample
        public string Reagent_Lot { get; set; }
        public string Reagent_Valid { get; set; }
        public string Calibrate_Lot { get; set; }
        public string Calibrate_Valid { get; set; }
        public float K_Factor { get; set; }
        public float a_Factor { get; set; }
        public float b_Factor { get; set; }
        public float c_Factor { get; set; }
        public float d_Factor { get; set; }
        public float Ro_Factor { get; set; }
        public short Direction { get; set; }
        public bool IsDilution { get; set; }                //For Calibration
        public short Dilution_Ratio { get; set; }           //For calibration
        public short Calibrate_Num { get; set; }           //No. of Calibration Replicates
        public float A_Factor { get; set; }                 //Co-relation A and B factor
        public float B_Factor { get; set; }
        public short Calib_ValidDays { get; set; }
        public bool IsCalib_Expired { get; set; }
        public bool Is_ISE { get; set; }
    }