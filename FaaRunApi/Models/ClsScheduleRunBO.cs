using System;

namespace FaaRunApi.Models;

[Serializable]
public class ClsScheduleRunBO
{
    public int ID { get; set; }
    public int Item_ID { get; set; }
    public string Control_Code { get; set; }
    public int Cuvet { get; set; }
    public int Sample_Position { get; set; }
    public float Sample_Volume { get; set; }
    public int R1_Position { get; set; }
    public int R1_Volume { get; set; }
    public int R2_Position { get; set; }
    public int R2_Volume { get; set; }
    public int Mark { get; set; }
    public bool IsCuvWashed { get; set; }
    public int Add_R1_Time { get; set; }
    public int Add_SMP_Time { get; set; }
    public int Add_R2_Time { get; set; }
    public int R1_Incubation { get; set; }
    public int R2_Incubation { get; set; }
    public int Read_Time { get; set; }
    public float Value0 { get; set; }
    public float Value { get; set; }
    public float Result { get; set; }

    //  public short Scal_Sample { get; set; }
    // public short Scal_Sample0 { get; set; }
    // public float K_Factor { get; set; }
    public bool Is_R1_Short { get; set; }
    public bool Is_R2_Short { get; set; }
    public bool Is_SMP_Short { get; set; }
    public bool Check_Again { get; set; }
    public float Blank_Value { get; set; }
    public float Blank_Low { get; set; }
    public float Blank_High { get; set; }
    public float Normal_Low { get; set; }
    public float Normal_High { get; set; }
    public short Decimal { get; set; }
    public string Item { get; set; }
    public float Linearity { get; set; }
    public short Method { get; set; }
    public short Check_Point { get; set; }
    public double[] ABS_OD { get; set; }
    public short[] ABS_Mark { get; set; }
    public double[] ABS_Time { get; set; }
    public short Begin_Point { get; set; }       //Point at which Reading Point start  
    public short Smp_Cup_Depth { get; set; }
    public short State { get; set; }
    public short S_R1_R2_SA { get; set; }
    /*  public short Standard_Count { get; set; }
    public float[] Standard_OD { get; set; }
    public float[] Standard_Value { get; set; }
    public short Direction { get; set; }*/
    public float Absorb_Value { get; set; }
    public float Cuv_Volume { get; set; }
    public string Flag { get; set; }
    public short Dil_Cup { get; set; }
    public short AutoDil { get; set; }
    public int SampleCheck_S_ID { get; set; }
    public short SampleCheck_ITEM_ID { get; set; }
    public bool Is_ISE { get; set; }
}