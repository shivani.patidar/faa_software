using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.NewtonsoftJson;
using System;
using System.Timers;
//using System.Threading;
using FaaRunApi.Models;
using System.Collections;
using System.IO.Ports;
using SerialCommunication;
using FAA_INSTRUMENT;
using FaaRunApi;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using System.Data;

namespace FaaRunApi.Controllers;

[ApiController]
[Route("[controller]")]
public class RunController : Controller
{
     enum MARKS
    {
        NONE=0,
        SMP=1,
        R1_SMP=2,
        R2=3,
        RESULT=4,
        R1 = 5,
        DIRTY =6,
        RERUN=7,
        SHORT=8,
        ISE=10
    }

    enum RunAction
    {
        END=0,
        RUN = 1,
        END_WASH=2,
        FIRSTCYCLE = 3,
        ADD_R1_SMP=4,
        ADD_R2_2Min=5,
        ADD_R2_5Min=6,
        STR_R1_SMP=7,
        STR_R2=8,
        WATER_BLANKCHECK =9,
        CLEAN_DIRTY_CUV=10,
        INITIALIZATION = 11,
        WASH_CUV=12,
        ROTATE_RCT=13,

        ROTATE_RCT_STR=14,
        ERROR=15
    }

    enum STAT_TYPE
    {
        Normal=1,
        Emergency=2
    }

    enum ASSAY_TYPE
    {
        END_POINT=0,
        KINETIC=1,
        TWO_POINT=2
    }

       #region Declaration
        double m_Cxp;
        double m_x0, m_y0;
       
        int BeginCuv;
        MARKS[] Cuv_Marks;
        int CurrentSchedule;
        int CurrentCuv;
        int CurrentSMP;
        DateTime Start;
        DateTime End;
        double CycleTime = 12000;          //in millisec;
        RunAction CurrentAction;
        bool IsRunPaused;
        int CurrentTime;
        int CurrentCycle;
       // DispatcherTimer dispatcherTimer;
        #endregion

    bool isStarted=false;
    public static System.Timers.Timer _tm = null;
    public static int _counter = 0;

    public int Begin_Cup=1;
    public int R1_Cup=1;
    public int Str_Cup=1;

    
    
   
    
    public static ClsBasicParameterBO Parameter;
    public static List<ClsBasicParameterBO> Parameters;
    public static List<ClsWorklistBO> SampleList;
    public static ClsScheduleRunBO RunScheduleItem;
    public static List<ClsScheduleRunBO> RunSchedules;
    public static List<ClsWorklistBO> StatSampleList;
    public static List<ClsScheduleRunBO> StatRunSchedules;

       // ClsBasicTestParameterBusinessL BasicParameterBL;
 
    public static SerialPort objSPort;
    public static Initialize objPortInit;
    public static Cls_Instrument objInstrument;
    
    [HttpGet]
    [Produces("application/json")]
    public JsonResult GetCounter()         //0-STOP 1-Start 2-Pause
    {
        return Json(_counter);
    }

    [HttpGet("{strStatus}")]
    [Produces("application/json")]
    public JsonResult StartRun(int strStatus)         //0-STOP 1-Start 2-Pause
    {     
        Console.WriteLine(strStatus);
        int startStatus=Convert.ToInt32(strStatus);
        if(startStatus==1)      //Start
        {
            isStarted=true; 
            if(_tm==null)
            {
                _tm = new System.Timers.Timer(100);
                _tm.Elapsed += OnTimedEvent;
                _tm.AutoReset = true;
            }
            Instrument_Constants.CuvetCount=70;
            objPortInit = new Initialize();
            objPortInit.DataReceived += ObjSPort_DataReceived;
            //objPortInit.iDataReceived += ObjSPort_DataReceived;
            objSPort = new SerialPort();
           // if(viewModel.systemSetting.InstrumentPort==null)viewModel.systemSetting.InstrumentPort="COM2";
            Port_Configuration.strSerialPort="COM5";
            Port_Configuration.BaudRate=38400;
        // Port_Configuration.strSerialPort=Convert.ToString(listComItems.ElementAt(Convert.ToInt32(viewModel.systemSetting.InstrumentPort)).Text);
          //  Port_Configuration.BaudRate=Convert.ToInt32(listBaudItems.ElementAt(Convert.ToInt32(viewModel.systemSetting.InstrumentBaudRate)).Text);
            objPortInit.OpenPort(ref objSPort, Port_Configuration.strSerialPort, Port_Configuration.BaudRate);
            objInstrument = new Cls_Instrument(ref objPortInit);
            CurrentAction=RunAction.INITIALIZATION;
            LOAD_INST_STEPS_FROM_DB();
            PerformAction();
           // StartTimer(_tm);      
        }
        else if(startStatus==2)     //Pause
        {
            isStarted=false;
            PauseTimer(_tm);
        }
        else if(startStatus==3)     //PauseStart
        {
            isStarted=true;
            StartTimer(_tm); 
        }
        else                    //Stop
        {
            isStarted=false;
            StopTimer(_tm);
            
        }
        return Json(isStarted);
    }

    private static void StartTimer(System.Timers.Timer tm)
    {
        tm.Enabled = true;
    }

    private static void PauseTimer(System.Timers.Timer tm)
    {
        tm.Enabled = false;
    }

    private static void StopTimer(System.Timers.Timer tm)
    {
        _counter=0;
        tm.Enabled = false;
        tm.Stop();
        //tm.Dispose();
    }

    private void ObjSPort_DataReceived(byte[] RecievedData)
    {
        try
        {
            //Console.WriteLine(RecievedData);
            objInstrument.ProcessRawData(RecievedData);
            

            if (INST_STATUS.IS_INST_BUSY == false)
            {
                Console.WriteLine("IS_INST_BUSY");
                PerformAction();
            }

        }
        catch (Exception ex)
        {
            ex.ToString();
            //MessageBox.Show(ex.ToString());
        }
    }

    private static void OnTimedEvent(Object source, ElapsedEventArgs e)
    {
        _counter++;
    }

    public void GetAllTestParameters(string AllParameterdata)
    {
        DataTable dtBasicParameters = (DataTable)JsonConvert.DeserializeObject(AllParameterdata, (typeof(DataTable)));
        /* ASWCommon.Parameters = new System.Collections.ObjectModel.ObservableCollection<ClsBasicParameterBO>();
        BasicParameterBL = new ClsBasicTestParameterBusinessL();
        DataTable dtBasicParameters = new DataTable();
        dtBasicParameters = BasicParameterBL.GetBasicParameterDetailsForRun();*/
        foreach (DataRow dtRowItem in dtBasicParameters.Rows)
        {
            Parameter = new ClsBasicParameterBO();
            Parameter.Item = Convert.ToString(dtRowItem["TestCode"]);
            Parameter.Method = Convert.ToInt16(dtRowItem["TestMethodID"]);
            Parameter.Primary_Filter = Convert.ToInt16(dtRowItem["PrimaryFilter"]);
            Parameter.Secondary_Filter = Convert.ToInt16(dtRowItem["SecondayFilter"]);
            Parameter.SMP_Volume = Convert.ToInt16(dtRowItem["SampleVolume"]);
            Parameter.R1_Position = Convert.ToInt16(dtRowItem["R1Position"]);
            Parameter.R1_Volume = Convert.ToInt16(dtRowItem["R1Volume"]);
            Parameter.R2_Position = Convert.ToInt16(dtRowItem["R2Position"]);
            Parameter.R2_Volume = Convert.ToInt16(dtRowItem["R2Volume"]);
            Parameter.StdContainerID = Convert.ToInt16(dtRowItem["ContainerID"]);
            Parameter.R1_Incubation = Convert.ToInt16(dtRowItem["R1IncubationTime"]);
            Parameter.R2_Incubation = Convert.ToInt16(dtRowItem["R2IncubationTime"]);
            Parameter.Read_Time = Convert.ToInt16(dtRowItem["ReadingTime"]);
            Parameter.Standard_Count = Convert.ToInt16(dtRowItem["NoofStd"]);
            Parameter.Normal_Low = Convert.ToInt16(dtRowItem["RefLow"]);
            Parameter.Normal_High = Convert.ToInt16(dtRowItem["RefHigh"]);
            Parameter.Blank_Low = Convert.ToInt16(dtRowItem["RBLow"]);
            Parameter.Blank_High = Convert.ToInt16(dtRowItem["RBhigh"]);
            Parameter.Decimal = Convert.ToInt16(dtRowItem["DecimalPlaces"]);
            Parameter.Linearity = Convert.ToInt16(dtRowItem["Linearity"]);
            Parameter.Unit = Convert.ToString(dtRowItem["UnitName"]);
            //RATE_A_RATE_B    Parameter.One_Or_Two = Convert.ToInt16(dtRowItem["TestMethodID"]);
            Parameter.CurveType = Convert.ToInt16(dtRowItem["CurveID"]);
            // Parameter.SMP_DilutionRatio = Convert.ToInt16(dtRowItem["SMP_Dilution"]);        //For Pre And AutoDilition of Sample
            Parameter.SMP_DilutionRatio = 1;
            Parameter.Reagent_Lot = Convert.ToString(dtRowItem["ReagentLotNo"]);
            //Parameter.Reagent_Valid = Convert.ToInt16(dtRowItem["TestMethodID"]);
            // Parameter.Calibrate_Lot = Convert.ToInt16(dtRowItem["TestMethodID"]);
            // Parameter.Calibrate_Valid = Convert.ToInt16(dtRowItem["TestMethodID"]);
            Parameter.K_Factor = Convert.ToInt16(dtRowItem["K"]);
            Parameter.a_Factor = Convert.ToInt16(dtRowItem["a"]);
            Parameter.b_Factor = Convert.ToInt16(dtRowItem["b"]);
            Parameter.c_Factor = Convert.ToInt16(dtRowItem["c"]);
            Parameter.d_Factor = Convert.ToInt16(dtRowItem["d"]);
            Parameter.Ro_Factor = Convert.ToInt16(dtRowItem["R0"]);
            Parameter.Standard_Factor_Value = Convert.ToInt16(dtRowItem["K"]);
            Parameter.Direction = Convert.ToInt16(dtRowItem["KineticType_ID"]);
            // Parameter.IsDilution = Convert.ToBoolean(dtRowItem["IsStdDilution"]);               //For Calibration
            Parameter.IsDilution = false;
            // Parameter.Dilution_Ratio = Convert.ToInt16(dtRowItem["StdDilutionRatio"]);          //For calibration
            Parameter.Dilution_Ratio = 2;
            Parameter.Calibrate_Num = Convert.ToInt16(dtRowItem["StdReplicate"]);         //No. of Calibration Replicates
            Parameter.A_Factor = Convert.ToInt16(dtRowItem["XValue"]);             //Co-relation A and B factor
            Parameter.B_Factor = Convert.ToInt16(dtRowItem["YValue"]);
            Parameter.Calib_ValidDays = Convert.ToInt16(dtRowItem["StdValidDays"]);
            //   Parameter.IsCalib_Expired = Convert.ToInt16(dtRowItem["TestMethodID"]);
            Parameter.Standard_Position = new short[Parameter.Standard_Count];
            Parameter.Standard_Conc = new float[Parameter.Standard_Count];
            Parameter.Standard_OD = new float[Parameter.Standard_Count];
            for (short i=0;i<Parameter.Standard_Count;i++)
            {
                Parameter.Standard_Position[i] = i;
                Parameter.Standard_Conc[i] = i;
                Parameter.Standard_OD[i] = i;
            }
            Parameter.Is_ISE = Convert.ToBoolean(dtRowItem["IsISE"]);

            Parameters.Add(Parameter);
        }
    }
    public void AddSchedules(string WorklistData)
    {
        DataTable dtWorkList = (DataTable)JsonConvert.DeserializeObject(WorklistData, (typeof(DataTable)));
        BeginCuv = 1;
        SampleList = new List<ClsWorklistBO>();
        foreach (ClsWorklistBO worklistitem in dtWorkList.Rows)
        {
            SampleList.Add(worklistitem);
        }
        //ASWCommon.Worklist.Clear();
        foreach (ClsWorklistBO worklistitem in SampleList)
        {
            foreach (string item in worklistitem.Item)
            {
                RunScheduleItem = new ClsScheduleRunBO();
                RunScheduleItem.ID = worklistitem.ID;
                RunScheduleItem.Item_ID = Parameters.IndexOf(Parameters.Where(p => p.Item == item).FirstOrDefault());
                RunScheduleItem.Item = item;
                RunScheduleItem.Cuvet = BeginCuv++;
                if (RunScheduleItem.Cuvet > 70)
                {
                    RunScheduleItem.Cuvet = 1;
                    BeginCuv = 1;
                }
                RunScheduleItem.Is_R1_Short = false;
                RunScheduleItem.Is_R2_Short = false;
                RunScheduleItem.Is_SMP_Short = false;

                RunScheduleItem.IsCuvWashed = false;        //not sure why used
                RunScheduleItem.Flag = string.Empty;
                RunScheduleItem.R1_Position = Parameters[RunScheduleItem.Item_ID].R1_Position;
                RunScheduleItem.R2_Position = Parameters[RunScheduleItem.Item_ID].R2_Position;
                RunScheduleItem.R1_Volume = Parameters[RunScheduleItem.Item_ID].R1_Volume;
                RunScheduleItem.R2_Volume = Parameters[RunScheduleItem.Item_ID].R2_Volume;
                RunScheduleItem.Control_Code = worklistitem.QC_Lot;
                RunScheduleItem.Sample_Position = worklistitem.Sample_Cup;
                RunScheduleItem.Sample_Volume = Parameters[RunScheduleItem.Item_ID].SMP_Volume;
                RunScheduleItem.Mark = Convert.ToInt32(MARKS.NONE);
                Cuv_Marks[RunScheduleItem.Cuvet-1]= (MARKS)RunScheduleItem.Mark;
                RunScheduleItem.IsCuvWashed = true;
                RunScheduleItem.Add_R1_Time = 0;
                RunScheduleItem.Add_SMP_Time = 0;
                RunScheduleItem.Add_R2_Time = 0;
                RunScheduleItem.R1_Incubation = Parameters[RunScheduleItem.Item_ID].R1_Incubation;
                RunScheduleItem.R2_Incubation = Parameters[RunScheduleItem.Item_ID].R2_Incubation;
                RunScheduleItem.Read_Time = Parameters[RunScheduleItem.Item_ID].Read_Time;
                RunScheduleItem.Value0 = 0.000f;
                RunScheduleItem.Value = 0.000f;
                RunScheduleItem.Result = 0.000f;
                RunScheduleItem.Is_R1_Short = false;
                RunScheduleItem.Is_R2_Short = false;
                RunScheduleItem.Is_SMP_Short = false;
                //RunScheduleItem.Check_Again
                RunScheduleItem.Linearity = Parameters[RunScheduleItem.Item_ID].Linearity;
                RunScheduleItem.Method = Parameters[RunScheduleItem.Item_ID].Method;
                RunScheduleItem.Normal_High= Parameters[RunScheduleItem.Item_ID].Normal_High;
                RunScheduleItem.Normal_Low = Parameters[RunScheduleItem.Item_ID].Normal_Low;
                //RunScheduleItem.Check_Point
                RunScheduleItem.ABS_OD = new double[80];
                //RunScheduleItem.ABS_Mark
                RunScheduleItem.ABS_Time = new double[80];
                //RunScheduleItem.Begin_Point
                //RunScheduleItem.Smp_Cup_Depth= steps as per sample container ID Type 
                //RunScheduleItem.State
                //RunScheduleItem.S_R1_R2_SA
                //RunScheduleItem.Absorb_Value
                //RunScheduleItem.Cuv_Volume
                RunScheduleItem.Flag = string.Empty;
                //RunScheduleItem.Dil_Cup
                //RunScheduleItem.AutoDil
                //RunScheduleItem.SampleCheck_S_ID
                //RunScheduleItem.SampleCheck_ITEM_ID
                RunSchedules.Add(RunScheduleItem);
            }

        }
    }
    public void AddStatSchedules(string StatListData,string StatType)
    {
        BeginCuv = 1;
        DataTable dtStatSampleList = (DataTable)JsonConvert.DeserializeObject(StatListData, (typeof(DataTable)));

        foreach (ClsWorklistBO schedItem in dtStatSampleList.Rows)
        {
            StatSampleList.Add(schedItem);
            SampleList.Add(schedItem);
        }
        STAT_TYPE statType = (STAT_TYPE)Convert.ToInt32(StatType);
        switch (statType)
        {
            case STAT_TYPE.Normal:
                AddNormalStatSchedules();
                break;
            case STAT_TYPE.Emergency:
                if (RunSchedules.Where(p => p.Mark == (int)MARKS.NONE).Count() > 0)
                    AddEmergencyStatSchedules();
                else
                    AddNormalStatSchedules();
                break;
            default:
                break;
        }
        StatSampleList.Clear();
    }
    public void AddNormalStatSchedules()
    {
        foreach (ClsWorklistBO worklistitem in StatSampleList)
        {
            foreach (string item in worklistitem.Item)
            {
                RunScheduleItem = new ClsScheduleRunBO();
                RunScheduleItem.ID = worklistitem.ID;
                RunScheduleItem.Item_ID = Parameters.IndexOf(Parameters.Where(p => p.Item == item).FirstOrDefault());
                RunScheduleItem.Item = item;
                RunScheduleItem.Cuvet = RunSchedules[RunSchedules.Count() - 1].Cuvet + 1;
                if (RunScheduleItem.Cuvet > 60)
                {
                    RunScheduleItem.Cuvet = 1;
                    BeginCuv = 1;
                }
                RunScheduleItem.Is_R1_Short = false;
                RunScheduleItem.Is_R2_Short = false;
                RunScheduleItem.Is_SMP_Short = false;

                RunScheduleItem.IsCuvWashed = false;        //not sure why used
                RunScheduleItem.Flag = string.Empty;
                RunScheduleItem.R1_Position =Parameters[RunScheduleItem.Item_ID].R1_Position;
                RunScheduleItem.R2_Position = Parameters[RunScheduleItem.Item_ID].R2_Position;
                RunScheduleItem.R1_Volume = Parameters[RunScheduleItem.Item_ID].R1_Volume;
                RunScheduleItem.R2_Volume = Parameters[RunScheduleItem.Item_ID].R2_Volume;
                RunScheduleItem.Control_Code = worklistitem.QC_Lot;
                RunScheduleItem.Sample_Position = worklistitem.Sample_Cup;
                RunScheduleItem.Sample_Volume = Parameters[RunScheduleItem.Item_ID].SMP_Volume;
                RunScheduleItem.Mark = Convert.ToInt32(MARKS.NONE);
                Cuv_Marks[RunScheduleItem.Cuvet - 1] = (MARKS)RunScheduleItem.Mark;
                RunScheduleItem.IsCuvWashed = true;
                RunScheduleItem.Add_R1_Time = 0;
                RunScheduleItem.Add_SMP_Time = 0;
                RunScheduleItem.Add_R2_Time = 0;
                RunScheduleItem.R1_Incubation = Parameters[RunScheduleItem.Item_ID].R1_Incubation;
                RunScheduleItem.R2_Incubation = Parameters[RunScheduleItem.Item_ID].R2_Incubation;
                RunScheduleItem.Read_Time = Parameters[RunScheduleItem.Item_ID].Read_Time;
                RunScheduleItem.Value0 = 0.000f;
                RunScheduleItem.Value = 0.000f;
                RunScheduleItem.Result = 0.000f;
                RunScheduleItem.Is_R1_Short = false;
                RunScheduleItem.Is_R2_Short = false;
                RunScheduleItem.Is_SMP_Short = false;
                //RunScheduleItem.Check_Again
                RunScheduleItem.Linearity = Parameters[RunScheduleItem.Item_ID].Linearity;
                RunScheduleItem.Method = Parameters[RunScheduleItem.Item_ID].Method;
                RunScheduleItem.Normal_High = Parameters[RunScheduleItem.Item_ID].Normal_High;
                RunScheduleItem.Normal_Low = Parameters[RunScheduleItem.Item_ID].Normal_Low;
                //RunScheduleItem.Check_Point
                //RunScheduleItem.ABS_OD
                //RunScheduleItem.ABS_Mark
                //RunScheduleItem.ABS_Time
                //RunScheduleItem.Begin_Point
                //RunScheduleItem.Smp_Cup_Depth= steps as per sample container ID Type 
                //RunScheduleItem.State
                //RunScheduleItem.S_R1_R2_SA
                //RunScheduleItem.Absorb_Value
                //RunScheduleItem.Cuv_Volume
                RunScheduleItem.Flag = string.Empty;
                //RunScheduleItem.Dil_Cup
                //RunScheduleItem.AutoDil
                //RunScheduleItem.SampleCheck_S_ID
                //RunScheduleItem.SampleCheck_ITEM_ID
                RunSchedules.Add(RunScheduleItem);
            }

        }
    }
    public void AddEmergencyStatSchedules()
    {
        foreach (ClsWorklistBO worklistitem in StatSampleList)
        {
            foreach (string item in worklistitem.Item)
            {
                RunScheduleItem = new ClsScheduleRunBO();
                RunScheduleItem.ID = worklistitem.ID;
                RunScheduleItem.Item_ID = Parameters.IndexOf(Parameters.Where(p => p.Item == item).FirstOrDefault());
                RunScheduleItem.Item = item;
                RunScheduleItem.Cuvet = RunSchedules.Where(p => p.Mark == (int)MARKS.R1).Last().Cuvet + 1;
                if (RunScheduleItem.Cuvet > 60)
                {
                    RunScheduleItem.Cuvet = 1;
                    BeginCuv = 1;
                }
                RunScheduleItem.Is_R1_Short = false;
                RunScheduleItem.Is_R2_Short = false;
                RunScheduleItem.Is_SMP_Short = false;
                RunScheduleItem.IsCuvWashed = false;        //not sure why used
                RunScheduleItem.Flag = string.Empty;
                RunScheduleItem.R1_Position = Parameters[RunScheduleItem.Item_ID].R1_Position;
                RunScheduleItem.R2_Position = Parameters[RunScheduleItem.Item_ID].R2_Position;
                RunScheduleItem.R1_Volume = Parameters[RunScheduleItem.Item_ID].R1_Volume;
                RunScheduleItem.R2_Volume = Parameters[RunScheduleItem.Item_ID].R2_Volume;
                RunScheduleItem.Control_Code = worklistitem.QC_Lot;
                RunScheduleItem.Sample_Position = worklistitem.Sample_Cup;
                RunScheduleItem.Sample_Volume = Parameters[RunScheduleItem.Item_ID].SMP_Volume;
                RunScheduleItem.Mark = Convert.ToInt32(MARKS.NONE);
                Cuv_Marks[RunScheduleItem.Cuvet - 1] = (MARKS)RunScheduleItem.Mark;
                RunScheduleItem.IsCuvWashed = true;
                RunScheduleItem.Add_R1_Time = 0;
                RunScheduleItem.Add_SMP_Time = 0;
                RunScheduleItem.Add_R2_Time = 0;
                RunScheduleItem.R1_Incubation = Parameters[RunScheduleItem.Item_ID].R1_Incubation;
                RunScheduleItem.R2_Incubation = Parameters[RunScheduleItem.Item_ID].R2_Incubation;
                RunScheduleItem.Read_Time = Parameters[RunScheduleItem.Item_ID].Read_Time;
                RunScheduleItem.Value0 = 0.000f;
                RunScheduleItem.Value = 0.000f;
                RunScheduleItem.Result = 0.000f;
                RunScheduleItem.Is_R1_Short = false;
                RunScheduleItem.Is_R2_Short = false;
                RunScheduleItem.Is_SMP_Short = false;
                //RunScheduleItem.Check_Again
                RunScheduleItem.Linearity = Parameters[RunScheduleItem.Item_ID].Linearity;
                RunScheduleItem.Method = Parameters[RunScheduleItem.Item_ID].Method;
                RunScheduleItem.Normal_High = Parameters[RunScheduleItem.Item_ID].Normal_High;
                RunScheduleItem.Normal_Low = Parameters[RunScheduleItem.Item_ID].Normal_Low;
                //RunScheduleItem.Check_Point
                //RunScheduleItem.ABS_OD
                //RunScheduleItem.ABS_Mark
                //RunScheduleItem.ABS_Time
                //RunScheduleItem.Begin_Point
                //RunScheduleItem.Smp_Cup_Depth= steps as per sample container ID Type 
                //RunScheduleItem.State
                //RunScheduleItem.S_R1_R2_SA
                //RunScheduleItem.Absorb_Value
                //RunScheduleItem.Cuv_Volume
                RunScheduleItem.Flag = string.Empty;
                //RunScheduleItem.Dil_Cups
                //RunScheduleItem.AutoDil
                //RunScheduleItem.SampleCheck_S_ID
                //RunScheduleItem.SampleCheck_ITEM_ID
                foreach(ClsScheduleRunBO SchedItem in RunSchedules)
                {
                    int index = RunSchedules.IndexOf(SchedItem);
                    int indexR1 = RunSchedules.IndexOf(RunSchedules.Where(p => p.Mark == (int)MARKS.R1).Last());
                    if (index>indexR1)
                    {
                        SchedItem.Cuvet = SchedItem.Cuvet + 1;
                    }
                }
                RunSchedules.Insert(RunSchedules.IndexOf(RunSchedules.Where(p => p.Mark == (int)MARKS.R1).Last())+1, RunScheduleItem);
            }            
        }
    }
    public void PerformAction()
    {
        switch (CurrentAction)
        {
            case RunAction.END:
                //MessageBox.Show("Run Completed");
                //dispatcherTimer.Stop();
                break;
            case RunAction.ROTATE_RCT:
                //Console.WriteLine("ROTATE_RCT"+Begin_Cup);
                //Begin_Cup+=60;
                objInstrument.CheckMotor(ENUM_MOTOR200.RCT); 
                objInstrument.RCT_ROTATE(Begin_Cup+59);
                objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                CurrentAction=RunAction.WASH_CUV;
            break;
            case RunAction.WASH_CUV:
                bool m_OutWater = false;
                objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                objInstrument.CLEANING_PUMP(ON_OFF.ON,0);
                objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.DOWN);
                objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                if (Begin_Cup <= Instrument_Constants.CuvetCount)
                {
                    objInstrument.CRU_DISPENSE_PUMP(ON_OFF.ON, 0);
                    objInstrument.Delay_Time(10);
                    objInstrument.CRU_DISPENSE_PUMP(ON_OFF.OFF, 0);
                    m_OutWater = true;
                }
                else objInstrument.Delay_Time(10);
                //objInstrument.Delay_Time(40);
                objInstrument.CLEANING_PUMP(ON_OFF.OFF, 0);
                if (Begin_Cup <= Instrument_Constants.CuvetCount)
                {
                    objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.DOWN_MID1);
                    objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                    objInstrument.CRU_DISPENSE_PUMP(ON_OFF.ON, 0);
                    objInstrument.Delay_Time(CRU_STEPS.CRU_ADD_WATER_TIME);
                    objInstrument.CRU_DISPENSE_PUMP(ON_OFF.OFF, 0);
                }
                else
                {
                    objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.DOWN_MID1);
                    objInstrument.Delay_Time(CRU_STEPS.CRU_ADD_WATER_TIME);
                }
                if (m_OutWater) objInstrument.CLEANING_PUMP(ON_OFF.ON, 0);
                objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                if (m_OutWater)
                {
                    objInstrument.DI_BACK_PUMP(ON_OFF.ON, CRU_STEPS.CRU_PUMP_BACK_TIME);
                    objInstrument.Delay_Time(50);
                    objInstrument.CLEANING_PUMP(ON_OFF.OFF, 0);
                }
                objInstrument.CRU_UP_DOWN(ENUM_CRU_UPDN.UP_MAX);
                objInstrument.CheckMotor(ENUM_MOTOR200.CRU);
                objInstrument.Delay_Time(20);
                Begin_Cup++;
                CurrentAction=RunAction.ROTATE_RCT;
                if(Begin_Cup>70)Begin_Cup-=70;
                Console.WriteLine("Begin_Cup_COMP"+Begin_Cup);
                if(Begin_Cup-10==R1_Cup)
                {
                    Console.WriteLine("R1_Cup"+R1_Cup);
                    CurrentAction=RunAction.ADD_R1_SMP;
                }
            break;
            case RunAction.ADD_R1_SMP:
                {
                    objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                    objInstrument.RGT_ROTATE_RG_POS_IN(1);           //to be update as per parameter reagent pos
                    objInstrument.CheckMotor(ENUM_MOTOR200.RGT);
                    objInstrument.Delay_Time(40);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.ARM_ROTATE(ENUM_ARM_ROT.RGT_IN);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                    objInstrument.Delay_Time(40);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.LLS_VOD);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
//ADD R1 SYRINGE ASPIRATION WITH AIR;
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.Delay_Time(40);
                    objInstrument.ARM_ROTATE(ENUM_ARM_ROT.HOME);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.DOWN);
                    objInstrument.TROUGH_VALVE(ON_OFF.ON,200);
                    objInstrument.Delay_Time(40);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.ARM_ROTATE(ENUM_ARM_ROT.SMP);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                    objInstrument.Delay_Time(40);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.LLS_VOD);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
//ADD SMP SYRINGE ASPIRATION;
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.Delay_Time(40);
                    objInstrument.ARM_ROTATE(ENUM_ARM_ROT.CUVET);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                    objInstrument.Delay_Time(40);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.DOWN_CUV);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.Delay_Time(40);
//ADD SMP_R1 SYRINGE DISPENSE;
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_UPDOWN);
                    objInstrument.Delay_Time(40);
                    objInstrument.ARM_ROTATE(ENUM_ARM_ROT.HOME);
                    objInstrument.CheckMotor(ENUM_MOTOR200.ARM_ROTATE);
                    objInstrument.Delay_Time(40);
                    objInstrument.ARM_UP_DOWN(ENUM_ARM_UP_DN.DOWN);
                    objInstrument.TROUGH_VALVE(ON_OFF.ON,200);
                    objInstrument.INTERNAL_PROBE_WASH(ON_OFF.ON,200);
                    objInstrument.Delay_Time(40);
                    CurrentAction=RunAction.ROTATE_RCT_STR;
                }
            break;
            case RunAction.ROTATE_RCT_STR:
                if(R1_Cup>70)R1_Cup-=70;    
                objInstrument.RCT_ROTATE((int)(R1_Cup+48));
                Console.WriteLine("R1Cup"+R1_Cup);
                objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                CurrentAction=RunAction.STR_R1_SMP;
            break;
            case RunAction.STR_R1_SMP:
                {
                    objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.STR_UPDOWN);
                    objInstrument.Delay_Time(40);
                    objInstrument.STR_ROTATE(ENUM_STR_ROT.CUVET);
                        objInstrument.CheckMotor(ENUM_MOTOR200.STR_ROTATE);
                    objInstrument.Delay_Time(40);
                    objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.DOWN);
                    objInstrument.CheckMotor(ENUM_MOTOR200.STR_UPDOWN);
                    objInstrument.Delay_Time(40);
                    objInstrument.STIRRER_REVOLVE(ON_OFF.ON,100,20);
                    objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.UP_MAX);
                    objInstrument.CheckMotor(ENUM_MOTOR200.STR_UPDOWN);
                    objInstrument.Delay_Time(40);
                    objInstrument.STR_ROTATE(ENUM_STR_ROT.HOME);
                    objInstrument.CheckMotor(ENUM_MOTOR200.STR_ROTATE);
                    objInstrument.Delay_Time(40);
                    objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.DOWN);
                    objInstrument.TROUGH_VALVE(ON_OFF.ON,200);
                    objInstrument.CheckMotor(ENUM_MOTOR200.STR_UPDOWN);
                    objInstrument.Delay_Time(40);
                    objInstrument.STR_UP_DOWN(ENUM_STR_UPDN.UP_MAX);
                    Console.WriteLine("R1_Cup"+R1_Cup);
                    R1_Cup++;
                    if(R1_Cup>70)R1_Cup-=70;
                    CurrentAction = RunAction.ROTATE_RCT;
                }  
            break;
            case RunAction.ADD_R2_2Min:
            break;
            case RunAction.ADD_R2_5Min:

            break;
            case RunAction.STR_R2:

            break;
            case RunAction.RUN:
                // Ready_Action();
                //Instrument_Add_Reagent_And_Blood();
                //UpdateRack();
                // if (ASWCommon.RunSchedules.Where(p => p.Mark == (int)MARKS.RESULT).Count() >= ASWCommon.RunSchedules.Count)
                //     CurrentAction = RunAction.END;
                break;
            case RunAction.END_WASH:
                break;
            case RunAction.FIRSTCYCLE:
                break;
            case RunAction.WATER_BLANKCHECK:
                break;
            case RunAction.CLEAN_DIRTY_CUV:
                break;
            case RunAction.INITIALIZATION:
                objInstrument.INIT_INSTRUMENT();
                // objInstrument.CheckMotor(ENUM_MOTOR200.RCT);
                //initializeChart();
                CurrentAction = RunAction.ROTATE_RCT;
                Console.WriteLine("RunAction:"+CurrentAction);
                CurrentCycle = 0;
                break;
            case RunAction.ERROR:
                break;
            default:
                break;
        }
        objInstrument.SendEnd();
    }
    public bool Ready_Action()
    {
        /* if (CurrentSchedule < ASWCommon.RunSchedules.Count)
        {
            if (ASWCommon.RunSchedules[CurrentSchedule].S_R1_R2_SA == 2 || ASWCommon.RunSchedules[CurrentSchedule].S_R1_R2_SA == 4 || ASWCommon.RunSchedules[CurrentSchedule].S_R1_R2_SA == 3)
            {
                CurrentSchedule++;
                return false;
            }
            //  if (ASWCommon.RunSchedules[CurrentSchedule].ID == -6 || ASWCommon.RunSchedules[CurrentSchedule].ID == -8)
            // m_Dilit_Rec = ASWCommon.RunSchedules[CurrentSchedule].Cuvet;
            short M_Method = ASWCommon.Parameters[ASWCommon.RunSchedules[CurrentSchedule].Item_ID].Method;

            if (ASWCommon.RunSchedules[CurrentSchedule].ID > 0 || ASWCommon.RunSchedules[CurrentSchedule].ID == -2)
            {
                if ((M_Method == (short)ASSAY_TYPE.END_POINT && ASWCommon.Parameters[ASWCommon.RunSchedules[CurrentSchedule].Item_ID].One_Or_Two == 2)
                    || (M_Method == (short)ASSAY_TYPE.KINETIC && ASWCommon.RunSchedules[CurrentSchedule].R2_Position > 0)
                    || (M_Method == (short)ASSAY_TYPE.TWO_POINT && ASWCommon.RunSchedules[CurrentSchedule].R2_Position > 0))
                {
                }

            }
            
        }*/
        return true;
    }
    public void Instrument_Add_Reagent_And_Blood()
    {
        /* if (!IsRunPaused)
            JudgeStatScheduling();
        CurrentTime = (int)(DateTime.Now- Start).TotalMilliseconds;
        
        if (CurrentSchedule < ASWCommon.RunSchedules.Count)
        {

            if (!IsRunPaused)
            {
                if (ASWCommon.RunSchedules[CurrentSchedule].Mark == (int)MARKS.NONE)
                {
                    ASWCommon.RunSchedules[CurrentSchedule].Mark = (int)MARKS.R1;

                    ASWCommon.RunSchedules[CurrentSchedule].Add_R1_Time = CurrentTime;
                    Cuv_Marks[ASWCommon.RunSchedules[CurrentSchedule].Cuvet - 1] = (MARKS)ASWCommon.RunSchedules[CurrentSchedule].Mark;
                }
                CurrentSchedule++;
            }
        }
        CheckForR2orSample();
        CurrentCycle++;*/

    }
    public void CheckForR2orSample()
    {
        /* int TimePassed, R1Incubation, R2Incubation, ReadTime,i=0;
        foreach(ClsScheduleRunBO scheduleItem in ASWCommon.RunSchedules)
        {
            MARKS locMark = (MARKS)scheduleItem.Mark;
            if (scheduleItem.Add_R1_Time == 0)
                scheduleItem.Add_R1_Time = 1;
            i = Convert.ToInt16((CurrentTime - scheduleItem.Add_R1_Time)/CycleTime);
            scheduleItem.ABS_Time[i] = (CurrentTime- scheduleItem.Add_R1_Time)/1000;
            Random random = new Random();
            scheduleItem.ABS_OD[i] = Convert.ToDouble(random.Next(0,4));

            switch (locMark)
            {                   
                case MARKS.SMP:
                        TimePassed = CurrentTime - scheduleItem.Add_R1_Time;                        
                        R1Incubation = ASWCommon.Parameters[scheduleItem.Item_ID].R1_Incubation * 1000;                     
                        ReadTime = ASWCommon.Parameters[scheduleItem.Item_ID].Read_Time * 1000;
                    if (scheduleItem.Mark == (int)MARKS.SMP && R1Incubation - TimePassed <= 0 && (double)ASWCommon.Parameters[scheduleItem.Item_ID].R2_Position > 0)
                    {
                        scheduleItem.Mark = (int)MARKS.R2;
                        Cuv_Marks[scheduleItem.Cuvet - 1] = (MARKS)scheduleItem.Mark;
                        scheduleItem.Add_R2_Time = CurrentTime;
                    }
                    else if (TimePassed > (R1Incubation + ReadTime))
                    {
                        scheduleItem.Mark = (int)MARKS.RESULT;
                        Cuv_Marks[scheduleItem.Cuvet - 1] = (MARKS)scheduleItem.Mark;
                    }
                    break;
                case MARKS.R1_SMP:
                    break;
                case MARKS.R2:
                    TimePassed = CurrentTime - scheduleItem.Add_R1_Time;
                    R1Incubation = ASWCommon.Parameters[scheduleItem.Item_ID].R1_Incubation * 100;
                    R2Incubation = ASWCommon.Parameters[scheduleItem.Item_ID].R2_Incubation * 100;
                    ReadTime = ASWCommon.Parameters[scheduleItem.Item_ID].Read_Time * 1000;
                    if (TimePassed > (R1Incubation + R2Incubation + ReadTime))
                    {
                        scheduleItem.Mark = (int)MARKS.RESULT;
                        Cuv_Marks[scheduleItem.Cuvet - 1] = (MARKS)scheduleItem.Mark;
                    }
                    break;
                case MARKS.RESULT:
                    break;
                case MARKS.R1:
                    TimePassed = CurrentTime - scheduleItem.Add_R1_Time;              
                    if (TimePassed > 240)
                    {
                        scheduleItem.Mark = (int)MARKS.SMP;
                        Cuv_Marks[scheduleItem.Cuvet - 1] = (MARKS)scheduleItem.Mark;
                        scheduleItem.Add_SMP_Time = CurrentTime;
                    }
                    break;                 
                default:
                    break;
            }
        }*/
    }
    public bool JudgeStatScheduling()
    {
        /*if (ASWCommon.Worklist.Count > 0)
        {
            AddStatSchedules();
            return true;
        }
        else
            return false;*/

        return false;    
    }
    public void LOAD_INST_STEPS_FROM_DB()
        {
            //Port_Configuration.strSerialPort = "COM2";
           // Port_Configuration.BaudRate = 38400;
            ARMSTEPS.ARM_DN_CUV = 500;
            ARMSTEPS.ARM_DN_DIL_CUV = 500;
            ARMSTEPS.ARM_DN_HOME = 500;
            ARMSTEPS.ARM_DN_ISE = 575;
            ARMSTEPS.ARM_DN_RGT = 500;         // Dead Volume of Reagent Bottle
            ARMSTEPS.ARM_DN_SMP = 500;         // Dead Volume of Sample Container Type
            ARMSTEPS.ARM_ROT_CUV = 630;
            ARMSTEPS.ARM_ROT_DIL_CUV = 38;
            ARMSTEPS.ARM_ROT_ISE = 1720;
            ARMSTEPS.ARM_ROT_RGT_IN = 800;
            ARMSTEPS.ARM_ROT_RGT_OUT = 640;
            ARMSTEPS.ARM_ROT_SMP = 460;
            ARMSTEPS.ARM_UP_MAX = 3500;
            ARMSTEPS.ARM_UP_OFFSET = 100;
            STR_STEPS.STR_DN_CUV = 885;
            STR_STEPS.STR_DN_HOME = 450;
            STR_STEPS.STR_UP_MAX = 1100;
            STR_STEPS.STR_ROT_CUV = 675;
            CRU_STEPS.CRU_DOWN =695;
            CRU_STEPS.CRU_DI_DISPENSE = 220;
            CRU_STEPS.CRU_UP_MAX = 2800;
            CRU_STEPS.CRU_MID2 = 12;
            CRU_STEPS.CRU_MID1 = CRU_STEPS.CRU_DOWN- CRU_STEPS.CRU_DI_DISPENSE- CRU_STEPS.CRU_MID2;            
            CRU_STEPS.CRU_ADD_WATER_TIME =27;
            CRU_STEPS.CRU_PUMP_BACK_TIME = 11;
            RGT_STEPS.RGT_RG_POS_IN_1 = 1460;
            RGT_STEPS.RGT_RG_POS_OUT_1 = 1460;
            RGT_STEPS.RGT_SMP_POS1 = 1475;
            RGT_STEPS.RGT_RG_BARCODE = 3040;
            RGT_STEPS.RGT_SMP_BARCODE = 2950;
            Instrument_Constants.ReagentCount = 40;
            Instrument_Constants.CuvetCount = 70;
            Instrument_Constants.SampleCount = 40;
            Instrument_Constants.OpticalPathLength = 0.65;
            Instrument_Constants.FilterCount = 9;
            Instrument_Constants.Filters = new int[Instrument_Constants.FilterCount];
            Instrument_Constants.Filters[0] = 340;
            Instrument_Constants.Filters[1] = 405;
            Instrument_Constants.Filters[2] = 450;
            Instrument_Constants.Filters[3] = 510;
            Instrument_Constants.Filters[4] = 546;
            Instrument_Constants.Filters[5] = 578;
            Instrument_Constants.Filters[6] = 620;
            Instrument_Constants.Filters[7] = 670;
            Instrument_Constants.Filters[8] = 700;
            Instrument_Constants.Signal_Threshold = 35000;
            SYRINGE.SYRINGE_TYPE = ENUM_SYRINGE_TYPE.KEYTO_SYRINGE;
            SYRINGE.SYRINGE_OFFSET = 20;
            SYRINGE.SYRINGE_WASHTIME = 150;
            INST_STATUS.LAMP_STATUS = ON_OFF.ON;
            INST_STATUS.IS_INST_BUSY = false;
            CRU_STEPS.CRU_WASH_HANDS = 8;
            Instrument_Constants.CRUHands = new int[CRU_STEPS.CRU_WASH_HANDS];
            Instrument_Constants.ConnectedInstrument = ENUM_INSTRUMENTS.Nano200;
            Instrument_Constants.Cuv_WL_GWC = 16;
            Instrument_Constants.Cuv_WL_WASH = 12;
            Instrument_Constants.ISE_CALIBRATOR_POS = 30;
            Instrument_Constants.ISE_CLEANER_POS = 29;
            Instrument_Constants.ISE_CONDITIONER_POS = 28;

        } 
}